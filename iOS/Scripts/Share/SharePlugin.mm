//
//  SharePlugin.mm
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  Unity側とのシェア関連で連携を行うプラグイン

#import "ShareController.h"

//=================================================================================
//Unity側との連携
//=================================================================================


extern "C" {
    
    //初期化、ネイティブ側のプラグインのインスタンスを作成し、Unity側へ渡す
    void *_SharePlugin_Init(const char *gameObjectName){
        id instance = [[ShareController alloc]initWithGameObjectName:gameObjectName];
        CFRetain((CFTypeRef)instance);
        return (__bridge void *)instance;
    }
    
    //Tweet
    void _Tweet(void *instance, const char *text) {
        ShareController *controller = (__bridge ShareController*)instance;
        [controller tweet:text];
    }
    
}


