//
//  RankingController.m
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  ランキング関係の処理を行うクラス

#import "RankingController.h"

@implementation RankingController

//=================================================================================
//初期化
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName{
    self = [super init];
    
    [[GameCenter sharedGameCenter] startGameCenter];
    
    return self;
}

//=================================================================================
//内部
//=================================================================================

//リーダーボードで完了タップ時の処理、前の画面に戻る
- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString*)getRankingKey:(int)rankingNo{
        
    return RANKING_KEY_1;
}

//=================================================================================
//外部
//=================================================================================

//スコアをリーダーボードに送信
-(void)reportScore:(int)rankingNo intScore:(int)score{
    NSString* rankingKey = [self getRankingKey:rankingNo];
    NSLog(@"reportScore %@ : %d",rankingKey , score);
    [[GameCenter sharedGameCenter]reportScore:score Category:rankingKey];
}
-(void)reportScore:(int)rankingNo floatScore:(float)score{
    NSString* rankingKey = [self getRankingKey:rankingNo];
    NSLog(@"reportScore %@ : %f",rankingKey , score);
    [[GameCenter sharedGameCenter]reportScore:score Category:rankingKey];
}

//リーダーボード表示
-(void)showLeaderBoard{
    NSLog(@"showLeaderBoard");
    [[GameCenter sharedGameCenter]showLeaderboard];
}

-(void) getUserRanking{
    [[GameCenter sharedGameCenter]getUserRanking];
}

@end
