package jp.oliviaashley.Advertising;

import android.app.Activity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import jp.oliviaashley.Net.Utils.SimpleHttpRequestHelper;
import jp.oliviaashley.Twins.R;

public class CheckRecomJson {
    // XXX join into FlagsManager

    private static final String TAG = "CheckRecomJson";

    private String flgString;
    private String schemeString;
    private String titleString;
    private String urlString;

    public CheckRecomJson(Activity activity) {

        String FLG_ID;
        if (Locale.getDefault().equals(Locale.JAPAN)) {
            FLG_ID = activity.getResources().getString(R.string.Recom_FLG_ID);
        } else {
            FLG_ID = activity.getResources().getString(R.string.Recom_FLG_ID_E);
        }

        String url = activity.getResources().getString(R.string.Fms_url)
                + activity.getResources().getString(R.string.Recom_APP_ID)
                + "/" + FLG_ID;

        JSONObject jsonObject = SimpleHttpRequestHelper.getJSONObject(url);
        if (jsonObject != null) {
            try {
                flgString = jsonObject.getString("flg");
                schemeString = jsonObject.getString("scheme");
                titleString = jsonObject.getString("title");
                urlString = jsonObject.getString("url");
            } catch (JSONException e) {
                Log.e(TAG, "JSONException:" + e.toString());
            }
        }
    }

    public boolean checkFlg() {
        if (flgString != null) {
            int flgInt = Integer.parseInt(flgString);
            return flgInt == 1;
        } else {
            return false;
        }
    }

    public String getScheme() {
        return schemeString;
    }

    public String getTitle() {
        return titleString;
    }

    public String getUrl() {
        return urlString;
    }
}
