﻿using UnityEditor;

/// <summary>
/// Staticのテンプレートから新しくスクリプトを作るクラス
/// </summary>
public class StaticScriptCreator : CustomScriptCreator {

	private const string TEMPLATE_SCRIPT_NAME  = "Static";

	[MenuItem(MENU_PATH + TEMPLATE_SCRIPT_NAME)]
	private static void CreateScript(){
		ShowWindow (TEMPLATE_SCRIPT_NAME);
	}

}
