﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreButtonAssistant : ButtonAssistant
{

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		#if UNITY_ANDROID
		AdManager.Instance.Restore ();
		#else
		IAPController.Instance.RestorePurchases ();
		#endif
	}

}
