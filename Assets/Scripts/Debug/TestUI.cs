﻿//  TestUI.cs
//  ProductName EraserCrashers
//
//  Created by kikuchikan on 2015.06.25.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// テスト用のUIを表示するクラス
/// </summary>
public class TestUI : MonoBehaviour {
	
	//GUIスタイル
	protected GUIStyle _labelStyle, _buttonStyle;

	// 基準とする解像度
	protected static readonly Vector2 SCREEN_SIZE = new Vector2(640, 1136);	

	//=================================================================================
	//初期化
	//=================================================================================

	protected virtual void Awake(){

		//ラベルのスタイル
		_labelStyle = new GUIStyle();
		_labelStyle.fontSize = 40;
		_labelStyle.wordWrap = true;

		GUIStyleState styleState = new GUIStyleState();
		styleState.textColor = Color.black;
		_labelStyle.normal = styleState;
	}

	//=================================================================================
	//GUI表示
	//=================================================================================

	protected virtual void OnGUI () {

		GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / SCREEN_SIZE.x, Screen.height / SCREEN_SIZE.y), Vector2.zero);

		//ボタンのスタイル
		_buttonStyle = new GUIStyle(GUI.skin.button);
		_buttonStyle.fontSize = 30;

		UpdateGUI ();

		// GUIの解像度を元に戻す
		GUI.matrix = Matrix4x4.identity;
	}

	/// <summary>
	/// 継承先でGUIの処理を書く
	/// </summary>
	protected virtual void UpdateGUI(){

	}


}