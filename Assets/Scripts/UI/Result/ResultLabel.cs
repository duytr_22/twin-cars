﻿//  ResultLabel.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.22.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 結果表示ラベル
/// </summary>
public class ResultLabel : MonoBehaviour
{

	//テキストのフォーマット
	[SerializeField]
	private string _textFormat = "{0}pts";

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start ()
	{
		GetComponent<UILabel> ().text = string.Format (_textFormat, CacheData.Entity.CurrentScore);
	}

}