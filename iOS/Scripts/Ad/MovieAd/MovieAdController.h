//
//  MovieAdController.h
//  Unity-iPhone
//
//  Created  on 2015/06/30.
//
//  動画広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "CommonConfig.h"

@interface MovieAdController : NSObject{


    
}

-(id)init;
-(void)show;
-(BOOL)canShow;
-(void) showInterstitialAd;

+(void)sendPreparedMessage;
+(void)sendFinishedPlayingMessage:(BOOL)isSuccess;

+(void)finishInterstial;
+(void)startInterstial;
@end
