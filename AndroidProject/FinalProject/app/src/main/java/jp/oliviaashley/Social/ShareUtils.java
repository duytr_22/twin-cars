package jp.oliviaashley.Social;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import androidx.browser.customtabs.CustomTabsIntent;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jp.oliviaashley.Social.CustomTabs.CustomTabsHelper;
import jp.oliviaashley.Twins.R;

/**
 * Sharing utilities and functions.
 *
 * @author Aaron Santin - 2014
 * @version 1.0
 */
public class ShareUtils {
    private static final String TAG = "ShareUtils";

    /**
     * Launch other activities or URLs or whatever
     *
     * @param activity The main activity
     * @param uri      URI to launch
     */
    public static void launchUrl(final Activity activity, String uri) {
        String url = activity.getResources().getString(R.string.Appinfo_url);
        if (uri.equals("appinfo")) {
            url = activity.getResources().getString(R.string.Appinfo_url);
        }

//		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//		activity.startActivity(i);

        CustomTabsIntent tabsIntent = new CustomTabsIntent.Builder().build();

        String packageName = CustomTabsHelper.getPackageNameToUse(activity);
        tabsIntent.intent.setPackage(packageName);
        tabsIntent.launchUrl(activity, Uri.parse(url));
    }


    //フォーマットのツイート文章(スコア)
    public static String getTweetMessage(final Activity activity, final int scene, final long score, final String suffix) {

        // Remove the spaces
        String strAppName = activity.getResources().getString(R.string.app_name).replaceAll(" ", "");

        String str;
        String strGame = activity.getResources().getString(R.string.share_utils_android_game);
        if (scene == 1) {
            str = activity.getResources().getString(R.string.share_utils_best_score);
        } else {
            str = activity.getResources().getString(R.string.share_utils_score);
        }

//		//floatの場合
//		float floatScore = (float)(score / 100.0);
//		String strScore = String.format("%.2f", floatScore);

        //MARK: ゲームによって書式が変わる？(ゲームごとで確認)

        return str + "\n"
                + "[" + score + suffix + "]" + "\n"
                + strGame + " [" + activity.getResources().getString(R.string.app_name) + "] " + "\n"
                + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() + "\n"
                + "#" + strAppName;
    }


    //フォーマットのツイート文章
    public static String getTweetMessage(final Activity activity, final String msg) {

        // Remove the spaces
        String strAppName = activity.getResources().getString(R.string.app_name).replaceAll(" ", "");

        String strGame = activity.getResources().getString(R.string.share_utils_android_game);

        //MARK: ゲームによって書式が変わる？(ゲームごとで確認)

        return msg + "\n"
                + strGame + " [" + activity.getResources().getString(R.string.app_name) + "] " + "\n"
                + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() + "\n"
                + "#" + strAppName;
    }


    /**
     * Sharing method to share text over other applications. This method can be
     * customized.
     *
     * @param activity The main activity
     * @param scene    Scene from the game
     * @param score    Score to share
     * @param suffix   From the game
     */
    public static void launchTwitter(final Activity activity, int scene, long score, String suffix) {

        //ツイート文章
        final String tweet = ShareUtils.getTweetMessage(activity, scene, score, suffix);

        try {
            Intent tweetIntent = new Intent(Intent.ACTION_SEND);

            tweetIntent.setAction(Intent.ACTION_SEND);
            tweetIntent.setType("text/plain");
            tweetIntent.putExtra(Intent.EXTRA_TEXT, tweet);
            activity.startActivity(tweetIntent);
        } catch (Exception e) {
            Log.d(TAG, "Twitter起動失敗 Error");
        }
    }

    /**
     * Sharing method to share text over other applications. This method can be
     * customized.
     *
     * @param activity The main activity
     * @param msg      share message
     */
    public static void launchTwitterWithMessage(final Activity activity, final String msg) {

        //ツイート文章
        //final String tweet = ShareUtils.getTweetMessage(activity, msg);

        //ツイート文章（定型文なし）

        try {
            Intent tweetIntent = new Intent(Intent.ACTION_SEND);

            tweetIntent.setAction(Intent.ACTION_SEND);
            tweetIntent.setType("text/plain");
            tweetIntent.putExtra(Intent.EXTRA_TEXT, msg);

            activity.startActivity(tweetIntent);
        } catch (Exception e) {
            Log.d(TAG, "Twitter起動失敗 Error");
        }
    }

    /**
     * Sharing method to share text over other applications. This method can be
     * customized.
     *
     * @param activity The main activity
     * @param msg      Share message from the game
     * @param filePath Image from the game
     */
    public static void launchTwitterWithImage(final Activity activity, final String msg, String filePath) {

        //ツイート文章
        //final String tweet = ShareUtils.getTweetMessage(activity, msg);

        //ツイート文章（定型文なし）

        try {
            Intent tweetIntent = new Intent(Intent.ACTION_SEND);
            tweetIntent.setAction(Intent.ACTION_SEND);
            tweetIntent.setType("text/plain");
            tweetIntent.putExtra(Intent.EXTRA_TEXT, msg);

//			// ファイルの読み込み
//			byte[] data = null;
//
//			try {
//				data = readFileToByte(activity, filePath);
//			} catch (FileNotFoundException e) {
//				Log.e("TwitterShare", "FNFE:" + e.toString());
//			} catch (IOException e) {
//				Log.e("TwitterShare", "IOE:" + e.toString());
//			}

            // ファイルの保存先パスの作成
            File savePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File saveFile = new File(savePath, "screenshot.jpeg");
            if (!savePath.exists()) {
                if(!savePath.mkdir()) {
                    Log.d(TAG, "Can't create directory to save image.");
                    return;
                }
            }

            // 元画像の取得
            Bitmap resizedPicture;
            try {
                AssetManager assetManager = activity.getAssets();
                InputStream ims = assetManager.open(filePath);
                Bitmap originalPicture = BitmapFactory.decodeStream(ims);
                int height = originalPicture.getHeight();
                int width = originalPicture.getWidth();
                Matrix matrix = new Matrix();
                matrix.setScale(0.45f, 0.45f);
                resizedPicture = Bitmap.createBitmap(originalPicture, 0, 0, width, height, matrix, true);
            } catch (FileNotFoundException e) {
                Bitmap originalPicture = BitmapFactory.decodeFile(filePath);
                int height = originalPicture.getHeight();
                int width = originalPicture.getWidth();
                Matrix matrix = new Matrix();
                matrix.setScale(0.45f, 0.45f);
                resizedPicture = Bitmap.createBitmap(originalPicture, 0, 0, width, height, matrix, true);
            }

            // ファイルの作成
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(saveFile);
                resizedPicture.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                Log.e("TwitterShare", "IOE:" + e.toString());
                return;
            }
            Uri uri = Uri.fromFile(saveFile);
            tweetIntent.putExtra(Intent.EXTRA_STREAM, uri);
            tweetIntent.setType("image/jpeg");

            activity.startActivity(tweetIntent);
        } catch (Exception e) {
            Log.d(TAG, "Twitter起動失敗 Error");
        }
    }

    private static byte[] readFileToByte(Activity activity, String path) throws IOException {
        byte[] b = new byte[1];

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            //writable path
            FileInputStream fis = new FileInputStream(path);
            while (fis.read(b) > 0) {
                baos.write(b);
            }
            baos.close();
            fis.close();
            b = baos.toByteArray();
        } catch (FileNotFoundException e) {
            //assets
            AssetManager assetManager = activity.getAssets();
            InputStream ims = assetManager.open(path);
            while (ims.read(b) > 0) {
                baos.write(b);
            }
            baos.close();
            ims.close();
            b = baos.toByteArray();
        }

        return b;
    }
}
