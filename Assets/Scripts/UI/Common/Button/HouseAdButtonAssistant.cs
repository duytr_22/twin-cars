﻿//  HouseAdButtonAssistan.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 自社広告ボタンのアシスタントクラス
/// </summary>
public class HouseAdButtonAssistant : ButtonAssistant
{

	//Newパッチを表示する期間
	private const int INTERVAL_MOREAPPS_LASTVIEW = 10;

	//newバッチ
	[SerializeField]
	private GameObject _newBadge;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start ()
	{
		//newバッチを表示するか判定
		DateTime now = DateTime.Now;
		DateTime LastViewDateMoreApps = UserData.TimeOfPushedHouseAdButton;

		_newBadge.SetActive (now.Subtract (LastViewDateMoreApps).Days >= INTERVAL_MOREAPPS_LASTVIEW);
	}

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		//newバッチを消して自社広告Web表示
		_newBadge.SetActive (false);
		UserData.TimeOfPushedHouseAdButton = DateTime.Now;
		CommonNativeManager.Instance.ShowCommonWeb ();
	}

}
