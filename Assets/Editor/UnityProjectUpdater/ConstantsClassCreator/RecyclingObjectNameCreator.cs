﻿//  RecyclingObjectNameCreator.cs
//  ProductName MyKingdom
//
//  Created by kan.kikuchi on 2016.04.13.

using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 再利用オブジェクトの名前をまとめるクラスを作るクラス
/// </summary>
public class RecyclingObjectNameCreator : AssetPostprocessorEx{

	//=================================================================================
	//検知
	//=================================================================================

	//再利用オブジェクトが編集または追加されたら自動でAUDIO.csを作成する
	private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths){
		//最初の起動時に実行しないように
		if(!UnityProjectSetting.IsLaunchedProject){
			return;
		}

		List<string[]> assetsList = new List<string[]> (){
			importedAssets, deletedAssets, movedAssets, movedFromAssetPaths
		};

		List<string> targetDirectoryNameList = new List<string> (){
			DirectoryPath.TOP_RESOURCES + ResourcesDirectoryPath.PREFAB_RECYCLING_OBJECT
		};

		if(ExistsDirectoryInAssets(assetsList, targetDirectoryNameList)){
			Create ();
		}

	}

	//=================================================================================
	//作成
	//=================================================================================

	[MenuItem("Tools/Create/Recycling Object Name")]
	public static void Create(){

		//keyをまとめるdic
		Dictionary<string, string> keyDic = new Dictionary<string, string> ();

		//全プレハブを読み込み
		List<GameObject> prefabList = new List<GameObject>(Resources.LoadAll<GameObject>(ResourcesDirectoryPath.PREFAB_RECYCLING_OBJECT));

		for (int i = 0; i < prefabList.Count; i++) {
			keyDic[prefabList[i].name] = prefabList[i].name;
		}

		//定数クラス書き出し
		ConstantsClassCreator.Create ("RecyclingObjectName", "再利用オブジェクトの名前を定数で管理するクラス", keyDic);
	}

}