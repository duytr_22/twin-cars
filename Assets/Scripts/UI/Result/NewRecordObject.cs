﻿//  NewRecordBatch.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.22.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 記録を更新した時に表示するオブジェクト
/// </summary>
public class NewRecordObject : MonoBehaviour {

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start(){
		gameObject.SetActive (CacheData.Entity.IsNewRecored);
	}

}