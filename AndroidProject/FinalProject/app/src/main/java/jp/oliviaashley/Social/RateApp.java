package jp.oliviaashley.Social;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.EventListener;

import jp.oliviaashley.Twins.R;


public class RateApp {
    private static final String TAG = "RateApp";


    //レビュー依頼結果通知用リスナー
    public interface RateAppListener extends EventListener {
        //処理結果 [true]レビューを書くボタンを押した [false]それ以外
        void onFinish(boolean result);
    }


    //リスナー(ダイアログのボタンを押したときに通知)
    private static RateAppListener listener = null;

    private static void setRateAppListener(RateAppListener listener) {
        RateApp.listener = listener;
    }

    public static RateAppListener getRateAppListener() {
        return RateApp.listener;
    }


    //設定ファイルの確認 レビュー依頼用のダイアログを表示してもいいかどうか
    public static boolean isEnableShowRateDialogAgain(final Activity activity) {
        boolean result = false;

        final SharedPreferences sharedPreferences = activity.getSharedPreferences("RateApp", Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            result = (sharedPreferences.getBoolean("showRateDialogAgain", true));
        }

        return result;
    }


    //ダイアログの文言全部デフォルト (従来呼び出しを考慮)
    public static void showRateDialog(final Activity activity) {
        RateApp.showRateDialog(
                activity,
                "このアプリを評価する",
                "もしよかったら\nレビューを書いてもらえますか？",
                true);
    }

    //ダイアログの文言...ボタン全部デフォルト
    public static void showRateDialog(
            final Activity activity,
            final String title,
            final String text,
            final boolean isCheckCounter) {
        RateApp.showRateDialog(
                activity,
                "このアプリを評価する",
                "もしよかったら\nレビューを書いてもらえますか？",
                "レビューを書く" + activity.getResources().getString(R.string.app_name),
                "キャンセル",
                "二度と表示しない",
                isCheckCounter);
    }

    //ダイアログの文言全部指定(リスナー無し)
    public static void showRateDialog(
            final Activity activity,
            final String title,
            final String text,
            final String okButton,
            final String cancelButton,
            final String ngButton,
            final boolean isCheckCounter) {
        RateApp.showRateDialog(
                activity,
                title,
                text,
                okButton,
                cancelButton,
                ngButton,
                isCheckCounter,
                null);
    }

    //ダイアログの文言全部指定(リスナー有り)
    public static void showRateDialog(
            final Activity activity,
            final String title,
            final String text,
            final String okButton,
            final String cancelButton,
            final String ngButton,
            final boolean isCheckCounter,
            final RateAppListener listener) {
        RateApp.setRateAppListener(null);

        final SharedPreferences sharedPreferences = activity.getSharedPreferences("RateApp", Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean("showRateDialogAgain", true)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (editor != null) {
                editor.putInt("counter", sharedPreferences.getInt("counter", 0) + 1);
                editor.apply();
            }
            if ((isCheckCounter) && (sharedPreferences.getInt("counter", 0) % 5 != 0)) {
                return;
            }

            // Build the dialog in the UiThread
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RateApp.setRateAppListener(listener);

                    final Dialog dialog = new Dialog(activity);
                    dialog.setTitle(title);

                    LinearLayout ll = new LinearLayout(activity);
                    ll.setOrientation(LinearLayout.VERTICAL);
                    ll.setPadding(10, 10, 10, 10);

                    TextView tv = new TextView(activity);
                    tv.setText(text);
                    //tv.setWidth(240);
                    tv.setMinimumWidth(250);
                    tv.setPadding(10, 0, 10, 10);
                    ll.addView(tv);

                    Button b1 = new Button(activity);
                    b1.setText(okButton);
                    b1.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            //レビューを開く
                            rate(activity);

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            if (editor != null) {
                                editor.putBoolean("showRateDialogAgain", false);
                                editor.apply();
                            }

                            dialog.dismiss();

                            //レビュー依頼結果通知用
                            RateApp.RateAppListener listener = RateApp.getRateAppListener();
                            if (listener != null) {
                                listener.onFinish(true);
                            }
                            RateApp.setRateAppListener(null);
                        }
                    });
                    ll.addView(b1);

                    Button b2 = new Button(activity);
                    b2.setText(cancelButton);
                    b2.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();

                            //レビュー依頼結果通知用
                            RateApp.RateAppListener listener = RateApp.getRateAppListener();
                            if (listener != null) {
                                listener.onFinish(false);
                            }
                            RateApp.setRateAppListener(null);
                        }
                    });
                    ll.addView(b2);

                    Button b3 = new Button(activity);
                    b3.setText(ngButton);
                    b3.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            if (editor != null) {
                                editor.putBoolean("showRateDialogAgain", false);
                                editor.apply();
                            }
                            dialog.dismiss();

                            //レビュー依頼結果通知用
                            RateApp.RateAppListener listener = RateApp.getRateAppListener();
                            if (listener != null) {
                                listener.onFinish(true);
                            }
                            RateApp.setRateAppListener(null);
                        }
                    });
                    ll.addView(b3);

                    dialog.setContentView(ll);

                    //キャンセルなしで
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);

                    dialog.show();
                }
            });
        } // else don't show the dialog
    }

    public static void rate(final Activity activity) {
        Log.v(TAG, "Opening rating pop up");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        {
            // Try Google play
            String appName = activity.getPackageName();
            Uri uri = Uri.parse("market://details?id=" + appName);

            intent.setData(uri);
        }

        if (!MyStartActivity(activity, intent)) {
            Toast.makeText(
                    activity,
                    "Could not open Android market, please install the market app.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean MyStartActivity(Activity activity, Intent aIntent) {
        try {
            activity.startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}
