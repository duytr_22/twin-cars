﻿//  UIScaleAdjuster.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.22.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// UIのスケールを調整するクラス
/// </summary>
public class UIScaleAdjuster : UIAdjuster {

	private RectTransform _rectTransform;

	//ベースにするアス比
	[SerializeField]
	private AspectRateType _baseAspectRateType = AspectRateType.iPhone4;

	//基準位置とアス比に合わせて移動する距離
	[SerializeField]
	private float _scalingValue, _iPadScale = 1;

	//=================================================================================
	//更新
	//=================================================================================

	protected override void AdjustUI(){
		base.AdjustUI();

		if(_rectTransform == null){
			_rectTransform = GetComponent<RectTransform> ();
		}

		//アス比に合わせて下部ボタンを移動する
		Vector2 baseAspectRate = SCREEN_SIZE_640_960;
		if(_baseAspectRateType == AspectRateType.iPhone5){
			baseAspectRate = SCREEN_SIZE_640_1136;
		}

		float aspectRate = 1.0f / GetScreenSizeRate (baseAspectRate, false) - 1.0f;
		aspectRate = Mathf.Max (aspectRate, 0);

		float scale = 1f + _scalingValue * aspectRate;

		//iPadの場合は基準位置と移動距離を変える
		if(IsIpad()){
			scale = _iPadScale;
		}

		_rectTransform.localScale = new Vector3(scale, scale, scale);
	}

}