//
//  ImobileRectangleHelper.h
//
//  Created on 14/10/2014.
//
//

#import "FiveRectangleHelper.h"

@implementation FiveRectangleHelper
//初期化

+(void) startFive{
    FADConfig *config = [[FADConfig alloc] initWithAppId:FIVE_APP_ID];
    config.fiveAdFormat =
    [NSSet setWithObjects:
     [NSNumber numberWithInt:kFADFormatW320H180],
     nil];
#ifdef DEBUG_FIVE_TEST_MODE
    config.isTest = YES;
#endif
    [FADSettings registerConfig:config];
}

-(UIView *)prepareRectangleAd:(CGRect)frame
{

    if (isPopup) {
        
        _frameBottom = frame;
        _fiveViewBottom = [[FADAdViewW320H180 alloc] initWithFrame:frame slotId:FIVE_SPOTID_RECTANGLE_POPUP];
        _fiveViewBottom.backgroundColor = [UIColor clearColor];
        _fiveViewBottom.delegate = self;
        [_fiveViewBottom loadAd];
        
        _count2 = 0;
        _time2 = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(callFiveViewBottom) userInfo:nil repeats:YES];
        return nil;
        
    } else{
        
        _frameTop = frame;
        _fiveViewTop = [[FADAdViewW320H180 alloc] initWithFrame:frame slotId:FIVE_SPOTID_RECTANGLE_GAME];
        _fiveViewTop.backgroundColor = [UIColor clearColor];
        _fiveViewTop.delegate = self;
        [_fiveViewTop loadAd];
        
        _count1 = 0;
        _time1 = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(callFiveViewTop) userInfo:nil repeats:YES];
        return nil;
    }
}

-(void)callFiveViewTop{
    
    _count1++;
    
    if (_count1 > 5) {
        
        [_time1 invalidate];
        _time1 = nil;
    }
    _fiveViewTop = [[FADAdViewW320H180 alloc] initWithFrame:_frameTop slotId:FIVE_SPOTID_RECTANGLE_GAME];
    _fiveViewTop.backgroundColor = [UIColor clearColor];
    _fiveViewTop.delegate = self;
    [_fiveViewTop loadAd];
}

-(void)callFiveViewBottom{
    
    _count2++;
    if (_count2 > 5) {
        [_time2 invalidate];
        _time2 = nil;
    }
    _fiveViewBottom = [[FADAdViewW320H180 alloc] initWithFrame:_frameBottom slotId:FIVE_SPOTID_RECTANGLE_POPUP];
    _fiveViewBottom.backgroundColor = [UIColor clearColor];
    _fiveViewBottom.delegate = self;
    [_fiveViewBottom loadAd];
}
#pragma mark - Delagate

- (void)fiveAdDidLoad:(id<FADAdInterface>)ad{
    NSLog(@"Five:Ready for Rectangle Ad");
    
    if ([ad isEqual:_fiveViewTop]) {
        
        [_rectangleAdView addSubview:_fiveViewTop];
        
        [_time1 invalidate];
        _time1 = nil;
        
    }else {
        [_rectangleAdView addSubview:_fiveViewBottom];

        [_time2 invalidate];
        _time2 = nil;
    }
}
- (void)fiveAd:(id<FADAdInterface>)ad didFailedToReceiveAdWithError:(FADErrorCode) errorCode{
    NSLog(@"Five:Failed Load Rectangle Ad Value:%ld",(long)errorCode);
}

- (void)fiveAdDidClick:(id<FADAdInterface>)ad{}
- (void)fiveAdDidClose:(id<FADAdInterface>)ad{}
- (void)fiveAdDidStart:(id<FADAdInterface>)ad{}
- (void)fiveAdDidPause:(id<FADAdInterface>)ad{}
- (void)fiveAdDidResume:(id<FADAdInterface>)ad{}
- (void)fiveAdDidViewThrough:(id<FADAdInterface>)ad{
    
    double delayInSeconds = 5.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        if ([ad isEqual:_fiveViewTop]) {
            
            [self callFiveViewTop];
        }else {
            
            [self callFiveViewBottom];
        }
    });
}
- (void)fiveAdDidReplay:(id<FADAdInterface>)ad{}

@end
