//
//  ISMaioAdapter.h
//  ISMaioAdapter
//
//  Created by Dor Alon on 16/10/2017.
//  Copyright © 2017 IronSource. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IronSource/ISBaseAdapter+Internal.h"

//System Frameworks For Maio Adapter
@import AdSupport;
@import StoreKit;
@import MobileCoreServices;
@import SystemConfiguration;
@import AVFoundation;
@import UIKit;
@import Foundation;


@interface ISMaioAdapter : ISBaseAdapter

@end
