﻿//  TitleHelp.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// タイトルのヘルプ
/// </summary>
public class TitleHelp : MonoBehaviour, IPointerDownHandler {

	[SerializeField]
	private GameObject _main;

	//=================================================================================
	//タッチイベント
	//=================================================================================

	public void OnPointerDown (PointerEventData eventData){
		_main.SetActive (true);
		gameObject.SetActive (false);
		AdManager.Instance.UpdateAdWithScene(SceneName.TITLE, false);
	}

}
