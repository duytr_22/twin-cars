﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEnding : MonoBehaviour
{
    [SerializeField] UILabel _resultText;

    void OnEnable()
    {
        _resultText.text = GamePlayScript.Instance.totalPlayTime.ToString("0");
    }

    public void OnRetryButtonClick()
    {
        SceneNavigator.Instance.MoveScene(SceneName.TITLE);
    }
}
