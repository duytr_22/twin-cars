//
//  UserInfoObject.h
//
//  Created on 1/25/13.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "CommonConfig.h"

@interface UserInfoObject : NSObject <NSCoding>

+ (UserInfoObject *) userInfo;

@property (nonatomic, readonly) NSString *fmsHost;

//ポップアップ広告
- (void) confirmWhichNewAppAvailableWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler;

//表示する広告会社の確認
- (void) confirmWhichAdSplashWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler;

//アニメーションバナー広告
- (void) confirmWhichGenuineAdAvailableWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler;

// Reachability
@property (nonatomic, retain, readonly) Reachability *fmsReachability;

@end
