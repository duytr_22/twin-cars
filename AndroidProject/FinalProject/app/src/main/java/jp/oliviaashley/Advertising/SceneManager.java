package jp.oliviaashley.Advertising;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import jp.oliviaashley.Advertising.Providers.AdGenerationHelper;
import jp.oliviaashley.Advertising.Providers.AdmobHelper;
import jp.oliviaashley.Advertising.Providers.IronSourceHelper;

import jp.oliviaashley.Twins.R;

public class SceneManager {

    public static int currentScene;

    // SCENES
    enum GameSceneCode {
        SCENE_TITLE(0),
        SCENE_SELECT(1),
        SCENE_LOAD(4),
        SCENE_INGAME(3),
        SCENE_ENDING(2),
        SCENE_HELP(1000),
        SCENE_PAUSE(10000),
        SCENE_ENDING_NEW(100000),
        CLEAR(999999);

        private final int value;

        GameSceneCode(int value) {
            this.value = value;
        }

        int getValue() {
            return value;
        }

        boolean isEquals(int num) {
            return value == num;
        }
    }

    private static final String TAG = "SceneManager";

    // TO CONFIGURE THE REAL SCENARIO
    private static float WINDOW_WIDTH;
    private static float WINDOW_HEIGHT;
    private static RelativeLayout fullLayout = null;

    // Define your boxes
  	private static RelativeLayout bannerBottom = null;			// Banner Ad
    private static RelativeLayout bannerHouseAds = null;

  	private static RelativeLayout rectangleBottom = null;		// Ending
    private static RelativeLayout rectangleVideo= null;		// Pause
  	private static RelativeLayout rectangleCenter = null;		// Pause
    private static RelativeLayout rectanglePopup = null;		// Pause

    public static void doOnWindowFocusChanged(Activity activity, boolean hasFocus) {

    }

    public static void doOnCreate(Activity activity, FrameLayout mainLayout) {
        Log.v(TAG, "onCreate");
        fullLayout = new RelativeLayout(activity);
        fullLayout.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        fullLayout.setContentDescription("advertisingLayout");
        mainLayout.addView(fullLayout);
    }

    public static void setWindowSize(final Activity activity, float width,
                                     float height) {
        Log.v(TAG, "setWindowSize " + width + "x" + height);
        WINDOW_WIDTH = width;
        WINDOW_HEIGHT = height;

        activity.runOnUiThread(new Runnable() {
            // FIXME configure here your advertisement
            @Override
            public void run() {

                DisplayMetrics metrics = activity.getResources().getDisplayMetrics();

                int rectangleCenter_y = 80;

                // Banner bottom row BANNER
                bannerBottom = BannerManager.getView(activity);
                fullLayout.addView(bannerBottom);

//                bannerHouseAds = BannerManager.getHouseAdsView(activity);
//                fullLayout.addView(bannerHouseAds);

                // RectangleAd ENDING NEW
                rectangleBottom = getDynamicRectangleEndAd(activity, 10);
                fullLayout.addView(rectangleBottom);

                // RectangleAd PAUSE
                rectangleCenter = getDynamicRectangleAd(activity, rectangleCenter_y);
                fullLayout.addView(rectangleCenter);

                rectangleVideo = getDynamicVideoRectangleAd(activity, 250);
                fullLayout.addView(rectangleVideo);

                rectanglePopup = getDynamicPopupRectangleAd(activity, 150);
                fullLayout.addView(rectanglePopup);
            }
        });

        // Set all the views GONE
        setScene(activity, GameSceneCode.CLEAR.getValue());
    }

    public static void showVideoRect(final Activity activity) {

        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        boolean removeAds = sharedPref.getBoolean(activity.getResources().getString(R.string.remove_ads), false);

        if(!removeAds) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    rectangleVideo.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public static void hideVideoRect(final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                rectangleVideo.setVisibility(View.GONE);
            }
        });
    }

    public static void showPopupRect(final Activity activity) {

        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        boolean removeAds = sharedPref.getBoolean(activity.getResources().getString(R.string.remove_ads), false);

        if(!removeAds) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    rectanglePopup.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public static void hidePopupRect(final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                rectanglePopup.setVisibility(View.GONE);
            }
        });
    }

    public static void setScene(final Activity activity, final int scene) {
        Log.v(TAG, "setScene:" + scene);

        currentScene = scene;

        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        boolean removeAds = sharedPref.getBoolean(activity.getResources().getString(R.string.remove_ads), false);

        if(!removeAds) {

            if (GameSceneCode.SCENE_TITLE.isEquals(scene)) {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        IronSourceHelper.loadBanner();

                        bannerBottom.setVisibility(View.VISIBLE);
                        rectangleBottom.setVisibility(View.GONE);
                        rectangleCenter.setVisibility(View.GONE);
                        rectangleVideo.setVisibility(View.GONE);
                        rectanglePopup.setVisibility(View.GONE);
//                        bannerHouseAds.setVisibility(View.GONE);
                    }
                });
                return;
            }

            if (GameSceneCode.SCENE_ENDING.isEquals(scene)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        bannerBottom.setVisibility(View.GONE);
                        rectangleBottom.setVisibility(View.VISIBLE);
                        rectangleCenter.setVisibility(View.GONE);
                        rectangleVideo.setVisibility(View.GONE);
                        rectanglePopup.setVisibility(View.GONE);
//                        bannerHouseAds.setVisibility(View.GONE);
                    }
                });
                return;
            }

            if (GameSceneCode.SCENE_INGAME.isEquals(scene)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        bannerBottom.setVisibility(View.VISIBLE);
                        rectangleBottom.setVisibility(View.GONE);
                        rectangleCenter.setVisibility(View.GONE);
                        rectangleVideo.setVisibility(View.GONE);
                        rectanglePopup.setVisibility(View.GONE);
//                        bannerHouseAds.setVisibility(View.GONE);
                    }
                });
                return;
            }

            if (GameSceneCode.SCENE_HELP.isEquals(scene)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        bannerBottom.setVisibility(View.VISIBLE);
                        rectangleBottom.setVisibility(View.GONE);
                        rectangleCenter.setVisibility(View.GONE);
                        rectangleVideo.setVisibility(View.GONE);
                        rectanglePopup.setVisibility(View.GONE);
//                        bannerHouseAds.setVisibility(View.GONE);
                    }
                });
                return;
            }

            if (GameSceneCode.SCENE_PAUSE.isEquals(scene)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        bannerBottom.setVisibility(View.VISIBLE);
                        rectangleBottom.setVisibility(View.GONE);
                        rectangleCenter.setVisibility(View.VISIBLE);
                        rectangleVideo.setVisibility(View.GONE);
                        rectanglePopup.setVisibility(View.GONE);
//                        bannerHouseAds.setVisibility(View.GONE);

                    }
                });
                return;
            }

            if (GameSceneCode.SCENE_SELECT.isEquals(scene)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        bannerBottom.setVisibility(View.VISIBLE);
                        rectangleBottom.setVisibility(View.GONE);
                        rectangleCenter.setVisibility(View.GONE);
                        rectangleVideo.setVisibility(View.GONE);
                        rectanglePopup.setVisibility(View.GONE);
//                        bannerHouseAds.setVisibility(View.GONE);

                    }
                });
                return;
            }

            if (GameSceneCode.SCENE_LOAD.isEquals(scene)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        bannerBottom.setVisibility(View.VISIBLE);
                        rectangleBottom.setVisibility(View.GONE);
                        rectangleCenter.setVisibility(View.GONE);
                        rectangleVideo.setVisibility(View.GONE);
                        rectanglePopup.setVisibility(View.GONE);
//                        bannerHouseAds.setVisibility(View.GONE);

                    }
                });
                return;
            }

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    bannerBottom.setVisibility(View.VISIBLE);
                    rectangleBottom.setVisibility(View.GONE);
                    rectangleCenter.setVisibility(View.GONE);
                    rectangleVideo.setVisibility(View.GONE);
                    rectanglePopup.setVisibility(View.GONE);
//                    bannerHouseAds.setVisibility(View.GONE);

                }
            });
        } else {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    bannerBottom.setVisibility(View.GONE);
                    rectangleBottom.setVisibility(View.GONE);
                    rectangleCenter.setVisibility(View.GONE);
                    rectangleVideo.setVisibility(View.GONE);
                    rectanglePopup.setVisibility(View.GONE);
//                    bannerHouseAds.setVisibility(View.GONE);

                }
            });
        }
    }

    public static void HideAllAds(final Activity activity)
    {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                bannerBottom.setVisibility(View.GONE);
                rectangleBottom.setVisibility(View.GONE);
                rectangleCenter.setVisibility(View.GONE);
//                bannerHouseAds.setVisibility(View.GONE);
                rectanglePopup.setVisibility(View.GONE);
                rectangleVideo.setVisibility(View.GONE);
            }
        });
    }

    /**
     * インタースティシャルを閉じた時用の暫定処置
     * 一時停止からタイトルに戻った時に、タイトルにネイティブバナーがあると、インタースティシャルが被さった時に停止する
     * 最良の対応がわからなかったので、現在できる回避策を講じました。
     *
     * @param activity
     */
    public static void setNativeBannerVisible(Activity activity) {
        //前提条件(を満たしていなければ処理しない)
        if (activity == null) {
            return;
        }
    }

    // Helper method to fix a position to a view
    // First create your adView, then pass it a as a parameter to set a position
    // in this method
    // You need to put this layout in your main frame layout
    // This parameters are take from the default design constants
    protected static RelativeLayout createRectLayoutByMargins(
            Activity activity, final View v, int left, int top, int right,
            int bottom) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((android.view.WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                .getMetrics(metrics);
        // Rect parameters
        if ((top + bottom > WINDOW_HEIGHT) || (left + right > WINDOW_WIDTH)) {
            Log.w(TAG,
                    "Your layout is too small, probably will show unexpectedly");
        }
        int marginLeft = (int) (left / WINDOW_WIDTH * metrics.widthPixels);
        int marginTop = (int) (top / WINDOW_HEIGHT * metrics.heightPixels);
        int marginRight = (int) (right / WINDOW_WIDTH * metrics.widthPixels);
        int marginBottom = (int) (bottom / WINDOW_HEIGHT * metrics.heightPixels);
        int sizeW = metrics.widthPixels - marginLeft - marginRight;
        int sizeH = metrics.heightPixels - marginTop - marginBottom;
        // Result layout at fullscreen
        RelativeLayout mainLayout = new RelativeLayout(activity);
        mainLayout.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        // Rect layout where you insert an adView
        RelativeLayout subLayout = new RelativeLayout(activity);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                sizeW, sizeH);
        params.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        mainLayout.addView(subLayout, params);
        // Insert the adView
        subLayout.addView(v);
        return mainLayout;
    }

    // Same than before but passing the position and the size, try to no out of
    // bounds
    protected static RelativeLayout createRectLayoutBySize(Activity activity,
                                                           final View v, int x, int y, int width, int height) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((android.view.WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                .getMetrics(metrics);
        // Rect parameters
        if ((x + width > WINDOW_WIDTH) || (y + height > WINDOW_HEIGHT)) {
            Log.w(TAG,
                    "Your layout is too big, out of bounds, probably will show unexpectedly");
        }
        int marginLeft = (int) (x / WINDOW_WIDTH * metrics.widthPixels);
        int marginTop = (int) (y / WINDOW_HEIGHT * metrics.heightPixels);
        int marginRight = (int) ((WINDOW_WIDTH - width - x) / WINDOW_WIDTH * metrics.widthPixels);
        int marginBottom = (int) ((WINDOW_HEIGHT - height - y) / WINDOW_HEIGHT * metrics.heightPixels);
        int sizeW = metrics.widthPixels - marginLeft - marginRight;
        int sizeH = metrics.heightPixels - marginTop - marginBottom;
        // Result layout at fullscreen
        RelativeLayout mainLayout = new RelativeLayout(activity);
        mainLayout.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        // Rect layout where you insert an adView
        RelativeLayout subLayout = new RelativeLayout(activity);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                sizeW, sizeH);
        params.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        mainLayout.addView(subLayout, params);
        // Insert the adView
        subLayout.addView(v);
        return mainLayout;
    }

    private static String colorToHex(int color) {
        return String.format("#%06X", 0xFFFFFF & color);
    }

    protected static RelativeLayout getDynamicPopupRectangleAd(Activity activity, int y) {

        DisplayMetrics metrics = new DisplayMetrics();
        ((android.view.WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                .getMetrics(metrics);
        int marginTop = (int) (y / WINDOW_HEIGHT * metrics.heightPixels);

        RelativeLayout mainLayout = new RelativeLayout(activity);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.topMargin = marginTop;
        mainLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        mainLayout.setLayoutParams(params);

        if(isJapaneseLanguage(activity)) {

//            mainLayout.addView(FiveHelper.getAdRectangleEndView(activity));

        } else {

//            mainLayout.addView(AdmobHelper.getAdRectanglePopupView(activity));
        }

        return mainLayout;
    }

    protected static RelativeLayout getDynamicVideoRectangleAd(Activity activity, int y) {

        DisplayMetrics metrics = new DisplayMetrics();
        ((android.view.WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                .getMetrics(metrics);
        int marginTop = (int) (y / WINDOW_HEIGHT * metrics.heightPixels);

        RelativeLayout mainLayout = new RelativeLayout(activity);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.topMargin = marginTop;
        mainLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        mainLayout.setLayoutParams(params);

        if(isJapaneseLanguage(activity)) {

            mainLayout.addView(AdGenerationHelper.getAdRectangleView(activity));

        } else {

            mainLayout.addView(AdmobHelper.getAdRectangleView(activity));
        }

        return mainLayout;
    }

    // Check online for a type to return RectangleAd
    protected static RelativeLayout getDynamicRectangleAd(Activity activity, int y) {

        CheckSplSpfJson.doOnCreate(activity);

        String type = CheckSplSpfJson.getRectangleType();

        DisplayMetrics metrics = new DisplayMetrics();
        ((android.view.WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                .getMetrics(metrics);
        int marginTop = (int) (y / WINDOW_HEIGHT * metrics.heightPixels);

        RelativeLayout mainLayout = new RelativeLayout(activity);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.topMargin = marginTop;
        mainLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        mainLayout.setLayoutParams(params);

        if(isJapaneseLanguage(activity)) {

            mainLayout.addView(AdGenerationHelper.getAdRectangleView(activity));

        } else {

            mainLayout.addView(AdmobHelper.getAdRectangleView(activity));
        }

        return mainLayout;
    }

    // Check online for a type to return RectangleEndAd
    protected static RelativeLayout getDynamicRectangleEndAd(Activity activity, int y) {
        CheckSplSpfJson.doOnCreate(activity);
        String type = CheckSplSpfJson.getRectangleEndType();

        DisplayMetrics metrics = new DisplayMetrics();
        ((android.view.WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                .getMetrics(metrics);
        int marginBottom = (int) (y / WINDOW_HEIGHT * metrics.heightPixels);

        RelativeLayout mainLayout = new RelativeLayout(activity);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.bottomMargin = marginBottom;
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        mainLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        mainLayout.setLayoutParams(params);

        if(isJapaneseLanguage(activity)) {

            mainLayout.addView(AdGenerationHelper.getAdRectangleView(activity));

        } else {

            mainLayout.addView(AdmobHelper.getAdRectangleEndView(activity));
        }

        return mainLayout;
    }

    private static boolean isOnline(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null) {
            int type = info.getType();
            if (type == ConnectivityManager.TYPE_MOBILE
                    || type == ConnectivityManager.TYPE_WIFI) {
                Log.d("myTag", "NetworkInfo:" + info.getTypeName());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isJapaneseLanguage(Activity activity) {
        String lang = activity.getResources().getConfiguration().locale.getLanguage();
        if (lang.equals("ja")) {
            Log.d(TAG, "日本語");
            return true;
        }
        return false;
    }
}
