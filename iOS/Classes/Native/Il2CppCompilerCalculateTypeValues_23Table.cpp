﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t339727138;
// UnityEngine.Purchasing.FakeStore
struct FakeStore_t3710170489;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Action`2<System.Boolean,System.Int32>
struct Action_2_t2394327294;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.String>
struct Func_2_t1011448493;
// System.Action
struct Action_t1264377477;
// UnityEngine.Purchasing.AsyncWebUtil
struct AsyncWebUtil_t2717304869;
// UnityEngine.WWW
struct WWW_t3688466362;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t965808653;
// UnityEngine.ILogger
struct ILogger_t2607134790;
// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>
struct Action_1_t3372111503;
// UnityEngine.Purchasing.CloudCatalogImpl
struct CloudCatalogImpl_t1580312503;
// System.Func`3<System.Char,System.Int32,System.String>
struct Func_3_t1402304843;
// System.Func`3<System.String,System.String,System.String>
struct Func_3_t2383982614;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.Object
struct Object_t631007953;
// System.Type
struct Type_t;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// UnityEngine.Purchasing.IProductCatalogImpl
struct IProductCatalogImpl_t4135604393;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogItem>
struct List_1_t3613492376;
// UnityEngine.Purchasing.JSONStore
struct JSONStore_t1587640366;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t764570979;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// UnityEngine.Purchasing.Extension.IStore
struct IStore_t2324734081;
// UnityEngine.Purchasing.StandardPurchasingModule
struct StandardPurchasingModule_t2580735509;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Purchasing.Extension.UnityUtil
struct UnityUtil_t103543446;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription>
struct List_1_t2186087874;
// UnityEngine.Purchasing.Extension.IPurchasingBinder
struct IPurchasingBinder_t3569785493;
// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t620144240;
// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>
struct Action_3_t3666713281;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t4045980644;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1290832230;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>
struct Dictionary_2_t1588782543;
// UnityEngine.Purchasing.UnityChannelBindings
struct UnityChannelBindings_t3184348469;
// UnityEngine.Purchasing.UnityChannelImpl
struct UnityChannelImpl_t3062682360;
// UnityEngine.Purchasing.Extension.ProductDescription
struct ProductDescription_t714013132;
// System.Text.RegularExpressions.MatchEvaluator
struct MatchEvaluator_t632122704;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.Boolean>
struct Func_2_t3556253065;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription>
struct Func_2_t1244879711;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Purchasing.Default.IWindowsIAP
struct IWindowsIAP_t4053602182;
// Uniject.IUtil
struct IUtil_t1069285358;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t2607409403;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t3225273728;
// System.Action`1<UnityEngine.Purchasing.Product>
struct Action_1_t3416877654;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// UnityEngine.Purchasing.INativeAppleStore
struct INativeAppleStore_t2995889902;
// UnityEngine.Purchasing.INativeUnityChannelStore
struct INativeUnityChannelStore_t3197501291;
// UnityEngine.Purchasing.INativeFacebookStore
struct INativeFacebookStore_t2453197597;
// UnityEngine.Purchasing.INativeTizenStore
struct INativeTizenStore_t2727987947;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.StoreID>
struct List_1_t2166831371;
// UnityEngine.Purchasing.LocalizedProductDescription
struct LocalizedProductDescription_t1808411718;
// UnityEngine.Purchasing.Price
struct Price_t1857690312;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.LocalizedProductDescription>
struct List_1_t3280486460;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogPayout>
struct List_1_t2396426280;
// UnityEngine.Purchasing.INativeStoreProvider
struct INativeStoreProvider_t2882146526;
// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance
struct StoreInstance_t2416643455;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String>
struct Dictionary_2_t2070795836;
// UnityEngine.Purchasing.WinRTStore
struct WinRTStore_t2015085940;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>
struct Action_1_t1412811613;
// UnityEngine.Purchasing.UIFakeStore/DialogRequest
struct DialogRequest_t599015159;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<UnityEngine.RuntimePlatform>
struct List_1_t1336965349;
// System.Collections.Generic.List`1<System.Action`1<System.Boolean>>
struct List_1_t1741830302;




#ifndef U3CMODULEU3E_T692745562_H
#define U3CMODULEU3E_T692745562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745562 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745562_H
#ifndef U3CMODULEU3E_T692745561_H
#define U3CMODULEU3E_T692745561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745561 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745561_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T771099449_H
#define U3CU3EC__DISPLAYCLASS15_0_T771099449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t771099449  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.FakeStore/<>c__DisplayClass15_0::product
	ProductDefinition_t339727138 * ___product_0;
	// UnityEngine.Purchasing.FakeStore UnityEngine.Purchasing.FakeStore/<>c__DisplayClass15_0::<>4__this
	FakeStore_t3710170489 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_product_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t771099449, ___product_0)); }
	inline ProductDefinition_t339727138 * get_product_0() const { return ___product_0; }
	inline ProductDefinition_t339727138 ** get_address_of_product_0() { return &___product_0; }
	inline void set_product_0(ProductDefinition_t339727138 * value)
	{
		___product_0 = value;
		Il2CppCodeGenWriteBarrier((&___product_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t771099449, ___U3CU3E4__this_1)); }
	inline FakeStore_t3710170489 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FakeStore_t3710170489 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FakeStore_t3710170489 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T771099449_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef DIALOGREQUEST_T599015159_H
#define DIALOGREQUEST_T599015159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore/DialogRequest
struct  DialogRequest_t599015159  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UIFakeStore/DialogRequest::QueryText
	String_t* ___QueryText_0;
	// System.String UnityEngine.Purchasing.UIFakeStore/DialogRequest::OkayButtonText
	String_t* ___OkayButtonText_1;
	// System.String UnityEngine.Purchasing.UIFakeStore/DialogRequest::CancelButtonText
	String_t* ___CancelButtonText_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Purchasing.UIFakeStore/DialogRequest::Options
	List_1_t3319525431 * ___Options_3;
	// System.Action`2<System.Boolean,System.Int32> UnityEngine.Purchasing.UIFakeStore/DialogRequest::Callback
	Action_2_t2394327294 * ___Callback_4;

public:
	inline static int32_t get_offset_of_QueryText_0() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___QueryText_0)); }
	inline String_t* get_QueryText_0() const { return ___QueryText_0; }
	inline String_t** get_address_of_QueryText_0() { return &___QueryText_0; }
	inline void set_QueryText_0(String_t* value)
	{
		___QueryText_0 = value;
		Il2CppCodeGenWriteBarrier((&___QueryText_0), value);
	}

	inline static int32_t get_offset_of_OkayButtonText_1() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___OkayButtonText_1)); }
	inline String_t* get_OkayButtonText_1() const { return ___OkayButtonText_1; }
	inline String_t** get_address_of_OkayButtonText_1() { return &___OkayButtonText_1; }
	inline void set_OkayButtonText_1(String_t* value)
	{
		___OkayButtonText_1 = value;
		Il2CppCodeGenWriteBarrier((&___OkayButtonText_1), value);
	}

	inline static int32_t get_offset_of_CancelButtonText_2() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___CancelButtonText_2)); }
	inline String_t* get_CancelButtonText_2() const { return ___CancelButtonText_2; }
	inline String_t** get_address_of_CancelButtonText_2() { return &___CancelButtonText_2; }
	inline void set_CancelButtonText_2(String_t* value)
	{
		___CancelButtonText_2 = value;
		Il2CppCodeGenWriteBarrier((&___CancelButtonText_2), value);
	}

	inline static int32_t get_offset_of_Options_3() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___Options_3)); }
	inline List_1_t3319525431 * get_Options_3() const { return ___Options_3; }
	inline List_1_t3319525431 ** get_address_of_Options_3() { return &___Options_3; }
	inline void set_Options_3(List_1_t3319525431 * value)
	{
		___Options_3 = value;
		Il2CppCodeGenWriteBarrier((&___Options_3), value);
	}

	inline static int32_t get_offset_of_Callback_4() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___Callback_4)); }
	inline Action_2_t2394327294 * get_Callback_4() const { return ___Callback_4; }
	inline Action_2_t2394327294 ** get_address_of_Callback_4() { return &___Callback_4; }
	inline void set_Callback_4(Action_2_t2394327294 * value)
	{
		___Callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGREQUEST_T599015159_H
#ifndef U3CU3EC_T126346862_H
#define U3CU3EC_T126346862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore/<>c
struct  U3CU3Ec_t126346862  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t126346862_StaticFields
{
public:
	// UnityEngine.Purchasing.UIFakeStore/<>c UnityEngine.Purchasing.UIFakeStore/<>c::<>9
	U3CU3Ec_t126346862 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.String> UnityEngine.Purchasing.UIFakeStore/<>c::<>9__18_0
	Func_2_t1011448493 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t126346862_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t126346862 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t126346862 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t126346862 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t126346862_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t1011448493 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t1011448493 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t1011448493 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T126346862_H
#ifndef U3CDOINVOKEU3ED__2_T4183149284_H
#define U3CDOINVOKEU3ED__2_T4183149284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2
struct  U3CDoInvokeU3Ed__2_t4183149284  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::a
	Action_t1264377477 * ___a_2;
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::delayInSeconds
	int32_t ___delayInSeconds_3;
	// UnityEngine.Purchasing.AsyncWebUtil UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::<>4__this
	AsyncWebUtil_t2717304869 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__2_t4183149284, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__2_t4183149284, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_a_2() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__2_t4183149284, ___a_2)); }
	inline Action_t1264377477 * get_a_2() const { return ___a_2; }
	inline Action_t1264377477 ** get_address_of_a_2() { return &___a_2; }
	inline void set_a_2(Action_t1264377477 * value)
	{
		___a_2 = value;
		Il2CppCodeGenWriteBarrier((&___a_2), value);
	}

	inline static int32_t get_offset_of_delayInSeconds_3() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__2_t4183149284, ___delayInSeconds_3)); }
	inline int32_t get_delayInSeconds_3() const { return ___delayInSeconds_3; }
	inline int32_t* get_address_of_delayInSeconds_3() { return &___delayInSeconds_3; }
	inline void set_delayInSeconds_3(int32_t value)
	{
		___delayInSeconds_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__2_t4183149284, ___U3CU3E4__this_4)); }
	inline AsyncWebUtil_t2717304869 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline AsyncWebUtil_t2717304869 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(AsyncWebUtil_t2717304869 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOINVOKEU3ED__2_T4183149284_H
#ifndef U3CPROCESSU3ED__3_T1355240730_H
#define U3CPROCESSU3ED__3_T1355240730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3
struct  U3CProcessU3Ed__3_t1355240730  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.WWW UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::request
	WWW_t3688466362 * ___request_2;
	// System.Action`1<System.String> UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::responseHandler
	Action_1_t2019918284 * ___responseHandler_3;
	// System.Action`1<System.String> UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::errorHandler
	Action_1_t2019918284 * ___errorHandler_4;
	// UnityEngine.Purchasing.AsyncWebUtil UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::<>4__this
	AsyncWebUtil_t2717304869 * ___U3CU3E4__this_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__3_t1355240730, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__3_t1355240730, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__3_t1355240730, ___request_2)); }
	inline WWW_t3688466362 * get_request_2() const { return ___request_2; }
	inline WWW_t3688466362 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(WWW_t3688466362 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier((&___request_2), value);
	}

	inline static int32_t get_offset_of_responseHandler_3() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__3_t1355240730, ___responseHandler_3)); }
	inline Action_1_t2019918284 * get_responseHandler_3() const { return ___responseHandler_3; }
	inline Action_1_t2019918284 ** get_address_of_responseHandler_3() { return &___responseHandler_3; }
	inline void set_responseHandler_3(Action_1_t2019918284 * value)
	{
		___responseHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___responseHandler_3), value);
	}

	inline static int32_t get_offset_of_errorHandler_4() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__3_t1355240730, ___errorHandler_4)); }
	inline Action_1_t2019918284 * get_errorHandler_4() const { return ___errorHandler_4; }
	inline Action_1_t2019918284 ** get_address_of_errorHandler_4() { return &___errorHandler_4; }
	inline void set_errorHandler_4(Action_1_t2019918284 * value)
	{
		___errorHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___errorHandler_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__3_t1355240730, ___U3CU3E4__this_5)); }
	inline AsyncWebUtil_t2717304869 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline AsyncWebUtil_t2717304869 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(AsyncWebUtil_t2717304869 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSU3ED__3_T1355240730_H
#ifndef CLOUDCATALOGIMPL_T1580312503_H
#define CLOUDCATALOGIMPL_T1580312503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl
struct  CloudCatalogImpl_t1580312503  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.CloudCatalogImpl::m_AsyncUtil
	RuntimeObject* ___m_AsyncUtil_0;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_CacheFileName
	String_t* ___m_CacheFileName_1;
	// UnityEngine.ILogger UnityEngine.Purchasing.CloudCatalogImpl::m_Logger
	RuntimeObject* ___m_Logger_2;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_CatalogURL
	String_t* ___m_CatalogURL_3;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_StoreName
	String_t* ___m_StoreName_4;

public:
	inline static int32_t get_offset_of_m_AsyncUtil_0() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t1580312503, ___m_AsyncUtil_0)); }
	inline RuntimeObject* get_m_AsyncUtil_0() const { return ___m_AsyncUtil_0; }
	inline RuntimeObject** get_address_of_m_AsyncUtil_0() { return &___m_AsyncUtil_0; }
	inline void set_m_AsyncUtil_0(RuntimeObject* value)
	{
		___m_AsyncUtil_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncUtil_0), value);
	}

	inline static int32_t get_offset_of_m_CacheFileName_1() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t1580312503, ___m_CacheFileName_1)); }
	inline String_t* get_m_CacheFileName_1() const { return ___m_CacheFileName_1; }
	inline String_t** get_address_of_m_CacheFileName_1() { return &___m_CacheFileName_1; }
	inline void set_m_CacheFileName_1(String_t* value)
	{
		___m_CacheFileName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheFileName_1), value);
	}

	inline static int32_t get_offset_of_m_Logger_2() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t1580312503, ___m_Logger_2)); }
	inline RuntimeObject* get_m_Logger_2() const { return ___m_Logger_2; }
	inline RuntimeObject** get_address_of_m_Logger_2() { return &___m_Logger_2; }
	inline void set_m_Logger_2(RuntimeObject* value)
	{
		___m_Logger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_2), value);
	}

	inline static int32_t get_offset_of_m_CatalogURL_3() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t1580312503, ___m_CatalogURL_3)); }
	inline String_t* get_m_CatalogURL_3() const { return ___m_CatalogURL_3; }
	inline String_t** get_address_of_m_CatalogURL_3() { return &___m_CatalogURL_3; }
	inline void set_m_CatalogURL_3(String_t* value)
	{
		___m_CatalogURL_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CatalogURL_3), value);
	}

	inline static int32_t get_offset_of_m_StoreName_4() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t1580312503, ___m_StoreName_4)); }
	inline String_t* get_m_StoreName_4() const { return ___m_StoreName_4; }
	inline String_t** get_address_of_m_StoreName_4() { return &___m_StoreName_4; }
	inline void set_m_StoreName_4(String_t* value)
	{
		___m_StoreName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDCATALOGIMPL_T1580312503_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T1700828538_H
#define U3CU3EC__DISPLAYCLASS10_0_T1700828538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t1700828538  : public RuntimeObject
{
public:
	// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>> UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::callback
	Action_1_t3372111503 * ___callback_0;
	// System.Int32 UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::delayInSeconds
	int32_t ___delayInSeconds_1;
	// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::<>4__this
	CloudCatalogImpl_t1580312503 * ___U3CU3E4__this_2;
	// System.Action UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::<>9__2
	Action_t1264377477 * ___U3CU3E9__2_3;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1700828538, ___callback_0)); }
	inline Action_1_t3372111503 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3372111503 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3372111503 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_delayInSeconds_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1700828538, ___delayInSeconds_1)); }
	inline int32_t get_delayInSeconds_1() const { return ___delayInSeconds_1; }
	inline int32_t* get_address_of_delayInSeconds_1() { return &___delayInSeconds_1; }
	inline void set_delayInSeconds_1(int32_t value)
	{
		___delayInSeconds_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1700828538, ___U3CU3E4__this_2)); }
	inline CloudCatalogImpl_t1580312503 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CloudCatalogImpl_t1580312503 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CloudCatalogImpl_t1580312503 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1700828538, ___U3CU3E9__2_3)); }
	inline Action_t1264377477 * get_U3CU3E9__2_3() const { return ___U3CU3E9__2_3; }
	inline Action_t1264377477 ** get_address_of_U3CU3E9__2_3() { return &___U3CU3E9__2_3; }
	inline void set_U3CU3E9__2_3(Action_t1264377477 * value)
	{
		___U3CU3E9__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T1700828538_H
#ifndef U3CU3EC_T1424112183_H
#define U3CU3EC_T1424112183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl/<>c
struct  U3CU3Ec_t1424112183  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1424112183_StaticFields
{
public:
	// UnityEngine.Purchasing.CloudCatalogImpl/<>c UnityEngine.Purchasing.CloudCatalogImpl/<>c::<>9
	U3CU3Ec_t1424112183 * ___U3CU3E9_0;
	// System.Func`3<System.Char,System.Int32,System.String> UnityEngine.Purchasing.CloudCatalogImpl/<>c::<>9__12_0
	Func_3_t1402304843 * ___U3CU3E9__12_0_1;
	// System.Func`3<System.String,System.String,System.String> UnityEngine.Purchasing.CloudCatalogImpl/<>c::<>9__12_1
	Func_3_t2383982614 * ___U3CU3E9__12_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1424112183_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1424112183 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1424112183 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1424112183 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1424112183_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_3_t1402304843 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_3_t1402304843 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_3_t1402304843 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1424112183_StaticFields, ___U3CU3E9__12_1_2)); }
	inline Func_3_t2383982614 * get_U3CU3E9__12_1_2() const { return ___U3CU3E9__12_1_2; }
	inline Func_3_t2383982614 ** get_address_of_U3CU3E9__12_1_2() { return &___U3CU3E9__12_1_2; }
	inline void set_U3CU3E9__12_1_2(Func_3_t2383982614 * value)
	{
		___U3CU3E9__12_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1424112183_H
#ifndef NATIVESTOREPROVIDER_T1575491570_H
#define NATIVESTOREPROVIDER_T1575491570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.NativeStoreProvider
struct  NativeStoreProvider_t1575491570  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVESTOREPROVIDER_T1575491570_H
#ifndef ANDROIDJAVASTORE_T1912360766_H
#define ANDROIDJAVASTORE_T1912360766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AndroidJavaStore
struct  AndroidJavaStore_t1912360766  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AndroidJavaStore::m_Store
	AndroidJavaObject_t4131667876 * ___m_Store_0;

public:
	inline static int32_t get_offset_of_m_Store_0() { return static_cast<int32_t>(offsetof(AndroidJavaStore_t1912360766, ___m_Store_0)); }
	inline AndroidJavaObject_t4131667876 * get_m_Store_0() const { return ___m_Store_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_m_Store_0() { return &___m_Store_0; }
	inline void set_m_Store_0(AndroidJavaObject_t4131667876 * value)
	{
		___m_Store_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Store_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVASTORE_T1912360766_H
#ifndef STOREID_T694756629_H
#define STOREID_T694756629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreID
struct  StoreID_t694756629  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.StoreID::store
	String_t* ___store_0;
	// System.String UnityEngine.Purchasing.StoreID::id
	String_t* ___id_1;

public:
	inline static int32_t get_offset_of_store_0() { return static_cast<int32_t>(offsetof(StoreID_t694756629, ___store_0)); }
	inline String_t* get_store_0() const { return ___store_0; }
	inline String_t** get_address_of_store_0() { return &___store_0; }
	inline void set_store_0(String_t* value)
	{
		___store_0 = value;
		Il2CppCodeGenWriteBarrier((&___store_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(StoreID_t694756629, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOREID_T694756629_H
#ifndef PRODUCTCATALOGPAYOUT_T924351538_H
#define PRODUCTCATALOGPAYOUT_T924351538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogPayout
struct  ProductCatalogPayout_t924351538  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::t
	String_t* ___t_0;
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::st
	String_t* ___st_1;
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::d
	String_t* ___d_2;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t924351538, ___t_0)); }
	inline String_t* get_t_0() const { return ___t_0; }
	inline String_t** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(String_t* value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_st_1() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t924351538, ___st_1)); }
	inline String_t* get_st_1() const { return ___st_1; }
	inline String_t** get_address_of_st_1() { return &___st_1; }
	inline void set_st_1(String_t* value)
	{
		___st_1 = value;
		Il2CppCodeGenWriteBarrier((&___st_1), value);
	}

	inline static int32_t get_offset_of_d_2() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t924351538, ___d_2)); }
	inline String_t* get_d_2() const { return ___d_2; }
	inline String_t** get_address_of_d_2() { return &___d_2; }
	inline void set_d_2(String_t* value)
	{
		___d_2 = value;
		Il2CppCodeGenWriteBarrier((&___d_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGPAYOUT_T924351538_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef PRODUCTCATALOG_T3178009003_H
#define PRODUCTCATALOG_T3178009003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalog
struct  ProductCatalog_t3178009003  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogItem> UnityEngine.Purchasing.ProductCatalog::products
	List_1_t3613492376 * ___products_1;

public:
	inline static int32_t get_offset_of_products_1() { return static_cast<int32_t>(offsetof(ProductCatalog_t3178009003, ___products_1)); }
	inline List_1_t3613492376 * get_products_1() const { return ___products_1; }
	inline List_1_t3613492376 ** get_address_of_products_1() { return &___products_1; }
	inline void set_products_1(List_1_t3613492376 * value)
	{
		___products_1 = value;
		Il2CppCodeGenWriteBarrier((&___products_1), value);
	}
};

struct ProductCatalog_t3178009003_StaticFields
{
public:
	// UnityEngine.Purchasing.IProductCatalogImpl UnityEngine.Purchasing.ProductCatalog::instance
	RuntimeObject* ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(ProductCatalog_t3178009003_StaticFields, ___instance_0)); }
	inline RuntimeObject* get_instance_0() const { return ___instance_0; }
	inline RuntimeObject** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject* value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOG_T3178009003_H
#ifndef PRODUCTCATALOGIMPL_T923359024_H
#define PRODUCTCATALOGIMPL_T923359024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogImpl
struct  ProductCatalogImpl_t923359024  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGIMPL_T923359024_H
#ifndef PROMO_T281370406_H
#define PROMO_T281370406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Promo
struct  Promo_t281370406  : public RuntimeObject
{
public:

public:
};

struct Promo_t281370406_StaticFields
{
public:
	// UnityEngine.Purchasing.JSONStore UnityEngine.Purchasing.Promo::s_PromoPurchaser
	JSONStore_t1587640366 * ___s_PromoPurchaser_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.Promo::s_Unity
	RuntimeObject* ___s_Unity_1;

public:
	inline static int32_t get_offset_of_s_PromoPurchaser_0() { return static_cast<int32_t>(offsetof(Promo_t281370406_StaticFields, ___s_PromoPurchaser_0)); }
	inline JSONStore_t1587640366 * get_s_PromoPurchaser_0() const { return ___s_PromoPurchaser_0; }
	inline JSONStore_t1587640366 ** get_address_of_s_PromoPurchaser_0() { return &___s_PromoPurchaser_0; }
	inline void set_s_PromoPurchaser_0(JSONStore_t1587640366 * value)
	{
		___s_PromoPurchaser_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PromoPurchaser_0), value);
	}

	inline static int32_t get_offset_of_s_Unity_1() { return static_cast<int32_t>(offsetof(Promo_t281370406_StaticFields, ___s_Unity_1)); }
	inline RuntimeObject* get_s_Unity_1() const { return ___s_Unity_1; }
	inline RuntimeObject** get_address_of_s_Unity_1() { return &___s_Unity_1; }
	inline void set_s_Unity_1(RuntimeObject* value)
	{
		___s_Unity_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Unity_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROMO_T281370406_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef STOREINSTANCE_T2416643455_H
#define STOREINSTANCE_T2416643455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance
struct  StoreInstance_t2416643455  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance::<storeName>k__BackingField
	String_t* ___U3CstoreNameU3Ek__BackingField_0;
	// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance::<instance>k__BackingField
	RuntimeObject* ___U3CinstanceU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CstoreNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StoreInstance_t2416643455, ___U3CstoreNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CstoreNameU3Ek__BackingField_0() const { return ___U3CstoreNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstoreNameU3Ek__BackingField_0() { return &___U3CstoreNameU3Ek__BackingField_0; }
	inline void set_U3CstoreNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CstoreNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StoreInstance_t2416643455, ___U3CinstanceU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CinstanceU3Ek__BackingField_1() const { return ___U3CinstanceU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CinstanceU3Ek__BackingField_1() { return &___U3CinstanceU3Ek__BackingField_1; }
	inline void set_U3CinstanceU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CinstanceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOREINSTANCE_T2416643455_H
#ifndef MICROSOFTCONFIGURATION_T2761062260_H
#define MICROSOFTCONFIGURATION_T2761062260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StandardPurchasingModule/MicrosoftConfiguration
struct  MicrosoftConfiguration_t2761062260  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule/MicrosoftConfiguration::useMock
	bool ___useMock_0;
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule/MicrosoftConfiguration::module
	StandardPurchasingModule_t2580735509 * ___module_1;

public:
	inline static int32_t get_offset_of_useMock_0() { return static_cast<int32_t>(offsetof(MicrosoftConfiguration_t2761062260, ___useMock_0)); }
	inline bool get_useMock_0() const { return ___useMock_0; }
	inline bool* get_address_of_useMock_0() { return &___useMock_0; }
	inline void set_useMock_0(bool value)
	{
		___useMock_0 = value;
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(MicrosoftConfiguration_t2761062260, ___module_1)); }
	inline StandardPurchasingModule_t2580735509 * get_module_1() const { return ___module_1; }
	inline StandardPurchasingModule_t2580735509 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(StandardPurchasingModule_t2580735509 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROSOFTCONFIGURATION_T2761062260_H
#ifndef UNIFIEDRECEIPT_T1348780434_H
#define UNIFIEDRECEIPT_T1348780434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnifiedReceipt
struct  UnifiedReceipt_t1348780434  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UnifiedReceipt::Payload
	String_t* ___Payload_0;

public:
	inline static int32_t get_offset_of_Payload_0() { return static_cast<int32_t>(offsetof(UnifiedReceipt_t1348780434, ___Payload_0)); }
	inline String_t* get_Payload_0() const { return ___Payload_0; }
	inline String_t** get_address_of_Payload_0() { return &___Payload_0; }
	inline void set_Payload_0(String_t* value)
	{
		___Payload_0 = value;
		Il2CppCodeGenWriteBarrier((&___Payload_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFIEDRECEIPT_T1348780434_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef U3CDELAYEDCOROUTINEU3ED__29_T3667477285_H
#define U3CDELAYEDCOROUTINEU3ED__29_T3667477285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__29
struct  U3CDelayedCoroutineU3Ed__29_t3667477285  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__29::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Collections.IEnumerator UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__29::coroutine
	RuntimeObject* ___coroutine_2;
	// System.Int32 UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__29::delay
	int32_t ___delay_3;
	// UnityEngine.Purchasing.Extension.UnityUtil UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__29::<>4__this
	UnityUtil_t103543446 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__29_t3667477285, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__29_t3667477285, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_coroutine_2() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__29_t3667477285, ___coroutine_2)); }
	inline RuntimeObject* get_coroutine_2() const { return ___coroutine_2; }
	inline RuntimeObject** get_address_of_coroutine_2() { return &___coroutine_2; }
	inline void set_coroutine_2(RuntimeObject* value)
	{
		___coroutine_2 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_2), value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__29_t3667477285, ___delay_3)); }
	inline int32_t get_delay_3() const { return ___delay_3; }
	inline int32_t* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(int32_t value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__29_t3667477285, ___U3CU3E4__this_4)); }
	inline UnityUtil_t103543446 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline UnityUtil_t103543446 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(UnityUtil_t103543446 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDCOROUTINEU3ED__29_T3667477285_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T1153436473_H
#define U3CU3EC__DISPLAYCLASS13_0_T1153436473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t1153436473  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription> UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0::products
	List_1_t2186087874 * ___products_0;
	// UnityEngine.Purchasing.FakeStore UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0::<>4__this
	FakeStore_t3710170489 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_products_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t1153436473, ___products_0)); }
	inline List_1_t2186087874 * get_products_0() const { return ___products_0; }
	inline List_1_t2186087874 ** get_address_of_products_0() { return &___products_0; }
	inline void set_products_0(List_1_t2186087874 * value)
	{
		___products_0 = value;
		Il2CppCodeGenWriteBarrier((&___products_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t1153436473, ___U3CU3E4__this_1)); }
	inline FakeStore_t3710170489 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FakeStore_t3710170489 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FakeStore_t3710170489 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T1153436473_H
#ifndef ABSTRACTPURCHASINGMODULE_T2882497868_H
#define ABSTRACTPURCHASINGMODULE_T2882497868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractPurchasingModule
struct  AbstractPurchasingModule_t2882497868  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.IPurchasingBinder UnityEngine.Purchasing.Extension.AbstractPurchasingModule::m_Binder
	RuntimeObject* ___m_Binder_0;

public:
	inline static int32_t get_offset_of_m_Binder_0() { return static_cast<int32_t>(offsetof(AbstractPurchasingModule_t2882497868, ___m_Binder_0)); }
	inline RuntimeObject* get_m_Binder_0() const { return ___m_Binder_0; }
	inline RuntimeObject** get_address_of_m_Binder_0() { return &___m_Binder_0; }
	inline void set_m_Binder_0(RuntimeObject* value)
	{
		___m_Binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTPURCHASINGMODULE_T2882497868_H
#ifndef ABSTRACTSTORE_T285429589_H
#define ABSTRACTSTORE_T285429589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractStore
struct  AbstractStore_t285429589  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTSTORE_T285429589_H
#ifndef U3CU3EC__DISPLAYCLASS7_1_T2576459376_H
#define U3CU3EC__DISPLAYCLASS7_1_T2576459376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1
struct  U3CU3Ec__DisplayClass7_1_t2576459376  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::dic
	Dictionary_2_t2865362463 * ___dic_0;
	// System.String UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::transactionId
	String_t* ___transactionId_1;
	// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0 UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass7_0_t620144240 * ___CSU24U3CU3E8__locals1_2;

public:
	inline static int32_t get_offset_of_dic_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t2576459376, ___dic_0)); }
	inline Dictionary_2_t2865362463 * get_dic_0() const { return ___dic_0; }
	inline Dictionary_2_t2865362463 ** get_address_of_dic_0() { return &___dic_0; }
	inline void set_dic_0(Dictionary_2_t2865362463 * value)
	{
		___dic_0 = value;
		Il2CppCodeGenWriteBarrier((&___dic_0), value);
	}

	inline static int32_t get_offset_of_transactionId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t2576459376, ___transactionId_1)); }
	inline String_t* get_transactionId_1() const { return ___transactionId_1; }
	inline String_t** get_address_of_transactionId_1() { return &___transactionId_1; }
	inline void set_transactionId_1(String_t* value)
	{
		___transactionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_1), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t2576459376, ___CSU24U3CU3E8__locals1_2)); }
	inline U3CU3Ec__DisplayClass7_0_t620144240 * get_CSU24U3CU3E8__locals1_2() const { return ___CSU24U3CU3E8__locals1_2; }
	inline U3CU3Ec__DisplayClass7_0_t620144240 ** get_address_of_CSU24U3CU3E8__locals1_2() { return &___CSU24U3CU3E8__locals1_2; }
	inline void set_CSU24U3CU3E8__locals1_2(U3CU3Ec__DisplayClass7_0_t620144240 * value)
	{
		___CSU24U3CU3E8__locals1_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_1_T2576459376_H
#ifndef U3CVALIDATERECEIPTPROCESSU3ED__47_T372105418_H
#define U3CVALIDATERECEIPTPROCESSU3ED__47_T372105418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47
struct  U3CValidateReceiptProcessU3Ed__47_t372105418  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::transactionId
	String_t* ___transactionId_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::receipt
	String_t* ___receipt_3;
	// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::result
	Action_3_t3666713281 * ___result_4;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<tempJson>5__1
	Dictionary_2_t2865362463 * ___U3CtempJsonU3E5__1_6;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<wf>5__2
	WWWForm_t4064702195 * ___U3CwfU3E5__2_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<sign>5__3
	String_t* ___U3CsignU3E5__3_8;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<w>5__4
	WWW_t3688466362 * ___U3CwU3E5__4_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<jsonObjects>5__5
	Dictionary_2_t2865362463 * ___U3CjsonObjectsU3E5__5_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<code>5__6
	String_t* ___U3CcodeU3E5__6_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<msg>5__7
	String_t* ___U3CmsgU3E5__7_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_transactionId_2() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___transactionId_2)); }
	inline String_t* get_transactionId_2() const { return ___transactionId_2; }
	inline String_t** get_address_of_transactionId_2() { return &___transactionId_2; }
	inline void set_transactionId_2(String_t* value)
	{
		___transactionId_2 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_2), value);
	}

	inline static int32_t get_offset_of_receipt_3() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___receipt_3)); }
	inline String_t* get_receipt_3() const { return ___receipt_3; }
	inline String_t** get_address_of_receipt_3() { return &___receipt_3; }
	inline void set_receipt_3(String_t* value)
	{
		___receipt_3 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_3), value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___result_4)); }
	inline Action_3_t3666713281 * get_result_4() const { return ___result_4; }
	inline Action_3_t3666713281 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Action_3_t3666713281 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CU3E4__this_5)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CtempJsonU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CtempJsonU3E5__1_6)); }
	inline Dictionary_2_t2865362463 * get_U3CtempJsonU3E5__1_6() const { return ___U3CtempJsonU3E5__1_6; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CtempJsonU3E5__1_6() { return &___U3CtempJsonU3E5__1_6; }
	inline void set_U3CtempJsonU3E5__1_6(Dictionary_2_t2865362463 * value)
	{
		___U3CtempJsonU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempJsonU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CwfU3E5__2_7)); }
	inline WWWForm_t4064702195 * get_U3CwfU3E5__2_7() const { return ___U3CwfU3E5__2_7; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwfU3E5__2_7() { return &___U3CwfU3E5__2_7; }
	inline void set_U3CwfU3E5__2_7(WWWForm_t4064702195 * value)
	{
		___U3CwfU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CsignU3E5__3_8)); }
	inline String_t* get_U3CsignU3E5__3_8() const { return ___U3CsignU3E5__3_8; }
	inline String_t** get_address_of_U3CsignU3E5__3_8() { return &___U3CsignU3E5__3_8; }
	inline void set_U3CsignU3E5__3_8(String_t* value)
	{
		___U3CsignU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CwU3E5__4_9)); }
	inline WWW_t3688466362 * get_U3CwU3E5__4_9() const { return ___U3CwU3E5__4_9; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E5__4_9() { return &___U3CwU3E5__4_9; }
	inline void set_U3CwU3E5__4_9(WWW_t3688466362 * value)
	{
		___U3CwU3E5__4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__4_9), value);
	}

	inline static int32_t get_offset_of_U3CjsonObjectsU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CjsonObjectsU3E5__5_10)); }
	inline Dictionary_2_t2865362463 * get_U3CjsonObjectsU3E5__5_10() const { return ___U3CjsonObjectsU3E5__5_10; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CjsonObjectsU3E5__5_10() { return &___U3CjsonObjectsU3E5__5_10; }
	inline void set_U3CjsonObjectsU3E5__5_10(Dictionary_2_t2865362463 * value)
	{
		___U3CjsonObjectsU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonObjectsU3E5__5_10), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__6_11() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CcodeU3E5__6_11)); }
	inline String_t* get_U3CcodeU3E5__6_11() const { return ___U3CcodeU3E5__6_11; }
	inline String_t** get_address_of_U3CcodeU3E5__6_11() { return &___U3CcodeU3E5__6_11; }
	inline void set_U3CcodeU3E5__6_11(String_t* value)
	{
		___U3CcodeU3E5__6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__6_11), value);
	}

	inline static int32_t get_offset_of_U3CmsgU3E5__7_12() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CmsgU3E5__7_12)); }
	inline String_t* get_U3CmsgU3E5__7_12() const { return ___U3CmsgU3E5__7_12; }
	inline String_t** get_address_of_U3CmsgU3E5__7_12() { return &___U3CmsgU3E5__7_12; }
	inline void set_U3CmsgU3E5__7_12(String_t* value)
	{
		___U3CmsgU3E5__7_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmsgU3E5__7_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATERECEIPTPROCESSU3ED__47_T372105418_H
#ifndef PAYMETHOD_T328949237_H
#define PAYMETHOD_T328949237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PayMethod
struct  PayMethod_t328949237  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYMETHOD_T328949237_H
#ifndef FAKEGOOGLEPLAYCONFIGURATION_T3630139311_H
#define FAKEGOOGLEPLAYCONFIGURATION_T3630139311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeGooglePlayConfiguration
struct  FakeGooglePlayConfiguration_t3630139311  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEGOOGLEPLAYCONFIGURATION_T3630139311_H
#ifndef FAKESAMSUNGAPPSEXTENSIONS_T2880024647_H
#define FAKESAMSUNGAPPSEXTENSIONS_T2880024647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeSamsungAppsExtensions
struct  FakeSamsungAppsExtensions_t2880024647  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESAMSUNGAPPSEXTENSIONS_T2880024647_H
#ifndef FAKEUNITYCHANNELCONFIGURATION_T2647072263_H
#define FAKEUNITYCHANNELCONFIGURATION_T2647072263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeUnityChannelConfiguration
struct  FakeUnityChannelConfiguration_t2647072263  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Purchasing.FakeUnityChannelConfiguration::<fetchReceiptPayloadOnPurchase>k__BackingField
	bool ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FakeUnityChannelConfiguration_t2647072263, ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0)); }
	inline bool get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() const { return ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() { return &___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0; }
	inline void set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(bool value)
	{
		___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEUNITYCHANNELCONFIGURATION_T2647072263_H
#ifndef FAKEUNITYCHANNELEXTENSIONS_T3596584646_H
#define FAKEUNITYCHANNELEXTENSIONS_T3596584646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeUnityChannelExtensions
struct  FakeUnityChannelExtensions_t3596584646  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEUNITYCHANNELEXTENSIONS_T3596584646_H
#ifndef XIAOMIPRICETIERS_T2048704184_H
#define XIAOMIPRICETIERS_T2048704184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.XiaomiPriceTiers
struct  XiaomiPriceTiers_t2048704184  : public RuntimeObject
{
public:

public:
};

struct XiaomiPriceTiers_t2048704184_StaticFields
{
public:
	// System.Int32[] UnityEngine.Purchasing.XiaomiPriceTiers::XiaomiPriceTierPrices
	Int32U5BU5D_t385246372* ___XiaomiPriceTierPrices_0;

public:
	inline static int32_t get_offset_of_XiaomiPriceTierPrices_0() { return static_cast<int32_t>(offsetof(XiaomiPriceTiers_t2048704184_StaticFields, ___XiaomiPriceTierPrices_0)); }
	inline Int32U5BU5D_t385246372* get_XiaomiPriceTierPrices_0() const { return ___XiaomiPriceTierPrices_0; }
	inline Int32U5BU5D_t385246372** get_address_of_XiaomiPriceTierPrices_0() { return &___XiaomiPriceTierPrices_0; }
	inline void set_XiaomiPriceTierPrices_0(Int32U5BU5D_t385246372* value)
	{
		___XiaomiPriceTierPrices_0 = value;
		Il2CppCodeGenWriteBarrier((&___XiaomiPriceTierPrices_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XIAOMIPRICETIERS_T2048704184_H
#ifndef UNITYCHANNELBINDINGS_T3184348469_H
#define UNITYCHANNELBINDINGS_T3184348469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelBindings
struct  UnityChannelBindings_t3184348469  : public RuntimeObject
{
public:
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseCallback
	Action_2_t1290832230 * ___m_PurchaseCallback_0;
	// System.String UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseGuid
	String_t* ___m_PurchaseGuid_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>> UnityEngine.Purchasing.UnityChannelBindings::m_ValidateCallbacks
	Dictionary_2_t1588782543 * ___m_ValidateCallbacks_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>> UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseConfirmCallbacks
	Dictionary_2_t1588782543 * ___m_PurchaseConfirmCallbacks_3;

public:
	inline static int32_t get_offset_of_m_PurchaseCallback_0() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t3184348469, ___m_PurchaseCallback_0)); }
	inline Action_2_t1290832230 * get_m_PurchaseCallback_0() const { return ___m_PurchaseCallback_0; }
	inline Action_2_t1290832230 ** get_address_of_m_PurchaseCallback_0() { return &___m_PurchaseCallback_0; }
	inline void set_m_PurchaseCallback_0(Action_2_t1290832230 * value)
	{
		___m_PurchaseCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseCallback_0), value);
	}

	inline static int32_t get_offset_of_m_PurchaseGuid_1() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t3184348469, ___m_PurchaseGuid_1)); }
	inline String_t* get_m_PurchaseGuid_1() const { return ___m_PurchaseGuid_1; }
	inline String_t** get_address_of_m_PurchaseGuid_1() { return &___m_PurchaseGuid_1; }
	inline void set_m_PurchaseGuid_1(String_t* value)
	{
		___m_PurchaseGuid_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseGuid_1), value);
	}

	inline static int32_t get_offset_of_m_ValidateCallbacks_2() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t3184348469, ___m_ValidateCallbacks_2)); }
	inline Dictionary_2_t1588782543 * get_m_ValidateCallbacks_2() const { return ___m_ValidateCallbacks_2; }
	inline Dictionary_2_t1588782543 ** get_address_of_m_ValidateCallbacks_2() { return &___m_ValidateCallbacks_2; }
	inline void set_m_ValidateCallbacks_2(Dictionary_2_t1588782543 * value)
	{
		___m_ValidateCallbacks_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidateCallbacks_2), value);
	}

	inline static int32_t get_offset_of_m_PurchaseConfirmCallbacks_3() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t3184348469, ___m_PurchaseConfirmCallbacks_3)); }
	inline Dictionary_2_t1588782543 * get_m_PurchaseConfirmCallbacks_3() const { return ___m_PurchaseConfirmCallbacks_3; }
	inline Dictionary_2_t1588782543 ** get_address_of_m_PurchaseConfirmCallbacks_3() { return &___m_PurchaseConfirmCallbacks_3; }
	inline void set_m_PurchaseConfirmCallbacks_3(Dictionary_2_t1588782543 * value)
	{
		___m_PurchaseConfirmCallbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseConfirmCallbacks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELBINDINGS_T3184348469_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T777686714_H
#define U3CU3EC__DISPLAYCLASS16_0_T777686714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t777686714  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::transactionId
	String_t* ___transactionId_0;
	// UnityEngine.Purchasing.UnityChannelBindings UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::<>4__this
	UnityChannelBindings_t3184348469 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_transactionId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t777686714, ___transactionId_0)); }
	inline String_t* get_transactionId_0() const { return ___transactionId_0; }
	inline String_t** get_address_of_transactionId_0() { return &___transactionId_0; }
	inline void set_transactionId_0(String_t* value)
	{
		___transactionId_0 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t777686714, ___U3CU3E4__this_1)); }
	inline UnityChannelBindings_t3184348469 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UnityChannelBindings_t3184348469 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UnityChannelBindings_t3184348469 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T777686714_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T620144240_H
#define U3CU3EC__DISPLAYCLASS7_0_T620144240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t620144240  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0::product
	ProductDefinition_t339727138 * ___product_0;
	// UnityEngine.Purchasing.UnityChannelImpl UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0::<>4__this
	UnityChannelImpl_t3062682360 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_product_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t620144240, ___product_0)); }
	inline ProductDefinition_t339727138 * get_product_0() const { return ___product_0; }
	inline ProductDefinition_t339727138 ** get_address_of_product_0() { return &___product_0; }
	inline void set_product_0(ProductDefinition_t339727138 * value)
	{
		___product_0 = value;
		Il2CppCodeGenWriteBarrier((&___product_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t620144240, ___U3CU3E4__this_1)); }
	inline UnityChannelImpl_t3062682360 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UnityChannelImpl_t3062682360 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UnityChannelImpl_t3062682360 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T620144240_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T1261164119_H
#define U3CU3EC__DISPLAYCLASS6_0_T1261164119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t1261164119  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::subject
	String_t* ___subject_0;
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::payload
	String_t* ___payload_1;
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::receipt
	String_t* ___receipt_2;
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::transactionId
	String_t* ___transactionId_3;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___subject_0)); }
	inline String_t* get_subject_0() const { return ___subject_0; }
	inline String_t** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(String_t* value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier((&___subject_0), value);
	}

	inline static int32_t get_offset_of_payload_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___payload_1)); }
	inline String_t* get_payload_1() const { return ___payload_1; }
	inline String_t** get_address_of_payload_1() { return &___payload_1; }
	inline void set_payload_1(String_t* value)
	{
		___payload_1 = value;
		Il2CppCodeGenWriteBarrier((&___payload_1), value);
	}

	inline static int32_t get_offset_of_receipt_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___receipt_2)); }
	inline String_t* get_receipt_2() const { return ___receipt_2; }
	inline String_t** get_address_of_receipt_2() { return &___receipt_2; }
	inline void set_receipt_2(String_t* value)
	{
		___receipt_2 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_2), value);
	}

	inline static int32_t get_offset_of_transactionId_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___transactionId_3)); }
	inline String_t* get_transactionId_3() const { return ___transactionId_3; }
	inline String_t** get_address_of_transactionId_3() { return &___transactionId_3; }
	inline void set_transactionId_3(String_t* value)
	{
		___transactionId_3 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T1261164119_H
#ifndef UNITYCHANNELPURCHASERECEIPT_T3342804115_H
#define UNITYCHANNELPURCHASERECEIPT_T3342804115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelPurchaseReceipt
struct  UnityChannelPurchaseReceipt_t3342804115  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UnityChannelPurchaseReceipt::storeSpecificId
	String_t* ___storeSpecificId_0;
	// System.String UnityEngine.Purchasing.UnityChannelPurchaseReceipt::transactionId
	String_t* ___transactionId_1;
	// System.String UnityEngine.Purchasing.UnityChannelPurchaseReceipt::orderQueryToken
	String_t* ___orderQueryToken_2;

public:
	inline static int32_t get_offset_of_storeSpecificId_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseReceipt_t3342804115, ___storeSpecificId_0)); }
	inline String_t* get_storeSpecificId_0() const { return ___storeSpecificId_0; }
	inline String_t** get_address_of_storeSpecificId_0() { return &___storeSpecificId_0; }
	inline void set_storeSpecificId_0(String_t* value)
	{
		___storeSpecificId_0 = value;
		Il2CppCodeGenWriteBarrier((&___storeSpecificId_0), value);
	}

	inline static int32_t get_offset_of_transactionId_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseReceipt_t3342804115, ___transactionId_1)); }
	inline String_t* get_transactionId_1() const { return ___transactionId_1; }
	inline String_t** get_address_of_transactionId_1() { return &___transactionId_1; }
	inline void set_transactionId_1(String_t* value)
	{
		___transactionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_1), value);
	}

	inline static int32_t get_offset_of_orderQueryToken_2() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseReceipt_t3342804115, ___orderQueryToken_2)); }
	inline String_t* get_orderQueryToken_2() const { return ___orderQueryToken_2; }
	inline String_t** get_address_of_orderQueryToken_2() { return &___orderQueryToken_2; }
	inline void set_orderQueryToken_2(String_t* value)
	{
		___orderQueryToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___orderQueryToken_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASERECEIPT_T3342804115_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T1949579283_H
#define U3CU3EC__DISPLAYCLASS19_0_T1949579283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t1949579283  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.ProductDescription UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass19_0::productDescription
	ProductDescription_t714013132 * ___productDescription_0;

public:
	inline static int32_t get_offset_of_productDescription_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t1949579283, ___productDescription_0)); }
	inline ProductDescription_t714013132 * get_productDescription_0() const { return ___productDescription_0; }
	inline ProductDescription_t714013132 ** get_address_of_productDescription_0() { return &___productDescription_0; }
	inline void set_productDescription_0(ProductDescription_t714013132 * value)
	{
		___productDescription_0 = value;
		Il2CppCodeGenWriteBarrier((&___productDescription_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T1949579283_H
#ifndef U3CU3EC_T4178581212_H
#define U3CU3EC_T4178581212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.LocalizedProductDescription/<>c
struct  U3CU3Ec_t4178581212  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4178581212_StaticFields
{
public:
	// UnityEngine.Purchasing.LocalizedProductDescription/<>c UnityEngine.Purchasing.LocalizedProductDescription/<>c::<>9
	U3CU3Ec_t4178581212 * ___U3CU3E9_0;
	// System.Text.RegularExpressions.MatchEvaluator UnityEngine.Purchasing.LocalizedProductDescription/<>c::<>9__11_0
	MatchEvaluator_t632122704 * ___U3CU3E9__11_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4178581212_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4178581212 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4178581212 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4178581212 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4178581212_StaticFields, ___U3CU3E9__11_0_1)); }
	inline MatchEvaluator_t632122704 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline MatchEvaluator_t632122704 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(MatchEvaluator_t632122704 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4178581212_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T1547909138_H
#define U3CU3EC__DISPLAYCLASS28_0_T1547909138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t1547909138  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0::subject
	String_t* ___subject_0;
	// System.String UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0::payload
	String_t* ___payload_1;
	// System.String UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0::receipt
	String_t* ___receipt_2;
	// System.String UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0::transactionId
	String_t* ___transactionId_3;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t1547909138, ___subject_0)); }
	inline String_t* get_subject_0() const { return ___subject_0; }
	inline String_t** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(String_t* value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier((&___subject_0), value);
	}

	inline static int32_t get_offset_of_payload_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t1547909138, ___payload_1)); }
	inline String_t* get_payload_1() const { return ___payload_1; }
	inline String_t** get_address_of_payload_1() { return &___payload_1; }
	inline void set_payload_1(String_t* value)
	{
		___payload_1 = value;
		Il2CppCodeGenWriteBarrier((&___payload_1), value);
	}

	inline static int32_t get_offset_of_receipt_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t1547909138, ___receipt_2)); }
	inline String_t* get_receipt_2() const { return ___receipt_2; }
	inline String_t** get_address_of_receipt_2() { return &___receipt_2; }
	inline void set_receipt_2(String_t* value)
	{
		___receipt_2 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_2), value);
	}

	inline static int32_t get_offset_of_transactionId_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t1547909138, ___transactionId_3)); }
	inline String_t* get_transactionId_3() const { return ___transactionId_3; }
	inline String_t** get_address_of_transactionId_3() { return &___transactionId_3; }
	inline void set_transactionId_3(String_t* value)
	{
		___transactionId_3 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T1547909138_H
#ifndef U3CU3EC_T3746618910_H
#define U3CU3EC_T3746618910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.WinRTStore/<>c
struct  U3CU3Ec_t3746618910  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3746618910_StaticFields
{
public:
	// UnityEngine.Purchasing.WinRTStore/<>c UnityEngine.Purchasing.WinRTStore/<>c::<>9
	U3CU3Ec_t3746618910 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.Boolean> UnityEngine.Purchasing.WinRTStore/<>c::<>9__8_0
	Func_2_t3556253065 * ___U3CU3E9__8_0_1;
	// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription> UnityEngine.Purchasing.WinRTStore/<>c::<>9__8_1
	Func_2_t1244879711 * ___U3CU3E9__8_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3746618910_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3746618910 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3746618910 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3746618910 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3746618910_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t3556253065 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t3556253065 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t3556253065 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3746618910_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t1244879711 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t1244879711 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t1244879711 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3746618910_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FAKEAPPLEEXTENSIONS_T3685865150_H
#define FAKEAPPLEEXTENSIONS_T3685865150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeAppleExtensions
struct  FakeAppleExtensions_t3685865150  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEAPPLEEXTENSIONS_T3685865150_H
#ifndef FAKEMICROSOFTEXTENSIONS_T4237934947_H
#define FAKEMICROSOFTEXTENSIONS_T4237934947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeMicrosoftExtensions
struct  FakeMicrosoftExtensions_t4237934947  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMICROSOFTEXTENSIONS_T4237934947_H
#ifndef FAKEAPPLECONFIGUATION_T2415379217_H
#define FAKEAPPLECONFIGUATION_T2415379217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeAppleConfiguation
struct  FakeAppleConfiguation_t2415379217  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEAPPLECONFIGUATION_T2415379217_H
#ifndef FAKETIZENSTORECONFIGURATION_T714964994_H
#define FAKETIZENSTORECONFIGURATION_T714964994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeTizenStoreConfiguration
struct  FakeTizenStoreConfiguration_t714964994  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKETIZENSTORECONFIGURATION_T714964994_H
#ifndef __STATICARRAYINITTYPESIZEU3D368_T2501028641_H
#define __STATICARRAYINITTYPESIZEU3D368_T2501028641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=368
struct  __StaticArrayInitTypeSizeU3D368_t2501028641 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D368_t2501028641__padding[368];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D368_T2501028641_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef WINRTSTORE_T2015085940_H
#define WINRTSTORE_T2015085940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.WinRTStore
struct  WinRTStore_t2015085940  : public AbstractStore_t285429589
{
public:
	// UnityEngine.Purchasing.Default.IWindowsIAP UnityEngine.Purchasing.WinRTStore::win8
	RuntimeObject* ___win8_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.WinRTStore::callback
	RuntimeObject* ___callback_1;
	// Uniject.IUtil UnityEngine.Purchasing.WinRTStore::util
	RuntimeObject* ___util_2;
	// UnityEngine.ILogger UnityEngine.Purchasing.WinRTStore::logger
	RuntimeObject* ___logger_3;
	// System.Boolean UnityEngine.Purchasing.WinRTStore::m_CanReceivePurchases
	bool ___m_CanReceivePurchases_4;

public:
	inline static int32_t get_offset_of_win8_0() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___win8_0)); }
	inline RuntimeObject* get_win8_0() const { return ___win8_0; }
	inline RuntimeObject** get_address_of_win8_0() { return &___win8_0; }
	inline void set_win8_0(RuntimeObject* value)
	{
		___win8_0 = value;
		Il2CppCodeGenWriteBarrier((&___win8_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___callback_1)); }
	inline RuntimeObject* get_callback_1() const { return ___callback_1; }
	inline RuntimeObject** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(RuntimeObject* value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_util_2() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___util_2)); }
	inline RuntimeObject* get_util_2() const { return ___util_2; }
	inline RuntimeObject** get_address_of_util_2() { return &___util_2; }
	inline void set_util_2(RuntimeObject* value)
	{
		___util_2 = value;
		Il2CppCodeGenWriteBarrier((&___util_2), value);
	}

	inline static int32_t get_offset_of_logger_3() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___logger_3)); }
	inline RuntimeObject* get_logger_3() const { return ___logger_3; }
	inline RuntimeObject** get_address_of_logger_3() { return &___logger_3; }
	inline void set_logger_3(RuntimeObject* value)
	{
		___logger_3 = value;
		Il2CppCodeGenWriteBarrier((&___logger_3), value);
	}

	inline static int32_t get_offset_of_m_CanReceivePurchases_4() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___m_CanReceivePurchases_4)); }
	inline bool get_m_CanReceivePurchases_4() const { return ___m_CanReceivePurchases_4; }
	inline bool* get_address_of_m_CanReceivePurchases_4() { return &___m_CanReceivePurchases_4; }
	inline void set_m_CanReceivePurchases_4(bool value)
	{
		___m_CanReceivePurchases_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINRTSTORE_T2015085940_H
#ifndef JSONSTORE_T1587640366_H
#define JSONSTORE_T1587640366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JSONStore
struct  JSONStore_t1587640366  : public AbstractStore_t285429589
{
public:
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.JSONStore::unity
	RuntimeObject* ___unity_0;
	// UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.JSONStore::store
	RuntimeObject* ___store_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONStore::promoPayload
	Dictionary_2_t2865362463 * ___promoPayload_2;

public:
	inline static int32_t get_offset_of_unity_0() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___unity_0)); }
	inline RuntimeObject* get_unity_0() const { return ___unity_0; }
	inline RuntimeObject** get_address_of_unity_0() { return &___unity_0; }
	inline void set_unity_0(RuntimeObject* value)
	{
		___unity_0 = value;
		Il2CppCodeGenWriteBarrier((&___unity_0), value);
	}

	inline static int32_t get_offset_of_store_1() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___store_1)); }
	inline RuntimeObject* get_store_1() const { return ___store_1; }
	inline RuntimeObject** get_address_of_store_1() { return &___store_1; }
	inline void set_store_1(RuntimeObject* value)
	{
		___store_1 = value;
		Il2CppCodeGenWriteBarrier((&___store_1), value);
	}

	inline static int32_t get_offset_of_promoPayload_2() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___promoPayload_2)); }
	inline Dictionary_2_t2865362463 * get_promoPayload_2() const { return ___promoPayload_2; }
	inline Dictionary_2_t2865362463 ** get_address_of_promoPayload_2() { return &___promoPayload_2; }
	inline void set_promoPayload_2(Dictionary_2_t2865362463 * value)
	{
		___promoPayload_2 = value;
		Il2CppCodeGenWriteBarrier((&___promoPayload_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTORE_T1587640366_H
#ifndef DECIMAL_T2948259380_H
#define DECIMAL_T2948259380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2948259380 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t2948259380_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t2948259380  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2948259380  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2948259380  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t2948259380  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t2948259380  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinValue_0)); }
	inline Decimal_t2948259380  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t2948259380 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t2948259380  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValue_1)); }
	inline Decimal_t2948259380  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t2948259380 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t2948259380  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinusOne_2)); }
	inline Decimal_t2948259380  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t2948259380 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t2948259380  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___One_3)); }
	inline Decimal_t2948259380  get_One_3() const { return ___One_3; }
	inline Decimal_t2948259380 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t2948259380  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t2948259380  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t2948259380 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t2948259380  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2948259380_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GOOGLEPLAYANDROIDJAVASTORE_T3169878644_H
#define GOOGLEPLAYANDROIDJAVASTORE_T3169878644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayAndroidJavaStore
struct  GooglePlayAndroidJavaStore_t3169878644  : public AndroidJavaStore_t1912360766
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYANDROIDJAVASTORE_T3169878644_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef RUNTIMEPLATFORM_T4159857903_H
#define RUNTIMEPLATFORM_T4159857903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t4159857903 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t4159857903, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T4159857903_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaProxy::javaInterface
	AndroidJavaClass_t32045322 * ___javaInterface_0;
	// UnityEngine.AndroidJavaObject UnityEngine.AndroidJavaProxy::proxyObject
	AndroidJavaObject_t4131667876 * ___proxyObject_1;

public:
	inline static int32_t get_offset_of_javaInterface_0() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643, ___javaInterface_0)); }
	inline AndroidJavaClass_t32045322 * get_javaInterface_0() const { return ___javaInterface_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_javaInterface_0() { return &___javaInterface_0; }
	inline void set_javaInterface_0(AndroidJavaClass_t32045322 * value)
	{
		___javaInterface_0 = value;
		Il2CppCodeGenWriteBarrier((&___javaInterface_0), value);
	}

	inline static int32_t get_offset_of_proxyObject_1() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643, ___proxyObject_1)); }
	inline AndroidJavaObject_t4131667876 * get_proxyObject_1() const { return ___proxyObject_1; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_proxyObject_1() { return &___proxyObject_1; }
	inline void set_proxyObject_1(AndroidJavaObject_t4131667876 * value)
	{
		___proxyObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___proxyObject_1), value);
	}
};

struct AndroidJavaProxy_t2835824643_StaticFields
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaProxy::s_JavaLangSystemClass
	GlobalJavaObjectRef_t3225273728 * ___s_JavaLangSystemClass_2;
	// System.IntPtr UnityEngine.AndroidJavaProxy::s_HashCodeMethodID
	intptr_t ___s_HashCodeMethodID_3;

public:
	inline static int32_t get_offset_of_s_JavaLangSystemClass_2() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643_StaticFields, ___s_JavaLangSystemClass_2)); }
	inline GlobalJavaObjectRef_t3225273728 * get_s_JavaLangSystemClass_2() const { return ___s_JavaLangSystemClass_2; }
	inline GlobalJavaObjectRef_t3225273728 ** get_address_of_s_JavaLangSystemClass_2() { return &___s_JavaLangSystemClass_2; }
	inline void set_s_JavaLangSystemClass_2(GlobalJavaObjectRef_t3225273728 * value)
	{
		___s_JavaLangSystemClass_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_JavaLangSystemClass_2), value);
	}

	inline static int32_t get_offset_of_s_HashCodeMethodID_3() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643_StaticFields, ___s_HashCodeMethodID_3)); }
	inline intptr_t get_s_HashCodeMethodID_3() const { return ___s_HashCodeMethodID_3; }
	inline intptr_t* get_address_of_s_HashCodeMethodID_3() { return &___s_HashCodeMethodID_3; }
	inline void set_s_HashCodeMethodID_3(intptr_t value)
	{
		___s_HashCodeMethodID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef PRODUCTTYPE_T1868976581_H
#define PRODUCTTYPE_T1868976581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductType
struct  ProductType_t1868976581 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProductType_t1868976581, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTTYPE_T1868976581_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=368 <PrivateImplementationDetails>::8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41
	__StaticArrayInitTypeSizeU3D368_t2501028641  ___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0;

public:
	inline static int32_t get_offset_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0)); }
	inline __StaticArrayInitTypeSizeU3D368_t2501028641  get_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0() const { return ___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0; }
	inline __StaticArrayInitTypeSizeU3D368_t2501028641 * get_address_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0() { return &___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0; }
	inline void set_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0(__StaticArrayInitTypeSizeU3D368_t2501028641  value)
	{
		___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef FAKESTOREUIMODE_T680685637_H
#define FAKESTOREUIMODE_T680685637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStoreUIMode
struct  FakeStoreUIMode_t680685637 
{
public:
	// System.Int32 UnityEngine.Purchasing.FakeStoreUIMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FakeStoreUIMode_t680685637, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESTOREUIMODE_T680685637_H
#ifndef DIALOGTYPE_T918222323_H
#define DIALOGTYPE_T918222323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore/DialogType
struct  DialogType_t918222323 
{
public:
	// System.Int32 UnityEngine.Purchasing.FakeStore/DialogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DialogType_t918222323, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGTYPE_T918222323_H
#ifndef APPSTORE_T355301105_H
#define APPSTORE_T355301105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppStore
struct  AppStore_t355301105 
{
public:
	// System.Int32 UnityEngine.Purchasing.AppStore::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AppStore_t355301105, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORE_T355301105_H
#ifndef APPLESTOREIMPL_T1049492593_H
#define APPLESTOREIMPL_T1049492593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl
struct  AppleStoreImpl_t1049492593  : public JSONStore_t1587640366
{
public:
	// System.Action`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.AppleStoreImpl::m_DeferredCallback
	Action_1_t3416877654 * ___m_DeferredCallback_3;
	// System.Action UnityEngine.Purchasing.AppleStoreImpl::m_RefreshReceiptError
	Action_t1264377477 * ___m_RefreshReceiptError_4;
	// System.Action`1<System.String> UnityEngine.Purchasing.AppleStoreImpl::m_RefreshReceiptSuccess
	Action_1_t2019918284 * ___m_RefreshReceiptSuccess_5;
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.AppleStoreImpl::m_RestoreCallback
	Action_1_t269755560 * ___m_RestoreCallback_6;
	// UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.AppleStoreImpl::m_Native
	RuntimeObject* ___m_Native_7;

public:
	inline static int32_t get_offset_of_m_DeferredCallback_3() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1049492593, ___m_DeferredCallback_3)); }
	inline Action_1_t3416877654 * get_m_DeferredCallback_3() const { return ___m_DeferredCallback_3; }
	inline Action_1_t3416877654 ** get_address_of_m_DeferredCallback_3() { return &___m_DeferredCallback_3; }
	inline void set_m_DeferredCallback_3(Action_1_t3416877654 * value)
	{
		___m_DeferredCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DeferredCallback_3), value);
	}

	inline static int32_t get_offset_of_m_RefreshReceiptError_4() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1049492593, ___m_RefreshReceiptError_4)); }
	inline Action_t1264377477 * get_m_RefreshReceiptError_4() const { return ___m_RefreshReceiptError_4; }
	inline Action_t1264377477 ** get_address_of_m_RefreshReceiptError_4() { return &___m_RefreshReceiptError_4; }
	inline void set_m_RefreshReceiptError_4(Action_t1264377477 * value)
	{
		___m_RefreshReceiptError_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefreshReceiptError_4), value);
	}

	inline static int32_t get_offset_of_m_RefreshReceiptSuccess_5() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1049492593, ___m_RefreshReceiptSuccess_5)); }
	inline Action_1_t2019918284 * get_m_RefreshReceiptSuccess_5() const { return ___m_RefreshReceiptSuccess_5; }
	inline Action_1_t2019918284 ** get_address_of_m_RefreshReceiptSuccess_5() { return &___m_RefreshReceiptSuccess_5; }
	inline void set_m_RefreshReceiptSuccess_5(Action_1_t2019918284 * value)
	{
		___m_RefreshReceiptSuccess_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefreshReceiptSuccess_5), value);
	}

	inline static int32_t get_offset_of_m_RestoreCallback_6() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1049492593, ___m_RestoreCallback_6)); }
	inline Action_1_t269755560 * get_m_RestoreCallback_6() const { return ___m_RestoreCallback_6; }
	inline Action_1_t269755560 ** get_address_of_m_RestoreCallback_6() { return &___m_RestoreCallback_6; }
	inline void set_m_RestoreCallback_6(Action_1_t269755560 * value)
	{
		___m_RestoreCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RestoreCallback_6), value);
	}

	inline static int32_t get_offset_of_m_Native_7() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1049492593, ___m_Native_7)); }
	inline RuntimeObject* get_m_Native_7() const { return ___m_Native_7; }
	inline RuntimeObject** get_address_of_m_Native_7() { return &___m_Native_7; }
	inline void set_m_Native_7(RuntimeObject* value)
	{
		___m_Native_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Native_7), value);
	}
};

struct AppleStoreImpl_t1049492593_StaticFields
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.AppleStoreImpl::util
	RuntimeObject* ___util_8;
	// UnityEngine.Purchasing.AppleStoreImpl UnityEngine.Purchasing.AppleStoreImpl::instance
	AppleStoreImpl_t1049492593 * ___instance_9;

public:
	inline static int32_t get_offset_of_util_8() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1049492593_StaticFields, ___util_8)); }
	inline RuntimeObject* get_util_8() const { return ___util_8; }
	inline RuntimeObject** get_address_of_util_8() { return &___util_8; }
	inline void set_util_8(RuntimeObject* value)
	{
		___util_8 = value;
		Il2CppCodeGenWriteBarrier((&___util_8), value);
	}

	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1049492593_StaticFields, ___instance_9)); }
	inline AppleStoreImpl_t1049492593 * get_instance_9() const { return ___instance_9; }
	inline AppleStoreImpl_t1049492593 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(AppleStoreImpl_t1049492593 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___instance_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLESTOREIMPL_T1049492593_H
#ifndef UNITYCHANNELIMPL_T3062682360_H
#define UNITYCHANNELIMPL_T3062682360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl
struct  UnityChannelImpl_t3062682360  : public JSONStore_t1587640366
{
public:
	// UnityEngine.Purchasing.INativeUnityChannelStore UnityEngine.Purchasing.UnityChannelImpl::m_Bindings
	RuntimeObject* ___m_Bindings_3;
	// System.String UnityEngine.Purchasing.UnityChannelImpl::m_LastPurchaseError
	String_t* ___m_LastPurchaseError_4;
	// System.Boolean UnityEngine.Purchasing.UnityChannelImpl::<fetchReceiptPayloadOnPurchase>k__BackingField
	bool ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_m_Bindings_3() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t3062682360, ___m_Bindings_3)); }
	inline RuntimeObject* get_m_Bindings_3() const { return ___m_Bindings_3; }
	inline RuntimeObject** get_address_of_m_Bindings_3() { return &___m_Bindings_3; }
	inline void set_m_Bindings_3(RuntimeObject* value)
	{
		___m_Bindings_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bindings_3), value);
	}

	inline static int32_t get_offset_of_m_LastPurchaseError_4() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t3062682360, ___m_LastPurchaseError_4)); }
	inline String_t* get_m_LastPurchaseError_4() const { return ___m_LastPurchaseError_4; }
	inline String_t** get_address_of_m_LastPurchaseError_4() { return &___m_LastPurchaseError_4; }
	inline void set_m_LastPurchaseError_4(String_t* value)
	{
		___m_LastPurchaseError_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastPurchaseError_4), value);
	}

	inline static int32_t get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t3062682360, ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5)); }
	inline bool get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5() const { return ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5() { return &___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5; }
	inline void set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5(bool value)
	{
		___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELIMPL_T3062682360_H
#ifndef PRICE_T1857690312_H
#define PRICE_T1857690312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Price
struct  Price_t1857690312  : public RuntimeObject
{
public:
	// System.Decimal UnityEngine.Purchasing.Price::value
	Decimal_t2948259380  ___value_0;
	// System.Int32[] UnityEngine.Purchasing.Price::data
	Int32U5BU5D_t385246372* ___data_1;
	// System.Double UnityEngine.Purchasing.Price::num
	double ___num_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Price_t1857690312, ___value_0)); }
	inline Decimal_t2948259380  get_value_0() const { return ___value_0; }
	inline Decimal_t2948259380 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Decimal_t2948259380  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Price_t1857690312, ___data_1)); }
	inline Int32U5BU5D_t385246372* get_data_1() const { return ___data_1; }
	inline Int32U5BU5D_t385246372** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(Int32U5BU5D_t385246372* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_num_2() { return static_cast<int32_t>(offsetof(Price_t1857690312, ___num_2)); }
	inline double get_num_2() const { return ___num_2; }
	inline double* get_address_of_num_2() { return &___num_2; }
	inline void set_num_2(double value)
	{
		___num_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRICE_T1857690312_H
#ifndef TRANSLATIONLOCALE_T265313191_H
#define TRANSLATIONLOCALE_T265313191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TranslationLocale
struct  TranslationLocale_t265313191 
{
public:
	// System.Int32 UnityEngine.Purchasing.TranslationLocale::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TranslationLocale_t265313191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONLOCALE_T265313191_H
#ifndef FACEBOOKSTOREIMPL_T2480281949_H
#define FACEBOOKSTOREIMPL_T2480281949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FacebookStoreImpl
struct  FacebookStoreImpl_t2480281949  : public JSONStore_t1587640366
{
public:
	// UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.FacebookStoreImpl::m_Native
	RuntimeObject* ___m_Native_3;

public:
	inline static int32_t get_offset_of_m_Native_3() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t2480281949, ___m_Native_3)); }
	inline RuntimeObject* get_m_Native_3() const { return ___m_Native_3; }
	inline RuntimeObject** get_address_of_m_Native_3() { return &___m_Native_3; }
	inline void set_m_Native_3(RuntimeObject* value)
	{
		___m_Native_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Native_3), value);
	}
};

struct FacebookStoreImpl_t2480281949_StaticFields
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.FacebookStoreImpl::util
	RuntimeObject* ___util_4;
	// UnityEngine.Purchasing.FacebookStoreImpl UnityEngine.Purchasing.FacebookStoreImpl::instance
	FacebookStoreImpl_t2480281949 * ___instance_5;

public:
	inline static int32_t get_offset_of_util_4() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t2480281949_StaticFields, ___util_4)); }
	inline RuntimeObject* get_util_4() const { return ___util_4; }
	inline RuntimeObject** get_address_of_util_4() { return &___util_4; }
	inline void set_util_4(RuntimeObject* value)
	{
		___util_4 = value;
		Il2CppCodeGenWriteBarrier((&___util_4), value);
	}

	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t2480281949_StaticFields, ___instance_5)); }
	inline FacebookStoreImpl_t2480281949 * get_instance_5() const { return ___instance_5; }
	inline FacebookStoreImpl_t2480281949 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(FacebookStoreImpl_t2480281949 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSTOREIMPL_T2480281949_H
#ifndef SAMSUNGAPPSMODE_T3466750770_H
#define SAMSUNGAPPSMODE_T3466750770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SamsungAppsMode
struct  SamsungAppsMode_t3466750770 
{
public:
	// System.Int32 UnityEngine.Purchasing.SamsungAppsMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SamsungAppsMode_t3466750770, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMSUNGAPPSMODE_T3466750770_H
#ifndef TIZENSTOREIMPL_T2691530403_H
#define TIZENSTOREIMPL_T2691530403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TizenStoreImpl
struct  TizenStoreImpl_t2691530403  : public JSONStore_t1587640366
{
public:
	// UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.TizenStoreImpl::m_Native
	RuntimeObject* ___m_Native_4;

public:
	inline static int32_t get_offset_of_m_Native_4() { return static_cast<int32_t>(offsetof(TizenStoreImpl_t2691530403, ___m_Native_4)); }
	inline RuntimeObject* get_m_Native_4() const { return ___m_Native_4; }
	inline RuntimeObject** get_address_of_m_Native_4() { return &___m_Native_4; }
	inline void set_m_Native_4(RuntimeObject* value)
	{
		___m_Native_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Native_4), value);
	}
};

struct TizenStoreImpl_t2691530403_StaticFields
{
public:
	// UnityEngine.Purchasing.TizenStoreImpl UnityEngine.Purchasing.TizenStoreImpl::instance
	TizenStoreImpl_t2691530403 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(TizenStoreImpl_t2691530403_StaticFields, ___instance_3)); }
	inline TizenStoreImpl_t2691530403 * get_instance_3() const { return ___instance_3; }
	inline TizenStoreImpl_t2691530403 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(TizenStoreImpl_t2691530403 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIZENSTOREIMPL_T2691530403_H
#ifndef PRODUCTCATALOGPAYOUTTYPE_T1381797773_H
#define PRODUCTCATALOGPAYOUTTYPE_T1381797773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogPayout/ProductCatalogPayoutType
struct  ProductCatalogPayoutType_t1381797773 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductCatalogPayout/ProductCatalogPayoutType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProductCatalogPayoutType_t1381797773, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGPAYOUTTYPE_T1381797773_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef PRODUCTCATALOGITEM_T2141417634_H
#define PRODUCTCATALOGITEM_T2141417634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogItem
struct  ProductCatalogItem_t2141417634  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductCatalogItem::id
	String_t* ___id_0;
	// UnityEngine.Purchasing.ProductType UnityEngine.Purchasing.ProductCatalogItem::type
	int32_t ___type_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.StoreID> UnityEngine.Purchasing.ProductCatalogItem::storeIDs
	List_1_t2166831371 * ___storeIDs_2;
	// UnityEngine.Purchasing.LocalizedProductDescription UnityEngine.Purchasing.ProductCatalogItem::defaultDescription
	LocalizedProductDescription_t1808411718 * ___defaultDescription_3;
	// System.Int32 UnityEngine.Purchasing.ProductCatalogItem::applePriceTier
	int32_t ___applePriceTier_4;
	// System.Int32 UnityEngine.Purchasing.ProductCatalogItem::xiaomiPriceTier
	int32_t ___xiaomiPriceTier_5;
	// UnityEngine.Purchasing.Price UnityEngine.Purchasing.ProductCatalogItem::googlePrice
	Price_t1857690312 * ___googlePrice_6;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.LocalizedProductDescription> UnityEngine.Purchasing.ProductCatalogItem::descriptions
	List_1_t3280486460 * ___descriptions_7;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogPayout> UnityEngine.Purchasing.ProductCatalogItem::payouts
	List_1_t2396426280 * ___payouts_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_storeIDs_2() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___storeIDs_2)); }
	inline List_1_t2166831371 * get_storeIDs_2() const { return ___storeIDs_2; }
	inline List_1_t2166831371 ** get_address_of_storeIDs_2() { return &___storeIDs_2; }
	inline void set_storeIDs_2(List_1_t2166831371 * value)
	{
		___storeIDs_2 = value;
		Il2CppCodeGenWriteBarrier((&___storeIDs_2), value);
	}

	inline static int32_t get_offset_of_defaultDescription_3() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___defaultDescription_3)); }
	inline LocalizedProductDescription_t1808411718 * get_defaultDescription_3() const { return ___defaultDescription_3; }
	inline LocalizedProductDescription_t1808411718 ** get_address_of_defaultDescription_3() { return &___defaultDescription_3; }
	inline void set_defaultDescription_3(LocalizedProductDescription_t1808411718 * value)
	{
		___defaultDescription_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDescription_3), value);
	}

	inline static int32_t get_offset_of_applePriceTier_4() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___applePriceTier_4)); }
	inline int32_t get_applePriceTier_4() const { return ___applePriceTier_4; }
	inline int32_t* get_address_of_applePriceTier_4() { return &___applePriceTier_4; }
	inline void set_applePriceTier_4(int32_t value)
	{
		___applePriceTier_4 = value;
	}

	inline static int32_t get_offset_of_xiaomiPriceTier_5() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___xiaomiPriceTier_5)); }
	inline int32_t get_xiaomiPriceTier_5() const { return ___xiaomiPriceTier_5; }
	inline int32_t* get_address_of_xiaomiPriceTier_5() { return &___xiaomiPriceTier_5; }
	inline void set_xiaomiPriceTier_5(int32_t value)
	{
		___xiaomiPriceTier_5 = value;
	}

	inline static int32_t get_offset_of_googlePrice_6() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___googlePrice_6)); }
	inline Price_t1857690312 * get_googlePrice_6() const { return ___googlePrice_6; }
	inline Price_t1857690312 ** get_address_of_googlePrice_6() { return &___googlePrice_6; }
	inline void set_googlePrice_6(Price_t1857690312 * value)
	{
		___googlePrice_6 = value;
		Il2CppCodeGenWriteBarrier((&___googlePrice_6), value);
	}

	inline static int32_t get_offset_of_descriptions_7() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___descriptions_7)); }
	inline List_1_t3280486460 * get_descriptions_7() const { return ___descriptions_7; }
	inline List_1_t3280486460 ** get_address_of_descriptions_7() { return &___descriptions_7; }
	inline void set_descriptions_7(List_1_t3280486460 * value)
	{
		___descriptions_7 = value;
		Il2CppCodeGenWriteBarrier((&___descriptions_7), value);
	}

	inline static int32_t get_offset_of_payouts_8() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t2141417634, ___payouts_8)); }
	inline List_1_t2396426280 * get_payouts_8() const { return ___payouts_8; }
	inline List_1_t2396426280 ** get_address_of_payouts_8() { return &___payouts_8; }
	inline void set_payouts_8(List_1_t2396426280 * value)
	{
		___payouts_8 = value;
		Il2CppCodeGenWriteBarrier((&___payouts_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGITEM_T2141417634_H
#ifndef SAMSUNGAPPSSTOREEXTENSIONS_T3605433812_H
#define SAMSUNGAPPSSTOREEXTENSIONS_T3605433812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SamsungAppsStoreExtensions
struct  SamsungAppsStoreExtensions_t3605433812  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.SamsungAppsStoreExtensions::m_RestoreCallback
	Action_1_t269755560 * ___m_RestoreCallback_4;
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.SamsungAppsStoreExtensions::m_Java
	AndroidJavaObject_t4131667876 * ___m_Java_5;

public:
	inline static int32_t get_offset_of_m_RestoreCallback_4() { return static_cast<int32_t>(offsetof(SamsungAppsStoreExtensions_t3605433812, ___m_RestoreCallback_4)); }
	inline Action_1_t269755560 * get_m_RestoreCallback_4() const { return ___m_RestoreCallback_4; }
	inline Action_1_t269755560 ** get_address_of_m_RestoreCallback_4() { return &___m_RestoreCallback_4; }
	inline void set_m_RestoreCallback_4(Action_1_t269755560 * value)
	{
		___m_RestoreCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RestoreCallback_4), value);
	}

	inline static int32_t get_offset_of_m_Java_5() { return static_cast<int32_t>(offsetof(SamsungAppsStoreExtensions_t3605433812, ___m_Java_5)); }
	inline AndroidJavaObject_t4131667876 * get_m_Java_5() const { return ___m_Java_5; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_m_Java_5() { return &___m_Java_5; }
	inline void set_m_Java_5(AndroidJavaObject_t4131667876 * value)
	{
		___m_Java_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Java_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMSUNGAPPSSTOREEXTENSIONS_T3605433812_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T1127161168_H
#define U3CU3EC__DISPLAYCLASS21_0_T1127161168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogItem/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t1127161168  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.TranslationLocale UnityEngine.Purchasing.ProductCatalogItem/<>c__DisplayClass21_0::locale
	int32_t ___locale_0;

public:
	inline static int32_t get_offset_of_locale_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t1127161168, ___locale_0)); }
	inline int32_t get_locale_0() const { return ___locale_0; }
	inline int32_t* get_address_of_locale_0() { return &___locale_0; }
	inline void set_locale_0(int32_t value)
	{
		___locale_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T1127161168_H
#ifndef STANDARDPURCHASINGMODULE_T2580735509_H
#define STANDARDPURCHASINGMODULE_T2580735509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StandardPurchasingModule
struct  StandardPurchasingModule_t2580735509  : public AbstractPurchasingModule_t2882497868
{
public:
	// UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StandardPurchasingModule::m_AppStorePlatform
	int32_t ___m_AppStorePlatform_1;
	// UnityEngine.Purchasing.INativeStoreProvider UnityEngine.Purchasing.StandardPurchasingModule::m_NativeStoreProvider
	RuntimeObject* ___m_NativeStoreProvider_2;
	// UnityEngine.RuntimePlatform UnityEngine.Purchasing.StandardPurchasingModule::m_RuntimePlatform
	int32_t ___m_RuntimePlatform_3;
	// Uniject.IUtil UnityEngine.Purchasing.StandardPurchasingModule::m_Util
	RuntimeObject* ___m_Util_4;
	// UnityEngine.ILogger UnityEngine.Purchasing.StandardPurchasingModule::m_Logger
	RuntimeObject* ___m_Logger_5;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::m_UseCloudCatalog
	bool ___m_UseCloudCatalog_6;
	// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::m_StoreInstance
	StoreInstance_t2416643455 * ___m_StoreInstance_8;
	// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.StandardPurchasingModule::m_CloudCatalog
	CloudCatalogImpl_t1580312503 * ___m_CloudCatalog_10;
	// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreUIMode>k__BackingField
	int32_t ___U3CuseFakeStoreUIModeU3Ek__BackingField_11;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreAlways>k__BackingField
	bool ___U3CuseFakeStoreAlwaysU3Ek__BackingField_12;
	// UnityEngine.Purchasing.WinRTStore UnityEngine.Purchasing.StandardPurchasingModule::windowsStore
	WinRTStore_t2015085940 * ___windowsStore_13;

public:
	inline static int32_t get_offset_of_m_AppStorePlatform_1() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_AppStorePlatform_1)); }
	inline int32_t get_m_AppStorePlatform_1() const { return ___m_AppStorePlatform_1; }
	inline int32_t* get_address_of_m_AppStorePlatform_1() { return &___m_AppStorePlatform_1; }
	inline void set_m_AppStorePlatform_1(int32_t value)
	{
		___m_AppStorePlatform_1 = value;
	}

	inline static int32_t get_offset_of_m_NativeStoreProvider_2() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_NativeStoreProvider_2)); }
	inline RuntimeObject* get_m_NativeStoreProvider_2() const { return ___m_NativeStoreProvider_2; }
	inline RuntimeObject** get_address_of_m_NativeStoreProvider_2() { return &___m_NativeStoreProvider_2; }
	inline void set_m_NativeStoreProvider_2(RuntimeObject* value)
	{
		___m_NativeStoreProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeStoreProvider_2), value);
	}

	inline static int32_t get_offset_of_m_RuntimePlatform_3() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_RuntimePlatform_3)); }
	inline int32_t get_m_RuntimePlatform_3() const { return ___m_RuntimePlatform_3; }
	inline int32_t* get_address_of_m_RuntimePlatform_3() { return &___m_RuntimePlatform_3; }
	inline void set_m_RuntimePlatform_3(int32_t value)
	{
		___m_RuntimePlatform_3 = value;
	}

	inline static int32_t get_offset_of_m_Util_4() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_Util_4)); }
	inline RuntimeObject* get_m_Util_4() const { return ___m_Util_4; }
	inline RuntimeObject** get_address_of_m_Util_4() { return &___m_Util_4; }
	inline void set_m_Util_4(RuntimeObject* value)
	{
		___m_Util_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Util_4), value);
	}

	inline static int32_t get_offset_of_m_Logger_5() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_Logger_5)); }
	inline RuntimeObject* get_m_Logger_5() const { return ___m_Logger_5; }
	inline RuntimeObject** get_address_of_m_Logger_5() { return &___m_Logger_5; }
	inline void set_m_Logger_5(RuntimeObject* value)
	{
		___m_Logger_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_5), value);
	}

	inline static int32_t get_offset_of_m_UseCloudCatalog_6() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_UseCloudCatalog_6)); }
	inline bool get_m_UseCloudCatalog_6() const { return ___m_UseCloudCatalog_6; }
	inline bool* get_address_of_m_UseCloudCatalog_6() { return &___m_UseCloudCatalog_6; }
	inline void set_m_UseCloudCatalog_6(bool value)
	{
		___m_UseCloudCatalog_6 = value;
	}

	inline static int32_t get_offset_of_m_StoreInstance_8() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_StoreInstance_8)); }
	inline StoreInstance_t2416643455 * get_m_StoreInstance_8() const { return ___m_StoreInstance_8; }
	inline StoreInstance_t2416643455 ** get_address_of_m_StoreInstance_8() { return &___m_StoreInstance_8; }
	inline void set_m_StoreInstance_8(StoreInstance_t2416643455 * value)
	{
		___m_StoreInstance_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreInstance_8), value);
	}

	inline static int32_t get_offset_of_m_CloudCatalog_10() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_CloudCatalog_10)); }
	inline CloudCatalogImpl_t1580312503 * get_m_CloudCatalog_10() const { return ___m_CloudCatalog_10; }
	inline CloudCatalogImpl_t1580312503 ** get_address_of_m_CloudCatalog_10() { return &___m_CloudCatalog_10; }
	inline void set_m_CloudCatalog_10(CloudCatalogImpl_t1580312503 * value)
	{
		___m_CloudCatalog_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CloudCatalog_10), value);
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CuseFakeStoreUIModeU3Ek__BackingField_11)); }
	inline int32_t get_U3CuseFakeStoreUIModeU3Ek__BackingField_11() const { return ___U3CuseFakeStoreUIModeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CuseFakeStoreUIModeU3Ek__BackingField_11() { return &___U3CuseFakeStoreUIModeU3Ek__BackingField_11; }
	inline void set_U3CuseFakeStoreUIModeU3Ek__BackingField_11(int32_t value)
	{
		___U3CuseFakeStoreUIModeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CuseFakeStoreAlwaysU3Ek__BackingField_12)); }
	inline bool get_U3CuseFakeStoreAlwaysU3Ek__BackingField_12() const { return ___U3CuseFakeStoreAlwaysU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_12() { return &___U3CuseFakeStoreAlwaysU3Ek__BackingField_12; }
	inline void set_U3CuseFakeStoreAlwaysU3Ek__BackingField_12(bool value)
	{
		___U3CuseFakeStoreAlwaysU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_windowsStore_13() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___windowsStore_13)); }
	inline WinRTStore_t2015085940 * get_windowsStore_13() const { return ___windowsStore_13; }
	inline WinRTStore_t2015085940 ** get_address_of_windowsStore_13() { return &___windowsStore_13; }
	inline void set_windowsStore_13(WinRTStore_t2015085940 * value)
	{
		___windowsStore_13 = value;
		Il2CppCodeGenWriteBarrier((&___windowsStore_13), value);
	}
};

struct StandardPurchasingModule_t2580735509_StaticFields
{
public:
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::ModuleInstance
	StandardPurchasingModule_t2580735509 * ___ModuleInstance_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String> UnityEngine.Purchasing.StandardPurchasingModule::AndroidStoreNameMap
	Dictionary_2_t2070795836 * ___AndroidStoreNameMap_9;

public:
	inline static int32_t get_offset_of_ModuleInstance_7() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509_StaticFields, ___ModuleInstance_7)); }
	inline StandardPurchasingModule_t2580735509 * get_ModuleInstance_7() const { return ___ModuleInstance_7; }
	inline StandardPurchasingModule_t2580735509 ** get_address_of_ModuleInstance_7() { return &___ModuleInstance_7; }
	inline void set_ModuleInstance_7(StandardPurchasingModule_t2580735509 * value)
	{
		___ModuleInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___ModuleInstance_7), value);
	}

	inline static int32_t get_offset_of_AndroidStoreNameMap_9() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509_StaticFields, ___AndroidStoreNameMap_9)); }
	inline Dictionary_2_t2070795836 * get_AndroidStoreNameMap_9() const { return ___AndroidStoreNameMap_9; }
	inline Dictionary_2_t2070795836 ** get_address_of_AndroidStoreNameMap_9() { return &___AndroidStoreNameMap_9; }
	inline void set_AndroidStoreNameMap_9(Dictionary_2_t2070795836 * value)
	{
		___AndroidStoreNameMap_9 = value;
		Il2CppCodeGenWriteBarrier((&___AndroidStoreNameMap_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDPURCHASINGMODULE_T2580735509_H
#ifndef STORECONFIGURATION_T1737855245_H
#define STORECONFIGURATION_T1737855245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreConfiguration
struct  StoreConfiguration_t1737855245  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StoreConfiguration::<androidStore>k__BackingField
	int32_t ___U3CandroidStoreU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CandroidStoreU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StoreConfiguration_t1737855245, ___U3CandroidStoreU3Ek__BackingField_0)); }
	inline int32_t get_U3CandroidStoreU3Ek__BackingField_0() const { return ___U3CandroidStoreU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CandroidStoreU3Ek__BackingField_0() { return &___U3CandroidStoreU3Ek__BackingField_0; }
	inline void set_U3CandroidStoreU3Ek__BackingField_0(int32_t value)
	{
		___U3CandroidStoreU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORECONFIGURATION_T1737855245_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef FAKESTORE_T3710170489_H
#define FAKESTORE_T3710170489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore
struct  FakeStore_t3710170489  : public JSONStore_t1587640366
{
public:
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.FakeStore::m_Biller
	RuntimeObject* ___m_Biller_3;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Purchasing.FakeStore::m_PurchasedProducts
	List_1_t3319525431 * ___m_PurchasedProducts_4;
	// System.Boolean UnityEngine.Purchasing.FakeStore::purchaseCalled
	bool ___purchaseCalled_5;
	// System.String UnityEngine.Purchasing.FakeStore::<unavailableProductId>k__BackingField
	String_t* ___U3CunavailableProductIdU3Ek__BackingField_6;
	// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.FakeStore::UIMode
	int32_t ___UIMode_7;

public:
	inline static int32_t get_offset_of_m_Biller_3() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___m_Biller_3)); }
	inline RuntimeObject* get_m_Biller_3() const { return ___m_Biller_3; }
	inline RuntimeObject** get_address_of_m_Biller_3() { return &___m_Biller_3; }
	inline void set_m_Biller_3(RuntimeObject* value)
	{
		___m_Biller_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Biller_3), value);
	}

	inline static int32_t get_offset_of_m_PurchasedProducts_4() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___m_PurchasedProducts_4)); }
	inline List_1_t3319525431 * get_m_PurchasedProducts_4() const { return ___m_PurchasedProducts_4; }
	inline List_1_t3319525431 ** get_address_of_m_PurchasedProducts_4() { return &___m_PurchasedProducts_4; }
	inline void set_m_PurchasedProducts_4(List_1_t3319525431 * value)
	{
		___m_PurchasedProducts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchasedProducts_4), value);
	}

	inline static int32_t get_offset_of_purchaseCalled_5() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___purchaseCalled_5)); }
	inline bool get_purchaseCalled_5() const { return ___purchaseCalled_5; }
	inline bool* get_address_of_purchaseCalled_5() { return &___purchaseCalled_5; }
	inline void set_purchaseCalled_5(bool value)
	{
		___purchaseCalled_5 = value;
	}

	inline static int32_t get_offset_of_U3CunavailableProductIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___U3CunavailableProductIdU3Ek__BackingField_6)); }
	inline String_t* get_U3CunavailableProductIdU3Ek__BackingField_6() const { return ___U3CunavailableProductIdU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CunavailableProductIdU3Ek__BackingField_6() { return &___U3CunavailableProductIdU3Ek__BackingField_6; }
	inline void set_U3CunavailableProductIdU3Ek__BackingField_6(String_t* value)
	{
		___U3CunavailableProductIdU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunavailableProductIdU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_UIMode_7() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___UIMode_7)); }
	inline int32_t get_UIMode_7() const { return ___UIMode_7; }
	inline int32_t* get_address_of_UIMode_7() { return &___UIMode_7; }
	inline void set_UIMode_7(int32_t value)
	{
		___UIMode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESTORE_T3710170489_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef LOCALIZEDPRODUCTDESCRIPTION_T1808411718_H
#define LOCALIZEDPRODUCTDESCRIPTION_T1808411718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.LocalizedProductDescription
struct  LocalizedProductDescription_t1808411718  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.TranslationLocale UnityEngine.Purchasing.LocalizedProductDescription::googleLocale
	int32_t ___googleLocale_0;
	// System.String UnityEngine.Purchasing.LocalizedProductDescription::title
	String_t* ___title_1;
	// System.String UnityEngine.Purchasing.LocalizedProductDescription::description
	String_t* ___description_2;

public:
	inline static int32_t get_offset_of_googleLocale_0() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t1808411718, ___googleLocale_0)); }
	inline int32_t get_googleLocale_0() const { return ___googleLocale_0; }
	inline int32_t* get_address_of_googleLocale_0() { return &___googleLocale_0; }
	inline void set_googleLocale_0(int32_t value)
	{
		___googleLocale_0 = value;
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t1808411718, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t1808411718, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDPRODUCTDESCRIPTION_T1808411718_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T3017501136_H
#define U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T3017501136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45
struct  U3CRestoreTransactionIDProcessU3Ed__45_t3017501136  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::result
	Action_1_t1412811613 * ___result_2;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<customID>5__1
	String_t* ___U3CcustomIDU3E5__1_4;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<wf>5__2
	WWWForm_t4064702195 * ___U3CwfU3E5__2_5;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<now>5__3
	DateTime_t3738529785  ___U3CnowU3E5__3_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<endDate>5__4
	String_t* ___U3CendDateU3E5__4_7;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<upperWeek>5__5
	DateTime_t3738529785  ___U3CupperWeekU3E5__5_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<startDate>5__6
	String_t* ___U3CstartDateU3E5__6_9;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<sign>5__7
	String_t* ___U3CsignU3E5__7_10;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<w>5__8
	WWW_t3688466362 * ___U3CwU3E5__8_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreObjects>5__9
	Dictionary_2_t2865362463 * ___U3CrestoreObjectsU3E5__9_12;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<code>5__10
	String_t* ___U3CcodeU3E5__10_13;
	// System.Collections.Generic.List`1<System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreValues>5__11
	List_1_t257213610 * ___U3CrestoreValuesU3E5__11_14;
	// System.Collections.Generic.List`1/Enumerator<System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>s__12
	Enumerator_t2146457487  ___U3CU3Es__12_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreObjectElem>5__13
	Dictionary_2_t2865362463 * ___U3CrestoreObjectElemU3E5__13_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<productId>5__14
	String_t* ___U3CproductIdU3E5__14_17;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<tradeSeq>5__15
	String_t* ___U3CtradeSeqU3E5__15_18;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<receipt>5__16
	String_t* ___U3CreceiptU3E5__16_19;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___result_2)); }
	inline Action_1_t1412811613 * get_result_2() const { return ___result_2; }
	inline Action_1_t1412811613 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(Action_1_t1412811613 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier((&___result_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3E4__this_3)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcustomIDU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CcustomIDU3E5__1_4)); }
	inline String_t* get_U3CcustomIDU3E5__1_4() const { return ___U3CcustomIDU3E5__1_4; }
	inline String_t** get_address_of_U3CcustomIDU3E5__1_4() { return &___U3CcustomIDU3E5__1_4; }
	inline void set_U3CcustomIDU3E5__1_4(String_t* value)
	{
		___U3CcustomIDU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomIDU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CwfU3E5__2_5)); }
	inline WWWForm_t4064702195 * get_U3CwfU3E5__2_5() const { return ___U3CwfU3E5__2_5; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwfU3E5__2_5() { return &___U3CwfU3E5__2_5; }
	inline void set_U3CwfU3E5__2_5(WWWForm_t4064702195 * value)
	{
		___U3CwfU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CnowU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CnowU3E5__3_6)); }
	inline DateTime_t3738529785  get_U3CnowU3E5__3_6() const { return ___U3CnowU3E5__3_6; }
	inline DateTime_t3738529785 * get_address_of_U3CnowU3E5__3_6() { return &___U3CnowU3E5__3_6; }
	inline void set_U3CnowU3E5__3_6(DateTime_t3738529785  value)
	{
		___U3CnowU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CendDateU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CendDateU3E5__4_7)); }
	inline String_t* get_U3CendDateU3E5__4_7() const { return ___U3CendDateU3E5__4_7; }
	inline String_t** get_address_of_U3CendDateU3E5__4_7() { return &___U3CendDateU3E5__4_7; }
	inline void set_U3CendDateU3E5__4_7(String_t* value)
	{
		___U3CendDateU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CendDateU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CupperWeekU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CupperWeekU3E5__5_8)); }
	inline DateTime_t3738529785  get_U3CupperWeekU3E5__5_8() const { return ___U3CupperWeekU3E5__5_8; }
	inline DateTime_t3738529785 * get_address_of_U3CupperWeekU3E5__5_8() { return &___U3CupperWeekU3E5__5_8; }
	inline void set_U3CupperWeekU3E5__5_8(DateTime_t3738529785  value)
	{
		___U3CupperWeekU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CstartDateU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CstartDateU3E5__6_9)); }
	inline String_t* get_U3CstartDateU3E5__6_9() const { return ___U3CstartDateU3E5__6_9; }
	inline String_t** get_address_of_U3CstartDateU3E5__6_9() { return &___U3CstartDateU3E5__6_9; }
	inline void set_U3CstartDateU3E5__6_9(String_t* value)
	{
		___U3CstartDateU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstartDateU3E5__6_9), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CsignU3E5__7_10)); }
	inline String_t* get_U3CsignU3E5__7_10() const { return ___U3CsignU3E5__7_10; }
	inline String_t** get_address_of_U3CsignU3E5__7_10() { return &___U3CsignU3E5__7_10; }
	inline void set_U3CsignU3E5__7_10(String_t* value)
	{
		___U3CsignU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__7_10), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CwU3E5__8_11)); }
	inline WWW_t3688466362 * get_U3CwU3E5__8_11() const { return ___U3CwU3E5__8_11; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E5__8_11() { return &___U3CwU3E5__8_11; }
	inline void set_U3CwU3E5__8_11(WWW_t3688466362 * value)
	{
		___U3CwU3E5__8_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__8_11), value);
	}

	inline static int32_t get_offset_of_U3CrestoreObjectsU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CrestoreObjectsU3E5__9_12)); }
	inline Dictionary_2_t2865362463 * get_U3CrestoreObjectsU3E5__9_12() const { return ___U3CrestoreObjectsU3E5__9_12; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CrestoreObjectsU3E5__9_12() { return &___U3CrestoreObjectsU3E5__9_12; }
	inline void set_U3CrestoreObjectsU3E5__9_12(Dictionary_2_t2865362463 * value)
	{
		___U3CrestoreObjectsU3E5__9_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreObjectsU3E5__9_12), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CcodeU3E5__10_13)); }
	inline String_t* get_U3CcodeU3E5__10_13() const { return ___U3CcodeU3E5__10_13; }
	inline String_t** get_address_of_U3CcodeU3E5__10_13() { return &___U3CcodeU3E5__10_13; }
	inline void set_U3CcodeU3E5__10_13(String_t* value)
	{
		___U3CcodeU3E5__10_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__10_13), value);
	}

	inline static int32_t get_offset_of_U3CrestoreValuesU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CrestoreValuesU3E5__11_14)); }
	inline List_1_t257213610 * get_U3CrestoreValuesU3E5__11_14() const { return ___U3CrestoreValuesU3E5__11_14; }
	inline List_1_t257213610 ** get_address_of_U3CrestoreValuesU3E5__11_14() { return &___U3CrestoreValuesU3E5__11_14; }
	inline void set_U3CrestoreValuesU3E5__11_14(List_1_t257213610 * value)
	{
		___U3CrestoreValuesU3E5__11_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreValuesU3E5__11_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__12_15() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3Es__12_15)); }
	inline Enumerator_t2146457487  get_U3CU3Es__12_15() const { return ___U3CU3Es__12_15; }
	inline Enumerator_t2146457487 * get_address_of_U3CU3Es__12_15() { return &___U3CU3Es__12_15; }
	inline void set_U3CU3Es__12_15(Enumerator_t2146457487  value)
	{
		___U3CU3Es__12_15 = value;
	}

	inline static int32_t get_offset_of_U3CrestoreObjectElemU3E5__13_16() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CrestoreObjectElemU3E5__13_16)); }
	inline Dictionary_2_t2865362463 * get_U3CrestoreObjectElemU3E5__13_16() const { return ___U3CrestoreObjectElemU3E5__13_16; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CrestoreObjectElemU3E5__13_16() { return &___U3CrestoreObjectElemU3E5__13_16; }
	inline void set_U3CrestoreObjectElemU3E5__13_16(Dictionary_2_t2865362463 * value)
	{
		___U3CrestoreObjectElemU3E5__13_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreObjectElemU3E5__13_16), value);
	}

	inline static int32_t get_offset_of_U3CproductIdU3E5__14_17() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CproductIdU3E5__14_17)); }
	inline String_t* get_U3CproductIdU3E5__14_17() const { return ___U3CproductIdU3E5__14_17; }
	inline String_t** get_address_of_U3CproductIdU3E5__14_17() { return &___U3CproductIdU3E5__14_17; }
	inline void set_U3CproductIdU3E5__14_17(String_t* value)
	{
		___U3CproductIdU3E5__14_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIdU3E5__14_17), value);
	}

	inline static int32_t get_offset_of_U3CtradeSeqU3E5__15_18() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CtradeSeqU3E5__15_18)); }
	inline String_t* get_U3CtradeSeqU3E5__15_18() const { return ___U3CtradeSeqU3E5__15_18; }
	inline String_t** get_address_of_U3CtradeSeqU3E5__15_18() { return &___U3CtradeSeqU3E5__15_18; }
	inline void set_U3CtradeSeqU3E5__15_18(String_t* value)
	{
		___U3CtradeSeqU3E5__15_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtradeSeqU3E5__15_18), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3E5__16_19() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CreceiptU3E5__16_19)); }
	inline String_t* get_U3CreceiptU3E5__16_19() const { return ___U3CreceiptU3E5__16_19; }
	inline String_t** get_address_of_U3CreceiptU3E5__16_19() { return &___U3CreceiptU3E5__16_19; }
	inline void set_U3CreceiptU3E5__16_19(String_t* value)
	{
		___U3CreceiptU3E5__16_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3E5__16_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T3017501136_H
#ifndef UIFAKESTORE_T4139165440_H
#define UIFAKESTORE_T4139165440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore
struct  UIFakeStore_t4139165440  : public FakeStore_t3710170489
{
public:
	// UnityEngine.Purchasing.UIFakeStore/DialogRequest UnityEngine.Purchasing.UIFakeStore::m_CurrentDialog
	DialogRequest_t599015159 * ___m_CurrentDialog_8;
	// System.Int32 UnityEngine.Purchasing.UIFakeStore::m_LastSelectedDropdownIndex
	int32_t ___m_LastSelectedDropdownIndex_9;
	// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::UIFakeStoreCanvasPrefab
	GameObject_t1113636619 * ___UIFakeStoreCanvasPrefab_10;
	// UnityEngine.Canvas UnityEngine.Purchasing.UIFakeStore::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::m_EventSystem
	GameObject_t1113636619 * ___m_EventSystem_12;
	// System.String UnityEngine.Purchasing.UIFakeStore::m_ParentGameObjectPath
	String_t* ___m_ParentGameObjectPath_13;

public:
	inline static int32_t get_offset_of_m_CurrentDialog_8() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_CurrentDialog_8)); }
	inline DialogRequest_t599015159 * get_m_CurrentDialog_8() const { return ___m_CurrentDialog_8; }
	inline DialogRequest_t599015159 ** get_address_of_m_CurrentDialog_8() { return &___m_CurrentDialog_8; }
	inline void set_m_CurrentDialog_8(DialogRequest_t599015159 * value)
	{
		___m_CurrentDialog_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentDialog_8), value);
	}

	inline static int32_t get_offset_of_m_LastSelectedDropdownIndex_9() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_LastSelectedDropdownIndex_9)); }
	inline int32_t get_m_LastSelectedDropdownIndex_9() const { return ___m_LastSelectedDropdownIndex_9; }
	inline int32_t* get_address_of_m_LastSelectedDropdownIndex_9() { return &___m_LastSelectedDropdownIndex_9; }
	inline void set_m_LastSelectedDropdownIndex_9(int32_t value)
	{
		___m_LastSelectedDropdownIndex_9 = value;
	}

	inline static int32_t get_offset_of_UIFakeStoreCanvasPrefab_10() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___UIFakeStoreCanvasPrefab_10)); }
	inline GameObject_t1113636619 * get_UIFakeStoreCanvasPrefab_10() const { return ___UIFakeStoreCanvasPrefab_10; }
	inline GameObject_t1113636619 ** get_address_of_UIFakeStoreCanvasPrefab_10() { return &___UIFakeStoreCanvasPrefab_10; }
	inline void set_UIFakeStoreCanvasPrefab_10(GameObject_t1113636619 * value)
	{
		___UIFakeStoreCanvasPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___UIFakeStoreCanvasPrefab_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_12() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_EventSystem_12)); }
	inline GameObject_t1113636619 * get_m_EventSystem_12() const { return ___m_EventSystem_12; }
	inline GameObject_t1113636619 ** get_address_of_m_EventSystem_12() { return &___m_EventSystem_12; }
	inline void set_m_EventSystem_12(GameObject_t1113636619 * value)
	{
		___m_EventSystem_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_12), value);
	}

	inline static int32_t get_offset_of_m_ParentGameObjectPath_13() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_ParentGameObjectPath_13)); }
	inline String_t* get_m_ParentGameObjectPath_13() const { return ___m_ParentGameObjectPath_13; }
	inline String_t** get_address_of_m_ParentGameObjectPath_13() { return &___m_ParentGameObjectPath_13; }
	inline void set_m_ParentGameObjectPath_13(String_t* value)
	{
		___m_ParentGameObjectPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentGameObjectPath_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFAKESTORE_T4139165440_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef LIFECYCLENOTIFIER_T3201457069_H
#define LIFECYCLENOTIFIER_T3201457069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier
struct  LifecycleNotifier_t3201457069  : public MonoBehaviour_t3962482529
{
public:
	// System.Action UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier::OnDestroyCallback
	Action_t1264377477 * ___OnDestroyCallback_2;

public:
	inline static int32_t get_offset_of_OnDestroyCallback_2() { return static_cast<int32_t>(offsetof(LifecycleNotifier_t3201457069, ___OnDestroyCallback_2)); }
	inline Action_t1264377477 * get_OnDestroyCallback_2() const { return ___OnDestroyCallback_2; }
	inline Action_t1264377477 ** get_address_of_OnDestroyCallback_2() { return &___OnDestroyCallback_2; }
	inline void set_OnDestroyCallback_2(Action_t1264377477 * value)
	{
		___OnDestroyCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIFECYCLENOTIFIER_T3201457069_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_2;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_3;

public:
	inline static int32_t get_offset_of_m_Trigger_2() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_2)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_2() const { return ___m_Trigger_2; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_2() { return &___m_Trigger_2; }
	inline void set_m_Trigger_2(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_2), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_3() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_3)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_3() const { return ___m_EventPayload_3; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_3() { return &___m_EventPayload_3; }
	inline void set_m_EventPayload_3(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_3;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_4;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_5;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_6;

public:
	inline static int32_t get_offset_of_m_EventName_2() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_2)); }
	inline String_t* get_m_EventName_2() const { return ___m_EventName_2; }
	inline String_t** get_address_of_m_EventName_2() { return &___m_EventName_2; }
	inline void set_m_EventName_2(String_t* value)
	{
		___m_EventName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_2), value);
	}

	inline static int32_t get_offset_of_m_Dict_3() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_3)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_3() const { return ___m_Dict_3; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_3() { return &___m_Dict_3; }
	inline void set_m_Dict_3(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_3), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_4)); }
	inline int32_t get_m_PrevDictHash_4() const { return ___m_PrevDictHash_4; }
	inline int32_t* get_address_of_m_PrevDictHash_4() { return &___m_PrevDictHash_4; }
	inline void set_m_PrevDictHash_4(int32_t value)
	{
		___m_PrevDictHash_4 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_5)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_5() const { return ___m_TrackableProperty_5; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_5() { return &___m_TrackableProperty_5; }
	inline void set_m_TrackableProperty_5(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_5), value);
	}

	inline static int32_t get_offset_of_m_Trigger_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_6)); }
	inline int32_t get_m_Trigger_6() const { return ___m_Trigger_6; }
	inline int32_t* get_address_of_m_Trigger_6() { return &___m_Trigger_6; }
	inline void set_m_Trigger_6(int32_t value)
	{
		___m_Trigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef UNITYUTIL_T103543446_H
#define UNITYUTIL_T103543446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.UnityUtil
struct  UnityUtil_t103543446  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> UnityEngine.Purchasing.Extension.UnityUtil::pauseListeners
	List_1_t1741830302 * ___pauseListeners_5;

public:
	inline static int32_t get_offset_of_pauseListeners_5() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446, ___pauseListeners_5)); }
	inline List_1_t1741830302 * get_pauseListeners_5() const { return ___pauseListeners_5; }
	inline List_1_t1741830302 ** get_address_of_pauseListeners_5() { return &___pauseListeners_5; }
	inline void set_pauseListeners_5(List_1_t1741830302 * value)
	{
		___pauseListeners_5 = value;
		Il2CppCodeGenWriteBarrier((&___pauseListeners_5), value);
	}
};

struct UnityUtil_t103543446_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Action> UnityEngine.Purchasing.Extension.UnityUtil::s_Callbacks
	List_1_t2736452219 * ___s_Callbacks_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.Purchasing.Extension.UnityUtil::s_CallbacksPending
	bool ___s_CallbacksPending_3;
	// System.Collections.Generic.List`1<UnityEngine.RuntimePlatform> UnityEngine.Purchasing.Extension.UnityUtil::s_PcControlledPlatforms
	List_1_t1336965349 * ___s_PcControlledPlatforms_4;

public:
	inline static int32_t get_offset_of_s_Callbacks_2() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446_StaticFields, ___s_Callbacks_2)); }
	inline List_1_t2736452219 * get_s_Callbacks_2() const { return ___s_Callbacks_2; }
	inline List_1_t2736452219 ** get_address_of_s_Callbacks_2() { return &___s_Callbacks_2; }
	inline void set_s_Callbacks_2(List_1_t2736452219 * value)
	{
		___s_Callbacks_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Callbacks_2), value);
	}

	inline static int32_t get_offset_of_s_CallbacksPending_3() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446_StaticFields, ___s_CallbacksPending_3)); }
	inline bool get_s_CallbacksPending_3() const { return ___s_CallbacksPending_3; }
	inline bool* get_address_of_s_CallbacksPending_3() { return &___s_CallbacksPending_3; }
	inline void set_s_CallbacksPending_3(bool value)
	{
		___s_CallbacksPending_3 = value;
	}

	inline static int32_t get_offset_of_s_PcControlledPlatforms_4() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446_StaticFields, ___s_PcControlledPlatforms_4)); }
	inline List_1_t1336965349 * get_s_PcControlledPlatforms_4() const { return ___s_PcControlledPlatforms_4; }
	inline List_1_t1336965349 ** get_address_of_s_PcControlledPlatforms_4() { return &___s_PcControlledPlatforms_4; }
	inline void set_s_PcControlledPlatforms_4(List_1_t1336965349 * value)
	{
		___s_PcControlledPlatforms_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PcControlledPlatforms_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYUTIL_T103543446_H
#ifndef ASYNCWEBUTIL_T2717304869_H
#define ASYNCWEBUTIL_T2717304869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil
struct  AsyncWebUtil_t2717304869  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCWEBUTIL_T2717304869_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (U3CRestoreTransactionIDProcessU3Ed__45_t3017501136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[20] = 
{
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3E1__state_0(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3E2__current_1(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_result_2(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3E4__this_3(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CcustomIDU3E5__1_4(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CwfU3E5__2_5(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CnowU3E5__3_6(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CendDateU3E5__4_7(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CupperWeekU3E5__5_8(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CstartDateU3E5__6_9(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CsignU3E5__7_10(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CwU3E5__8_11(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CrestoreObjectsU3E5__9_12(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CcodeU3E5__10_13(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CrestoreValuesU3E5__11_14(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3Es__12_15(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CrestoreObjectElemU3E5__13_16(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CproductIdU3E5__14_17(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CtradeSeqU3E5__15_18(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CreceiptU3E5__16_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CValidateReceiptProcessU3Ed__47_t372105418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[13] = 
{
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CU3E1__state_0(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CU3E2__current_1(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_transactionId_2(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_receipt_3(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_result_4(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CU3E4__this_5(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CtempJsonU3E5__1_6(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CwfU3E5__2_7(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CsignU3E5__3_8(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CwU3E5__4_9(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CjsonObjectsU3E5__5_10(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CcodeU3E5__6_11(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CmsgU3E5__7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (PayMethod_t328949237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (GooglePlayAndroidJavaStore_t3169878644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (FakeGooglePlayConfiguration_t3630139311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (FakeSamsungAppsExtensions_t2880024647), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (SamsungAppsMode_t3466750770)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2310[4] = 
{
	SamsungAppsMode_t3466750770::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (SamsungAppsStoreExtensions_t3605433812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[2] = 
{
	SamsungAppsStoreExtensions_t3605433812::get_offset_of_m_RestoreCallback_4(),
	SamsungAppsStoreExtensions_t3605433812::get_offset_of_m_Java_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (FakeUnityChannelConfiguration_t2647072263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[1] = 
{
	FakeUnityChannelConfiguration_t2647072263::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (FakeUnityChannelExtensions_t3596584646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (XiaomiPriceTiers_t2048704184), -1, sizeof(XiaomiPriceTiers_t2048704184_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2317[1] = 
{
	XiaomiPriceTiers_t2048704184_StaticFields::get_offset_of_XiaomiPriceTierPrices_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (UnityChannelBindings_t3184348469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[4] = 
{
	UnityChannelBindings_t3184348469::get_offset_of_m_PurchaseCallback_0(),
	UnityChannelBindings_t3184348469::get_offset_of_m_PurchaseGuid_1(),
	UnityChannelBindings_t3184348469::get_offset_of_m_ValidateCallbacks_2(),
	UnityChannelBindings_t3184348469::get_offset_of_m_PurchaseConfirmCallbacks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (U3CU3Ec__DisplayClass16_0_t777686714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[2] = 
{
	U3CU3Ec__DisplayClass16_0_t777686714::get_offset_of_transactionId_0(),
	U3CU3Ec__DisplayClass16_0_t777686714::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (UnityChannelImpl_t3062682360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	UnityChannelImpl_t3062682360::get_offset_of_m_Bindings_3(),
	UnityChannelImpl_t3062682360::get_offset_of_m_LastPurchaseError_4(),
	UnityChannelImpl_t3062682360::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (U3CU3Ec__DisplayClass7_0_t620144240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[2] = 
{
	U3CU3Ec__DisplayClass7_0_t620144240::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass7_0_t620144240::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (U3CU3Ec__DisplayClass7_1_t2576459376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[3] = 
{
	U3CU3Ec__DisplayClass7_1_t2576459376::get_offset_of_dic_0(),
	U3CU3Ec__DisplayClass7_1_t2576459376::get_offset_of_transactionId_1(),
	U3CU3Ec__DisplayClass7_1_t2576459376::get_offset_of_CSU24U3CU3E8__locals1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (UnityChannelPurchaseReceipt_t3342804115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[3] = 
{
	UnityChannelPurchaseReceipt_t3342804115::get_offset_of_storeSpecificId_0(),
	UnityChannelPurchaseReceipt_t3342804115::get_offset_of_transactionId_1(),
	UnityChannelPurchaseReceipt_t3342804115::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (AppleStoreImpl_t1049492593), -1, sizeof(AppleStoreImpl_t1049492593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2324[7] = 
{
	AppleStoreImpl_t1049492593::get_offset_of_m_DeferredCallback_3(),
	AppleStoreImpl_t1049492593::get_offset_of_m_RefreshReceiptError_4(),
	AppleStoreImpl_t1049492593::get_offset_of_m_RefreshReceiptSuccess_5(),
	AppleStoreImpl_t1049492593::get_offset_of_m_RestoreCallback_6(),
	AppleStoreImpl_t1049492593::get_offset_of_m_Native_7(),
	AppleStoreImpl_t1049492593_StaticFields::get_offset_of_util_8(),
	AppleStoreImpl_t1049492593_StaticFields::get_offset_of_instance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (U3CU3Ec__DisplayClass19_0_t1949579283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[1] = 
{
	U3CU3Ec__DisplayClass19_0_t1949579283::get_offset_of_productDescription_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (U3CU3Ec__DisplayClass28_0_t1547909138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[4] = 
{
	U3CU3Ec__DisplayClass28_0_t1547909138::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass28_0_t1547909138::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass28_0_t1547909138::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass28_0_t1547909138::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (FakeAppleConfiguation_t2415379217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (FakeAppleExtensions_t3685865150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (AppStore_t355301105)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2331[13] = 
{
	AppStore_t355301105::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (FakeMicrosoftExtensions_t4237934947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (WinRTStore_t2015085940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[5] = 
{
	WinRTStore_t2015085940::get_offset_of_win8_0(),
	WinRTStore_t2015085940::get_offset_of_callback_1(),
	WinRTStore_t2015085940::get_offset_of_util_2(),
	WinRTStore_t2015085940::get_offset_of_logger_3(),
	WinRTStore_t2015085940::get_offset_of_m_CanReceivePurchases_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (U3CU3Ec_t3746618910), -1, sizeof(U3CU3Ec_t3746618910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2336[3] = 
{
	U3CU3Ec_t3746618910_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3746618910_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t3746618910_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (TizenStoreImpl_t2691530403), -1, sizeof(TizenStoreImpl_t2691530403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2338[2] = 
{
	TizenStoreImpl_t2691530403_StaticFields::get_offset_of_instance_3(),
	TizenStoreImpl_t2691530403::get_offset_of_m_Native_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (FakeTizenStoreConfiguration_t714964994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (FacebookStoreImpl_t2480281949), -1, sizeof(FacebookStoreImpl_t2480281949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2340[3] = 
{
	FacebookStoreImpl_t2480281949::get_offset_of_m_Native_3(),
	FacebookStoreImpl_t2480281949_StaticFields::get_offset_of_util_4(),
	FacebookStoreImpl_t2480281949_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (U3CU3Ec__DisplayClass6_0_t1261164119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[4] = 
{
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (FakeStoreUIMode_t680685637)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2342[4] = 
{
	FakeStoreUIMode_t680685637::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (FakeStore_t3710170489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[5] = 
{
	FakeStore_t3710170489::get_offset_of_m_Biller_3(),
	FakeStore_t3710170489::get_offset_of_m_PurchasedProducts_4(),
	FakeStore_t3710170489::get_offset_of_purchaseCalled_5(),
	FakeStore_t3710170489::get_offset_of_U3CunavailableProductIdU3Ek__BackingField_6(),
	FakeStore_t3710170489::get_offset_of_UIMode_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (DialogType_t918222323)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2344[3] = 
{
	DialogType_t918222323::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (U3CU3Ec__DisplayClass13_0_t1153436473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[2] = 
{
	U3CU3Ec__DisplayClass13_0_t1153436473::get_offset_of_products_0(),
	U3CU3Ec__DisplayClass13_0_t1153436473::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (U3CU3Ec__DisplayClass15_0_t771099449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	U3CU3Ec__DisplayClass15_0_t771099449::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass15_0_t771099449::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (UIFakeStore_t4139165440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[6] = 
{
	UIFakeStore_t4139165440::get_offset_of_m_CurrentDialog_8(),
	UIFakeStore_t4139165440::get_offset_of_m_LastSelectedDropdownIndex_9(),
	UIFakeStore_t4139165440::get_offset_of_UIFakeStoreCanvasPrefab_10(),
	UIFakeStore_t4139165440::get_offset_of_m_Canvas_11(),
	UIFakeStore_t4139165440::get_offset_of_m_EventSystem_12(),
	UIFakeStore_t4139165440::get_offset_of_m_ParentGameObjectPath_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (DialogRequest_t599015159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[5] = 
{
	DialogRequest_t599015159::get_offset_of_QueryText_0(),
	DialogRequest_t599015159::get_offset_of_OkayButtonText_1(),
	DialogRequest_t599015159::get_offset_of_CancelButtonText_2(),
	DialogRequest_t599015159::get_offset_of_Options_3(),
	DialogRequest_t599015159::get_offset_of_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (LifecycleNotifier_t3201457069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[1] = 
{
	LifecycleNotifier_t3201457069::get_offset_of_OnDestroyCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (U3CU3Ec_t126346862), -1, sizeof(U3CU3Ec_t126346862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2352[2] = 
{
	U3CU3Ec_t126346862_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t126346862_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (AsyncWebUtil_t2717304869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (U3CDoInvokeU3Ed__2_t4183149284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[5] = 
{
	U3CDoInvokeU3Ed__2_t4183149284::get_offset_of_U3CU3E1__state_0(),
	U3CDoInvokeU3Ed__2_t4183149284::get_offset_of_U3CU3E2__current_1(),
	U3CDoInvokeU3Ed__2_t4183149284::get_offset_of_a_2(),
	U3CDoInvokeU3Ed__2_t4183149284::get_offset_of_delayInSeconds_3(),
	U3CDoInvokeU3Ed__2_t4183149284::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (U3CProcessU3Ed__3_t1355240730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[6] = 
{
	U3CProcessU3Ed__3_t1355240730::get_offset_of_U3CU3E1__state_0(),
	U3CProcessU3Ed__3_t1355240730::get_offset_of_U3CU3E2__current_1(),
	U3CProcessU3Ed__3_t1355240730::get_offset_of_request_2(),
	U3CProcessU3Ed__3_t1355240730::get_offset_of_responseHandler_3(),
	U3CProcessU3Ed__3_t1355240730::get_offset_of_errorHandler_4(),
	U3CProcessU3Ed__3_t1355240730::get_offset_of_U3CU3E4__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (CloudCatalogImpl_t1580312503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[7] = 
{
	CloudCatalogImpl_t1580312503::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogImpl_t1580312503::get_offset_of_m_CacheFileName_1(),
	CloudCatalogImpl_t1580312503::get_offset_of_m_Logger_2(),
	CloudCatalogImpl_t1580312503::get_offset_of_m_CatalogURL_3(),
	CloudCatalogImpl_t1580312503::get_offset_of_m_StoreName_4(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (U3CU3Ec__DisplayClass10_0_t1700828538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[4] = 
{
	U3CU3Ec__DisplayClass10_0_t1700828538::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass10_0_t1700828538::get_offset_of_delayInSeconds_1(),
	U3CU3Ec__DisplayClass10_0_t1700828538::get_offset_of_U3CU3E4__this_2(),
	U3CU3Ec__DisplayClass10_0_t1700828538::get_offset_of_U3CU3E9__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (U3CU3Ec_t1424112183), -1, sizeof(U3CU3Ec_t1424112183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2358[3] = 
{
	U3CU3Ec_t1424112183_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1424112183_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
	U3CU3Ec_t1424112183_StaticFields::get_offset_of_U3CU3E9__12_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (JSONStore_t1587640366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[3] = 
{
	JSONStore_t1587640366::get_offset_of_unity_0(),
	JSONStore_t1587640366::get_offset_of_store_1(),
	JSONStore_t1587640366::get_offset_of_promoPayload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (NativeStoreProvider_t1575491570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (Price_t1857690312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[3] = 
{
	Price_t1857690312::get_offset_of_value_0(),
	Price_t1857690312::get_offset_of_data_1(),
	Price_t1857690312::get_offset_of_num_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (StoreID_t694756629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[2] = 
{
	StoreID_t694756629::get_offset_of_store_0(),
	StoreID_t694756629::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (TranslationLocale_t265313191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2365[33] = 
{
	TranslationLocale_t265313191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (LocalizedProductDescription_t1808411718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[3] = 
{
	LocalizedProductDescription_t1808411718::get_offset_of_googleLocale_0(),
	LocalizedProductDescription_t1808411718::get_offset_of_title_1(),
	LocalizedProductDescription_t1808411718::get_offset_of_description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (U3CU3Ec_t4178581212), -1, sizeof(U3CU3Ec_t4178581212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[2] = 
{
	U3CU3Ec_t4178581212_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4178581212_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ProductCatalogPayout_t924351538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[3] = 
{
	ProductCatalogPayout_t924351538::get_offset_of_t_0(),
	ProductCatalogPayout_t924351538::get_offset_of_st_1(),
	ProductCatalogPayout_t924351538::get_offset_of_d_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ProductCatalogPayoutType_t1381797773)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[5] = 
{
	ProductCatalogPayoutType_t1381797773::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ProductCatalogItem_t2141417634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[9] = 
{
	ProductCatalogItem_t2141417634::get_offset_of_id_0(),
	ProductCatalogItem_t2141417634::get_offset_of_type_1(),
	ProductCatalogItem_t2141417634::get_offset_of_storeIDs_2(),
	ProductCatalogItem_t2141417634::get_offset_of_defaultDescription_3(),
	ProductCatalogItem_t2141417634::get_offset_of_applePriceTier_4(),
	ProductCatalogItem_t2141417634::get_offset_of_xiaomiPriceTier_5(),
	ProductCatalogItem_t2141417634::get_offset_of_googlePrice_6(),
	ProductCatalogItem_t2141417634::get_offset_of_descriptions_7(),
	ProductCatalogItem_t2141417634::get_offset_of_payouts_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (U3CU3Ec__DisplayClass21_0_t1127161168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[1] = 
{
	U3CU3Ec__DisplayClass21_0_t1127161168::get_offset_of_locale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ProductCatalog_t3178009003), -1, sizeof(ProductCatalog_t3178009003_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2372[2] = 
{
	ProductCatalog_t3178009003_StaticFields::get_offset_of_instance_0(),
	ProductCatalog_t3178009003::get_offset_of_products_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ProductCatalogImpl_t923359024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (Promo_t281370406), -1, sizeof(Promo_t281370406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2375[2] = 
{
	Promo_t281370406_StaticFields::get_offset_of_s_PromoPurchaser_0(),
	Promo_t281370406_StaticFields::get_offset_of_s_Unity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (StandardPurchasingModule_t2580735509), -1, sizeof(StandardPurchasingModule_t2580735509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2376[13] = 
{
	StandardPurchasingModule_t2580735509::get_offset_of_m_AppStorePlatform_1(),
	StandardPurchasingModule_t2580735509::get_offset_of_m_NativeStoreProvider_2(),
	StandardPurchasingModule_t2580735509::get_offset_of_m_RuntimePlatform_3(),
	StandardPurchasingModule_t2580735509::get_offset_of_m_Util_4(),
	StandardPurchasingModule_t2580735509::get_offset_of_m_Logger_5(),
	StandardPurchasingModule_t2580735509::get_offset_of_m_UseCloudCatalog_6(),
	StandardPurchasingModule_t2580735509_StaticFields::get_offset_of_ModuleInstance_7(),
	StandardPurchasingModule_t2580735509::get_offset_of_m_StoreInstance_8(),
	StandardPurchasingModule_t2580735509_StaticFields::get_offset_of_AndroidStoreNameMap_9(),
	StandardPurchasingModule_t2580735509::get_offset_of_m_CloudCatalog_10(),
	StandardPurchasingModule_t2580735509::get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_11(),
	StandardPurchasingModule_t2580735509::get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_12(),
	StandardPurchasingModule_t2580735509::get_offset_of_windowsStore_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (StoreInstance_t2416643455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[2] = 
{
	StoreInstance_t2416643455::get_offset_of_U3CstoreNameU3Ek__BackingField_0(),
	StoreInstance_t2416643455::get_offset_of_U3CinstanceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (MicrosoftConfiguration_t2761062260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[2] = 
{
	MicrosoftConfiguration_t2761062260::get_offset_of_useMock_0(),
	MicrosoftConfiguration_t2761062260::get_offset_of_module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (StoreConfiguration_t1737855245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[1] = 
{
	StoreConfiguration_t1737855245::get_offset_of_U3CandroidStoreU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (UnifiedReceipt_t1348780434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[1] = 
{
	UnifiedReceipt_t1348780434::get_offset_of_Payload_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (UnityUtil_t103543446), -1, sizeof(UnityUtil_t103543446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2381[4] = 
{
	UnityUtil_t103543446_StaticFields::get_offset_of_s_Callbacks_2(),
	UnityUtil_t103543446_StaticFields::get_offset_of_s_CallbacksPending_3(),
	UnityUtil_t103543446_StaticFields::get_offset_of_s_PcControlledPlatforms_4(),
	UnityUtil_t103543446::get_offset_of_pauseListeners_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (U3CDelayedCoroutineU3Ed__29_t3667477285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[5] = 
{
	U3CDelayedCoroutineU3Ed__29_t3667477285::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedCoroutineU3Ed__29_t3667477285::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedCoroutineU3Ed__29_t3667477285::get_offset_of_coroutine_2(),
	U3CDelayedCoroutineU3Ed__29_t3667477285::get_offset_of_delay_3(),
	U3CDelayedCoroutineU3Ed__29_t3667477285::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2383[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (__StaticArrayInitTypeSizeU3D368_t2501028641)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D368_t2501028641 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (U3CModuleU3E_t692745561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2386[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (U3CModuleU3E_t692745562), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_2(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2390[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2392[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2394[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2398[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
