﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody))]
public class CarCrashingPhysiscsBehavior : MonoBehaviour
{
	public enum CarType
	{
		Player,
		Obstacle
	}

	public CarType type;
	public event Action onCollisionEnterEvent;

	[SerializeField] int _activeCount = 1;
	[SerializeField] CarsRenderer _carRenderer;
	[SerializeField] CarsRenderPatternController _patternController;

	[Header("FORCE STATS")] [SerializeField]
	float _forceAmp;

	[SerializeField] float _forceModifer;
	[SerializeField] float _forceRadius;
	Rigidbody _rigid;

	void Awake()
	{
		_rigid = GetComponent<Rigidbody>();
	}

	void Start()
	{
	}

	void OnCollisionEnter(Collision other)
	{
		if (_activeCount < 1)
			return;

		if (type == CarType.Player)
		{
			if (other.gameObject.name.Contains(Define.OBSTACLE_NAME))
			{
				//vfx
				VFXSystem.Instance.SpawnFXAtPosition(VFXSystem.VFXCode.CarExplosion, other.GetContact(0).point);

				_activeCount--;
				AddPhysicsForce(other.GetContact(0).point);
				onCollisionEnterEvent?.Invoke();
			}

			return;
		}

		if (type == CarType.Obstacle)
		{
			if (other.gameObject.name.Contains(Define.PLAYER_NAME))
			{
				_activeCount--;
				AddPhysicsForce(other.GetContact(0).point);
				onCollisionEnterEvent?.Invoke();

				var fx = VFXSystem.Instance.SpawnFXAtPosition(VFXSystem.VFXCode.CarAccident, Vector3.zero);
				fx.transform.SetParent(transform);
				fx.transform.localPosition = Vector3.zero;
			}

			if (other.gameObject.name.Contains(Define.OBSTACLE_NAME))
			{
				_activeCount--;
				AddPhysicsForce(other.GetContact(0).point);
				onCollisionEnterEvent?.Invoke();

				var fx = VFXSystem.Instance.SpawnFXAtPosition(VFXSystem.VFXCode.CarAccident, Vector3.zero);
				fx.transform.SetParent(transform);
				fx.transform.localPosition = Vector3.zero;
			}
		}
	}

	private void AddPhysicsForce(Vector3 atPosition)
	{
		_rigid.isKinematic = false;
		_rigid.useGravity = true;
		_rigid.constraints = RigidbodyConstraints.None;
		_rigid.AddExplosionForce(_forceAmp, atPosition, _forceRadius, _forceModifer);
	}

	[ContextMenu("Set Default Configuration")]
	void SetDefaultConfig()
	{
		_forceAmp = 1000;
		_forceModifer = 1.5f;
		_forceRadius = 5f;
	}

	[ContextMenu("Test add force")]
	void TestAddForce()
	{
		_rigid.constraints = RigidbodyConstraints.None;
		_rigid.AddExplosionForce(500, transform.position + transform.forward * 2, 5, 1.2f);
	}
}