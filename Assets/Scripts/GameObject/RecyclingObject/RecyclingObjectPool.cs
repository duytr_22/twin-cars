﻿//  RecyclingObjectPool.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.05.10.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 再利用するオブジェクトをプールするクラス
/// </summary>
public class RecyclingObjectPool : MonoBehaviour {

	//オリジナルのオブジェクト
	private Dictionary<string, GameObject> _originalDict = new Dictionary<string, GameObject>();

	//生成したオブジェクトのプール
	private Dictionary<string, List<GameObject>> _pool = new Dictionary<string, List<GameObject>>();

	//=================================================================================
	//初期化
	//=================================================================================

	private void Awake (){
		//全プレハブを読み込み
		List<GameObject> prefabList = new List<GameObject>(Resources.LoadAll<GameObject>(ResourcesDirectoryPath.PREFAB_RECYCLING_OBJECT));

		for (int i = 0; i < prefabList.Count; i++) {
			//オリジナルを作成
			GameObject original = Instantiate(prefabList[i]);
			original.transform.parent = transform;
			original.name = prefabList[i].name;
			original.transform.position = Vector3.zero;

			_originalDict[original.name] = original;
			_pool[original.name] = new List<GameObject>();

			//オリジナルは常に非表示
			original.SetActive(false);
		}
	}

	//=================================================================================
	//マップオブジェクト
	//=================================================================================

	/// <summary>
	/// オブジェクトを取得
	/// </summary>
	public GameObject Get(string objectName){
		GameObject target = null;

		if(objectName.IsNullOrEmpty()){
			Debug.LogError("名前がありません！");
		}
		else if(!_pool.ContainsKey(objectName)){
			Debug.LogError(objectName + "はありません！");
		}

		//既にあるのでリサイクル
		if(_pool[objectName].Count > 0){
			target = _pool[objectName][0];
			_pool[objectName].Remove(target);
		}
		//新規作成
		else{
			target = Instantiate(_originalDict[objectName]);
			target.name = objectName;
			target.transform.parent = transform;
		}

		//表示、初期化して返す
		target.SetActive(true);
		if(target.GetComponent<RecyclingObject>() != null){
			target.GetComponent<RecyclingObject>().Init();
		}

		return target;
	}

	/// <summary>
	/// オブジェクトを解放
	/// </summary>
	public void Release(GameObject target){
		//リリース
		if(target.GetComponent<RecyclingObject>() != null){
			target.GetComponent<RecyclingObject>().Release();
		}

		//非表示、プールに格納
		target.SetActive(false);
		target.transform.parent = transform;
		_pool[target.name].Add(target);
	}

}