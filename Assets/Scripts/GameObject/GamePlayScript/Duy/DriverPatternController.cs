﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriverPatternController : MonoBehaviour
{
    [SerializeField] Transform _driverTransformParent;
    
    public void ActiveDriver(GameObject driverPrefab)
    {
        var driver = Instantiate(driverPrefab);
        driver.transform.SetParent(_driverTransformParent);
        driver.transform.localPosition = Vector3.zero + new Vector3(0, -0.15f, 0);
        driver.transform.localRotation = Quaternion.identity;
    }
}
