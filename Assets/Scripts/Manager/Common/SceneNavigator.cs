﻿//  EnumUtility.cs
//  ProductName Template
//
//  Created by kan.kikuchi

using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class SceneNavigator : SingletonMonoBehaviour<SceneNavigator>
{

	//フェード中か否か
	private bool _isFading = false;

	public  bool IsFading {
		get{ return _isFading; }
	}

	//一つ前と次のシーン名
	private string _afterSceneName = "", _beforeSceneName = "";

	public string BeforeSceneName {
		get{ return _beforeSceneName; }
	}

	//現在のシーン名とシーン番号
	public string CurrentSceneName {
		get{ return SceneManager.GetActiveScene ().name; }
	}

	public int CurrentSceneNo {
		get{ return SceneManager.GetActiveScene ().buildIndex; }
	}

	//フェードアウト終了時のイベント
	public event Action OnFinishedFadeOut = delegate{};

	//シーン読み込み完了時のイベント
	public event Action<int> OnLoadedScene = delegate{};

	//タイトルに来た回数
	private int _titleCount = 0;

	public  int TitleCount {
		get{ return _titleCount; }
	}

	//フェード用のクラス
	private FadeTransition _fadeTransition = null;

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Init ()
	{
		base.Init ();
		_fadeTransition = gameObject.AddComponent<FadeTransition> ();
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	//=================================================================================
	//シーン加算
	//=================================================================================

	/// <summary>
	/// シーンを加算する
	/// </summary>
	public void AddScene (string sceneName)
	{
		SceneManager.LoadScene (sceneName, LoadSceneMode.Additive);
	}

	//=================================================================================
	//シーン移動
	//=================================================================================

	//シーン移動後
	private void OnSceneLoaded (Scene scene, LoadSceneMode sceneMode)
	{
		OnLoadedScene (scene.buildIndex);
	}

	//ロードシーンに行く必要があるか
	private bool needToMoveLoadScene ()
	{

		//レベルモード以外は行かない
		if (UnityProjectSetting.Entity.GameType != GameType.LevelSelect) {
			return false;
		}

		//最初のステージor6の倍数以外のステージは行かない
		int stageNo = CacheData.Entity.StageNo;
		;
		if (stageNo == 1 || (stageNo - 1) % (UnityProjectSetting.Entity.LoadStageNo - 1) != 0) {
			return false;
		}

		//リトライの時は行かない
		if (CurrentSceneName == SceneName.RESULT && !CacheData.Entity.IsClear) {
			return false;
		}

		return true;
	}

	public void MoveScene (string setSceneName)
	{
		_afterSceneName = setSceneName;
		_beforeSceneName = CurrentSceneName;

		SceneManager.LoadScene (setSceneName);
		Time.timeScale = 1;
	}

	/// <summary>
	/// 指定したシーン名のシーンへ移動する
	/// </summary>
	public void MoveSceneFade (string setSceneName)
	{
		if (_isFading) {
			return;
		}
		_isFading = true;

		_afterSceneName = setSceneName;
		_beforeSceneName = CurrentSceneName;

		if (_beforeSceneName == SceneName.TITLE) {
			_titleCount++;
		}

		_fadeTransition.FadeOut ();
	}

	/// <summary>
	/// フェードアウト終了、シーンを移動し、フェードイン開始
	/// </summary>
	public void FadeOutEnd ()
	{
		if (!_isFading) {
			return;
		}

		Time.timeScale = 1;

		OnFinishedFadeOut ();

		//シーン移動
		SceneManager.LoadScene (_afterSceneName);

		//メモリ解放
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();

		//フェードアウト終了、シーン移動、フェードイン開始
		_isFading = false;

		_fadeTransition.FadeIn ();
	}

}