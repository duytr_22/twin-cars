﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButtonAssisstant : ButtonAssistant
{
	[SerializeField]
	GameObject _canvas;

	private bool pauseStatus = false;

	private void Start ()
	{
		//ゲーム終了時にキャンバスを非表示にし、ポーズの広告を消すように
		GameManager.Instance.OnGameEnd += (bool obj) => {
			
			_canvas.SetActive (false);

			DisableButton ();

			AdManager.Instance.UpdateAdWithPause (false);
		};
	}

	private void OnApplicationPause (bool pauseStatus)
	{		
		if (!pauseStatus && !_canvas.activeSelf && !MainManager.Instance.showAdWhenRetry) {

			pauseStatus = true;

			ChangeState (pauseStatus);
		}
	}

	protected override void OnClickButton ()
	{
		pauseStatus = !pauseStatus;

		ChangeState (pauseStatus);
	}

	private void ChangeState (bool isPause)
	{
		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial) {

			TutorialManager.Instance.isPause = isPause;

			if (!isPause) {
				Time.timeScale = TutorialManager.Instance.lastTimeScale;
			} else {
				TutorialManager.Instance.lastTimeScale = Time.timeScale;
				Time.timeScale = 0;
			}
		} else {
			Time.timeScale = isPause ? 0 : 1;
		}

		_canvas.SetActive (isPause);

		AdManager.Instance.UpdateAdWithPause (isPause);

		if (isPause) {
			BGMManager.Instance.Pause ();
		} else {
			BGMManager.Instance.Resume ();
		}
	}
}
