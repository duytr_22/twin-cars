//
//  PurchaseController.m
//  Unity-iPhone
//
//  Created  on 2015/09/10.
//
//  課金関係の処理を行うクラス

#import "PurchaseController.h"

@implementation PurchaseController

//購入中かどうかのフラグ
static BOOL _isPurchasing;

//=================================================================================
//初期化
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName{
    self = [super init];
    _isPurchasing = NO;
    return self;
}


//=================================================================================
//判定、取得
//=================================================================================

// 課金処理が実行できるか
- (BOOL)canPurchase{
    if (![SKPaymentQueue canMakePayments]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"エラー"
                                                        message:@"アプリ内課金が制限されています。"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
        return NO;
    }
    return YES;
}

//=================================================================================
//外部
//=================================================================================

// 課金アイテム情報のリクエストを送る
- (void)sendProductsRequest:(const char *)productID isPurchasing:(BOOL)isPurchasing{
    //課金処理が実行できるか判定
    if(![self canPurchase]){
        return;
    }
    _isPurchasing = isPurchasing;
    
    //課金アイテムのIDを指定して情報取得
    NSSet *set = [NSSet setWithObjects:[NSString stringWithCString:productID encoding:NSUTF8StringEncoding], nil];
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
    productsRequest.delegate = self;
    [productsRequest start];
}


//=================================================================================
//内部
//=================================================================================

//アイテムの情報取得
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    //無効なアイテムがないかチェック
    if ([response.invalidProductIdentifiers count] > 0) {
        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"エラー"
                                                        message:@"アイテムIDが不正です。"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];*/
        return;
    }
    
    //各課金アイテムの情報取得
    for (SKProduct *product in response.products) {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:product.priceLocale];
        NSString *localizedPrice = [numberFormatter stringFromNumber:product.price];
        
        //取得した情報をUnity側に伝える id#title#description#priceTextの形式で
        NSString* message = [NSString stringWithFormat:@"%@#%@#%@#%@",
                             product.productIdentifier, product.localizedTitle, product.localizedDescription, localizedPrice];
        
        UnitySendMessage(UNITY_OBJECT_NAME, UNITY_SET_ITEM_INFO_METHOD_NAME, [message UTF8String]);
        
        //NSLog(@"price : %@ : %@ : %@ : %@ : %@",
              //localizedPrice, product.localizedDescription, product.localizedTitle, product.productIdentifier, product.description);
    }
    
    if(!_isPurchasing){
        return;
    }
    
    //購入処理開始(「iTunes Storeにサインイン」ポップアップが表示)
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    for (SKProduct *product in response.products) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}

// トランザクション処理
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"購入処理中");
                UnitySendMessage(UNITY_OBJECT_NAME, UNITY_PURCHASING_METHOD_NAME, [transaction.payment.productIdentifier UTF8String]);
                break;
                
            case SKPaymentTransactionStatePurchased:
                NSLog(@"購入成功");
                UnitySendMessage(UNITY_OBJECT_NAME, UNITY_SUCCESS_METHOD_NAME, [transaction.payment.productIdentifier UTF8String]);
                [queue finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                UnitySendMessage(UNITY_OBJECT_NAME, UNITY_FAILED_METHOD_NAME, [transaction.payment.productIdentifier UTF8String]);
                NSLog(@"購入失敗: %@, %@", transaction.transactionIdentifier, transaction.error);
                [queue finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                // リストア処理
                NSLog(@"以前に購入した機能を復元");
                [queue finishTransaction:transaction];
                break;
                
            default:
                UnitySendMessage(UNITY_OBJECT_NAME, UNITY_FAILED_METHOD_NAME, [transaction.payment.productIdentifier UTF8String]);
                [queue finishTransaction:transaction];
                break;
        }
    }
    
}

//ネットワークに接続できないなどエラーが発生した場合に呼ばれる
-(void)request:(SKRequest*)request didFailWithError:(NSError*)error{
    UnitySendMessage(UNITY_OBJECT_NAME, UNITY_FAILED_METHOD_NAME, "");
    NSLog(@"request : %@", [error localizedDescription]);
}

@end