﻿/*
 * MultiResolutionCamera.cs
 * Created by Loi VT - New Gate
 * Created on: #CREATIONDATE#
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MultiResolutionCamera : MonoBehaviour
{
    Camera _camera;

    public float _deviceScale;
    public float _baseSize;
    // Use this for initialization
    void Start()
    {
        _camera = GetComponent<Camera>();

        Vector2 standardResolution = new Vector2(750, 1334);
        Vector2 frameSize = new Vector2(Screen.width, Screen.height);
        float factor = standardResolution.y / standardResolution.x;
        float shouldBeHeight = frameSize.x * factor;
        float scale = frameSize.y / shouldBeHeight;
        _deviceScale = scale;

        if (_deviceScale > 1.1f)//iphone x handle
        {
            if (_camera.orthographic)
            {
                _baseSize = _camera.orthographicSize;
                _camera.orthographicSize = _camera.orthographicSize * _deviceScale;
            }
            else
            {
                _baseSize = _camera.fieldOfView;
                // _camera.fieldOfView = _camera.fieldOfView * _deviceScale;
                _camera.fieldOfView = 70;
            }
        }
    }
}
