﻿//  ImagesPostprocessor.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.18.

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Imagesフォルダにインポートされた時に処理するクラス
/// </summary>
public sealed class ImagesPostprocessor : AssetPostprocessor{

	//returnの値が小さいほど優先される
	public override int GetPostprocessOrder (){
		return -1;
	}

	//Imagesディレクトリへのパス
	private const string IMAGES_DIRECTORY_PATH = DirectoryPath.TOP_RESOURCES_MAYBE_BUILD + "Images/";

	//==================================================================
	//Texture設定
	//==================================================================

	//ファイルが編集または追加されたら呼ばれる
	private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths){

		//Images内でディレクトリを移動したら再インポートして、OnPreprocessTextureが呼ばれるように
		foreach (string path in movedAssets) {
			if(path.Contains(IMAGES_DIRECTORY_PATH)){
				AssetDatabase.ImportAsset (path, ImportAssetOptions.ForceUpdate);
			}
		}

	}

	//==================================================================
	//Texture設定
	//==================================================================

	//Textureファイルのインポート設定 Textureファイルがインポートされる直前に呼び出される
	private void OnPreprocessTexture (){

		//インポート時のTextureファイルを設定するクラス
		TextureImporter textureImporter = assetImporter as TextureImporter;

		//Imageディレクトリ以外はスルー
		if (!textureImporter.assetPath.Contains (IMAGES_DIRECTORY_PATH)) {
			return;
		}

		//直下のディレクトリ名取得
		string directoryName = System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(textureImporter.assetPath));

		//アイコンディレクトリはスルー
		if(directoryName.ToLower().Contains("icon")){
			return;
		}

		//テクスチャの設定
		textureImporter.textureType      = TextureImporterType.Sprite; //テクスチャタイプをSpriteに
		textureImporter.mipmapEnabled    = false;                      //MipMapを作成しないように
		textureImporter.spritePackingTag = directoryName;              //タグをディレクトリ名に設定

		//圧縮用ディレクトリかそうじゃないかでタグとフォーマットを変える
		if(directoryName == "Compressed"){
			textureImporter.textureFormat = TextureImporterFormat.AutomaticCompressed;
		}
		else{
			textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
		}

	}

}