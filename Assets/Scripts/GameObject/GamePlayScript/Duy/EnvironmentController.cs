﻿using System;
using System.Collections;
using System.Collections.Generic;
using MF;
using UnityEngine;
using UnityEngine.SceneManagement;
using static GameManager.PlayMode;
using Random = UnityEngine.Random;

public class EnvironmentController : SingletonMonoBehaviour<EnvironmentController>
{
    public enum EnvironmentPattern
    {
        Pattern_1,
        Pattern_2,
        Pattern_3,
        Pattern_4,
        Pattern_5,
        Pattern_6
    }
    [System.Serializable]
    public struct EnvironmentData
    {
        public EnvironmentPattern cityCode;
        public GameObject pattern;
        public GameObject moveablePart;
        public Gradient backgroundColor;
        public Color fogColor;
        public Color ambientColor;
        public float fogStartDistance;
        public float fogEndDistance;
    }

    static EnvironmentPattern _cityPattern;
    [SerializeField] List<EnvironmentData> _data;
    [SerializeField] GradientBackground _targetBackgroundCamera;
    [SerializeField] List<EnvironmentPattern> _patternInUsed;
    static List<int> _tempIndexList;
    EnvironmentMoving _currentEnvironment;

    [Header("Debug"), SerializeField] UILabel _debugBGText;
    static int _debugBGIndex = -1;

    void Start()
    {
        var envData = _data.Find(item => item.cityCode == _cityPattern);
        envData.pattern.SetActive(true);
        _currentEnvironment = envData.moveablePart.GetComponent<EnvironmentMoving>();

        //set init
        TimelineSystem.onTimelineBeforeStop += envData.moveablePart.GetComponent<EnvironmentMoving>().OnPlay;
        _targetBackgroundCamera.Gradient = envData.backgroundColor;
        RenderSettings.fogColor = envData.fogColor;
        RenderSettings.ambientLight = envData.ambientColor;
        RenderSettings.fogStartDistance = envData.fogStartDistance;
        RenderSettings.fogEndDistance = envData.fogEndDistance;
    }


    public string GetBackgroundName()
    {
        return Enum.GetName(typeof(EnvironmentPattern), _debugBGIndex);
    }

    void OnEnable()
    {
#if SHOW_DEBUG
        if (_debugBGIndex != -1)
            _cityPattern = EnumUtility.NoToType<EnvironmentPattern>(_debugBGIndex);
        else
            _debugBGIndex = (int)_cityPattern;

        if (_debugBGText != null)
            _debugBGText.text = $"BG {Enum.GetName(typeof(EnvironmentPattern), _debugBGIndex)}";
#else
        if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTitle)
        {
            if (_tempIndexList == null || _tempIndexList.Count == 0)
            {
                _tempIndexList = new List<int>();
                for (int i = 0; i < _patternInUsed.Count; i++)
                    _tempIndexList.Add(i);
            }

            var index = _tempIndexList[Random.Range(0, _tempIndexList.Count)];
            _tempIndexList.Remove(index);
            _cityPattern = _patternInUsed[index];
            Debug.Log(_cityPattern);
        }
#endif
    }

    public void OnDebugNextBG()
    {
        if (_debugBGIndex < _data.Count - 1)
            _debugBGIndex++;
        else
            _debugBGIndex = 0;

        SceneManager.LoadScene(SceneNo.TITLE);
    }

    public void OnDebugPrevBG()
    {
        if (_debugBGIndex > 0)
            _debugBGIndex--;
        else
            _debugBGIndex = _data.Count - 1;

        SceneManager.LoadScene(SceneNo.TITLE);
    }

    public void StopMoving()
    {
        _currentEnvironment.StopMovingGradually();
    }
}
