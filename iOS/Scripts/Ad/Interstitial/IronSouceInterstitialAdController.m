//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  Nendのスプラッシュ(インターステイシャル)広告コントローラー

#import "IronSouceInterstitialAdController.h"
#import "MovieAdController.h"

@implementation IronSouceInterstitialAdController

//=================================================================================
//外部
//=================================================================================

//初期化
- (id)init{
    self = [super init];
    [self initInterstitial];
    return self;
}

//表示
-(void)showInterstitialAd:(int)playCount{
//    if (![super canShow:playCount]) {
//        return;
//    }
    [IronSource showInterstitialWithViewController:UnityGetGLViewController() placement:IRONSOURCE_INTERSTITIAL];
}

//=================================================================================
//内部
//=================================================================================


#pragma mark - Nend Interstitial
- (void)initInterstitial{
    [IronSource loadInterstitial];
    [IronSource setInterstitialDelegate:self];
}


#pragma -delegate

- (void)interstitialDidLoad {
    
    NSLog(@"Ironsource: did load");
}

- (void)interstitialDidFailToLoadWithError:(NSError *)error {
    
    NSLog(@"Ironsource: load fail with error %@", error.description);
}

- (void)interstitialDidOpen {
    
    NSLog(@"Ironsource: did open");
    UnitySendMessage(UNITY_OBJECT_NAME, "PauseGame", "");
    [MovieAdController startInterstial];
}

- (void)interstitialDidClose {
    
    [IronSource loadInterstitial];
    [MovieAdController finishInterstial];
    NSLog(@"Ironsource: did close");
}

- (void)interstitialDidShow {
    
    NSLog(@"Ironsource: did show");
}

- (void)interstitialDidFailToShowWithError:(NSError *)error {
    
    NSLog(@"Ironsource: show fail with error %@", error.description);
    [MovieAdController finishInterstial];
}

- (void)didClickInterstitial {
    
    NSLog(@"Ironsource: did click");
}

@end
