﻿/// <summary>
/// PlayerSettingsの設定を定数で管理するクラス
/// </summary>
public static class PlayerSettingsValue{

	  public const string BUNDLE_IDENTIFIER = "jp.keitachibana.wrongway";
	  public const string BUNDLE_VERSION    = "1.0.4";
	  public const string PRODUCT_NAME      = "Wrong Way Duo";

}
