package jp.oliviaashley.Advertising.Providers;

import android.app.Activity;

import jp.oliviaashley.Advertising.CheckSplSpfJson;
import jp.oliviaashley.Advertising.FlagsManager;

public class MovieAdsHelper {

    private static final String TAG = "MovieAdsHelper";
    static String type;

    public static void doOnCreate(Activity activity) {

        IronSouceVideoAdsHelper.doOnCreate(activity);
    }

    public static  void ShowMovieAds(Activity activity) {

        IronSouceVideoAdsHelper.showVideoAd();
    }

    public static boolean IsCanShowMovieAds(Activity activity) {

        boolean result = false;
        result = IronSouceVideoAdsHelper.isCanShow();
        return result;
    }
}
