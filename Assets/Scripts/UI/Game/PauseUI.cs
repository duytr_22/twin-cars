﻿//  PauseUICanvas.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.05.10.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ポーズ中のキャンバス
/// </summary>
public class PauseUI : MonoBehaviour
{

	private GameObject _canvas;

	//ボタン
	[SerializeField]
	private ButtonAssistant _pauseButtonAssistant = null, _resumeButtonAssistant = null;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Awake ()
	{
		_canvas = transform.GetChild (0).gameObject;
		_canvas.SetActive (false);
	}

	private void Start ()
	{
		//ゲーム終了時にキャンバスを非表示にし、ポーズの広告を消すように
		GameManager.Instance.OnGameEnd += (bool obj) => {
			_canvas.SetActive (false);
			AdManager.Instance.UpdateAdWithPause (false);
		};
	}

	//一時停止(ホームへ戻った時とか)or再開時
	private void OnApplicationPause (bool pauseStatus)
	{		
		if (pauseStatus && !_canvas.activeSelf && GameManager.Instance.CanShowPauseAds && Time.timeScale != 0) {			
			ChangeState (isPause: true);
		}
	}

	//=================================================================================
	//更新
	//=================================================================================

	//ポーズ状態を切り替える
	private void ChangeState (bool isPause)
	{
		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial) {
			
			TutorialManager.Instance.isPause = isPause;

			if (!isPause) {
				Time.timeScale = TutorialManager.Instance.lastTimeScale;
			} else {
				TutorialManager.Instance.lastTimeScale = Time.timeScale;
				Time.timeScale = 0;
			}
		} else {
			Time.timeScale = isPause ? 0 : 1;
		}

		_canvas.SetActive (isPause);
		AdManager.Instance.UpdateAdWithPause (isPause);

		if (isPause) {
			BGMManager.Instance.Pause ();
		} else {
			BGMManager.Instance.Resume ();
		}
	}


}