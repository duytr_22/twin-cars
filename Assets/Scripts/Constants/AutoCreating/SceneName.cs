﻿/// <summary>
/// シーン名を定数で管理するクラス
/// </summary>
public static class SceneName{

	  public const string BALL                     = "Ball";
	  public const string COMMON_PEOPLE_DEMO_SCENE = "CommonPeopleDemoScene";
	  public const string CRASH_SCENE              = "Crash_Scene";
	  public const string DEMO                     = "Demo";
	  public const string DEMO_VROR_MOBILE         = "Demo VR Or Mobile";
	  public const string DEMO_CUSTOM_MESH         = "DemoCustomMesh";
	  public const string DEMO_HIGH_QUALITY        = "DemoHighQuality";
	  public const string GAME                     = "Game";
	  public const string IAPDEMO                  = "IAP Demo";
	  public const string ISLAND_EXAMPLE           = "island_example";
	  public const string ISLAND_SHOWCASE          = "island_showcase";
	  public const string LINEUP                   = "Lineup";
	  public const string PLAYER_C_DEMO            = "player_c_demo";
	  public const string RESULT                   = "Result";
	  public const string TEST                     = "Test";
	  public const string TITLE                    = "Title";
	  public const string TUTORIAL                 = "Tutorial";
	  public const string UIENDING                 = "UIEnding";
	  public const string UIGAMEPLAY               = "UIGameplay";
	  public const string UITITLE                  = "UITitle";

}
