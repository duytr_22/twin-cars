﻿//  LoadProgressBar.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.29.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ロード画面の進捗を表すプログレスバー
/// </summary>
public class LoadProgressBar : MonoBehaviour
{

	private Scrollbar _scrollBar;

	//ロード中の文言を表示するラベル
	[SerializeField]
	private GameObject _loadTitleLabel;

	//ロード終了するボタン
	[SerializeField]
	private LoadEndButtonAssistant _loadEndButton;

	//ロードと待ち時間の割合の合計と全体のロード時間
	private float _totalTimePer = 0, _loadTime = 0;

	//ロードの時間
	private const float LOAD_TIME_MIN = 4.0f;
	private const float LOAD_TIME_MAX = 5.0f;

	private List<float> _loadTimePerList;
	private int _loadNo = 0;

	//待ち時間
	private List<float> _waitTimePerList;
	private bool _isWaiting = true;
	private int _waitingNo = 0;

	//進捗
	private List<float> _progressPerList;
	private float _totalProgressPer = 0;
	private float _progress = 0, _addtionalProgress = 0;

	//更新
	private float _updateLimit = 0, _updateCount = 0;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Awake ()
	{
		_scrollBar = GetComponent<Scrollbar> ();
		_scrollBar.size = 0;

		_loadTitleLabel.SetActive (true);

		//ロード時間決定
		_loadTime = UnityEngine.Random.Range (LOAD_TIME_MIN, LOAD_TIME_MAX);

		_loadTimePerList = new List<float> ();
		AddLoadTimePer (UnityEngine.Random.Range (5, 20));
		AddLoadTimePer (UnityEngine.Random.Range (20, 40));
		AddLoadTimePer (UnityEngine.Random.Range (5, 20));

		_waitTimePerList = new List<float> ();
		AddWaitTimePer (UnityEngine.Random.Range (5, 20) + FadeTransition.FADE_TIME);
		AddWaitTimePer (UnityEngine.Random.Range (5, 20));
		AddWaitTimePer (UnityEngine.Random.Range (20, 40));

		_progressPerList = new List<float> ();
		AddProgresPer (UnityEngine.Random.Range (40, 60));
		AddProgresPer (UnityEngine.Random.Range (40, 60));
		AddProgresPer (UnityEngine.Random.Range (20, 30));

		//シーン移動完了後、ロード開始
		StartNextAction ();
	}

	//ロード時間の割合を追加
	private void AddLoadTimePer (float loadTimePer)
	{
		_totalTimePer += loadTimePer;
		_loadTimePerList.Add (loadTimePer);
	}

	//待ち時間時間の割合を追加
	private void AddWaitTimePer (float waitTimePer)
	{
		_totalTimePer += waitTimePer;
		_waitTimePerList.Add (waitTimePer);
	}

	//進捗の割合を追加
	private void AddProgresPer (float progressPer)
	{
		_totalProgressPer += progressPer;
		_progressPerList.Add (progressPer);
	}

	//=================================================================================
	//更新
	//=================================================================================

	//次のアクション
	private void StartNextAction ()
	{
		if (_isWaiting) {
			_updateLimit = _loadTime * _waitTimePerList [_waitingNo] / _totalTimePer;
			_addtionalProgress = 0;
		} else {
			_updateLimit = _loadTime * _loadTimePerList [_loadNo] / _totalTimePer;
			_addtionalProgress = _progressPerList [_loadNo] / _totalProgressPer;
		}
		_updateCount = 0;
	}

	private void Update ()
	{

		_updateCount = Mathf.Min (_updateLimit, _updateCount + Time.deltaTime);
		_scrollBar.size = _progress + _addtionalProgress * _updateCount / _updateLimit;

		//更新終了判定
		if (_updateCount != _updateLimit) {
			return;
		}

		if (_isWaiting) {
			_waitingNo++;
		} else {
			_loadNo++;
		}
		_isWaiting = !_isWaiting;
		_progress += _addtionalProgress;

		//ロード終了判定
		if (_loadNo == _loadTimePerList.Count) {
			_loadTitleLabel.SetActive (false);
			this.enabled = false;
		} else {
			StartNextAction ();
		}

	}

}