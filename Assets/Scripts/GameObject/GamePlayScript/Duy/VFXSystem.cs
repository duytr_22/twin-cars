﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXSystem : SingletonMonoBehaviour<VFXSystem>
{
	public enum VFXCode
	{
		CarExplosion,
		CarAccident,
		CarSmoke
	}

	[Serializable]
	public struct VFXData
	{
		public VFXCode code;
		public GameObject prefab;
	}

	[SerializeField] List<VFXData> _listVFX;
	Dictionary<VFXCode, GameObject> _internalDictionary;
	Dictionary<VFXCode, List<GameObject>> _currentActiveFX;
	bool _isSmokeFXStopped;

	protected override void Init()
	{
		_currentActiveFX = new Dictionary<VFXCode, List<GameObject>>();
		_internalDictionary = new Dictionary<VFXCode, GameObject>();

		base.Init();
	}

	void OnEnable()
	{
		_listVFX.ForEach(item =>
		{
			if (_internalDictionary.ContainsKey(item.code) == false)
			{
				// Debug.Log($"Add item {item.code}");
				_internalDictionary.Add(item.code, item.prefab);
			}
		});
	}

	public GameObject SpawnFXAtPosition(VFXCode code, Vector3 position, bool isPlayNow = false)
	{
		var obj = Instantiate(_internalDictionary[code], position, Quaternion.identity);
		if (isPlayNow == true)
			obj.GetComponent<ParticleSystem>().Play();

		AddToActiveDictionary(code, obj);


		return obj;
	}

	void AddToActiveDictionary(VFXCode code, GameObject obj)
	{
		if (_currentActiveFX.ContainsKey(code) == false) //init new pair
			_currentActiveFX.Add(code, new List<GameObject>() {obj});
		else //or add to existing pair
			_currentActiveFX[code].Add(obj);
	}

	// void OnValidate()
	// {
	//     if (_internalDictionary == null)
	//         _internalDictionary = new Dictionary<VFXCode, GameObject>();

	//     _listVFX.ForEach(item =>
	//     {
	//         if (_internalDictionary.ContainsKey(item.code) == false)
	//         {
	//             Debug.Log($"Add item {item.code}");
	//             _internalDictionary.Add(item.code, item.prefab);
	//         }
	//     });
	// }
	public void RegisterItemManually(GameObject ps, VFXCode code)
	{
		AddToActiveDictionary(code, ps);
	}

	public void StopAllSmokeFX()
	{
		if (_isSmokeFXStopped == true)
			return;
		_isSmokeFXStopped = true;
		if (_currentActiveFX.ContainsKey(VFXCode.CarSmoke))
		{
			var list = _currentActiveFX[VFXCode.CarSmoke];
			list.ForEach(item => item.GetComponent<ParticleSystem>().Stop());
		}
	}
}