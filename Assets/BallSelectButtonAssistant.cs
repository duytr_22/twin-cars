﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSelectButtonAssistant : MonoBehaviour
{

	[SerializeField]
	GameObject _check, _icon;

	private UISprite _sprite;
	private UIButton _button;

	private int idx;
	private string name = "ball0{0}";
	private string nameLock = "ball0{0}_lock";

	// Use this for initialization
	void Start ()
	{
		idx = int.Parse (gameObject.name);

		_sprite = GetComponent<UISprite> ();
		_button = GetComponent<UIButton> ();

		BallController.Instance.BallList.Add (this);

		// Check available
		Refresh ();
	}

	void OnClick ()
	{
		SEManager.Instance.PlaySE (AudioName.SE_BUTTON);

		BallController.Instance.UncheckAll ();

		PlayerPrefs.SetInt (string.Format (Define.BALL_UNLOCK, idx), 2);

		PlayerPrefs.SetInt (Define.BALL_SELECTED, idx);

		PlayerPrefs.Save ();

		Refresh ();

		BallController.Instance.CheckShowIcon ();
	}

	public void Uncheck ()
	{
		_check.SetActive (false);
	}

	void Refresh ()
	{
		int unlock = PlayerPrefs.GetInt (string.Format (Define.BALL_UNLOCK, idx), 0);

		if (idx == 1)
			unlock = 2;

		if (unlock == 0) {

			_button.isEnabled = false;

			_sprite.spriteName = string.Format (nameLock, idx);

			_button.normalSprite = _sprite.spriteName;
			_button.hoverSprite = _sprite.spriteName;
			_button.pressedSprite = _sprite.spriteName;

			_icon.SetActive (false);
			_check.SetActive (false);

		} else if (unlock == 1) { // New

			_sprite.spriteName = string.Format (name, idx);

			_button.isEnabled = true;

			_button.normalSprite = _sprite.spriteName;
			_button.hoverSprite = _sprite.spriteName;
			_button.pressedSprite = _sprite.spriteName;

			_icon.SetActive (true);
			_check.SetActive (false);

		} else if (unlock == 2) { // Unlock

			GetComponent<BoxCollider> ().enabled = true;

			_sprite.spriteName = string.Format (name, idx);

			_button.isEnabled = true;

			_button.normalSprite = _sprite.spriteName;
			_button.hoverSprite = _sprite.spriteName;
			_button.pressedSprite = _sprite.spriteName;

			_icon.SetActive (false);
			_check.SetActive (false);
		}

		if (PlayerPrefs.GetInt (Define.BALL_SELECTED, 1) == idx) {

			_check.SetActive (true);
		}
	}
}
