﻿using UnityEngine;
using System.Collections;

public class HazeScript : MonoBehaviour
{

	UISprite sprite;

	// Use this for initialization
	void Start ()
	{
		sprite = this.GetComponent<UISprite> ();

		sprite.color = BackgroundScript.Instance.hazeColors [0];

		BackgroundScript.Instance.OnHazeChangeColor += OnChangeColor;
	}

	void OnChangeColor (Color color)
	{
		sprite.color = color;
	}
}
