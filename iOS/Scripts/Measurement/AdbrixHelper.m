//
//  AdbrixHelper.m
//  TemplateProject32
//
//

#import "AdbrixHelper.h"
#import <IgaworksCore/IgaworksCore.h>
#import <AdSupport/AdSupport.h>
#import "CommonConfig.h"

@implementation AdbrixHelper
+ (void) startAdbrix {
    if (NSClassFromString(@"ASIdentifierManager")){
        NSUUID *ifa =[[ASIdentifierManager sharedManager]advertisingIdentifier];
        BOOL isAppleAdvertisingTrackingEnalbed = [[ASIdentifierManager sharedManager]isAdvertisingTrackingEnabled];
        [IgaworksCore setAppleAdvertisingIdentifier:[ifa UUIDString] isAppleAdvertisingTrackingEnabled:isAppleAdvertisingTrackingEnalbed];
    }    
    [IgaworksCore igaworksCoreWithAppKey:ADBRIX_APP_KEY andHashKey:ADBRIX_HASH_KEY];
}
@end
