﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TimelineSystem : MonoBehaviour
{
    public static event Action onTimelineCompleted;
    public static event Action onTimelineBeforeStop;

    [SerializeField] TimelineAsset _normalAssets, _ipXAssets;
    [SerializeField] float _offsetToTriggerStop = 0.9f;
    PlayableDirector _director;
    float _duration;

    void Awake()
    {
        _director = GetComponent<PlayableDirector>();
        _duration = (float)_director.duration;
        _director.stopped += param => onTimelineCompleted?.Invoke();

        if (MultiResolutionUI.IsIPhoneX())
            _director.playableAsset = _ipXAssets;
        else
            _director.playableAsset = _normalAssets;
        
        _director.Play();
    }

    void OnDisable()
    {
        onTimelineBeforeStop = null;
        onTimelineCompleted = null;
    }

    void Update()
    {
        //config to make game started before the time line stopped
        if (_director.time > _duration * _offsetToTriggerStop)
        {
            _offsetToTriggerStop = int.MaxValue;
            onTimelineBeforeStop?.Invoke();
        }
    }
}
