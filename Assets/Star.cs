﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{

	TweenAlpha _tween;

	// Use this for initialization
	void Start ()
	{
		_tween = GetComponent<TweenAlpha> ();

		float scale = Random.Range (0.1f, 0.75f);

		transform.localScale = new Vector3 (scale, scale, scale);

		_tween.delay = Random.Range (0.0f, 2.5f);
		_tween.duration = Random.Range (1.0f, 1.25f);

		_tween.PlayForward ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
