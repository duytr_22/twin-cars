//
//  ISMopubAdapter.h
//  ISMopubAdapter
//
//  Created by Artem on 8/22/17.
//  Copyright © 2017 IronSource. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IronSource/ISBaseAdapter+Internal.h"

//System Frameworks For Mopub Adapter

@import AdSupport;
@import CoreGraphics;
@import CoreLocation;
@import CoreTelephony;
@import Foundation;
@import MediaPlayer;
@import QuartzCore;
@import StoreKit;
@import UIKit;
@import WebKit;

@interface ISMoPubAdapter : ISBaseAdapter
@end
