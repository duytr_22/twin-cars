﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIBackScript : MonoBehaviour
{

    //Mountain
    public GameObject mountainPrefab;
    public int NumOfMountain;

    public GameObject cloud1;
    public GameObject cloud2;

    private float timeCount;
    private List<DynamicBack> listDynamicBacks;

    void Awake()
    {
        this.InitCloud();
        timeCount = 1;

        //TODO: something here
        listDynamicBacks = new List<DynamicBack>();

        // int firstMountaionNum = 5;
        // for (int i = 0; i < firstMountaionNum; i++) {
        // 	CreateGamePlayMountain (0.2f * (float)i);
        // }
    }

    // Use this for initialization
    void Start()
    {
        //		timeCount = 1;
        //		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeGame) {
        //			//TODO: something here
        //		} else {
        //			listDynamicBacks = new List<DynamicBack> ();
        //
        //			int firstMountaionNum = 5;
        //			for (int i = 0; i < firstMountaionNum; i++) {
        //				CreateGamePlayMountain(0.2f * (float)i);
        //				Debug.Log ("CreateMountain");
        //			}
        //		}
    }

    void Update()
    {
        if (Time.timeScale == 0 || GamePlayScript.Instance.isStartedGame == false)
        {
            return;
        }

        if (GameManager.Instance.playMode != GameManager.PlayMode.PlayModeTitle)
        {
            if (GameManager.Instance.IsPlayingGame && !GamePlayScript.Instance.isOver)
            {
                timeCount += Time.deltaTime;
                if (timeCount >= 1.0f / 2)
                {
                    CreateMountain(0);
                }
            }
        }
        else
        {
            timeCount += Time.deltaTime;
            if (timeCount >= 1.0f)
            {
                CreateMountain(0);
            }
        }
    }

    private void CreateMountain(float factor)
    {
        timeCount = 0;
        bool isLeft = false;
        for (int i = 0; i < 2; i++)
        {
            float delay = i % (2 / 2);
            GameObject mountain = Instantiate(mountainPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            mountain.transform.SetParent(this.transform);
            DynamicBack mountainScript = mountain.GetComponent<DynamicBack>();
            mountainScript.Init(factor, isLeft, delay);
            isLeft = true;
        }
    }

    private void CreateGamePlayMountain(float factor)
    {
        timeCount = 0;
        bool isLeft = false;

        for (int i = 0; i < 2; i++)
        {

            float delay = i % (2 / 2);

            GameObject mountain = Instantiate(mountainPrefab, Vector3.zero, Quaternion.identity) as GameObject;

            mountain.transform.SetParent(this.transform);

            DynamicBack mountainScript = mountain.GetComponent<DynamicBack>();

            mountainScript.Init(factor, isLeft, delay, true);

            isLeft = true;

            if (GameManager.Instance.playMode != GameManager.PlayMode.PlayModeTitle)
            {

                listDynamicBacks.Add(mountainScript);
            }
        }
    }

    void InitCloud()
    {
        if (cloud1)
            cloud1.SetActive(true);

        if (cloud2)
            cloud2.SetActive(true);
    }

    public void StartUIBack()
    {
        //		if (listDynamicBacks == null) {
        //			listDynamicBacks = new List<DynamicBack> ();
        //
        //			int firstMountaionNum = 5;
        //			for (int i = 0; i < firstMountaionNum; i++) {
        //				CreateGamePlayMountain(0.2f * (float)i);
        //				Debug.Log ("CreateMountain");
        //			}
        //
        //			this.InitCloud ();
        //		}

        //		foreach (DynamicBack dynamicBack in listDynamicBacks) {
        //			dynamicBack.StartTween ();
        //		}

        if (cloud1)
            cloud1.SetActive(true);

        if (cloud2)
            cloud2.SetActive(true);
    }

    public void StopUIBack()
    {
        DynamicBack[] dynamicBackArray = transform.GetComponentsInChildren<DynamicBack>();
        foreach (DynamicBack dynamicBack in dynamicBackArray)
        {
            dynamicBack.enabled = false;
            dynamicBack.StopTween();
        }

        if (cloud1)
        {
            UITweener tw1 = cloud1.GetComponent<UITweener>();
            tw1.enabled = false;
        }

        if (cloud1)
        {
            UITweener tw2 = cloud2.GetComponent<UITweener>();
            tw2.enabled = false;
        }
    }
}
