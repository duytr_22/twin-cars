//
//  BannerAdController.h
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Adstirのバナー広告のコントローラー

#import "BannerAdController.h"
#import "MoPub.h"

@interface MoPubBannerHelper : BannerAdController<MPAdViewDelegate>

@property (nonatomic, retain) UIView* view;

-(UIView*)prepareBannerAd;

@end
