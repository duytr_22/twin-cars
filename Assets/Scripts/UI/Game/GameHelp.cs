﻿//  GameHelp.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲーム画面のヘルプ
/// </summary>
public class GameHelp : MonoBehaviour
{
	private void Start ()
	{
		GameStart ();
	}

	private void GameStart ()
	{
		GameManager.Instance.Begin ();
	
		gameObject.SetActive (false);
	}
}