﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabStageButtonAssistant : ButtonAssistant
{
	TweenScale _scaleTw;
	TweenPosition _positionTw;

	[SerializeField]
	GameObject _icon;

	void Start ()
	{
		_scaleTw = GetComponent<TweenScale> ();
		_positionTw = GetComponent<TweenPosition> ();

		CheckShowIcon ();
	}

	public void CheckShowIcon ()
	{
		_icon.SetActive (false);

		for (int i = 0; i < 5; i++) {

			if (PlayerPrefs.GetInt (string.Format (Define.STAGE_UNLOCK, i + 1), 0) == 1) { // New

				_icon.SetActive (true);

				break;
			}
		}
	}

	protected override void OnClickButton ()
	{
		BallController.Instance.ShowTab (1);
	}

	public void SelectAnim ()
	{
		_scaleTw.PlayForward ();
		_positionTw.PlayForward ();
	}

	public void UnselectAnim ()
	{
		_scaleTw.PlayReverse ();
		_positionTw.PlayReverse ();
	}
}
