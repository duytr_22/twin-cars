﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<UnityEngine.Store.UserInfo>
struct Action_1_t3058893588;
// RankingButtonAssistant
struct RankingButtonAssistant_t3062667213;
// IAPDemo/UnityChannelPurchaseInfo
struct UnityChannelPurchaseInfo_t74063925;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t3178009003;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton>
struct List_1_t3820967359;
// UnityEngine.Purchasing.IAPListener
struct IAPListener_t2001792988;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t2579314702;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t3180538779;
// BackTitleButtonAssistant
struct BackTitleButtonAssistant_t2533991015;
// FadeTransition
struct FadeTransition_t2992252162;
// System.Collections.Generic.Dictionary`2<System.String,BaseTenjin>
struct Dictionary_2_t1599024990;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1618671084;
// IAPDemo
struct IAPDemo_t3681080565;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// AppStoresSupport.AppStoreSetting
struct AppStoreSetting_t1592337179;
// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t1675809258;
// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t800864861;
// UnityEngine.Collider
struct Collider_t1773347010;
// UIButton
struct UIButton_t1100396938;
// UISprite
struct UISprite_t194114938;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// LoadEndButtonAssistant
struct LoadEndButtonAssistant_t4277748694;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// ButtonAssistant
struct ButtonAssistant_t3922690862;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UILabel
struct UILabel_t3248798549;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t3721407765;
// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t1729542224;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t4146644616;
// UnityEngine.Purchasing.IMoolahExtension
struct IMoolahExtension_t955300474;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_t2712620151;
// UnityEngine.Purchasing.IMicrosoftExtensions
struct IMicrosoftExtensions_t4020186927;
// UnityEngine.Purchasing.IUnityChannelExtensions
struct IUnityChannelExtensions_t3299991497;
// IAPDemo/UnityChannelLoginHandler
struct UnityChannelLoginHandler_t2949829254;
// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI>
struct Dictionary_2_t708210053;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// TweenAlpha
struct TweenAlpha_t3706845226;
// TweenScale
struct TweenScale_t2539309033;
// TweenPosition
struct TweenPosition_t1378762002;
// UnityEngine.Sprite
struct Sprite_t280657092;
// TouchEventHandler
struct TouchEventHandler_t1627989417;
// System.Collections.Generic.Stack`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Stack_1_t2476096443;
// Tenjin/DeferredDeeplinkDelegate
struct DeferredDeeplinkDelegate_t4234501760;
// IosTenjin/DeepLinkHandlerNativeDelegate
struct DeepLinkHandlerNativeDelegate_t1988926173;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2328697118;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t3894781059;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745564_H
#define U3CMODULEU3E_T692745564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745564 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745564_H
#ifndef NATIVEUTILITY_T307969918_H
#define NATIVEUTILITY_T307969918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IosTenjin/NativeUtility
struct  NativeUtility_t307969918  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEUTILITY_T307969918_H
#ifndef U3CVALIDATEBUTTONCLICKU3EC__ANONSTOREY1_T541528072_H
#define U3CVALIDATEBUTTONCLICKU3EC__ANONSTOREY1_T541528072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/<ValidateButtonClick>c__AnonStorey1
struct  U3CValidateButtonClickU3Ec__AnonStorey1_t541528072  : public RuntimeObject
{
public:
	// System.String IAPDemo/<ValidateButtonClick>c__AnonStorey1::txId
	String_t* ___txId_0;

public:
	inline static int32_t get_offset_of_txId_0() { return static_cast<int32_t>(offsetof(U3CValidateButtonClickU3Ec__AnonStorey1_t541528072, ___txId_0)); }
	inline String_t* get_txId_0() const { return ___txId_0; }
	inline String_t** get_address_of_txId_0() { return &___txId_0; }
	inline void set_txId_0(String_t* value)
	{
		___txId_0 = value;
		Il2CppCodeGenWriteBarrier((&___txId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATEBUTTONCLICKU3EC__ANONSTOREY1_T541528072_H
#ifndef UNITYCHANNELLOGINHANDLER_T2949829254_H
#define UNITYCHANNELLOGINHANDLER_T2949829254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelLoginHandler
struct  UnityChannelLoginHandler_t2949829254  : public RuntimeObject
{
public:
	// System.Action IAPDemo/UnityChannelLoginHandler::initializeSucceededAction
	Action_t1264377477 * ___initializeSucceededAction_0;
	// System.Action`1<System.String> IAPDemo/UnityChannelLoginHandler::initializeFailedAction
	Action_1_t2019918284 * ___initializeFailedAction_1;
	// System.Action`1<UnityEngine.Store.UserInfo> IAPDemo/UnityChannelLoginHandler::loginSucceededAction
	Action_1_t3058893588 * ___loginSucceededAction_2;
	// System.Action`1<System.String> IAPDemo/UnityChannelLoginHandler::loginFailedAction
	Action_1_t2019918284 * ___loginFailedAction_3;

public:
	inline static int32_t get_offset_of_initializeSucceededAction_0() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___initializeSucceededAction_0)); }
	inline Action_t1264377477 * get_initializeSucceededAction_0() const { return ___initializeSucceededAction_0; }
	inline Action_t1264377477 ** get_address_of_initializeSucceededAction_0() { return &___initializeSucceededAction_0; }
	inline void set_initializeSucceededAction_0(Action_t1264377477 * value)
	{
		___initializeSucceededAction_0 = value;
		Il2CppCodeGenWriteBarrier((&___initializeSucceededAction_0), value);
	}

	inline static int32_t get_offset_of_initializeFailedAction_1() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___initializeFailedAction_1)); }
	inline Action_1_t2019918284 * get_initializeFailedAction_1() const { return ___initializeFailedAction_1; }
	inline Action_1_t2019918284 ** get_address_of_initializeFailedAction_1() { return &___initializeFailedAction_1; }
	inline void set_initializeFailedAction_1(Action_1_t2019918284 * value)
	{
		___initializeFailedAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___initializeFailedAction_1), value);
	}

	inline static int32_t get_offset_of_loginSucceededAction_2() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___loginSucceededAction_2)); }
	inline Action_1_t3058893588 * get_loginSucceededAction_2() const { return ___loginSucceededAction_2; }
	inline Action_1_t3058893588 ** get_address_of_loginSucceededAction_2() { return &___loginSucceededAction_2; }
	inline void set_loginSucceededAction_2(Action_1_t3058893588 * value)
	{
		___loginSucceededAction_2 = value;
		Il2CppCodeGenWriteBarrier((&___loginSucceededAction_2), value);
	}

	inline static int32_t get_offset_of_loginFailedAction_3() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___loginFailedAction_3)); }
	inline Action_1_t2019918284 * get_loginFailedAction_3() const { return ___loginFailedAction_3; }
	inline Action_1_t2019918284 ** get_address_of_loginFailedAction_3() { return &___loginFailedAction_3; }
	inline void set_loginFailedAction_3(Action_1_t2019918284 * value)
	{
		___loginFailedAction_3 = value;
		Il2CppCodeGenWriteBarrier((&___loginFailedAction_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELLOGINHANDLER_T2949829254_H
#ifndef UNITYCHANNELPURCHASEINFO_T74063925_H
#define UNITYCHANNELPURCHASEINFO_T74063925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelPurchaseInfo
struct  UnityChannelPurchaseInfo_t74063925  : public RuntimeObject
{
public:
	// System.String IAPDemo/UnityChannelPurchaseInfo::productCode
	String_t* ___productCode_0;
	// System.String IAPDemo/UnityChannelPurchaseInfo::gameOrderId
	String_t* ___gameOrderId_1;
	// System.String IAPDemo/UnityChannelPurchaseInfo::orderQueryToken
	String_t* ___orderQueryToken_2;

public:
	inline static int32_t get_offset_of_productCode_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t74063925, ___productCode_0)); }
	inline String_t* get_productCode_0() const { return ___productCode_0; }
	inline String_t** get_address_of_productCode_0() { return &___productCode_0; }
	inline void set_productCode_0(String_t* value)
	{
		___productCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___productCode_0), value);
	}

	inline static int32_t get_offset_of_gameOrderId_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t74063925, ___gameOrderId_1)); }
	inline String_t* get_gameOrderId_1() const { return ___gameOrderId_1; }
	inline String_t** get_address_of_gameOrderId_1() { return &___gameOrderId_1; }
	inline void set_gameOrderId_1(String_t* value)
	{
		___gameOrderId_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameOrderId_1), value);
	}

	inline static int32_t get_offset_of_orderQueryToken_2() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t74063925, ___orderQueryToken_2)); }
	inline String_t* get_orderQueryToken_2() const { return ___orderQueryToken_2; }
	inline String_t** get_address_of_orderQueryToken_2() { return &___orderQueryToken_2; }
	inline void set_orderQueryToken_2(String_t* value)
	{
		___orderQueryToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___orderQueryToken_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASEINFO_T74063925_H
#ifndef U3CSTARTU3EC__ITERATOR0_T843897986_H
#define U3CSTARTU3EC__ITERATOR0_T843897986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RankingButtonAssistant/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t843897986  : public RuntimeObject
{
public:
	// RankingButtonAssistant RankingButtonAssistant/<Start>c__Iterator0::$this
	RankingButtonAssistant_t3062667213 * ___U24this_0;
	// System.Object RankingButtonAssistant/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RankingButtonAssistant/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 RankingButtonAssistant/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t843897986, ___U24this_0)); }
	inline RankingButtonAssistant_t3062667213 * get_U24this_0() const { return ___U24this_0; }
	inline RankingButtonAssistant_t3062667213 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RankingButtonAssistant_t3062667213 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t843897986, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t843897986, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t843897986, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T843897986_H
#ifndef UNITYCHANNELPURCHASEERROR_T2306817818_H
#define UNITYCHANNELPURCHASEERROR_T2306817818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelPurchaseError
struct  UnityChannelPurchaseError_t2306817818  : public RuntimeObject
{
public:
	// System.String IAPDemo/UnityChannelPurchaseError::error
	String_t* ___error_0;
	// IAPDemo/UnityChannelPurchaseInfo IAPDemo/UnityChannelPurchaseError::purchaseInfo
	UnityChannelPurchaseInfo_t74063925 * ___purchaseInfo_1;

public:
	inline static int32_t get_offset_of_error_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_t2306817818, ___error_0)); }
	inline String_t* get_error_0() const { return ___error_0; }
	inline String_t** get_address_of_error_0() { return &___error_0; }
	inline void set_error_0(String_t* value)
	{
		___error_0 = value;
		Il2CppCodeGenWriteBarrier((&___error_0), value);
	}

	inline static int32_t get_offset_of_purchaseInfo_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_t2306817818, ___purchaseInfo_1)); }
	inline UnityChannelPurchaseInfo_t74063925 * get_purchaseInfo_1() const { return ___purchaseInfo_1; }
	inline UnityChannelPurchaseInfo_t74063925 ** get_address_of_purchaseInfo_1() { return &___purchaseInfo_1; }
	inline void set_purchaseInfo_1(UnityChannelPurchaseInfo_t74063925 * value)
	{
		___purchaseInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASEERROR_T2306817818_H
#ifndef IAPCONFIGURATIONHELPER_T2483224394_H
#define IAPCONFIGURATIONHELPER_T2483224394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPConfigurationHelper
struct  IAPConfigurationHelper_t2483224394  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPCONFIGURATIONHELPER_T2483224394_H
#ifndef IAPBUTTONSTOREMANAGER_T3446654887_H
#define IAPBUTTONSTOREMANAGER_T3446654887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager
struct  IAPButtonStoreManager_t3446654887  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::catalog
	ProductCatalog_t3178009003 * ___catalog_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton> UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::activeButtons
	List_1_t3820967359 * ___activeButtons_2;
	// UnityEngine.Purchasing.IAPListener UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::m_Listener
	IAPListener_t2001792988 * ___m_Listener_3;
	// UnityEngine.Purchasing.IStoreController UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::controller
	RuntimeObject* ___controller_4;
	// UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::extensions
	RuntimeObject* ___extensions_5;

public:
	inline static int32_t get_offset_of_catalog_1() { return static_cast<int32_t>(offsetof(IAPButtonStoreManager_t3446654887, ___catalog_1)); }
	inline ProductCatalog_t3178009003 * get_catalog_1() const { return ___catalog_1; }
	inline ProductCatalog_t3178009003 ** get_address_of_catalog_1() { return &___catalog_1; }
	inline void set_catalog_1(ProductCatalog_t3178009003 * value)
	{
		___catalog_1 = value;
		Il2CppCodeGenWriteBarrier((&___catalog_1), value);
	}

	inline static int32_t get_offset_of_activeButtons_2() { return static_cast<int32_t>(offsetof(IAPButtonStoreManager_t3446654887, ___activeButtons_2)); }
	inline List_1_t3820967359 * get_activeButtons_2() const { return ___activeButtons_2; }
	inline List_1_t3820967359 ** get_address_of_activeButtons_2() { return &___activeButtons_2; }
	inline void set_activeButtons_2(List_1_t3820967359 * value)
	{
		___activeButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___activeButtons_2), value);
	}

	inline static int32_t get_offset_of_m_Listener_3() { return static_cast<int32_t>(offsetof(IAPButtonStoreManager_t3446654887, ___m_Listener_3)); }
	inline IAPListener_t2001792988 * get_m_Listener_3() const { return ___m_Listener_3; }
	inline IAPListener_t2001792988 ** get_address_of_m_Listener_3() { return &___m_Listener_3; }
	inline void set_m_Listener_3(IAPListener_t2001792988 * value)
	{
		___m_Listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Listener_3), value);
	}

	inline static int32_t get_offset_of_controller_4() { return static_cast<int32_t>(offsetof(IAPButtonStoreManager_t3446654887, ___controller_4)); }
	inline RuntimeObject* get_controller_4() const { return ___controller_4; }
	inline RuntimeObject** get_address_of_controller_4() { return &___controller_4; }
	inline void set_controller_4(RuntimeObject* value)
	{
		___controller_4 = value;
		Il2CppCodeGenWriteBarrier((&___controller_4), value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(IAPButtonStoreManager_t3446654887, ___extensions_5)); }
	inline RuntimeObject* get_extensions_5() const { return ___extensions_5; }
	inline RuntimeObject** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(RuntimeObject* value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_5), value);
	}
};

struct IAPButtonStoreManager_t3446654887_StaticFields
{
public:
	// UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::instance
	IAPButtonStoreManager_t3446654887 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(IAPButtonStoreManager_t3446654887_StaticFields, ___instance_0)); }
	inline IAPButtonStoreManager_t3446654887 * get_instance_0() const { return ___instance_0; }
	inline IAPButtonStoreManager_t3446654887 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(IAPButtonStoreManager_t3446654887 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPBUTTONSTOREMANAGER_T3446654887_H
#ifndef ENUMUTILITY_T4065693008_H
#define ENUMUTILITY_T4065693008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumUtility
struct  EnumUtility_t4065693008  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMUTILITY_T4065693008_H
#ifndef PLAYERPREFSUTILITY_T3733645470_H
#define PLAYERPREFSUTILITY_T3733645470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerPrefsUtility
struct  PlayerPrefsUtility_t3733645470  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSUTILITY_T3733645470_H
#ifndef PROBABILITYCALCLATOR_T1670675042_H
#define PROBABILITYCALCLATOR_T1670675042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProbabilityCalclator
struct  ProbabilityCalclator_t1670675042  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROBABILITYCALCLATOR_T1670675042_H
#ifndef STRINGUTILITY_T3266228228_H
#define STRINGUTILITY_T3266228228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StringUtility
struct  StringUtility_t3266228228  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILITY_T3266228228_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef VECTORCALCULATOR_T2681497494_H
#define VECTORCALCULATOR_T2681497494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VectorCalculator
struct  VectorCalculator_t2681497494  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORCALCULATOR_T2681497494_H
#ifndef APPSTORESETTING_T1592337179_H
#define APPSTORESETTING_T1592337179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSetting
struct  AppStoreSetting_t1592337179  : public RuntimeObject
{
public:
	// System.String AppStoresSupport.AppStoreSetting::AppID
	String_t* ___AppID_0;
	// System.String AppStoresSupport.AppStoreSetting::AppKey
	String_t* ___AppKey_1;
	// System.Boolean AppStoresSupport.AppStoreSetting::IsTestMode
	bool ___IsTestMode_2;

public:
	inline static int32_t get_offset_of_AppID_0() { return static_cast<int32_t>(offsetof(AppStoreSetting_t1592337179, ___AppID_0)); }
	inline String_t* get_AppID_0() const { return ___AppID_0; }
	inline String_t** get_address_of_AppID_0() { return &___AppID_0; }
	inline void set_AppID_0(String_t* value)
	{
		___AppID_0 = value;
		Il2CppCodeGenWriteBarrier((&___AppID_0), value);
	}

	inline static int32_t get_offset_of_AppKey_1() { return static_cast<int32_t>(offsetof(AppStoreSetting_t1592337179, ___AppKey_1)); }
	inline String_t* get_AppKey_1() const { return ___AppKey_1; }
	inline String_t** get_address_of_AppKey_1() { return &___AppKey_1; }
	inline void set_AppKey_1(String_t* value)
	{
		___AppKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___AppKey_1), value);
	}

	inline static int32_t get_offset_of_IsTestMode_2() { return static_cast<int32_t>(offsetof(AppStoreSetting_t1592337179, ___IsTestMode_2)); }
	inline bool get_IsTestMode_2() const { return ___IsTestMode_2; }
	inline bool* get_address_of_IsTestMode_2() { return &___IsTestMode_2; }
	inline void set_IsTestMode_2(bool value)
	{
		___IsTestMode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTING_T1592337179_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2073787043_H
#define U3CSTARTU3EC__ITERATOR0_T2073787043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackTitleButtonAssistant/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2073787043  : public RuntimeObject
{
public:
	// BackTitleButtonAssistant BackTitleButtonAssistant/<Start>c__Iterator0::$this
	BackTitleButtonAssistant_t2533991015 * ___U24this_0;
	// System.Object BackTitleButtonAssistant/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BackTitleButtonAssistant/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 BackTitleButtonAssistant/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2073787043, ___U24this_0)); }
	inline BackTitleButtonAssistant_t2533991015 * get_U24this_0() const { return ___U24this_0; }
	inline BackTitleButtonAssistant_t2533991015 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(BackTitleButtonAssistant_t2533991015 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2073787043, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2073787043, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2073787043, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2073787043_H
#ifndef U3CCREATEBLACKTEXTUREU3EC__ITERATOR0_T4152081107_H
#define U3CCREATEBLACKTEXTUREU3EC__ITERATOR0_T4152081107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeTransition/<CreateBlackTexture>c__Iterator0
struct  U3CCreateBlackTextureU3Ec__Iterator0_t4152081107  : public RuntimeObject
{
public:
	// FadeTransition FadeTransition/<CreateBlackTexture>c__Iterator0::$this
	FadeTransition_t2992252162 * ___U24this_0;
	// System.Object FadeTransition/<CreateBlackTexture>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean FadeTransition/<CreateBlackTexture>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 FadeTransition/<CreateBlackTexture>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCreateBlackTextureU3Ec__Iterator0_t4152081107, ___U24this_0)); }
	inline FadeTransition_t2992252162 * get_U24this_0() const { return ___U24this_0; }
	inline FadeTransition_t2992252162 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(FadeTransition_t2992252162 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCreateBlackTextureU3Ec__Iterator0_t4152081107, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCreateBlackTextureU3Ec__Iterator0_t4152081107, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCreateBlackTextureU3Ec__Iterator0_t4152081107, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEBLACKTEXTUREU3EC__ITERATOR0_T4152081107_H
#ifndef TENJIN_T2555426487_H
#define TENJIN_T2555426487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tenjin
struct  Tenjin_t2555426487  : public RuntimeObject
{
public:

public:
};

struct Tenjin_t2555426487_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,BaseTenjin> Tenjin::_instances
	Dictionary_2_t1599024990 * ____instances_0;

public:
	inline static int32_t get_offset_of__instances_0() { return static_cast<int32_t>(offsetof(Tenjin_t2555426487_StaticFields, ____instances_0)); }
	inline Dictionary_2_t1599024990 * get__instances_0() const { return ____instances_0; }
	inline Dictionary_2_t1599024990 ** get_address_of__instances_0() { return &____instances_0; }
	inline void set__instances_0(Dictionary_2_t1599024990 * value)
	{
		____instances_0 = value;
		Il2CppCodeGenWriteBarrier((&____instances_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TENJIN_T2555426487_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CAWAKEU3EC__ANONSTOREY0_T2364586269_H
#define U3CAWAKEU3EC__ANONSTOREY0_T2364586269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/<Awake>c__AnonStorey0
struct  U3CAwakeU3Ec__AnonStorey0_t2364586269  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ConfigurationBuilder IAPDemo/<Awake>c__AnonStorey0::builder
	ConfigurationBuilder_t1618671084 * ___builder_0;
	// System.Action IAPDemo/<Awake>c__AnonStorey0::initializeUnityIap
	Action_t1264377477 * ___initializeUnityIap_1;
	// IAPDemo IAPDemo/<Awake>c__AnonStorey0::$this
	IAPDemo_t3681080565 * ___U24this_2;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2364586269, ___builder_0)); }
	inline ConfigurationBuilder_t1618671084 * get_builder_0() const { return ___builder_0; }
	inline ConfigurationBuilder_t1618671084 ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(ConfigurationBuilder_t1618671084 * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}

	inline static int32_t get_offset_of_initializeUnityIap_1() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2364586269, ___initializeUnityIap_1)); }
	inline Action_t1264377477 * get_initializeUnityIap_1() const { return ___initializeUnityIap_1; }
	inline Action_t1264377477 ** get_address_of_initializeUnityIap_1() { return &___initializeUnityIap_1; }
	inline void set_initializeUnityIap_1(Action_t1264377477 * value)
	{
		___initializeUnityIap_1 = value;
		Il2CppCodeGenWriteBarrier((&___initializeUnityIap_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2364586269, ___U24this_2)); }
	inline IAPDemo_t3681080565 * get_U24this_2() const { return ___U24this_2; }
	inline IAPDemo_t3681080565 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(IAPDemo_t3681080565 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAWAKEU3EC__ANONSTOREY0_T2364586269_H
#ifndef U24ARRAYTYPEU3D8_T3242499063_H
#define U24ARRAYTYPEU3D8_T3242499063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=8
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D8_t3242499063 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D8_t3242499063__padding[8];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D8_T3242499063_H
#ifndef U24ARRAYTYPEU3D580_T353960863_H
#define U24ARRAYTYPEU3D580_T353960863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=580
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D580_t353960863 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D580_t353960863__padding[580];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D580_T353960863_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef STRINGSTRINGKEYVALUEPAIR_T2008113712_H
#define STRINGSTRINGKEYVALUEPAIR_T2008113712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IosTenjin/NativeUtility/StringStringKeyValuePair
struct  StringStringKeyValuePair_t2008113712 
{
public:
	// System.String IosTenjin/NativeUtility/StringStringKeyValuePair::Key
	String_t* ___Key_0;
	// System.String IosTenjin/NativeUtility/StringStringKeyValuePair::Value
	String_t* ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(StringStringKeyValuePair_t2008113712, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(StringStringKeyValuePair_t2008113712, ___Value_1)); }
	inline String_t* get_Value_1() const { return ___Value_1; }
	inline String_t** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(String_t* value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of IosTenjin/NativeUtility/StringStringKeyValuePair
struct StringStringKeyValuePair_t2008113712_marshaled_pinvoke
{
	char* ___Key_0;
	char* ___Value_1;
};
// Native definition for COM marshalling of IosTenjin/NativeUtility/StringStringKeyValuePair
struct StringStringKeyValuePair_t2008113712_marshaled_com
{
	Il2CppChar* ___Key_0;
	Il2CppChar* ___Value_1;
};
#endif // STRINGSTRINGKEYVALUEPAIR_T2008113712_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_2_T1877158062_H
#define UNITYEVENT_2_T1877158062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason>
struct  UnityEvent_2_t1877158062  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1877158062, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1877158062_H
#ifndef UNITYEVENT_1_T4126069563_H
#define UNITYEVENT_1_T4126069563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Purchasing.Product>
struct  UnityEvent_1_t4126069563  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4126069563, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4126069563_H
#ifndef FADESTATE_T3570061009_H
#define FADESTATE_T3570061009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanvasFader/FadeState
struct  FadeState_t3570061009 
{
public:
	// System.Int32 CanvasFader/FadeState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FadeState_t3570061009, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESTATE_T3570061009_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255369  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-4057698FC35E52288B87C31D67A93D5A5AAC27C0
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-68F00C41318114691E02CD7532ACF69A8DBE23C2
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1;
	// <PrivateImplementationDetails>/$ArrayType=580 <PrivateImplementationDetails>::$field-7FB9790B49277F6151D3EB5D555CCF105904DB43
	U24ArrayTypeU3D580_t353960863  ___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2;
	// <PrivateImplementationDetails>/$ArrayType=8 <PrivateImplementationDetails>::$field-D26A27B5531D6252D57917C90488F9C3F7AF8F98
	U24ArrayTypeU3D8_t3242499063  ___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3;

public:
	inline static int32_t get_offset_of_U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0() const { return ___U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0() { return &___U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0; }
	inline void set_U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1() const { return ___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1() { return &___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1; }
	inline void set_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2)); }
	inline U24ArrayTypeU3D580_t353960863  get_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2() const { return ___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2; }
	inline U24ArrayTypeU3D580_t353960863 * get_address_of_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2() { return &___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2; }
	inline void set_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2(U24ArrayTypeU3D580_t353960863  value)
	{
		___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3)); }
	inline U24ArrayTypeU3D8_t3242499063  get_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3() const { return ___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3; }
	inline U24ArrayTypeU3D8_t3242499063 * get_address_of_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3() { return &___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3; }
	inline void set_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3(U24ArrayTypeU3D8_t3242499063  value)
	{
		___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef STATUS_T56047631_H
#define STATUS_T56047631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeTransition/Status
struct  Status_t56047631 
{
public:
	// System.Int32 FadeTransition/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t56047631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T56047631_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef LOCATION_T1923176401_H
#define LOCATION_T1923176401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackTitleButtonAssistant/Location
struct  Location_t1923176401 
{
public:
	// System.Int32 BackTitleButtonAssistant/Location::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Location_t1923176401, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T1923176401_H
#ifndef ONPURCHASEFAILEDEVENT_T800864861_H
#define ONPURCHASEFAILEDEVENT_T800864861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct  OnPurchaseFailedEvent_t800864861  : public UnityEvent_2_t1877158062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASEFAILEDEVENT_T800864861_H
#ifndef ONPURCHASECOMPLETEDEVENT_T1675809258_H
#define ONPURCHASECOMPLETEDEVENT_T1675809258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct  OnPurchaseCompletedEvent_t1675809258  : public UnityEvent_1_t4126069563
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASECOMPLETEDEVENT_T1675809258_H
#ifndef BUTTONTYPE_T908070482_H
#define BUTTONTYPE_T908070482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton/ButtonType
struct  ButtonType_t908070482 
{
public:
	// System.Int32 UnityEngine.Purchasing.IAPButton/ButtonType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ButtonType_t908070482, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTYPE_T908070482_H
#ifndef ONPURCHASECOMPLETEDEVENT_T3721407765_H
#define ONPURCHASECOMPLETEDEVENT_T3721407765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent
struct  OnPurchaseCompletedEvent_t3721407765  : public UnityEvent_1_t4126069563
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASECOMPLETEDEVENT_T3721407765_H
#ifndef ONPURCHASEFAILEDEVENT_T1729542224_H
#define ONPURCHASEFAILEDEVENT_T1729542224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent
struct  OnPurchaseFailedEvent_t1729542224  : public UnityEvent_2_t1877158062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASEFAILEDEVENT_T1729542224_H
#ifndef GAMETYPE_T3504513020_H
#define GAMETYPE_T3504513020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameType
struct  GameType_t3504513020 
{
public:
	// System.Int32 GameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameType_t3504513020, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMETYPE_T3504513020_H
#ifndef RESULTTYPE_T3090920659_H
#define RESULTTYPE_T3090920659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultUISwitcher/ResultType
struct  ResultType_t3090920659 
{
public:
	// System.Int32 ResultUISwitcher/ResultType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResultType_t3090920659, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTTYPE_T3090920659_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DEFERREDDEEPLINKDELEGATE_T4234501760_H
#define DEFERREDDEEPLINKDELEGATE_T4234501760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tenjin/DeferredDeeplinkDelegate
struct  DeferredDeeplinkDelegate_t4234501760  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFERREDDEEPLINKDELEGATE_T4234501760_H
#ifndef DEEPLINKHANDLERNATIVEDELEGATE_T1988926173_H
#define DEEPLINKHANDLERNATIVEDELEGATE_T1988926173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IosTenjin/DeepLinkHandlerNativeDelegate
struct  DeepLinkHandlerNativeDelegate_t1988926173  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEEPLINKHANDLERNATIVEDELEGATE_T1988926173_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef APPSTORESETTINGS_T2325796953_H
#define APPSTORESETTINGS_T2325796953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSettings
struct  AppStoreSettings_t2325796953  : public ScriptableObject_t2528358522
{
public:
	// System.String AppStoresSupport.AppStoreSettings::UnityClientID
	String_t* ___UnityClientID_2;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientKey
	String_t* ___UnityClientKey_3;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientRSAPublicKey
	String_t* ___UnityClientRSAPublicKey_4;
	// AppStoresSupport.AppStoreSetting AppStoresSupport.AppStoreSettings::XiaomiAppStoreSetting
	AppStoreSetting_t1592337179 * ___XiaomiAppStoreSetting_5;

public:
	inline static int32_t get_offset_of_UnityClientID_2() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___UnityClientID_2)); }
	inline String_t* get_UnityClientID_2() const { return ___UnityClientID_2; }
	inline String_t** get_address_of_UnityClientID_2() { return &___UnityClientID_2; }
	inline void set_UnityClientID_2(String_t* value)
	{
		___UnityClientID_2 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientID_2), value);
	}

	inline static int32_t get_offset_of_UnityClientKey_3() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___UnityClientKey_3)); }
	inline String_t* get_UnityClientKey_3() const { return ___UnityClientKey_3; }
	inline String_t** get_address_of_UnityClientKey_3() { return &___UnityClientKey_3; }
	inline void set_UnityClientKey_3(String_t* value)
	{
		___UnityClientKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientKey_3), value);
	}

	inline static int32_t get_offset_of_UnityClientRSAPublicKey_4() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___UnityClientRSAPublicKey_4)); }
	inline String_t* get_UnityClientRSAPublicKey_4() const { return ___UnityClientRSAPublicKey_4; }
	inline String_t** get_address_of_UnityClientRSAPublicKey_4() { return &___UnityClientRSAPublicKey_4; }
	inline void set_UnityClientRSAPublicKey_4(String_t* value)
	{
		___UnityClientRSAPublicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientRSAPublicKey_4), value);
	}

	inline static int32_t get_offset_of_XiaomiAppStoreSetting_5() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___XiaomiAppStoreSetting_5)); }
	inline AppStoreSetting_t1592337179 * get_XiaomiAppStoreSetting_5() const { return ___XiaomiAppStoreSetting_5; }
	inline AppStoreSetting_t1592337179 ** get_address_of_XiaomiAppStoreSetting_5() { return &___XiaomiAppStoreSetting_5; }
	inline void set_XiaomiAppStoreSetting_5(AppStoreSetting_t1592337179 * value)
	{
		___XiaomiAppStoreSetting_5 = value;
		Il2CppCodeGenWriteBarrier((&___XiaomiAppStoreSetting_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTINGS_T2325796953_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef IAPLISTENER_T2001792988_H
#define IAPLISTENER_T2001792988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener
struct  IAPListener_t2001792988  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.Purchasing.IAPListener::consumePurchase
	bool ___consumePurchase_2;
	// System.Boolean UnityEngine.Purchasing.IAPListener::dontDestroyOnLoad
	bool ___dontDestroyOnLoad_3;
	// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent UnityEngine.Purchasing.IAPListener::onPurchaseComplete
	OnPurchaseCompletedEvent_t1675809258 * ___onPurchaseComplete_4;
	// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent UnityEngine.Purchasing.IAPListener::onPurchaseFailed
	OnPurchaseFailedEvent_t800864861 * ___onPurchaseFailed_5;

public:
	inline static int32_t get_offset_of_consumePurchase_2() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___consumePurchase_2)); }
	inline bool get_consumePurchase_2() const { return ___consumePurchase_2; }
	inline bool* get_address_of_consumePurchase_2() { return &___consumePurchase_2; }
	inline void set_consumePurchase_2(bool value)
	{
		___consumePurchase_2 = value;
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_3() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___dontDestroyOnLoad_3)); }
	inline bool get_dontDestroyOnLoad_3() const { return ___dontDestroyOnLoad_3; }
	inline bool* get_address_of_dontDestroyOnLoad_3() { return &___dontDestroyOnLoad_3; }
	inline void set_dontDestroyOnLoad_3(bool value)
	{
		___dontDestroyOnLoad_3 = value;
	}

	inline static int32_t get_offset_of_onPurchaseComplete_4() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___onPurchaseComplete_4)); }
	inline OnPurchaseCompletedEvent_t1675809258 * get_onPurchaseComplete_4() const { return ___onPurchaseComplete_4; }
	inline OnPurchaseCompletedEvent_t1675809258 ** get_address_of_onPurchaseComplete_4() { return &___onPurchaseComplete_4; }
	inline void set_onPurchaseComplete_4(OnPurchaseCompletedEvent_t1675809258 * value)
	{
		___onPurchaseComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseComplete_4), value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_5() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___onPurchaseFailed_5)); }
	inline OnPurchaseFailedEvent_t800864861 * get_onPurchaseFailed_5() const { return ___onPurchaseFailed_5; }
	inline OnPurchaseFailedEvent_t800864861 ** get_address_of_onPurchaseFailed_5() { return &___onPurchaseFailed_5; }
	inline void set_onPurchaseFailed_5(OnPurchaseFailedEvent_t800864861 * value)
	{
		___onPurchaseFailed_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseFailed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPLISTENER_T2001792988_H
#ifndef RESULTLABEL_T3723221400_H
#define RESULTLABEL_T3723221400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultLabel
struct  ResultLabel_t3723221400  : public MonoBehaviour_t3962482529
{
public:
	// System.String ResultLabel::_textFormat
	String_t* ____textFormat_2;

public:
	inline static int32_t get_offset_of__textFormat_2() { return static_cast<int32_t>(offsetof(ResultLabel_t3723221400, ____textFormat_2)); }
	inline String_t* get__textFormat_2() const { return ____textFormat_2; }
	inline String_t** get_address_of__textFormat_2() { return &____textFormat_2; }
	inline void set__textFormat_2(String_t* value)
	{
		____textFormat_2 = value;
		Il2CppCodeGenWriteBarrier((&____textFormat_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTLABEL_T3723221400_H
#ifndef NEWRECORDOBJECT_T1797845672_H
#define NEWRECORDOBJECT_T1797845672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewRecordObject
struct  NewRecordObject_t1797845672  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWRECORDOBJECT_T1797845672_H
#ifndef LOADSCENEBACK_T3289937302_H
#define LOADSCENEBACK_T3289937302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadSceneBack
struct  LoadSceneBack_t3289937302  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEBACK_T3289937302_H
#ifndef BUTTONASSISTANT_T3922690862_H
#define BUTTONASSISTANT_T3922690862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonAssistant
struct  ButtonAssistant_t3922690862  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Collider ButtonAssistant::_collider
	Collider_t1773347010 * ____collider_2;
	// UIButton ButtonAssistant::_button
	UIButton_t1100396938 * ____button_3;
	// UISprite ButtonAssistant::_image
	UISprite_t194114938 * ____image_4;

public:
	inline static int32_t get_offset_of__collider_2() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____collider_2)); }
	inline Collider_t1773347010 * get__collider_2() const { return ____collider_2; }
	inline Collider_t1773347010 ** get_address_of__collider_2() { return &____collider_2; }
	inline void set__collider_2(Collider_t1773347010 * value)
	{
		____collider_2 = value;
		Il2CppCodeGenWriteBarrier((&____collider_2), value);
	}

	inline static int32_t get_offset_of__button_3() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____button_3)); }
	inline UIButton_t1100396938 * get__button_3() const { return ____button_3; }
	inline UIButton_t1100396938 ** get_address_of__button_3() { return &____button_3; }
	inline void set__button_3(UIButton_t1100396938 * value)
	{
		____button_3 = value;
		Il2CppCodeGenWriteBarrier((&____button_3), value);
	}

	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____image_4)); }
	inline UISprite_t194114938 * get__image_4() const { return ____image_4; }
	inline UISprite_t194114938 ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(UISprite_t194114938 * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONASSISTANT_T3922690862_H
#ifndef LOADPROGRESSBAR_T2582436008_H
#define LOADPROGRESSBAR_T2582436008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadProgressBar
struct  LoadProgressBar_t2582436008  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Scrollbar LoadProgressBar::_scrollBar
	Scrollbar_t1494447233 * ____scrollBar_2;
	// UnityEngine.GameObject LoadProgressBar::_loadTitleLabel
	GameObject_t1113636619 * ____loadTitleLabel_3;
	// LoadEndButtonAssistant LoadProgressBar::_loadEndButton
	LoadEndButtonAssistant_t4277748694 * ____loadEndButton_4;
	// System.Single LoadProgressBar::_totalTimePer
	float ____totalTimePer_5;
	// System.Single LoadProgressBar::_loadTime
	float ____loadTime_6;
	// System.Collections.Generic.List`1<System.Single> LoadProgressBar::_loadTimePerList
	List_1_t2869341516 * ____loadTimePerList_9;
	// System.Int32 LoadProgressBar::_loadNo
	int32_t ____loadNo_10;
	// System.Collections.Generic.List`1<System.Single> LoadProgressBar::_waitTimePerList
	List_1_t2869341516 * ____waitTimePerList_11;
	// System.Boolean LoadProgressBar::_isWaiting
	bool ____isWaiting_12;
	// System.Int32 LoadProgressBar::_waitingNo
	int32_t ____waitingNo_13;
	// System.Collections.Generic.List`1<System.Single> LoadProgressBar::_progressPerList
	List_1_t2869341516 * ____progressPerList_14;
	// System.Single LoadProgressBar::_totalProgressPer
	float ____totalProgressPer_15;
	// System.Single LoadProgressBar::_progress
	float ____progress_16;
	// System.Single LoadProgressBar::_addtionalProgress
	float ____addtionalProgress_17;
	// System.Single LoadProgressBar::_updateLimit
	float ____updateLimit_18;
	// System.Single LoadProgressBar::_updateCount
	float ____updateCount_19;

public:
	inline static int32_t get_offset_of__scrollBar_2() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____scrollBar_2)); }
	inline Scrollbar_t1494447233 * get__scrollBar_2() const { return ____scrollBar_2; }
	inline Scrollbar_t1494447233 ** get_address_of__scrollBar_2() { return &____scrollBar_2; }
	inline void set__scrollBar_2(Scrollbar_t1494447233 * value)
	{
		____scrollBar_2 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBar_2), value);
	}

	inline static int32_t get_offset_of__loadTitleLabel_3() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____loadTitleLabel_3)); }
	inline GameObject_t1113636619 * get__loadTitleLabel_3() const { return ____loadTitleLabel_3; }
	inline GameObject_t1113636619 ** get_address_of__loadTitleLabel_3() { return &____loadTitleLabel_3; }
	inline void set__loadTitleLabel_3(GameObject_t1113636619 * value)
	{
		____loadTitleLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&____loadTitleLabel_3), value);
	}

	inline static int32_t get_offset_of__loadEndButton_4() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____loadEndButton_4)); }
	inline LoadEndButtonAssistant_t4277748694 * get__loadEndButton_4() const { return ____loadEndButton_4; }
	inline LoadEndButtonAssistant_t4277748694 ** get_address_of__loadEndButton_4() { return &____loadEndButton_4; }
	inline void set__loadEndButton_4(LoadEndButtonAssistant_t4277748694 * value)
	{
		____loadEndButton_4 = value;
		Il2CppCodeGenWriteBarrier((&____loadEndButton_4), value);
	}

	inline static int32_t get_offset_of__totalTimePer_5() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____totalTimePer_5)); }
	inline float get__totalTimePer_5() const { return ____totalTimePer_5; }
	inline float* get_address_of__totalTimePer_5() { return &____totalTimePer_5; }
	inline void set__totalTimePer_5(float value)
	{
		____totalTimePer_5 = value;
	}

	inline static int32_t get_offset_of__loadTime_6() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____loadTime_6)); }
	inline float get__loadTime_6() const { return ____loadTime_6; }
	inline float* get_address_of__loadTime_6() { return &____loadTime_6; }
	inline void set__loadTime_6(float value)
	{
		____loadTime_6 = value;
	}

	inline static int32_t get_offset_of__loadTimePerList_9() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____loadTimePerList_9)); }
	inline List_1_t2869341516 * get__loadTimePerList_9() const { return ____loadTimePerList_9; }
	inline List_1_t2869341516 ** get_address_of__loadTimePerList_9() { return &____loadTimePerList_9; }
	inline void set__loadTimePerList_9(List_1_t2869341516 * value)
	{
		____loadTimePerList_9 = value;
		Il2CppCodeGenWriteBarrier((&____loadTimePerList_9), value);
	}

	inline static int32_t get_offset_of__loadNo_10() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____loadNo_10)); }
	inline int32_t get__loadNo_10() const { return ____loadNo_10; }
	inline int32_t* get_address_of__loadNo_10() { return &____loadNo_10; }
	inline void set__loadNo_10(int32_t value)
	{
		____loadNo_10 = value;
	}

	inline static int32_t get_offset_of__waitTimePerList_11() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____waitTimePerList_11)); }
	inline List_1_t2869341516 * get__waitTimePerList_11() const { return ____waitTimePerList_11; }
	inline List_1_t2869341516 ** get_address_of__waitTimePerList_11() { return &____waitTimePerList_11; }
	inline void set__waitTimePerList_11(List_1_t2869341516 * value)
	{
		____waitTimePerList_11 = value;
		Il2CppCodeGenWriteBarrier((&____waitTimePerList_11), value);
	}

	inline static int32_t get_offset_of__isWaiting_12() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____isWaiting_12)); }
	inline bool get__isWaiting_12() const { return ____isWaiting_12; }
	inline bool* get_address_of__isWaiting_12() { return &____isWaiting_12; }
	inline void set__isWaiting_12(bool value)
	{
		____isWaiting_12 = value;
	}

	inline static int32_t get_offset_of__waitingNo_13() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____waitingNo_13)); }
	inline int32_t get__waitingNo_13() const { return ____waitingNo_13; }
	inline int32_t* get_address_of__waitingNo_13() { return &____waitingNo_13; }
	inline void set__waitingNo_13(int32_t value)
	{
		____waitingNo_13 = value;
	}

	inline static int32_t get_offset_of__progressPerList_14() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____progressPerList_14)); }
	inline List_1_t2869341516 * get__progressPerList_14() const { return ____progressPerList_14; }
	inline List_1_t2869341516 ** get_address_of__progressPerList_14() { return &____progressPerList_14; }
	inline void set__progressPerList_14(List_1_t2869341516 * value)
	{
		____progressPerList_14 = value;
		Il2CppCodeGenWriteBarrier((&____progressPerList_14), value);
	}

	inline static int32_t get_offset_of__totalProgressPer_15() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____totalProgressPer_15)); }
	inline float get__totalProgressPer_15() const { return ____totalProgressPer_15; }
	inline float* get_address_of__totalProgressPer_15() { return &____totalProgressPer_15; }
	inline void set__totalProgressPer_15(float value)
	{
		____totalProgressPer_15 = value;
	}

	inline static int32_t get_offset_of__progress_16() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____progress_16)); }
	inline float get__progress_16() const { return ____progress_16; }
	inline float* get_address_of__progress_16() { return &____progress_16; }
	inline void set__progress_16(float value)
	{
		____progress_16 = value;
	}

	inline static int32_t get_offset_of__addtionalProgress_17() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____addtionalProgress_17)); }
	inline float get__addtionalProgress_17() const { return ____addtionalProgress_17; }
	inline float* get_address_of__addtionalProgress_17() { return &____addtionalProgress_17; }
	inline void set__addtionalProgress_17(float value)
	{
		____addtionalProgress_17 = value;
	}

	inline static int32_t get_offset_of__updateLimit_18() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____updateLimit_18)); }
	inline float get__updateLimit_18() const { return ____updateLimit_18; }
	inline float* get_address_of__updateLimit_18() { return &____updateLimit_18; }
	inline void set__updateLimit_18(float value)
	{
		____updateLimit_18 = value;
	}

	inline static int32_t get_offset_of__updateCount_19() { return static_cast<int32_t>(offsetof(LoadProgressBar_t2582436008, ____updateCount_19)); }
	inline float get__updateCount_19() const { return ____updateCount_19; }
	inline float* get_address_of__updateCount_19() { return &____updateCount_19; }
	inline void set__updateCount_19(float value)
	{
		____updateCount_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADPROGRESSBAR_T2582436008_H
#ifndef LEVELSELECTMENU_T2404780637_H
#define LEVELSELECTMENU_T2404780637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelectMenu
struct  LevelSelectMenu_t2404780637  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 LevelSelectMenu::_buttonNumInTable
	Vector2_t2156229523  ____buttonNumInTable_2;
	// UnityEngine.Vector2 LevelSelectMenu::_space
	Vector2_t2156229523  ____space_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> LevelSelectMenu::_tableList
	List_1_t2585711361 * ____tableList_4;
	// System.Int32 LevelSelectMenu::_currentTableNo
	int32_t ____currentTableNo_5;
	// ButtonAssistant LevelSelectMenu::_backTableButton
	ButtonAssistant_t3922690862 * ____backTableButton_6;
	// ButtonAssistant LevelSelectMenu::_nextTableButton
	ButtonAssistant_t3922690862 * ____nextTableButton_7;

public:
	inline static int32_t get_offset_of__buttonNumInTable_2() { return static_cast<int32_t>(offsetof(LevelSelectMenu_t2404780637, ____buttonNumInTable_2)); }
	inline Vector2_t2156229523  get__buttonNumInTable_2() const { return ____buttonNumInTable_2; }
	inline Vector2_t2156229523 * get_address_of__buttonNumInTable_2() { return &____buttonNumInTable_2; }
	inline void set__buttonNumInTable_2(Vector2_t2156229523  value)
	{
		____buttonNumInTable_2 = value;
	}

	inline static int32_t get_offset_of__space_3() { return static_cast<int32_t>(offsetof(LevelSelectMenu_t2404780637, ____space_3)); }
	inline Vector2_t2156229523  get__space_3() const { return ____space_3; }
	inline Vector2_t2156229523 * get_address_of__space_3() { return &____space_3; }
	inline void set__space_3(Vector2_t2156229523  value)
	{
		____space_3 = value;
	}

	inline static int32_t get_offset_of__tableList_4() { return static_cast<int32_t>(offsetof(LevelSelectMenu_t2404780637, ____tableList_4)); }
	inline List_1_t2585711361 * get__tableList_4() const { return ____tableList_4; }
	inline List_1_t2585711361 ** get_address_of__tableList_4() { return &____tableList_4; }
	inline void set__tableList_4(List_1_t2585711361 * value)
	{
		____tableList_4 = value;
		Il2CppCodeGenWriteBarrier((&____tableList_4), value);
	}

	inline static int32_t get_offset_of__currentTableNo_5() { return static_cast<int32_t>(offsetof(LevelSelectMenu_t2404780637, ____currentTableNo_5)); }
	inline int32_t get__currentTableNo_5() const { return ____currentTableNo_5; }
	inline int32_t* get_address_of__currentTableNo_5() { return &____currentTableNo_5; }
	inline void set__currentTableNo_5(int32_t value)
	{
		____currentTableNo_5 = value;
	}

	inline static int32_t get_offset_of__backTableButton_6() { return static_cast<int32_t>(offsetof(LevelSelectMenu_t2404780637, ____backTableButton_6)); }
	inline ButtonAssistant_t3922690862 * get__backTableButton_6() const { return ____backTableButton_6; }
	inline ButtonAssistant_t3922690862 ** get_address_of__backTableButton_6() { return &____backTableButton_6; }
	inline void set__backTableButton_6(ButtonAssistant_t3922690862 * value)
	{
		____backTableButton_6 = value;
		Il2CppCodeGenWriteBarrier((&____backTableButton_6), value);
	}

	inline static int32_t get_offset_of__nextTableButton_7() { return static_cast<int32_t>(offsetof(LevelSelectMenu_t2404780637, ____nextTableButton_7)); }
	inline ButtonAssistant_t3922690862 * get__nextTableButton_7() const { return ____nextTableButton_7; }
	inline ButtonAssistant_t3922690862 ** get_address_of__nextTableButton_7() { return &____nextTableButton_7; }
	inline void set__nextTableButton_7(ButtonAssistant_t3922690862 * value)
	{
		____nextTableButton_7 = value;
		Il2CppCodeGenWriteBarrier((&____nextTableButton_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSELECTMENU_T2404780637_H
#ifndef IAPDEMOPRODUCTUI_T922953754_H
#define IAPDEMOPRODUCTUI_T922953754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemoProductUI
struct  IAPDemoProductUI_t922953754  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button IAPDemoProductUI::purchaseButton
	Button_t4055032469 * ___purchaseButton_2;
	// UnityEngine.UI.Button IAPDemoProductUI::receiptButton
	Button_t4055032469 * ___receiptButton_3;
	// UnityEngine.UI.Text IAPDemoProductUI::titleText
	Text_t1901882714 * ___titleText_4;
	// UnityEngine.UI.Text IAPDemoProductUI::descriptionText
	Text_t1901882714 * ___descriptionText_5;
	// UnityEngine.UI.Text IAPDemoProductUI::priceText
	Text_t1901882714 * ___priceText_6;
	// UnityEngine.UI.Text IAPDemoProductUI::statusText
	Text_t1901882714 * ___statusText_7;
	// System.String IAPDemoProductUI::m_ProductID
	String_t* ___m_ProductID_8;
	// System.Action`1<System.String> IAPDemoProductUI::m_PurchaseCallback
	Action_1_t2019918284 * ___m_PurchaseCallback_9;
	// System.String IAPDemoProductUI::m_Receipt
	String_t* ___m_Receipt_10;

public:
	inline static int32_t get_offset_of_purchaseButton_2() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___purchaseButton_2)); }
	inline Button_t4055032469 * get_purchaseButton_2() const { return ___purchaseButton_2; }
	inline Button_t4055032469 ** get_address_of_purchaseButton_2() { return &___purchaseButton_2; }
	inline void set_purchaseButton_2(Button_t4055032469 * value)
	{
		___purchaseButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseButton_2), value);
	}

	inline static int32_t get_offset_of_receiptButton_3() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___receiptButton_3)); }
	inline Button_t4055032469 * get_receiptButton_3() const { return ___receiptButton_3; }
	inline Button_t4055032469 ** get_address_of_receiptButton_3() { return &___receiptButton_3; }
	inline void set_receiptButton_3(Button_t4055032469 * value)
	{
		___receiptButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___receiptButton_3), value);
	}

	inline static int32_t get_offset_of_titleText_4() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___titleText_4)); }
	inline Text_t1901882714 * get_titleText_4() const { return ___titleText_4; }
	inline Text_t1901882714 ** get_address_of_titleText_4() { return &___titleText_4; }
	inline void set_titleText_4(Text_t1901882714 * value)
	{
		___titleText_4 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_4), value);
	}

	inline static int32_t get_offset_of_descriptionText_5() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___descriptionText_5)); }
	inline Text_t1901882714 * get_descriptionText_5() const { return ___descriptionText_5; }
	inline Text_t1901882714 ** get_address_of_descriptionText_5() { return &___descriptionText_5; }
	inline void set_descriptionText_5(Text_t1901882714 * value)
	{
		___descriptionText_5 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_5), value);
	}

	inline static int32_t get_offset_of_priceText_6() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___priceText_6)); }
	inline Text_t1901882714 * get_priceText_6() const { return ___priceText_6; }
	inline Text_t1901882714 ** get_address_of_priceText_6() { return &___priceText_6; }
	inline void set_priceText_6(Text_t1901882714 * value)
	{
		___priceText_6 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_6), value);
	}

	inline static int32_t get_offset_of_statusText_7() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___statusText_7)); }
	inline Text_t1901882714 * get_statusText_7() const { return ___statusText_7; }
	inline Text_t1901882714 ** get_address_of_statusText_7() { return &___statusText_7; }
	inline void set_statusText_7(Text_t1901882714 * value)
	{
		___statusText_7 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_7), value);
	}

	inline static int32_t get_offset_of_m_ProductID_8() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___m_ProductID_8)); }
	inline String_t* get_m_ProductID_8() const { return ___m_ProductID_8; }
	inline String_t** get_address_of_m_ProductID_8() { return &___m_ProductID_8; }
	inline void set_m_ProductID_8(String_t* value)
	{
		___m_ProductID_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductID_8), value);
	}

	inline static int32_t get_offset_of_m_PurchaseCallback_9() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___m_PurchaseCallback_9)); }
	inline Action_1_t2019918284 * get_m_PurchaseCallback_9() const { return ___m_PurchaseCallback_9; }
	inline Action_1_t2019918284 ** get_address_of_m_PurchaseCallback_9() { return &___m_PurchaseCallback_9; }
	inline void set_m_PurchaseCallback_9(Action_1_t2019918284 * value)
	{
		___m_PurchaseCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseCallback_9), value);
	}

	inline static int32_t get_offset_of_m_Receipt_10() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___m_Receipt_10)); }
	inline String_t* get_m_Receipt_10() const { return ___m_Receipt_10; }
	inline String_t** get_address_of_m_Receipt_10() { return &___m_Receipt_10; }
	inline void set_m_Receipt_10(String_t* value)
	{
		___m_Receipt_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Receipt_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPDEMOPRODUCTUI_T922953754_H
#ifndef PAUSEUI_T2388357271_H
#define PAUSEUI_T2388357271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseUI
struct  PauseUI_t2388357271  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PauseUI::_canvas
	GameObject_t1113636619 * ____canvas_2;
	// ButtonAssistant PauseUI::_pauseButtonAssistant
	ButtonAssistant_t3922690862 * ____pauseButtonAssistant_3;
	// ButtonAssistant PauseUI::_resumeButtonAssistant
	ButtonAssistant_t3922690862 * ____resumeButtonAssistant_4;

public:
	inline static int32_t get_offset_of__canvas_2() { return static_cast<int32_t>(offsetof(PauseUI_t2388357271, ____canvas_2)); }
	inline GameObject_t1113636619 * get__canvas_2() const { return ____canvas_2; }
	inline GameObject_t1113636619 ** get_address_of__canvas_2() { return &____canvas_2; }
	inline void set__canvas_2(GameObject_t1113636619 * value)
	{
		____canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_2), value);
	}

	inline static int32_t get_offset_of__pauseButtonAssistant_3() { return static_cast<int32_t>(offsetof(PauseUI_t2388357271, ____pauseButtonAssistant_3)); }
	inline ButtonAssistant_t3922690862 * get__pauseButtonAssistant_3() const { return ____pauseButtonAssistant_3; }
	inline ButtonAssistant_t3922690862 ** get_address_of__pauseButtonAssistant_3() { return &____pauseButtonAssistant_3; }
	inline void set__pauseButtonAssistant_3(ButtonAssistant_t3922690862 * value)
	{
		____pauseButtonAssistant_3 = value;
		Il2CppCodeGenWriteBarrier((&____pauseButtonAssistant_3), value);
	}

	inline static int32_t get_offset_of__resumeButtonAssistant_4() { return static_cast<int32_t>(offsetof(PauseUI_t2388357271, ____resumeButtonAssistant_4)); }
	inline ButtonAssistant_t3922690862 * get__resumeButtonAssistant_4() const { return ____resumeButtonAssistant_4; }
	inline ButtonAssistant_t3922690862 ** get_address_of__resumeButtonAssistant_4() { return &____resumeButtonAssistant_4; }
	inline void set__resumeButtonAssistant_4(ButtonAssistant_t3922690862 * value)
	{
		____resumeButtonAssistant_4 = value;
		Il2CppCodeGenWriteBarrier((&____resumeButtonAssistant_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEUI_T2388357271_H
#ifndef GAMESTAGENOLABEL_T3553093228_H
#define GAMESTAGENOLABEL_T3553093228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameStageNoLabel
struct  GameStageNoLabel_t3553093228  : public MonoBehaviour_t3962482529
{
public:
	// System.String GameStageNoLabel::_textFormat
	String_t* ____textFormat_2;

public:
	inline static int32_t get_offset_of__textFormat_2() { return static_cast<int32_t>(offsetof(GameStageNoLabel_t3553093228, ____textFormat_2)); }
	inline String_t* get__textFormat_2() const { return ____textFormat_2; }
	inline String_t** get_address_of__textFormat_2() { return &____textFormat_2; }
	inline void set__textFormat_2(String_t* value)
	{
		____textFormat_2 = value;
		Il2CppCodeGenWriteBarrier((&____textFormat_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESTAGENOLABEL_T3553093228_H
#ifndef GAMESCORELABEL_T3714137216_H
#define GAMESCORELABEL_T3714137216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameScoreLabel
struct  GameScoreLabel_t3714137216  : public MonoBehaviour_t3962482529
{
public:
	// System.String GameScoreLabel::_textFormat
	String_t* ____textFormat_2;
	// UILabel GameScoreLabel::_label
	UILabel_t3248798549 * ____label_3;

public:
	inline static int32_t get_offset_of__textFormat_2() { return static_cast<int32_t>(offsetof(GameScoreLabel_t3714137216, ____textFormat_2)); }
	inline String_t* get__textFormat_2() const { return ____textFormat_2; }
	inline String_t** get_address_of__textFormat_2() { return &____textFormat_2; }
	inline void set__textFormat_2(String_t* value)
	{
		____textFormat_2 = value;
		Il2CppCodeGenWriteBarrier((&____textFormat_2), value);
	}

	inline static int32_t get_offset_of__label_3() { return static_cast<int32_t>(offsetof(GameScoreLabel_t3714137216, ____label_3)); }
	inline UILabel_t3248798549 * get__label_3() const { return ____label_3; }
	inline UILabel_t3248798549 ** get_address_of__label_3() { return &____label_3; }
	inline void set__label_3(UILabel_t3248798549 * value)
	{
		____label_3 = value;
		Il2CppCodeGenWriteBarrier((&____label_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESCORELABEL_T3714137216_H
#ifndef GAMEHELP_T51737220_H
#define GAMEHELP_T51737220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameHelp
struct  GameHelp_t51737220  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEHELP_T51737220_H
#ifndef MONOBEHAVIOURWITHINIT_T1117120792_H
#define MONOBEHAVIOURWITHINIT_T1117120792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoBehaviourWithInit
struct  MonoBehaviourWithInit_t1117120792  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MonoBehaviourWithInit::_isInitialized
	bool ____isInitialized_2;

public:
	inline static int32_t get_offset_of__isInitialized_2() { return static_cast<int32_t>(offsetof(MonoBehaviourWithInit_t1117120792, ____isInitialized_2)); }
	inline bool get__isInitialized_2() const { return ____isInitialized_2; }
	inline bool* get_address_of__isInitialized_2() { return &____isInitialized_2; }
	inline void set__isInitialized_2(bool value)
	{
		____isInitialized_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURWITHINIT_T1117120792_H
#ifndef FADETRANSITION_T2992252162_H
#define FADETRANSITION_T2992252162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeTransition
struct  FadeTransition_t2992252162  : public MonoBehaviour_t3962482529
{
public:
	// FadeTransition/Status FadeTransition::_status
	int32_t ____status_3;
	// UnityEngine.Texture2D FadeTransition::_blackTexture
	Texture2D_t3840446185 * ____blackTexture_4;
	// System.Single FadeTransition::_fadeAlpha
	float ____fadeAlpha_5;
	// System.Boolean FadeTransition::_isFading
	bool ____isFading_6;

public:
	inline static int32_t get_offset_of__status_3() { return static_cast<int32_t>(offsetof(FadeTransition_t2992252162, ____status_3)); }
	inline int32_t get__status_3() const { return ____status_3; }
	inline int32_t* get_address_of__status_3() { return &____status_3; }
	inline void set__status_3(int32_t value)
	{
		____status_3 = value;
	}

	inline static int32_t get_offset_of__blackTexture_4() { return static_cast<int32_t>(offsetof(FadeTransition_t2992252162, ____blackTexture_4)); }
	inline Texture2D_t3840446185 * get__blackTexture_4() const { return ____blackTexture_4; }
	inline Texture2D_t3840446185 ** get_address_of__blackTexture_4() { return &____blackTexture_4; }
	inline void set__blackTexture_4(Texture2D_t3840446185 * value)
	{
		____blackTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&____blackTexture_4), value);
	}

	inline static int32_t get_offset_of__fadeAlpha_5() { return static_cast<int32_t>(offsetof(FadeTransition_t2992252162, ____fadeAlpha_5)); }
	inline float get__fadeAlpha_5() const { return ____fadeAlpha_5; }
	inline float* get_address_of__fadeAlpha_5() { return &____fadeAlpha_5; }
	inline void set__fadeAlpha_5(float value)
	{
		____fadeAlpha_5 = value;
	}

	inline static int32_t get_offset_of__isFading_6() { return static_cast<int32_t>(offsetof(FadeTransition_t2992252162, ____isFading_6)); }
	inline bool get__isFading_6() const { return ____isFading_6; }
	inline bool* get_address_of__isFading_6() { return &____isFading_6; }
	inline void set__isFading_6(bool value)
	{
		____isFading_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADETRANSITION_T2992252162_H
#ifndef BESTLABEL_T2479364186_H
#define BESTLABEL_T2479364186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestLabel
struct  BestLabel_t2479364186  : public MonoBehaviour_t3962482529
{
public:
	// System.String BestLabel::_textFormat
	String_t* ____textFormat_2;

public:
	inline static int32_t get_offset_of__textFormat_2() { return static_cast<int32_t>(offsetof(BestLabel_t2479364186, ____textFormat_2)); }
	inline String_t* get__textFormat_2() const { return ____textFormat_2; }
	inline String_t** get_address_of__textFormat_2() { return &____textFormat_2; }
	inline void set__textFormat_2(String_t* value)
	{
		____textFormat_2 = value;
		Il2CppCodeGenWriteBarrier((&____textFormat_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BESTLABEL_T2479364186_H
#ifndef RESULTUISWITCHER_T101865093_H
#define RESULTUISWITCHER_T101865093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultUISwitcher
struct  ResultUISwitcher_t101865093  : public MonoBehaviour_t3962482529
{
public:
	// ResultUISwitcher/ResultType ResultUISwitcher::_targetResultType
	int32_t ____targetResultType_2;
	// GameType ResultUISwitcher::_targetGameType
	int32_t ____targetGameType_3;

public:
	inline static int32_t get_offset_of__targetResultType_2() { return static_cast<int32_t>(offsetof(ResultUISwitcher_t101865093, ____targetResultType_2)); }
	inline int32_t get__targetResultType_2() const { return ____targetResultType_2; }
	inline int32_t* get_address_of__targetResultType_2() { return &____targetResultType_2; }
	inline void set__targetResultType_2(int32_t value)
	{
		____targetResultType_2 = value;
	}

	inline static int32_t get_offset_of__targetGameType_3() { return static_cast<int32_t>(offsetof(ResultUISwitcher_t101865093, ____targetGameType_3)); }
	inline int32_t get__targetGameType_3() const { return ____targetGameType_3; }
	inline int32_t* get_address_of__targetGameType_3() { return &____targetGameType_3; }
	inline void set__targetGameType_3(int32_t value)
	{
		____targetGameType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTUISWITCHER_T101865093_H
#ifndef CANVASFADER_T2370851967_H
#define CANVASFADER_T2370851967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanvasFader
struct  CanvasFader_t2370851967  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup CanvasFader::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_2;
	// CanvasFader/FadeState CanvasFader::_fadeState
	int32_t ____fadeState_3;
	// System.Single CanvasFader::_duration
	float ____duration_4;
	// System.Boolean CanvasFader::_ignoreTimeScale
	bool ____ignoreTimeScale_5;
	// System.Action CanvasFader::_onFinished
	Action_t1264377477 * ____onFinished_6;

public:
	inline static int32_t get_offset_of__canvasGroup_2() { return static_cast<int32_t>(offsetof(CanvasFader_t2370851967, ____canvasGroup_2)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_2() const { return ____canvasGroup_2; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_2() { return &____canvasGroup_2; }
	inline void set__canvasGroup_2(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_2), value);
	}

	inline static int32_t get_offset_of__fadeState_3() { return static_cast<int32_t>(offsetof(CanvasFader_t2370851967, ____fadeState_3)); }
	inline int32_t get__fadeState_3() const { return ____fadeState_3; }
	inline int32_t* get_address_of__fadeState_3() { return &____fadeState_3; }
	inline void set__fadeState_3(int32_t value)
	{
		____fadeState_3 = value;
	}

	inline static int32_t get_offset_of__duration_4() { return static_cast<int32_t>(offsetof(CanvasFader_t2370851967, ____duration_4)); }
	inline float get__duration_4() const { return ____duration_4; }
	inline float* get_address_of__duration_4() { return &____duration_4; }
	inline void set__duration_4(float value)
	{
		____duration_4 = value;
	}

	inline static int32_t get_offset_of__ignoreTimeScale_5() { return static_cast<int32_t>(offsetof(CanvasFader_t2370851967, ____ignoreTimeScale_5)); }
	inline bool get__ignoreTimeScale_5() const { return ____ignoreTimeScale_5; }
	inline bool* get_address_of__ignoreTimeScale_5() { return &____ignoreTimeScale_5; }
	inline void set__ignoreTimeScale_5(bool value)
	{
		____ignoreTimeScale_5 = value;
	}

	inline static int32_t get_offset_of__onFinished_6() { return static_cast<int32_t>(offsetof(CanvasFader_t2370851967, ____onFinished_6)); }
	inline Action_t1264377477 * get__onFinished_6() const { return ____onFinished_6; }
	inline Action_t1264377477 ** get_address_of__onFinished_6() { return &____onFinished_6; }
	inline void set__onFinished_6(Action_t1264377477 * value)
	{
		____onFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&____onFinished_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFADER_T2370851967_H
#ifndef IAPBUTTON_T2348892617_H
#define IAPBUTTON_T2348892617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton
struct  IAPButton_t2348892617  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Purchasing.IAPButton::productId
	String_t* ___productId_2;
	// UnityEngine.Purchasing.IAPButton/ButtonType UnityEngine.Purchasing.IAPButton::buttonType
	int32_t ___buttonType_3;
	// System.Boolean UnityEngine.Purchasing.IAPButton::consumePurchase
	bool ___consumePurchase_4;
	// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent UnityEngine.Purchasing.IAPButton::onPurchaseComplete
	OnPurchaseCompletedEvent_t3721407765 * ___onPurchaseComplete_5;
	// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent UnityEngine.Purchasing.IAPButton::onPurchaseFailed
	OnPurchaseFailedEvent_t1729542224 * ___onPurchaseFailed_6;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::titleText
	Text_t1901882714 * ___titleText_7;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::descriptionText
	Text_t1901882714 * ___descriptionText_8;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::priceText
	Text_t1901882714 * ___priceText_9;

public:
	inline static int32_t get_offset_of_productId_2() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___productId_2)); }
	inline String_t* get_productId_2() const { return ___productId_2; }
	inline String_t** get_address_of_productId_2() { return &___productId_2; }
	inline void set_productId_2(String_t* value)
	{
		___productId_2 = value;
		Il2CppCodeGenWriteBarrier((&___productId_2), value);
	}

	inline static int32_t get_offset_of_buttonType_3() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___buttonType_3)); }
	inline int32_t get_buttonType_3() const { return ___buttonType_3; }
	inline int32_t* get_address_of_buttonType_3() { return &___buttonType_3; }
	inline void set_buttonType_3(int32_t value)
	{
		___buttonType_3 = value;
	}

	inline static int32_t get_offset_of_consumePurchase_4() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___consumePurchase_4)); }
	inline bool get_consumePurchase_4() const { return ___consumePurchase_4; }
	inline bool* get_address_of_consumePurchase_4() { return &___consumePurchase_4; }
	inline void set_consumePurchase_4(bool value)
	{
		___consumePurchase_4 = value;
	}

	inline static int32_t get_offset_of_onPurchaseComplete_5() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___onPurchaseComplete_5)); }
	inline OnPurchaseCompletedEvent_t3721407765 * get_onPurchaseComplete_5() const { return ___onPurchaseComplete_5; }
	inline OnPurchaseCompletedEvent_t3721407765 ** get_address_of_onPurchaseComplete_5() { return &___onPurchaseComplete_5; }
	inline void set_onPurchaseComplete_5(OnPurchaseCompletedEvent_t3721407765 * value)
	{
		___onPurchaseComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseComplete_5), value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_6() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___onPurchaseFailed_6)); }
	inline OnPurchaseFailedEvent_t1729542224 * get_onPurchaseFailed_6() const { return ___onPurchaseFailed_6; }
	inline OnPurchaseFailedEvent_t1729542224 ** get_address_of_onPurchaseFailed_6() { return &___onPurchaseFailed_6; }
	inline void set_onPurchaseFailed_6(OnPurchaseFailedEvent_t1729542224 * value)
	{
		___onPurchaseFailed_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseFailed_6), value);
	}

	inline static int32_t get_offset_of_titleText_7() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___titleText_7)); }
	inline Text_t1901882714 * get_titleText_7() const { return ___titleText_7; }
	inline Text_t1901882714 ** get_address_of_titleText_7() { return &___titleText_7; }
	inline void set_titleText_7(Text_t1901882714 * value)
	{
		___titleText_7 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_7), value);
	}

	inline static int32_t get_offset_of_descriptionText_8() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___descriptionText_8)); }
	inline Text_t1901882714 * get_descriptionText_8() const { return ___descriptionText_8; }
	inline Text_t1901882714 ** get_address_of_descriptionText_8() { return &___descriptionText_8; }
	inline void set_descriptionText_8(Text_t1901882714 * value)
	{
		___descriptionText_8 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_8), value);
	}

	inline static int32_t get_offset_of_priceText_9() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___priceText_9)); }
	inline Text_t1901882714 * get_priceText_9() const { return ___priceText_9; }
	inline Text_t1901882714 ** get_address_of_priceText_9() { return &___priceText_9; }
	inline void set_priceText_9(Text_t1901882714 * value)
	{
		___priceText_9 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPBUTTON_T2348892617_H
#ifndef BASETENJIN_T1813768691_H
#define BASETENJIN_T1813768691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseTenjin
struct  BaseTenjin_t1813768691  : public MonoBehaviour_t3962482529
{
public:
	// System.String BaseTenjin::apiKey
	String_t* ___apiKey_2;

public:
	inline static int32_t get_offset_of_apiKey_2() { return static_cast<int32_t>(offsetof(BaseTenjin_t1813768691, ___apiKey_2)); }
	inline String_t* get_apiKey_2() const { return ___apiKey_2; }
	inline String_t** get_address_of_apiKey_2() { return &___apiKey_2; }
	inline void set_apiKey_2(String_t* value)
	{
		___apiKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___apiKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASETENJIN_T1813768691_H
#ifndef IAPDEMO_T3681080565_H
#define IAPDEMO_T3681080565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo
struct  IAPDemo_t3681080565  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Purchasing.IStoreController IAPDemo::m_Controller
	RuntimeObject* ___m_Controller_2;
	// UnityEngine.Purchasing.IAppleExtensions IAPDemo::m_AppleExtensions
	RuntimeObject* ___m_AppleExtensions_3;
	// UnityEngine.Purchasing.IMoolahExtension IAPDemo::m_MoolahExtensions
	RuntimeObject* ___m_MoolahExtensions_4;
	// UnityEngine.Purchasing.ISamsungAppsExtensions IAPDemo::m_SamsungExtensions
	RuntimeObject* ___m_SamsungExtensions_5;
	// UnityEngine.Purchasing.IMicrosoftExtensions IAPDemo::m_MicrosoftExtensions
	RuntimeObject* ___m_MicrosoftExtensions_6;
	// UnityEngine.Purchasing.IUnityChannelExtensions IAPDemo::m_UnityChannelExtensions
	RuntimeObject* ___m_UnityChannelExtensions_7;
	// System.Boolean IAPDemo::m_IsGooglePlayStoreSelected
	bool ___m_IsGooglePlayStoreSelected_8;
	// System.Boolean IAPDemo::m_IsSamsungAppsStoreSelected
	bool ___m_IsSamsungAppsStoreSelected_9;
	// System.Boolean IAPDemo::m_IsCloudMoolahStoreSelected
	bool ___m_IsCloudMoolahStoreSelected_10;
	// System.Boolean IAPDemo::m_IsUnityChannelSelected
	bool ___m_IsUnityChannelSelected_11;
	// System.String IAPDemo::m_LastTransactionID
	String_t* ___m_LastTransactionID_12;
	// System.Boolean IAPDemo::m_IsLoggedIn
	bool ___m_IsLoggedIn_13;
	// IAPDemo/UnityChannelLoginHandler IAPDemo::unityChannelLoginHandler
	UnityChannelLoginHandler_t2949829254 * ___unityChannelLoginHandler_14;
	// System.Boolean IAPDemo::m_FetchReceiptPayloadOnPurchase
	bool ___m_FetchReceiptPayloadOnPurchase_15;
	// System.Boolean IAPDemo::m_PurchaseInProgress
	bool ___m_PurchaseInProgress_16;
	// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI> IAPDemo::m_ProductUIs
	Dictionary_2_t708210053 * ___m_ProductUIs_17;
	// UnityEngine.GameObject IAPDemo::productUITemplate
	GameObject_t1113636619 * ___productUITemplate_18;
	// UnityEngine.RectTransform IAPDemo::contentRect
	RectTransform_t3704657025 * ___contentRect_19;
	// UnityEngine.UI.Button IAPDemo::restoreButton
	Button_t4055032469 * ___restoreButton_20;
	// UnityEngine.UI.Button IAPDemo::loginButton
	Button_t4055032469 * ___loginButton_21;
	// UnityEngine.UI.Button IAPDemo::validateButton
	Button_t4055032469 * ___validateButton_22;
	// UnityEngine.UI.Text IAPDemo::versionText
	Text_t1901882714 * ___versionText_23;

public:
	inline static int32_t get_offset_of_m_Controller_2() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_Controller_2)); }
	inline RuntimeObject* get_m_Controller_2() const { return ___m_Controller_2; }
	inline RuntimeObject** get_address_of_m_Controller_2() { return &___m_Controller_2; }
	inline void set_m_Controller_2(RuntimeObject* value)
	{
		___m_Controller_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Controller_2), value);
	}

	inline static int32_t get_offset_of_m_AppleExtensions_3() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_AppleExtensions_3)); }
	inline RuntimeObject* get_m_AppleExtensions_3() const { return ___m_AppleExtensions_3; }
	inline RuntimeObject** get_address_of_m_AppleExtensions_3() { return &___m_AppleExtensions_3; }
	inline void set_m_AppleExtensions_3(RuntimeObject* value)
	{
		___m_AppleExtensions_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AppleExtensions_3), value);
	}

	inline static int32_t get_offset_of_m_MoolahExtensions_4() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_MoolahExtensions_4)); }
	inline RuntimeObject* get_m_MoolahExtensions_4() const { return ___m_MoolahExtensions_4; }
	inline RuntimeObject** get_address_of_m_MoolahExtensions_4() { return &___m_MoolahExtensions_4; }
	inline void set_m_MoolahExtensions_4(RuntimeObject* value)
	{
		___m_MoolahExtensions_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoolahExtensions_4), value);
	}

	inline static int32_t get_offset_of_m_SamsungExtensions_5() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_SamsungExtensions_5)); }
	inline RuntimeObject* get_m_SamsungExtensions_5() const { return ___m_SamsungExtensions_5; }
	inline RuntimeObject** get_address_of_m_SamsungExtensions_5() { return &___m_SamsungExtensions_5; }
	inline void set_m_SamsungExtensions_5(RuntimeObject* value)
	{
		___m_SamsungExtensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_SamsungExtensions_5), value);
	}

	inline static int32_t get_offset_of_m_MicrosoftExtensions_6() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_MicrosoftExtensions_6)); }
	inline RuntimeObject* get_m_MicrosoftExtensions_6() const { return ___m_MicrosoftExtensions_6; }
	inline RuntimeObject** get_address_of_m_MicrosoftExtensions_6() { return &___m_MicrosoftExtensions_6; }
	inline void set_m_MicrosoftExtensions_6(RuntimeObject* value)
	{
		___m_MicrosoftExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MicrosoftExtensions_6), value);
	}

	inline static int32_t get_offset_of_m_UnityChannelExtensions_7() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_UnityChannelExtensions_7)); }
	inline RuntimeObject* get_m_UnityChannelExtensions_7() const { return ___m_UnityChannelExtensions_7; }
	inline RuntimeObject** get_address_of_m_UnityChannelExtensions_7() { return &___m_UnityChannelExtensions_7; }
	inline void set_m_UnityChannelExtensions_7(RuntimeObject* value)
	{
		___m_UnityChannelExtensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnityChannelExtensions_7), value);
	}

	inline static int32_t get_offset_of_m_IsGooglePlayStoreSelected_8() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsGooglePlayStoreSelected_8)); }
	inline bool get_m_IsGooglePlayStoreSelected_8() const { return ___m_IsGooglePlayStoreSelected_8; }
	inline bool* get_address_of_m_IsGooglePlayStoreSelected_8() { return &___m_IsGooglePlayStoreSelected_8; }
	inline void set_m_IsGooglePlayStoreSelected_8(bool value)
	{
		___m_IsGooglePlayStoreSelected_8 = value;
	}

	inline static int32_t get_offset_of_m_IsSamsungAppsStoreSelected_9() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsSamsungAppsStoreSelected_9)); }
	inline bool get_m_IsSamsungAppsStoreSelected_9() const { return ___m_IsSamsungAppsStoreSelected_9; }
	inline bool* get_address_of_m_IsSamsungAppsStoreSelected_9() { return &___m_IsSamsungAppsStoreSelected_9; }
	inline void set_m_IsSamsungAppsStoreSelected_9(bool value)
	{
		___m_IsSamsungAppsStoreSelected_9 = value;
	}

	inline static int32_t get_offset_of_m_IsCloudMoolahStoreSelected_10() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsCloudMoolahStoreSelected_10)); }
	inline bool get_m_IsCloudMoolahStoreSelected_10() const { return ___m_IsCloudMoolahStoreSelected_10; }
	inline bool* get_address_of_m_IsCloudMoolahStoreSelected_10() { return &___m_IsCloudMoolahStoreSelected_10; }
	inline void set_m_IsCloudMoolahStoreSelected_10(bool value)
	{
		___m_IsCloudMoolahStoreSelected_10 = value;
	}

	inline static int32_t get_offset_of_m_IsUnityChannelSelected_11() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsUnityChannelSelected_11)); }
	inline bool get_m_IsUnityChannelSelected_11() const { return ___m_IsUnityChannelSelected_11; }
	inline bool* get_address_of_m_IsUnityChannelSelected_11() { return &___m_IsUnityChannelSelected_11; }
	inline void set_m_IsUnityChannelSelected_11(bool value)
	{
		___m_IsUnityChannelSelected_11 = value;
	}

	inline static int32_t get_offset_of_m_LastTransactionID_12() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_LastTransactionID_12)); }
	inline String_t* get_m_LastTransactionID_12() const { return ___m_LastTransactionID_12; }
	inline String_t** get_address_of_m_LastTransactionID_12() { return &___m_LastTransactionID_12; }
	inline void set_m_LastTransactionID_12(String_t* value)
	{
		___m_LastTransactionID_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastTransactionID_12), value);
	}

	inline static int32_t get_offset_of_m_IsLoggedIn_13() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsLoggedIn_13)); }
	inline bool get_m_IsLoggedIn_13() const { return ___m_IsLoggedIn_13; }
	inline bool* get_address_of_m_IsLoggedIn_13() { return &___m_IsLoggedIn_13; }
	inline void set_m_IsLoggedIn_13(bool value)
	{
		___m_IsLoggedIn_13 = value;
	}

	inline static int32_t get_offset_of_unityChannelLoginHandler_14() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___unityChannelLoginHandler_14)); }
	inline UnityChannelLoginHandler_t2949829254 * get_unityChannelLoginHandler_14() const { return ___unityChannelLoginHandler_14; }
	inline UnityChannelLoginHandler_t2949829254 ** get_address_of_unityChannelLoginHandler_14() { return &___unityChannelLoginHandler_14; }
	inline void set_unityChannelLoginHandler_14(UnityChannelLoginHandler_t2949829254 * value)
	{
		___unityChannelLoginHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___unityChannelLoginHandler_14), value);
	}

	inline static int32_t get_offset_of_m_FetchReceiptPayloadOnPurchase_15() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_FetchReceiptPayloadOnPurchase_15)); }
	inline bool get_m_FetchReceiptPayloadOnPurchase_15() const { return ___m_FetchReceiptPayloadOnPurchase_15; }
	inline bool* get_address_of_m_FetchReceiptPayloadOnPurchase_15() { return &___m_FetchReceiptPayloadOnPurchase_15; }
	inline void set_m_FetchReceiptPayloadOnPurchase_15(bool value)
	{
		___m_FetchReceiptPayloadOnPurchase_15 = value;
	}

	inline static int32_t get_offset_of_m_PurchaseInProgress_16() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_PurchaseInProgress_16)); }
	inline bool get_m_PurchaseInProgress_16() const { return ___m_PurchaseInProgress_16; }
	inline bool* get_address_of_m_PurchaseInProgress_16() { return &___m_PurchaseInProgress_16; }
	inline void set_m_PurchaseInProgress_16(bool value)
	{
		___m_PurchaseInProgress_16 = value;
	}

	inline static int32_t get_offset_of_m_ProductUIs_17() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_ProductUIs_17)); }
	inline Dictionary_2_t708210053 * get_m_ProductUIs_17() const { return ___m_ProductUIs_17; }
	inline Dictionary_2_t708210053 ** get_address_of_m_ProductUIs_17() { return &___m_ProductUIs_17; }
	inline void set_m_ProductUIs_17(Dictionary_2_t708210053 * value)
	{
		___m_ProductUIs_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductUIs_17), value);
	}

	inline static int32_t get_offset_of_productUITemplate_18() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___productUITemplate_18)); }
	inline GameObject_t1113636619 * get_productUITemplate_18() const { return ___productUITemplate_18; }
	inline GameObject_t1113636619 ** get_address_of_productUITemplate_18() { return &___productUITemplate_18; }
	inline void set_productUITemplate_18(GameObject_t1113636619 * value)
	{
		___productUITemplate_18 = value;
		Il2CppCodeGenWriteBarrier((&___productUITemplate_18), value);
	}

	inline static int32_t get_offset_of_contentRect_19() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___contentRect_19)); }
	inline RectTransform_t3704657025 * get_contentRect_19() const { return ___contentRect_19; }
	inline RectTransform_t3704657025 ** get_address_of_contentRect_19() { return &___contentRect_19; }
	inline void set_contentRect_19(RectTransform_t3704657025 * value)
	{
		___contentRect_19 = value;
		Il2CppCodeGenWriteBarrier((&___contentRect_19), value);
	}

	inline static int32_t get_offset_of_restoreButton_20() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___restoreButton_20)); }
	inline Button_t4055032469 * get_restoreButton_20() const { return ___restoreButton_20; }
	inline Button_t4055032469 ** get_address_of_restoreButton_20() { return &___restoreButton_20; }
	inline void set_restoreButton_20(Button_t4055032469 * value)
	{
		___restoreButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___restoreButton_20), value);
	}

	inline static int32_t get_offset_of_loginButton_21() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___loginButton_21)); }
	inline Button_t4055032469 * get_loginButton_21() const { return ___loginButton_21; }
	inline Button_t4055032469 ** get_address_of_loginButton_21() { return &___loginButton_21; }
	inline void set_loginButton_21(Button_t4055032469 * value)
	{
		___loginButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___loginButton_21), value);
	}

	inline static int32_t get_offset_of_validateButton_22() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___validateButton_22)); }
	inline Button_t4055032469 * get_validateButton_22() const { return ___validateButton_22; }
	inline Button_t4055032469 ** get_address_of_validateButton_22() { return &___validateButton_22; }
	inline void set_validateButton_22(Button_t4055032469 * value)
	{
		___validateButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___validateButton_22), value);
	}

	inline static int32_t get_offset_of_versionText_23() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___versionText_23)); }
	inline Text_t1901882714 * get_versionText_23() const { return ___versionText_23; }
	inline Text_t1901882714 ** get_address_of_versionText_23() { return &___versionText_23; }
	inline void set_versionText_23(Text_t1901882714 * value)
	{
		___versionText_23 = value;
		Il2CppCodeGenWriteBarrier((&___versionText_23), value);
	}
};

struct IAPDemo_t3681080565_StaticFields
{
public:
	// System.Action`1<System.String> IAPDemo::<>f__am$cache0
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache0_24;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_24() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565_StaticFields, ___U3CU3Ef__amU24cache0_24)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache0_24() const { return ___U3CU3Ef__amU24cache0_24; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache0_24() { return &___U3CU3Ef__amU24cache0_24; }
	inline void set_U3CU3Ef__amU24cache0_24(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache0_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPDEMO_T3681080565_H
#ifndef STAR_T583751260_H
#define STAR_T583751260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Star
struct  Star_t583751260  : public MonoBehaviour_t3962482529
{
public:
	// TweenAlpha Star::_tween
	TweenAlpha_t3706845226 * ____tween_2;

public:
	inline static int32_t get_offset_of__tween_2() { return static_cast<int32_t>(offsetof(Star_t583751260, ____tween_2)); }
	inline TweenAlpha_t3706845226 * get__tween_2() const { return ____tween_2; }
	inline TweenAlpha_t3706845226 ** get_address_of__tween_2() { return &____tween_2; }
	inline void set__tween_2(TweenAlpha_t3706845226 * value)
	{
		____tween_2 = value;
		Il2CppCodeGenWriteBarrier((&____tween_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAR_T583751260_H
#ifndef TITLEHELP_T4092326671_H
#define TITLEHELP_T4092326671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleHelp
struct  TitleHelp_t4092326671  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TitleHelp::_main
	GameObject_t1113636619 * ____main_2;

public:
	inline static int32_t get_offset_of__main_2() { return static_cast<int32_t>(offsetof(TitleHelp_t4092326671, ____main_2)); }
	inline GameObject_t1113636619 * get__main_2() const { return ____main_2; }
	inline GameObject_t1113636619 ** get_address_of__main_2() { return &____main_2; }
	inline void set__main_2(GameObject_t1113636619 * value)
	{
		____main_2 = value;
		Il2CppCodeGenWriteBarrier((&____main_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEHELP_T4092326671_H
#ifndef TITLERECORDLABEL_T1707182038_H
#define TITLERECORDLABEL_T1707182038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleRecordLabel
struct  TitleRecordLabel_t1707182038  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLERECORDLABEL_T1707182038_H
#ifndef DEMOINVENTORY_T843047770_H
#define DEMOINVENTORY_T843047770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.DemoInventory
struct  DemoInventory_t843047770  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOINVENTORY_T843047770_H
#ifndef DEBUGTENJIN_T1974259765_H
#define DEBUGTENJIN_T1974259765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugTenjin
struct  DebugTenjin_t1974259765  : public BaseTenjin_t1813768691
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGTENJIN_T1974259765_H
#ifndef ANDROIDTENJIN_T297874381_H
#define ANDROIDTENJIN_T297874381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidTenjin
struct  AndroidTenjin_t297874381  : public BaseTenjin_t1813768691
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDTENJIN_T297874381_H
#ifndef BACKTITLEBUTTONASSISTANT_T2533991015_H
#define BACKTITLEBUTTONASSISTANT_T2533991015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackTitleButtonAssistant
struct  BackTitleButtonAssistant_t2533991015  : public ButtonAssistant_t3922690862
{
public:
	// BackTitleButtonAssistant/Location BackTitleButtonAssistant::_location
	int32_t ____location_5;
	// System.Boolean BackTitleButtonAssistant::_forceReset
	bool ____forceReset_6;
	// System.Boolean BackTitleButtonAssistant::_canTap
	bool ____canTap_7;

public:
	inline static int32_t get_offset_of__location_5() { return static_cast<int32_t>(offsetof(BackTitleButtonAssistant_t2533991015, ____location_5)); }
	inline int32_t get__location_5() const { return ____location_5; }
	inline int32_t* get_address_of__location_5() { return &____location_5; }
	inline void set__location_5(int32_t value)
	{
		____location_5 = value;
	}

	inline static int32_t get_offset_of__forceReset_6() { return static_cast<int32_t>(offsetof(BackTitleButtonAssistant_t2533991015, ____forceReset_6)); }
	inline bool get__forceReset_6() const { return ____forceReset_6; }
	inline bool* get_address_of__forceReset_6() { return &____forceReset_6; }
	inline void set__forceReset_6(bool value)
	{
		____forceReset_6 = value;
	}

	inline static int32_t get_offset_of__canTap_7() { return static_cast<int32_t>(offsetof(BackTitleButtonAssistant_t2533991015, ____canTap_7)); }
	inline bool get__canTap_7() const { return ____canTap_7; }
	inline bool* get_address_of__canTap_7() { return &____canTap_7; }
	inline void set__canTap_7(bool value)
	{
		____canTap_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKTITLEBUTTONASSISTANT_T2533991015_H
#ifndef TOSTAGESELECTBUTTONASSISTANT_T3549747967_H
#define TOSTAGESELECTBUTTONASSISTANT_T3549747967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToStageSelectButtonAssistant
struct  ToStageSelectButtonAssistant_t3549747967  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSTAGESELECTBUTTONASSISTANT_T3549747967_H
#ifndef TABSTAGEBUTTONASSISTANT_T2589366364_H
#define TABSTAGEBUTTONASSISTANT_T2589366364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabStageButtonAssistant
struct  TabStageButtonAssistant_t2589366364  : public ButtonAssistant_t3922690862
{
public:
	// TweenScale TabStageButtonAssistant::_scaleTw
	TweenScale_t2539309033 * ____scaleTw_5;
	// TweenPosition TabStageButtonAssistant::_positionTw
	TweenPosition_t1378762002 * ____positionTw_6;
	// UnityEngine.GameObject TabStageButtonAssistant::_icon
	GameObject_t1113636619 * ____icon_7;

public:
	inline static int32_t get_offset_of__scaleTw_5() { return static_cast<int32_t>(offsetof(TabStageButtonAssistant_t2589366364, ____scaleTw_5)); }
	inline TweenScale_t2539309033 * get__scaleTw_5() const { return ____scaleTw_5; }
	inline TweenScale_t2539309033 ** get_address_of__scaleTw_5() { return &____scaleTw_5; }
	inline void set__scaleTw_5(TweenScale_t2539309033 * value)
	{
		____scaleTw_5 = value;
		Il2CppCodeGenWriteBarrier((&____scaleTw_5), value);
	}

	inline static int32_t get_offset_of__positionTw_6() { return static_cast<int32_t>(offsetof(TabStageButtonAssistant_t2589366364, ____positionTw_6)); }
	inline TweenPosition_t1378762002 * get__positionTw_6() const { return ____positionTw_6; }
	inline TweenPosition_t1378762002 ** get_address_of__positionTw_6() { return &____positionTw_6; }
	inline void set__positionTw_6(TweenPosition_t1378762002 * value)
	{
		____positionTw_6 = value;
		Il2CppCodeGenWriteBarrier((&____positionTw_6), value);
	}

	inline static int32_t get_offset_of__icon_7() { return static_cast<int32_t>(offsetof(TabStageButtonAssistant_t2589366364, ____icon_7)); }
	inline GameObject_t1113636619 * get__icon_7() const { return ____icon_7; }
	inline GameObject_t1113636619 ** get_address_of__icon_7() { return &____icon_7; }
	inline void set__icon_7(GameObject_t1113636619 * value)
	{
		____icon_7 = value;
		Il2CppCodeGenWriteBarrier((&____icon_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABSTAGEBUTTONASSISTANT_T2589366364_H
#ifndef TABBALLBUTTONASSISTANT_T3092544413_H
#define TABBALLBUTTONASSISTANT_T3092544413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabBallButtonAssistant
struct  TabBallButtonAssistant_t3092544413  : public ButtonAssistant_t3922690862
{
public:
	// TweenScale TabBallButtonAssistant::_scaleTw
	TweenScale_t2539309033 * ____scaleTw_5;
	// TweenPosition TabBallButtonAssistant::_positionTw
	TweenPosition_t1378762002 * ____positionTw_6;
	// UnityEngine.GameObject TabBallButtonAssistant::_icon
	GameObject_t1113636619 * ____icon_7;

public:
	inline static int32_t get_offset_of__scaleTw_5() { return static_cast<int32_t>(offsetof(TabBallButtonAssistant_t3092544413, ____scaleTw_5)); }
	inline TweenScale_t2539309033 * get__scaleTw_5() const { return ____scaleTw_5; }
	inline TweenScale_t2539309033 ** get_address_of__scaleTw_5() { return &____scaleTw_5; }
	inline void set__scaleTw_5(TweenScale_t2539309033 * value)
	{
		____scaleTw_5 = value;
		Il2CppCodeGenWriteBarrier((&____scaleTw_5), value);
	}

	inline static int32_t get_offset_of__positionTw_6() { return static_cast<int32_t>(offsetof(TabBallButtonAssistant_t3092544413, ____positionTw_6)); }
	inline TweenPosition_t1378762002 * get__positionTw_6() const { return ____positionTw_6; }
	inline TweenPosition_t1378762002 ** get_address_of__positionTw_6() { return &____positionTw_6; }
	inline void set__positionTw_6(TweenPosition_t1378762002 * value)
	{
		____positionTw_6 = value;
		Il2CppCodeGenWriteBarrier((&____positionTw_6), value);
	}

	inline static int32_t get_offset_of__icon_7() { return static_cast<int32_t>(offsetof(TabBallButtonAssistant_t3092544413, ____icon_7)); }
	inline GameObject_t1113636619 * get__icon_7() const { return ____icon_7; }
	inline GameObject_t1113636619 ** get_address_of__icon_7() { return &____icon_7; }
	inline void set__icon_7(GameObject_t1113636619 * value)
	{
		____icon_7 = value;
		Il2CppCodeGenWriteBarrier((&____icon_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABBALLBUTTONASSISTANT_T3092544413_H
#ifndef STAGESELECTBUTTONASSISTANT_T565047406_H
#define STAGESELECTBUTTONASSISTANT_T565047406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StageSelectButtonAssistant
struct  StageSelectButtonAssistant_t565047406  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject StageSelectButtonAssistant::_icon
	GameObject_t1113636619 * ____icon_5;
	// UnityEngine.GameObject StageSelectButtonAssistant::_check
	GameObject_t1113636619 * ____check_6;
	// UISprite StageSelectButtonAssistant::_sprite
	UISprite_t194114938 * ____sprite_7;
	// System.Int32 StageSelectButtonAssistant::idx
	int32_t ___idx_8;
	// System.String StageSelectButtonAssistant::name
	String_t* ___name_9;
	// System.String StageSelectButtonAssistant::nameLock
	String_t* ___nameLock_10;

public:
	inline static int32_t get_offset_of__icon_5() { return static_cast<int32_t>(offsetof(StageSelectButtonAssistant_t565047406, ____icon_5)); }
	inline GameObject_t1113636619 * get__icon_5() const { return ____icon_5; }
	inline GameObject_t1113636619 ** get_address_of__icon_5() { return &____icon_5; }
	inline void set__icon_5(GameObject_t1113636619 * value)
	{
		____icon_5 = value;
		Il2CppCodeGenWriteBarrier((&____icon_5), value);
	}

	inline static int32_t get_offset_of__check_6() { return static_cast<int32_t>(offsetof(StageSelectButtonAssistant_t565047406, ____check_6)); }
	inline GameObject_t1113636619 * get__check_6() const { return ____check_6; }
	inline GameObject_t1113636619 ** get_address_of__check_6() { return &____check_6; }
	inline void set__check_6(GameObject_t1113636619 * value)
	{
		____check_6 = value;
		Il2CppCodeGenWriteBarrier((&____check_6), value);
	}

	inline static int32_t get_offset_of__sprite_7() { return static_cast<int32_t>(offsetof(StageSelectButtonAssistant_t565047406, ____sprite_7)); }
	inline UISprite_t194114938 * get__sprite_7() const { return ____sprite_7; }
	inline UISprite_t194114938 ** get_address_of__sprite_7() { return &____sprite_7; }
	inline void set__sprite_7(UISprite_t194114938 * value)
	{
		____sprite_7 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_7), value);
	}

	inline static int32_t get_offset_of_idx_8() { return static_cast<int32_t>(offsetof(StageSelectButtonAssistant_t565047406, ___idx_8)); }
	inline int32_t get_idx_8() const { return ___idx_8; }
	inline int32_t* get_address_of_idx_8() { return &___idx_8; }
	inline void set_idx_8(int32_t value)
	{
		___idx_8 = value;
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(StageSelectButtonAssistant_t565047406, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_nameLock_10() { return static_cast<int32_t>(offsetof(StageSelectButtonAssistant_t565047406, ___nameLock_10)); }
	inline String_t* get_nameLock_10() const { return ___nameLock_10; }
	inline String_t** get_address_of_nameLock_10() { return &___nameLock_10; }
	inline void set_nameLock_10(String_t* value)
	{
		___nameLock_10 = value;
		Il2CppCodeGenWriteBarrier((&___nameLock_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAGESELECTBUTTONASSISTANT_T565047406_H
#ifndef LEVELSELECTBUTTONASSISTANT_T3081551862_H
#define LEVELSELECTBUTTONASSISTANT_T3081551862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelectButtonAssistant
struct  LevelSelectButtonAssistant_t3081551862  : public ButtonAssistant_t3922690862
{
public:
	// System.Int32 LevelSelectButtonAssistant::_stageNo
	int32_t ____stageNo_5;
	// UnityEngine.Sprite LevelSelectButtonAssistant::_lockSprite
	Sprite_t280657092 * ____lockSprite_6;
	// UnityEngine.UI.Text LevelSelectButtonAssistant::_stageNoText
	Text_t1901882714 * ____stageNoText_7;

public:
	inline static int32_t get_offset_of__stageNo_5() { return static_cast<int32_t>(offsetof(LevelSelectButtonAssistant_t3081551862, ____stageNo_5)); }
	inline int32_t get__stageNo_5() const { return ____stageNo_5; }
	inline int32_t* get_address_of__stageNo_5() { return &____stageNo_5; }
	inline void set__stageNo_5(int32_t value)
	{
		____stageNo_5 = value;
	}

	inline static int32_t get_offset_of__lockSprite_6() { return static_cast<int32_t>(offsetof(LevelSelectButtonAssistant_t3081551862, ____lockSprite_6)); }
	inline Sprite_t280657092 * get__lockSprite_6() const { return ____lockSprite_6; }
	inline Sprite_t280657092 ** get_address_of__lockSprite_6() { return &____lockSprite_6; }
	inline void set__lockSprite_6(Sprite_t280657092 * value)
	{
		____lockSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&____lockSprite_6), value);
	}

	inline static int32_t get_offset_of__stageNoText_7() { return static_cast<int32_t>(offsetof(LevelSelectButtonAssistant_t3081551862, ____stageNoText_7)); }
	inline Text_t1901882714 * get__stageNoText_7() const { return ____stageNoText_7; }
	inline Text_t1901882714 ** get_address_of__stageNoText_7() { return &____stageNoText_7; }
	inline void set__stageNoText_7(Text_t1901882714 * value)
	{
		____stageNoText_7 = value;
		Il2CppCodeGenWriteBarrier((&____stageNoText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSELECTBUTTONASSISTANT_T3081551862_H
#ifndef HELPBUTTONASSISTANT_T2911537551_H
#define HELPBUTTONASSISTANT_T2911537551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpButtonAssistant
struct  HelpButtonAssistant_t2911537551  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPBUTTONASSISTANT_T2911537551_H
#ifndef LOADENDBUTTONASSISTANT_T4277748694_H
#define LOADENDBUTTONASSISTANT_T4277748694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadEndButtonAssistant
struct  LoadEndButtonAssistant_t4277748694  : public ButtonAssistant_t3922690862
{
public:
	// System.Boolean LoadEndButtonAssistant::_isInteractable
	bool ____isInteractable_5;

public:
	inline static int32_t get_offset_of__isInteractable_5() { return static_cast<int32_t>(offsetof(LoadEndButtonAssistant_t4277748694, ____isInteractable_5)); }
	inline bool get__isInteractable_5() const { return ____isInteractable_5; }
	inline bool* get_address_of__isInteractable_5() { return &____isInteractable_5; }
	inline void set__isInteractable_5(bool value)
	{
		____isInteractable_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADENDBUTTONASSISTANT_T4277748694_H
#ifndef TITLEBESTLABEL_T3979446273_H
#define TITLEBESTLABEL_T3979446273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleBestLabel
struct  TitleBestLabel_t3979446273  : public BestLabel_t2479364186
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEBESTLABEL_T3979446273_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T3897790875_H
#define SINGLETONMONOBEHAVIOUR_1_T3897790875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<TouchEventHandler>
struct  SingletonMonoBehaviour_1_t3897790875  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t3897790875_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	TouchEventHandler_t1627989417 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t3897790875_StaticFields, ____instance_3)); }
	inline TouchEventHandler_t1627989417 * get__instance_3() const { return ____instance_3; }
	inline TouchEventHandler_t1627989417 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(TouchEventHandler_t1627989417 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T3897790875_H
#ifndef MODESELECTBUTTONASSISTANT_T3315832045_H
#define MODESELECTBUTTONASSISTANT_T3315832045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModeSelectButtonAssistant
struct  ModeSelectButtonAssistant_t3315832045  : public ButtonAssistant_t3922690862
{
public:
	// System.Int32 ModeSelectButtonAssistant::_stageNo
	int32_t ____stageNo_5;
	// UnityEngine.UI.Text ModeSelectButtonAssistant::_bestScoreText
	Text_t1901882714 * ____bestScoreText_6;
	// UnityEngine.GameObject ModeSelectButtonAssistant::_eventSystem
	GameObject_t1113636619 * ____eventSystem_7;

public:
	inline static int32_t get_offset_of__stageNo_5() { return static_cast<int32_t>(offsetof(ModeSelectButtonAssistant_t3315832045, ____stageNo_5)); }
	inline int32_t get__stageNo_5() const { return ____stageNo_5; }
	inline int32_t* get_address_of__stageNo_5() { return &____stageNo_5; }
	inline void set__stageNo_5(int32_t value)
	{
		____stageNo_5 = value;
	}

	inline static int32_t get_offset_of__bestScoreText_6() { return static_cast<int32_t>(offsetof(ModeSelectButtonAssistant_t3315832045, ____bestScoreText_6)); }
	inline Text_t1901882714 * get__bestScoreText_6() const { return ____bestScoreText_6; }
	inline Text_t1901882714 ** get_address_of__bestScoreText_6() { return &____bestScoreText_6; }
	inline void set__bestScoreText_6(Text_t1901882714 * value)
	{
		____bestScoreText_6 = value;
		Il2CppCodeGenWriteBarrier((&____bestScoreText_6), value);
	}

	inline static int32_t get_offset_of__eventSystem_7() { return static_cast<int32_t>(offsetof(ModeSelectButtonAssistant_t3315832045, ____eventSystem_7)); }
	inline GameObject_t1113636619 * get__eventSystem_7() const { return ____eventSystem_7; }
	inline GameObject_t1113636619 ** get_address_of__eventSystem_7() { return &____eventSystem_7; }
	inline void set__eventSystem_7(GameObject_t1113636619 * value)
	{
		____eventSystem_7 = value;
		Il2CppCodeGenWriteBarrier((&____eventSystem_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODESELECTBUTTONASSISTANT_T3315832045_H
#ifndef TITLEGAMESTARTBUTTONASSISTANT_T2932851523_H
#define TITLEGAMESTARTBUTTONASSISTANT_T2932851523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleGameStartButtonAssistant
struct  TitleGameStartButtonAssistant_t2932851523  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEGAMESTARTBUTTONASSISTANT_T2932851523_H
#ifndef TITLEADBUTTONASSISTANT_T2481891752_H
#define TITLEADBUTTONASSISTANT_T2481891752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleAdButtonAssistant
struct  TitleAdButtonAssistant_t2481891752  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject TitleAdButtonAssistant::_houseAdButton
	GameObject_t1113636619 * ____houseAdButton_5;

public:
	inline static int32_t get_offset_of__houseAdButton_5() { return static_cast<int32_t>(offsetof(TitleAdButtonAssistant_t2481891752, ____houseAdButton_5)); }
	inline GameObject_t1113636619 * get__houseAdButton_5() const { return ____houseAdButton_5; }
	inline GameObject_t1113636619 ** get_address_of__houseAdButton_5() { return &____houseAdButton_5; }
	inline void set__houseAdButton_5(GameObject_t1113636619 * value)
	{
		____houseAdButton_5 = value;
		Il2CppCodeGenWriteBarrier((&____houseAdButton_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEADBUTTONASSISTANT_T2481891752_H
#ifndef NEXTLEVELBUTTONASSISTANT_T2533271905_H
#define NEXTLEVELBUTTONASSISTANT_T2533271905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NextLevelButtonAssistant
struct  NextLevelButtonAssistant_t2533271905  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEXTLEVELBUTTONASSISTANT_T2533271905_H
#ifndef SOUNDBUTTONASSISTANT_T1770237438_H
#define SOUNDBUTTONASSISTANT_T1770237438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundButtonAssistant
struct  SoundButtonAssistant_t1770237438  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDBUTTONASSISTANT_T1770237438_H
#ifndef RANKINGBUTTONASSISTANT_T3062667213_H
#define RANKINGBUTTONASSISTANT_T3062667213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RankingButtonAssistant
struct  RankingButtonAssistant_t3062667213  : public ButtonAssistant_t3922690862
{
public:
	// System.Boolean RankingButtonAssistant::_canTap
	bool ____canTap_5;

public:
	inline static int32_t get_offset_of__canTap_5() { return static_cast<int32_t>(offsetof(RankingButtonAssistant_t3062667213, ____canTap_5)); }
	inline bool get__canTap_5() const { return ____canTap_5; }
	inline bool* get_address_of__canTap_5() { return &____canTap_5; }
	inline void set__canTap_5(bool value)
	{
		____canTap_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKINGBUTTONASSISTANT_T3062667213_H
#ifndef RETRYBUTTONASSISTANT_T3622188528_H
#define RETRYBUTTONASSISTANT_T3622188528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RetryButtonAssistant
struct  RetryButtonAssistant_t3622188528  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYBUTTONASSISTANT_T3622188528_H
#ifndef IOSTENJIN_T1325461354_H
#define IOSTENJIN_T1325461354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IosTenjin
struct  IosTenjin_t1325461354  : public BaseTenjin_t1813768691
{
public:

public:
};

struct IosTenjin_t1325461354_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<System.Collections.Generic.Dictionary`2<System.String,System.String>> IosTenjin::deferredDeeplinkEvents
	Stack_1_t2476096443 * ___deferredDeeplinkEvents_3;
	// Tenjin/DeferredDeeplinkDelegate IosTenjin::registeredDeferredDeeplinkDelegate
	DeferredDeeplinkDelegate_t4234501760 * ___registeredDeferredDeeplinkDelegate_4;
	// IosTenjin/DeepLinkHandlerNativeDelegate IosTenjin::<>f__mg$cache0
	DeepLinkHandlerNativeDelegate_t1988926173 * ___U3CU3Ef__mgU24cache0_5;

public:
	inline static int32_t get_offset_of_deferredDeeplinkEvents_3() { return static_cast<int32_t>(offsetof(IosTenjin_t1325461354_StaticFields, ___deferredDeeplinkEvents_3)); }
	inline Stack_1_t2476096443 * get_deferredDeeplinkEvents_3() const { return ___deferredDeeplinkEvents_3; }
	inline Stack_1_t2476096443 ** get_address_of_deferredDeeplinkEvents_3() { return &___deferredDeeplinkEvents_3; }
	inline void set_deferredDeeplinkEvents_3(Stack_1_t2476096443 * value)
	{
		___deferredDeeplinkEvents_3 = value;
		Il2CppCodeGenWriteBarrier((&___deferredDeeplinkEvents_3), value);
	}

	inline static int32_t get_offset_of_registeredDeferredDeeplinkDelegate_4() { return static_cast<int32_t>(offsetof(IosTenjin_t1325461354_StaticFields, ___registeredDeferredDeeplinkDelegate_4)); }
	inline DeferredDeeplinkDelegate_t4234501760 * get_registeredDeferredDeeplinkDelegate_4() const { return ___registeredDeferredDeeplinkDelegate_4; }
	inline DeferredDeeplinkDelegate_t4234501760 ** get_address_of_registeredDeferredDeeplinkDelegate_4() { return &___registeredDeferredDeeplinkDelegate_4; }
	inline void set_registeredDeferredDeeplinkDelegate_4(DeferredDeeplinkDelegate_t4234501760 * value)
	{
		___registeredDeferredDeeplinkDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&___registeredDeferredDeeplinkDelegate_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(IosTenjin_t1325461354_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline DeepLinkHandlerNativeDelegate_t1988926173 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline DeepLinkHandlerNativeDelegate_t1988926173 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(DeepLinkHandlerNativeDelegate_t1988926173 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSTENJIN_T1325461354_H
#ifndef TOUCHEVENTHANDLER_T1627989417_H
#define TOUCHEVENTHANDLER_T1627989417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchEventHandler
struct  TouchEventHandler_t1627989417  : public SingletonMonoBehaviour_1_t3897790875
{
public:
	// System.Boolean TouchEventHandler::_isPressing
	bool ____isPressing_4;
	// System.Boolean TouchEventHandler::_isDragging
	bool ____isDragging_5;
	// System.Boolean TouchEventHandler::_isPinching
	bool ____isPinching_6;
	// UnityEngine.Vector3 TouchEventHandler::_beforeTapWorldPoint
	Vector3_t3722313464  ____beforeTapWorldPoint_7;
	// System.Single TouchEventHandler::_beforeDistanceOfPinch
	float ____beforeDistanceOfPinch_8;
	// System.Action`1<System.Boolean> TouchEventHandler::onPress
	Action_1_t269755560 * ___onPress_9;
	// System.Action TouchEventHandler::onBeginPress
	Action_t1264377477 * ___onBeginPress_10;
	// System.Action TouchEventHandler::onEndPress
	Action_t1264377477 * ___onEndPress_11;
	// System.Action`1<UnityEngine.Vector2> TouchEventHandler::onDrag
	Action_1_t2328697118 * ___onDrag_12;
	// System.Action`1<UnityEngine.Vector3> TouchEventHandler::onDragIn3D
	Action_1_t3894781059 * ___onDragIn3D_13;
	// System.Action TouchEventHandler::onBeginDrag
	Action_t1264377477 * ___onBeginDrag_14;
	// System.Action TouchEventHandler::onEndDrag
	Action_t1264377477 * ___onEndDrag_15;
	// System.Action`1<System.Single> TouchEventHandler::onPinch
	Action_1_t1569734369 * ___onPinch_16;
	// System.Action TouchEventHandler::onBeginPinch
	Action_t1264377477 * ___onBeginPinch_17;
	// System.Action TouchEventHandler::onEndPinch
	Action_t1264377477 * ___onEndPinch_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> TouchEventHandler::_draggingDataDict
	Dictionary_2_t2696614423 * ____draggingDataDict_19;

public:
	inline static int32_t get_offset_of__isPressing_4() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ____isPressing_4)); }
	inline bool get__isPressing_4() const { return ____isPressing_4; }
	inline bool* get_address_of__isPressing_4() { return &____isPressing_4; }
	inline void set__isPressing_4(bool value)
	{
		____isPressing_4 = value;
	}

	inline static int32_t get_offset_of__isDragging_5() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ____isDragging_5)); }
	inline bool get__isDragging_5() const { return ____isDragging_5; }
	inline bool* get_address_of__isDragging_5() { return &____isDragging_5; }
	inline void set__isDragging_5(bool value)
	{
		____isDragging_5 = value;
	}

	inline static int32_t get_offset_of__isPinching_6() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ____isPinching_6)); }
	inline bool get__isPinching_6() const { return ____isPinching_6; }
	inline bool* get_address_of__isPinching_6() { return &____isPinching_6; }
	inline void set__isPinching_6(bool value)
	{
		____isPinching_6 = value;
	}

	inline static int32_t get_offset_of__beforeTapWorldPoint_7() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ____beforeTapWorldPoint_7)); }
	inline Vector3_t3722313464  get__beforeTapWorldPoint_7() const { return ____beforeTapWorldPoint_7; }
	inline Vector3_t3722313464 * get_address_of__beforeTapWorldPoint_7() { return &____beforeTapWorldPoint_7; }
	inline void set__beforeTapWorldPoint_7(Vector3_t3722313464  value)
	{
		____beforeTapWorldPoint_7 = value;
	}

	inline static int32_t get_offset_of__beforeDistanceOfPinch_8() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ____beforeDistanceOfPinch_8)); }
	inline float get__beforeDistanceOfPinch_8() const { return ____beforeDistanceOfPinch_8; }
	inline float* get_address_of__beforeDistanceOfPinch_8() { return &____beforeDistanceOfPinch_8; }
	inline void set__beforeDistanceOfPinch_8(float value)
	{
		____beforeDistanceOfPinch_8 = value;
	}

	inline static int32_t get_offset_of_onPress_9() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onPress_9)); }
	inline Action_1_t269755560 * get_onPress_9() const { return ___onPress_9; }
	inline Action_1_t269755560 ** get_address_of_onPress_9() { return &___onPress_9; }
	inline void set_onPress_9(Action_1_t269755560 * value)
	{
		___onPress_9 = value;
		Il2CppCodeGenWriteBarrier((&___onPress_9), value);
	}

	inline static int32_t get_offset_of_onBeginPress_10() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onBeginPress_10)); }
	inline Action_t1264377477 * get_onBeginPress_10() const { return ___onBeginPress_10; }
	inline Action_t1264377477 ** get_address_of_onBeginPress_10() { return &___onBeginPress_10; }
	inline void set_onBeginPress_10(Action_t1264377477 * value)
	{
		___onBeginPress_10 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginPress_10), value);
	}

	inline static int32_t get_offset_of_onEndPress_11() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onEndPress_11)); }
	inline Action_t1264377477 * get_onEndPress_11() const { return ___onEndPress_11; }
	inline Action_t1264377477 ** get_address_of_onEndPress_11() { return &___onEndPress_11; }
	inline void set_onEndPress_11(Action_t1264377477 * value)
	{
		___onEndPress_11 = value;
		Il2CppCodeGenWriteBarrier((&___onEndPress_11), value);
	}

	inline static int32_t get_offset_of_onDrag_12() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onDrag_12)); }
	inline Action_1_t2328697118 * get_onDrag_12() const { return ___onDrag_12; }
	inline Action_1_t2328697118 ** get_address_of_onDrag_12() { return &___onDrag_12; }
	inline void set_onDrag_12(Action_1_t2328697118 * value)
	{
		___onDrag_12 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_12), value);
	}

	inline static int32_t get_offset_of_onDragIn3D_13() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onDragIn3D_13)); }
	inline Action_1_t3894781059 * get_onDragIn3D_13() const { return ___onDragIn3D_13; }
	inline Action_1_t3894781059 ** get_address_of_onDragIn3D_13() { return &___onDragIn3D_13; }
	inline void set_onDragIn3D_13(Action_1_t3894781059 * value)
	{
		___onDragIn3D_13 = value;
		Il2CppCodeGenWriteBarrier((&___onDragIn3D_13), value);
	}

	inline static int32_t get_offset_of_onBeginDrag_14() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onBeginDrag_14)); }
	inline Action_t1264377477 * get_onBeginDrag_14() const { return ___onBeginDrag_14; }
	inline Action_t1264377477 ** get_address_of_onBeginDrag_14() { return &___onBeginDrag_14; }
	inline void set_onBeginDrag_14(Action_t1264377477 * value)
	{
		___onBeginDrag_14 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginDrag_14), value);
	}

	inline static int32_t get_offset_of_onEndDrag_15() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onEndDrag_15)); }
	inline Action_t1264377477 * get_onEndDrag_15() const { return ___onEndDrag_15; }
	inline Action_t1264377477 ** get_address_of_onEndDrag_15() { return &___onEndDrag_15; }
	inline void set_onEndDrag_15(Action_t1264377477 * value)
	{
		___onEndDrag_15 = value;
		Il2CppCodeGenWriteBarrier((&___onEndDrag_15), value);
	}

	inline static int32_t get_offset_of_onPinch_16() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onPinch_16)); }
	inline Action_1_t1569734369 * get_onPinch_16() const { return ___onPinch_16; }
	inline Action_1_t1569734369 ** get_address_of_onPinch_16() { return &___onPinch_16; }
	inline void set_onPinch_16(Action_1_t1569734369 * value)
	{
		___onPinch_16 = value;
		Il2CppCodeGenWriteBarrier((&___onPinch_16), value);
	}

	inline static int32_t get_offset_of_onBeginPinch_17() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onBeginPinch_17)); }
	inline Action_t1264377477 * get_onBeginPinch_17() const { return ___onBeginPinch_17; }
	inline Action_t1264377477 ** get_address_of_onBeginPinch_17() { return &___onBeginPinch_17; }
	inline void set_onBeginPinch_17(Action_t1264377477 * value)
	{
		___onBeginPinch_17 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginPinch_17), value);
	}

	inline static int32_t get_offset_of_onEndPinch_18() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ___onEndPinch_18)); }
	inline Action_t1264377477 * get_onEndPinch_18() const { return ___onEndPinch_18; }
	inline Action_t1264377477 ** get_address_of_onEndPinch_18() { return &___onEndPinch_18; }
	inline void set_onEndPinch_18(Action_t1264377477 * value)
	{
		___onEndPinch_18 = value;
		Il2CppCodeGenWriteBarrier((&___onEndPinch_18), value);
	}

	inline static int32_t get_offset_of__draggingDataDict_19() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417, ____draggingDataDict_19)); }
	inline Dictionary_2_t2696614423 * get__draggingDataDict_19() const { return ____draggingDataDict_19; }
	inline Dictionary_2_t2696614423 ** get_address_of__draggingDataDict_19() { return &____draggingDataDict_19; }
	inline void set__draggingDataDict_19(Dictionary_2_t2696614423 * value)
	{
		____draggingDataDict_19 = value;
		Il2CppCodeGenWriteBarrier((&____draggingDataDict_19), value);
	}
};

struct TouchEventHandler_t1627989417_StaticFields
{
public:
	// System.Action`1<System.Boolean> TouchEventHandler::<>f__am$cache0
	Action_1_t269755560 * ___U3CU3Ef__amU24cache0_20;
	// System.Action TouchEventHandler::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_21;
	// System.Action TouchEventHandler::<>f__am$cache2
	Action_t1264377477 * ___U3CU3Ef__amU24cache2_22;
	// System.Action`1<UnityEngine.Vector2> TouchEventHandler::<>f__am$cache3
	Action_1_t2328697118 * ___U3CU3Ef__amU24cache3_23;
	// System.Action`1<UnityEngine.Vector3> TouchEventHandler::<>f__am$cache4
	Action_1_t3894781059 * ___U3CU3Ef__amU24cache4_24;
	// System.Action TouchEventHandler::<>f__am$cache5
	Action_t1264377477 * ___U3CU3Ef__amU24cache5_25;
	// System.Action TouchEventHandler::<>f__am$cache6
	Action_t1264377477 * ___U3CU3Ef__amU24cache6_26;
	// System.Action`1<System.Single> TouchEventHandler::<>f__am$cache7
	Action_1_t1569734369 * ___U3CU3Ef__amU24cache7_27;
	// System.Action TouchEventHandler::<>f__am$cache8
	Action_t1264377477 * ___U3CU3Ef__amU24cache8_28;
	// System.Action TouchEventHandler::<>f__am$cache9
	Action_t1264377477 * ___U3CU3Ef__amU24cache9_29;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_20() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache0_20)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache0_20() const { return ___U3CU3Ef__amU24cache0_20; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache0_20() { return &___U3CU3Ef__amU24cache0_20; }
	inline void set_U3CU3Ef__amU24cache0_20(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_21() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache1_21)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_21() const { return ___U3CU3Ef__amU24cache1_21; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_21() { return &___U3CU3Ef__amU24cache1_21; }
	inline void set_U3CU3Ef__amU24cache1_21(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_22() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache2_22)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache2_22() const { return ___U3CU3Ef__amU24cache2_22; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache2_22() { return &___U3CU3Ef__amU24cache2_22; }
	inline void set_U3CU3Ef__amU24cache2_22(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache2_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_23() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache3_23)); }
	inline Action_1_t2328697118 * get_U3CU3Ef__amU24cache3_23() const { return ___U3CU3Ef__amU24cache3_23; }
	inline Action_1_t2328697118 ** get_address_of_U3CU3Ef__amU24cache3_23() { return &___U3CU3Ef__amU24cache3_23; }
	inline void set_U3CU3Ef__amU24cache3_23(Action_1_t2328697118 * value)
	{
		___U3CU3Ef__amU24cache3_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_24() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache4_24)); }
	inline Action_1_t3894781059 * get_U3CU3Ef__amU24cache4_24() const { return ___U3CU3Ef__amU24cache4_24; }
	inline Action_1_t3894781059 ** get_address_of_U3CU3Ef__amU24cache4_24() { return &___U3CU3Ef__amU24cache4_24; }
	inline void set_U3CU3Ef__amU24cache4_24(Action_1_t3894781059 * value)
	{
		___U3CU3Ef__amU24cache4_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_25() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache5_25)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache5_25() const { return ___U3CU3Ef__amU24cache5_25; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache5_25() { return &___U3CU3Ef__amU24cache5_25; }
	inline void set_U3CU3Ef__amU24cache5_25(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache5_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_26() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache6_26)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache6_26() const { return ___U3CU3Ef__amU24cache6_26; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache6_26() { return &___U3CU3Ef__amU24cache6_26; }
	inline void set_U3CU3Ef__amU24cache6_26(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache6_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_27() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache7_27)); }
	inline Action_1_t1569734369 * get_U3CU3Ef__amU24cache7_27() const { return ___U3CU3Ef__amU24cache7_27; }
	inline Action_1_t1569734369 ** get_address_of_U3CU3Ef__amU24cache7_27() { return &___U3CU3Ef__amU24cache7_27; }
	inline void set_U3CU3Ef__amU24cache7_27(Action_1_t1569734369 * value)
	{
		___U3CU3Ef__amU24cache7_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_28() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache8_28)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache8_28() const { return ___U3CU3Ef__amU24cache8_28; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache8_28() { return &___U3CU3Ef__amU24cache8_28; }
	inline void set_U3CU3Ef__amU24cache8_28(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache8_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache8_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_29() { return static_cast<int32_t>(offsetof(TouchEventHandler_t1627989417_StaticFields, ___U3CU3Ef__amU24cache9_29)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache9_29() const { return ___U3CU3Ef__amU24cache9_29; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache9_29() { return &___U3CU3Ef__amU24cache9_29; }
	inline void set_U3CU3Ef__amU24cache9_29(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache9_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache9_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENTHANDLER_T1627989417_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (CanvasFader_t2370851967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[5] = 
{
	CanvasFader_t2370851967::get_offset_of__canvasGroup_2(),
	CanvasFader_t2370851967::get_offset_of__fadeState_3(),
	CanvasFader_t2370851967::get_offset_of__duration_4(),
	CanvasFader_t2370851967::get_offset_of__ignoreTimeScale_5(),
	CanvasFader_t2370851967::get_offset_of__onFinished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (FadeState_t3570061009)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2801[4] = 
{
	FadeState_t3570061009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (FadeTransition_t2992252162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[5] = 
{
	0,
	FadeTransition_t2992252162::get_offset_of__status_3(),
	FadeTransition_t2992252162::get_offset_of__blackTexture_4(),
	FadeTransition_t2992252162::get_offset_of__fadeAlpha_5(),
	FadeTransition_t2992252162::get_offset_of__isFading_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (Status_t56047631)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2803[5] = 
{
	Status_t56047631::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (U3CCreateBlackTextureU3Ec__Iterator0_t4152081107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[4] = 
{
	U3CCreateBlackTextureU3Ec__Iterator0_t4152081107::get_offset_of_U24this_0(),
	U3CCreateBlackTextureU3Ec__Iterator0_t4152081107::get_offset_of_U24current_1(),
	U3CCreateBlackTextureU3Ec__Iterator0_t4152081107::get_offset_of_U24disposing_2(),
	U3CCreateBlackTextureU3Ec__Iterator0_t4152081107::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (TouchEventHandler_t1627989417), -1, sizeof(TouchEventHandler_t1627989417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2805[26] = 
{
	TouchEventHandler_t1627989417::get_offset_of__isPressing_4(),
	TouchEventHandler_t1627989417::get_offset_of__isDragging_5(),
	TouchEventHandler_t1627989417::get_offset_of__isPinching_6(),
	TouchEventHandler_t1627989417::get_offset_of__beforeTapWorldPoint_7(),
	TouchEventHandler_t1627989417::get_offset_of__beforeDistanceOfPinch_8(),
	TouchEventHandler_t1627989417::get_offset_of_onPress_9(),
	TouchEventHandler_t1627989417::get_offset_of_onBeginPress_10(),
	TouchEventHandler_t1627989417::get_offset_of_onEndPress_11(),
	TouchEventHandler_t1627989417::get_offset_of_onDrag_12(),
	TouchEventHandler_t1627989417::get_offset_of_onDragIn3D_13(),
	TouchEventHandler_t1627989417::get_offset_of_onBeginDrag_14(),
	TouchEventHandler_t1627989417::get_offset_of_onEndDrag_15(),
	TouchEventHandler_t1627989417::get_offset_of_onPinch_16(),
	TouchEventHandler_t1627989417::get_offset_of_onBeginPinch_17(),
	TouchEventHandler_t1627989417::get_offset_of_onEndPinch_18(),
	TouchEventHandler_t1627989417::get_offset_of__draggingDataDict_19(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_21(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_22(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_23(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_24(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_25(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_26(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_27(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_28(),
	TouchEventHandler_t1627989417_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (BackTitleButtonAssistant_t2533991015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[3] = 
{
	BackTitleButtonAssistant_t2533991015::get_offset_of__location_5(),
	BackTitleButtonAssistant_t2533991015::get_offset_of__forceReset_6(),
	BackTitleButtonAssistant_t2533991015::get_offset_of__canTap_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (Location_t1923176401)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2807[4] = 
{
	Location_t1923176401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (U3CStartU3Ec__Iterator0_t2073787043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[4] = 
{
	U3CStartU3Ec__Iterator0_t2073787043::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2073787043::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2073787043::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2073787043::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (ToStageSelectButtonAssistant_t3549747967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (GameHelp_t51737220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (GameScoreLabel_t3714137216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[2] = 
{
	GameScoreLabel_t3714137216::get_offset_of__textFormat_2(),
	GameScoreLabel_t3714137216::get_offset_of__label_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (GameStageNoLabel_t3553093228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[1] = 
{
	GameStageNoLabel_t3553093228::get_offset_of__textFormat_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (PauseUI_t2388357271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[3] = 
{
	PauseUI_t2388357271::get_offset_of__canvas_2(),
	PauseUI_t2388357271::get_offset_of__pauseButtonAssistant_3(),
	PauseUI_t2388357271::get_offset_of__resumeButtonAssistant_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (LevelSelectButtonAssistant_t3081551862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[3] = 
{
	LevelSelectButtonAssistant_t3081551862::get_offset_of__stageNo_5(),
	LevelSelectButtonAssistant_t3081551862::get_offset_of__lockSprite_6(),
	LevelSelectButtonAssistant_t3081551862::get_offset_of__stageNoText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (LevelSelectMenu_t2404780637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[6] = 
{
	LevelSelectMenu_t2404780637::get_offset_of__buttonNumInTable_2(),
	LevelSelectMenu_t2404780637::get_offset_of__space_3(),
	LevelSelectMenu_t2404780637::get_offset_of__tableList_4(),
	LevelSelectMenu_t2404780637::get_offset_of__currentTableNo_5(),
	LevelSelectMenu_t2404780637::get_offset_of__backTableButton_6(),
	LevelSelectMenu_t2404780637::get_offset_of__nextTableButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (LoadEndButtonAssistant_t4277748694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[1] = 
{
	LoadEndButtonAssistant_t4277748694::get_offset_of__isInteractable_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (LoadProgressBar_t2582436008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[18] = 
{
	LoadProgressBar_t2582436008::get_offset_of__scrollBar_2(),
	LoadProgressBar_t2582436008::get_offset_of__loadTitleLabel_3(),
	LoadProgressBar_t2582436008::get_offset_of__loadEndButton_4(),
	LoadProgressBar_t2582436008::get_offset_of__totalTimePer_5(),
	LoadProgressBar_t2582436008::get_offset_of__loadTime_6(),
	0,
	0,
	LoadProgressBar_t2582436008::get_offset_of__loadTimePerList_9(),
	LoadProgressBar_t2582436008::get_offset_of__loadNo_10(),
	LoadProgressBar_t2582436008::get_offset_of__waitTimePerList_11(),
	LoadProgressBar_t2582436008::get_offset_of__isWaiting_12(),
	LoadProgressBar_t2582436008::get_offset_of__waitingNo_13(),
	LoadProgressBar_t2582436008::get_offset_of__progressPerList_14(),
	LoadProgressBar_t2582436008::get_offset_of__totalProgressPer_15(),
	LoadProgressBar_t2582436008::get_offset_of__progress_16(),
	LoadProgressBar_t2582436008::get_offset_of__addtionalProgress_17(),
	LoadProgressBar_t2582436008::get_offset_of__updateLimit_18(),
	LoadProgressBar_t2582436008::get_offset_of__updateCount_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (LoadSceneBack_t3289937302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (ModeSelectButtonAssistant_t3315832045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[3] = 
{
	ModeSelectButtonAssistant_t3315832045::get_offset_of__stageNo_5(),
	ModeSelectButtonAssistant_t3315832045::get_offset_of__bestScoreText_6(),
	ModeSelectButtonAssistant_t3315832045::get_offset_of__eventSystem_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (NewRecordObject_t1797845672), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (NextLevelButtonAssistant_t2533271905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (ResultLabel_t3723221400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[1] = 
{
	ResultLabel_t3723221400::get_offset_of__textFormat_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (ResultUISwitcher_t101865093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[2] = 
{
	ResultUISwitcher_t101865093::get_offset_of__targetResultType_2(),
	ResultUISwitcher_t101865093::get_offset_of__targetGameType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (ResultType_t3090920659)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2824[4] = 
{
	ResultType_t3090920659::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (RetryButtonAssistant_t3622188528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (HelpButtonAssistant_t2911537551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (RankingButtonAssistant_t3062667213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[1] = 
{
	RankingButtonAssistant_t3062667213::get_offset_of__canTap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (U3CStartU3Ec__Iterator0_t843897986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[4] = 
{
	U3CStartU3Ec__Iterator0_t843897986::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t843897986::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t843897986::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t843897986::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (SoundButtonAssistant_t1770237438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (TitleAdButtonAssistant_t2481891752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[1] = 
{
	TitleAdButtonAssistant_t2481891752::get_offset_of__houseAdButton_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (TitleGameStartButtonAssistant_t2932851523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (TitleBestLabel_t3979446273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (TitleHelp_t4092326671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[1] = 
{
	TitleHelp_t4092326671::get_offset_of__main_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (TitleRecordLabel_t1707182038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (EnumUtility_t4065693008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (PlayerPrefsUtility_t3733645470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (ProbabilityCalclator_t1670675042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (StringUtility_t3266228228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (VectorCalculator_t2681497494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (StageSelectButtonAssistant_t565047406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[6] = 
{
	StageSelectButtonAssistant_t565047406::get_offset_of__icon_5(),
	StageSelectButtonAssistant_t565047406::get_offset_of__check_6(),
	StageSelectButtonAssistant_t565047406::get_offset_of__sprite_7(),
	StageSelectButtonAssistant_t565047406::get_offset_of_idx_8(),
	StageSelectButtonAssistant_t565047406::get_offset_of_name_9(),
	StageSelectButtonAssistant_t565047406::get_offset_of_nameLock_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (Star_t583751260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[1] = 
{
	Star_t583751260::get_offset_of__tween_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (TabBallButtonAssistant_t3092544413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[3] = 
{
	TabBallButtonAssistant_t3092544413::get_offset_of__scaleTw_5(),
	TabBallButtonAssistant_t3092544413::get_offset_of__positionTw_6(),
	TabBallButtonAssistant_t3092544413::get_offset_of__icon_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (TabStageButtonAssistant_t2589366364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[3] = 
{
	TabStageButtonAssistant_t2589366364::get_offset_of__scaleTw_5(),
	TabStageButtonAssistant_t2589366364::get_offset_of__positionTw_6(),
	TabStageButtonAssistant_t2589366364::get_offset_of__icon_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (AndroidTenjin_t297874381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (BaseTenjin_t1813768691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[1] = 
{
	BaseTenjin_t1813768691::get_offset_of_apiKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (DebugTenjin_t1974259765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (IosTenjin_t1325461354), -1, sizeof(IosTenjin_t1325461354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2847[3] = 
{
	IosTenjin_t1325461354_StaticFields::get_offset_of_deferredDeeplinkEvents_3(),
	IosTenjin_t1325461354_StaticFields::get_offset_of_registeredDeferredDeeplinkDelegate_4(),
	IosTenjin_t1325461354_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (DeepLinkHandlerNativeDelegate_t1988926173), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (NativeUtility_t307969918), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (StringStringKeyValuePair_t2008113712)+ sizeof (RuntimeObject), sizeof(StringStringKeyValuePair_t2008113712_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2850[2] = 
{
	StringStringKeyValuePair_t2008113712::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringStringKeyValuePair_t2008113712::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (Tenjin_t2555426487), -1, sizeof(Tenjin_t2555426487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2851[1] = 
{
	Tenjin_t2555426487_StaticFields::get_offset_of__instances_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (DeferredDeeplinkDelegate_t4234501760), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2853[4] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D4057698FC35E52288B87C31D67A93D5A5AAC27C0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_1(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_2(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (U24ArrayTypeU3D580_t353960863)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D580_t353960863 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (U24ArrayTypeU3D8_t3242499063)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D8_t3242499063 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (U3CModuleU3E_t692745564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (AppStoreSetting_t1592337179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[3] = 
{
	AppStoreSetting_t1592337179::get_offset_of_AppID_0(),
	AppStoreSetting_t1592337179::get_offset_of_AppKey_1(),
	AppStoreSetting_t1592337179::get_offset_of_IsTestMode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (AppStoreSettings_t2325796953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[4] = 
{
	AppStoreSettings_t2325796953::get_offset_of_UnityClientID_2(),
	AppStoreSettings_t2325796953::get_offset_of_UnityClientKey_3(),
	AppStoreSettings_t2325796953::get_offset_of_UnityClientRSAPublicKey_4(),
	AppStoreSettings_t2325796953::get_offset_of_XiaomiAppStoreSetting_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (DemoInventory_t843047770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (IAPButton_t2348892617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[8] = 
{
	IAPButton_t2348892617::get_offset_of_productId_2(),
	IAPButton_t2348892617::get_offset_of_buttonType_3(),
	IAPButton_t2348892617::get_offset_of_consumePurchase_4(),
	IAPButton_t2348892617::get_offset_of_onPurchaseComplete_5(),
	IAPButton_t2348892617::get_offset_of_onPurchaseFailed_6(),
	IAPButton_t2348892617::get_offset_of_titleText_7(),
	IAPButton_t2348892617::get_offset_of_descriptionText_8(),
	IAPButton_t2348892617::get_offset_of_priceText_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (ButtonType_t908070482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2863[3] = 
{
	ButtonType_t908070482::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (OnPurchaseCompletedEvent_t3721407765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (OnPurchaseFailedEvent_t1729542224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (IAPButtonStoreManager_t3446654887), -1, sizeof(IAPButtonStoreManager_t3446654887_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2866[6] = 
{
	IAPButtonStoreManager_t3446654887_StaticFields::get_offset_of_instance_0(),
	IAPButtonStoreManager_t3446654887::get_offset_of_catalog_1(),
	IAPButtonStoreManager_t3446654887::get_offset_of_activeButtons_2(),
	IAPButtonStoreManager_t3446654887::get_offset_of_m_Listener_3(),
	IAPButtonStoreManager_t3446654887::get_offset_of_controller_4(),
	IAPButtonStoreManager_t3446654887::get_offset_of_extensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (IAPConfigurationHelper_t2483224394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (IAPDemo_t3681080565), -1, sizeof(IAPDemo_t3681080565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2868[23] = 
{
	IAPDemo_t3681080565::get_offset_of_m_Controller_2(),
	IAPDemo_t3681080565::get_offset_of_m_AppleExtensions_3(),
	IAPDemo_t3681080565::get_offset_of_m_MoolahExtensions_4(),
	IAPDemo_t3681080565::get_offset_of_m_SamsungExtensions_5(),
	IAPDemo_t3681080565::get_offset_of_m_MicrosoftExtensions_6(),
	IAPDemo_t3681080565::get_offset_of_m_UnityChannelExtensions_7(),
	IAPDemo_t3681080565::get_offset_of_m_IsGooglePlayStoreSelected_8(),
	IAPDemo_t3681080565::get_offset_of_m_IsSamsungAppsStoreSelected_9(),
	IAPDemo_t3681080565::get_offset_of_m_IsCloudMoolahStoreSelected_10(),
	IAPDemo_t3681080565::get_offset_of_m_IsUnityChannelSelected_11(),
	IAPDemo_t3681080565::get_offset_of_m_LastTransactionID_12(),
	IAPDemo_t3681080565::get_offset_of_m_IsLoggedIn_13(),
	IAPDemo_t3681080565::get_offset_of_unityChannelLoginHandler_14(),
	IAPDemo_t3681080565::get_offset_of_m_FetchReceiptPayloadOnPurchase_15(),
	IAPDemo_t3681080565::get_offset_of_m_PurchaseInProgress_16(),
	IAPDemo_t3681080565::get_offset_of_m_ProductUIs_17(),
	IAPDemo_t3681080565::get_offset_of_productUITemplate_18(),
	IAPDemo_t3681080565::get_offset_of_contentRect_19(),
	IAPDemo_t3681080565::get_offset_of_restoreButton_20(),
	IAPDemo_t3681080565::get_offset_of_loginButton_21(),
	IAPDemo_t3681080565::get_offset_of_validateButton_22(),
	IAPDemo_t3681080565::get_offset_of_versionText_23(),
	IAPDemo_t3681080565_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (UnityChannelPurchaseError_t2306817818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[2] = 
{
	UnityChannelPurchaseError_t2306817818::get_offset_of_error_0(),
	UnityChannelPurchaseError_t2306817818::get_offset_of_purchaseInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (UnityChannelPurchaseInfo_t74063925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[3] = 
{
	UnityChannelPurchaseInfo_t74063925::get_offset_of_productCode_0(),
	UnityChannelPurchaseInfo_t74063925::get_offset_of_gameOrderId_1(),
	UnityChannelPurchaseInfo_t74063925::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (UnityChannelLoginHandler_t2949829254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[4] = 
{
	UnityChannelLoginHandler_t2949829254::get_offset_of_initializeSucceededAction_0(),
	UnityChannelLoginHandler_t2949829254::get_offset_of_initializeFailedAction_1(),
	UnityChannelLoginHandler_t2949829254::get_offset_of_loginSucceededAction_2(),
	UnityChannelLoginHandler_t2949829254::get_offset_of_loginFailedAction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (U3CAwakeU3Ec__AnonStorey0_t2364586269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[3] = 
{
	U3CAwakeU3Ec__AnonStorey0_t2364586269::get_offset_of_builder_0(),
	U3CAwakeU3Ec__AnonStorey0_t2364586269::get_offset_of_initializeUnityIap_1(),
	U3CAwakeU3Ec__AnonStorey0_t2364586269::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (U3CValidateButtonClickU3Ec__AnonStorey1_t541528072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	U3CValidateButtonClickU3Ec__AnonStorey1_t541528072::get_offset_of_txId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (IAPDemoProductUI_t922953754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[9] = 
{
	IAPDemoProductUI_t922953754::get_offset_of_purchaseButton_2(),
	IAPDemoProductUI_t922953754::get_offset_of_receiptButton_3(),
	IAPDemoProductUI_t922953754::get_offset_of_titleText_4(),
	IAPDemoProductUI_t922953754::get_offset_of_descriptionText_5(),
	IAPDemoProductUI_t922953754::get_offset_of_priceText_6(),
	IAPDemoProductUI_t922953754::get_offset_of_statusText_7(),
	IAPDemoProductUI_t922953754::get_offset_of_m_ProductID_8(),
	IAPDemoProductUI_t922953754::get_offset_of_m_PurchaseCallback_9(),
	IAPDemoProductUI_t922953754::get_offset_of_m_Receipt_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (IAPListener_t2001792988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[4] = 
{
	IAPListener_t2001792988::get_offset_of_consumePurchase_2(),
	IAPListener_t2001792988::get_offset_of_dontDestroyOnLoad_3(),
	IAPListener_t2001792988::get_offset_of_onPurchaseComplete_4(),
	IAPListener_t2001792988::get_offset_of_onPurchaseFailed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (OnPurchaseCompletedEvent_t1675809258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (OnPurchaseFailedEvent_t800864861), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
