//
//  Encryption.cpp
//  TemplateProject
//
//
//

#include "Encryption.h"

#include <stdio.h>
#include <string.h>
#include <sys/file.h>


#define PASSWORD_STR_LEN 6


//XOR演算による暗号化・復号化処理
//参考URL : http://uguisu.skr.jp/Windows/c_xor.html
//key ... 1 〜 9999999999 (XCode 4.5でテストした値)
std::string Encryption::xorCalc(const int key, const char * data)
{
    //前提条件
    if (!data)
    {
        return NULL;
    }

    int length = strlen(data);
    if (length <= 0)
    {
        return "";
    }


    std::string value = "";

    for (int i = 0; i < length; i++)
    {
        char c = data[i];
        c ^= key;

        value += c;
    }

    return value;
}
