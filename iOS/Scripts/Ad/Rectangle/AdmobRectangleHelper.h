//
//  ImobileRectangleHelper.h
//
//  Created on 14/10/2014.
//
//

#import "RectangleAdController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface AdmobRectangleHelper : RectangleAdController <GADBannerViewDelegate>

@property(nonatomic) UIView *uv;

-(UIView *)prepareRectangleAd:(CGRect)frame;
-(void) reloadRectangleAd;
-(void) removeRectangleAd;
@end
