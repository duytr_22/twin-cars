﻿//  AdManager.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.27.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 広告関連を管理するクラス
/// </summary>
public class AdManager : SingletonMonoBehaviour<AdManager>
{

	#if !NO_AD || !NO_NAITIVE
	//ネイティブ連携用プラグイン
	private AdPlugin _adPlugin = null;
	#endif

	//仮のシーン名
	private const string TITLE_HELP_SCENE_NAME = "TITLE_HELP_SCENE_NAME";
	private const string PAUSE_SCENE_NAME = "PAUSE_SCENE_NAME";

	//審査中か
	private bool _canShowSplash = false;

	public  bool  CanShowSplash {
		set{ _canShowSplash = value; }
		get{ return _canShowSplash; }
	}

	//審査中か
	private bool _canShowVideo = false;

	public  bool  CanShowVideo {
		set{ _canShowVideo = value; }
		get{ return _canShowVideo; }
	}

	//審査中か
	private bool _isRevewing = true;

	public  bool  IsRevewing {
		get{ return _isRevewing; }
	}

	private int _ranking = -1;

	public int Ranking {

		get{ return _ranking; }
	}

	public void UpdateRanking (string value)
	{
		_ranking = int.Parse (value);
	}

	#if !NO_AD || !NO_NAITIVE
	//広告用の各シーンの番号
	private readonly static Dictionary<string, int> SCENE_NO_DICT_FOR_AD = new Dictionary<string, int> () {
		{ SceneName.TITLE,        0 },
		{ SceneName.GAME,         1 },
		{ SceneName.RESULT,       2 },
		{ SceneName.TUTORIAL,     3 },
		{ SceneName.BALL,       4 },
		{ TITLE_HELP_SCENE_NAME,  1000 },
		{ PAUSE_SCENE_NAME,       10000 }
	};
	#endif

	//=================================================================================
	//初期化
	//=================================================================================
	//広告を表示しないデバッグ
	#if !NO_AD || !NO_NAITIVE

	protected override void Init ()
	{
		base.Init ();

		//ネイティブ連携用プラグイン初期化
		_adPlugin = gameObject.AddComponent<AdPlugin> ();
		_adPlugin.Init ();

		//Tenjin.getInstance ("QENGYAKZDN1V4C9UQUBFLMGPYKIJ8VXU").Connect ();
	}

	void OnApplicationPause (bool pauseStatus)
	{
		if (!pauseStatus) {
			
			//Tenjin.getInstance ("API_KEY").Connect ();
		} 
	}

	private void Start ()
	{
		if (!_adPlugin) {
			return;
		}

		//リーダーボード表示時に広告を重ねるように
		RankingManager.Instance.OnShowLeaderBoard += _adPlugin.AddAdToLeaderBoard;

		//Android or Unity上では即ネイティブの初期化が終わった事にする(審査かどうか待つ必要がないため)
		#if UNITY_ANDROID || UNITY_EDITOR
		OnInitNative ("false");
		#endif
		
	}

	#endif
	//=================================================================================
	//内部
	//=================================================================================
	#if !NO_AD || !NO_NAITIVE

	//ネイティブ側の初期化が終わった時のメソッド
	private void OnInitNative (string isRevewing)
	{
		_isRevewing = isRevewing == "true";
		UpdateAdWithScene (SceneNavigator.Instance.CurrentSceneName, false);
		Debug.Log ("OnInitNative : " + _isRevewing);
	}

	//必要であればスプラッシュ広告を表示
	private void ShowSplashAdIfNeeded (int sceneNo)
	{
		if (!_canShowSplash)
			return;

		//タイトルとリザルト以外では表示しないように
		if (sceneNo == SCENE_NO_DICT_FOR_AD [SceneName.TITLE]) {
			_adPlugin.ShowSplashAd (sceneNo, UserData.PlayCount);	
		}		
	}

	public void ShowSplashAd ()
	{
		if (!_canShowSplash)
			return;
		
		_adPlugin.ShowSplashAd (SceneNo.RESULT, UserData.PlayCount);					
	}

	#endif
	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// シーンに合わせて各広告の表示or非表示を設定
	/// </summary>
	public void UpdateAdWithScene (string sceneName, bool canSplash = true)
	{
		#if !NO_AD || !NO_NAITIVE

		if (!_adPlugin) {
			return;
		}

		int sceneNoForAd = SCENE_NO_DICT_FOR_AD [sceneName];

		//シーンに合わせて広告の更新
		_adPlugin.UpdateAdByScene (sceneNoForAd);

		//必要であればスプラッシュ広告を表示
		if (canSplash && SceneNavigator.Instance.BeforeSceneName != SceneName.BALL) {
			
//			ShowSplashAdIfNeeded (sceneNoForAd);
		}

		#endif
	}

	/// <summary>
	/// タイトルでヘルプを表示した時の広告設定に
	/// </summary>
	public void UpdateAdWithTitleHelp ()
	{
		#if !NO_AD || !NO_NAITIVE
		UpdateAdWithScene (TITLE_HELP_SCENE_NAME);
		#endif
	}

	/// <summary>
	/// ポーズ画面の広告の表示設定
	/// </summary>
	public void UpdateAdWithPause (bool isPause)
	{
		#if !NO_AD || !NO_NAITIVE
		UpdateAdWithScene (isPause ? PAUSE_SCENE_NAME : SceneName.GAME);
		#endif
	}

	/// <summary>
	/// ウォール広告表示
	/// </summary>
	public void ShowWallAd ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.ShowWallAd ();
		#endif
	}

	public bool CanShowRewardVideo ()
	{
		#if !NO_AD || !NO_NAITIVE

		if(!_adPlugin)
		{
			return false;
		}

		return _adPlugin.CanShowRewardVideo ();
		#endif
	}

	public void ShowRewardVideo ()
	{
		if (SceneNavigator.Instance.CurrentSceneName == SceneName.GAME) {

			#if !NO_AD || !NO_NAITIVE
			_adPlugin.ShowRewardVideo ();
			#endif
		}
	}

	// Sound for ads
	public void StopAllSound ()
	{
		SEManager.Instance.muteAll ();
		BGMManager.Instance.muteAll ();
	}

	public void ResumeAllSound ()
	{
		SEManager.Instance.resumeAll ();
		BGMManager.Instance.resumeAll ();
	}

	public void PauseGame ()
	{
		Time.timeScale = 0;
	}

	public void ResumeGame ()
	{
		Time.timeScale = 1;
	}

	public void FinishedPlayingMovieAd (string success)
	{
		if (SceneNavigator.Instance.CurrentSceneNo != SceneNo.GAME)
			return;

		_canShowVideo = false;

		if (success.Equals ("true")) {

			PlayerPrefs.SetInt (Define.CONTINUE_GAME, 1);
			PlayerPrefs.Save ();
		}

		AdManager.Instance.ResumeGame ();
		GameManager.Instance.End (false);
	}

	public void SetCanShowVideo ()
	{
		_canShowVideo = true;
	}

	public void HideAllAds ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.HideAllAds ();
		#endif
	}

	public void ShowIndicator ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.ShowIndicator ();
		#endif
	}

	public void HideIndicator ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.HideIndicator ();
		#endif
	}

	public void ShowGameRect ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.ShowGameRect ();
		#endif
	}

	public void HideGameRect ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.HideGameRect ();
		#endif
	}

	public void ShowPopupRect ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.ShowPopupRect ();
		#endif
	}

	public void HidePopupRect ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.HidePopupRect ();
		#endif
	}

	/// <summary>
	///  Android IAP
	/// </summary>

	public void Buy ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.Buy ();
		#endif
	}

	public void Restore ()
	{
		#if !NO_AD || !NO_NAITIVE
		_adPlugin.Restore ();
		#endif		
	}

	public void PurchaseSuccess ()
	{
		PlayerPrefs.SetInt (Define.NO_ADS, 1);
		PlayerPrefs.Save ();

		AdManager.Instance.HideAllAds ();
	}


	public void interstitalCompleted ()
	{
		StartCoroutine (DelayPauseButton ());

		AdManager.Instance.ResumeGame ();
		BGMManager.Instance.Resume ();
	}

	IEnumerator DelayPauseButton()
	{
		yield return new WaitForSeconds (1);
		MainManager.Instance.showAdWhenRetry = false;
	}

	public void startInterstital ()
	{

		BGMManager.Instance.Pause ();
	}
}