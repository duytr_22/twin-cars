﻿/*
 * RaiseUpYForiPhoneX.cs
 * Created by Loi VT - New Gate
 * Created on: #CREATIONDATE#
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiResolutionUI : MonoBehaviour
{

    enum SupportType
    {
        RaiseUpPositionY,
        RaiseDownForTopAnchor,//ssupport ngui only
        SetScaleYBigger,//for mirnor anchor
        HorizontalGridForiPad,
    };

    [SerializeField]
    SupportType _supportType = SupportType.RaiseUpPositionY;

    [SerializeField]
    float _factorForIPhoneX = 1.0f;

    [SerializeField]
    float _topDownOffset = 58.0f;
    // Use this for initialization
    void Start()
    {
        if (IsIPhoneX())
        {
            Debug.Log("ipx");
            switch (_supportType)
            {
                case SupportType.RaiseUpPositionY:
                    RaiseUpY();
                    break;

                case SupportType.RaiseDownForTopAnchor:
                    RaiseDownTopAnchor();
                    break;

                case SupportType.SetScaleYBigger:
                    SetScaleYBigger();
                    break;
            }
        }
        if (IsIPad())
        {
            switch (_supportType)
            {
                case SupportType.HorizontalGridForiPad:
                    {
                        FixHorizontalGridForIPad();
                    }
                    break;

                default:
                    break;
            }
        }
    }

    void RaiseUpY()
    {
        transform.SetPositionY(transform.position.y * GetDeviceScale() * _factorForIPhoneX);
    }

    void RaiseDownTopAnchor()
    {
        // Debug.Log(GetDeviceScale());
        var anchor = GetComponent<UIAnchor>();
        if (anchor != null)
        {
            anchor.pixelOffset.y = -(_topDownOffset * GetDeviceScale());
        }
    }

    public void SetScaleYBigger()
    {
        transform.SetLocalScaleY(transform.localScale.y * GetDeviceScale());
    }

    private void FixHorizontalGridForIPad()
    {
        var grid = GetComponent<UIGrid>();
        if (grid)
        {
            grid.cellWidth /= GetDeviceScale();
            grid.Reposition();
        }
    }
    public static bool IsIPad()
    {
        bool isIpad = SystemInfo.deviceModel.Contains("iPad");

        //エディター上ではiPadの判定が取れないのでアス比で判断
#if UNITY_EDITOR && UNITY_IOS

        float aspectRate = (float)Screen.width / (float)Screen.height;

        if (Mathf.Abs(aspectRate - (3f / 4f)) < 0.001f)
        {
            isIpad = true;
        }

#endif
        return isIpad;
    }

    public static bool IsSupportHaptic()
    {
        bool result = true;
#if UNITY_IOS && !UNITY_EDITOR
        UnityEngine.iOS.DeviceGeneration generation = UnityEngine.iOS.Device.generation;
        if(generation != UnityEngine.iOS.DeviceGeneration.iPhone
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone3G
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone3GS
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone4
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone4S
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone5
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone5C  
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone5S
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone6
            && generation != UnityEngine.iOS.DeviceGeneration.iPhone6Plus)
        {
            result = true;
        }        
        else
        {
            result = false;
        }
#endif
        if (IsIPad())
            result = false;

        return result;
    }

    //--NG--
    public static bool IsIPhoneX()//iphone x, xr, xs, xsmax or android device have same same resolusion
    {
        return GetDeviceScale() > 1.1f;
    }
    public static float GetDeviceScale()//calculate device scale base on height
    {
        Vector2 standardResolution = new Vector2(750, 1334);
        Vector2 frameSize = new Vector2(Screen.width, Screen.height);
        float factor = standardResolution.y / standardResolution.x;
        float shouldBeHeight = frameSize.x * factor;
        float scale = frameSize.y / shouldBeHeight;
        return scale;
    }
}

