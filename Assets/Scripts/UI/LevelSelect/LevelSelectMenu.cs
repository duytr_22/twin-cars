﻿//  LevelSelectMenu.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.25.

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(GridDeployer))]

/// <summary>
/// レベルの選択を行うためのメニュー
/// </summary>
public class LevelSelectMenu : MonoBehaviour
{

	//何列何行のテーブルにするかと並べる間隔
	[SerializeField]
	private Vector2 _buttonNumInTable = Vector2.zero, _space = Vector2.zero;

	//テーブル
	private List<GameObject> _tableList = new List<GameObject> ();
	private int _currentTableNo = 0;

	//テーブル移動ボタン
	[SerializeField]
	private ButtonAssistant _backTableButton = null, _nextTableButton = null;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Awake ()
	{
		_tableList = gameObject.GetChildren ();
	}

	private void Start ()
	{
		//表示するテーブルを決定、テーブル移動
		int recentlyStageNo = CacheData.Entity.StageNo;
		_currentTableNo = (recentlyStageNo - 1) / (int)(_buttonNumInTable.x * _buttonNumInTable.y);
		transform.localPosition = GetMovePostion ();

		Recreate ();
		UpdateMoveTableButton ();
	}

	//エディタ上だけで実行
	#if UNITY_EDITOR
	private void OnValidate ()
	{
		if (Application.isPlaying) {
			return;
		}

		//並ばせる個数は整数に
		_buttonNumInTable = new Vector2 (Mathf.Max (1, Mathf.Floor (_buttonNumInTable.x)), Mathf.Max (1, Mathf.Floor (_buttonNumInTable.y)));

		EditorApplication.delayCall += () => {
			if (Application.isPlaying) {
				return;
			}
			Recreate ();
		};
	}
	#endif

	//=================================================================================
	//メニュー作成
	//=================================================================================

	/// <summary>
	/// メニューを最生成する
	/// </summary>
	[ContextMenu ("Recreate")]
	private void Recreate ()
	{
		//子を全て消す
		foreach (GameObject child in gameObject.GetChildren()) {
			DestroyImmediate (child);
		}

		_tableList = new List<GameObject> ();

		//ステージの個数分ボタンを作成
		GameObject currentTable = CreateTable ();
		for (int i = 0; i < UnityProjectSetting.Entity.StageNum; i++) {

			//次のテーブルを作るか判定
			if (i == _buttonNumInTable.x * _buttonNumInTable.y * _tableList.Count) {
				currentTable = CreateTable ();
			}

			//ボタン作成
			GameObject button = Instantiate (Resources.Load<GameObject> (ResourcesFilePath.PREFAB_LEVEL_SELECT_BUTTON));
			button.SetActive (true);

			button.transform.SetParent (currentTable.transform);
			button.transform.localScale = Vector3.one;

			//ボタン初期化
			button.GetComponent<LevelSelectButtonAssistant> ().SetStageNo (stageNo: i + 1);

		}

		//各ボタンを並べる
		foreach (GameObject table in _tableList) {
			GridDeployer gridDeployer = table.GetComponent<GridDeployer> ();

			gridDeployer.NextPivot = GridDeployer.PivotType.First;
			gridDeployer.Space = _space;
			gridDeployer.Limit = (int)_buttonNumInTable.x;

			gridDeployer.Deploy ();
		}

		//各テーブルを並べる
		GetComponent<GridDeployer> ().Deploy ();
	}

	//テーブル作成
	private GameObject CreateTable ()
	{
		GameObject table = new GameObject ("Table");

		table.transform.SetParent (transform);
		table.transform.position = Vector3.zero;
		table.transform.localScale = Vector3.one;

		table.AddComponent<GridDeployer> ();

		_tableList.Add (table);
		return table;
	}


	//=================================================================================
	//テーブル移動
	//=================================================================================

	//テーブル移動
	private void MoveTable (bool isRight)
	{
		_currentTableNo += isRight ? 1 : -1;
		_currentTableNo = Mathf.Clamp (_currentTableNo, 0, _tableList.Count - 1);

		//移動アニメーション
		TweenPosition tweenPostion = TweenPosition.Begin (gameObject, 0.3f, GetMovePostion ());
		tweenPostion.animationCurve = AnimationCurve.EaseInOut (0, 0, 1, 1);

		//ボタンの表示設定
		UpdateMoveTableButton ();
	}

	//移動先の座標を取得
	private Vector3 GetMovePostion ()
	{
		Vector3 postion = transform.localPosition;
		postion.x = -_currentTableNo * GetComponent<GridDeployer> ().Space.x;
		return postion;
	}

	//テーブル移動ボタンの表示非表示設定
	private void UpdateMoveTableButton ()
	{
		_backTableButton.gameObject.SetActive (_currentTableNo > 0);
		_nextTableButton.gameObject.SetActive (_currentTableNo < _tableList.Count - 1);
	}

}