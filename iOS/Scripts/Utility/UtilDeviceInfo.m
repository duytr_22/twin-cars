//
//  UtilDeviceInfo.m
//
//  Copyright © 2016 All rights reserved.
//

#import "UtilDeviceInfo.h"

@implementation UtilDeviceInfo

static BOOL isJapanese = YES;
+(BOOL)isJapaneseLanguage{ return isJapanese;}

+(void) confirmDeviceLanguage {
	NSString *countryCode = [[NSLocale preferredLanguages] objectAtIndex:0];
	isJapanese = [countryCode hasPrefix:@"ja"];
}


@end
