//
//  AdManager.m
//  Unity-iPhone
//
//  Created by kikuchikan on 2015/10/27.
//
//  広告関係の処理を行うクラス

#import "FlurryHelper.h"
#import "AdManager.h"
#import "AdControllerInitializer.h"
#import "StartupPopAdHelper.h"
#import "UtilDeviceInfo.h"
#import "MovieAdController.h"
#import "UnityAppController.h"

@implementation AdManager

//=================================================================================
//初期化
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName{
    
    if (self = [super init]) {
        //do stuff
    }
    
    _isFirstTimes = NO;
    
    //各フラグ、広告のコントローラーを初期化
    _isConfirmedGAE      = NO;
    _isPreparedHouseAd   = NO;
    
    _baseInterstitialAdController = nil;
    _rankInterstitialAdController = nil;
    _bannerAdController = nil;
    
    _gameRectangleAdController = nil;
    _endRectangleAdController     = nil;
    
    _movieAdController = nil;
    
    //Flurry開始
    [FlurryHelper startFlurry];
    
    //デバイスの言語を取得
    [UtilDeviceInfo confirmDeviceLanguage];
    
    //GAE confirmation
    [GAEConfig confirmGAE:self selector:@selector(confirmedGAE)];
       
    return self;
}

//GAEの確認が終了した時のコールバック
- (void)confirmedGAE{
    _isConfirmedGAE = YES;
    
    //必要に応じて広告開始
    [self startAdIfNeeded];
}

//自社広告の準備が終了した時のコールバック
- (void)preparedHouseAd:(BOOL)isRevewing{
    
    _isPreparedHouseAd = YES;
    
#ifdef DEBUG_IN_REVIEW
    _isRevewing = true;
#elif DEBUG_NOT_IN_REVIEW
    _isRevewing = false;
#endif
    
    NSLog(@"preparedHouseAd:(isRevewing %@)", isRevewing ? @"true" : @"false");
    
    //必要に応じて広告開始
    [self startAdIfNeeded];
}

//=================================================================================
//内部
//=================================================================================

//必要に応じて広告開始
- (void)startAdIfNeeded{
    
    if(!_isConfirmedGAE){
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _isRemoveAds = [defaults valueForKey:@"remove_ads"];
    
//    StartupPopAdHelper* popupAd = [[StartupPopAdHelper alloc] init];
//    [popupAd showStartupPopAd];
    
    if(!_isRemoveAds) {
     
        //レクタングル広告
        _pauseRectangleAdController     = [AdControllerInitializer initPauseRectangleAdController];
        _endRectangleAdController     = [AdControllerInitializer initEndRectangleAdController];
        _gameRectangleAdController    = [AdControllerInitializer initGameRectangleAdController];
        _popupRectangleAdController    = [AdControllerInitializer initPopupRectangleAdController];
        
        _bannerAdController = [AdControllerInitializer initBannerAdController];
//        _resultBigBannerAdController = [AdControllerInitializer initResultBigBannerAdController];
        
        _baseInterstitialAdController = [AdControllerInitializer initBaseInterstitialAdController];
//        _rankInterstitialAdController = [AdControllerInitializer initRankInterstitialAdController];
        
        [_bannerAdController updateBannerAd:true];
    }
    
    _movieAdController = [AdControllerInitializer initMovieAdController];
    
    { // Show crosspromotion
        NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
        int showStartupPopAd = (int)[userDefault integerForKey:@"ShowStartupPopAd"];
     
        if(showStartupPopAd > 1) {
            
            NSString *customURL = @"jp.keitachibana.Pinball://";
            
            if (!_isRemoveAds && ![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURL]])
            {
                
//                UnitySendMessage("CrossPromotion", "ShowCrossPromotion", "");
            }
        }
        
        [userDefault setInteger:showStartupPopAd + 1 forKey:@"ShowStartupPopAd"];
        [userDefault synchronize];
    }
}

//=================================================================================
//外部
//=================================================================================

//スプラッシュ広告を表示
-(void)showInterstitialAd:(int)scene playCount:(int)playCount{
    
    if(_isRemoveAds)
        return;
    
//    if (scene == SCENE_RESULT) {
    
        [_baseInterstitialAdController showInterstitialAd:playCount];
//    }
//    else{
//        [_rankInterstitialAdController showInterstitialAd:playCount];
//    }
}

- (void) showGameRect:(BOOL) enable {
    
    if(_isRemoveAds)
        return;
    
    [_bannerAdController updateBannerAd:false];
    [_gameRectangleAdController updateRectangleAd:enable];
}

- (void) showPopupRect:(BOOL) enable {
    
    if(_isRemoveAds)
        return;
    
    [_popupRectangleAdController updateRectangleAd:enable];
}

-(void)showIndicator:(BOOL) enable
{
    if(enable) {
        
        [GetAppController() showIndicator];
        
    } else {
        
        [GetAppController() hideIndicator];
        
    }
}

-(void)showMovieAd{
    
    [_movieAdController show];
}

-(BOOL)canShowMovieAd
{
    return  [_movieAdController canShow];
}

-(void) hideAllAds
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:true forKey:@"remove_ads"];
    [defaults synchronize];
    
    _isRemoveAds = true;
    
    [_bannerAdController updateBannerAd:false];
//    [_resultBigBannerAdController updateBannerAd:false];
    
    [_endRectangleAdController updateRectangleAd:false];
    [_pauseRectangleAdController updateRectangleAd:false];
    [_gameRectangleAdController updateRectangleAd:false];
}

//シーンに合わせて広告の更新
-(void)updateAdByScene:(int)scene{
    
    if(_isRemoveAds)
        return;
    
    //バナー広告
    [_bannerAdController updateBannerAd:scene != SCENE_RESULT];
//    [_resultBigBannerAdController updateBannerAd:scene == SCENE_RESULT];
    
    [_endRectangleAdController updateRectangleAd:scene == SCENE_RESULT];
    [_pauseRectangleAdController updateRectangleAd:scene == SCENE_PAUSE];
    
    [_gameRectangleAdController updateRectangleAd:false];
}

@end
