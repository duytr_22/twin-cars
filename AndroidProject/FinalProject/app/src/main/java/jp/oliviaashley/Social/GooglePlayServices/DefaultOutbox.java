package jp.oliviaashley.Social.GooglePlayServices;

import android.content.Context;
import android.content.SharedPreferences;

public class DefaultOutbox {

    private static final String PREFERENCES_NAME = "DefaultOutbox";
    private static final long RESET = -999;
    private static final long NODATA = -998;
    // FIXME name it correctly

    // this is just pendant to unlock achievements and leaderboards

    private static final String LEADERBOARD_WORLD_RANKING_NAME = "leaderboard_world_ranking";
    private long leaderboard_world_ranking = -1;

    boolean isEmpty() {
        return leaderboard_world_ranking < 0;
    }

    public void saveLocal(Context ctx) {
        saveLocal(ctx, NODATA);
    }

    public void saveLocal(Context ctx, long score) {
        if (score == NODATA) {
            return;
        }
        if (score >= RESET) {
            leaderboard_world_ranking = score;
            SharedPreferences settings = ctx.getSharedPreferences(
                    PREFERENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putLong(LEADERBOARD_WORLD_RANKING_NAME, score);
            // Commit the edits!
            editor.apply();
        }
    }

    public void loadLocal(Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES_NAME,
                Context.MODE_PRIVATE);
        leaderboard_world_ranking = settings.getLong(LEADERBOARD_WORLD_RANKING_NAME, RESET);
    }

    public long getLeaderboard_world_ranking() {
        return leaderboard_world_ranking;
    }

    public void setLeaderboard_world_ranking(long leaderboard_world_ranking) {
        this.leaderboard_world_ranking = leaderboard_world_ranking;
    }

}
