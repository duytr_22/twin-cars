//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  Nendのスプラッシュ(インターステイシャル)広告コントローラー

#import "InterstitialAdController.h"
#import <IronSource/IronSource.h>

@interface IronSouceInterstitialAdController : InterstitialAdController<ISInterstitialDelegate>{

}

-(id)init;
-(void)showInterstitialAd:(int)playCount;

@end
