﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct TutorialSpawnPoints
{
	public int LeftPoint;
	public int RightPoint;
}

public class SpawnPointScript : MonoBehaviour
{
	public GameObject cubePrefab;
	[HideInInspector] public List<CubeScript> activeCubes;
	[HideInInspector] public List<CubeScript> inActiveCubes;

	private int inactiveCount;

	//cube random position
	private int lastLeft;
	private int lastRight;
	private int SamelineCount;

	// Use this for initialization
	void Start()
	{
		activeCubes = new List<CubeScript>();
		inActiveCubes = new List<CubeScript>();


		//tutorial 
		//spawn point
		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial)
		{
			for (int i = 0; i < 16; i++)
			{
				TutorialSpawnPoints points;
				int[] linePos = this.randomCubeLine();
				points.LeftPoint = linePos[0];
				points.RightPoint = linePos[1];
				lastLeft = linePos[0];
				lastRight = linePos[1];

				TutorialManager.Instance.tutorialSpawnPoints.Add(points);
			}
		}
	}

	public void SpawnLineOfCube()
	{
		//i = -1 is left side, i = 1 is right side
		int[] linePos = this.randomCubeLine();

		lastLeft = linePos[0];
		lastRight = linePos[1];

		List<int> randomList = new List<int>() {-1, 1};

		int id = 0;
		for (int i = -1; i < 2; i += 2)
		{
			//random position
			int r = linePos[id];
			id++;
			CubeScript script = GetInActiveCubeScript();
			// float cubeHeight = 
			// 	script.gameObject.GetComponent<CarsRenderPatternController>().GetRenderer().bounds.size.y;
			// var cubeHeight = 1f;
			Transform parent = transform.parent;
			Vector3 dir = parent.right.normalized;
			dir = (dir * (r + 0.5f)) * i;
			dir.y = 0.5f;
			Vector3 localposition = parent.InverseTransformPoint(this.transform.position + dir);

			script.InitCubeWithLocalPosition(localposition);

			var randomSpeedDirection = randomList[Random.Range(0, randomList.Count)];
			randomList.Remove(randomSpeedDirection);
			script.RandomizeSpeed(randomSpeedDirection);
		}
	}


	public void TitleSpawnLineOfCube()
	{
		//i = -1 is left side, i = 1 is right side
		int num = Random.Range(1, 3);
		if (num == 1)
		{
			int r = 1;
			int n = num * Random.Range(0, 2) == 0 ? -1 : 1;

			CubeScript script = this.GetInActiveCubeScript();
			float cubeHeight = script.GetComponent<Renderer>().bounds.size.y;
			Transform parent = this.transform.parent;
			Vector3 dir = parent.right.normalized;
			dir = (dir * (r + 0.5f)) * n;
			dir.y = cubeHeight * 0.5f;
			Vector3 localposition = parent.InverseTransformPoint(this.transform.position + dir);

			script.InitCubeWithLocalPosition(localposition);
		}
		else
		{
			int id = 0;

			for (int i = -1; i < 2; i += 2)
			{
				//random position
				int r = 1;
				id++;
				CubeScript script = this.GetInActiveCubeScript();
				float cubeHeight = script.GetComponent<Renderer>().bounds.size.y;
				Transform parent = this.transform.parent;
				Vector3 dir = parent.right.normalized;
				dir = (dir * (r + 0.5f)) * i;
				dir.y = cubeHeight * 0.5f;
				Vector3 localposition = parent.InverseTransformPoint(this.transform.position + dir);

				script.InitCubeWithLocalPosition(localposition);
			}
		}
	}

	public void TutorialSpawnLineOfCube()
	{
		if (TutorialManager.Instance.tutorialMaxRowsCube < TutorialManager.Instance.tutorialSpawnPoints.Count &&
		    TutorialManager.Instance.demoPlay)
		{
			TutorialSpawnPoints point =
				TutorialManager.Instance.tutorialSpawnPoints[TutorialManager.Instance.tutorialMaxRowsCube];
			int[] linePos = new int[2] {point.LeftPoint, point.RightPoint};

			int id = 0;
			for (int i = -1; i < 2; i += 2)
			{
				//random position
				int r = linePos[id];
				id++;
				CubeScript script = this.GetInActiveCubeScript();
				float cubeHeight = script.GetComponent<Renderer>().bounds.size.y;
				Transform parent = this.transform.parent;
				Vector3 dir = parent.right.normalized;
				dir = (dir * (r + 0.5f)) * i;
				dir.y = cubeHeight * 0.5f;
				Vector3 localposition = parent.InverseTransformPoint(this.transform.position + dir);

				script.InitCubeWithLocalPosition(localposition);
				script.RandomizeSpeed(0);
			}

			TutorialManager.Instance.tutorialMaxRowsCube++;
			if (TutorialManager.Instance.tutorialMaxRowsCube == 4)
			{
				TutorialManager.Instance.demoPlay = false;
			}
		}
	}

	int[] randomCubeLine()
	{
		int[] linePos = new int[2];
		for (int i = 0; i < 2; i++)
		{
			int r = Random.Range(0, 2);
			linePos[i] = r;
		}

		//check duplicate line
		if (linePos[0] == lastLeft && linePos[1] == lastRight)
		{
			if (SamelineCount < 1)
			{
				SamelineCount++;
			}
			else
			{
				SamelineCount = 0;
				return this.randomCubeLine();
			}
		}

		return linePos;
	}

	CubeScript GetInActiveCubeScript()
	{
		CubeScript script;
		if (inActiveCubes.Count > 0)
		{
			script = inActiveCubes[0];
			inActiveCubes.RemoveAt(0);
			activeCubes.Add(script);
		}
		else
		{
			GameObject obj = (GameObject) Instantiate(cubePrefab, this.transform.position, this.transform.rotation);
			obj.transform.SetParent(this.transform.parent);
			script = obj.GetComponent<CubeScript>();
			activeCubes.Add(script);
		}

		return script;
	}

	public void CubeOnInActive(GameObject obj)
	{
		CubeScript script = obj.GetComponent<CubeScript>();
		activeCubes.Remove(script);
		inActiveCubes.Add(script);
		inactiveCount++;

		if (inactiveCount >= 2)
		{
			inactiveCount = 0;
			GameManager.Instance.AddScore(1);
		}
	}
}