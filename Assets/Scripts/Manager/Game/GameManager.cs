﻿//  GameManager.cs
//  ProductName BreakoutTower
//
//  Created by kikuchikan on 2015.08.05.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// ゲームシーンを管理するマネージャ
/// </summary>
public class GameManager : SingletonMonoBehaviour<GameManager>
{
	public enum PlayMode
	{
		PlayModeTitle,
		PlayModeGame,
		PlayModeTutorial,
	}

	//スコア
	public int _score = 0;
	public PlayMode playMode;

	public GameObject gamePlay;
	// private GameObject background;

	[SerializeField] GameObject dialogContinue, noThanksButton;

	[SerializeField] GameObject labelNewBall, labelNewStage, labelNewStageTop;

	[SerializeField] GameObject[] _theme;

	[SerializeField] GameObject _snowEffect;

	public bool _alreadyContinue = false;

	private bool _canShowPauseAds = true;

	public bool CanShowPauseAds
	{
		get { return _canShowPauseAds; }
	}

	//ゲーム中フラグ
	private bool _isPlayingGame = false;

	public bool IsPlayingGame
	{
		get { return _isPlayingGame; }
	}

	//イベント
	public event Action<bool> OnGameEnd = delegate { };
	public event Action<int> OnChangeScore = delegate { };

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Init()
	{
		base.Init();

		_isPlayingGame = false;
		CacheData.Entity.IsClear = false;

		int idx = PlayerPrefs.GetInt(Define.STAGE_SELECTED, 1);

		for (int i = 0; i < _theme.Length; i++)
		{
			_theme[i].SetActive(false);
		}

		// _theme [idx - 1].SetActive (true);

		// background = _theme [idx - 1];

		if (idx == 3)
		{
			_snowEffect.SetActive(true);
		}

		if (playMode == PlayMode.PlayModeGame)
			SceneNavigator.Instance.AddScene(SceneName.UIGAMEPLAY);
		else if (playMode == PlayMode.PlayModeTitle)
			SceneNavigator.Instance.AddScene(SceneName.UITITLE);

		PlayerPrefs.SetInt(Define.IS_SHOW_TUTORIAL_KEY, 0);
	}

	public void Start()
	{
		if (PlayerPrefs.GetInt(Define.CONTINUE_GAME, 0) == 1 &&
		    SceneNavigator.Instance.CurrentSceneName == SceneName.GAME)
		{
			_alreadyContinue = true;

			_score = PlayerPrefs.GetInt(Define.LAST_SCORE, 0);
		}

		//ミニゲームでない時の処理
		if (UnityProjectSetting.Entity.GameType != GameType.Mini)
		{
			string sceneName = SceneNavigator.Instance.CurrentSceneName;

			//ゲームシーンならステージシーン合成
			if (sceneName == SceneName.GAME)
			{
				SceneNavigator.Instance.AddScene("Stage" + CacheData.Entity.StageNo.ToString());
			}
			//ステージシーンならステージNoを設定
			else
			{
				CacheData.Entity.StageNo = int.Parse(sceneName.Substring("Stage".Length));
			}
		}
	}

	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// スコアを加算する
	/// </summary>
	public void AddScore(int score)
	{
		_score += score;
		OnChangeScore(_score);

		if (SceneNavigator.Instance.CurrentSceneName == SceneName.GAME)
		{
			// Updat total score
			int totalScore = PlayerPrefs.GetInt(Define.TOTAL_SCORE, 0);
			totalScore += score;

			// Debug.Log ("totalScore " + totalScore);

			// Save new total score
			PlayerPrefs.SetInt(Define.TOTAL_SCORE, totalScore);
			PlayerPrefs.Save();

			// Check total score to unlock ball - 500 score each step
			int idx = (totalScore / 500) + 1;

			if (idx > 1 && idx <= 9)
			{
				if (PlayerPrefs.GetInt(string.Format(Define.BALL_UNLOCK, idx), 0) == 0 && labelNewBall)
				{
					// Not unlock yet

					labelNewBall.SetActive(true);

//					TweenAlpha.Begin (labelNewBall, 0.0f, 255);

					labelNewBall.GetComponent<TweenAlpha>().PlayForward();

					Invoke("DisableLabel", 11);

					PlayerPrefs.SetInt(string.Format(Define.BALL_UNLOCK, idx), 1);
				}
			}

			idx = (totalScore / 1500) + 1;

			if (idx > 1 && idx <= 5)
			{
				if (PlayerPrefs.GetInt(string.Format(Define.STAGE_UNLOCK, idx), 0) == 0 && labelNewStage)
				{
					// Not unlock yet

					if (labelNewBall.activeSelf)
					{
						labelNewStage.SetActive(true);

//						TweenAlpha.Begin (labelNewStage, 0.0f, 255);

						labelNewStage.GetComponent<TweenAlpha>().PlayForward();
					}
					else
					{
						labelNewStageTop.SetActive(true);

//						TweenAlpha.Begin (labelNewStageTop, 0.0f, 255);

						labelNewStageTop.GetComponent<TweenAlpha>().PlayForward();
					}

					Invoke("DisableLabel", 11);

					PlayerPrefs.SetInt(string.Format(Define.STAGE_UNLOCK, idx), 1);
				}
			}
		}
	}

	void DisableLabel()
	{
		labelNewBall.SetActive(false);
		labelNewStage.SetActive(false);
		labelNewStageTop.SetActive(false);
	}

	/// <summary>
	/// ゲーム開始
	/// </summary>
	public void Begin()
	{
		_isPlayingGame = true;
		gamePlay.SetActive(true);
		// background.GetComponent<UIBackScript> ().StartUIBack ();
	}

	public void ShowContinue()
	{
#if !CHEAT_ADS
		if (AdManager.Instance.CanShowRewardVideo() && !_alreadyContinue)
		{
			_canShowPauseAds = false;

			AdManager.Instance.ShowGameRect();

			dialogContinue.SetActive(true);

			float delayTime = 1.5f;

			UIButton button = noThanksButton.GetComponent<UIButton>();
			button.isEnabled = false;

			//演出が終わったらリザルトへ
			StartCoroutine(this.DelayMethod(delayTime, () =>
			{
				button.isEnabled = true;
				button.defaultColor = Color.white;
			}));
		}
		else
		{
			End(false);
		}
#else
		End(false);
#endif
	}

	/// <summary>
	/// ゲーム終了
	/// </summary>
	public void End(bool isClear)
	{
		PlayerPrefs.SetInt(Define.LAST_SCORE, _score);

		//stop background
		// background.GetComponent<UIBackScript> ().StopUIBack ();

		//複数回実行しないようにフラグ確認
		if (!_isPlayingGame)
		{
			return;
		}

		_isPlayingGame = false;

		//イベント実行
		OnGameEnd(isClear);

		//Levelはステージ数をスコアにする
		if (UnityProjectSetting.Entity.GameType == GameType.LevelSelect)
		{
			_score = isClear ? CacheData.Entity.StageNo : 0;
		}

		//現在のハイスコア取得&更新(モードセレクトの場合はステージ毎に取得)
		int highScore = 0;

		if (UnityProjectSetting.Entity.GameType == GameType.ModeSelect)
		{
			string key = CacheData.Entity.StageNo.ToString();
			highScore = UserData.HighScoreDict.GetValueOrDefault(key, 0);
			UserData.HighScoreDict[key] = _score > highScore ? _score : highScore;

			//ランキングにスコア送信
			RankingManager.Instance.ReportScore(CacheData.Entity.StageNo, _score);
		}
		else
		{
			highScore = UserData.HighScore;
			UserData.HighScore = _score > highScore ? _score : highScore;

			//ランキングにスコア送信
			RankingManager.Instance.ReportScore(_score);
		}

		//新記録かどうか
		bool isNewRecored = _score > highScore;

		//Levelタイプはステージを初めてクリアした時にFlurryに送信するように
		if (UnityProjectSetting.Entity.GameType == GameType.LevelSelect && isNewRecored)
		{
			CommonNativeManager.Instance.ReportClearStageNo(CacheData.Entity.StageNo);
		}

		//キャッシュにフラグとスコア登録
		CacheData.Entity.IsNewRecored = isNewRecored;
		CacheData.Entity.IsClear = isClear;
		CacheData.Entity.CurrentScore = _score;

		//BGMフェードアウト
		BGMManager.Instance.FadeOutBGM();

		//演出&SE
		float delayTime = 0;

		if (isClear)
		{
			//クリア時の演出
			float seDeleyTime = 0.5f;

			StartCoroutine(this.DelayMethod(seDeleyTime, () => { }));

			delayTime = 2.5f + seDeleyTime;
		}
		else
		{
			delayTime = 0.9f;
		}

		if (PlayerPrefs.GetInt(Define.CONTINUE_GAME, 0) == 1)
		{
			SceneNavigator.Instance.MoveScene(SceneName.GAME);
		}
		else
		{
			//演出が終わったらリザルトへ
			StartCoroutine(this.DelayMethod(delayTime, () =>
			{
				Time.timeScale = 1;
				AdManager.Instance.ShowSplashAd();
				SceneManager.UnloadSceneAsync(SceneNo.UIGAMEPLAY);
				SceneNavigator.Instance.AddScene(SceneName.UIENDING);
				// SceneNavigator.Instance.MoveScene (SceneName.RESULT);
			}));
		}
	}
}