﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// レビューボタンのアシスタントクラス
/// </summary>
public class AppStoreButtonAssistant : ButtonAssistant
{

	[SerializeField]
	private ShareLocation _location;

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{
		CommonNativeManager.Instance.ShowReviewPage ();
	}

}