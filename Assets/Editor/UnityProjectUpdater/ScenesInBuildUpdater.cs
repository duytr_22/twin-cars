﻿//  SceneSetter.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.22.

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// シーンを自動でビルドに設定するクラス
/// </summary>
public class ScenesInBuildUpdater : AssetPostprocessorEx
{

	private const string STAGE_KEY = "Stage";

	//returnの値が小さいほど優先される
	public override int GetPostprocessOrder ()
	{
		return -5;
	}

	//=================================================================================
	//検知
	//=================================================================================

	private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		//最初の起動時に実行しないように
		if (!UnityProjectSetting.IsLaunchedProject) {
			return;
		}

		List<string[]> assetsList = new List<string[]> () {
			importedAssets, deletedAssets, movedAssets, movedFromAssetPaths
		};

		List<string> targetDirectoryNameList = new List<string> () {
			DirectoryPath.TOP_RESOURCES + ResourcesDirectoryPath.DATA, 
			DirectoryPath.SCENES,
			DirectoryPath.SCENES + "/" + STAGE_KEY, 
		};

		if (ExistsDirectoryInAssets (assetsList, targetDirectoryNameList)) {
			UpdateScenesInBuild ();
		}

	}

	//=================================================================================
	//作成
	//=================================================================================

	[MenuItem ("Tools/Update/Reset Data")]
	private static void ResetData ()
	{
		PlayerPrefs.DeleteAll ();
		PlayerPrefs.Save ();
	}

	//スクリプトを作成します
	[MenuItem ("Tools/Update/Scenes In Build")]
	private static void UpdateScenesInBuild ()
	{

		//Sceneファイルを全て取得、名前とパスで辞書作成
		Dictionary<string, string> scenePathDict = new Dictionary<string, string> ();
		List<string> stageSceneList = new List<string> ();

		foreach (var guid in AssetDatabase.FindAssets("t:Scene")) {
			var path = AssetDatabase.GUIDToAssetPath (guid);
			string sceneName = System.IO.Path.GetFileNameWithoutExtension (path);

			//シーン名が被っている
			if (scenePathDict.ContainsKey (sceneName)) {
				Debug.LogError (sceneName + " scene is duplicated");
			} else {
				scenePathDict [sceneName] = path;

				//親ディレクトリがステージの場合はステージ一覧リストに追加
				if (System.IO.Path.GetFileName (System.IO.Path.GetDirectoryName (path)) == STAGE_KEY) {
					stageSceneList.Add (sceneName);
				}
			}
		}

		//シーンを順番に登録していく
		List<string> sceneNameList = new List<string> (){ SceneName.TITLE };
		GameType gameType = UnityProjectSetting.Entity.GameType;

		//ゲームとリザルトシーンは常に追加
		sceneNameList.Add (SceneName.GAME);
		sceneNameList.Add (SceneName.RESULT);
		sceneNameList.Add (SceneName.TUTORIAL);
		sceneNameList.Add (SceneName.BALL);

		//ステージ制の時はステージシーンも追加
		if (gameType == GameType.LevelSelect || gameType == GameType.ModeSelect) {
			sceneNameList.AddRange (stageSceneList);

			//ちゃんとステージがあるか確認
			string lastStageName = sceneNameList [sceneNameList.Count - 1];
			int lastStageNo = int.Parse (lastStageName.Substring (STAGE_KEY.Length, lastStageName.Length - STAGE_KEY.Length));

			for (int i = 1; i <= lastStageNo; i++) {
				string stageName = STAGE_KEY + i.ToString ();
				if (!sceneNameList.Contains (stageName)) {
					Debug.LogError ("Not found " + stageName);
				}
			}
		}

		//追加するシーンリスト作成、追加
		List<EditorBuildSettingsScene> sceneList = new List<EditorBuildSettingsScene> ();
		// foreach (string sceneName in sceneNameList) {
		// 	sceneList.Add (new EditorBuildSettingsScene (scenePathDict [sceneName], true));
		// }

		// EditorBuildSettings.scenes = sceneList.ToArray ();

		//ステージ数を登録
		UnityProjectSetting.Entity.StageNum = stageSceneList.Count;
	}

}