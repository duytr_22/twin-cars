﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// PlayerScript
struct PlayerScript_t1783516946;
// TutorialManager
struct TutorialManager_t3418541267;
// GamePlayScript
struct GamePlayScript_t1203922991;
// UIBackScript
struct UIBackScript_t2417306190;
// TweetButtonAssistant
struct TweetButtonAssistant_t1668838560;
// BackgroundScript
struct BackgroundScript_t3530941027;
// System.Action
struct Action_t1264377477;
// UIButton
struct UIButton_t1100396938;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// BackgroundScript/ColorBackChange
struct ColorBackChange_t4063191080;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t4027761066;
// System.Collections.Generic.List`1<ColorBalance>
struct List_1_t4075065786;
// PlayerScript/PlayerDie
struct PlayerDie_t274471295;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t3972987605;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// System.String
struct String_t;
// UnityEngine.Collider
struct Collider_t1773347010;
// UISprite
struct UISprite_t194114938;
// System.Comparison`1<UnityEngine.GameObject>
struct Comparison_1_t888567798;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t2370967660;
// TutorialPointScript/TriggerTutorial
struct TriggerTutorial_t2088781469;
// TweenPosition
struct TweenPosition_t1378762002;
// TweenAlpha
struct TweenAlpha_t3706845226;
// TweenScale
struct TweenScale_t2539309033;
// TweenColor
struct TweenColor_t2112002648;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// System.Collections.Generic.List`1<DynamicBack>
struct List_1_t2926401837;
// SpawnPointScript
struct SpawnPointScript_t711038611;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<SpeedBalance>
struct List_1_t2150885654;
// System.Collections.Generic.List`1<SpawnDistance>
struct List_1_t2900403471;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<TutorialSpawnPoints>
struct List_1_t2820345059;
// System.Collections.Generic.List`1<CubeScript>
struct List_1_t1859315636;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// RankingManager
struct RankingManager_t1599285360;
// SceneNavigator
struct SceneNavigator_t2956304001;
// GameManager
struct GameManager_t1536523654;
// AdManager
struct AdManager_t2410889370;
// SEManager
struct SEManager_t324035629;
// CommonNativeManager
struct CommonNativeManager_t3006429997;
// BGMManager
struct BGMManager_t1790042108;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// MainManager
struct MainManager_t729209856;
// ShareManager
struct ShareManager_t2456449165;
// System.Collections.Generic.Dictionary`2<CameraSizeAdjuster/AspectRateType,System.Single>
struct Dictionary_2_t1187321941;
// System.Func`2<System.String,System.IntPtr>
struct Func_2_t2939991702;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t1112413034;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip>
struct Dictionary_2_t3466145964;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// FadeTransition
struct FadeTransition_t2992252162;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// AdPlugin
struct AdPlugin_t1451289319;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// RankingPlugin
struct RankingPlugin_t2338328566;
// SharePlugin
struct SharePlugin_t456476949;
// CommonNativePlugin
struct CommonNativePlugin_t3889574804;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMOVEPLAYERU3EC__ITERATOR0_T1481740848_H
#define U3CMOVEPLAYERU3EC__ITERATOR0_T1481740848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<MovePlayer>c__Iterator0
struct  U3CMovePlayerU3Ec__Iterator0_t1481740848  : public RuntimeObject
{
public:
	// PlayerScript TutorialManager/<MovePlayer>c__Iterator0::script
	PlayerScript_t1783516946 * ___script_0;
	// System.Single TutorialManager/<MovePlayer>c__Iterator0::<t>__0
	float ___U3CtU3E__0_1;
	// System.Single TutorialManager/<MovePlayer>c__Iterator0::<duration>__0
	float ___U3CdurationU3E__0_2;
	// System.Int32 TutorialManager/<MovePlayer>c__Iterator0::dir
	int32_t ___dir_3;
	// System.Single TutorialManager/<MovePlayer>c__Iterator0::<posX>__1
	float ___U3CposXU3E__1_4;
	// System.Boolean TutorialManager/<MovePlayer>c__Iterator0::isState
	bool ___isState_5;
	// TutorialManager TutorialManager/<MovePlayer>c__Iterator0::$this
	TutorialManager_t3418541267 * ___U24this_6;
	// System.Object TutorialManager/<MovePlayer>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean TutorialManager/<MovePlayer>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 TutorialManager/<MovePlayer>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___script_0)); }
	inline PlayerScript_t1783516946 * get_script_0() const { return ___script_0; }
	inline PlayerScript_t1783516946 ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(PlayerScript_t1783516946 * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}

	inline static int32_t get_offset_of_U3CtU3E__0_1() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___U3CtU3E__0_1)); }
	inline float get_U3CtU3E__0_1() const { return ___U3CtU3E__0_1; }
	inline float* get_address_of_U3CtU3E__0_1() { return &___U3CtU3E__0_1; }
	inline void set_U3CtU3E__0_1(float value)
	{
		___U3CtU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CdurationU3E__0_2() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___U3CdurationU3E__0_2)); }
	inline float get_U3CdurationU3E__0_2() const { return ___U3CdurationU3E__0_2; }
	inline float* get_address_of_U3CdurationU3E__0_2() { return &___U3CdurationU3E__0_2; }
	inline void set_U3CdurationU3E__0_2(float value)
	{
		___U3CdurationU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_dir_3() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___dir_3)); }
	inline int32_t get_dir_3() const { return ___dir_3; }
	inline int32_t* get_address_of_dir_3() { return &___dir_3; }
	inline void set_dir_3(int32_t value)
	{
		___dir_3 = value;
	}

	inline static int32_t get_offset_of_U3CposXU3E__1_4() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___U3CposXU3E__1_4)); }
	inline float get_U3CposXU3E__1_4() const { return ___U3CposXU3E__1_4; }
	inline float* get_address_of_U3CposXU3E__1_4() { return &___U3CposXU3E__1_4; }
	inline void set_U3CposXU3E__1_4(float value)
	{
		___U3CposXU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_isState_5() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___isState_5)); }
	inline bool get_isState_5() const { return ___isState_5; }
	inline bool* get_address_of_isState_5() { return &___isState_5; }
	inline void set_isState_5(bool value)
	{
		___isState_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___U24this_6)); }
	inline TutorialManager_t3418541267 * get_U24this_6() const { return ___U24this_6; }
	inline TutorialManager_t3418541267 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(TutorialManager_t3418541267 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CMovePlayerU3Ec__Iterator0_t1481740848, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVEPLAYERU3EC__ITERATOR0_T1481740848_H
#ifndef U3CINSCREASINGSPEEDU3EC__ITERATOR0_T3231141296_H
#define U3CINSCREASINGSPEEDU3EC__ITERATOR0_T3231141296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlayScript/<InscreasingSpeed>c__Iterator0
struct  U3CInscreasingSpeedU3Ec__Iterator0_t3231141296  : public RuntimeObject
{
public:
	// System.Single GamePlayScript/<InscreasingSpeed>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single GamePlayScript/<InscreasingSpeed>c__Iterator0::<startSpeed>__0
	float ___U3CstartSpeedU3E__0_1;
	// System.Single GamePlayScript/<InscreasingSpeed>c__Iterator0::<speed>__0
	float ___U3CspeedU3E__0_2;
	// GamePlayScript GamePlayScript/<InscreasingSpeed>c__Iterator0::$this
	GamePlayScript_t1203922991 * ___U24this_3;
	// System.Object GamePlayScript/<InscreasingSpeed>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean GamePlayScript/<InscreasingSpeed>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 GamePlayScript/<InscreasingSpeed>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CInscreasingSpeedU3Ec__Iterator0_t3231141296, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartSpeedU3E__0_1() { return static_cast<int32_t>(offsetof(U3CInscreasingSpeedU3Ec__Iterator0_t3231141296, ___U3CstartSpeedU3E__0_1)); }
	inline float get_U3CstartSpeedU3E__0_1() const { return ___U3CstartSpeedU3E__0_1; }
	inline float* get_address_of_U3CstartSpeedU3E__0_1() { return &___U3CstartSpeedU3E__0_1; }
	inline void set_U3CstartSpeedU3E__0_1(float value)
	{
		___U3CstartSpeedU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CspeedU3E__0_2() { return static_cast<int32_t>(offsetof(U3CInscreasingSpeedU3Ec__Iterator0_t3231141296, ___U3CspeedU3E__0_2)); }
	inline float get_U3CspeedU3E__0_2() const { return ___U3CspeedU3E__0_2; }
	inline float* get_address_of_U3CspeedU3E__0_2() { return &___U3CspeedU3E__0_2; }
	inline void set_U3CspeedU3E__0_2(float value)
	{
		___U3CspeedU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CInscreasingSpeedU3Ec__Iterator0_t3231141296, ___U24this_3)); }
	inline GamePlayScript_t1203922991 * get_U24this_3() const { return ___U24this_3; }
	inline GamePlayScript_t1203922991 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(GamePlayScript_t1203922991 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CInscreasingSpeedU3Ec__Iterator0_t3231141296, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CInscreasingSpeedU3Ec__Iterator0_t3231141296, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CInscreasingSpeedU3Ec__Iterator0_t3231141296, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSCREASINGSPEEDU3EC__ITERATOR0_T3231141296_H
#ifndef U3CDECREASINGSPEEDTOZEROU3EC__ITERATOR1_T3656943898_H
#define U3CDECREASINGSPEEDTOZEROU3EC__ITERATOR1_T3656943898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlayScript/<DecreasingSpeedToZero>c__Iterator1
struct  U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898  : public RuntimeObject
{
public:
	// System.Single GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::duration
	float ___duration_0;
	// System.Single GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::<d>__0
	float ___U3CdU3E__0_1;
	// System.Single GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::<t>__0
	float ___U3CtU3E__0_2;
	// System.Single GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::<ratio>__0
	float ___U3CratioU3E__0_3;
	// System.Single GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::<startSpeed>__0
	float ___U3CstartSpeedU3E__0_4;
	// GamePlayScript GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::$this
	GamePlayScript_t1203922991 * ___U24this_5;
	// System.Object GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 GamePlayScript/<DecreasingSpeedToZero>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___duration_0)); }
	inline float get_duration_0() const { return ___duration_0; }
	inline float* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(float value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U3CdU3E__0_1)); }
	inline float get_U3CdU3E__0_1() const { return ___U3CdU3E__0_1; }
	inline float* get_address_of_U3CdU3E__0_1() { return &___U3CdU3E__0_1; }
	inline void set_U3CdU3E__0_1(float value)
	{
		___U3CdU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U3CtU3E__0_2)); }
	inline float get_U3CtU3E__0_2() const { return ___U3CtU3E__0_2; }
	inline float* get_address_of_U3CtU3E__0_2() { return &___U3CtU3E__0_2; }
	inline void set_U3CtU3E__0_2(float value)
	{
		___U3CtU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U3CratioU3E__0_3)); }
	inline float get_U3CratioU3E__0_3() const { return ___U3CratioU3E__0_3; }
	inline float* get_address_of_U3CratioU3E__0_3() { return &___U3CratioU3E__0_3; }
	inline void set_U3CratioU3E__0_3(float value)
	{
		___U3CratioU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CstartSpeedU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U3CstartSpeedU3E__0_4)); }
	inline float get_U3CstartSpeedU3E__0_4() const { return ___U3CstartSpeedU3E__0_4; }
	inline float* get_address_of_U3CstartSpeedU3E__0_4() { return &___U3CstartSpeedU3E__0_4; }
	inline void set_U3CstartSpeedU3E__0_4(float value)
	{
		___U3CstartSpeedU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U24this_5)); }
	inline GamePlayScript_t1203922991 * get_U24this_5() const { return ___U24this_5; }
	inline GamePlayScript_t1203922991 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(GamePlayScript_t1203922991 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDECREASINGSPEEDTOZEROU3EC__ITERATOR1_T3656943898_H
#ifndef U3CWAITTOSTOPMOVEBACKGROUNDU3EC__ITERATOR2_T2772810352_H
#define U3CWAITTOSTOPMOVEBACKGROUNDU3EC__ITERATOR2_T2772810352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlayScript/<WaitToStopMoveBackground>c__Iterator2
struct  U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352  : public RuntimeObject
{
public:
	// System.Boolean GamePlayScript/<WaitToStopMoveBackground>c__Iterator2::<canContinue>__0
	bool ___U3CcanContinueU3E__0_0;
	// System.Single GamePlayScript/<WaitToStopMoveBackground>c__Iterator2::duration
	float ___duration_1;
	// UIBackScript GamePlayScript/<WaitToStopMoveBackground>c__Iterator2::<uibs>__0
	UIBackScript_t2417306190 * ___U3CuibsU3E__0_2;
	// System.Object GamePlayScript/<WaitToStopMoveBackground>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GamePlayScript/<WaitToStopMoveBackground>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 GamePlayScript/<WaitToStopMoveBackground>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CcanContinueU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352, ___U3CcanContinueU3E__0_0)); }
	inline bool get_U3CcanContinueU3E__0_0() const { return ___U3CcanContinueU3E__0_0; }
	inline bool* get_address_of_U3CcanContinueU3E__0_0() { return &___U3CcanContinueU3E__0_0; }
	inline void set_U3CcanContinueU3E__0_0(bool value)
	{
		___U3CcanContinueU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_U3CuibsU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352, ___U3CuibsU3E__0_2)); }
	inline UIBackScript_t2417306190 * get_U3CuibsU3E__0_2() const { return ___U3CuibsU3E__0_2; }
	inline UIBackScript_t2417306190 ** get_address_of_U3CuibsU3E__0_2() { return &___U3CuibsU3E__0_2; }
	inline void set_U3CuibsU3E__0_2(UIBackScript_t2417306190 * value)
	{
		___U3CuibsU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuibsU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITTOSTOPMOVEBACKGROUNDU3EC__ITERATOR2_T2772810352_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1337170120_H
#define U3CSTARTU3EC__ITERATOR0_T1337170120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweetButtonAssistant/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1337170120  : public RuntimeObject
{
public:
	// TweetButtonAssistant TweetButtonAssistant/<Start>c__Iterator0::$this
	TweetButtonAssistant_t1668838560 * ___U24this_0;
	// System.Object TweetButtonAssistant/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TweetButtonAssistant/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TweetButtonAssistant/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1337170120, ___U24this_0)); }
	inline TweetButtonAssistant_t1668838560 * get_U24this_0() const { return ___U24this_0; }
	inline TweetButtonAssistant_t1668838560 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TweetButtonAssistant_t1668838560 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1337170120, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1337170120, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1337170120, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1337170120_H
#ifndef U3CCHANGECLOUDCOLORU3EC__ITERATOR2_T2544988547_H
#define U3CCHANGECLOUDCOLORU3EC__ITERATOR2_T2544988547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScript/<ChangeCloudColor>c__Iterator2
struct  U3CChangeCloudColorU3Ec__Iterator2_t2544988547  : public RuntimeObject
{
public:
	// System.Single BackgroundScript/<ChangeCloudColor>c__Iterator2::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single BackgroundScript/<ChangeCloudColor>c__Iterator2::<ratio>__1
	float ___U3CratioU3E__1_1;
	// BackgroundScript BackgroundScript/<ChangeCloudColor>c__Iterator2::$this
	BackgroundScript_t3530941027 * ___U24this_2;
	// System.Object BackgroundScript/<ChangeCloudColor>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BackgroundScript/<ChangeCloudColor>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 BackgroundScript/<ChangeCloudColor>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeCloudColorU3Ec__Iterator2_t2544988547, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__1_1() { return static_cast<int32_t>(offsetof(U3CChangeCloudColorU3Ec__Iterator2_t2544988547, ___U3CratioU3E__1_1)); }
	inline float get_U3CratioU3E__1_1() const { return ___U3CratioU3E__1_1; }
	inline float* get_address_of_U3CratioU3E__1_1() { return &___U3CratioU3E__1_1; }
	inline void set_U3CratioU3E__1_1(float value)
	{
		___U3CratioU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CChangeCloudColorU3Ec__Iterator2_t2544988547, ___U24this_2)); }
	inline BackgroundScript_t3530941027 * get_U24this_2() const { return ___U24this_2; }
	inline BackgroundScript_t3530941027 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BackgroundScript_t3530941027 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CChangeCloudColorU3Ec__Iterator2_t2544988547, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CChangeCloudColorU3Ec__Iterator2_t2544988547, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CChangeCloudColorU3Ec__Iterator2_t2544988547, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGECLOUDCOLORU3EC__ITERATOR2_T2544988547_H
#ifndef U3CCHANGEFOGCOLORU3EC__ITERATOR3_T753132848_H
#define U3CCHANGEFOGCOLORU3EC__ITERATOR3_T753132848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScript/<ChangeFogColor>c__Iterator3
struct  U3CChangeFogColorU3Ec__Iterator3_t753132848  : public RuntimeObject
{
public:
	// System.Single BackgroundScript/<ChangeFogColor>c__Iterator3::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single BackgroundScript/<ChangeFogColor>c__Iterator3::<ratio>__1
	float ___U3CratioU3E__1_1;
	// BackgroundScript BackgroundScript/<ChangeFogColor>c__Iterator3::$this
	BackgroundScript_t3530941027 * ___U24this_2;
	// System.Object BackgroundScript/<ChangeFogColor>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BackgroundScript/<ChangeFogColor>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 BackgroundScript/<ChangeFogColor>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeFogColorU3Ec__Iterator3_t753132848, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__1_1() { return static_cast<int32_t>(offsetof(U3CChangeFogColorU3Ec__Iterator3_t753132848, ___U3CratioU3E__1_1)); }
	inline float get_U3CratioU3E__1_1() const { return ___U3CratioU3E__1_1; }
	inline float* get_address_of_U3CratioU3E__1_1() { return &___U3CratioU3E__1_1; }
	inline void set_U3CratioU3E__1_1(float value)
	{
		___U3CratioU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CChangeFogColorU3Ec__Iterator3_t753132848, ___U24this_2)); }
	inline BackgroundScript_t3530941027 * get_U24this_2() const { return ___U24this_2; }
	inline BackgroundScript_t3530941027 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BackgroundScript_t3530941027 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CChangeFogColorU3Ec__Iterator3_t753132848, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CChangeFogColorU3Ec__Iterator3_t753132848, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CChangeFogColorU3Ec__Iterator3_t753132848, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEFOGCOLORU3EC__ITERATOR3_T753132848_H
#ifndef U3CDELAYTIMEU3EC__ITERATOR1_T3935045367_H
#define U3CDELAYTIMEU3EC__ITERATOR1_T3935045367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<DelayTime>c__Iterator1
struct  U3CDelayTimeU3Ec__Iterator1_t3935045367  : public RuntimeObject
{
public:
	// System.Single TutorialManager/<DelayTime>c__Iterator1::<countDelayTime>__0
	float ___U3CcountDelayTimeU3E__0_0;
	// System.Single TutorialManager/<DelayTime>c__Iterator1::maxTime
	float ___maxTime_1;
	// System.Boolean TutorialManager/<DelayTime>c__Iterator1::isState
	bool ___isState_2;
	// TutorialManager TutorialManager/<DelayTime>c__Iterator1::$this
	TutorialManager_t3418541267 * ___U24this_3;
	// System.Object TutorialManager/<DelayTime>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TutorialManager/<DelayTime>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 TutorialManager/<DelayTime>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcountDelayTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ec__Iterator1_t3935045367, ___U3CcountDelayTimeU3E__0_0)); }
	inline float get_U3CcountDelayTimeU3E__0_0() const { return ___U3CcountDelayTimeU3E__0_0; }
	inline float* get_address_of_U3CcountDelayTimeU3E__0_0() { return &___U3CcountDelayTimeU3E__0_0; }
	inline void set_U3CcountDelayTimeU3E__0_0(float value)
	{
		___U3CcountDelayTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_maxTime_1() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ec__Iterator1_t3935045367, ___maxTime_1)); }
	inline float get_maxTime_1() const { return ___maxTime_1; }
	inline float* get_address_of_maxTime_1() { return &___maxTime_1; }
	inline void set_maxTime_1(float value)
	{
		___maxTime_1 = value;
	}

	inline static int32_t get_offset_of_isState_2() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ec__Iterator1_t3935045367, ___isState_2)); }
	inline bool get_isState_2() const { return ___isState_2; }
	inline bool* get_address_of_isState_2() { return &___isState_2; }
	inline void set_isState_2(bool value)
	{
		___isState_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ec__Iterator1_t3935045367, ___U24this_3)); }
	inline TutorialManager_t3418541267 * get_U24this_3() const { return ___U24this_3; }
	inline TutorialManager_t3418541267 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TutorialManager_t3418541267 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ec__Iterator1_t3935045367, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ec__Iterator1_t3935045367, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ec__Iterator1_t3935045367, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYTIMEU3EC__ITERATOR1_T3935045367_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRANSFORMEXTENSION_T4238110091_H
#define TRANSFORMEXTENSION_T4238110091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformExtension
struct  TransformExtension_t4238110091  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSION_T4238110091_H
#ifndef STRINGEXTENSIONS_T731559555_H
#define STRINGEXTENSIONS_T731559555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StringExtensions
struct  StringExtensions_t731559555  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGEXTENSIONS_T731559555_H
#ifndef U3CCHANGECAMERACOLORU3EC__ITERATOR4_T3541598473_H
#define U3CCHANGECAMERACOLORU3EC__ITERATOR4_T3541598473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScript/<ChangeCameraColor>c__Iterator4
struct  U3CChangeCameraColorU3Ec__Iterator4_t3541598473  : public RuntimeObject
{
public:
	// System.Single BackgroundScript/<ChangeCameraColor>c__Iterator4::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single BackgroundScript/<ChangeCameraColor>c__Iterator4::<ratio>__1
	float ___U3CratioU3E__1_1;
	// BackgroundScript BackgroundScript/<ChangeCameraColor>c__Iterator4::$this
	BackgroundScript_t3530941027 * ___U24this_2;
	// System.Object BackgroundScript/<ChangeCameraColor>c__Iterator4::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BackgroundScript/<ChangeCameraColor>c__Iterator4::$disposing
	bool ___U24disposing_4;
	// System.Int32 BackgroundScript/<ChangeCameraColor>c__Iterator4::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeCameraColorU3Ec__Iterator4_t3541598473, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__1_1() { return static_cast<int32_t>(offsetof(U3CChangeCameraColorU3Ec__Iterator4_t3541598473, ___U3CratioU3E__1_1)); }
	inline float get_U3CratioU3E__1_1() const { return ___U3CratioU3E__1_1; }
	inline float* get_address_of_U3CratioU3E__1_1() { return &___U3CratioU3E__1_1; }
	inline void set_U3CratioU3E__1_1(float value)
	{
		___U3CratioU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CChangeCameraColorU3Ec__Iterator4_t3541598473, ___U24this_2)); }
	inline BackgroundScript_t3530941027 * get_U24this_2() const { return ___U24this_2; }
	inline BackgroundScript_t3530941027 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BackgroundScript_t3530941027 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CChangeCameraColorU3Ec__Iterator4_t3541598473, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CChangeCameraColorU3Ec__Iterator4_t3541598473, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CChangeCameraColorU3Ec__Iterator4_t3541598473, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGECAMERACOLORU3EC__ITERATOR4_T3541598473_H
#ifndef U3CCHANGEBACKCOLORU3EC__ITERATOR0_T1298991505_H
#define U3CCHANGEBACKCOLORU3EC__ITERATOR0_T1298991505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScript/<ChangeBackColor>c__Iterator0
struct  U3CChangeBackColorU3Ec__Iterator0_t1298991505  : public RuntimeObject
{
public:
	// System.Single BackgroundScript/<ChangeBackColor>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single BackgroundScript/<ChangeBackColor>c__Iterator0::<ratio>__1
	float ___U3CratioU3E__1_1;
	// BackgroundScript BackgroundScript/<ChangeBackColor>c__Iterator0::$this
	BackgroundScript_t3530941027 * ___U24this_2;
	// System.Object BackgroundScript/<ChangeBackColor>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BackgroundScript/<ChangeBackColor>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 BackgroundScript/<ChangeBackColor>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeBackColorU3Ec__Iterator0_t1298991505, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__1_1() { return static_cast<int32_t>(offsetof(U3CChangeBackColorU3Ec__Iterator0_t1298991505, ___U3CratioU3E__1_1)); }
	inline float get_U3CratioU3E__1_1() const { return ___U3CratioU3E__1_1; }
	inline float* get_address_of_U3CratioU3E__1_1() { return &___U3CratioU3E__1_1; }
	inline void set_U3CratioU3E__1_1(float value)
	{
		___U3CratioU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CChangeBackColorU3Ec__Iterator0_t1298991505, ___U24this_2)); }
	inline BackgroundScript_t3530941027 * get_U24this_2() const { return ___U24this_2; }
	inline BackgroundScript_t3530941027 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BackgroundScript_t3530941027 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CChangeBackColorU3Ec__Iterator0_t1298991505, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CChangeBackColorU3Ec__Iterator0_t1298991505, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CChangeBackColorU3Ec__Iterator0_t1298991505, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEBACKCOLORU3EC__ITERATOR0_T1298991505_H
#ifndef U3CCHANGEHAZECOLORU3EC__ITERATOR1_T3094754686_H
#define U3CCHANGEHAZECOLORU3EC__ITERATOR1_T3094754686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScript/<ChangeHazeColor>c__Iterator1
struct  U3CChangeHazeColorU3Ec__Iterator1_t3094754686  : public RuntimeObject
{
public:
	// System.Single BackgroundScript/<ChangeHazeColor>c__Iterator1::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single BackgroundScript/<ChangeHazeColor>c__Iterator1::<ratio>__1
	float ___U3CratioU3E__1_1;
	// BackgroundScript BackgroundScript/<ChangeHazeColor>c__Iterator1::$this
	BackgroundScript_t3530941027 * ___U24this_2;
	// System.Object BackgroundScript/<ChangeHazeColor>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BackgroundScript/<ChangeHazeColor>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 BackgroundScript/<ChangeHazeColor>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeHazeColorU3Ec__Iterator1_t3094754686, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__1_1() { return static_cast<int32_t>(offsetof(U3CChangeHazeColorU3Ec__Iterator1_t3094754686, ___U3CratioU3E__1_1)); }
	inline float get_U3CratioU3E__1_1() const { return ___U3CratioU3E__1_1; }
	inline float* get_address_of_U3CratioU3E__1_1() { return &___U3CratioU3E__1_1; }
	inline void set_U3CratioU3E__1_1(float value)
	{
		___U3CratioU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CChangeHazeColorU3Ec__Iterator1_t3094754686, ___U24this_2)); }
	inline BackgroundScript_t3530941027 * get_U24this_2() const { return ___U24this_2; }
	inline BackgroundScript_t3530941027 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BackgroundScript_t3530941027 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CChangeHazeColorU3Ec__Iterator1_t3094754686, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CChangeHazeColorU3Ec__Iterator1_t3094754686, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CChangeHazeColorU3Ec__Iterator1_t3094754686, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEHAZECOLORU3EC__ITERATOR1_T3094754686_H
#ifndef U3CCAMERASHAKEU3EC__ITERATOR3_T38765107_H
#define U3CCAMERASHAKEU3EC__ITERATOR3_T38765107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlayScript/<CameraShake>c__Iterator3
struct  U3CCameraShakeU3Ec__Iterator3_t38765107  : public RuntimeObject
{
public:
	// System.Single GamePlayScript/<CameraShake>c__Iterator3::<t>__0
	float ___U3CtU3E__0_0;
	// GamePlayScript GamePlayScript/<CameraShake>c__Iterator3::$this
	GamePlayScript_t1203922991 * ___U24this_1;
	// System.Object GamePlayScript/<CameraShake>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GamePlayScript/<CameraShake>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 GamePlayScript/<CameraShake>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCameraShakeU3Ec__Iterator3_t38765107, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCameraShakeU3Ec__Iterator3_t38765107, ___U24this_1)); }
	inline GamePlayScript_t1203922991 * get_U24this_1() const { return ___U24this_1; }
	inline GamePlayScript_t1203922991 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GamePlayScript_t1203922991 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCameraShakeU3Ec__Iterator3_t38765107, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCameraShakeU3Ec__Iterator3_t38765107, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCameraShakeU3Ec__Iterator3_t38765107, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCAMERASHAKEU3EC__ITERATOR3_T38765107_H
#ifndef PHYSICSEXTENTSION_T553451405_H
#define PHYSICSEXTENTSION_T553451405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhysicsExtentsion
struct  PhysicsExtentsion_t553451405  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSEXTENTSION_T553451405_H
#ifndef U3CDELAYMETHODU3EC__ITERATOR3_T3849733590_H
#define U3CDELAYMETHODU3EC__ITERATOR3_T3849733590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoBehaviorExtentsion/<DelayMethod>c__Iterator3
struct  U3CDelayMethodU3Ec__Iterator3_t3849733590  : public RuntimeObject
{
public:
	// System.Single MonoBehaviorExtentsion/<DelayMethod>c__Iterator3::waitTime
	float ___waitTime_0;
	// System.Action MonoBehaviorExtentsion/<DelayMethod>c__Iterator3::action
	Action_t1264377477 * ___action_1;
	// System.Object MonoBehaviorExtentsion/<DelayMethod>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MonoBehaviorExtentsion/<DelayMethod>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 MonoBehaviorExtentsion/<DelayMethod>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(U3CDelayMethodU3Ec__Iterator3_t3849733590, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CDelayMethodU3Ec__Iterator3_t3849733590, ___action_1)); }
	inline Action_t1264377477 * get_action_1() const { return ___action_1; }
	inline Action_t1264377477 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t1264377477 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayMethodU3Ec__Iterator3_t3849733590, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayMethodU3Ec__Iterator3_t3849733590, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayMethodU3Ec__Iterator3_t3849733590, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYMETHODU3EC__ITERATOR3_T3849733590_H
#ifndef MONOBEHAVIOREXTENTSION_T1290638044_H
#define MONOBEHAVIOREXTENTSION_T1290638044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoBehaviorExtentsion
struct  MonoBehaviorExtentsion_t1290638044  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOREXTENTSION_T1290638044_H
#ifndef GAMEOBJECTEXTENSION_T1934678351_H
#define GAMEOBJECTEXTENSION_T1934678351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectExtension
struct  GameObjectExtension_t1934678351  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSION_T1934678351_H
#ifndef ENUMEXTENSIONS_T4248483755_H
#define ENUMEXTENSIONS_T4248483755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumExtensions
struct  EnumExtensions_t4248483755  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMEXTENSIONS_T4248483755_H
#ifndef DICTIONARYEXTENSIONS_T457327208_H
#define DICTIONARYEXTENSIONS_T457327208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DictionaryExtensions
struct  DictionaryExtensions_t457327208  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYEXTENSIONS_T457327208_H
#ifndef U3CDELAYPAUSEBUTTONU3EC__ITERATOR0_T3539102654_H
#define U3CDELAYPAUSEBUTTONU3EC__ITERATOR0_T3539102654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdManager/<DelayPauseButton>c__Iterator0
struct  U3CDelayPauseButtonU3Ec__Iterator0_t3539102654  : public RuntimeObject
{
public:
	// System.Object AdManager/<DelayPauseButton>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean AdManager/<DelayPauseButton>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 AdManager/<DelayPauseButton>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CDelayPauseButtonU3Ec__Iterator0_t3539102654, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CDelayPauseButtonU3Ec__Iterator0_t3539102654, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CDelayPauseButtonU3Ec__Iterator0_t3539102654, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYPAUSEBUTTONU3EC__ITERATOR0_T3539102654_H
#ifndef DEBUG_T3683223393_H
#define DEBUG_T3683223393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Debug
struct  Debug_t3683223393  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_T3683223393_H
#ifndef U3CSHOWCONTINUEU3EC__ANONSTOREY0_T388195091_H
#define U3CSHOWCONTINUEU3EC__ANONSTOREY0_T388195091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<ShowContinue>c__AnonStorey0
struct  U3CShowContinueU3Ec__AnonStorey0_t388195091  : public RuntimeObject
{
public:
	// UIButton GameManager/<ShowContinue>c__AnonStorey0::button
	UIButton_t1100396938 * ___button_0;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(U3CShowContinueU3Ec__AnonStorey0_t388195091, ___button_0)); }
	inline UIButton_t1100396938 * get_button_0() const { return ___button_0; }
	inline UIButton_t1100396938 ** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(UIButton_t1100396938 * value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier((&___button_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWCONTINUEU3EC__ANONSTOREY0_T388195091_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef RIGIDBODY2DEXTENSION_T2198349777_H
#define RIGIDBODY2DEXTENSION_T2198349777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rigidbody2DExtension
struct  Rigidbody2DExtension_t2198349777  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2DEXTENSION_T2198349777_H
#ifndef SPEEDBALANCE_T678810912_H
#define SPEEDBALANCE_T678810912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedBalance
struct  SpeedBalance_t678810912 
{
public:
	// System.Int32 SpeedBalance::score
	int32_t ___score_0;
	// System.Single SpeedBalance::speed
	float ___speed_1;
	// System.Single SpeedBalance::duration
	float ___duration_2;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(SpeedBalance_t678810912, ___score_0)); }
	inline int32_t get_score_0() const { return ___score_0; }
	inline int32_t* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(int32_t value)
	{
		___score_0 = value;
	}

	inline static int32_t get_offset_of_speed_1() { return static_cast<int32_t>(offsetof(SpeedBalance_t678810912, ___speed_1)); }
	inline float get_speed_1() const { return ___speed_1; }
	inline float* get_address_of_speed_1() { return &___speed_1; }
	inline void set_speed_1(float value)
	{
		___speed_1 = value;
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(SpeedBalance_t678810912, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDBALANCE_T678810912_H
#ifndef SPAWNDISTANCE_T1428328729_H
#define SPAWNDISTANCE_T1428328729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpawnDistance
struct  SpawnDistance_t1428328729 
{
public:
	// System.Int32 SpawnDistance::score
	int32_t ___score_0;
	// System.Single SpawnDistance::min
	float ___min_1;
	// System.Single SpawnDistance::max
	float ___max_2;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(SpawnDistance_t1428328729, ___score_0)); }
	inline int32_t get_score_0() const { return ___score_0; }
	inline int32_t* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(int32_t value)
	{
		___score_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(SpawnDistance_t1428328729, ___min_1)); }
	inline float get_min_1() const { return ___min_1; }
	inline float* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(float value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(SpawnDistance_t1428328729, ___max_2)); }
	inline float get_max_2() const { return ___max_2; }
	inline float* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(float value)
	{
		___max_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNDISTANCE_T1428328729_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef TUTORIALSPAWNPOINTS_T1348270317_H
#define TUTORIALSPAWNPOINTS_T1348270317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialSpawnPoints
struct  TutorialSpawnPoints_t1348270317 
{
public:
	// System.Int32 TutorialSpawnPoints::LeftPoint
	int32_t ___LeftPoint_0;
	// System.Int32 TutorialSpawnPoints::RightPoint
	int32_t ___RightPoint_1;

public:
	inline static int32_t get_offset_of_LeftPoint_0() { return static_cast<int32_t>(offsetof(TutorialSpawnPoints_t1348270317, ___LeftPoint_0)); }
	inline int32_t get_LeftPoint_0() const { return ___LeftPoint_0; }
	inline int32_t* get_address_of_LeftPoint_0() { return &___LeftPoint_0; }
	inline void set_LeftPoint_0(int32_t value)
	{
		___LeftPoint_0 = value;
	}

	inline static int32_t get_offset_of_RightPoint_1() { return static_cast<int32_t>(offsetof(TutorialSpawnPoints_t1348270317, ___RightPoint_1)); }
	inline int32_t get_RightPoint_1() const { return ___RightPoint_1; }
	inline int32_t* get_address_of_RightPoint_1() { return &___RightPoint_1; }
	inline void set_RightPoint_1(int32_t value)
	{
		___RightPoint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALSPAWNPOINTS_T1348270317_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLORBALANCE_T2602991044_H
#define COLORBALANCE_T2602991044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorBalance
struct  ColorBalance_t2602991044 
{
public:
	// System.Int32 ColorBalance::score
	int32_t ___score_0;
	// System.Int32 ColorBalance::colorIndex
	int32_t ___colorIndex_1;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(ColorBalance_t2602991044, ___score_0)); }
	inline int32_t get_score_0() const { return ___score_0; }
	inline int32_t* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(int32_t value)
	{
		___score_0 = value;
	}

	inline static int32_t get_offset_of_colorIndex_1() { return static_cast<int32_t>(offsetof(ColorBalance_t2602991044, ___colorIndex_1)); }
	inline int32_t get_colorIndex_1() const { return ___colorIndex_1; }
	inline int32_t* get_address_of_colorIndex_1() { return &___colorIndex_1; }
	inline void set_colorIndex_1(int32_t value)
	{
		___colorIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBALANCE_T2602991044_H
#ifndef TUTORIALSTATE_T1806054907_H
#define TUTORIALSTATE_T1806054907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/TutorialState
struct  TutorialState_t1806054907 
{
public:
	// System.Int32 TutorialManager/TutorialState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TutorialState_t1806054907, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALSTATE_T1806054907_H
#ifndef BANNERTYPE_T3341813445_H
#define BANNERTYPE_T3341813445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdRegionViewer/BannerType
struct  BannerType_t3341813445 
{
public:
	// System.Int32 AdRegionViewer/BannerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BannerType_t3341813445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERTYPE_T3341813445_H
#ifndef SHARELOCATION_T3603649529_H
#define SHARELOCATION_T3603649529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShareLocation
struct  ShareLocation_t3603649529 
{
public:
	// System.Int32 ShareLocation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShareLocation_t3603649529, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARELOCATION_T3603649529_H
#ifndef ENUMFLAGSATTRIBUTE_T3331586256_H
#define ENUMFLAGSATTRIBUTE_T3331586256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumFlagsAttribute
struct  EnumFlagsAttribute_t3331586256  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMFLAGSATTRIBUTE_T3331586256_H
#ifndef ASPECTRATETYPE_T2035530557_H
#define ASPECTRATETYPE_T2035530557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSizeAdjuster/AspectRateType
struct  AspectRateType_t2035530557 
{
public:
	// System.Int32 CameraSizeAdjuster/AspectRateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectRateType_t2035530557, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATETYPE_T2035530557_H
#ifndef ASPECTRATETYPE_T2201292060_H
#define ASPECTRATETYPE_T2201292060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdjuster/AspectRateType
struct  AspectRateType_t2201292060 
{
public:
	// System.Int32 UIAdjuster/AspectRateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectRateType_t2201292060, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATETYPE_T2201292060_H
#ifndef PLAYERSTATE_T1870377573_H
#define PLAYERSTATE_T1870377573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerScript/PlayerState
struct  PlayerState_t1870377573 
{
public:
	// System.Int32 PlayerScript/PlayerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayerState_t1870377573, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATE_T1870377573_H
#ifndef FOGMODE_T1277989386_H
#define FOGMODE_T1277989386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FogMode
struct  FogMode_t1277989386 
{
public:
	// System.Int32 UnityEngine.FogMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FogMode_t1277989386, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGMODE_T1277989386_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef PLAYMODE_T2810632737_H
#define PLAYMODE_T2810632737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/PlayMode
struct  PlayMode_t2810632737 
{
public:
	// System.Int32 GameManager/PlayMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayMode_t2810632737, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODE_T2810632737_H
#ifndef PLAYERTYPE_T1512665253_H
#define PLAYERTYPE_T1512665253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerScript/PlayerType
struct  PlayerType_t1512665253 
{
public:
	// System.Int32 PlayerScript/PlayerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayerType_t1512665253, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERTYPE_T1512665253_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RIGIDBODYEXTENSION_T1329912879_H
#define RIGIDBODYEXTENSION_T1329912879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RigidbodyExtension
struct  RigidbodyExtension_t1329912879  : public RuntimeObject
{
public:

public:
};

struct RigidbodyExtension_t1329912879_StaticFields
{
public:
	// UnityEngine.Vector3 RigidbodyExtension::_angularVelocity
	Vector3_t3722313464  ____angularVelocity_0;
	// UnityEngine.Vector3 RigidbodyExtension::_velocity
	Vector3_t3722313464  ____velocity_1;

public:
	inline static int32_t get_offset_of__angularVelocity_0() { return static_cast<int32_t>(offsetof(RigidbodyExtension_t1329912879_StaticFields, ____angularVelocity_0)); }
	inline Vector3_t3722313464  get__angularVelocity_0() const { return ____angularVelocity_0; }
	inline Vector3_t3722313464 * get_address_of__angularVelocity_0() { return &____angularVelocity_0; }
	inline void set__angularVelocity_0(Vector3_t3722313464  value)
	{
		____angularVelocity_0 = value;
	}

	inline static int32_t get_offset_of__velocity_1() { return static_cast<int32_t>(offsetof(RigidbodyExtension_t1329912879_StaticFields, ____velocity_1)); }
	inline Vector3_t3722313464  get__velocity_1() const { return ____velocity_1; }
	inline Vector3_t3722313464 * get_address_of__velocity_1() { return &____velocity_1; }
	inline void set__velocity_1(Vector3_t3722313464  value)
	{
		____velocity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYEXTENSION_T1329912879_H
#ifndef RECTANGLETYPE_T3748261860_H
#define RECTANGLETYPE_T3748261860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdRegionViewer/RectangleType
struct  RectangleType_t3748261860 
{
public:
	// System.Int32 AdRegionViewer/RectangleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RectangleType_t3748261860, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLETYPE_T3748261860_H
#ifndef PIVOTTYPE_T1871032332_H
#define PIVOTTYPE_T1871032332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GridDeployer/PivotType
struct  PivotType_t1871032332 
{
public:
	// System.Int32 GridDeployer/PivotType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PivotType_t1871032332, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOTTYPE_T1871032332_H
#ifndef ICONTYPE_T1006196312_H
#define ICONTYPE_T1006196312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdRegionViewer/IconType
struct  IconType_t1006196312 
{
public:
	// System.Int32 AdRegionViewer/IconType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IconType_t1006196312, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONTYPE_T1006196312_H
#ifndef DIMENSION_T1087553712_H
#define DIMENSION_T1087553712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GridDeployer/Dimension
struct  Dimension_t1087553712 
{
public:
	// System.Int32 GridDeployer/Dimension::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Dimension_t1087553712, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIMENSION_T1087553712_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COLORBACKCHANGE_T4063191080_H
#define COLORBACKCHANGE_T4063191080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScript/ColorBackChange
struct  ColorBackChange_t4063191080  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBACKCHANGE_T4063191080_H
#ifndef TRIGGERTUTORIAL_T2088781469_H
#define TRIGGERTUTORIAL_T2088781469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPointScript/TriggerTutorial
struct  TriggerTutorial_t2088781469  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTUTORIAL_T2088781469_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef PLAYERDIE_T274471295_H
#define PLAYERDIE_T274471295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerScript/PlayerDie
struct  PlayerDie_t274471295  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDIE_T274471295_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SAMPLECUBE_T1605149290_H
#define SAMPLECUBE_T1605149290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleCube
struct  SampleCube_t1605149290  : public MonoBehaviour_t3962482529
{
public:
	// System.Action SampleCube::OnClick
	Action_t1264377477 * ___OnClick_2;

public:
	inline static int32_t get_offset_of_OnClick_2() { return static_cast<int32_t>(offsetof(SampleCube_t1605149290, ___OnClick_2)); }
	inline Action_t1264377477 * get_OnClick_2() const { return ___OnClick_2; }
	inline Action_t1264377477 ** get_address_of_OnClick_2() { return &___OnClick_2; }
	inline void set_OnClick_2(Action_t1264377477 * value)
	{
		___OnClick_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnClick_2), value);
	}
};

struct SampleCube_t1605149290_StaticFields
{
public:
	// System.Action SampleCube::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(SampleCube_t1605149290_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECUBE_T1605149290_H
#ifndef TOUCHEVENTSAMPLE_T2102076809_H
#define TOUCHEVENTSAMPLE_T2102076809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchEventSample
struct  TouchEventSample_t2102076809  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TouchEventSample::_UpdateSelected
	bool ____UpdateSelected_2;

public:
	inline static int32_t get_offset_of__UpdateSelected_2() { return static_cast<int32_t>(offsetof(TouchEventSample_t2102076809, ____UpdateSelected_2)); }
	inline bool get__UpdateSelected_2() const { return ____UpdateSelected_2; }
	inline bool* get_address_of__UpdateSelected_2() { return &____UpdateSelected_2; }
	inline void set__UpdateSelected_2(bool value)
	{
		____UpdateSelected_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENTSAMPLE_T2102076809_H
#ifndef UIADJUSTER_T2553577866_H
#define UIADJUSTER_T2553577866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdjuster
struct  UIAdjuster_t2553577866  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UIAdjuster::_currentAspectRate
	float ____currentAspectRate_2;

public:
	inline static int32_t get_offset_of__currentAspectRate_2() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866, ____currentAspectRate_2)); }
	inline float get__currentAspectRate_2() const { return ____currentAspectRate_2; }
	inline float* get_address_of__currentAspectRate_2() { return &____currentAspectRate_2; }
	inline void set__currentAspectRate_2(float value)
	{
		____currentAspectRate_2 = value;
	}
};

struct UIAdjuster_t2553577866_StaticFields
{
public:
	// UnityEngine.Vector2 UIAdjuster::SCREEN_SIZE_DEFULT
	Vector2_t2156229523  ___SCREEN_SIZE_DEFULT_5;
	// UnityEngine.Vector2 UIAdjuster::SCREEN_SIZE_640_960
	Vector2_t2156229523  ___SCREEN_SIZE_640_960_6;
	// UnityEngine.Vector2 UIAdjuster::SCREEN_SIZE_640_1136
	Vector2_t2156229523  ___SCREEN_SIZE_640_1136_7;

public:
	inline static int32_t get_offset_of_SCREEN_SIZE_DEFULT_5() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866_StaticFields, ___SCREEN_SIZE_DEFULT_5)); }
	inline Vector2_t2156229523  get_SCREEN_SIZE_DEFULT_5() const { return ___SCREEN_SIZE_DEFULT_5; }
	inline Vector2_t2156229523 * get_address_of_SCREEN_SIZE_DEFULT_5() { return &___SCREEN_SIZE_DEFULT_5; }
	inline void set_SCREEN_SIZE_DEFULT_5(Vector2_t2156229523  value)
	{
		___SCREEN_SIZE_DEFULT_5 = value;
	}

	inline static int32_t get_offset_of_SCREEN_SIZE_640_960_6() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866_StaticFields, ___SCREEN_SIZE_640_960_6)); }
	inline Vector2_t2156229523  get_SCREEN_SIZE_640_960_6() const { return ___SCREEN_SIZE_640_960_6; }
	inline Vector2_t2156229523 * get_address_of_SCREEN_SIZE_640_960_6() { return &___SCREEN_SIZE_640_960_6; }
	inline void set_SCREEN_SIZE_640_960_6(Vector2_t2156229523  value)
	{
		___SCREEN_SIZE_640_960_6 = value;
	}

	inline static int32_t get_offset_of_SCREEN_SIZE_640_1136_7() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866_StaticFields, ___SCREEN_SIZE_640_1136_7)); }
	inline Vector2_t2156229523  get_SCREEN_SIZE_640_1136_7() const { return ___SCREEN_SIZE_640_1136_7; }
	inline Vector2_t2156229523 * get_address_of_SCREEN_SIZE_640_1136_7() { return &___SCREEN_SIZE_640_1136_7; }
	inline void set_SCREEN_SIZE_640_1136_7(Vector2_t2156229523  value)
	{
		___SCREEN_SIZE_640_1136_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADJUSTER_T2553577866_H
#ifndef BACKGROUNDSCRIPT_T3530941027_H
#define BACKGROUNDSCRIPT_T3530941027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScript
struct  BackgroundScript_t3530941027  : public MonoBehaviour_t3962482529
{
public:
	// BackgroundScript/ColorBackChange BackgroundScript::OnBackChangeColor
	ColorBackChange_t4063191080 * ___OnBackChangeColor_3;
	// BackgroundScript/ColorBackChange BackgroundScript::OnHazeChangeColor
	ColorBackChange_t4063191080 * ___OnHazeChangeColor_4;
	// BackgroundScript/ColorBackChange BackgroundScript::OnMountainChangeColor
	ColorBackChange_t4063191080 * ___OnMountainChangeColor_5;
	// BackgroundScript/ColorBackChange BackgroundScript::OnCloudChangeColor
	ColorBackChange_t4063191080 * ___OnCloudChangeColor_6;
	// UnityEngine.Camera BackgroundScript::renderCamera
	Camera_t4157153871 * ___renderCamera_7;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::backColors
	List_1_t4027761066 * ___backColors_8;
	// UnityEngine.Color BackgroundScript::currentBackColor
	Color_t2555686324  ___currentBackColor_9;
	// UnityEngine.Color BackgroundScript::nextBackColor
	Color_t2555686324  ___nextBackColor_10;
	// UnityEngine.Color BackgroundScript::previousBackColor
	Color_t2555686324  ___previousBackColor_11;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::hazeColors
	List_1_t4027761066 * ___hazeColors_12;
	// UnityEngine.Color BackgroundScript::currentHazeColor
	Color_t2555686324  ___currentHazeColor_13;
	// UnityEngine.Color BackgroundScript::nextHazeColor
	Color_t2555686324  ___nextHazeColor_14;
	// UnityEngine.Color BackgroundScript::previousHazeColor
	Color_t2555686324  ___previousHazeColor_15;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::moutainColors
	List_1_t4027761066 * ___moutainColors_16;
	// UnityEngine.Color BackgroundScript::currentMountainColor
	Color_t2555686324  ___currentMountainColor_17;
	// UnityEngine.Color BackgroundScript::nextMountainColor
	Color_t2555686324  ___nextMountainColor_18;
	// UnityEngine.Color BackgroundScript::previousMountainColor
	Color_t2555686324  ___previousMountainColor_19;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::cloudColors
	List_1_t4027761066 * ___cloudColors_20;
	// UnityEngine.Color BackgroundScript::currentCloudColor
	Color_t2555686324  ___currentCloudColor_21;
	// UnityEngine.Color BackgroundScript::nextCloudColor
	Color_t2555686324  ___nextCloudColor_22;
	// UnityEngine.Color BackgroundScript::previousCloudColor
	Color_t2555686324  ___previousCloudColor_23;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::fogColors
	List_1_t4027761066 * ___fogColors_24;
	// UnityEngine.Color BackgroundScript::currentFogColor
	Color_t2555686324  ___currentFogColor_25;
	// UnityEngine.Color BackgroundScript::nextFogColor
	Color_t2555686324  ___nextFogColor_26;
	// UnityEngine.Color BackgroundScript::previousFogColor
	Color_t2555686324  ___previousFogColor_27;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::cameraColors
	List_1_t4027761066 * ___cameraColors_28;
	// UnityEngine.Color BackgroundScript::currentCameraColor
	Color_t2555686324  ___currentCameraColor_29;
	// UnityEngine.Color BackgroundScript::nextCameraColor
	Color_t2555686324  ___nextCameraColor_30;
	// UnityEngine.Color BackgroundScript::previousCameraColor
	Color_t2555686324  ___previousCameraColor_31;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::mStartColors
	List_1_t4027761066 * ___mStartColors_32;
	// UnityEngine.Color BackgroundScript::currentMStartColor
	Color_t2555686324  ___currentMStartColor_33;
	// UnityEngine.Color BackgroundScript::nextMStartColor
	Color_t2555686324  ___nextMStartColor_34;
	// UnityEngine.Color BackgroundScript::previousMStartColor
	Color_t2555686324  ___previousMStartColor_35;
	// System.Collections.Generic.List`1<UnityEngine.Color> BackgroundScript::mEndColors
	List_1_t4027761066 * ___mEndColors_36;
	// UnityEngine.Color BackgroundScript::currentMEndColor
	Color_t2555686324  ___currentMEndColor_37;
	// UnityEngine.Color BackgroundScript::nextMEndColor
	Color_t2555686324  ___nextMEndColor_38;
	// UnityEngine.Color BackgroundScript::previousMEndColor
	Color_t2555686324  ___previousMEndColor_39;
	// System.Int32 BackgroundScript::currentColorIndex
	int32_t ___currentColorIndex_40;
	// System.Collections.Generic.List`1<ColorBalance> BackgroundScript::colorBalances
	List_1_t4075065786 * ___colorBalances_41;
	// System.Int32 BackgroundScript::maxColorScoreLoop
	int32_t ___maxColorScoreLoop_42;
	// System.Single BackgroundScript::changeColorDuration
	float ___changeColorDuration_43;
	// System.Int32 BackgroundScript::colorScoreCount
	int32_t ___colorScoreCount_44;
	// System.Boolean BackgroundScript::isChangeColor
	bool ___isChangeColor_45;
	// System.Int32 BackgroundScript::colorStep
	int32_t ___colorStep_46;

public:
	inline static int32_t get_offset_of_OnBackChangeColor_3() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___OnBackChangeColor_3)); }
	inline ColorBackChange_t4063191080 * get_OnBackChangeColor_3() const { return ___OnBackChangeColor_3; }
	inline ColorBackChange_t4063191080 ** get_address_of_OnBackChangeColor_3() { return &___OnBackChangeColor_3; }
	inline void set_OnBackChangeColor_3(ColorBackChange_t4063191080 * value)
	{
		___OnBackChangeColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnBackChangeColor_3), value);
	}

	inline static int32_t get_offset_of_OnHazeChangeColor_4() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___OnHazeChangeColor_4)); }
	inline ColorBackChange_t4063191080 * get_OnHazeChangeColor_4() const { return ___OnHazeChangeColor_4; }
	inline ColorBackChange_t4063191080 ** get_address_of_OnHazeChangeColor_4() { return &___OnHazeChangeColor_4; }
	inline void set_OnHazeChangeColor_4(ColorBackChange_t4063191080 * value)
	{
		___OnHazeChangeColor_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnHazeChangeColor_4), value);
	}

	inline static int32_t get_offset_of_OnMountainChangeColor_5() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___OnMountainChangeColor_5)); }
	inline ColorBackChange_t4063191080 * get_OnMountainChangeColor_5() const { return ___OnMountainChangeColor_5; }
	inline ColorBackChange_t4063191080 ** get_address_of_OnMountainChangeColor_5() { return &___OnMountainChangeColor_5; }
	inline void set_OnMountainChangeColor_5(ColorBackChange_t4063191080 * value)
	{
		___OnMountainChangeColor_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnMountainChangeColor_5), value);
	}

	inline static int32_t get_offset_of_OnCloudChangeColor_6() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___OnCloudChangeColor_6)); }
	inline ColorBackChange_t4063191080 * get_OnCloudChangeColor_6() const { return ___OnCloudChangeColor_6; }
	inline ColorBackChange_t4063191080 ** get_address_of_OnCloudChangeColor_6() { return &___OnCloudChangeColor_6; }
	inline void set_OnCloudChangeColor_6(ColorBackChange_t4063191080 * value)
	{
		___OnCloudChangeColor_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnCloudChangeColor_6), value);
	}

	inline static int32_t get_offset_of_renderCamera_7() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___renderCamera_7)); }
	inline Camera_t4157153871 * get_renderCamera_7() const { return ___renderCamera_7; }
	inline Camera_t4157153871 ** get_address_of_renderCamera_7() { return &___renderCamera_7; }
	inline void set_renderCamera_7(Camera_t4157153871 * value)
	{
		___renderCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___renderCamera_7), value);
	}

	inline static int32_t get_offset_of_backColors_8() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___backColors_8)); }
	inline List_1_t4027761066 * get_backColors_8() const { return ___backColors_8; }
	inline List_1_t4027761066 ** get_address_of_backColors_8() { return &___backColors_8; }
	inline void set_backColors_8(List_1_t4027761066 * value)
	{
		___backColors_8 = value;
		Il2CppCodeGenWriteBarrier((&___backColors_8), value);
	}

	inline static int32_t get_offset_of_currentBackColor_9() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentBackColor_9)); }
	inline Color_t2555686324  get_currentBackColor_9() const { return ___currentBackColor_9; }
	inline Color_t2555686324 * get_address_of_currentBackColor_9() { return &___currentBackColor_9; }
	inline void set_currentBackColor_9(Color_t2555686324  value)
	{
		___currentBackColor_9 = value;
	}

	inline static int32_t get_offset_of_nextBackColor_10() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextBackColor_10)); }
	inline Color_t2555686324  get_nextBackColor_10() const { return ___nextBackColor_10; }
	inline Color_t2555686324 * get_address_of_nextBackColor_10() { return &___nextBackColor_10; }
	inline void set_nextBackColor_10(Color_t2555686324  value)
	{
		___nextBackColor_10 = value;
	}

	inline static int32_t get_offset_of_previousBackColor_11() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousBackColor_11)); }
	inline Color_t2555686324  get_previousBackColor_11() const { return ___previousBackColor_11; }
	inline Color_t2555686324 * get_address_of_previousBackColor_11() { return &___previousBackColor_11; }
	inline void set_previousBackColor_11(Color_t2555686324  value)
	{
		___previousBackColor_11 = value;
	}

	inline static int32_t get_offset_of_hazeColors_12() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___hazeColors_12)); }
	inline List_1_t4027761066 * get_hazeColors_12() const { return ___hazeColors_12; }
	inline List_1_t4027761066 ** get_address_of_hazeColors_12() { return &___hazeColors_12; }
	inline void set_hazeColors_12(List_1_t4027761066 * value)
	{
		___hazeColors_12 = value;
		Il2CppCodeGenWriteBarrier((&___hazeColors_12), value);
	}

	inline static int32_t get_offset_of_currentHazeColor_13() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentHazeColor_13)); }
	inline Color_t2555686324  get_currentHazeColor_13() const { return ___currentHazeColor_13; }
	inline Color_t2555686324 * get_address_of_currentHazeColor_13() { return &___currentHazeColor_13; }
	inline void set_currentHazeColor_13(Color_t2555686324  value)
	{
		___currentHazeColor_13 = value;
	}

	inline static int32_t get_offset_of_nextHazeColor_14() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextHazeColor_14)); }
	inline Color_t2555686324  get_nextHazeColor_14() const { return ___nextHazeColor_14; }
	inline Color_t2555686324 * get_address_of_nextHazeColor_14() { return &___nextHazeColor_14; }
	inline void set_nextHazeColor_14(Color_t2555686324  value)
	{
		___nextHazeColor_14 = value;
	}

	inline static int32_t get_offset_of_previousHazeColor_15() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousHazeColor_15)); }
	inline Color_t2555686324  get_previousHazeColor_15() const { return ___previousHazeColor_15; }
	inline Color_t2555686324 * get_address_of_previousHazeColor_15() { return &___previousHazeColor_15; }
	inline void set_previousHazeColor_15(Color_t2555686324  value)
	{
		___previousHazeColor_15 = value;
	}

	inline static int32_t get_offset_of_moutainColors_16() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___moutainColors_16)); }
	inline List_1_t4027761066 * get_moutainColors_16() const { return ___moutainColors_16; }
	inline List_1_t4027761066 ** get_address_of_moutainColors_16() { return &___moutainColors_16; }
	inline void set_moutainColors_16(List_1_t4027761066 * value)
	{
		___moutainColors_16 = value;
		Il2CppCodeGenWriteBarrier((&___moutainColors_16), value);
	}

	inline static int32_t get_offset_of_currentMountainColor_17() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentMountainColor_17)); }
	inline Color_t2555686324  get_currentMountainColor_17() const { return ___currentMountainColor_17; }
	inline Color_t2555686324 * get_address_of_currentMountainColor_17() { return &___currentMountainColor_17; }
	inline void set_currentMountainColor_17(Color_t2555686324  value)
	{
		___currentMountainColor_17 = value;
	}

	inline static int32_t get_offset_of_nextMountainColor_18() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextMountainColor_18)); }
	inline Color_t2555686324  get_nextMountainColor_18() const { return ___nextMountainColor_18; }
	inline Color_t2555686324 * get_address_of_nextMountainColor_18() { return &___nextMountainColor_18; }
	inline void set_nextMountainColor_18(Color_t2555686324  value)
	{
		___nextMountainColor_18 = value;
	}

	inline static int32_t get_offset_of_previousMountainColor_19() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousMountainColor_19)); }
	inline Color_t2555686324  get_previousMountainColor_19() const { return ___previousMountainColor_19; }
	inline Color_t2555686324 * get_address_of_previousMountainColor_19() { return &___previousMountainColor_19; }
	inline void set_previousMountainColor_19(Color_t2555686324  value)
	{
		___previousMountainColor_19 = value;
	}

	inline static int32_t get_offset_of_cloudColors_20() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___cloudColors_20)); }
	inline List_1_t4027761066 * get_cloudColors_20() const { return ___cloudColors_20; }
	inline List_1_t4027761066 ** get_address_of_cloudColors_20() { return &___cloudColors_20; }
	inline void set_cloudColors_20(List_1_t4027761066 * value)
	{
		___cloudColors_20 = value;
		Il2CppCodeGenWriteBarrier((&___cloudColors_20), value);
	}

	inline static int32_t get_offset_of_currentCloudColor_21() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentCloudColor_21)); }
	inline Color_t2555686324  get_currentCloudColor_21() const { return ___currentCloudColor_21; }
	inline Color_t2555686324 * get_address_of_currentCloudColor_21() { return &___currentCloudColor_21; }
	inline void set_currentCloudColor_21(Color_t2555686324  value)
	{
		___currentCloudColor_21 = value;
	}

	inline static int32_t get_offset_of_nextCloudColor_22() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextCloudColor_22)); }
	inline Color_t2555686324  get_nextCloudColor_22() const { return ___nextCloudColor_22; }
	inline Color_t2555686324 * get_address_of_nextCloudColor_22() { return &___nextCloudColor_22; }
	inline void set_nextCloudColor_22(Color_t2555686324  value)
	{
		___nextCloudColor_22 = value;
	}

	inline static int32_t get_offset_of_previousCloudColor_23() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousCloudColor_23)); }
	inline Color_t2555686324  get_previousCloudColor_23() const { return ___previousCloudColor_23; }
	inline Color_t2555686324 * get_address_of_previousCloudColor_23() { return &___previousCloudColor_23; }
	inline void set_previousCloudColor_23(Color_t2555686324  value)
	{
		___previousCloudColor_23 = value;
	}

	inline static int32_t get_offset_of_fogColors_24() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___fogColors_24)); }
	inline List_1_t4027761066 * get_fogColors_24() const { return ___fogColors_24; }
	inline List_1_t4027761066 ** get_address_of_fogColors_24() { return &___fogColors_24; }
	inline void set_fogColors_24(List_1_t4027761066 * value)
	{
		___fogColors_24 = value;
		Il2CppCodeGenWriteBarrier((&___fogColors_24), value);
	}

	inline static int32_t get_offset_of_currentFogColor_25() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentFogColor_25)); }
	inline Color_t2555686324  get_currentFogColor_25() const { return ___currentFogColor_25; }
	inline Color_t2555686324 * get_address_of_currentFogColor_25() { return &___currentFogColor_25; }
	inline void set_currentFogColor_25(Color_t2555686324  value)
	{
		___currentFogColor_25 = value;
	}

	inline static int32_t get_offset_of_nextFogColor_26() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextFogColor_26)); }
	inline Color_t2555686324  get_nextFogColor_26() const { return ___nextFogColor_26; }
	inline Color_t2555686324 * get_address_of_nextFogColor_26() { return &___nextFogColor_26; }
	inline void set_nextFogColor_26(Color_t2555686324  value)
	{
		___nextFogColor_26 = value;
	}

	inline static int32_t get_offset_of_previousFogColor_27() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousFogColor_27)); }
	inline Color_t2555686324  get_previousFogColor_27() const { return ___previousFogColor_27; }
	inline Color_t2555686324 * get_address_of_previousFogColor_27() { return &___previousFogColor_27; }
	inline void set_previousFogColor_27(Color_t2555686324  value)
	{
		___previousFogColor_27 = value;
	}

	inline static int32_t get_offset_of_cameraColors_28() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___cameraColors_28)); }
	inline List_1_t4027761066 * get_cameraColors_28() const { return ___cameraColors_28; }
	inline List_1_t4027761066 ** get_address_of_cameraColors_28() { return &___cameraColors_28; }
	inline void set_cameraColors_28(List_1_t4027761066 * value)
	{
		___cameraColors_28 = value;
		Il2CppCodeGenWriteBarrier((&___cameraColors_28), value);
	}

	inline static int32_t get_offset_of_currentCameraColor_29() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentCameraColor_29)); }
	inline Color_t2555686324  get_currentCameraColor_29() const { return ___currentCameraColor_29; }
	inline Color_t2555686324 * get_address_of_currentCameraColor_29() { return &___currentCameraColor_29; }
	inline void set_currentCameraColor_29(Color_t2555686324  value)
	{
		___currentCameraColor_29 = value;
	}

	inline static int32_t get_offset_of_nextCameraColor_30() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextCameraColor_30)); }
	inline Color_t2555686324  get_nextCameraColor_30() const { return ___nextCameraColor_30; }
	inline Color_t2555686324 * get_address_of_nextCameraColor_30() { return &___nextCameraColor_30; }
	inline void set_nextCameraColor_30(Color_t2555686324  value)
	{
		___nextCameraColor_30 = value;
	}

	inline static int32_t get_offset_of_previousCameraColor_31() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousCameraColor_31)); }
	inline Color_t2555686324  get_previousCameraColor_31() const { return ___previousCameraColor_31; }
	inline Color_t2555686324 * get_address_of_previousCameraColor_31() { return &___previousCameraColor_31; }
	inline void set_previousCameraColor_31(Color_t2555686324  value)
	{
		___previousCameraColor_31 = value;
	}

	inline static int32_t get_offset_of_mStartColors_32() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___mStartColors_32)); }
	inline List_1_t4027761066 * get_mStartColors_32() const { return ___mStartColors_32; }
	inline List_1_t4027761066 ** get_address_of_mStartColors_32() { return &___mStartColors_32; }
	inline void set_mStartColors_32(List_1_t4027761066 * value)
	{
		___mStartColors_32 = value;
		Il2CppCodeGenWriteBarrier((&___mStartColors_32), value);
	}

	inline static int32_t get_offset_of_currentMStartColor_33() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentMStartColor_33)); }
	inline Color_t2555686324  get_currentMStartColor_33() const { return ___currentMStartColor_33; }
	inline Color_t2555686324 * get_address_of_currentMStartColor_33() { return &___currentMStartColor_33; }
	inline void set_currentMStartColor_33(Color_t2555686324  value)
	{
		___currentMStartColor_33 = value;
	}

	inline static int32_t get_offset_of_nextMStartColor_34() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextMStartColor_34)); }
	inline Color_t2555686324  get_nextMStartColor_34() const { return ___nextMStartColor_34; }
	inline Color_t2555686324 * get_address_of_nextMStartColor_34() { return &___nextMStartColor_34; }
	inline void set_nextMStartColor_34(Color_t2555686324  value)
	{
		___nextMStartColor_34 = value;
	}

	inline static int32_t get_offset_of_previousMStartColor_35() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousMStartColor_35)); }
	inline Color_t2555686324  get_previousMStartColor_35() const { return ___previousMStartColor_35; }
	inline Color_t2555686324 * get_address_of_previousMStartColor_35() { return &___previousMStartColor_35; }
	inline void set_previousMStartColor_35(Color_t2555686324  value)
	{
		___previousMStartColor_35 = value;
	}

	inline static int32_t get_offset_of_mEndColors_36() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___mEndColors_36)); }
	inline List_1_t4027761066 * get_mEndColors_36() const { return ___mEndColors_36; }
	inline List_1_t4027761066 ** get_address_of_mEndColors_36() { return &___mEndColors_36; }
	inline void set_mEndColors_36(List_1_t4027761066 * value)
	{
		___mEndColors_36 = value;
		Il2CppCodeGenWriteBarrier((&___mEndColors_36), value);
	}

	inline static int32_t get_offset_of_currentMEndColor_37() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentMEndColor_37)); }
	inline Color_t2555686324  get_currentMEndColor_37() const { return ___currentMEndColor_37; }
	inline Color_t2555686324 * get_address_of_currentMEndColor_37() { return &___currentMEndColor_37; }
	inline void set_currentMEndColor_37(Color_t2555686324  value)
	{
		___currentMEndColor_37 = value;
	}

	inline static int32_t get_offset_of_nextMEndColor_38() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___nextMEndColor_38)); }
	inline Color_t2555686324  get_nextMEndColor_38() const { return ___nextMEndColor_38; }
	inline Color_t2555686324 * get_address_of_nextMEndColor_38() { return &___nextMEndColor_38; }
	inline void set_nextMEndColor_38(Color_t2555686324  value)
	{
		___nextMEndColor_38 = value;
	}

	inline static int32_t get_offset_of_previousMEndColor_39() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___previousMEndColor_39)); }
	inline Color_t2555686324  get_previousMEndColor_39() const { return ___previousMEndColor_39; }
	inline Color_t2555686324 * get_address_of_previousMEndColor_39() { return &___previousMEndColor_39; }
	inline void set_previousMEndColor_39(Color_t2555686324  value)
	{
		___previousMEndColor_39 = value;
	}

	inline static int32_t get_offset_of_currentColorIndex_40() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___currentColorIndex_40)); }
	inline int32_t get_currentColorIndex_40() const { return ___currentColorIndex_40; }
	inline int32_t* get_address_of_currentColorIndex_40() { return &___currentColorIndex_40; }
	inline void set_currentColorIndex_40(int32_t value)
	{
		___currentColorIndex_40 = value;
	}

	inline static int32_t get_offset_of_colorBalances_41() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___colorBalances_41)); }
	inline List_1_t4075065786 * get_colorBalances_41() const { return ___colorBalances_41; }
	inline List_1_t4075065786 ** get_address_of_colorBalances_41() { return &___colorBalances_41; }
	inline void set_colorBalances_41(List_1_t4075065786 * value)
	{
		___colorBalances_41 = value;
		Il2CppCodeGenWriteBarrier((&___colorBalances_41), value);
	}

	inline static int32_t get_offset_of_maxColorScoreLoop_42() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___maxColorScoreLoop_42)); }
	inline int32_t get_maxColorScoreLoop_42() const { return ___maxColorScoreLoop_42; }
	inline int32_t* get_address_of_maxColorScoreLoop_42() { return &___maxColorScoreLoop_42; }
	inline void set_maxColorScoreLoop_42(int32_t value)
	{
		___maxColorScoreLoop_42 = value;
	}

	inline static int32_t get_offset_of_changeColorDuration_43() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___changeColorDuration_43)); }
	inline float get_changeColorDuration_43() const { return ___changeColorDuration_43; }
	inline float* get_address_of_changeColorDuration_43() { return &___changeColorDuration_43; }
	inline void set_changeColorDuration_43(float value)
	{
		___changeColorDuration_43 = value;
	}

	inline static int32_t get_offset_of_colorScoreCount_44() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___colorScoreCount_44)); }
	inline int32_t get_colorScoreCount_44() const { return ___colorScoreCount_44; }
	inline int32_t* get_address_of_colorScoreCount_44() { return &___colorScoreCount_44; }
	inline void set_colorScoreCount_44(int32_t value)
	{
		___colorScoreCount_44 = value;
	}

	inline static int32_t get_offset_of_isChangeColor_45() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___isChangeColor_45)); }
	inline bool get_isChangeColor_45() const { return ___isChangeColor_45; }
	inline bool* get_address_of_isChangeColor_45() { return &___isChangeColor_45; }
	inline void set_isChangeColor_45(bool value)
	{
		___isChangeColor_45 = value;
	}

	inline static int32_t get_offset_of_colorStep_46() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027, ___colorStep_46)); }
	inline int32_t get_colorStep_46() const { return ___colorStep_46; }
	inline int32_t* get_address_of_colorStep_46() { return &___colorStep_46; }
	inline void set_colorStep_46(int32_t value)
	{
		___colorStep_46 = value;
	}
};

struct BackgroundScript_t3530941027_StaticFields
{
public:
	// BackgroundScript BackgroundScript::instance
	BackgroundScript_t3530941027 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(BackgroundScript_t3530941027_StaticFields, ___instance_2)); }
	inline BackgroundScript_t3530941027 * get_instance_2() const { return ___instance_2; }
	inline BackgroundScript_t3530941027 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(BackgroundScript_t3530941027 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDSCRIPT_T3530941027_H
#ifndef GAMESAMPLE_T1080372905_H
#define GAMESAMPLE_T1080372905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSample
struct  GameSample_t1080372905  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GameSample::_cubeNum
	int32_t ____cubeNum_2;

public:
	inline static int32_t get_offset_of__cubeNum_2() { return static_cast<int32_t>(offsetof(GameSample_t1080372905, ____cubeNum_2)); }
	inline int32_t get__cubeNum_2() const { return ____cubeNum_2; }
	inline int32_t* get_address_of__cubeNum_2() { return &____cubeNum_2; }
	inline void set__cubeNum_2(int32_t value)
	{
		____cubeNum_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESAMPLE_T1080372905_H
#ifndef PLAYERSCRIPT_T1783516946_H
#define PLAYERSCRIPT_T1783516946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerScript
struct  PlayerScript_t1783516946  : public MonoBehaviour_t3962482529
{
public:
	// PlayerScript/PlayerType PlayerScript::type
	int32_t ___type_3;
	// System.Single PlayerScript::moveDistance
	float ___moveDistance_4;
	// System.Single PlayerScript::moveDuration
	float ___moveDuration_5;
	// PlayerScript/PlayerState PlayerScript::state
	int32_t ___state_6;
	// UnityEngine.Vector3 PlayerScript::positionA
	Vector3_t3722313464  ___positionA_7;
	// UnityEngine.Vector3 PlayerScript::positionB
	Vector3_t3722313464  ___positionB_8;
	// System.Single PlayerScript::moveTime
	float ___moveTime_9;
	// System.Boolean PlayerScript::isDead
	bool ___isDead_10;
	// System.Int32 PlayerScript::_touchId
	int32_t ____touchId_11;
	// UnityEngine.MeshRenderer PlayerScript::_rendererFirst
	MeshRenderer_t587009260 * ____rendererFirst_12;
	// UnityEngine.MeshRenderer PlayerScript::_rendererSecond
	MeshRenderer_t587009260 * ____rendererSecond_13;
	// UnityEngine.ParticleSystem PlayerScript::_particle
	ParticleSystem_t1800779281 * ____particle_14;
	// UnityEngine.Material[] PlayerScript::_material
	MaterialU5BU5D_t561872642* ____material_15;
	// UnityEngine.Material[] PlayerScript::_materialComplex
	MaterialU5BU5D_t561872642* ____materialComplex_16;
	// UnityEngine.Mesh[] PlayerScript::_mesh
	MeshU5BU5D_t3972987605* ____mesh_17;
	// UnityEngine.GameObject PlayerScript::extraShader
	GameObject_t1113636619 * ___extraShader_18;
	// UnityEngine.Rigidbody PlayerScript::_rigid
	Rigidbody_t3916780224 * ____rigid_19;
	// UnityEngine.MeshRenderer PlayerScript::_renderer
	MeshRenderer_t587009260 * ____renderer_20;

public:
	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_moveDistance_4() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___moveDistance_4)); }
	inline float get_moveDistance_4() const { return ___moveDistance_4; }
	inline float* get_address_of_moveDistance_4() { return &___moveDistance_4; }
	inline void set_moveDistance_4(float value)
	{
		___moveDistance_4 = value;
	}

	inline static int32_t get_offset_of_moveDuration_5() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___moveDuration_5)); }
	inline float get_moveDuration_5() const { return ___moveDuration_5; }
	inline float* get_address_of_moveDuration_5() { return &___moveDuration_5; }
	inline void set_moveDuration_5(float value)
	{
		___moveDuration_5 = value;
	}

	inline static int32_t get_offset_of_state_6() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___state_6)); }
	inline int32_t get_state_6() const { return ___state_6; }
	inline int32_t* get_address_of_state_6() { return &___state_6; }
	inline void set_state_6(int32_t value)
	{
		___state_6 = value;
	}

	inline static int32_t get_offset_of_positionA_7() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___positionA_7)); }
	inline Vector3_t3722313464  get_positionA_7() const { return ___positionA_7; }
	inline Vector3_t3722313464 * get_address_of_positionA_7() { return &___positionA_7; }
	inline void set_positionA_7(Vector3_t3722313464  value)
	{
		___positionA_7 = value;
	}

	inline static int32_t get_offset_of_positionB_8() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___positionB_8)); }
	inline Vector3_t3722313464  get_positionB_8() const { return ___positionB_8; }
	inline Vector3_t3722313464 * get_address_of_positionB_8() { return &___positionB_8; }
	inline void set_positionB_8(Vector3_t3722313464  value)
	{
		___positionB_8 = value;
	}

	inline static int32_t get_offset_of_moveTime_9() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___moveTime_9)); }
	inline float get_moveTime_9() const { return ___moveTime_9; }
	inline float* get_address_of_moveTime_9() { return &___moveTime_9; }
	inline void set_moveTime_9(float value)
	{
		___moveTime_9 = value;
	}

	inline static int32_t get_offset_of_isDead_10() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___isDead_10)); }
	inline bool get_isDead_10() const { return ___isDead_10; }
	inline bool* get_address_of_isDead_10() { return &___isDead_10; }
	inline void set_isDead_10(bool value)
	{
		___isDead_10 = value;
	}

	inline static int32_t get_offset_of__touchId_11() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____touchId_11)); }
	inline int32_t get__touchId_11() const { return ____touchId_11; }
	inline int32_t* get_address_of__touchId_11() { return &____touchId_11; }
	inline void set__touchId_11(int32_t value)
	{
		____touchId_11 = value;
	}

	inline static int32_t get_offset_of__rendererFirst_12() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____rendererFirst_12)); }
	inline MeshRenderer_t587009260 * get__rendererFirst_12() const { return ____rendererFirst_12; }
	inline MeshRenderer_t587009260 ** get_address_of__rendererFirst_12() { return &____rendererFirst_12; }
	inline void set__rendererFirst_12(MeshRenderer_t587009260 * value)
	{
		____rendererFirst_12 = value;
		Il2CppCodeGenWriteBarrier((&____rendererFirst_12), value);
	}

	inline static int32_t get_offset_of__rendererSecond_13() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____rendererSecond_13)); }
	inline MeshRenderer_t587009260 * get__rendererSecond_13() const { return ____rendererSecond_13; }
	inline MeshRenderer_t587009260 ** get_address_of__rendererSecond_13() { return &____rendererSecond_13; }
	inline void set__rendererSecond_13(MeshRenderer_t587009260 * value)
	{
		____rendererSecond_13 = value;
		Il2CppCodeGenWriteBarrier((&____rendererSecond_13), value);
	}

	inline static int32_t get_offset_of__particle_14() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____particle_14)); }
	inline ParticleSystem_t1800779281 * get__particle_14() const { return ____particle_14; }
	inline ParticleSystem_t1800779281 ** get_address_of__particle_14() { return &____particle_14; }
	inline void set__particle_14(ParticleSystem_t1800779281 * value)
	{
		____particle_14 = value;
		Il2CppCodeGenWriteBarrier((&____particle_14), value);
	}

	inline static int32_t get_offset_of__material_15() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____material_15)); }
	inline MaterialU5BU5D_t561872642* get__material_15() const { return ____material_15; }
	inline MaterialU5BU5D_t561872642** get_address_of__material_15() { return &____material_15; }
	inline void set__material_15(MaterialU5BU5D_t561872642* value)
	{
		____material_15 = value;
		Il2CppCodeGenWriteBarrier((&____material_15), value);
	}

	inline static int32_t get_offset_of__materialComplex_16() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____materialComplex_16)); }
	inline MaterialU5BU5D_t561872642* get__materialComplex_16() const { return ____materialComplex_16; }
	inline MaterialU5BU5D_t561872642** get_address_of__materialComplex_16() { return &____materialComplex_16; }
	inline void set__materialComplex_16(MaterialU5BU5D_t561872642* value)
	{
		____materialComplex_16 = value;
		Il2CppCodeGenWriteBarrier((&____materialComplex_16), value);
	}

	inline static int32_t get_offset_of__mesh_17() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____mesh_17)); }
	inline MeshU5BU5D_t3972987605* get__mesh_17() const { return ____mesh_17; }
	inline MeshU5BU5D_t3972987605** get_address_of__mesh_17() { return &____mesh_17; }
	inline void set__mesh_17(MeshU5BU5D_t3972987605* value)
	{
		____mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&____mesh_17), value);
	}

	inline static int32_t get_offset_of_extraShader_18() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ___extraShader_18)); }
	inline GameObject_t1113636619 * get_extraShader_18() const { return ___extraShader_18; }
	inline GameObject_t1113636619 ** get_address_of_extraShader_18() { return &___extraShader_18; }
	inline void set_extraShader_18(GameObject_t1113636619 * value)
	{
		___extraShader_18 = value;
		Il2CppCodeGenWriteBarrier((&___extraShader_18), value);
	}

	inline static int32_t get_offset_of__rigid_19() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____rigid_19)); }
	inline Rigidbody_t3916780224 * get__rigid_19() const { return ____rigid_19; }
	inline Rigidbody_t3916780224 ** get_address_of__rigid_19() { return &____rigid_19; }
	inline void set__rigid_19(Rigidbody_t3916780224 * value)
	{
		____rigid_19 = value;
		Il2CppCodeGenWriteBarrier((&____rigid_19), value);
	}

	inline static int32_t get_offset_of__renderer_20() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946, ____renderer_20)); }
	inline MeshRenderer_t587009260 * get__renderer_20() const { return ____renderer_20; }
	inline MeshRenderer_t587009260 ** get_address_of__renderer_20() { return &____renderer_20; }
	inline void set__renderer_20(MeshRenderer_t587009260 * value)
	{
		____renderer_20 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_20), value);
	}
};

struct PlayerScript_t1783516946_StaticFields
{
public:
	// PlayerScript/PlayerDie PlayerScript::OnPlayerDie
	PlayerDie_t274471295 * ___OnPlayerDie_2;

public:
	inline static int32_t get_offset_of_OnPlayerDie_2() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946_StaticFields, ___OnPlayerDie_2)); }
	inline PlayerDie_t274471295 * get_OnPlayerDie_2() const { return ___OnPlayerDie_2; }
	inline PlayerDie_t274471295 ** get_address_of_OnPlayerDie_2() { return &___OnPlayerDie_2; }
	inline void set_OnPlayerDie_2(PlayerDie_t274471295 * value)
	{
		___OnPlayerDie_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayerDie_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSCRIPT_T1783516946_H
#ifndef BACKIMAGECAMERA_T3631742959_H
#define BACKIMAGECAMERA_T3631742959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackImageCamera
struct  BackImageCamera_t3631742959  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean BackImageCamera::reverseFog
	bool ___reverseFog_2;
	// UnityEngine.Color BackImageCamera::reverseFogColor
	Color_t2555686324  ___reverseFogColor_3;
	// System.Single BackImageCamera::reverseFogDensity
	float ___reverseFogDensity_4;
	// System.Single BackImageCamera::reverseFogStart
	float ___reverseFogStart_5;
	// System.Single BackImageCamera::reverseFogEnd
	float ___reverseFogEnd_6;
	// UnityEngine.FogMode BackImageCamera::reverseFogMode
	int32_t ___reverseFogMode_7;

public:
	inline static int32_t get_offset_of_reverseFog_2() { return static_cast<int32_t>(offsetof(BackImageCamera_t3631742959, ___reverseFog_2)); }
	inline bool get_reverseFog_2() const { return ___reverseFog_2; }
	inline bool* get_address_of_reverseFog_2() { return &___reverseFog_2; }
	inline void set_reverseFog_2(bool value)
	{
		___reverseFog_2 = value;
	}

	inline static int32_t get_offset_of_reverseFogColor_3() { return static_cast<int32_t>(offsetof(BackImageCamera_t3631742959, ___reverseFogColor_3)); }
	inline Color_t2555686324  get_reverseFogColor_3() const { return ___reverseFogColor_3; }
	inline Color_t2555686324 * get_address_of_reverseFogColor_3() { return &___reverseFogColor_3; }
	inline void set_reverseFogColor_3(Color_t2555686324  value)
	{
		___reverseFogColor_3 = value;
	}

	inline static int32_t get_offset_of_reverseFogDensity_4() { return static_cast<int32_t>(offsetof(BackImageCamera_t3631742959, ___reverseFogDensity_4)); }
	inline float get_reverseFogDensity_4() const { return ___reverseFogDensity_4; }
	inline float* get_address_of_reverseFogDensity_4() { return &___reverseFogDensity_4; }
	inline void set_reverseFogDensity_4(float value)
	{
		___reverseFogDensity_4 = value;
	}

	inline static int32_t get_offset_of_reverseFogStart_5() { return static_cast<int32_t>(offsetof(BackImageCamera_t3631742959, ___reverseFogStart_5)); }
	inline float get_reverseFogStart_5() const { return ___reverseFogStart_5; }
	inline float* get_address_of_reverseFogStart_5() { return &___reverseFogStart_5; }
	inline void set_reverseFogStart_5(float value)
	{
		___reverseFogStart_5 = value;
	}

	inline static int32_t get_offset_of_reverseFogEnd_6() { return static_cast<int32_t>(offsetof(BackImageCamera_t3631742959, ___reverseFogEnd_6)); }
	inline float get_reverseFogEnd_6() const { return ___reverseFogEnd_6; }
	inline float* get_address_of_reverseFogEnd_6() { return &___reverseFogEnd_6; }
	inline void set_reverseFogEnd_6(float value)
	{
		___reverseFogEnd_6 = value;
	}

	inline static int32_t get_offset_of_reverseFogMode_7() { return static_cast<int32_t>(offsetof(BackImageCamera_t3631742959, ___reverseFogMode_7)); }
	inline int32_t get_reverseFogMode_7() const { return ___reverseFogMode_7; }
	inline int32_t* get_address_of_reverseFogMode_7() { return &___reverseFogMode_7; }
	inline void set_reverseFogMode_7(int32_t value)
	{
		___reverseFogMode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKIMAGECAMERA_T3631742959_H
#ifndef BESTLABEL_T2479364186_H
#define BESTLABEL_T2479364186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestLabel
struct  BestLabel_t2479364186  : public MonoBehaviour_t3962482529
{
public:
	// System.String BestLabel::_textFormat
	String_t* ____textFormat_2;

public:
	inline static int32_t get_offset_of__textFormat_2() { return static_cast<int32_t>(offsetof(BestLabel_t2479364186, ____textFormat_2)); }
	inline String_t* get__textFormat_2() const { return ____textFormat_2; }
	inline String_t** get_address_of__textFormat_2() { return &____textFormat_2; }
	inline void set__textFormat_2(String_t* value)
	{
		____textFormat_2 = value;
		Il2CppCodeGenWriteBarrier((&____textFormat_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BESTLABEL_T2479364186_H
#ifndef BUTTONASSISTANT_T3922690862_H
#define BUTTONASSISTANT_T3922690862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonAssistant
struct  ButtonAssistant_t3922690862  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Collider ButtonAssistant::_collider
	Collider_t1773347010 * ____collider_2;
	// UIButton ButtonAssistant::_button
	UIButton_t1100396938 * ____button_3;
	// UISprite ButtonAssistant::_image
	UISprite_t194114938 * ____image_4;

public:
	inline static int32_t get_offset_of__collider_2() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____collider_2)); }
	inline Collider_t1773347010 * get__collider_2() const { return ____collider_2; }
	inline Collider_t1773347010 ** get_address_of__collider_2() { return &____collider_2; }
	inline void set__collider_2(Collider_t1773347010 * value)
	{
		____collider_2 = value;
		Il2CppCodeGenWriteBarrier((&____collider_2), value);
	}

	inline static int32_t get_offset_of__button_3() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____button_3)); }
	inline UIButton_t1100396938 * get__button_3() const { return ____button_3; }
	inline UIButton_t1100396938 ** get_address_of__button_3() { return &____button_3; }
	inline void set__button_3(UIButton_t1100396938 * value)
	{
		____button_3 = value;
		Il2CppCodeGenWriteBarrier((&____button_3), value);
	}

	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____image_4)); }
	inline UISprite_t194114938 * get__image_4() const { return ____image_4; }
	inline UISprite_t194114938 ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(UISprite_t194114938 * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONASSISTANT_T3922690862_H
#ifndef BACKCAMERASCRIPT_T420974540_H
#define BACKCAMERASCRIPT_T420974540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackCameraScript
struct  BackCameraScript_t420974540  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean BackCameraScript::reverseSetting
	bool ___reverseSetting_2;

public:
	inline static int32_t get_offset_of_reverseSetting_2() { return static_cast<int32_t>(offsetof(BackCameraScript_t420974540, ___reverseSetting_2)); }
	inline bool get_reverseSetting_2() const { return ___reverseSetting_2; }
	inline bool* get_address_of_reverseSetting_2() { return &___reverseSetting_2; }
	inline void set_reverseSetting_2(bool value)
	{
		___reverseSetting_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKCAMERASCRIPT_T420974540_H
#ifndef GRIDDEPLOYER_T1241683661_H
#define GRIDDEPLOYER_T1241683661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GridDeployer
struct  GridDeployer_t1241683661  : public MonoBehaviour_t3962482529
{
public:
	// GridDeployer/Dimension GridDeployer::_previousDimension
	int32_t ____previousDimension_2;
	// GridDeployer/Dimension GridDeployer::_nextDimension
	int32_t ____nextDimension_3;
	// GridDeployer/PivotType GridDeployer::_previousPivot
	int32_t ____previousPivot_4;
	// GridDeployer/PivotType GridDeployer::_nextPivot
	int32_t ____nextPivot_5;
	// System.Single GridDeployer::_previousSpace
	float ____previousSpace_6;
	// System.Single GridDeployer::_nextSpace
	float ____nextSpace_7;
	// System.Int32 GridDeployer::_limit
	int32_t ____limit_8;

public:
	inline static int32_t get_offset_of__previousDimension_2() { return static_cast<int32_t>(offsetof(GridDeployer_t1241683661, ____previousDimension_2)); }
	inline int32_t get__previousDimension_2() const { return ____previousDimension_2; }
	inline int32_t* get_address_of__previousDimension_2() { return &____previousDimension_2; }
	inline void set__previousDimension_2(int32_t value)
	{
		____previousDimension_2 = value;
	}

	inline static int32_t get_offset_of__nextDimension_3() { return static_cast<int32_t>(offsetof(GridDeployer_t1241683661, ____nextDimension_3)); }
	inline int32_t get__nextDimension_3() const { return ____nextDimension_3; }
	inline int32_t* get_address_of__nextDimension_3() { return &____nextDimension_3; }
	inline void set__nextDimension_3(int32_t value)
	{
		____nextDimension_3 = value;
	}

	inline static int32_t get_offset_of__previousPivot_4() { return static_cast<int32_t>(offsetof(GridDeployer_t1241683661, ____previousPivot_4)); }
	inline int32_t get__previousPivot_4() const { return ____previousPivot_4; }
	inline int32_t* get_address_of__previousPivot_4() { return &____previousPivot_4; }
	inline void set__previousPivot_4(int32_t value)
	{
		____previousPivot_4 = value;
	}

	inline static int32_t get_offset_of__nextPivot_5() { return static_cast<int32_t>(offsetof(GridDeployer_t1241683661, ____nextPivot_5)); }
	inline int32_t get__nextPivot_5() const { return ____nextPivot_5; }
	inline int32_t* get_address_of__nextPivot_5() { return &____nextPivot_5; }
	inline void set__nextPivot_5(int32_t value)
	{
		____nextPivot_5 = value;
	}

	inline static int32_t get_offset_of__previousSpace_6() { return static_cast<int32_t>(offsetof(GridDeployer_t1241683661, ____previousSpace_6)); }
	inline float get__previousSpace_6() const { return ____previousSpace_6; }
	inline float* get_address_of__previousSpace_6() { return &____previousSpace_6; }
	inline void set__previousSpace_6(float value)
	{
		____previousSpace_6 = value;
	}

	inline static int32_t get_offset_of__nextSpace_7() { return static_cast<int32_t>(offsetof(GridDeployer_t1241683661, ____nextSpace_7)); }
	inline float get__nextSpace_7() const { return ____nextSpace_7; }
	inline float* get_address_of__nextSpace_7() { return &____nextSpace_7; }
	inline void set__nextSpace_7(float value)
	{
		____nextSpace_7 = value;
	}

	inline static int32_t get_offset_of__limit_8() { return static_cast<int32_t>(offsetof(GridDeployer_t1241683661, ____limit_8)); }
	inline int32_t get__limit_8() const { return ____limit_8; }
	inline int32_t* get_address_of__limit_8() { return &____limit_8; }
	inline void set__limit_8(int32_t value)
	{
		____limit_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDDEPLOYER_T1241683661_H
#ifndef CIRCLEDEPLOYER_T1503689571_H
#define CIRCLEDEPLOYER_T1503689571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleDeployer
struct  CircleDeployer_t1503689571  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CircleDeployer::_radius
	float ____radius_2;

public:
	inline static int32_t get_offset_of__radius_2() { return static_cast<int32_t>(offsetof(CircleDeployer_t1503689571, ____radius_2)); }
	inline float get__radius_2() const { return ____radius_2; }
	inline float* get_address_of__radius_2() { return &____radius_2; }
	inline void set__radius_2(float value)
	{
		____radius_2 = value;
	}
};

struct CircleDeployer_t1503689571_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.GameObject> CircleDeployer::<>f__am$cache0
	Comparison_1_t888567798 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(CircleDeployer_t1503689571_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Comparison_1_t888567798 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Comparison_1_t888567798 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Comparison_1_t888567798 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLEDEPLOYER_T1503689571_H
#ifndef AUTOBGMSWITCHER_T3639340263_H
#define AUTOBGMSWITCHER_T3639340263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoBGMSwitcher
struct  AutoBGMSwitcher_t3639340263  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOBGMSWITCHER_T3639340263_H
#ifndef NATIVEPLUGIN_T1231223992_H
#define NATIVEPLUGIN_T1231223992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativePlugin
struct  NativePlugin_t1231223992  : public MonoBehaviour_t3962482529
{
public:
	// System.IntPtr NativePlugin::_nativeInstance
	intptr_t ____nativeInstance_3;

public:
	inline static int32_t get_offset_of__nativeInstance_3() { return static_cast<int32_t>(offsetof(NativePlugin_t1231223992, ____nativeInstance_3)); }
	inline intptr_t get__nativeInstance_3() const { return ____nativeInstance_3; }
	inline intptr_t* get_address_of__nativeInstance_3() { return &____nativeInstance_3; }
	inline void set__nativeInstance_3(intptr_t value)
	{
		____nativeInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEPLUGIN_T1231223992_H
#ifndef VELOCITYTMP_T3274308841_H
#define VELOCITYTMP_T3274308841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VelocityTmp
struct  VelocityTmp_t3274308841  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 VelocityTmp::_angularVelocity
	Vector3_t3722313464  ____angularVelocity_2;
	// UnityEngine.Vector3 VelocityTmp::_velocity
	Vector3_t3722313464  ____velocity_3;

public:
	inline static int32_t get_offset_of__angularVelocity_2() { return static_cast<int32_t>(offsetof(VelocityTmp_t3274308841, ____angularVelocity_2)); }
	inline Vector3_t3722313464  get__angularVelocity_2() const { return ____angularVelocity_2; }
	inline Vector3_t3722313464 * get_address_of__angularVelocity_2() { return &____angularVelocity_2; }
	inline void set__angularVelocity_2(Vector3_t3722313464  value)
	{
		____angularVelocity_2 = value;
	}

	inline static int32_t get_offset_of__velocity_3() { return static_cast<int32_t>(offsetof(VelocityTmp_t3274308841, ____velocity_3)); }
	inline Vector3_t3722313464  get__velocity_3() const { return ____velocity_3; }
	inline Vector3_t3722313464 * get_address_of__velocity_3() { return &____velocity_3; }
	inline void set__velocity_3(Vector3_t3722313464  value)
	{
		____velocity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VELOCITYTMP_T3274308841_H
#ifndef VELOCITY2DTMP_T2283591314_H
#define VELOCITY2DTMP_T2283591314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Velocity2DTmp
struct  Velocity2DTmp_t2283591314  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Velocity2DTmp::_angularVelocity
	float ____angularVelocity_2;
	// UnityEngine.Vector2 Velocity2DTmp::_velocity
	Vector2_t2156229523  ____velocity_3;

public:
	inline static int32_t get_offset_of__angularVelocity_2() { return static_cast<int32_t>(offsetof(Velocity2DTmp_t2283591314, ____angularVelocity_2)); }
	inline float get__angularVelocity_2() const { return ____angularVelocity_2; }
	inline float* get_address_of__angularVelocity_2() { return &____angularVelocity_2; }
	inline void set__angularVelocity_2(float value)
	{
		____angularVelocity_2 = value;
	}

	inline static int32_t get_offset_of__velocity_3() { return static_cast<int32_t>(offsetof(Velocity2DTmp_t2283591314, ____velocity_3)); }
	inline Vector2_t2156229523  get__velocity_3() const { return ____velocity_3; }
	inline Vector2_t2156229523 * get_address_of__velocity_3() { return &____velocity_3; }
	inline void set__velocity_3(Vector2_t2156229523  value)
	{
		____velocity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VELOCITY2DTMP_T2283591314_H
#ifndef RECYCLINGOBJECTPOOL_T1893803714_H
#define RECYCLINGOBJECTPOOL_T1893803714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecyclingObjectPool
struct  RecyclingObjectPool_t1893803714  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> RecyclingObjectPool::_originalDict
	Dictionary_2_t898892918 * ____originalDict_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>> RecyclingObjectPool::_pool
	Dictionary_2_t2370967660 * ____pool_3;

public:
	inline static int32_t get_offset_of__originalDict_2() { return static_cast<int32_t>(offsetof(RecyclingObjectPool_t1893803714, ____originalDict_2)); }
	inline Dictionary_2_t898892918 * get__originalDict_2() const { return ____originalDict_2; }
	inline Dictionary_2_t898892918 ** get_address_of__originalDict_2() { return &____originalDict_2; }
	inline void set__originalDict_2(Dictionary_2_t898892918 * value)
	{
		____originalDict_2 = value;
		Il2CppCodeGenWriteBarrier((&____originalDict_2), value);
	}

	inline static int32_t get_offset_of__pool_3() { return static_cast<int32_t>(offsetof(RecyclingObjectPool_t1893803714, ____pool_3)); }
	inline Dictionary_2_t2370967660 * get__pool_3() const { return ____pool_3; }
	inline Dictionary_2_t2370967660 ** get_address_of__pool_3() { return &____pool_3; }
	inline void set__pool_3(Dictionary_2_t2370967660 * value)
	{
		____pool_3 = value;
		Il2CppCodeGenWriteBarrier((&____pool_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECYCLINGOBJECTPOOL_T1893803714_H
#ifndef TUTORIALPOINTSCRIPT_T2361877884_H
#define TUTORIALPOINTSCRIPT_T2361877884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPointScript
struct  TutorialPointScript_t2361877884  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody TutorialPointScript::rigibody
	Rigidbody_t3916780224 * ___rigibody_4;
	// System.Int32 TutorialPointScript::collisionCount
	int32_t ___collisionCount_5;

public:
	inline static int32_t get_offset_of_rigibody_4() { return static_cast<int32_t>(offsetof(TutorialPointScript_t2361877884, ___rigibody_4)); }
	inline Rigidbody_t3916780224 * get_rigibody_4() const { return ___rigibody_4; }
	inline Rigidbody_t3916780224 ** get_address_of_rigibody_4() { return &___rigibody_4; }
	inline void set_rigibody_4(Rigidbody_t3916780224 * value)
	{
		___rigibody_4 = value;
		Il2CppCodeGenWriteBarrier((&___rigibody_4), value);
	}

	inline static int32_t get_offset_of_collisionCount_5() { return static_cast<int32_t>(offsetof(TutorialPointScript_t2361877884, ___collisionCount_5)); }
	inline int32_t get_collisionCount_5() const { return ___collisionCount_5; }
	inline int32_t* get_address_of_collisionCount_5() { return &___collisionCount_5; }
	inline void set_collisionCount_5(int32_t value)
	{
		___collisionCount_5 = value;
	}
};

struct TutorialPointScript_t2361877884_StaticFields
{
public:
	// TutorialPointScript/TriggerTutorial TutorialPointScript::OnTriggerTutorialPoint
	TriggerTutorial_t2088781469 * ___OnTriggerTutorialPoint_2;
	// TutorialPointScript/TriggerTutorial TutorialPointScript::OnTriggerTutorialPassPoint
	TriggerTutorial_t2088781469 * ___OnTriggerTutorialPassPoint_3;

public:
	inline static int32_t get_offset_of_OnTriggerTutorialPoint_2() { return static_cast<int32_t>(offsetof(TutorialPointScript_t2361877884_StaticFields, ___OnTriggerTutorialPoint_2)); }
	inline TriggerTutorial_t2088781469 * get_OnTriggerTutorialPoint_2() const { return ___OnTriggerTutorialPoint_2; }
	inline TriggerTutorial_t2088781469 ** get_address_of_OnTriggerTutorialPoint_2() { return &___OnTriggerTutorialPoint_2; }
	inline void set_OnTriggerTutorialPoint_2(TriggerTutorial_t2088781469 * value)
	{
		___OnTriggerTutorialPoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnTriggerTutorialPoint_2), value);
	}

	inline static int32_t get_offset_of_OnTriggerTutorialPassPoint_3() { return static_cast<int32_t>(offsetof(TutorialPointScript_t2361877884_StaticFields, ___OnTriggerTutorialPassPoint_3)); }
	inline TriggerTutorial_t2088781469 * get_OnTriggerTutorialPassPoint_3() const { return ___OnTriggerTutorialPassPoint_3; }
	inline TriggerTutorial_t2088781469 ** get_address_of_OnTriggerTutorialPassPoint_3() { return &___OnTriggerTutorialPassPoint_3; }
	inline void set_OnTriggerTutorialPassPoint_3(TriggerTutorial_t2088781469 * value)
	{
		___OnTriggerTutorialPassPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnTriggerTutorialPassPoint_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALPOINTSCRIPT_T2361877884_H
#ifndef BACKSCALESCRIPT_T207004863_H
#define BACKSCALESCRIPT_T207004863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackScaleScript
struct  BackScaleScript_t207004863  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera BackScaleScript::renderCamera
	Camera_t4157153871 * ___renderCamera_2;

public:
	inline static int32_t get_offset_of_renderCamera_2() { return static_cast<int32_t>(offsetof(BackScaleScript_t207004863, ___renderCamera_2)); }
	inline Camera_t4157153871 * get_renderCamera_2() const { return ___renderCamera_2; }
	inline Camera_t4157153871 ** get_address_of_renderCamera_2() { return &___renderCamera_2; }
	inline void set_renderCamera_2(Camera_t4157153871 * value)
	{
		___renderCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___renderCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKSCALESCRIPT_T207004863_H
#ifndef DYNAMICBACK_T1454327095_H
#define DYNAMICBACK_T1454327095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicBack
struct  DynamicBack_t1454327095  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DynamicBack::_childObject
	GameObject_t1113636619 * ____childObject_3;
	// UISprite DynamicBack::_sprite
	UISprite_t194114938 * ____sprite_4;
	// TweenPosition DynamicBack::_tweenPosition
	TweenPosition_t1378762002 * ____tweenPosition_5;
	// TweenAlpha DynamicBack::_tweenAlpha
	TweenAlpha_t3706845226 * ____tweenAlpha_6;
	// TweenScale DynamicBack::_tweenScale
	TweenScale_t2539309033 * ____tweenScale_7;
	// TweenColor DynamicBack::_tweenColor
	TweenColor_t2112002648 * ____tweenColor_8;
	// System.Boolean DynamicBack::_isLeft
	bool ____isLeft_9;
	// System.Single DynamicBack::_delay
	float ____delay_10;

public:
	inline static int32_t get_offset_of__childObject_3() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____childObject_3)); }
	inline GameObject_t1113636619 * get__childObject_3() const { return ____childObject_3; }
	inline GameObject_t1113636619 ** get_address_of__childObject_3() { return &____childObject_3; }
	inline void set__childObject_3(GameObject_t1113636619 * value)
	{
		____childObject_3 = value;
		Il2CppCodeGenWriteBarrier((&____childObject_3), value);
	}

	inline static int32_t get_offset_of__sprite_4() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____sprite_4)); }
	inline UISprite_t194114938 * get__sprite_4() const { return ____sprite_4; }
	inline UISprite_t194114938 ** get_address_of__sprite_4() { return &____sprite_4; }
	inline void set__sprite_4(UISprite_t194114938 * value)
	{
		____sprite_4 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_4), value);
	}

	inline static int32_t get_offset_of__tweenPosition_5() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____tweenPosition_5)); }
	inline TweenPosition_t1378762002 * get__tweenPosition_5() const { return ____tweenPosition_5; }
	inline TweenPosition_t1378762002 ** get_address_of__tweenPosition_5() { return &____tweenPosition_5; }
	inline void set__tweenPosition_5(TweenPosition_t1378762002 * value)
	{
		____tweenPosition_5 = value;
		Il2CppCodeGenWriteBarrier((&____tweenPosition_5), value);
	}

	inline static int32_t get_offset_of__tweenAlpha_6() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____tweenAlpha_6)); }
	inline TweenAlpha_t3706845226 * get__tweenAlpha_6() const { return ____tweenAlpha_6; }
	inline TweenAlpha_t3706845226 ** get_address_of__tweenAlpha_6() { return &____tweenAlpha_6; }
	inline void set__tweenAlpha_6(TweenAlpha_t3706845226 * value)
	{
		____tweenAlpha_6 = value;
		Il2CppCodeGenWriteBarrier((&____tweenAlpha_6), value);
	}

	inline static int32_t get_offset_of__tweenScale_7() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____tweenScale_7)); }
	inline TweenScale_t2539309033 * get__tweenScale_7() const { return ____tweenScale_7; }
	inline TweenScale_t2539309033 ** get_address_of__tweenScale_7() { return &____tweenScale_7; }
	inline void set__tweenScale_7(TweenScale_t2539309033 * value)
	{
		____tweenScale_7 = value;
		Il2CppCodeGenWriteBarrier((&____tweenScale_7), value);
	}

	inline static int32_t get_offset_of__tweenColor_8() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____tweenColor_8)); }
	inline TweenColor_t2112002648 * get__tweenColor_8() const { return ____tweenColor_8; }
	inline TweenColor_t2112002648 ** get_address_of__tweenColor_8() { return &____tweenColor_8; }
	inline void set__tweenColor_8(TweenColor_t2112002648 * value)
	{
		____tweenColor_8 = value;
		Il2CppCodeGenWriteBarrier((&____tweenColor_8), value);
	}

	inline static int32_t get_offset_of__isLeft_9() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____isLeft_9)); }
	inline bool get__isLeft_9() const { return ____isLeft_9; }
	inline bool* get_address_of__isLeft_9() { return &____isLeft_9; }
	inline void set__isLeft_9(bool value)
	{
		____isLeft_9 = value;
	}

	inline static int32_t get_offset_of__delay_10() { return static_cast<int32_t>(offsetof(DynamicBack_t1454327095, ____delay_10)); }
	inline float get__delay_10() const { return ____delay_10; }
	inline float* get_address_of__delay_10() { return &____delay_10; }
	inline void set__delay_10(float value)
	{
		____delay_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICBACK_T1454327095_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef MONOBEHAVIOURWITHINIT_T1117120792_H
#define MONOBEHAVIOURWITHINIT_T1117120792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoBehaviourWithInit
struct  MonoBehaviourWithInit_t1117120792  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MonoBehaviourWithInit::_isInitialized
	bool ____isInitialized_2;

public:
	inline static int32_t get_offset_of__isInitialized_2() { return static_cast<int32_t>(offsetof(MonoBehaviourWithInit_t1117120792, ____isInitialized_2)); }
	inline bool get__isInitialized_2() const { return ____isInitialized_2; }
	inline bool* get_address_of__isInitialized_2() { return &____isInitialized_2; }
	inline void set__isInitialized_2(bool value)
	{
		____isInitialized_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURWITHINIT_T1117120792_H
#ifndef CUBESCRIPT_T387240894_H
#define CUBESCRIPT_T387240894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeScript
struct  CubeScript_t387240894  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CubeScript::maxFadeDistance
	float ___maxFadeDistance_2;
	// System.Single CubeScript::minFadeDistance
	float ___minFadeDistance_3;
	// UnityEngine.Rigidbody CubeScript::rigid
	Rigidbody_t3916780224 * ___rigid_4;
	// System.Single CubeScript::alpha
	float ___alpha_5;
	// System.Boolean CubeScript::isFade
	bool ___isFade_6;
	// UnityEngine.Renderer CubeScript::_renderer
	Renderer_t2627027031 * ____renderer_7;

public:
	inline static int32_t get_offset_of_maxFadeDistance_2() { return static_cast<int32_t>(offsetof(CubeScript_t387240894, ___maxFadeDistance_2)); }
	inline float get_maxFadeDistance_2() const { return ___maxFadeDistance_2; }
	inline float* get_address_of_maxFadeDistance_2() { return &___maxFadeDistance_2; }
	inline void set_maxFadeDistance_2(float value)
	{
		___maxFadeDistance_2 = value;
	}

	inline static int32_t get_offset_of_minFadeDistance_3() { return static_cast<int32_t>(offsetof(CubeScript_t387240894, ___minFadeDistance_3)); }
	inline float get_minFadeDistance_3() const { return ___minFadeDistance_3; }
	inline float* get_address_of_minFadeDistance_3() { return &___minFadeDistance_3; }
	inline void set_minFadeDistance_3(float value)
	{
		___minFadeDistance_3 = value;
	}

	inline static int32_t get_offset_of_rigid_4() { return static_cast<int32_t>(offsetof(CubeScript_t387240894, ___rigid_4)); }
	inline Rigidbody_t3916780224 * get_rigid_4() const { return ___rigid_4; }
	inline Rigidbody_t3916780224 ** get_address_of_rigid_4() { return &___rigid_4; }
	inline void set_rigid_4(Rigidbody_t3916780224 * value)
	{
		___rigid_4 = value;
		Il2CppCodeGenWriteBarrier((&___rigid_4), value);
	}

	inline static int32_t get_offset_of_alpha_5() { return static_cast<int32_t>(offsetof(CubeScript_t387240894, ___alpha_5)); }
	inline float get_alpha_5() const { return ___alpha_5; }
	inline float* get_address_of_alpha_5() { return &___alpha_5; }
	inline void set_alpha_5(float value)
	{
		___alpha_5 = value;
	}

	inline static int32_t get_offset_of_isFade_6() { return static_cast<int32_t>(offsetof(CubeScript_t387240894, ___isFade_6)); }
	inline bool get_isFade_6() const { return ___isFade_6; }
	inline bool* get_address_of_isFade_6() { return &___isFade_6; }
	inline void set_isFade_6(bool value)
	{
		___isFade_6 = value;
	}

	inline static int32_t get_offset_of__renderer_7() { return static_cast<int32_t>(offsetof(CubeScript_t387240894, ____renderer_7)); }
	inline Renderer_t2627027031 * get__renderer_7() const { return ____renderer_7; }
	inline Renderer_t2627027031 ** get_address_of__renderer_7() { return &____renderer_7; }
	inline void set__renderer_7(Renderer_t2627027031 * value)
	{
		____renderer_7 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBESCRIPT_T387240894_H
#ifndef UIBACKSCRIPT_T2417306190_H
#define UIBACKSCRIPT_T2417306190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBackScript
struct  UIBackScript_t2417306190  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIBackScript::mountainPrefab
	GameObject_t1113636619 * ___mountainPrefab_2;
	// System.Int32 UIBackScript::NumOfMountain
	int32_t ___NumOfMountain_3;
	// UnityEngine.GameObject UIBackScript::cloud1
	GameObject_t1113636619 * ___cloud1_4;
	// UnityEngine.GameObject UIBackScript::cloud2
	GameObject_t1113636619 * ___cloud2_5;
	// System.Single UIBackScript::timeCount
	float ___timeCount_6;
	// System.Collections.Generic.List`1<DynamicBack> UIBackScript::listDynamicBacks
	List_1_t2926401837 * ___listDynamicBacks_7;

public:
	inline static int32_t get_offset_of_mountainPrefab_2() { return static_cast<int32_t>(offsetof(UIBackScript_t2417306190, ___mountainPrefab_2)); }
	inline GameObject_t1113636619 * get_mountainPrefab_2() const { return ___mountainPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_mountainPrefab_2() { return &___mountainPrefab_2; }
	inline void set_mountainPrefab_2(GameObject_t1113636619 * value)
	{
		___mountainPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___mountainPrefab_2), value);
	}

	inline static int32_t get_offset_of_NumOfMountain_3() { return static_cast<int32_t>(offsetof(UIBackScript_t2417306190, ___NumOfMountain_3)); }
	inline int32_t get_NumOfMountain_3() const { return ___NumOfMountain_3; }
	inline int32_t* get_address_of_NumOfMountain_3() { return &___NumOfMountain_3; }
	inline void set_NumOfMountain_3(int32_t value)
	{
		___NumOfMountain_3 = value;
	}

	inline static int32_t get_offset_of_cloud1_4() { return static_cast<int32_t>(offsetof(UIBackScript_t2417306190, ___cloud1_4)); }
	inline GameObject_t1113636619 * get_cloud1_4() const { return ___cloud1_4; }
	inline GameObject_t1113636619 ** get_address_of_cloud1_4() { return &___cloud1_4; }
	inline void set_cloud1_4(GameObject_t1113636619 * value)
	{
		___cloud1_4 = value;
		Il2CppCodeGenWriteBarrier((&___cloud1_4), value);
	}

	inline static int32_t get_offset_of_cloud2_5() { return static_cast<int32_t>(offsetof(UIBackScript_t2417306190, ___cloud2_5)); }
	inline GameObject_t1113636619 * get_cloud2_5() const { return ___cloud2_5; }
	inline GameObject_t1113636619 ** get_address_of_cloud2_5() { return &___cloud2_5; }
	inline void set_cloud2_5(GameObject_t1113636619 * value)
	{
		___cloud2_5 = value;
		Il2CppCodeGenWriteBarrier((&___cloud2_5), value);
	}

	inline static int32_t get_offset_of_timeCount_6() { return static_cast<int32_t>(offsetof(UIBackScript_t2417306190, ___timeCount_6)); }
	inline float get_timeCount_6() const { return ___timeCount_6; }
	inline float* get_address_of_timeCount_6() { return &___timeCount_6; }
	inline void set_timeCount_6(float value)
	{
		___timeCount_6 = value;
	}

	inline static int32_t get_offset_of_listDynamicBacks_7() { return static_cast<int32_t>(offsetof(UIBackScript_t2417306190, ___listDynamicBacks_7)); }
	inline List_1_t2926401837 * get_listDynamicBacks_7() const { return ___listDynamicBacks_7; }
	inline List_1_t2926401837 ** get_address_of_listDynamicBacks_7() { return &___listDynamicBacks_7; }
	inline void set_listDynamicBacks_7(List_1_t2926401837 * value)
	{
		___listDynamicBacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___listDynamicBacks_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBACKSCRIPT_T2417306190_H
#ifndef CLOUDSCRIPT_T436153671_H
#define CLOUDSCRIPT_T436153671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudScript
struct  CloudScript_t436153671  : public MonoBehaviour_t3962482529
{
public:
	// UISprite CloudScript::sprite
	UISprite_t194114938 * ___sprite_2;

public:
	inline static int32_t get_offset_of_sprite_2() { return static_cast<int32_t>(offsetof(CloudScript_t436153671, ___sprite_2)); }
	inline UISprite_t194114938 * get_sprite_2() const { return ___sprite_2; }
	inline UISprite_t194114938 ** get_address_of_sprite_2() { return &___sprite_2; }
	inline void set_sprite_2(UISprite_t194114938 * value)
	{
		___sprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDSCRIPT_T436153671_H
#ifndef BALLROTATESCRIPT_T2206315450_H
#define BALLROTATESCRIPT_T2206315450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallRotateScript
struct  BallRotateScript_t2206315450  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLROTATESCRIPT_T2206315450_H
#ifndef GAMEPLAYSCRIPT_T1203922991_H
#define GAMEPLAYSCRIPT_T1203922991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlayScript
struct  GamePlayScript_t1203922991  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GamePlayScript::spawnPointCube
	GameObject_t1113636619 * ___spawnPointCube_3;
	// SpawnPointScript GamePlayScript::spawnPointScript
	SpawnPointScript_t711038611 * ___spawnPointScript_4;
	// System.Single GamePlayScript::spawnDistance
	float ___spawnDistance_5;
	// System.Single GamePlayScript::distanceCount
	float ___distanceCount_6;
	// System.Boolean GamePlayScript::isRandomSpawn
	bool ___isRandomSpawn_7;
	// System.Boolean GamePlayScript::isSpeedUp
	bool ___isSpeedUp_8;
	// System.Single GamePlayScript::nextScrollSpeed
	float ___nextScrollSpeed_9;
	// System.Single GamePlayScript::nextScrollSpeedDuration
	float ___nextScrollSpeedDuration_10;
	// System.Single GamePlayScript::maxDieCountDown
	float ___maxDieCountDown_11;
	// System.Boolean GamePlayScript::isOver
	bool ___isOver_12;
	// System.Boolean GamePlayScript::isDelayOver
	bool ___isDelayOver_13;
	// UnityEngine.Transform GamePlayScript::camTransform
	Transform_t3600365921 * ___camTransform_14;
	// UnityEngine.Vector3 GamePlayScript::camLocalPos
	Vector3_t3722313464  ___camLocalPos_15;
	// System.Single GamePlayScript::scrollSpeed
	float ___scrollSpeed_16;
	// System.Collections.Generic.List`1<SpeedBalance> GamePlayScript::speedBalance
	List_1_t2150885654 * ___speedBalance_17;
	// System.Collections.Generic.List`1<SpawnDistance> GamePlayScript::spawnDistanceData
	List_1_t2900403471 * ___spawnDistanceData_18;

public:
	inline static int32_t get_offset_of_spawnPointCube_3() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___spawnPointCube_3)); }
	inline GameObject_t1113636619 * get_spawnPointCube_3() const { return ___spawnPointCube_3; }
	inline GameObject_t1113636619 ** get_address_of_spawnPointCube_3() { return &___spawnPointCube_3; }
	inline void set_spawnPointCube_3(GameObject_t1113636619 * value)
	{
		___spawnPointCube_3 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPointCube_3), value);
	}

	inline static int32_t get_offset_of_spawnPointScript_4() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___spawnPointScript_4)); }
	inline SpawnPointScript_t711038611 * get_spawnPointScript_4() const { return ___spawnPointScript_4; }
	inline SpawnPointScript_t711038611 ** get_address_of_spawnPointScript_4() { return &___spawnPointScript_4; }
	inline void set_spawnPointScript_4(SpawnPointScript_t711038611 * value)
	{
		___spawnPointScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPointScript_4), value);
	}

	inline static int32_t get_offset_of_spawnDistance_5() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___spawnDistance_5)); }
	inline float get_spawnDistance_5() const { return ___spawnDistance_5; }
	inline float* get_address_of_spawnDistance_5() { return &___spawnDistance_5; }
	inline void set_spawnDistance_5(float value)
	{
		___spawnDistance_5 = value;
	}

	inline static int32_t get_offset_of_distanceCount_6() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___distanceCount_6)); }
	inline float get_distanceCount_6() const { return ___distanceCount_6; }
	inline float* get_address_of_distanceCount_6() { return &___distanceCount_6; }
	inline void set_distanceCount_6(float value)
	{
		___distanceCount_6 = value;
	}

	inline static int32_t get_offset_of_isRandomSpawn_7() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___isRandomSpawn_7)); }
	inline bool get_isRandomSpawn_7() const { return ___isRandomSpawn_7; }
	inline bool* get_address_of_isRandomSpawn_7() { return &___isRandomSpawn_7; }
	inline void set_isRandomSpawn_7(bool value)
	{
		___isRandomSpawn_7 = value;
	}

	inline static int32_t get_offset_of_isSpeedUp_8() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___isSpeedUp_8)); }
	inline bool get_isSpeedUp_8() const { return ___isSpeedUp_8; }
	inline bool* get_address_of_isSpeedUp_8() { return &___isSpeedUp_8; }
	inline void set_isSpeedUp_8(bool value)
	{
		___isSpeedUp_8 = value;
	}

	inline static int32_t get_offset_of_nextScrollSpeed_9() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___nextScrollSpeed_9)); }
	inline float get_nextScrollSpeed_9() const { return ___nextScrollSpeed_9; }
	inline float* get_address_of_nextScrollSpeed_9() { return &___nextScrollSpeed_9; }
	inline void set_nextScrollSpeed_9(float value)
	{
		___nextScrollSpeed_9 = value;
	}

	inline static int32_t get_offset_of_nextScrollSpeedDuration_10() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___nextScrollSpeedDuration_10)); }
	inline float get_nextScrollSpeedDuration_10() const { return ___nextScrollSpeedDuration_10; }
	inline float* get_address_of_nextScrollSpeedDuration_10() { return &___nextScrollSpeedDuration_10; }
	inline void set_nextScrollSpeedDuration_10(float value)
	{
		___nextScrollSpeedDuration_10 = value;
	}

	inline static int32_t get_offset_of_maxDieCountDown_11() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___maxDieCountDown_11)); }
	inline float get_maxDieCountDown_11() const { return ___maxDieCountDown_11; }
	inline float* get_address_of_maxDieCountDown_11() { return &___maxDieCountDown_11; }
	inline void set_maxDieCountDown_11(float value)
	{
		___maxDieCountDown_11 = value;
	}

	inline static int32_t get_offset_of_isOver_12() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___isOver_12)); }
	inline bool get_isOver_12() const { return ___isOver_12; }
	inline bool* get_address_of_isOver_12() { return &___isOver_12; }
	inline void set_isOver_12(bool value)
	{
		___isOver_12 = value;
	}

	inline static int32_t get_offset_of_isDelayOver_13() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___isDelayOver_13)); }
	inline bool get_isDelayOver_13() const { return ___isDelayOver_13; }
	inline bool* get_address_of_isDelayOver_13() { return &___isDelayOver_13; }
	inline void set_isDelayOver_13(bool value)
	{
		___isDelayOver_13 = value;
	}

	inline static int32_t get_offset_of_camTransform_14() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___camTransform_14)); }
	inline Transform_t3600365921 * get_camTransform_14() const { return ___camTransform_14; }
	inline Transform_t3600365921 ** get_address_of_camTransform_14() { return &___camTransform_14; }
	inline void set_camTransform_14(Transform_t3600365921 * value)
	{
		___camTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___camTransform_14), value);
	}

	inline static int32_t get_offset_of_camLocalPos_15() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___camLocalPos_15)); }
	inline Vector3_t3722313464  get_camLocalPos_15() const { return ___camLocalPos_15; }
	inline Vector3_t3722313464 * get_address_of_camLocalPos_15() { return &___camLocalPos_15; }
	inline void set_camLocalPos_15(Vector3_t3722313464  value)
	{
		___camLocalPos_15 = value;
	}

	inline static int32_t get_offset_of_scrollSpeed_16() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___scrollSpeed_16)); }
	inline float get_scrollSpeed_16() const { return ___scrollSpeed_16; }
	inline float* get_address_of_scrollSpeed_16() { return &___scrollSpeed_16; }
	inline void set_scrollSpeed_16(float value)
	{
		___scrollSpeed_16 = value;
	}

	inline static int32_t get_offset_of_speedBalance_17() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___speedBalance_17)); }
	inline List_1_t2150885654 * get_speedBalance_17() const { return ___speedBalance_17; }
	inline List_1_t2150885654 ** get_address_of_speedBalance_17() { return &___speedBalance_17; }
	inline void set_speedBalance_17(List_1_t2150885654 * value)
	{
		___speedBalance_17 = value;
		Il2CppCodeGenWriteBarrier((&___speedBalance_17), value);
	}

	inline static int32_t get_offset_of_spawnDistanceData_18() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991, ___spawnDistanceData_18)); }
	inline List_1_t2900403471 * get_spawnDistanceData_18() const { return ___spawnDistanceData_18; }
	inline List_1_t2900403471 ** get_address_of_spawnDistanceData_18() { return &___spawnDistanceData_18; }
	inline void set_spawnDistanceData_18(List_1_t2900403471 * value)
	{
		___spawnDistanceData_18 = value;
		Il2CppCodeGenWriteBarrier((&___spawnDistanceData_18), value);
	}
};

struct GamePlayScript_t1203922991_StaticFields
{
public:
	// GamePlayScript GamePlayScript::instance
	GamePlayScript_t1203922991 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GamePlayScript_t1203922991_StaticFields, ___instance_2)); }
	inline GamePlayScript_t1203922991 * get_instance_2() const { return ___instance_2; }
	inline GamePlayScript_t1203922991 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GamePlayScript_t1203922991 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPLAYSCRIPT_T1203922991_H
#ifndef TUTORIALMANAGER_T3418541267_H
#define TUTORIALMANAGER_T3418541267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager
struct  TutorialManager_t3418541267  : public MonoBehaviour_t3962482529
{
public:
	// GamePlayScript TutorialManager::_gamePlayScript
	GamePlayScript_t1203922991 * ____gamePlayScript_3;
	// UnityEngine.GameObject TutorialManager::playerLeft
	GameObject_t1113636619 * ___playerLeft_4;
	// UnityEngine.GameObject TutorialManager::playerRight
	GameObject_t1113636619 * ___playerRight_5;
	// PlayerScript TutorialManager::_pLeft
	PlayerScript_t1783516946 * ____pLeft_6;
	// PlayerScript TutorialManager::_pRight
	PlayerScript_t1783516946 * ____pRight_7;
	// UnityEngine.GameObject TutorialManager::fingerLeft
	GameObject_t1113636619 * ___fingerLeft_8;
	// UnityEngine.GameObject TutorialManager::fingerRight
	GameObject_t1113636619 * ___fingerRight_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> TutorialManager::tutTexts
	List_1_t2585711361 * ___tutTexts_10;
	// System.Collections.Generic.List`1<TutorialSpawnPoints> TutorialManager::tutorialSpawnPoints
	List_1_t2820345059 * ___tutorialSpawnPoints_11;
	// System.Int32 TutorialManager::tutorialMaxRowsCube
	int32_t ___tutorialMaxRowsCube_12;
	// TutorialManager/TutorialState TutorialManager::state
	int32_t ___state_13;
	// System.Boolean TutorialManager::movingLeft
	bool ___movingLeft_14;
	// System.Boolean TutorialManager::movingRight
	bool ___movingRight_15;
	// System.Boolean TutorialManager::isDelay
	bool ___isDelay_16;
	// System.Single TutorialManager::_timeDelay
	float ____timeDelay_17;
	// System.Int32 TutorialManager::tapCount
	int32_t ___tapCount_18;
	// System.Single TutorialManager::lastTimeScale
	float ___lastTimeScale_19;
	// System.Boolean TutorialManager::isPause
	bool ___isPause_20;
	// System.Boolean TutorialManager::demoPlay
	bool ___demoPlay_21;
	// System.Boolean TutorialManager::isPass
	bool ___isPass_22;

public:
	inline static int32_t get_offset_of__gamePlayScript_3() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ____gamePlayScript_3)); }
	inline GamePlayScript_t1203922991 * get__gamePlayScript_3() const { return ____gamePlayScript_3; }
	inline GamePlayScript_t1203922991 ** get_address_of__gamePlayScript_3() { return &____gamePlayScript_3; }
	inline void set__gamePlayScript_3(GamePlayScript_t1203922991 * value)
	{
		____gamePlayScript_3 = value;
		Il2CppCodeGenWriteBarrier((&____gamePlayScript_3), value);
	}

	inline static int32_t get_offset_of_playerLeft_4() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___playerLeft_4)); }
	inline GameObject_t1113636619 * get_playerLeft_4() const { return ___playerLeft_4; }
	inline GameObject_t1113636619 ** get_address_of_playerLeft_4() { return &___playerLeft_4; }
	inline void set_playerLeft_4(GameObject_t1113636619 * value)
	{
		___playerLeft_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerLeft_4), value);
	}

	inline static int32_t get_offset_of_playerRight_5() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___playerRight_5)); }
	inline GameObject_t1113636619 * get_playerRight_5() const { return ___playerRight_5; }
	inline GameObject_t1113636619 ** get_address_of_playerRight_5() { return &___playerRight_5; }
	inline void set_playerRight_5(GameObject_t1113636619 * value)
	{
		___playerRight_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerRight_5), value);
	}

	inline static int32_t get_offset_of__pLeft_6() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ____pLeft_6)); }
	inline PlayerScript_t1783516946 * get__pLeft_6() const { return ____pLeft_6; }
	inline PlayerScript_t1783516946 ** get_address_of__pLeft_6() { return &____pLeft_6; }
	inline void set__pLeft_6(PlayerScript_t1783516946 * value)
	{
		____pLeft_6 = value;
		Il2CppCodeGenWriteBarrier((&____pLeft_6), value);
	}

	inline static int32_t get_offset_of__pRight_7() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ____pRight_7)); }
	inline PlayerScript_t1783516946 * get__pRight_7() const { return ____pRight_7; }
	inline PlayerScript_t1783516946 ** get_address_of__pRight_7() { return &____pRight_7; }
	inline void set__pRight_7(PlayerScript_t1783516946 * value)
	{
		____pRight_7 = value;
		Il2CppCodeGenWriteBarrier((&____pRight_7), value);
	}

	inline static int32_t get_offset_of_fingerLeft_8() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___fingerLeft_8)); }
	inline GameObject_t1113636619 * get_fingerLeft_8() const { return ___fingerLeft_8; }
	inline GameObject_t1113636619 ** get_address_of_fingerLeft_8() { return &___fingerLeft_8; }
	inline void set_fingerLeft_8(GameObject_t1113636619 * value)
	{
		___fingerLeft_8 = value;
		Il2CppCodeGenWriteBarrier((&___fingerLeft_8), value);
	}

	inline static int32_t get_offset_of_fingerRight_9() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___fingerRight_9)); }
	inline GameObject_t1113636619 * get_fingerRight_9() const { return ___fingerRight_9; }
	inline GameObject_t1113636619 ** get_address_of_fingerRight_9() { return &___fingerRight_9; }
	inline void set_fingerRight_9(GameObject_t1113636619 * value)
	{
		___fingerRight_9 = value;
		Il2CppCodeGenWriteBarrier((&___fingerRight_9), value);
	}

	inline static int32_t get_offset_of_tutTexts_10() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___tutTexts_10)); }
	inline List_1_t2585711361 * get_tutTexts_10() const { return ___tutTexts_10; }
	inline List_1_t2585711361 ** get_address_of_tutTexts_10() { return &___tutTexts_10; }
	inline void set_tutTexts_10(List_1_t2585711361 * value)
	{
		___tutTexts_10 = value;
		Il2CppCodeGenWriteBarrier((&___tutTexts_10), value);
	}

	inline static int32_t get_offset_of_tutorialSpawnPoints_11() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___tutorialSpawnPoints_11)); }
	inline List_1_t2820345059 * get_tutorialSpawnPoints_11() const { return ___tutorialSpawnPoints_11; }
	inline List_1_t2820345059 ** get_address_of_tutorialSpawnPoints_11() { return &___tutorialSpawnPoints_11; }
	inline void set_tutorialSpawnPoints_11(List_1_t2820345059 * value)
	{
		___tutorialSpawnPoints_11 = value;
		Il2CppCodeGenWriteBarrier((&___tutorialSpawnPoints_11), value);
	}

	inline static int32_t get_offset_of_tutorialMaxRowsCube_12() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___tutorialMaxRowsCube_12)); }
	inline int32_t get_tutorialMaxRowsCube_12() const { return ___tutorialMaxRowsCube_12; }
	inline int32_t* get_address_of_tutorialMaxRowsCube_12() { return &___tutorialMaxRowsCube_12; }
	inline void set_tutorialMaxRowsCube_12(int32_t value)
	{
		___tutorialMaxRowsCube_12 = value;
	}

	inline static int32_t get_offset_of_state_13() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___state_13)); }
	inline int32_t get_state_13() const { return ___state_13; }
	inline int32_t* get_address_of_state_13() { return &___state_13; }
	inline void set_state_13(int32_t value)
	{
		___state_13 = value;
	}

	inline static int32_t get_offset_of_movingLeft_14() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___movingLeft_14)); }
	inline bool get_movingLeft_14() const { return ___movingLeft_14; }
	inline bool* get_address_of_movingLeft_14() { return &___movingLeft_14; }
	inline void set_movingLeft_14(bool value)
	{
		___movingLeft_14 = value;
	}

	inline static int32_t get_offset_of_movingRight_15() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___movingRight_15)); }
	inline bool get_movingRight_15() const { return ___movingRight_15; }
	inline bool* get_address_of_movingRight_15() { return &___movingRight_15; }
	inline void set_movingRight_15(bool value)
	{
		___movingRight_15 = value;
	}

	inline static int32_t get_offset_of_isDelay_16() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___isDelay_16)); }
	inline bool get_isDelay_16() const { return ___isDelay_16; }
	inline bool* get_address_of_isDelay_16() { return &___isDelay_16; }
	inline void set_isDelay_16(bool value)
	{
		___isDelay_16 = value;
	}

	inline static int32_t get_offset_of__timeDelay_17() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ____timeDelay_17)); }
	inline float get__timeDelay_17() const { return ____timeDelay_17; }
	inline float* get_address_of__timeDelay_17() { return &____timeDelay_17; }
	inline void set__timeDelay_17(float value)
	{
		____timeDelay_17 = value;
	}

	inline static int32_t get_offset_of_tapCount_18() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___tapCount_18)); }
	inline int32_t get_tapCount_18() const { return ___tapCount_18; }
	inline int32_t* get_address_of_tapCount_18() { return &___tapCount_18; }
	inline void set_tapCount_18(int32_t value)
	{
		___tapCount_18 = value;
	}

	inline static int32_t get_offset_of_lastTimeScale_19() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___lastTimeScale_19)); }
	inline float get_lastTimeScale_19() const { return ___lastTimeScale_19; }
	inline float* get_address_of_lastTimeScale_19() { return &___lastTimeScale_19; }
	inline void set_lastTimeScale_19(float value)
	{
		___lastTimeScale_19 = value;
	}

	inline static int32_t get_offset_of_isPause_20() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___isPause_20)); }
	inline bool get_isPause_20() const { return ___isPause_20; }
	inline bool* get_address_of_isPause_20() { return &___isPause_20; }
	inline void set_isPause_20(bool value)
	{
		___isPause_20 = value;
	}

	inline static int32_t get_offset_of_demoPlay_21() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___demoPlay_21)); }
	inline bool get_demoPlay_21() const { return ___demoPlay_21; }
	inline bool* get_address_of_demoPlay_21() { return &___demoPlay_21; }
	inline void set_demoPlay_21(bool value)
	{
		___demoPlay_21 = value;
	}

	inline static int32_t get_offset_of_isPass_22() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___isPass_22)); }
	inline bool get_isPass_22() const { return ___isPass_22; }
	inline bool* get_address_of_isPass_22() { return &___isPass_22; }
	inline void set_isPass_22(bool value)
	{
		___isPass_22 = value;
	}
};

struct TutorialManager_t3418541267_StaticFields
{
public:
	// TutorialManager TutorialManager::instance
	TutorialManager_t3418541267 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267_StaticFields, ___instance_2)); }
	inline TutorialManager_t3418541267 * get_instance_2() const { return ___instance_2; }
	inline TutorialManager_t3418541267 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TutorialManager_t3418541267 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALMANAGER_T3418541267_H
#ifndef SPAWNPOINTSCRIPT_T711038611_H
#define SPAWNPOINTSCRIPT_T711038611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpawnPointScript
struct  SpawnPointScript_t711038611  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SpawnPointScript::cubePrefab
	GameObject_t1113636619 * ___cubePrefab_2;
	// System.Collections.Generic.List`1<CubeScript> SpawnPointScript::activeCubes
	List_1_t1859315636 * ___activeCubes_3;
	// System.Collections.Generic.List`1<CubeScript> SpawnPointScript::inActiveCubes
	List_1_t1859315636 * ___inActiveCubes_4;
	// System.Int32 SpawnPointScript::inactiveCount
	int32_t ___inactiveCount_5;
	// System.Int32 SpawnPointScript::lastLeft
	int32_t ___lastLeft_6;
	// System.Int32 SpawnPointScript::lastRight
	int32_t ___lastRight_7;
	// System.Int32 SpawnPointScript::SamelineCount
	int32_t ___SamelineCount_8;

public:
	inline static int32_t get_offset_of_cubePrefab_2() { return static_cast<int32_t>(offsetof(SpawnPointScript_t711038611, ___cubePrefab_2)); }
	inline GameObject_t1113636619 * get_cubePrefab_2() const { return ___cubePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_cubePrefab_2() { return &___cubePrefab_2; }
	inline void set_cubePrefab_2(GameObject_t1113636619 * value)
	{
		___cubePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___cubePrefab_2), value);
	}

	inline static int32_t get_offset_of_activeCubes_3() { return static_cast<int32_t>(offsetof(SpawnPointScript_t711038611, ___activeCubes_3)); }
	inline List_1_t1859315636 * get_activeCubes_3() const { return ___activeCubes_3; }
	inline List_1_t1859315636 ** get_address_of_activeCubes_3() { return &___activeCubes_3; }
	inline void set_activeCubes_3(List_1_t1859315636 * value)
	{
		___activeCubes_3 = value;
		Il2CppCodeGenWriteBarrier((&___activeCubes_3), value);
	}

	inline static int32_t get_offset_of_inActiveCubes_4() { return static_cast<int32_t>(offsetof(SpawnPointScript_t711038611, ___inActiveCubes_4)); }
	inline List_1_t1859315636 * get_inActiveCubes_4() const { return ___inActiveCubes_4; }
	inline List_1_t1859315636 ** get_address_of_inActiveCubes_4() { return &___inActiveCubes_4; }
	inline void set_inActiveCubes_4(List_1_t1859315636 * value)
	{
		___inActiveCubes_4 = value;
		Il2CppCodeGenWriteBarrier((&___inActiveCubes_4), value);
	}

	inline static int32_t get_offset_of_inactiveCount_5() { return static_cast<int32_t>(offsetof(SpawnPointScript_t711038611, ___inactiveCount_5)); }
	inline int32_t get_inactiveCount_5() const { return ___inactiveCount_5; }
	inline int32_t* get_address_of_inactiveCount_5() { return &___inactiveCount_5; }
	inline void set_inactiveCount_5(int32_t value)
	{
		___inactiveCount_5 = value;
	}

	inline static int32_t get_offset_of_lastLeft_6() { return static_cast<int32_t>(offsetof(SpawnPointScript_t711038611, ___lastLeft_6)); }
	inline int32_t get_lastLeft_6() const { return ___lastLeft_6; }
	inline int32_t* get_address_of_lastLeft_6() { return &___lastLeft_6; }
	inline void set_lastLeft_6(int32_t value)
	{
		___lastLeft_6 = value;
	}

	inline static int32_t get_offset_of_lastRight_7() { return static_cast<int32_t>(offsetof(SpawnPointScript_t711038611, ___lastRight_7)); }
	inline int32_t get_lastRight_7() const { return ___lastRight_7; }
	inline int32_t* get_address_of_lastRight_7() { return &___lastRight_7; }
	inline void set_lastRight_7(int32_t value)
	{
		___lastRight_7 = value;
	}

	inline static int32_t get_offset_of_SamelineCount_8() { return static_cast<int32_t>(offsetof(SpawnPointScript_t711038611, ___SamelineCount_8)); }
	inline int32_t get_SamelineCount_8() const { return ___SamelineCount_8; }
	inline int32_t* get_address_of_SamelineCount_8() { return &___SamelineCount_8; }
	inline void set_SamelineCount_8(int32_t value)
	{
		___SamelineCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNPOINTSCRIPT_T711038611_H
#ifndef HAZESCRIPT_T1605201346_H
#define HAZESCRIPT_T1605201346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HazeScript
struct  HazeScript_t1605201346  : public MonoBehaviour_t3962482529
{
public:
	// UISprite HazeScript::sprite
	UISprite_t194114938 * ___sprite_2;

public:
	inline static int32_t get_offset_of_sprite_2() { return static_cast<int32_t>(offsetof(HazeScript_t1605201346, ___sprite_2)); }
	inline UISprite_t194114938 * get_sprite_2() const { return ___sprite_2; }
	inline UISprite_t194114938 ** get_address_of_sprite_2() { return &___sprite_2; }
	inline void set_sprite_2(UISprite_t194114938 * value)
	{
		___sprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAZESCRIPT_T1605201346_H
#ifndef TESTUI_T3746935818_H
#define TESTUI_T3746935818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestUI
struct  TestUI_t3746935818  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIStyle TestUI::_labelStyle
	GUIStyle_t3956901511 * ____labelStyle_2;
	// UnityEngine.GUIStyle TestUI::_buttonStyle
	GUIStyle_t3956901511 * ____buttonStyle_3;

public:
	inline static int32_t get_offset_of__labelStyle_2() { return static_cast<int32_t>(offsetof(TestUI_t3746935818, ____labelStyle_2)); }
	inline GUIStyle_t3956901511 * get__labelStyle_2() const { return ____labelStyle_2; }
	inline GUIStyle_t3956901511 ** get_address_of__labelStyle_2() { return &____labelStyle_2; }
	inline void set__labelStyle_2(GUIStyle_t3956901511 * value)
	{
		____labelStyle_2 = value;
		Il2CppCodeGenWriteBarrier((&____labelStyle_2), value);
	}

	inline static int32_t get_offset_of__buttonStyle_3() { return static_cast<int32_t>(offsetof(TestUI_t3746935818, ____buttonStyle_3)); }
	inline GUIStyle_t3956901511 * get__buttonStyle_3() const { return ____buttonStyle_3; }
	inline GUIStyle_t3956901511 ** get_address_of__buttonStyle_3() { return &____buttonStyle_3; }
	inline void set__buttonStyle_3(GUIStyle_t3956901511 * value)
	{
		____buttonStyle_3 = value;
		Il2CppCodeGenWriteBarrier((&____buttonStyle_3), value);
	}
};

struct TestUI_t3746935818_StaticFields
{
public:
	// UnityEngine.Vector2 TestUI::SCREEN_SIZE
	Vector2_t2156229523  ___SCREEN_SIZE_4;

public:
	inline static int32_t get_offset_of_SCREEN_SIZE_4() { return static_cast<int32_t>(offsetof(TestUI_t3746935818_StaticFields, ___SCREEN_SIZE_4)); }
	inline Vector2_t2156229523  get_SCREEN_SIZE_4() const { return ___SCREEN_SIZE_4; }
	inline Vector2_t2156229523 * get_address_of_SCREEN_SIZE_4() { return &___SCREEN_SIZE_4; }
	inline void set_SCREEN_SIZE_4(Vector2_t2156229523  value)
	{
		___SCREEN_SIZE_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTUI_T3746935818_H
#ifndef FINGERBLINKSCRIPT_T4209160530_H
#define FINGERBLINKSCRIPT_T4209160530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingerBlinkScript
struct  FingerBlinkScript_t4209160530  : public MonoBehaviour_t3962482529
{
public:
	// UISprite FingerBlinkScript::canvas
	UISprite_t194114938 * ___canvas_2;
	// UnityEngine.Transform FingerBlinkScript::rectTransform
	Transform_t3600365921 * ___rectTransform_3;
	// System.Boolean FingerBlinkScript::isStart
	bool ___isStart_4;
	// System.Single FingerBlinkScript::t
	float ___t_5;
	// System.Boolean FingerBlinkScript::isScaleUp
	bool ___isScaleUp_6;
	// System.Single FingerBlinkScript::duration
	float ___duration_7;
	// System.Single FingerBlinkScript::minScale
	float ___minScale_8;
	// System.Single FingerBlinkScript::maxScale
	float ___maxScale_9;
	// System.Boolean FingerBlinkScript::_shouldUpdateScale
	bool ____shouldUpdateScale_10;

public:
	inline static int32_t get_offset_of_canvas_2() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___canvas_2)); }
	inline UISprite_t194114938 * get_canvas_2() const { return ___canvas_2; }
	inline UISprite_t194114938 ** get_address_of_canvas_2() { return &___canvas_2; }
	inline void set_canvas_2(UISprite_t194114938 * value)
	{
		___canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_2), value);
	}

	inline static int32_t get_offset_of_rectTransform_3() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___rectTransform_3)); }
	inline Transform_t3600365921 * get_rectTransform_3() const { return ___rectTransform_3; }
	inline Transform_t3600365921 ** get_address_of_rectTransform_3() { return &___rectTransform_3; }
	inline void set_rectTransform_3(Transform_t3600365921 * value)
	{
		___rectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_3), value);
	}

	inline static int32_t get_offset_of_isStart_4() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___isStart_4)); }
	inline bool get_isStart_4() const { return ___isStart_4; }
	inline bool* get_address_of_isStart_4() { return &___isStart_4; }
	inline void set_isStart_4(bool value)
	{
		___isStart_4 = value;
	}

	inline static int32_t get_offset_of_t_5() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___t_5)); }
	inline float get_t_5() const { return ___t_5; }
	inline float* get_address_of_t_5() { return &___t_5; }
	inline void set_t_5(float value)
	{
		___t_5 = value;
	}

	inline static int32_t get_offset_of_isScaleUp_6() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___isScaleUp_6)); }
	inline bool get_isScaleUp_6() const { return ___isScaleUp_6; }
	inline bool* get_address_of_isScaleUp_6() { return &___isScaleUp_6; }
	inline void set_isScaleUp_6(bool value)
	{
		___isScaleUp_6 = value;
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___duration_7)); }
	inline float get_duration_7() const { return ___duration_7; }
	inline float* get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(float value)
	{
		___duration_7 = value;
	}

	inline static int32_t get_offset_of_minScale_8() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___minScale_8)); }
	inline float get_minScale_8() const { return ___minScale_8; }
	inline float* get_address_of_minScale_8() { return &___minScale_8; }
	inline void set_minScale_8(float value)
	{
		___minScale_8 = value;
	}

	inline static int32_t get_offset_of_maxScale_9() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ___maxScale_9)); }
	inline float get_maxScale_9() const { return ___maxScale_9; }
	inline float* get_address_of_maxScale_9() { return &___maxScale_9; }
	inline void set_maxScale_9(float value)
	{
		___maxScale_9 = value;
	}

	inline static int32_t get_offset_of__shouldUpdateScale_10() { return static_cast<int32_t>(offsetof(FingerBlinkScript_t4209160530, ____shouldUpdateScale_10)); }
	inline bool get__shouldUpdateScale_10() const { return ____shouldUpdateScale_10; }
	inline bool* get_address_of__shouldUpdateScale_10() { return &____shouldUpdateScale_10; }
	inline void set__shouldUpdateScale_10(bool value)
	{
		____shouldUpdateScale_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERBLINKSCRIPT_T4209160530_H
#ifndef RECYCLINGOBJECT_T2513625666_H
#define RECYCLINGOBJECT_T2513625666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecyclingObject
struct  RecyclingObject_t2513625666  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECYCLINGOBJECT_T2513625666_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T3869086818_H
#define SINGLETONMONOBEHAVIOUR_1_T3869086818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<RankingManager>
struct  SingletonMonoBehaviour_1_t3869086818  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t3869086818_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	RankingManager_t1599285360 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t3869086818_StaticFields, ____instance_3)); }
	inline RankingManager_t1599285360 * get__instance_3() const { return ____instance_3; }
	inline RankingManager_t1599285360 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(RankingManager_t1599285360 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T3869086818_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T931138163_H
#define SINGLETONMONOBEHAVIOUR_1_T931138163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<SceneNavigator>
struct  SingletonMonoBehaviour_1_t931138163  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t931138163_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	SceneNavigator_t2956304001 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t931138163_StaticFields, ____instance_3)); }
	inline SceneNavigator_t2956304001 * get__instance_3() const { return ____instance_3; }
	inline SceneNavigator_t2956304001 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(SceneNavigator_t2956304001 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T931138163_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T3806325112_H
#define SINGLETONMONOBEHAVIOUR_1_T3806325112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<GameManager>
struct  SingletonMonoBehaviour_1_t3806325112  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t3806325112_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	GameManager_t1536523654 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t3806325112_StaticFields, ____instance_3)); }
	inline GameManager_t1536523654 * get__instance_3() const { return ____instance_3; }
	inline GameManager_t1536523654 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(GameManager_t1536523654 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T3806325112_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T385723532_H
#define SINGLETONMONOBEHAVIOUR_1_T385723532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<AdManager>
struct  SingletonMonoBehaviour_1_t385723532  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t385723532_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	AdManager_t2410889370 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t385723532_StaticFields, ____instance_3)); }
	inline AdManager_t2410889370 * get__instance_3() const { return ____instance_3; }
	inline AdManager_t2410889370 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(AdManager_t2410889370 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T385723532_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T2593837087_H
#define SINGLETONMONOBEHAVIOUR_1_T2593837087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<SEManager>
struct  SingletonMonoBehaviour_1_t2593837087  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t2593837087_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	SEManager_t324035629 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t2593837087_StaticFields, ____instance_3)); }
	inline SEManager_t324035629 * get__instance_3() const { return ____instance_3; }
	inline SEManager_t324035629 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(SEManager_t324035629 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T2593837087_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T981264159_H
#define SINGLETONMONOBEHAVIOUR_1_T981264159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<CommonNativeManager>
struct  SingletonMonoBehaviour_1_t981264159  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t981264159_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	CommonNativeManager_t3006429997 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t981264159_StaticFields, ____instance_3)); }
	inline CommonNativeManager_t3006429997 * get__instance_3() const { return ____instance_3; }
	inline CommonNativeManager_t3006429997 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(CommonNativeManager_t3006429997 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T981264159_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T4059843566_H
#define SINGLETONMONOBEHAVIOUR_1_T4059843566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<BGMManager>
struct  SingletonMonoBehaviour_1_t4059843566  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t4059843566_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	BGMManager_t1790042108 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t4059843566_StaticFields, ____instance_3)); }
	inline BGMManager_t1790042108 * get__instance_3() const { return ____instance_3; }
	inline BGMManager_t1790042108 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(BGMManager_t1790042108 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T4059843566_H
#ifndef FPSUI_T1916804684_H
#define FPSUI_T1916804684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSUI
struct  FPSUI_t1916804684  : public TestUI_t3746935818
{
public:
	// System.Collections.Generic.List`1<System.Single> FPSUI::_fpsList
	List_1_t2869341516 * ____fpsList_5;
	// System.Single FPSUI::_fps
	float ____fps_7;

public:
	inline static int32_t get_offset_of__fpsList_5() { return static_cast<int32_t>(offsetof(FPSUI_t1916804684, ____fpsList_5)); }
	inline List_1_t2869341516 * get__fpsList_5() const { return ____fpsList_5; }
	inline List_1_t2869341516 ** get_address_of__fpsList_5() { return &____fpsList_5; }
	inline void set__fpsList_5(List_1_t2869341516 * value)
	{
		____fpsList_5 = value;
		Il2CppCodeGenWriteBarrier((&____fpsList_5), value);
	}

	inline static int32_t get_offset_of__fps_7() { return static_cast<int32_t>(offsetof(FPSUI_t1916804684, ____fps_7)); }
	inline float get__fps_7() const { return ____fps_7; }
	inline float* get_address_of__fps_7() { return &____fps_7; }
	inline void set__fps_7(float value)
	{
		____fps_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSUI_T1916804684_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T2999011314_H
#define SINGLETONMONOBEHAVIOUR_1_T2999011314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<MainManager>
struct  SingletonMonoBehaviour_1_t2999011314  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t2999011314_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	MainManager_t729209856 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t2999011314_StaticFields, ____instance_3)); }
	inline MainManager_t729209856 * get__instance_3() const { return ____instance_3; }
	inline MainManager_t729209856 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(MainManager_t729209856 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T2999011314_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T431283327_H
#define SINGLETONMONOBEHAVIOUR_1_T431283327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<ShareManager>
struct  SingletonMonoBehaviour_1_t431283327  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t431283327_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	ShareManager_t2456449165 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t431283327_StaticFields, ____instance_3)); }
	inline ShareManager_t2456449165 * get__instance_3() const { return ____instance_3; }
	inline ShareManager_t2456449165 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(ShareManager_t2456449165 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T431283327_H
#ifndef UISCALEADJUSTER_T982702719_H
#define UISCALEADJUSTER_T982702719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScaleAdjuster
struct  UIScaleAdjuster_t982702719  : public UIAdjuster_t2553577866
{
public:
	// UnityEngine.RectTransform UIScaleAdjuster::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_8;
	// UIAdjuster/AspectRateType UIScaleAdjuster::_baseAspectRateType
	int32_t ____baseAspectRateType_9;
	// System.Single UIScaleAdjuster::_scalingValue
	float ____scalingValue_10;
	// System.Single UIScaleAdjuster::_iPadScale
	float ____iPadScale_11;

public:
	inline static int32_t get_offset_of__rectTransform_8() { return static_cast<int32_t>(offsetof(UIScaleAdjuster_t982702719, ____rectTransform_8)); }
	inline RectTransform_t3704657025 * get__rectTransform_8() const { return ____rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_8() { return &____rectTransform_8; }
	inline void set__rectTransform_8(RectTransform_t3704657025 * value)
	{
		____rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_8), value);
	}

	inline static int32_t get_offset_of__baseAspectRateType_9() { return static_cast<int32_t>(offsetof(UIScaleAdjuster_t982702719, ____baseAspectRateType_9)); }
	inline int32_t get__baseAspectRateType_9() const { return ____baseAspectRateType_9; }
	inline int32_t* get_address_of__baseAspectRateType_9() { return &____baseAspectRateType_9; }
	inline void set__baseAspectRateType_9(int32_t value)
	{
		____baseAspectRateType_9 = value;
	}

	inline static int32_t get_offset_of__scalingValue_10() { return static_cast<int32_t>(offsetof(UIScaleAdjuster_t982702719, ____scalingValue_10)); }
	inline float get__scalingValue_10() const { return ____scalingValue_10; }
	inline float* get_address_of__scalingValue_10() { return &____scalingValue_10; }
	inline void set__scalingValue_10(float value)
	{
		____scalingValue_10 = value;
	}

	inline static int32_t get_offset_of__iPadScale_11() { return static_cast<int32_t>(offsetof(UIScaleAdjuster_t982702719, ____iPadScale_11)); }
	inline float get__iPadScale_11() const { return ____iPadScale_11; }
	inline float* get_address_of__iPadScale_11() { return &____iPadScale_11; }
	inline void set__iPadScale_11(float value)
	{
		____iPadScale_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCALEADJUSTER_T982702719_H
#ifndef TWEETBUTTONASSISTANT_T1668838560_H
#define TWEETBUTTONASSISTANT_T1668838560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweetButtonAssistant
struct  TweetButtonAssistant_t1668838560  : public ButtonAssistant_t3922690862
{
public:
	// ShareLocation TweetButtonAssistant::_location
	int32_t ____location_5;
	// System.Boolean TweetButtonAssistant::_canTap
	bool ____canTap_6;

public:
	inline static int32_t get_offset_of__location_5() { return static_cast<int32_t>(offsetof(TweetButtonAssistant_t1668838560, ____location_5)); }
	inline int32_t get__location_5() const { return ____location_5; }
	inline int32_t* get_address_of__location_5() { return &____location_5; }
	inline void set__location_5(int32_t value)
	{
		____location_5 = value;
	}

	inline static int32_t get_offset_of__canTap_6() { return static_cast<int32_t>(offsetof(TweetButtonAssistant_t1668838560, ____canTap_6)); }
	inline bool get__canTap_6() const { return ____canTap_6; }
	inline bool* get_address_of__canTap_6() { return &____canTap_6; }
	inline void set__canTap_6(bool value)
	{
		____canTap_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEETBUTTONASSISTANT_T1668838560_H
#ifndef HOUSEADBUTTONASSISTANT_T1278736859_H
#define HOUSEADBUTTONASSISTANT_T1278736859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HouseAdButtonAssistant
struct  HouseAdButtonAssistant_t1278736859  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject HouseAdButtonAssistant::_newBadge
	GameObject_t1113636619 * ____newBadge_6;

public:
	inline static int32_t get_offset_of__newBadge_6() { return static_cast<int32_t>(offsetof(HouseAdButtonAssistant_t1278736859, ____newBadge_6)); }
	inline GameObject_t1113636619 * get__newBadge_6() const { return ____newBadge_6; }
	inline GameObject_t1113636619 ** get_address_of__newBadge_6() { return &____newBadge_6; }
	inline void set__newBadge_6(GameObject_t1113636619 * value)
	{
		____newBadge_6 = value;
		Il2CppCodeGenWriteBarrier((&____newBadge_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOUSEADBUTTONASSISTANT_T1278736859_H
#ifndef APPSTOREBUTTONASSISTANT_T455463633_H
#define APPSTOREBUTTONASSISTANT_T455463633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoreButtonAssistant
struct  AppStoreButtonAssistant_t455463633  : public ButtonAssistant_t3922690862
{
public:
	// ShareLocation AppStoreButtonAssistant::_location
	int32_t ____location_5;

public:
	inline static int32_t get_offset_of__location_5() { return static_cast<int32_t>(offsetof(AppStoreButtonAssistant_t455463633, ____location_5)); }
	inline int32_t get__location_5() const { return ____location_5; }
	inline int32_t* get_address_of__location_5() { return &____location_5; }
	inline void set__location_5(int32_t value)
	{
		____location_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTOREBUTTONASSISTANT_T455463633_H
#ifndef UIPOSITIONADJUSTER_T4076901380_H
#define UIPOSITIONADJUSTER_T4076901380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPositionAdjuster
struct  UIPositionAdjuster_t4076901380  : public UIAdjuster_t2553577866
{
public:
	// UnityEngine.RectTransform UIPositionAdjuster::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_8;
	// UIAdjuster/AspectRateType UIPositionAdjuster::_baseAspectRateType
	int32_t ____baseAspectRateType_9;
	// UnityEngine.Vector2 UIPositionAdjuster::_baisisPos
	Vector2_t2156229523  ____baisisPos_10;
	// UnityEngine.Vector2 UIPositionAdjuster::_moveDistance
	Vector2_t2156229523  ____moveDistance_11;
	// UnityEngine.Vector2 UIPositionAdjuster::_iPadBaisisPos
	Vector2_t2156229523  ____iPadBaisisPos_12;

public:
	inline static int32_t get_offset_of__rectTransform_8() { return static_cast<int32_t>(offsetof(UIPositionAdjuster_t4076901380, ____rectTransform_8)); }
	inline RectTransform_t3704657025 * get__rectTransform_8() const { return ____rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_8() { return &____rectTransform_8; }
	inline void set__rectTransform_8(RectTransform_t3704657025 * value)
	{
		____rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_8), value);
	}

	inline static int32_t get_offset_of__baseAspectRateType_9() { return static_cast<int32_t>(offsetof(UIPositionAdjuster_t4076901380, ____baseAspectRateType_9)); }
	inline int32_t get__baseAspectRateType_9() const { return ____baseAspectRateType_9; }
	inline int32_t* get_address_of__baseAspectRateType_9() { return &____baseAspectRateType_9; }
	inline void set__baseAspectRateType_9(int32_t value)
	{
		____baseAspectRateType_9 = value;
	}

	inline static int32_t get_offset_of__baisisPos_10() { return static_cast<int32_t>(offsetof(UIPositionAdjuster_t4076901380, ____baisisPos_10)); }
	inline Vector2_t2156229523  get__baisisPos_10() const { return ____baisisPos_10; }
	inline Vector2_t2156229523 * get_address_of__baisisPos_10() { return &____baisisPos_10; }
	inline void set__baisisPos_10(Vector2_t2156229523  value)
	{
		____baisisPos_10 = value;
	}

	inline static int32_t get_offset_of__moveDistance_11() { return static_cast<int32_t>(offsetof(UIPositionAdjuster_t4076901380, ____moveDistance_11)); }
	inline Vector2_t2156229523  get__moveDistance_11() const { return ____moveDistance_11; }
	inline Vector2_t2156229523 * get_address_of__moveDistance_11() { return &____moveDistance_11; }
	inline void set__moveDistance_11(Vector2_t2156229523  value)
	{
		____moveDistance_11 = value;
	}

	inline static int32_t get_offset_of__iPadBaisisPos_12() { return static_cast<int32_t>(offsetof(UIPositionAdjuster_t4076901380, ____iPadBaisisPos_12)); }
	inline Vector2_t2156229523  get__iPadBaisisPos_12() const { return ____iPadBaisisPos_12; }
	inline Vector2_t2156229523 * get_address_of__iPadBaisisPos_12() { return &____iPadBaisisPos_12; }
	inline void set__iPadBaisisPos_12(Vector2_t2156229523  value)
	{
		____iPadBaisisPos_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOSITIONADJUSTER_T4076901380_H
#ifndef CAMERASIZEADJUSTER_T3580329891_H
#define CAMERASIZEADJUSTER_T3580329891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSizeAdjuster
struct  CameraSizeAdjuster_t3580329891  : public UIAdjuster_t2553577866
{
public:
	// System.Single CameraSizeAdjuster::_basisCameraSize
	float ____basisCameraSize_8;
	// CameraSizeAdjuster/AspectRateType CameraSizeAdjuster::_basisAspectRateType
	int32_t ____basisAspectRateType_9;

public:
	inline static int32_t get_offset_of__basisCameraSize_8() { return static_cast<int32_t>(offsetof(CameraSizeAdjuster_t3580329891, ____basisCameraSize_8)); }
	inline float get__basisCameraSize_8() const { return ____basisCameraSize_8; }
	inline float* get_address_of__basisCameraSize_8() { return &____basisCameraSize_8; }
	inline void set__basisCameraSize_8(float value)
	{
		____basisCameraSize_8 = value;
	}

	inline static int32_t get_offset_of__basisAspectRateType_9() { return static_cast<int32_t>(offsetof(CameraSizeAdjuster_t3580329891, ____basisAspectRateType_9)); }
	inline int32_t get__basisAspectRateType_9() const { return ____basisAspectRateType_9; }
	inline int32_t* get_address_of__basisAspectRateType_9() { return &____basisAspectRateType_9; }
	inline void set__basisAspectRateType_9(int32_t value)
	{
		____basisAspectRateType_9 = value;
	}
};

struct CameraSizeAdjuster_t3580329891_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<CameraSizeAdjuster/AspectRateType,System.Single> CameraSizeAdjuster::ASPECT_RATE_DICT
	Dictionary_2_t1187321941 * ___ASPECT_RATE_DICT_10;

public:
	inline static int32_t get_offset_of_ASPECT_RATE_DICT_10() { return static_cast<int32_t>(offsetof(CameraSizeAdjuster_t3580329891_StaticFields, ___ASPECT_RATE_DICT_10)); }
	inline Dictionary_2_t1187321941 * get_ASPECT_RATE_DICT_10() const { return ___ASPECT_RATE_DICT_10; }
	inline Dictionary_2_t1187321941 ** get_address_of_ASPECT_RATE_DICT_10() { return &___ASPECT_RATE_DICT_10; }
	inline void set_ASPECT_RATE_DICT_10(Dictionary_2_t1187321941 * value)
	{
		___ASPECT_RATE_DICT_10 = value;
		Il2CppCodeGenWriteBarrier((&___ASPECT_RATE_DICT_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASIZEADJUSTER_T3580329891_H
#ifndef SHAREPLUGIN_T456476949_H
#define SHAREPLUGIN_T456476949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharePlugin
struct  SharePlugin_t456476949  : public NativePlugin_t1231223992
{
public:

public:
};

struct SharePlugin_t456476949_StaticFields
{
public:
	// System.Func`2<System.String,System.IntPtr> SharePlugin::<>f__mg$cache0
	Func_2_t2939991702 * ___U3CU3Ef__mgU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(SharePlugin_t456476949_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Func_2_t2939991702 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Func_2_t2939991702 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Func_2_t2939991702 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREPLUGIN_T456476949_H
#ifndef RANKINGPLUGIN_T2338328566_H
#define RANKINGPLUGIN_T2338328566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RankingPlugin
struct  RankingPlugin_t2338328566  : public NativePlugin_t1231223992
{
public:

public:
};

struct RankingPlugin_t2338328566_StaticFields
{
public:
	// System.Func`2<System.String,System.IntPtr> RankingPlugin::<>f__mg$cache0
	Func_2_t2939991702 * ___U3CU3Ef__mgU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(RankingPlugin_t2338328566_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Func_2_t2939991702 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Func_2_t2939991702 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Func_2_t2939991702 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKINGPLUGIN_T2338328566_H
#ifndef COMMONNATIVEPLUGIN_T3889574804_H
#define COMMONNATIVEPLUGIN_T3889574804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommonNativePlugin
struct  CommonNativePlugin_t3889574804  : public NativePlugin_t1231223992
{
public:

public:
};

struct CommonNativePlugin_t3889574804_StaticFields
{
public:
	// System.Func`2<System.String,System.IntPtr> CommonNativePlugin::<>f__mg$cache0
	Func_2_t2939991702 * ___U3CU3Ef__mgU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(CommonNativePlugin_t3889574804_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline Func_2_t2939991702 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline Func_2_t2939991702 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(Func_2_t2939991702 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONNATIVEPLUGIN_T3889574804_H
#ifndef ADPLUGIN_T1451289319_H
#define ADPLUGIN_T1451289319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdPlugin
struct  AdPlugin_t1451289319  : public NativePlugin_t1231223992
{
public:

public:
};

struct AdPlugin_t1451289319_StaticFields
{
public:
	// System.Func`2<System.String,System.IntPtr> AdPlugin::<>f__mg$cache0
	Func_2_t2939991702 * ___U3CU3Ef__mgU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(AdPlugin_t1451289319_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Func_2_t2939991702 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Func_2_t2939991702 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Func_2_t2939991702 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADPLUGIN_T1451289319_H
#ifndef AUDIOMANAGER_1_T1091618403_H
#define AUDIOMANAGER_1_T1091618403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager`1<SEManager>
struct  AudioManager_1_t1091618403  : public SingletonMonoBehaviour_1_t2593837087
{
public:
	// System.Single AudioManager`1::_pitch
	float ____pitch_4;
	// System.String AudioManager`1::_nextAudioName
	String_t* ____nextAudioName_5;
	// System.Boolean AudioManager`1::_isMute
	bool ____isMute_6;
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> AudioManager`1::_audioSourceList
	List_1_t1112413034 * ____audioSourceList_7;
	// System.Int32 AudioManager`1::_audioSourceNum
	int32_t ____audioSourceNum_8;
	// System.Single AudioManager`1::_volumeDefult
	float ____volumeDefult_9;
	// System.Boolean AudioManager`1::_isLoop
	bool ____isLoop_10;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip> AudioManager`1::_audoClipDic
	Dictionary_2_t3466145964 * ____audoClipDic_11;
	// System.String AudioManager`1::_audioDirectoryPath
	String_t* ____audioDirectoryPath_12;

public:
	inline static int32_t get_offset_of__pitch_4() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____pitch_4)); }
	inline float get__pitch_4() const { return ____pitch_4; }
	inline float* get_address_of__pitch_4() { return &____pitch_4; }
	inline void set__pitch_4(float value)
	{
		____pitch_4 = value;
	}

	inline static int32_t get_offset_of__nextAudioName_5() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____nextAudioName_5)); }
	inline String_t* get__nextAudioName_5() const { return ____nextAudioName_5; }
	inline String_t** get_address_of__nextAudioName_5() { return &____nextAudioName_5; }
	inline void set__nextAudioName_5(String_t* value)
	{
		____nextAudioName_5 = value;
		Il2CppCodeGenWriteBarrier((&____nextAudioName_5), value);
	}

	inline static int32_t get_offset_of__isMute_6() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____isMute_6)); }
	inline bool get__isMute_6() const { return ____isMute_6; }
	inline bool* get_address_of__isMute_6() { return &____isMute_6; }
	inline void set__isMute_6(bool value)
	{
		____isMute_6 = value;
	}

	inline static int32_t get_offset_of__audioSourceList_7() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____audioSourceList_7)); }
	inline List_1_t1112413034 * get__audioSourceList_7() const { return ____audioSourceList_7; }
	inline List_1_t1112413034 ** get_address_of__audioSourceList_7() { return &____audioSourceList_7; }
	inline void set__audioSourceList_7(List_1_t1112413034 * value)
	{
		____audioSourceList_7 = value;
		Il2CppCodeGenWriteBarrier((&____audioSourceList_7), value);
	}

	inline static int32_t get_offset_of__audioSourceNum_8() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____audioSourceNum_8)); }
	inline int32_t get__audioSourceNum_8() const { return ____audioSourceNum_8; }
	inline int32_t* get_address_of__audioSourceNum_8() { return &____audioSourceNum_8; }
	inline void set__audioSourceNum_8(int32_t value)
	{
		____audioSourceNum_8 = value;
	}

	inline static int32_t get_offset_of__volumeDefult_9() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____volumeDefult_9)); }
	inline float get__volumeDefult_9() const { return ____volumeDefult_9; }
	inline float* get_address_of__volumeDefult_9() { return &____volumeDefult_9; }
	inline void set__volumeDefult_9(float value)
	{
		____volumeDefult_9 = value;
	}

	inline static int32_t get_offset_of__isLoop_10() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____isLoop_10)); }
	inline bool get__isLoop_10() const { return ____isLoop_10; }
	inline bool* get_address_of__isLoop_10() { return &____isLoop_10; }
	inline void set__isLoop_10(bool value)
	{
		____isLoop_10 = value;
	}

	inline static int32_t get_offset_of__audoClipDic_11() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____audoClipDic_11)); }
	inline Dictionary_2_t3466145964 * get__audoClipDic_11() const { return ____audoClipDic_11; }
	inline Dictionary_2_t3466145964 ** get_address_of__audoClipDic_11() { return &____audoClipDic_11; }
	inline void set__audoClipDic_11(Dictionary_2_t3466145964 * value)
	{
		____audoClipDic_11 = value;
		Il2CppCodeGenWriteBarrier((&____audoClipDic_11), value);
	}

	inline static int32_t get_offset_of__audioDirectoryPath_12() { return static_cast<int32_t>(offsetof(AudioManager_1_t1091618403, ____audioDirectoryPath_12)); }
	inline String_t* get__audioDirectoryPath_12() const { return ____audioDirectoryPath_12; }
	inline String_t** get_address_of__audioDirectoryPath_12() { return &____audioDirectoryPath_12; }
	inline void set__audioDirectoryPath_12(String_t* value)
	{
		____audioDirectoryPath_12 = value;
		Il2CppCodeGenWriteBarrier((&____audioDirectoryPath_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_1_T1091618403_H
#ifndef MAINMANAGER_T729209856_H
#define MAINMANAGER_T729209856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainManager
struct  MainManager_t729209856  : public SingletonMonoBehaviour_1_t2999011314
{
public:
	// System.Boolean MainManager::showAdWhenRetry
	bool ___showAdWhenRetry_4;

public:
	inline static int32_t get_offset_of_showAdWhenRetry_4() { return static_cast<int32_t>(offsetof(MainManager_t729209856, ___showAdWhenRetry_4)); }
	inline bool get_showAdWhenRetry_4() const { return ___showAdWhenRetry_4; }
	inline bool* get_address_of_showAdWhenRetry_4() { return &___showAdWhenRetry_4; }
	inline void set_showAdWhenRetry_4(bool value)
	{
		___showAdWhenRetry_4 = value;
	}
};

struct MainManager_t729209856_StaticFields
{
public:
	// System.Action MainManager::<>f__mg$cache0
	Action_t1264377477 * ___U3CU3Ef__mgU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(MainManager_t729209856_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMANAGER_T729209856_H
#ifndef SCENENAVIGATOR_T2956304001_H
#define SCENENAVIGATOR_T2956304001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneNavigator
struct  SceneNavigator_t2956304001  : public SingletonMonoBehaviour_1_t931138163
{
public:
	// System.Boolean SceneNavigator::_isFading
	bool ____isFading_4;
	// System.String SceneNavigator::_afterSceneName
	String_t* ____afterSceneName_5;
	// System.String SceneNavigator::_beforeSceneName
	String_t* ____beforeSceneName_6;
	// System.Action SceneNavigator::OnFinishedFadeOut
	Action_t1264377477 * ___OnFinishedFadeOut_7;
	// System.Action`1<System.Int32> SceneNavigator::OnLoadedScene
	Action_1_t3123413348 * ___OnLoadedScene_8;
	// System.Int32 SceneNavigator::_titleCount
	int32_t ____titleCount_9;
	// FadeTransition SceneNavigator::_fadeTransition
	FadeTransition_t2992252162 * ____fadeTransition_10;

public:
	inline static int32_t get_offset_of__isFading_4() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001, ____isFading_4)); }
	inline bool get__isFading_4() const { return ____isFading_4; }
	inline bool* get_address_of__isFading_4() { return &____isFading_4; }
	inline void set__isFading_4(bool value)
	{
		____isFading_4 = value;
	}

	inline static int32_t get_offset_of__afterSceneName_5() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001, ____afterSceneName_5)); }
	inline String_t* get__afterSceneName_5() const { return ____afterSceneName_5; }
	inline String_t** get_address_of__afterSceneName_5() { return &____afterSceneName_5; }
	inline void set__afterSceneName_5(String_t* value)
	{
		____afterSceneName_5 = value;
		Il2CppCodeGenWriteBarrier((&____afterSceneName_5), value);
	}

	inline static int32_t get_offset_of__beforeSceneName_6() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001, ____beforeSceneName_6)); }
	inline String_t* get__beforeSceneName_6() const { return ____beforeSceneName_6; }
	inline String_t** get_address_of__beforeSceneName_6() { return &____beforeSceneName_6; }
	inline void set__beforeSceneName_6(String_t* value)
	{
		____beforeSceneName_6 = value;
		Il2CppCodeGenWriteBarrier((&____beforeSceneName_6), value);
	}

	inline static int32_t get_offset_of_OnFinishedFadeOut_7() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001, ___OnFinishedFadeOut_7)); }
	inline Action_t1264377477 * get_OnFinishedFadeOut_7() const { return ___OnFinishedFadeOut_7; }
	inline Action_t1264377477 ** get_address_of_OnFinishedFadeOut_7() { return &___OnFinishedFadeOut_7; }
	inline void set_OnFinishedFadeOut_7(Action_t1264377477 * value)
	{
		___OnFinishedFadeOut_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinishedFadeOut_7), value);
	}

	inline static int32_t get_offset_of_OnLoadedScene_8() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001, ___OnLoadedScene_8)); }
	inline Action_1_t3123413348 * get_OnLoadedScene_8() const { return ___OnLoadedScene_8; }
	inline Action_1_t3123413348 ** get_address_of_OnLoadedScene_8() { return &___OnLoadedScene_8; }
	inline void set_OnLoadedScene_8(Action_1_t3123413348 * value)
	{
		___OnLoadedScene_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoadedScene_8), value);
	}

	inline static int32_t get_offset_of__titleCount_9() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001, ____titleCount_9)); }
	inline int32_t get__titleCount_9() const { return ____titleCount_9; }
	inline int32_t* get_address_of__titleCount_9() { return &____titleCount_9; }
	inline void set__titleCount_9(int32_t value)
	{
		____titleCount_9 = value;
	}

	inline static int32_t get_offset_of__fadeTransition_10() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001, ____fadeTransition_10)); }
	inline FadeTransition_t2992252162 * get__fadeTransition_10() const { return ____fadeTransition_10; }
	inline FadeTransition_t2992252162 ** get_address_of__fadeTransition_10() { return &____fadeTransition_10; }
	inline void set__fadeTransition_10(FadeTransition_t2992252162 * value)
	{
		____fadeTransition_10 = value;
		Il2CppCodeGenWriteBarrier((&____fadeTransition_10), value);
	}
};

struct SceneNavigator_t2956304001_StaticFields
{
public:
	// System.Action SceneNavigator::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_11;
	// System.Action`1<System.Int32> SceneNavigator::<>f__am$cache1
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache1_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(SceneNavigator_t2956304001_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENENAVIGATOR_T2956304001_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public SingletonMonoBehaviour_1_t3806325112
{
public:
	// System.Int32 GameManager::_score
	int32_t ____score_4;
	// GameManager/PlayMode GameManager::playMode
	int32_t ___playMode_5;
	// UnityEngine.GameObject GameManager::gamePlay
	GameObject_t1113636619 * ___gamePlay_6;
	// UnityEngine.GameObject GameManager::background
	GameObject_t1113636619 * ___background_7;
	// UnityEngine.GameObject GameManager::dialogContinue
	GameObject_t1113636619 * ___dialogContinue_8;
	// UnityEngine.GameObject GameManager::noThanksButton
	GameObject_t1113636619 * ___noThanksButton_9;
	// UnityEngine.GameObject GameManager::labelNewBall
	GameObject_t1113636619 * ___labelNewBall_10;
	// UnityEngine.GameObject GameManager::labelNewStage
	GameObject_t1113636619 * ___labelNewStage_11;
	// UnityEngine.GameObject GameManager::labelNewStageTop
	GameObject_t1113636619 * ___labelNewStageTop_12;
	// UnityEngine.GameObject[] GameManager::_theme
	GameObjectU5BU5D_t3328599146* ____theme_13;
	// UnityEngine.GameObject GameManager::_snowEffect
	GameObject_t1113636619 * ____snowEffect_14;
	// System.Boolean GameManager::_alreadyContinue
	bool ____alreadyContinue_15;
	// System.Boolean GameManager::_canShowPauseAds
	bool ____canShowPauseAds_16;
	// System.Boolean GameManager::_isPlayingGame
	bool ____isPlayingGame_17;
	// System.Action`1<System.Boolean> GameManager::OnGameEnd
	Action_1_t269755560 * ___OnGameEnd_18;
	// System.Action`1<System.Int32> GameManager::OnChangeScore
	Action_1_t3123413348 * ___OnChangeScore_19;

public:
	inline static int32_t get_offset_of__score_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____score_4)); }
	inline int32_t get__score_4() const { return ____score_4; }
	inline int32_t* get_address_of__score_4() { return &____score_4; }
	inline void set__score_4(int32_t value)
	{
		____score_4 = value;
	}

	inline static int32_t get_offset_of_playMode_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___playMode_5)); }
	inline int32_t get_playMode_5() const { return ___playMode_5; }
	inline int32_t* get_address_of_playMode_5() { return &___playMode_5; }
	inline void set_playMode_5(int32_t value)
	{
		___playMode_5 = value;
	}

	inline static int32_t get_offset_of_gamePlay_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___gamePlay_6)); }
	inline GameObject_t1113636619 * get_gamePlay_6() const { return ___gamePlay_6; }
	inline GameObject_t1113636619 ** get_address_of_gamePlay_6() { return &___gamePlay_6; }
	inline void set_gamePlay_6(GameObject_t1113636619 * value)
	{
		___gamePlay_6 = value;
		Il2CppCodeGenWriteBarrier((&___gamePlay_6), value);
	}

	inline static int32_t get_offset_of_background_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___background_7)); }
	inline GameObject_t1113636619 * get_background_7() const { return ___background_7; }
	inline GameObject_t1113636619 ** get_address_of_background_7() { return &___background_7; }
	inline void set_background_7(GameObject_t1113636619 * value)
	{
		___background_7 = value;
		Il2CppCodeGenWriteBarrier((&___background_7), value);
	}

	inline static int32_t get_offset_of_dialogContinue_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___dialogContinue_8)); }
	inline GameObject_t1113636619 * get_dialogContinue_8() const { return ___dialogContinue_8; }
	inline GameObject_t1113636619 ** get_address_of_dialogContinue_8() { return &___dialogContinue_8; }
	inline void set_dialogContinue_8(GameObject_t1113636619 * value)
	{
		___dialogContinue_8 = value;
		Il2CppCodeGenWriteBarrier((&___dialogContinue_8), value);
	}

	inline static int32_t get_offset_of_noThanksButton_9() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___noThanksButton_9)); }
	inline GameObject_t1113636619 * get_noThanksButton_9() const { return ___noThanksButton_9; }
	inline GameObject_t1113636619 ** get_address_of_noThanksButton_9() { return &___noThanksButton_9; }
	inline void set_noThanksButton_9(GameObject_t1113636619 * value)
	{
		___noThanksButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___noThanksButton_9), value);
	}

	inline static int32_t get_offset_of_labelNewBall_10() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___labelNewBall_10)); }
	inline GameObject_t1113636619 * get_labelNewBall_10() const { return ___labelNewBall_10; }
	inline GameObject_t1113636619 ** get_address_of_labelNewBall_10() { return &___labelNewBall_10; }
	inline void set_labelNewBall_10(GameObject_t1113636619 * value)
	{
		___labelNewBall_10 = value;
		Il2CppCodeGenWriteBarrier((&___labelNewBall_10), value);
	}

	inline static int32_t get_offset_of_labelNewStage_11() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___labelNewStage_11)); }
	inline GameObject_t1113636619 * get_labelNewStage_11() const { return ___labelNewStage_11; }
	inline GameObject_t1113636619 ** get_address_of_labelNewStage_11() { return &___labelNewStage_11; }
	inline void set_labelNewStage_11(GameObject_t1113636619 * value)
	{
		___labelNewStage_11 = value;
		Il2CppCodeGenWriteBarrier((&___labelNewStage_11), value);
	}

	inline static int32_t get_offset_of_labelNewStageTop_12() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___labelNewStageTop_12)); }
	inline GameObject_t1113636619 * get_labelNewStageTop_12() const { return ___labelNewStageTop_12; }
	inline GameObject_t1113636619 ** get_address_of_labelNewStageTop_12() { return &___labelNewStageTop_12; }
	inline void set_labelNewStageTop_12(GameObject_t1113636619 * value)
	{
		___labelNewStageTop_12 = value;
		Il2CppCodeGenWriteBarrier((&___labelNewStageTop_12), value);
	}

	inline static int32_t get_offset_of__theme_13() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____theme_13)); }
	inline GameObjectU5BU5D_t3328599146* get__theme_13() const { return ____theme_13; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__theme_13() { return &____theme_13; }
	inline void set__theme_13(GameObjectU5BU5D_t3328599146* value)
	{
		____theme_13 = value;
		Il2CppCodeGenWriteBarrier((&____theme_13), value);
	}

	inline static int32_t get_offset_of__snowEffect_14() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____snowEffect_14)); }
	inline GameObject_t1113636619 * get__snowEffect_14() const { return ____snowEffect_14; }
	inline GameObject_t1113636619 ** get_address_of__snowEffect_14() { return &____snowEffect_14; }
	inline void set__snowEffect_14(GameObject_t1113636619 * value)
	{
		____snowEffect_14 = value;
		Il2CppCodeGenWriteBarrier((&____snowEffect_14), value);
	}

	inline static int32_t get_offset_of__alreadyContinue_15() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____alreadyContinue_15)); }
	inline bool get__alreadyContinue_15() const { return ____alreadyContinue_15; }
	inline bool* get_address_of__alreadyContinue_15() { return &____alreadyContinue_15; }
	inline void set__alreadyContinue_15(bool value)
	{
		____alreadyContinue_15 = value;
	}

	inline static int32_t get_offset_of__canShowPauseAds_16() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____canShowPauseAds_16)); }
	inline bool get__canShowPauseAds_16() const { return ____canShowPauseAds_16; }
	inline bool* get_address_of__canShowPauseAds_16() { return &____canShowPauseAds_16; }
	inline void set__canShowPauseAds_16(bool value)
	{
		____canShowPauseAds_16 = value;
	}

	inline static int32_t get_offset_of__isPlayingGame_17() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____isPlayingGame_17)); }
	inline bool get__isPlayingGame_17() const { return ____isPlayingGame_17; }
	inline bool* get_address_of__isPlayingGame_17() { return &____isPlayingGame_17; }
	inline void set__isPlayingGame_17(bool value)
	{
		____isPlayingGame_17 = value;
	}

	inline static int32_t get_offset_of_OnGameEnd_18() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___OnGameEnd_18)); }
	inline Action_1_t269755560 * get_OnGameEnd_18() const { return ___OnGameEnd_18; }
	inline Action_1_t269755560 ** get_address_of_OnGameEnd_18() { return &___OnGameEnd_18; }
	inline void set_OnGameEnd_18(Action_1_t269755560 * value)
	{
		___OnGameEnd_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnGameEnd_18), value);
	}

	inline static int32_t get_offset_of_OnChangeScore_19() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___OnChangeScore_19)); }
	inline Action_1_t3123413348 * get_OnChangeScore_19() const { return ___OnChangeScore_19; }
	inline Action_1_t3123413348 ** get_address_of_OnChangeScore_19() { return &___OnChangeScore_19; }
	inline void set_OnChangeScore_19(Action_1_t3123413348 * value)
	{
		___OnChangeScore_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnChangeScore_19), value);
	}
};

struct GameManager_t1536523654_StaticFields
{
public:
	// System.Action GameManager::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_20;
	// System.Action GameManager::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_21;
	// System.Action`1<System.Boolean> GameManager::<>f__am$cache2
	Action_1_t269755560 * ___U3CU3Ef__amU24cache2_22;
	// System.Action`1<System.Int32> GameManager::<>f__am$cache3
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache3_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_20() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___U3CU3Ef__amU24cache0_20)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_20() const { return ___U3CU3Ef__amU24cache0_20; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_20() { return &___U3CU3Ef__amU24cache0_20; }
	inline void set_U3CU3Ef__amU24cache0_20(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_21() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___U3CU3Ef__amU24cache1_21)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_21() const { return ___U3CU3Ef__amU24cache1_21; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_21() { return &___U3CU3Ef__amU24cache1_21; }
	inline void set_U3CU3Ef__amU24cache1_21(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_22() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___U3CU3Ef__amU24cache2_22)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache2_22() const { return ___U3CU3Ef__amU24cache2_22; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache2_22() { return &___U3CU3Ef__amU24cache2_22; }
	inline void set_U3CU3Ef__amU24cache2_22(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache2_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_23() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___U3CU3Ef__amU24cache3_23)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache3_23() const { return ___U3CU3Ef__amU24cache3_23; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache3_23() { return &___U3CU3Ef__amU24cache3_23; }
	inline void set_U3CU3Ef__amU24cache3_23(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache3_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef ADMANAGER_T2410889370_H
#define ADMANAGER_T2410889370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdManager
struct  AdManager_t2410889370  : public SingletonMonoBehaviour_1_t385723532
{
public:
	// AdPlugin AdManager::_adPlugin
	AdPlugin_t1451289319 * ____adPlugin_4;
	// System.Boolean AdManager::_canShowSplash
	bool ____canShowSplash_7;
	// System.Boolean AdManager::_canShowVideo
	bool ____canShowVideo_8;
	// System.Boolean AdManager::_isRevewing
	bool ____isRevewing_9;
	// System.Int32 AdManager::_ranking
	int32_t ____ranking_10;

public:
	inline static int32_t get_offset_of__adPlugin_4() { return static_cast<int32_t>(offsetof(AdManager_t2410889370, ____adPlugin_4)); }
	inline AdPlugin_t1451289319 * get__adPlugin_4() const { return ____adPlugin_4; }
	inline AdPlugin_t1451289319 ** get_address_of__adPlugin_4() { return &____adPlugin_4; }
	inline void set__adPlugin_4(AdPlugin_t1451289319 * value)
	{
		____adPlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____adPlugin_4), value);
	}

	inline static int32_t get_offset_of__canShowSplash_7() { return static_cast<int32_t>(offsetof(AdManager_t2410889370, ____canShowSplash_7)); }
	inline bool get__canShowSplash_7() const { return ____canShowSplash_7; }
	inline bool* get_address_of__canShowSplash_7() { return &____canShowSplash_7; }
	inline void set__canShowSplash_7(bool value)
	{
		____canShowSplash_7 = value;
	}

	inline static int32_t get_offset_of__canShowVideo_8() { return static_cast<int32_t>(offsetof(AdManager_t2410889370, ____canShowVideo_8)); }
	inline bool get__canShowVideo_8() const { return ____canShowVideo_8; }
	inline bool* get_address_of__canShowVideo_8() { return &____canShowVideo_8; }
	inline void set__canShowVideo_8(bool value)
	{
		____canShowVideo_8 = value;
	}

	inline static int32_t get_offset_of__isRevewing_9() { return static_cast<int32_t>(offsetof(AdManager_t2410889370, ____isRevewing_9)); }
	inline bool get__isRevewing_9() const { return ____isRevewing_9; }
	inline bool* get_address_of__isRevewing_9() { return &____isRevewing_9; }
	inline void set__isRevewing_9(bool value)
	{
		____isRevewing_9 = value;
	}

	inline static int32_t get_offset_of__ranking_10() { return static_cast<int32_t>(offsetof(AdManager_t2410889370, ____ranking_10)); }
	inline int32_t get__ranking_10() const { return ____ranking_10; }
	inline int32_t* get_address_of__ranking_10() { return &____ranking_10; }
	inline void set__ranking_10(int32_t value)
	{
		____ranking_10 = value;
	}
};

struct AdManager_t2410889370_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AdManager::SCENE_NO_DICT_FOR_AD
	Dictionary_2_t2736202052 * ___SCENE_NO_DICT_FOR_AD_11;

public:
	inline static int32_t get_offset_of_SCENE_NO_DICT_FOR_AD_11() { return static_cast<int32_t>(offsetof(AdManager_t2410889370_StaticFields, ___SCENE_NO_DICT_FOR_AD_11)); }
	inline Dictionary_2_t2736202052 * get_SCENE_NO_DICT_FOR_AD_11() const { return ___SCENE_NO_DICT_FOR_AD_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_SCENE_NO_DICT_FOR_AD_11() { return &___SCENE_NO_DICT_FOR_AD_11; }
	inline void set_SCENE_NO_DICT_FOR_AD_11(Dictionary_2_t2736202052 * value)
	{
		___SCENE_NO_DICT_FOR_AD_11 = value;
		Il2CppCodeGenWriteBarrier((&___SCENE_NO_DICT_FOR_AD_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMANAGER_T2410889370_H
#ifndef RANKINGMANAGER_T1599285360_H
#define RANKINGMANAGER_T1599285360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RankingManager
struct  RankingManager_t1599285360  : public SingletonMonoBehaviour_1_t3869086818
{
public:
	// RankingPlugin RankingManager::_rankingPlugin
	RankingPlugin_t2338328566 * ____rankingPlugin_4;
	// System.Action RankingManager::OnShowLeaderBoard
	Action_t1264377477 * ___OnShowLeaderBoard_5;

public:
	inline static int32_t get_offset_of__rankingPlugin_4() { return static_cast<int32_t>(offsetof(RankingManager_t1599285360, ____rankingPlugin_4)); }
	inline RankingPlugin_t2338328566 * get__rankingPlugin_4() const { return ____rankingPlugin_4; }
	inline RankingPlugin_t2338328566 ** get_address_of__rankingPlugin_4() { return &____rankingPlugin_4; }
	inline void set__rankingPlugin_4(RankingPlugin_t2338328566 * value)
	{
		____rankingPlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____rankingPlugin_4), value);
	}

	inline static int32_t get_offset_of_OnShowLeaderBoard_5() { return static_cast<int32_t>(offsetof(RankingManager_t1599285360, ___OnShowLeaderBoard_5)); }
	inline Action_t1264377477 * get_OnShowLeaderBoard_5() const { return ___OnShowLeaderBoard_5; }
	inline Action_t1264377477 ** get_address_of_OnShowLeaderBoard_5() { return &___OnShowLeaderBoard_5; }
	inline void set_OnShowLeaderBoard_5(Action_t1264377477 * value)
	{
		___OnShowLeaderBoard_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnShowLeaderBoard_5), value);
	}
};

struct RankingManager_t1599285360_StaticFields
{
public:
	// System.Action RankingManager::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(RankingManager_t1599285360_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKINGMANAGER_T1599285360_H
#ifndef AUDIOMANAGER_1_T2557624882_H
#define AUDIOMANAGER_1_T2557624882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager`1<BGMManager>
struct  AudioManager_1_t2557624882  : public SingletonMonoBehaviour_1_t4059843566
{
public:
	// System.Single AudioManager`1::_pitch
	float ____pitch_4;
	// System.String AudioManager`1::_nextAudioName
	String_t* ____nextAudioName_5;
	// System.Boolean AudioManager`1::_isMute
	bool ____isMute_6;
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> AudioManager`1::_audioSourceList
	List_1_t1112413034 * ____audioSourceList_7;
	// System.Int32 AudioManager`1::_audioSourceNum
	int32_t ____audioSourceNum_8;
	// System.Single AudioManager`1::_volumeDefult
	float ____volumeDefult_9;
	// System.Boolean AudioManager`1::_isLoop
	bool ____isLoop_10;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip> AudioManager`1::_audoClipDic
	Dictionary_2_t3466145964 * ____audoClipDic_11;
	// System.String AudioManager`1::_audioDirectoryPath
	String_t* ____audioDirectoryPath_12;

public:
	inline static int32_t get_offset_of__pitch_4() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____pitch_4)); }
	inline float get__pitch_4() const { return ____pitch_4; }
	inline float* get_address_of__pitch_4() { return &____pitch_4; }
	inline void set__pitch_4(float value)
	{
		____pitch_4 = value;
	}

	inline static int32_t get_offset_of__nextAudioName_5() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____nextAudioName_5)); }
	inline String_t* get__nextAudioName_5() const { return ____nextAudioName_5; }
	inline String_t** get_address_of__nextAudioName_5() { return &____nextAudioName_5; }
	inline void set__nextAudioName_5(String_t* value)
	{
		____nextAudioName_5 = value;
		Il2CppCodeGenWriteBarrier((&____nextAudioName_5), value);
	}

	inline static int32_t get_offset_of__isMute_6() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____isMute_6)); }
	inline bool get__isMute_6() const { return ____isMute_6; }
	inline bool* get_address_of__isMute_6() { return &____isMute_6; }
	inline void set__isMute_6(bool value)
	{
		____isMute_6 = value;
	}

	inline static int32_t get_offset_of__audioSourceList_7() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____audioSourceList_7)); }
	inline List_1_t1112413034 * get__audioSourceList_7() const { return ____audioSourceList_7; }
	inline List_1_t1112413034 ** get_address_of__audioSourceList_7() { return &____audioSourceList_7; }
	inline void set__audioSourceList_7(List_1_t1112413034 * value)
	{
		____audioSourceList_7 = value;
		Il2CppCodeGenWriteBarrier((&____audioSourceList_7), value);
	}

	inline static int32_t get_offset_of__audioSourceNum_8() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____audioSourceNum_8)); }
	inline int32_t get__audioSourceNum_8() const { return ____audioSourceNum_8; }
	inline int32_t* get_address_of__audioSourceNum_8() { return &____audioSourceNum_8; }
	inline void set__audioSourceNum_8(int32_t value)
	{
		____audioSourceNum_8 = value;
	}

	inline static int32_t get_offset_of__volumeDefult_9() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____volumeDefult_9)); }
	inline float get__volumeDefult_9() const { return ____volumeDefult_9; }
	inline float* get_address_of__volumeDefult_9() { return &____volumeDefult_9; }
	inline void set__volumeDefult_9(float value)
	{
		____volumeDefult_9 = value;
	}

	inline static int32_t get_offset_of__isLoop_10() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____isLoop_10)); }
	inline bool get__isLoop_10() const { return ____isLoop_10; }
	inline bool* get_address_of__isLoop_10() { return &____isLoop_10; }
	inline void set__isLoop_10(bool value)
	{
		____isLoop_10 = value;
	}

	inline static int32_t get_offset_of__audoClipDic_11() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____audoClipDic_11)); }
	inline Dictionary_2_t3466145964 * get__audoClipDic_11() const { return ____audoClipDic_11; }
	inline Dictionary_2_t3466145964 ** get_address_of__audoClipDic_11() { return &____audoClipDic_11; }
	inline void set__audoClipDic_11(Dictionary_2_t3466145964 * value)
	{
		____audoClipDic_11 = value;
		Il2CppCodeGenWriteBarrier((&____audoClipDic_11), value);
	}

	inline static int32_t get_offset_of__audioDirectoryPath_12() { return static_cast<int32_t>(offsetof(AudioManager_1_t2557624882, ____audioDirectoryPath_12)); }
	inline String_t* get__audioDirectoryPath_12() const { return ____audioDirectoryPath_12; }
	inline String_t** get_address_of__audioDirectoryPath_12() { return &____audioDirectoryPath_12; }
	inline void set__audioDirectoryPath_12(String_t* value)
	{
		____audioDirectoryPath_12 = value;
		Il2CppCodeGenWriteBarrier((&____audioDirectoryPath_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_1_T2557624882_H
#ifndef SHAREMANAGER_T2456449165_H
#define SHAREMANAGER_T2456449165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShareManager
struct  ShareManager_t2456449165  : public SingletonMonoBehaviour_1_t431283327
{
public:
	// SharePlugin ShareManager::_sharePlugin
	SharePlugin_t456476949 * ____sharePlugin_4;

public:
	inline static int32_t get_offset_of__sharePlugin_4() { return static_cast<int32_t>(offsetof(ShareManager_t2456449165, ____sharePlugin_4)); }
	inline SharePlugin_t456476949 * get__sharePlugin_4() const { return ____sharePlugin_4; }
	inline SharePlugin_t456476949 ** get_address_of__sharePlugin_4() { return &____sharePlugin_4; }
	inline void set__sharePlugin_4(SharePlugin_t456476949 * value)
	{
		____sharePlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____sharePlugin_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREMANAGER_T2456449165_H
#ifndef BUTTONCOLLIDER_T3142833335_H
#define BUTTONCOLLIDER_T3142833335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonCollider
struct  ButtonCollider_t3142833335  : public Graphic_t1660335611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCOLLIDER_T3142833335_H
#ifndef COMMONNATIVEMANAGER_T3006429997_H
#define COMMONNATIVEMANAGER_T3006429997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommonNativeManager
struct  CommonNativeManager_t3006429997  : public SingletonMonoBehaviour_1_t981264159
{
public:
	// CommonNativePlugin CommonNativeManager::_commonNativePlugin
	CommonNativePlugin_t3889574804 * ____commonNativePlugin_4;

public:
	inline static int32_t get_offset_of__commonNativePlugin_4() { return static_cast<int32_t>(offsetof(CommonNativeManager_t3006429997, ____commonNativePlugin_4)); }
	inline CommonNativePlugin_t3889574804 * get__commonNativePlugin_4() const { return ____commonNativePlugin_4; }
	inline CommonNativePlugin_t3889574804 ** get_address_of__commonNativePlugin_4() { return &____commonNativePlugin_4; }
	inline void set__commonNativePlugin_4(CommonNativePlugin_t3889574804 * value)
	{
		____commonNativePlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____commonNativePlugin_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONNATIVEMANAGER_T3006429997_H
#ifndef BGMMANAGER_T1790042108_H
#define BGMMANAGER_T1790042108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BGMManager
struct  BGMManager_t1790042108  : public AudioManager_1_t2557624882
{
public:
	// System.String BGMManager::_lastBGMName
	String_t* ____lastBGMName_13;
	// System.Boolean BGMManager::_isFadeOut
	bool ____isFadeOut_14;

public:
	inline static int32_t get_offset_of__lastBGMName_13() { return static_cast<int32_t>(offsetof(BGMManager_t1790042108, ____lastBGMName_13)); }
	inline String_t* get__lastBGMName_13() const { return ____lastBGMName_13; }
	inline String_t** get_address_of__lastBGMName_13() { return &____lastBGMName_13; }
	inline void set__lastBGMName_13(String_t* value)
	{
		____lastBGMName_13 = value;
		Il2CppCodeGenWriteBarrier((&____lastBGMName_13), value);
	}

	inline static int32_t get_offset_of__isFadeOut_14() { return static_cast<int32_t>(offsetof(BGMManager_t1790042108, ____isFadeOut_14)); }
	inline bool get__isFadeOut_14() const { return ____isFadeOut_14; }
	inline bool* get_address_of__isFadeOut_14() { return &____isFadeOut_14; }
	inline void set__isFadeOut_14(bool value)
	{
		____isFadeOut_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BGMMANAGER_T1790042108_H
#ifndef SEMANAGER_T324035629_H
#define SEMANAGER_T324035629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SEManager
struct  SEManager_t324035629  : public AudioManager_1_t1091618403
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEMANAGER_T324035629_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (BannerType_t3341813445)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2700[4] = 
{
	BannerType_t3341813445::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (RectangleType_t3748261860)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2701[3] = 
{
	RectangleType_t3748261860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (IconType_t1006196312)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2702[3] = 
{
	IconType_t1006196312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (FPSUI_t1916804684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[3] = 
{
	FPSUI_t1916804684::get_offset_of__fpsList_5(),
	0,
	FPSUI_t1916804684::get_offset_of__fps_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (Debug_t3683223393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (TestUI_t3746935818), -1, sizeof(TestUI_t3746935818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2705[3] = 
{
	TestUI_t3746935818::get_offset_of__labelStyle_2(),
	TestUI_t3746935818::get_offset_of__buttonStyle_3(),
	TestUI_t3746935818_StaticFields::get_offset_of_SCREEN_SIZE_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (DictionaryExtensions_t457327208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (EnumExtensions_t4248483755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (GameObjectExtension_t1934678351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (MonoBehaviorExtentsion_t1290638044), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (U3CDelayMethodU3Ec__Iterator3_t3849733590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[5] = 
{
	U3CDelayMethodU3Ec__Iterator3_t3849733590::get_offset_of_waitTime_0(),
	U3CDelayMethodU3Ec__Iterator3_t3849733590::get_offset_of_action_1(),
	U3CDelayMethodU3Ec__Iterator3_t3849733590::get_offset_of_U24current_2(),
	U3CDelayMethodU3Ec__Iterator3_t3849733590::get_offset_of_U24disposing_3(),
	U3CDelayMethodU3Ec__Iterator3_t3849733590::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (PhysicsExtentsion_t553451405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (Velocity2DTmp_t2283591314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[2] = 
{
	Velocity2DTmp_t2283591314::get_offset_of__angularVelocity_2(),
	Velocity2DTmp_t2283591314::get_offset_of__velocity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (Rigidbody2DExtension_t2198349777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (VelocityTmp_t3274308841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[2] = 
{
	VelocityTmp_t3274308841::get_offset_of__angularVelocity_2(),
	VelocityTmp_t3274308841::get_offset_of__velocity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (RigidbodyExtension_t1329912879), -1, sizeof(RigidbodyExtension_t1329912879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2718[2] = 
{
	RigidbodyExtension_t1329912879_StaticFields::get_offset_of__angularVelocity_0(),
	RigidbodyExtension_t1329912879_StaticFields::get_offset_of__velocity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (StringExtensions_t731559555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (TransformExtension_t4238110091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (AutoBGMSwitcher_t3639340263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (CircleDeployer_t1503689571), -1, sizeof(CircleDeployer_t1503689571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	CircleDeployer_t1503689571::get_offset_of__radius_2(),
	CircleDeployer_t1503689571_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (GridDeployer_t1241683661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[7] = 
{
	GridDeployer_t1241683661::get_offset_of__previousDimension_2(),
	GridDeployer_t1241683661::get_offset_of__nextDimension_3(),
	GridDeployer_t1241683661::get_offset_of__previousPivot_4(),
	GridDeployer_t1241683661::get_offset_of__nextPivot_5(),
	GridDeployer_t1241683661::get_offset_of__previousSpace_6(),
	GridDeployer_t1241683661::get_offset_of__nextSpace_7(),
	GridDeployer_t1241683661::get_offset_of__limit_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (Dimension_t1087553712)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2724[4] = 
{
	Dimension_t1087553712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (PivotType_t1871032332)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2725[3] = 
{
	PivotType_t1871032332::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (BackCameraScript_t420974540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[1] = 
{
	BackCameraScript_t420974540::get_offset_of_reverseSetting_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (ColorBalance_t2602991044)+ sizeof (RuntimeObject), sizeof(ColorBalance_t2602991044 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2727[2] = 
{
	ColorBalance_t2602991044::get_offset_of_score_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorBalance_t2602991044::get_offset_of_colorIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (BackgroundScript_t3530941027), -1, sizeof(BackgroundScript_t3530941027_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2728[45] = 
{
	BackgroundScript_t3530941027_StaticFields::get_offset_of_instance_2(),
	BackgroundScript_t3530941027::get_offset_of_OnBackChangeColor_3(),
	BackgroundScript_t3530941027::get_offset_of_OnHazeChangeColor_4(),
	BackgroundScript_t3530941027::get_offset_of_OnMountainChangeColor_5(),
	BackgroundScript_t3530941027::get_offset_of_OnCloudChangeColor_6(),
	BackgroundScript_t3530941027::get_offset_of_renderCamera_7(),
	BackgroundScript_t3530941027::get_offset_of_backColors_8(),
	BackgroundScript_t3530941027::get_offset_of_currentBackColor_9(),
	BackgroundScript_t3530941027::get_offset_of_nextBackColor_10(),
	BackgroundScript_t3530941027::get_offset_of_previousBackColor_11(),
	BackgroundScript_t3530941027::get_offset_of_hazeColors_12(),
	BackgroundScript_t3530941027::get_offset_of_currentHazeColor_13(),
	BackgroundScript_t3530941027::get_offset_of_nextHazeColor_14(),
	BackgroundScript_t3530941027::get_offset_of_previousHazeColor_15(),
	BackgroundScript_t3530941027::get_offset_of_moutainColors_16(),
	BackgroundScript_t3530941027::get_offset_of_currentMountainColor_17(),
	BackgroundScript_t3530941027::get_offset_of_nextMountainColor_18(),
	BackgroundScript_t3530941027::get_offset_of_previousMountainColor_19(),
	BackgroundScript_t3530941027::get_offset_of_cloudColors_20(),
	BackgroundScript_t3530941027::get_offset_of_currentCloudColor_21(),
	BackgroundScript_t3530941027::get_offset_of_nextCloudColor_22(),
	BackgroundScript_t3530941027::get_offset_of_previousCloudColor_23(),
	BackgroundScript_t3530941027::get_offset_of_fogColors_24(),
	BackgroundScript_t3530941027::get_offset_of_currentFogColor_25(),
	BackgroundScript_t3530941027::get_offset_of_nextFogColor_26(),
	BackgroundScript_t3530941027::get_offset_of_previousFogColor_27(),
	BackgroundScript_t3530941027::get_offset_of_cameraColors_28(),
	BackgroundScript_t3530941027::get_offset_of_currentCameraColor_29(),
	BackgroundScript_t3530941027::get_offset_of_nextCameraColor_30(),
	BackgroundScript_t3530941027::get_offset_of_previousCameraColor_31(),
	BackgroundScript_t3530941027::get_offset_of_mStartColors_32(),
	BackgroundScript_t3530941027::get_offset_of_currentMStartColor_33(),
	BackgroundScript_t3530941027::get_offset_of_nextMStartColor_34(),
	BackgroundScript_t3530941027::get_offset_of_previousMStartColor_35(),
	BackgroundScript_t3530941027::get_offset_of_mEndColors_36(),
	BackgroundScript_t3530941027::get_offset_of_currentMEndColor_37(),
	BackgroundScript_t3530941027::get_offset_of_nextMEndColor_38(),
	BackgroundScript_t3530941027::get_offset_of_previousMEndColor_39(),
	BackgroundScript_t3530941027::get_offset_of_currentColorIndex_40(),
	BackgroundScript_t3530941027::get_offset_of_colorBalances_41(),
	BackgroundScript_t3530941027::get_offset_of_maxColorScoreLoop_42(),
	BackgroundScript_t3530941027::get_offset_of_changeColorDuration_43(),
	BackgroundScript_t3530941027::get_offset_of_colorScoreCount_44(),
	BackgroundScript_t3530941027::get_offset_of_isChangeColor_45(),
	BackgroundScript_t3530941027::get_offset_of_colorStep_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (ColorBackChange_t4063191080), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (U3CChangeBackColorU3Ec__Iterator0_t1298991505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[6] = 
{
	U3CChangeBackColorU3Ec__Iterator0_t1298991505::get_offset_of_U3CtU3E__0_0(),
	U3CChangeBackColorU3Ec__Iterator0_t1298991505::get_offset_of_U3CratioU3E__1_1(),
	U3CChangeBackColorU3Ec__Iterator0_t1298991505::get_offset_of_U24this_2(),
	U3CChangeBackColorU3Ec__Iterator0_t1298991505::get_offset_of_U24current_3(),
	U3CChangeBackColorU3Ec__Iterator0_t1298991505::get_offset_of_U24disposing_4(),
	U3CChangeBackColorU3Ec__Iterator0_t1298991505::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (U3CChangeHazeColorU3Ec__Iterator1_t3094754686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[6] = 
{
	U3CChangeHazeColorU3Ec__Iterator1_t3094754686::get_offset_of_U3CtU3E__0_0(),
	U3CChangeHazeColorU3Ec__Iterator1_t3094754686::get_offset_of_U3CratioU3E__1_1(),
	U3CChangeHazeColorU3Ec__Iterator1_t3094754686::get_offset_of_U24this_2(),
	U3CChangeHazeColorU3Ec__Iterator1_t3094754686::get_offset_of_U24current_3(),
	U3CChangeHazeColorU3Ec__Iterator1_t3094754686::get_offset_of_U24disposing_4(),
	U3CChangeHazeColorU3Ec__Iterator1_t3094754686::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (U3CChangeCloudColorU3Ec__Iterator2_t2544988547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[6] = 
{
	U3CChangeCloudColorU3Ec__Iterator2_t2544988547::get_offset_of_U3CtU3E__0_0(),
	U3CChangeCloudColorU3Ec__Iterator2_t2544988547::get_offset_of_U3CratioU3E__1_1(),
	U3CChangeCloudColorU3Ec__Iterator2_t2544988547::get_offset_of_U24this_2(),
	U3CChangeCloudColorU3Ec__Iterator2_t2544988547::get_offset_of_U24current_3(),
	U3CChangeCloudColorU3Ec__Iterator2_t2544988547::get_offset_of_U24disposing_4(),
	U3CChangeCloudColorU3Ec__Iterator2_t2544988547::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (U3CChangeFogColorU3Ec__Iterator3_t753132848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[6] = 
{
	U3CChangeFogColorU3Ec__Iterator3_t753132848::get_offset_of_U3CtU3E__0_0(),
	U3CChangeFogColorU3Ec__Iterator3_t753132848::get_offset_of_U3CratioU3E__1_1(),
	U3CChangeFogColorU3Ec__Iterator3_t753132848::get_offset_of_U24this_2(),
	U3CChangeFogColorU3Ec__Iterator3_t753132848::get_offset_of_U24current_3(),
	U3CChangeFogColorU3Ec__Iterator3_t753132848::get_offset_of_U24disposing_4(),
	U3CChangeFogColorU3Ec__Iterator3_t753132848::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (U3CChangeCameraColorU3Ec__Iterator4_t3541598473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[6] = 
{
	U3CChangeCameraColorU3Ec__Iterator4_t3541598473::get_offset_of_U3CtU3E__0_0(),
	U3CChangeCameraColorU3Ec__Iterator4_t3541598473::get_offset_of_U3CratioU3E__1_1(),
	U3CChangeCameraColorU3Ec__Iterator4_t3541598473::get_offset_of_U24this_2(),
	U3CChangeCameraColorU3Ec__Iterator4_t3541598473::get_offset_of_U24current_3(),
	U3CChangeCameraColorU3Ec__Iterator4_t3541598473::get_offset_of_U24disposing_4(),
	U3CChangeCameraColorU3Ec__Iterator4_t3541598473::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (BackImageCamera_t3631742959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[6] = 
{
	BackImageCamera_t3631742959::get_offset_of_reverseFog_2(),
	BackImageCamera_t3631742959::get_offset_of_reverseFogColor_3(),
	BackImageCamera_t3631742959::get_offset_of_reverseFogDensity_4(),
	BackImageCamera_t3631742959::get_offset_of_reverseFogStart_5(),
	BackImageCamera_t3631742959::get_offset_of_reverseFogEnd_6(),
	BackImageCamera_t3631742959::get_offset_of_reverseFogMode_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (BackScaleScript_t207004863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[1] = 
{
	BackScaleScript_t207004863::get_offset_of_renderCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (BallRotateScript_t2206315450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (CloudScript_t436153671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[1] = 
{
	CloudScript_t436153671::get_offset_of_sprite_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (CubeScript_t387240894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[6] = 
{
	CubeScript_t387240894::get_offset_of_maxFadeDistance_2(),
	CubeScript_t387240894::get_offset_of_minFadeDistance_3(),
	CubeScript_t387240894::get_offset_of_rigid_4(),
	CubeScript_t387240894::get_offset_of_alpha_5(),
	CubeScript_t387240894::get_offset_of_isFade_6(),
	CubeScript_t387240894::get_offset_of__renderer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (DynamicBack_t1454327095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[9] = 
{
	0,
	DynamicBack_t1454327095::get_offset_of__childObject_3(),
	DynamicBack_t1454327095::get_offset_of__sprite_4(),
	DynamicBack_t1454327095::get_offset_of__tweenPosition_5(),
	DynamicBack_t1454327095::get_offset_of__tweenAlpha_6(),
	DynamicBack_t1454327095::get_offset_of__tweenScale_7(),
	DynamicBack_t1454327095::get_offset_of__tweenColor_8(),
	DynamicBack_t1454327095::get_offset_of__isLeft_9(),
	DynamicBack_t1454327095::get_offset_of__delay_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (FingerBlinkScript_t4209160530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[9] = 
{
	FingerBlinkScript_t4209160530::get_offset_of_canvas_2(),
	FingerBlinkScript_t4209160530::get_offset_of_rectTransform_3(),
	FingerBlinkScript_t4209160530::get_offset_of_isStart_4(),
	FingerBlinkScript_t4209160530::get_offset_of_t_5(),
	FingerBlinkScript_t4209160530::get_offset_of_isScaleUp_6(),
	FingerBlinkScript_t4209160530::get_offset_of_duration_7(),
	FingerBlinkScript_t4209160530::get_offset_of_minScale_8(),
	FingerBlinkScript_t4209160530::get_offset_of_maxScale_9(),
	FingerBlinkScript_t4209160530::get_offset_of__shouldUpdateScale_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (SpeedBalance_t678810912)+ sizeof (RuntimeObject), sizeof(SpeedBalance_t678810912 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[3] = 
{
	SpeedBalance_t678810912::get_offset_of_score_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpeedBalance_t678810912::get_offset_of_speed_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpeedBalance_t678810912::get_offset_of_duration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (SpawnDistance_t1428328729)+ sizeof (RuntimeObject), sizeof(SpawnDistance_t1428328729 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2743[3] = 
{
	SpawnDistance_t1428328729::get_offset_of_score_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpawnDistance_t1428328729::get_offset_of_min_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpawnDistance_t1428328729::get_offset_of_max_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (GamePlayScript_t1203922991), -1, sizeof(GamePlayScript_t1203922991_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[17] = 
{
	GamePlayScript_t1203922991_StaticFields::get_offset_of_instance_2(),
	GamePlayScript_t1203922991::get_offset_of_spawnPointCube_3(),
	GamePlayScript_t1203922991::get_offset_of_spawnPointScript_4(),
	GamePlayScript_t1203922991::get_offset_of_spawnDistance_5(),
	GamePlayScript_t1203922991::get_offset_of_distanceCount_6(),
	GamePlayScript_t1203922991::get_offset_of_isRandomSpawn_7(),
	GamePlayScript_t1203922991::get_offset_of_isSpeedUp_8(),
	GamePlayScript_t1203922991::get_offset_of_nextScrollSpeed_9(),
	GamePlayScript_t1203922991::get_offset_of_nextScrollSpeedDuration_10(),
	GamePlayScript_t1203922991::get_offset_of_maxDieCountDown_11(),
	GamePlayScript_t1203922991::get_offset_of_isOver_12(),
	GamePlayScript_t1203922991::get_offset_of_isDelayOver_13(),
	GamePlayScript_t1203922991::get_offset_of_camTransform_14(),
	GamePlayScript_t1203922991::get_offset_of_camLocalPos_15(),
	GamePlayScript_t1203922991::get_offset_of_scrollSpeed_16(),
	GamePlayScript_t1203922991::get_offset_of_speedBalance_17(),
	GamePlayScript_t1203922991::get_offset_of_spawnDistanceData_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (U3CInscreasingSpeedU3Ec__Iterator0_t3231141296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[7] = 
{
	U3CInscreasingSpeedU3Ec__Iterator0_t3231141296::get_offset_of_U3CtU3E__0_0(),
	U3CInscreasingSpeedU3Ec__Iterator0_t3231141296::get_offset_of_U3CstartSpeedU3E__0_1(),
	U3CInscreasingSpeedU3Ec__Iterator0_t3231141296::get_offset_of_U3CspeedU3E__0_2(),
	U3CInscreasingSpeedU3Ec__Iterator0_t3231141296::get_offset_of_U24this_3(),
	U3CInscreasingSpeedU3Ec__Iterator0_t3231141296::get_offset_of_U24current_4(),
	U3CInscreasingSpeedU3Ec__Iterator0_t3231141296::get_offset_of_U24disposing_5(),
	U3CInscreasingSpeedU3Ec__Iterator0_t3231141296::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[9] = 
{
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_duration_0(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U3CdU3E__0_1(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U3CtU3E__0_2(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U3CratioU3E__0_3(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U3CstartSpeedU3E__0_4(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U24this_5(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U24current_6(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U24disposing_7(),
	U3CDecreasingSpeedToZeroU3Ec__Iterator1_t3656943898::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[6] = 
{
	U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352::get_offset_of_U3CcanContinueU3E__0_0(),
	U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352::get_offset_of_duration_1(),
	U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352::get_offset_of_U3CuibsU3E__0_2(),
	U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352::get_offset_of_U24current_3(),
	U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352::get_offset_of_U24disposing_4(),
	U3CWaitToStopMoveBackgroundU3Ec__Iterator2_t2772810352::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (U3CCameraShakeU3Ec__Iterator3_t38765107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[5] = 
{
	U3CCameraShakeU3Ec__Iterator3_t38765107::get_offset_of_U3CtU3E__0_0(),
	U3CCameraShakeU3Ec__Iterator3_t38765107::get_offset_of_U24this_1(),
	U3CCameraShakeU3Ec__Iterator3_t38765107::get_offset_of_U24current_2(),
	U3CCameraShakeU3Ec__Iterator3_t38765107::get_offset_of_U24disposing_3(),
	U3CCameraShakeU3Ec__Iterator3_t38765107::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (HazeScript_t1605201346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[1] = 
{
	HazeScript_t1605201346::get_offset_of_sprite_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (PlayerScript_t1783516946), -1, sizeof(PlayerScript_t1783516946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2750[19] = 
{
	PlayerScript_t1783516946_StaticFields::get_offset_of_OnPlayerDie_2(),
	PlayerScript_t1783516946::get_offset_of_type_3(),
	PlayerScript_t1783516946::get_offset_of_moveDistance_4(),
	PlayerScript_t1783516946::get_offset_of_moveDuration_5(),
	PlayerScript_t1783516946::get_offset_of_state_6(),
	PlayerScript_t1783516946::get_offset_of_positionA_7(),
	PlayerScript_t1783516946::get_offset_of_positionB_8(),
	PlayerScript_t1783516946::get_offset_of_moveTime_9(),
	PlayerScript_t1783516946::get_offset_of_isDead_10(),
	PlayerScript_t1783516946::get_offset_of__touchId_11(),
	PlayerScript_t1783516946::get_offset_of__rendererFirst_12(),
	PlayerScript_t1783516946::get_offset_of__rendererSecond_13(),
	PlayerScript_t1783516946::get_offset_of__particle_14(),
	PlayerScript_t1783516946::get_offset_of__material_15(),
	PlayerScript_t1783516946::get_offset_of__materialComplex_16(),
	PlayerScript_t1783516946::get_offset_of__mesh_17(),
	PlayerScript_t1783516946::get_offset_of_extraShader_18(),
	PlayerScript_t1783516946::get_offset_of__rigid_19(),
	PlayerScript_t1783516946::get_offset_of__renderer_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (PlayerDie_t274471295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (PlayerType_t1512665253)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2752[3] = 
{
	PlayerType_t1512665253::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (PlayerState_t1870377573)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2753[5] = 
{
	PlayerState_t1870377573::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (TutorialSpawnPoints_t1348270317)+ sizeof (RuntimeObject), sizeof(TutorialSpawnPoints_t1348270317 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2754[2] = 
{
	TutorialSpawnPoints_t1348270317::get_offset_of_LeftPoint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TutorialSpawnPoints_t1348270317::get_offset_of_RightPoint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (SpawnPointScript_t711038611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[7] = 
{
	SpawnPointScript_t711038611::get_offset_of_cubePrefab_2(),
	SpawnPointScript_t711038611::get_offset_of_activeCubes_3(),
	SpawnPointScript_t711038611::get_offset_of_inActiveCubes_4(),
	SpawnPointScript_t711038611::get_offset_of_inactiveCount_5(),
	SpawnPointScript_t711038611::get_offset_of_lastLeft_6(),
	SpawnPointScript_t711038611::get_offset_of_lastRight_7(),
	SpawnPointScript_t711038611::get_offset_of_SamelineCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (TutorialManager_t3418541267), -1, sizeof(TutorialManager_t3418541267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2756[21] = 
{
	TutorialManager_t3418541267_StaticFields::get_offset_of_instance_2(),
	TutorialManager_t3418541267::get_offset_of__gamePlayScript_3(),
	TutorialManager_t3418541267::get_offset_of_playerLeft_4(),
	TutorialManager_t3418541267::get_offset_of_playerRight_5(),
	TutorialManager_t3418541267::get_offset_of__pLeft_6(),
	TutorialManager_t3418541267::get_offset_of__pRight_7(),
	TutorialManager_t3418541267::get_offset_of_fingerLeft_8(),
	TutorialManager_t3418541267::get_offset_of_fingerRight_9(),
	TutorialManager_t3418541267::get_offset_of_tutTexts_10(),
	TutorialManager_t3418541267::get_offset_of_tutorialSpawnPoints_11(),
	TutorialManager_t3418541267::get_offset_of_tutorialMaxRowsCube_12(),
	TutorialManager_t3418541267::get_offset_of_state_13(),
	TutorialManager_t3418541267::get_offset_of_movingLeft_14(),
	TutorialManager_t3418541267::get_offset_of_movingRight_15(),
	TutorialManager_t3418541267::get_offset_of_isDelay_16(),
	TutorialManager_t3418541267::get_offset_of__timeDelay_17(),
	TutorialManager_t3418541267::get_offset_of_tapCount_18(),
	TutorialManager_t3418541267::get_offset_of_lastTimeScale_19(),
	TutorialManager_t3418541267::get_offset_of_isPause_20(),
	TutorialManager_t3418541267::get_offset_of_demoPlay_21(),
	TutorialManager_t3418541267::get_offset_of_isPass_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (TutorialState_t1806054907)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2757[20] = 
{
	TutorialState_t1806054907::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (U3CMovePlayerU3Ec__Iterator0_t1481740848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[10] = 
{
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_script_0(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_U3CtU3E__0_1(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_U3CdurationU3E__0_2(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_dir_3(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_U3CposXU3E__1_4(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_isState_5(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_U24this_6(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_U24current_7(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_U24disposing_8(),
	U3CMovePlayerU3Ec__Iterator0_t1481740848::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (U3CDelayTimeU3Ec__Iterator1_t3935045367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[7] = 
{
	U3CDelayTimeU3Ec__Iterator1_t3935045367::get_offset_of_U3CcountDelayTimeU3E__0_0(),
	U3CDelayTimeU3Ec__Iterator1_t3935045367::get_offset_of_maxTime_1(),
	U3CDelayTimeU3Ec__Iterator1_t3935045367::get_offset_of_isState_2(),
	U3CDelayTimeU3Ec__Iterator1_t3935045367::get_offset_of_U24this_3(),
	U3CDelayTimeU3Ec__Iterator1_t3935045367::get_offset_of_U24current_4(),
	U3CDelayTimeU3Ec__Iterator1_t3935045367::get_offset_of_U24disposing_5(),
	U3CDelayTimeU3Ec__Iterator1_t3935045367::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (TutorialPointScript_t2361877884), -1, sizeof(TutorialPointScript_t2361877884_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2760[4] = 
{
	TutorialPointScript_t2361877884_StaticFields::get_offset_of_OnTriggerTutorialPoint_2(),
	TutorialPointScript_t2361877884_StaticFields::get_offset_of_OnTriggerTutorialPassPoint_3(),
	TutorialPointScript_t2361877884::get_offset_of_rigibody_4(),
	TutorialPointScript_t2361877884::get_offset_of_collisionCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (TriggerTutorial_t2088781469), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (UIBackScript_t2417306190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[6] = 
{
	UIBackScript_t2417306190::get_offset_of_mountainPrefab_2(),
	UIBackScript_t2417306190::get_offset_of_NumOfMountain_3(),
	UIBackScript_t2417306190::get_offset_of_cloud1_4(),
	UIBackScript_t2417306190::get_offset_of_cloud2_5(),
	UIBackScript_t2417306190::get_offset_of_timeCount_6(),
	UIBackScript_t2417306190::get_offset_of_listDynamicBacks_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (RecyclingObject_t2513625666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (RecyclingObjectPool_t1893803714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[2] = 
{
	RecyclingObjectPool_t1893803714::get_offset_of__originalDict_2(),
	RecyclingObjectPool_t1893803714::get_offset_of__pool_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (BGMManager_t1790042108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[2] = 
{
	BGMManager_t1790042108::get_offset_of__lastBGMName_13(),
	BGMManager_t1790042108::get_offset_of__isFadeOut_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (SEManager_t324035629), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (MainManager_t729209856), -1, sizeof(MainManager_t729209856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2768[2] = 
{
	MainManager_t729209856::get_offset_of_showAdWhenRetry_4(),
	MainManager_t729209856_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (SceneNavigator_t2956304001), -1, sizeof(SceneNavigator_t2956304001_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2769[9] = 
{
	SceneNavigator_t2956304001::get_offset_of__isFading_4(),
	SceneNavigator_t2956304001::get_offset_of__afterSceneName_5(),
	SceneNavigator_t2956304001::get_offset_of__beforeSceneName_6(),
	SceneNavigator_t2956304001::get_offset_of_OnFinishedFadeOut_7(),
	SceneNavigator_t2956304001::get_offset_of_OnLoadedScene_8(),
	SceneNavigator_t2956304001::get_offset_of__titleCount_9(),
	SceneNavigator_t2956304001::get_offset_of__fadeTransition_10(),
	SceneNavigator_t2956304001_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	SceneNavigator_t2956304001_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (GameManager_t1536523654), -1, sizeof(GameManager_t1536523654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2770[20] = 
{
	GameManager_t1536523654::get_offset_of__score_4(),
	GameManager_t1536523654::get_offset_of_playMode_5(),
	GameManager_t1536523654::get_offset_of_gamePlay_6(),
	GameManager_t1536523654::get_offset_of_background_7(),
	GameManager_t1536523654::get_offset_of_dialogContinue_8(),
	GameManager_t1536523654::get_offset_of_noThanksButton_9(),
	GameManager_t1536523654::get_offset_of_labelNewBall_10(),
	GameManager_t1536523654::get_offset_of_labelNewStage_11(),
	GameManager_t1536523654::get_offset_of_labelNewStageTop_12(),
	GameManager_t1536523654::get_offset_of__theme_13(),
	GameManager_t1536523654::get_offset_of__snowEffect_14(),
	GameManager_t1536523654::get_offset_of__alreadyContinue_15(),
	GameManager_t1536523654::get_offset_of__canShowPauseAds_16(),
	GameManager_t1536523654::get_offset_of__isPlayingGame_17(),
	GameManager_t1536523654::get_offset_of_OnGameEnd_18(),
	GameManager_t1536523654::get_offset_of_OnChangeScore_19(),
	GameManager_t1536523654_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
	GameManager_t1536523654_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_21(),
	GameManager_t1536523654_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_22(),
	GameManager_t1536523654_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (PlayMode_t2810632737)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2771[4] = 
{
	PlayMode_t2810632737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (U3CShowContinueU3Ec__AnonStorey0_t388195091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[1] = 
{
	U3CShowContinueU3Ec__AnonStorey0_t388195091::get_offset_of_button_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (AdManager_t2410889370), -1, sizeof(AdManager_t2410889370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2773[8] = 
{
	AdManager_t2410889370::get_offset_of__adPlugin_4(),
	0,
	0,
	AdManager_t2410889370::get_offset_of__canShowSplash_7(),
	AdManager_t2410889370::get_offset_of__canShowVideo_8(),
	AdManager_t2410889370::get_offset_of__isRevewing_9(),
	AdManager_t2410889370::get_offset_of__ranking_10(),
	AdManager_t2410889370_StaticFields::get_offset_of_SCENE_NO_DICT_FOR_AD_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (U3CDelayPauseButtonU3Ec__Iterator0_t3539102654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[3] = 
{
	U3CDelayPauseButtonU3Ec__Iterator0_t3539102654::get_offset_of_U24current_0(),
	U3CDelayPauseButtonU3Ec__Iterator0_t3539102654::get_offset_of_U24disposing_1(),
	U3CDelayPauseButtonU3Ec__Iterator0_t3539102654::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (AdPlugin_t1451289319), -1, sizeof(AdPlugin_t1451289319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2775[1] = 
{
	AdPlugin_t1451289319_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (CommonNativeManager_t3006429997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[4] = 
{
	CommonNativeManager_t3006429997::get_offset_of__commonNativePlugin_4(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (CommonNativePlugin_t3889574804), -1, sizeof(CommonNativePlugin_t3889574804_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2777[2] = 
{
	0,
	CommonNativePlugin_t3889574804_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (RankingManager_t1599285360), -1, sizeof(RankingManager_t1599285360_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2778[3] = 
{
	RankingManager_t1599285360::get_offset_of__rankingPlugin_4(),
	RankingManager_t1599285360::get_offset_of_OnShowLeaderBoard_5(),
	RankingManager_t1599285360_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (RankingPlugin_t2338328566), -1, sizeof(RankingPlugin_t2338328566_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2779[1] = 
{
	RankingPlugin_t2338328566_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (ShareLocation_t3603649529)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2780[5] = 
{
	ShareLocation_t3603649529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (ShareManager_t2456449165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[1] = 
{
	ShareManager_t2456449165::get_offset_of__sharePlugin_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (SharePlugin_t456476949), -1, sizeof(SharePlugin_t456476949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2782[1] = 
{
	SharePlugin_t456476949_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (EnumFlagsAttribute_t3331586256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (GameSample_t1080372905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[1] = 
{
	GameSample_t1080372905::get_offset_of__cubeNum_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (SampleCube_t1605149290), -1, sizeof(SampleCube_t1605149290_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2785[2] = 
{
	SampleCube_t1605149290::get_offset_of_OnClick_2(),
	SampleCube_t1605149290_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (TouchEventSample_t2102076809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[1] = 
{
	TouchEventSample_t2102076809::get_offset_of__UpdateSelected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (CameraSizeAdjuster_t3580329891), -1, sizeof(CameraSizeAdjuster_t3580329891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2787[3] = 
{
	CameraSizeAdjuster_t3580329891::get_offset_of__basisCameraSize_8(),
	CameraSizeAdjuster_t3580329891::get_offset_of__basisAspectRateType_9(),
	CameraSizeAdjuster_t3580329891_StaticFields::get_offset_of_ASPECT_RATE_DICT_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (AspectRateType_t2035530557)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2788[3] = 
{
	AspectRateType_t2035530557::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (UIAdjuster_t2553577866), -1, sizeof(UIAdjuster_t2553577866_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2789[6] = 
{
	UIAdjuster_t2553577866::get_offset_of__currentAspectRate_2(),
	0,
	0,
	UIAdjuster_t2553577866_StaticFields::get_offset_of_SCREEN_SIZE_DEFULT_5(),
	UIAdjuster_t2553577866_StaticFields::get_offset_of_SCREEN_SIZE_640_960_6(),
	UIAdjuster_t2553577866_StaticFields::get_offset_of_SCREEN_SIZE_640_1136_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (AspectRateType_t2201292060)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[3] = 
{
	AspectRateType_t2201292060::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (UIPositionAdjuster_t4076901380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[5] = 
{
	UIPositionAdjuster_t4076901380::get_offset_of__rectTransform_8(),
	UIPositionAdjuster_t4076901380::get_offset_of__baseAspectRateType_9(),
	UIPositionAdjuster_t4076901380::get_offset_of__baisisPos_10(),
	UIPositionAdjuster_t4076901380::get_offset_of__moveDistance_11(),
	UIPositionAdjuster_t4076901380::get_offset_of__iPadBaisisPos_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (UIScaleAdjuster_t982702719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[4] = 
{
	UIScaleAdjuster_t982702719::get_offset_of__rectTransform_8(),
	UIScaleAdjuster_t982702719::get_offset_of__baseAspectRateType_9(),
	UIScaleAdjuster_t982702719::get_offset_of__scalingValue_10(),
	UIScaleAdjuster_t982702719::get_offset_of__iPadScale_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (BestLabel_t2479364186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[1] = 
{
	BestLabel_t2479364186::get_offset_of__textFormat_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (AppStoreButtonAssistant_t455463633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[1] = 
{
	AppStoreButtonAssistant_t455463633::get_offset_of__location_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (ButtonAssistant_t3922690862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[3] = 
{
	ButtonAssistant_t3922690862::get_offset_of__collider_2(),
	ButtonAssistant_t3922690862::get_offset_of__button_3(),
	ButtonAssistant_t3922690862::get_offset_of__image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (ButtonCollider_t3142833335), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (HouseAdButtonAssistant_t1278736859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[2] = 
{
	0,
	HouseAdButtonAssistant_t1278736859::get_offset_of__newBadge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (TweetButtonAssistant_t1668838560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[2] = 
{
	TweetButtonAssistant_t1668838560::get_offset_of__location_5(),
	TweetButtonAssistant_t1668838560::get_offset_of__canTap_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (U3CStartU3Ec__Iterator0_t1337170120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[4] = 
{
	U3CStartU3Ec__Iterator0_t1337170120::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1337170120::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1337170120::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1337170120::get_offset_of_U24PC_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
