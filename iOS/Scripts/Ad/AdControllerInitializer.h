//
//  AdControllerInitializer.h
//  Unity-iPhone
//
//  Created by kikuchikan on 2015/10/29.
//
//  各広告のコントローラーを初期化するクラス

#import "InterstitialAdController.h"
#import "BannerAdController.h"
#import "RectangleAdController.h"
#import "MovieAdController.h"

@interface AdControllerInitializer : NSObject;

//バナー広告コントローラー初期化
+(InterstitialAdController*)initBaseInterstitialAdController;
+(InterstitialAdController*)initRankInterstitialAdController;

+(BannerAdController*)initBannerAdController;
+(BannerAdController*)initResultBigBannerAdController;

+(RectangleAdController*)initPauseRectangleAdController;
+(RectangleAdController*)initEndRectangleAdController;
+(RectangleAdController*)initGameRectangleAdController;
+(RectangleAdController*)initPopupRectangleAdController;

//動画広告コントローラー初期化
+(MovieAdController*)initMovieAdController;

@end
