//
//  BannerAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Adstirのバナー広告のコントローラー

#import "MoPubBannerHelper.h"
#import "CommonConfig.h"
#import <ADG/ADGSettings.h>
#import <UIKit/UIKit.h>

@implementation MoPubBannerHelper

//バナーアドを初期化
-(UIView*)prepareBannerAd{
    
    self.view = [[UIView alloc] initWithFrame:CGRectMake(UnityGetGLViewController().view.frame.size.width/2-160, UnityGetGLViewController().view.frame.size.height - 50.0f, 320, 50)];
    self.view.backgroundColor = [UIColor clearColor];
    
    MPAdView *adView = [[MPAdView alloc] initWithAdUnitId:MOPUB_BANNER_ID
                                                size:MOPUB_BANNER_SIZE];
    adView.delegate = self;    
    adView.frame = CGRectMake((self.view.bounds.size.width - MOPUB_BANNER_SIZE.width) / 2,
                                   self.view.bounds.size.height - MOPUB_BANNER_SIZE.height,
                                   MOPUB_BANNER_SIZE.width, MOPUB_BANNER_SIZE.height);
    [self.view addSubview:adView];
    [adView loadAd];
    
    return self.view;
}

#pragma mark - <MPAdViewDelegate>
- (UIViewController *)viewControllerForPresentingModalView {
    return UnityGetGLViewController();
}

- (void)adViewDidLoadAd:(MPAdView *)view
{
    NSLog(@"MoPub banner: did load");
}

- (void)adViewDidFailToLoadAd:(MPAdView *)view
{
    NSLog(@"MoPub banner: failed load");
}

- (void)willPresentModalViewForAd:(MPAdView *)view
{
    NSLog(@"MoPub banner: will present");
}

- (void)didDismissModalViewForAd:(MPAdView *)view
{
    NSLog(@"MoPub banner: dimiss modal");
}

- (void)willLeaveApplicationFromAd:(MPAdView *)view
{
    NSLog(@"MoPub banner: will leave application");
}
@end
