﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// オーディオを管理するマネージャクラスの親クラス
/// </summary>
public class AudioManager<T> : SingletonMonoBehaviour<T> where T : MonoBehaviourWithInit
{
	//ピッチ
	protected float _pitch = 1.0f;

	//次流すBGM名、SE名
	protected string _nextAudioName;

	//ミュート中か
	protected bool _isMute;

	//オーディオソースのリストとその設定
	protected List<AudioSource> _audioSourceList = new List<AudioSource> ();
	protected int _audioSourceNum;
	protected float _volumeDefult;
	protected bool _isLoop;

	//全AudioのDicとAudioがあるディレクトリへのパス
	protected Dictionary<string, AudioClip> _audoClipDic = new Dictionary<string, AudioClip> ();
	protected string _audioDirectoryPath;

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Init ()
	{
		base.Init ();

		//このオブジェクトにオーディオリスナーが無ければ作成
		if (!gameObject.GetComponent<AudioListener> ()) {
			gameObject.AddComponent<AudioListener> ();
		}

		//オーディオソースを作成、設定
		for (int i = 0; i < _audioSourceNum; i++) {
			AudioSource audioSource = gameObject.AddComponent<AudioSource> ();

			audioSource.playOnAwake = false;
			audioSource.volume = _volumeDefult;
			audioSource.loop = _isLoop;

			_audioSourceList.Add (audioSource);

		}

		//リソースフォルダから全オーディオファイルを読み込みセット
		object[] audioClipArray = Resources.LoadAll (_audioDirectoryPath);

		foreach (AudioClip audioClip in audioClipArray) {
			_audoClipDic [audioClip.name] = audioClip;
		}
	}

	protected virtual void Start ()
	{
		_isMute = UserData.IsMute;
		UserData.OnChangeMuteSetting += SetIsMute;
	}

	//=================================================================================
	//ミュート関係
	//=================================================================================

	//ミュートのON、OFFを設定する
	protected virtual void SetIsMute (bool isMute)
	{
		_isMute = isMute;
	}

	//=================================================================================
	//ピッチ関係
	//=================================================================================

	/// <summary>
	/// ピッチを変更する
	/// </summary>
	public virtual void ChangePitch (float pitch)
	{
		if (_pitch == pitch) {
			return;
		}
		_pitch = pitch;

		foreach (AudioSource audioSource in _audioSourceList) {
			audioSource.pitch = pitch;
		}
	}

	//=================================================================================
	//継承先で実装
	//=================================================================================
	protected virtual bool IsBGM ()
	{
		return true;
	}

	protected virtual void muteAll ()
	{

		foreach (AudioSource audioSource in _audioSourceList) {
			audioSource.mute = true;
		}
	}

	protected virtual void resumeAll ()
	{

		foreach (AudioSource audioSource in _audioSourceList) {
			audioSource.mute = false;
		}
	}
}