﻿//  GameScoreLabel.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.27.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲームシーンでスコアを表示するラベル
/// </summary>
[ExecuteInEditMode]
public class GameScoreLabel : MonoBehaviour
{

	//テキストのフォーマット
	[SerializeField]
	private string _textFormat = "{0}";

	UILabel _label;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start ()
	{
		_label = GetComponent<UILabel> ();

		if (PlayerPrefs.GetInt (Define.CONTINUE_GAME, 0) == 1) {
			
			UpdateText (PlayerPrefs.GetInt (Define.LAST_SCORE, 0));

			GameManager.Instance.OnChangeScore += UpdateText;

		} else {

			UpdateText (0);

			GameManager.Instance.OnChangeScore += UpdateText;
		}
	}

	private void UpdateText (int score)
	{
		_label.text = string.Format (_textFormat, score);
	}

	#if UNITY_EDITOR
	private void Update ()
	{
		if (Application.isPlaying) {
			return;
		}

		//Mode時は表示しないように
		_label.enabled = UnityProjectSetting.Entity.GameType != GameType.LevelSelect;
		UpdateText (0);
	}
	#endif

}