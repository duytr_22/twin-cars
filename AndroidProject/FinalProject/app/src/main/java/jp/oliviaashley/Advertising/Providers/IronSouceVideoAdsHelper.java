package jp.oliviaashley.Advertising.Providers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;
import com.unity3d.player.UnityPlayer;

import jp.oliviaashley.Advertising.CheckSplSpfJson;
import jp.oliviaashley.Advertising.FlagsManager;
import jp.oliviaashley.Twins.R;

public class IronSouceVideoAdsHelper {

    private static final String TAG = "IronSouceVideoAdsHelper";

    private  static  boolean isSuccess = false;
    private static Activity _activity;
    public static void doOnCreate(Activity activity) {

        _activity = activity;
        loadVideo();
    }

    public static void loadVideo(){

        IronSource.setRewardedVideoListener(new RewardedVideoListener() {
            @Override
            public void onRewardedVideoAdOpened() {

                Log.v(TAG, "RewardVideo Ad Opened...");
            }

            @Override
            public void onRewardedVideoAdClosed() {

                Log.v(TAG, "RewardVideo Ad Closed..." + isSuccess);

                if(isSuccess)
                {
                    // Save last mil
                    SharedPreferences sharedPreferences = _activity.getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putLong("Interstitial_delay", System.currentTimeMillis());
                    editor.commit();

                    UnityPlayer.UnitySendMessage("NativeManager", "FinishedPlayingMovieAd", "true");
                }
                else
                    UnityPlayer.UnitySendMessage("NativeManager", "FinishedPlayingMovieAd", "false");

                isSuccess = false;
            }

            @Override
            public void onRewardedVideoAvailabilityChanged(boolean b) {

                Log.v(TAG, "RewardVideo Ad Availability changed ..." + b);
            }

            @Override
            public void onRewardedVideoAdStarted() {

                Log.v(TAG, "RewardVideo Ad Started...");
            }

            @Override
            public void onRewardedVideoAdEnded() {

                Log.v(TAG, "RewardVideo Ad Ended...");
            }

            @Override
            public void onRewardedVideoAdRewarded(Placement placement) {

                isSuccess = true;
                Log.v(TAG, "RewardVideo Ad Rewarded ..." + placement);
            }

            @Override
            public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {

                Log.v(TAG, "RewardVideo Ad show failed ..." + ironSourceError);
            }

            @Override
            public void onRewardedVideoAdClicked(Placement placement) {

                Log.v(TAG, "RewardVideo Ad clicked ..." + placement);
            }
        });
    }

    //動画広告を表示
    public static void showVideoAd(){

        Log.v(TAG, "showVideoAd(canshow : " + IronSource.isRewardedVideoAvailable()+ ")");

        if (IronSource.isRewardedVideoAvailable()) {

            _activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    IronSource.showRewardedVideo();
                }
            });
        }
        else{
            UnityPlayer.UnitySendMessage("NativeManager", "FinishedPlayingMovieAd", "false");
        }
    }


    public static boolean CanShowRewardVideo()
    {
        return IronSource.isRewardedVideoAvailable();
    }

    public static boolean isCanShow()
    {

        return IronSource.isRewardedVideoAvailable();
    }
}
