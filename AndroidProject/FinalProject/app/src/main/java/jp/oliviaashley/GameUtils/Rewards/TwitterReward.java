package jp.oliviaashley.GameUtils.Rewards;


import android.app.Activity;

import jp.oliviaashley.GameUtils.Rewards.TwitterRewardHelper.TwitterRewardListener;


public class TwitterReward {
    /**
     * (Java -> JNI -> C++)ツイートしたかの結果
     *
     * @param result ... true:ツイート完了, false:ツイート未完了(キャンセル・中断 etc)
     */
    private static native void tweetCompleted(final boolean result);


    //テキストのみツイート
    public static void launchTwitter(Activity activity, final String msg) {
        TwitterRewardListener listener = new TwitterRewardListener() {
            @Override
            public void onTweet(boolean result) {
                //従来の処理 (JNI処理へ)
                TwitterReward.tweetCompleted(result);
            }
        };

        TwitterRewardHelper.launchTwitter(activity, msg, listener);
    }


    //画像付きテキストをツイート
    public static void launchTwitter(Activity activity, final String msg, final String filePath) {
        TwitterRewardListener listener = new TwitterRewardListener() {
            @Override
            public void onTweet(boolean result) {
                //従来の処理 (JNI処理へ)
                TwitterReward.tweetCompleted(result);
            }
        };

        TwitterRewardHelper.launchTwitter(activity, msg, filePath, listener);
    }
}
