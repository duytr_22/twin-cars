//
//  CommonConfig.h
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  諸々の設定を定義するファイル

#ifndef CommonConfig
#define CommonConfig

#import "GAEConfig.h"

//GAE URL
#define FMS_HOST @"flagsforapps.appspot.com";

//ポップアップ用アプリID
#define APP_ID @"DOC"//TST test.ver

//GAE ID
#define POP_ID @"POPNEW05"
#define POP_ID_E @"POPNEW05E"
#define END_ID @"END05"
#define SPL_ID @"SPL05"

//iOS_ID
#define IOS_APP_ID (1144020719)

//ランキングID(基本的に1を使う。2,3は複数のランキングがある場合)
#define RANKING_KEY_1 @"bestscore_twins1"

//=================================================================================
//シーンの設定
//=================================================================================

//各シーンの番号
enum SceneNo{
    SCENE_TITLE        = 0,
    SCENE_GAME         = 1,
    SCENE_RESULT       = 2,
    SCENE_TUTORIAL     = 3,
    SCENE_BALL         = 4,
    
    SCENE_TITLE_HELP   = 1000,
    SCENE_PAUSE        = 10000,
};

//=================================================================================
//レビュー
//=================================================================================
#pragma mark REVIEW

#define REVIEW_SHOW_COUNT (5)   // 動作は REVIEW_SHOW_COUNT +1 です。
#define POPUP_TITLE @"新記録おめでとうございます！"
#define POPUP_MESSAGE @"\nもしよかったら\nレビューを書いてもらえますか？"

#define POPUP_OK_BUTTON_TITLE @"レビューを書く"
#define POPUP_OK_OTHER_BUTTON_TITLE @"二度と表示しない"
#define POPUP_OK_CANCEL_BUTTON_TITLE @"キャンセル"

//Ad Layout Type
//自社広告を出す場合は有効に、ネイティブ広告を出す場合はコメントアウトしてください
//#define AD_LAYOUT_TYPE_A

//=================================================================================
//広告
//=================================================================================
#pragma mark AD

//AdStir
#define ADSTIR_ID                       @"MEDIA-afcb8c03"
#define ADSTIR_SPOT_NO                    (1)

#define ADSTIR_VIDEO_KEY                @"MEDIA-e119082b"

//*** i-mobile ***
#define IMOBILE_PUBLISHER_ID            @"46357"
#define IMOBILE_MEDIA_ID                @"298390"
//*** i-mobile bannar ***
#define IMOBILE_SPOTID_BANNER            @"988758"
//*** i-mobile rectangle ***
#define IMOBILE_SPOTID_RECTANGLE        @"1449440"
#define IMOBILE_SPOTID_RECTANGLE_END    @"1449441"
//*** i-mobile interstitial ***
#define IMOBILE_SPOTID_INTERSTITIAL        @"1449442"
#define IMOBILE_SPOTID_FULLSCREEN        @"1449443"

#define DFP_UNITID_BANNER               @"/9176203/1449438"
#define DFP_UNITID_RECTANGLE            @"/9176203/1449440"
#define DFP_UNITID_RECTANGLE_END        @"/9176203/1449441"

// Nend banner
#define NEND_APIKEY_BANNER                @"28df4d1f942fe238a8183a273e5b472f87147b4e"
#define NEND_SPOTID_BANNER                @"621190"
// Nend rectangle
#define NEND_APIKEY_RECTANGLE            @"9a8a4d3b30cf20a8332e8317f9dfa63889b8cf16"
#define NEND_SPOTID_RECTANGLE            @"799167"
#define NEND_APIKEY_RECTANGLE_END        @"9c7d2fabf4914fd4922256ee2b1e8cf544b99c16"
#define NEND_SPOTID_RECTANGLE_END        @"799170"
// Nend interstitial
#define NEND_APIKEY_INTERSTITIAL        @"d187fe98586456790da186d1da9a59b973ba704a"
#define NEND_SPOTID_INTERSTITIAL        @"799171"
// Nend startup_interstitial(common id)
#define NEND_APIKEY_STARTUP                @"950e17b17cf8497c2e393e29a3dc11ae31bb1eaa"
#define NEND_SPOTID_STARTUP                @"799173"
// Nend Native Banner
#define NEND_APIKEY_NATIVE_BANNER        @"10d9088b5bd36cf43b295b0774e5dcf7d20a4071"
#define NEND_SPOTID_NATIVE_BANNER        @"485500"
// Nend Native Rectは、/Advertising/AdNative/html/nendjs.htmlに設定してください
// Nend Full screen
#define NEND_APIKEY_FULLSCREEN          @"a827ec8c76aab225e2519ec18581ae86337d8bd3"
#define NEND_SPOTID_FULLSCREEN          @"832774"

// Five
#define FIVE_APP_ID                     @"897357"
#define FIVE_SPOTID_RECTANGLE_POPUP     @"951817"
#define FIVE_SPOTID_RECTANGLE_GAME      @"274925"
#define FIVE_SPOTID_RECTANGLE_END       @"210339"

// Adbrix
#define ADBRIX_APP_KEY                  @"48214526"
#define ADBRIX_HASH_KEY                 @"c4e0acc264fc4505"

//FLurry
#define FLURRY_API_KEY                    @"YWW76DGFP9BJSBQN96TQ"

#define FLURRY_EVENT_HEADER_TAPPED        (@"POPUP_TAP_YES")
#define FLURRY_EVENT_HEADER_SHOW        (@"POPUP_SHOW")

// Heyzap
#define HEYZAP_PUBLISHER_ID                @"db6ad6bfdfe629ed12e9cf4bd1f41c7d"
#define HEYZAP_HOUSEAD_TAG                @"housead_tag"
#define HEYZAP_INTERSTITIAL_TAG            @"interstitial_tag"

//AdGeneration
#define ADG_ID_BANNER                   @"58014"
#define ADG_ID_BIG_BANNER               @"51774"
#define ADG_ID_BIG_BANNER_EN            @"51776"
#define ADG_ID_RECTANGLE                @"58037"
#define ADG_ID_RECTANGLE_END            @"58037"
#define ADG_ID_RECTANGLE_GAME           @"58037"
#define ADG_ID_RECTANGLE_POPUP          @"58037"
#define ADG_ID_VIDEO                    @"57990"

//Admob
#define ADMOB_BANNER                    @"ca-app-pub-4523211369136078/4472024918"
#define ADMOB_RECTANGLE                 @"ca-app-pub-4523211369136078/1783062649"
#define ADMOB_RECTANGLE_END             @"ca-app-pub-4523211369136078/1783062649"
#define ADMOB_RECTANGLE_POPUP           @"ca-app-pub-4523211369136078/1783062649"
#define ADMOB_INTERSTITIAL              @"ca-app-pub-4523211369136078/4827248131"
#define ADMOB_VIDEO                     @"ca-app-pub-4523211369136078/8028126543"

// Unity
#define UNITY_ID                        @"1724945"
#define UNITY_VIDEO                     @"rewardedVideo"
#define UNITY_INTERSTITIAL              @"video"

//IronSource
#define IRONSOURCE_API_KEY              @"724aaf95"
#define IRONSOURCE_INTERSTITIAL         @"Interstitial"
#define IRONSOURCE_REWARDVIDEO          @"RewardedVideo"

//Mopub Mediation


#define UNITY_OBJECT_NAME "NativeManager"
#define MOPUB_APP_ID                   @"468f83e9c40e486bb16c45ecffa68704"
#define MOPUB_BANNER_ID                @"e7927ba63dd74045b020092607cbca55"
#define MOPUB_INTERSTITIAL_ID          @"7f5096f9751d4f9ab718be6c7c8ddd65"
#define MOPUB_VIDEO_ID                 @"37f1814435734bdc90c7729882225aae"

//=================================================================================
//他
//=================================================================================

//Rectangle Ad Size
#define RECT_AD_WIDTH  300
#define RECT_AD_HEIGHT 250

//ログ非表示用マクロ
//#ifdef DEBUG_LOG
//# define NSLog(...) NSLog(__VA_ARGS__);
//#else
//# define NSLog(...)
//#endif

#endif
