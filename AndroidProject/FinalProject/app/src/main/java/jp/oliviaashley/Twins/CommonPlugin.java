package jp.oliviaashley.Twins;

import android.app.Activity;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import jp.oliviaashley.Advertising.Providers.FlurryHelper;
import jp.oliviaashley.Advertising.Providers.MovieAdsHelper;
import jp.oliviaashley.Advertising.SceneManager;
import jp.oliviaashley.Advertising.SplashAdManager;
import jp.oliviaashley.Social.GooglePlayServices.GooglePlayServicesManager;
import jp.oliviaashley.Social.RateApp;
import jp.oliviaashley.Social.ShareUtils;

/*
 * CommonPlugin.java
 */

public class CommonPlugin {

    private static final String TAG = "CommonPlugin";

    private static Activity activity;

    //初期化
    public CommonPlugin() {
        Log.d(TAG, "CommonPlugin");
        activity = UnityPlayer.currentActivity;
    }

    //=================================================================================
    //コモン
    //=================================================================================

    //ウェブページ表示(日本)
    public void showCommonWebJPN() {
        String path = "";
        Log.d(TAG, "launchUrl:" + path);
        ShareUtils.launchUrl(activity, path);
    }

    //ウェブページ表示(海外)
    public void showCommonWebUSA() {
        String path = "";
        Log.d(TAG, "launchUrl:" + path);
        ShareUtils.launchUrl(activity, path);
    }

    //レビュー催促を表示する
    public void showReviewPopup() {
        Log.d(TAG, "showReviewPopup");
//        RateApp.showRateDialog(activity);
    }

    //レビューページを表示する
    public void showReviewPage() {
        Log.d(TAG, "showReviewPage");
        RateApp.rate(activity);
    }

    //Flurryにイベント送信
    public void reportToFlurry(String key) {
        Log.d(TAG, "reportToFlurry:" + key);
        FlurryHelper.reportFlurryWithEvent(key);
    }

    //=================================================================================
    //シェア
    //=================================================================================

    //ツイート
    public void launchTwitterWithMessage(String msg) {
        Log.d(TAG, "launchTwitterWithMessage:" + msg);
        ShareUtils.launchTwitterWithMessage(activity, msg);
    }

    //=================================================================================
    //ランキング
    //=================================================================================

    //リーダボード表示
    public void showLeaderBoard(int mode) {
        Log.d(TAG, "showLeaderBoard:" + mode);
        GooglePlayServicesManager.showWorldLeaderboard(activity);
    }

    //ランキングにスコア送信
    public void reportScore(String id, int score) {
        Log.d(TAG, "reportScore:" + id + ":" + score);
        GooglePlayServicesManager.pushAcomplishements(activity, score, Integer.parseInt(id));
    }

    public void reportScore(int id, int score) {
      GooglePlayServicesManager.pushAcomplishements(activity, score, id);
    }

    //=================================================================================
    //広告
    //=================================================================================

    //広告の表示設定
    public void updateAd(int scene) {
        Log.d(TAG, "updateAd:" + scene);
        SceneManager.setScene(activity, scene);
    }

    //タイトルに戻った時、ランキングボタンを押した時に表示するスプラッシュ広告
    public void showOptionalAdRank() {
        Log.d(TAG, "showOptionalAdRank");
        SplashAdManager.SplashAdListener listener = new SplashAdManager.SplashAdListener() {
            @Override
            public void onClose() {
                Log.d(TAG, "showOptionalAdRank ... onClose");

                //暫定処置
                SceneManager.setNativeBannerVisible(activity);
            }
        };

        SplashAdManager.showOptionalAdRank(activity, listener);
    }

    //リザルトで表示するスプラッシュ広告
    public void showOptionalAd(int adCount) {
        Log.d(TAG, "showOptionalAd:" + adCount);
        SplashAdManager.SplashAdListener listener = new SplashAdManager.SplashAdListener() {
            @Override
            public void onClose() {
                Log.d(TAG, "showOptionalAd ... onClose");

                //暫定処置
                SceneManager.setNativeBannerVisible(activity);
            }
        };

        SplashAdManager.showOptionalAd(activity, adCount, listener);
    }

    //終了時の広告
    public void showExitAd() {
    }

    //ウォール広告の表示
    public void showWallAd() {
    }

    public void Buy() {

        Log.v(TAG, "Buy");

        GooglePlayServicesManager.buyItem(activity.getResources().getString(R.string.Item_No_Ads));
    }

    public void Restore() {

        Log.v(TAG, "Restore");

        GooglePlayServicesManager.restoreItem();
    }

    public  void HideGameRect() {

        Log.v(TAG, "HideGameRect");

        SceneManager.hideVideoRect(activity);
    }

    public  void ShowGameRect()
    {
        Log.v(TAG, "ShowGameRect");

        SceneManager.showVideoRect(activity);
    }

    public void ShowIndicator()
    {
        Log.v("THS", "ShowIndicator");
    }

    public void HideIndicator()
    {
        Log.v("THS", "HideIndicator");
    }

    public void HideAllAds()
    {
        Log.v(TAG, "HideAllAds");

        SceneManager.HideAllAds(activity);
    }

    public static boolean _CanShowRewardVideo()
    {
        Log.v(TAG, "_CanShowRewardVideo");

        return MovieAdsHelper.IsCanShowMovieAds(activity);
    }

    public  void ShowRewardVideo()
    {
        Log.v(TAG, "ShowRewardVideo");

        MovieAdsHelper.ShowMovieAds(activity);
    }

    public  void GetRankingValue()
    {
        Log.v(TAG, "GetRankingValue");

        GooglePlayServicesManager.getUserRanking(activity);
    }

    public void ShowPopupRect()
    {
        SceneManager.setScene(activity, -1);
        SceneManager.showPopupRect(activity);
    }

    public void HidePopupRect()
    {
        SceneManager.setScene(activity, 4);
        SceneManager.hidePopupRect(activity);
    }
}
