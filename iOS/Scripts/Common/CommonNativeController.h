//
//  CommonNativeController.h
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  ネイティブ連携の汎用的な処理を行うクラス

#import <Twitter/Twitter.h>
#import "CommonConfig.h"

//レビュー催促のポップを表示した回数を保存するkey
#define REVIEW_SHOW_COUNT_KEY @"reviewShowCount"

//レビュー催促のアラートのタグ
#define ARERT_VIEW_TAG_REVIEW (456)

@interface CommonNativeController : UIViewController {
    
    //レビュー催促のポップを表示した回数
    NSInteger reviewPopupShowCount;
}

//=================================================================================
//メソッド宣言
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName;

//Flurryにイベント送信
-(void)reportFlurryWithEvent:(const char *)eventName;

//アプリ内でAppStoreを開く
- (void)showAppStoreInApp:(int)idNo;

//ウェブページ表示
- (void)showCommonWeb:(BOOL)checkNeedToDisplayGenuineAd;

//レビュー催促を表示する
- (void)showReviewPopUp;

//レビューページを表示する
- (void)showReviewPage;

@end
