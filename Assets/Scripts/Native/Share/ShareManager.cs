﻿//  ShareManager.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.26.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//シェアする場所
public enum ShareLocation
{
	Title,
	Result,
	Clear,
	Miss
}

/// <summary>
/// シェア関連のマネージャ
/// </summary>
public class ShareManager : SingletonMonoBehaviour<ShareManager>
{

	//ネイティブ連携用プラグイン
	#if !NO_NAITIVE
	private SharePlugin _sharePlugin;
	#endif

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Init ()
	{
		base.Init ();

		//ネイティブ連携用プラグイン初期化
		#if !NO_NAITIVE
		_sharePlugin = gameObject.AddComponent<SharePlugin> ();
		_sharePlugin.Init ();
		#endif
	}

	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// Tweet
	/// </summary>
	public void Tweet (ShareLocation location)
	{
		#if !NO_NAITIVE

		if (Application.systemLanguage == SystemLanguage.Japanese) {
		
			//文章のフォーマットを設定
			string shareMessage = UnityProjectSetting.Entity.ShareFormatDict [location];

			//場所に合わせて文章作成
			if (location == ShareLocation.Result) {
				string format = UnityProjectSetting.Entity.ShareFormatDict [ShareLocation.Miss];
				if (CacheData.Entity.IsClear) {
					format = UnityProjectSetting.Entity.ShareFormatDict [ShareLocation.Clear];
				}

				shareMessage = string.Format (format, CacheData.Entity.CurrentScore);
			} else {
				shareMessage = string.Format (shareMessage, CacheData.Entity.CurrentScore);
			}

			//タグとURLを追加
			shareMessage += "\n" + UnityProjectSetting.Entity.Tag;
			shareMessage += "\n" + UnityProjectSetting.Entity.StoreURL;

			//シェア
			_sharePlugin.Tweet (shareMessage);

		} else {

			//文章のフォーマットを設定
			string shareMessage = UnityProjectSetting.Entity.ShareFormatDictEnglish [location];

			//場所に合わせて文章作成
			if (location == ShareLocation.Result) {
				string format = UnityProjectSetting.Entity.ShareFormatDictEnglish [ShareLocation.Miss];
				if (CacheData.Entity.IsClear) {
					format = UnityProjectSetting.Entity.ShareFormatDictEnglish [ShareLocation.Clear];
				}

				shareMessage = string.Format (format, CacheData.Entity.CurrentScore);
			} else {
				shareMessage = string.Format (shareMessage, CacheData.Entity.CurrentScore);
			}

			//タグとURLを追加
			shareMessage += "\n" + UnityProjectSetting.Entity.Tag;
			shareMessage += "\n" + UnityProjectSetting.Entity.StoreURL;

			//シェア
			_sharePlugin.Tweet (shareMessage);
		}

		#endif
	}

}
