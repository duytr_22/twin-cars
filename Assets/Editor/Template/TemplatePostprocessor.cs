﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class TemplatePostprocessor : AssetPostprocessorEx {

	//コマンド名
	private const string COMMAND_NAME = "Tools/Create/Custom Script Creator";

	//書き出すディレクトリへのパス
	private const string EXPORT_DIRECTORY_PATH = "Assets/Editor/Template/CustomScriptCreator/";

	//作成するスクリプトの形式
	private const string EXPORT_SCRIPT_NAME_END    = "ScriptCreator";
	private const string EXPORT_SCRIPT_NAME_FORMAT = "{0}" + EXPORT_SCRIPT_NAME_END + ExtensionName.SCRIPT;

	//=================================================================================
	//テンプレートフォルダ監視
	//=================================================================================

	//テンプレートフォルダ内のファイルが変更されたら、CustomScriptCreatorを作成
	private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		List<string[]> assetsList = new List<string[]> (){
			importedAssets, deletedAssets, movedAssets, movedFromAssetPaths
		};

		List<string> targetDirectoryNameList = new List<string> (){
			DirectoryPath.TEMPLATE_SCRIPT_DIRECTORY_PATH
		};

		if(ExistsDirectoryInAssets(assetsList, targetDirectoryNameList)){
			CheckCustomScriptCreatorAndTemplate ();
		}
	}

	//=================================================================================
	//CustomScriptCreator作成
	//=================================================================================

	[MenuItem(COMMAND_NAME)]
	private static void CheckCustomScriptCreatorAndTemplate()
	{

		//テンプレートフォルダ内のtextファイルへのパスを全て取得
		string templateDirectoryPath = DirectoryPath.TEMPLATE_SCRIPT_DIRECTORY_PATH;
		string[] templatePathArray = System.IO.Directory.GetFiles (templateDirectoryPath, "*" + ExtensionName.TEMPLATE_SCRIPT);

		foreach(string templatePath in templatePathArray){

			//拡張子を含まないファイル名を取得
			string templateName = System.IO.Path.GetFileNameWithoutExtension(templatePath);

			//書き出し先
			string exportPath = EXPORT_DIRECTORY_PATH + string.Format (EXPORT_SCRIPT_NAME_FORMAT, templateName);

			//このテンプレのCustomScriptCreatorが作成されていなければ作成
			if (!System.IO.File.Exists(exportPath)){
				CreateCustomScriptCreator (templateName, exportPath);
			}

		}

		//テンプレートを再度取得し、テンプレートファイル名のリストを作る
		templatePathArray = System.IO.Directory.GetFiles (templateDirectoryPath, "*" + ExtensionName.TEMPLATE_SCRIPT);
		List<string > templateNameList = templatePathArray.
		                                 Select(path => System.IO.Path.GetFileNameWithoutExtension(path)).
		                                 ToList();

		//CustomScriptCreatorフォルダ内のスクリプトを全て取得
		string[] customScriptCreatorPathArray = System.IO.Directory.GetFiles (EXPORT_DIRECTORY_PATH, "*" + ExtensionName.SCRIPT);

		foreach (string customScriptCreatorPath in customScriptCreatorPathArray) {

			//拡張子を含まないファイル名を取得
			string scriptName = System.IO.Path.GetFileNameWithoutExtension(customScriptCreatorPath);

			//スクリプトから元のテンプレ名を取得
			string templateName = scriptName.Remove (scriptName.Length - EXPORT_SCRIPT_NAME_END.Length, EXPORT_SCRIPT_NAME_END.Length);

			//該当するテンプレが無ければCustomScriptCreatorを削除
			if(!templateNameList.Contains(templateName)){
				System.IO.File.Delete(customScriptCreatorPath);
				AssetDatabase.Refresh (ImportAssetOptions.ImportRecursive);
				Debug.Log (customScriptCreatorPath + "を削除しました");
			}

		}

	}

	//CustomScriptCreatorを作成、Createのメニューで対象のテンプレートを選べるようになる
	private static void CreateCustomScriptCreator(string templateName, string exportPath){

		string customScriptCreatorName = System.IO.Path.GetFileNameWithoutExtension(exportPath);

		//作成するスクリプトの内容
		string scriptText = 
			"using UnityEditor;\n\n" +
			"/// <summary>\n" +
			"/// " + templateName +"のテンプレートから新しくスクリプトを作るクラス\n" +
			"/// </summary>\n" +
			"public class " + customScriptCreatorName + " : CustomScriptCreator {\n\n\t" +
			"private const string TEMPLATE_SCRIPT_NAME  = \"" + templateName + "\";\n\n\t" +
			"[MenuItem(MENU_PATH + TEMPLATE_SCRIPT_NAME)]\n\t" +
			"private static void CreateScript(){\n\t\t" +
			"ShowWindow (TEMPLATE_SCRIPT_NAME);\n\t" +
			"}\n\n" +
			"}\n";
		 
		//スクリプトを書き出し
		File.WriteAllText (exportPath, scriptText, Encoding.UTF8);
		AssetDatabase.Refresh (ImportAssetOptions.ImportRecursive);

		Debug.Log (exportPath + "を作成しました");
	}

}
