﻿/// <summary>
/// シーン番号を定数で管理するクラス
/// </summary>
public static class SceneNo{

	  public const int BALL                     = 13;
	  public const int COMMON_PEOPLE_DEMO_SCENE = 17;
	  public const int CRASH_SCENE              = 5;
	  public const int DEMO                     = 7;
	  public const int DEMO_VROR_MOBILE         = 8;
	  public const int DEMO_CUSTOM_MESH         = 9;
	  public const int DEMO_HIGH_QUALITY        = 10;
	  public const int GAME                     = 1;
	  public const int IAPDEMO                  = 12;
	  public const int ISLAND_EXAMPLE           = 18;
	  public const int ISLAND_SHOWCASE          = 19;
	  public const int LINEUP                   = 6;
	  public const int PLAYER_C_DEMO            = 11;
	  public const int RESULT                   = 14;
	  public const int TEST                     = 15;
	  public const int TITLE                    = 0;
	  public const int TUTORIAL                 = 16;
	  public const int UIENDING                 = 4;
	  public const int UIGAMEPLAY               = 2;
	  public const int UITITLE                  = 3;

}
