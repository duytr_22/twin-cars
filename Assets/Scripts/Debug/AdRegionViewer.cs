﻿//  AdRegionViewer.cs
//  ProductName ゴール神3D
//
//  Created by kan.kikuchi on 2016.02.15.

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 広告の領域を表示するクラス
/// </summary>
public class AdRegionViewer : UIAdjuster {
	
	//色
	[SerializeField]
	private Color _color = new Color(1f, 1f, 1f, 0.8f);

	//バナー広告の表示タイプ
	[Flags]
	private enum BannerType{
		Top    = 1 << 0, 
		Bottom = 1 << 1,
		BigTop = 1 << 2,
	}

	[SerializeField][EnumFlags]
	private BannerType _bannerType;

	//レクタングル広告の表示タイプ
	[Flags]
	private enum RectangleType{
		Top    = 1 << 0, 
		Bottom = 1 << 1, 
	}

	[SerializeField][EnumFlags]
	private RectangleType _rectangleType;

	//アイコン広告の表示タイプ
	[Flags]
	private enum IconType{
		Title = 1 << 0, 
		Help  = 1 << 1, 
	}

	[SerializeField][EnumFlags]
	private IconType _iconType;

	//バナーサンプル
	[SerializeField]
	private GameObject _topBanner = null, _topBigBanner = null, _bottomBanner = null;

	//レクタングルサンプル
	[SerializeField]
	private GameObject _topRectangle = null, _bottomRectangle = null;

	//アイコンサンプル
	[SerializeField]
	private GameObject _titleIcon = null, _helpIcon = null;

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Awake(){
		base.Awake();

		//SHOW_DEBUGが定義されていない時は消す
		#if !SHOW_AD_PREVIEW
		if(Application.isPlaying){
			Destroy (gameObject);
		}
		#endif
	}

	//=================================================================================
	//更新
	//=================================================================================

	//調整
	protected override void AdjustUI(){
		base.AdjustUI ();

		//バナー
		AdjustAdSample(_topBanner,    _bannerType.HasFlag(BannerType.Top));
		AdjustAdSample(_bottomBanner, _bannerType.HasFlag(BannerType.Bottom));
		AdjustAdSample(_topBigBanner, _bannerType.HasFlag(BannerType.BigTop));

		//レクタングル
		AdjustAdSample(_topRectangle,    _rectangleType.HasFlag(RectangleType.Top));
		AdjustAdSample(_bottomRectangle, _rectangleType.HasFlag(RectangleType.Bottom));

		//アイコン
		#if UNITY_IOS

		AdjustAdSample(_titleIcon, false);
		AdjustAdSample(_helpIcon,  false);

		#else

		AdjustAdSample(_titleIcon, _iconType.HasFlag(IconType.Title));
		AdjustAdSample(_helpIcon,  _iconType.HasFlag(IconType.Help));

		#endif
	}

	//各サンプルを調整する
	private void AdjustAdSample(GameObject sample, bool isActive){
		if(sample == null){
			return;
		}

		//スケールをipadなら半分にする
		float adScale = IsIpad() ? 0.5f : 1;

		sample.transform.SetLocalScale (adScale, adScale, adScale);

		//色設定
		foreach (Image image in sample.GetComponentsInChildren<Image>()) {
			image.color = _color;
		}

		//表示設定
		sample.SetActive (isActive);

		//SHOW_DEBUGが定義されていない時は表示しない
		#if !SHOW_AD_PREVIEW
		sample.SetActive (false);
		#endif
	}

}