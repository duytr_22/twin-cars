//
//  PurchasePlugin.mm
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  Unity側との課金関連の連携を行うプラグイン

#import "PurchaseController.h"

//=================================================================================
//Unity側との連携
//=================================================================================


extern "C" {
    
    //初期化、ネイティブ側のプラグインのインスタンスを作成し、Unity側へ渡す
    void *_PurchasePlugin_Init(const char *gameObjectName){
        id instance = [[PurchaseController alloc]initWithGameObjectName:gameObjectName];
        CFRetain((CFTypeRef)instance);
        return (__bridge void *)instance;
    }
    
    //課金アイテムの情報取得(結果はUnitySendMessageで返す)
    void _GetItemInfo(void *instance, const char *productID) {
        NSLog(@"productID : %s", productID);
        PurchaseController *purchaseController = (__bridge PurchaseController*)instance;
        [purchaseController sendProductsRequest:productID isPurchasing:NO];
    }
    
    //課金実行
    void _Purchase(void *instance, const char *productID){
        NSLog(@"productID : %s", productID);
        
        PurchaseController *purchaseController = (__bridge PurchaseController*)instance;
        [purchaseController sendProductsRequest:productID isPurchasing:YES];
    }

}


