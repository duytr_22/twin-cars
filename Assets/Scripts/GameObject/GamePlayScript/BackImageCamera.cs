﻿using UnityEngine;
using System.Collections;

public class BackImageCamera : MonoBehaviour {

	private bool reverseFog;
	private Color reverseFogColor;
	private float reverseFogDensity;
	private float reverseFogStart;
	private float reverseFogEnd;
	private FogMode reverseFogMode;

	// Use this for initialization
	void Start () {
	
	}

	void OnPreRender() {
		reverseFog = RenderSettings.fog;
		reverseFogColor = RenderSettings.fogColor;
		reverseFogDensity = RenderSettings.fogDensity;
		reverseFogStart = RenderSettings.fogStartDistance;
		reverseFogEnd = RenderSettings.fogEndDistance;
		reverseFogMode = RenderSettings.fogMode;

		RenderSettings.fog = false;

	}

	void OnPostRender() {
		RenderSettings.fog = reverseFog;
		RenderSettings.fogColor = reverseFogColor;
		RenderSettings.fogDensity = reverseFogDensity;
		RenderSettings.fogStartDistance = reverseFogStart;
		RenderSettings.fogEndDistance = reverseFogEnd;
		RenderSettings.fogMode = reverseFogMode;
	}
}
