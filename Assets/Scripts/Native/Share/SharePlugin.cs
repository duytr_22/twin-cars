﻿//  SharePlugin.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.26.

//モバイルの時だけMOBILE_DEVICEを宣言する
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL || UNITY_WEBPLAYER
#undef MOBILE_DEVICE
#else
#define MOBILE_DEVICE
#endif

// #undef MOBILE_DEVICE//lazycat cheat

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// シェア関連のネイティブプラグイン
/// </summary>
public class SharePlugin : NativePlugin {

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init(){
		#if UNITY_IOS && MOBILE_DEVICE
		base.Init (_SharePlugin_Init, "");
		#elif UNITY_ANDROID && MOBILE_DEVICE && MOBILE_DEVICE
		base.Init (null, "");
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern IntPtr _SharePlugin_Init(string gameObjectName);
	#endif

	//=================================================================================
	//ツイートする
	//=================================================================================

	/// <summary>
	/// ツイートする
	/// </summary>
	public void Tweet(string text){
		Debug.Log ("Tweet 文字数 : " + text.Length + "\n" + text);

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
		return;
		}
		_Tweet(_nativeInstance, text);	
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call ("launchTwitterWithMessage", text);
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void  _Tweet (IntPtr instance, string text);
	#endif

}