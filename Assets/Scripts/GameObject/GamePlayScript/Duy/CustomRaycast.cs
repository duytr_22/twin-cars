﻿using System;
using UnityEngine;

public class CustomRaycast : MonoBehaviour
{
    public event Action<RaycastHit, bool> onRaycastResult;

    [SerializeField] Color _raycastDebugColor;
    [SerializeField] LayerMask _layer;
    [SerializeField] float _distance;
    [SerializeField] Vector3 _direction;
    Vector3 _origin;
    bool _internalResult;

    void Start()
    {

    }

    void Update()
    {
        _origin = transform.position + Vector3.up / 2;
        RaycastHit hit;
        _internalResult = Physics.Raycast(_origin, _direction, out hit, _distance, _layer);
        onRaycastResult?.Invoke(hit, _internalResult);
    }

    void OnDrawGizmos()
    {
        if (Application.isPlaying == false)
            _origin = transform.position + Vector3.up / 2;
        Gizmos.color = _raycastDebugColor;
        Gizmos.DrawRay(_origin, _direction * _distance);
    }
}
