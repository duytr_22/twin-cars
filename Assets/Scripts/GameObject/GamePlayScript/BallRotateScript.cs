﻿using UnityEngine;
using System.Collections;

public class BallRotateScript : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update ()
	{
		if (Time.timeScale == 0 || (!GameManager.Instance.IsPlayingGame && GameManager.Instance.playMode != GameManager.PlayMode.PlayModeTitle)) {
			return;
		}
		this.transform.RotateAround (this.transform.position, Vector3.right, GamePlayScript.Instance.scrollSpeed * 1.1f);
	}
}
