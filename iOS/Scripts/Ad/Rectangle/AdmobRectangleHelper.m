//
//  ImobileRectangleHelper.h
//
//  Created on 14/10/2014.
//
//

#import "AdmobRectangleHelper.h"
#import <UIKit/UIKit.h>

@implementation AdmobRectangleHelper

//初期化
-(UIView *)prepareRectangleAd:(CGRect)frame {
    
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeMediumRectangle origin:frame.origin];
    
    if (isEndRect) {
        bannerView.adUnitID = ADMOB_RECTANGLE_END;
    } else if(isPopup) {
        bannerView.adUnitID = ADMOB_RECTANGLE_POPUP;
    } else {
        bannerView.adUnitID = ADMOB_RECTANGLE;
    }
    
    bannerView.delegate = self;
    bannerView.rootViewController = UnityGetGLViewController();
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
    
    return bannerView;
}

#pragma mark - Delagate
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView{
        NSLog(@"Admob:Ready for Rectangle Ad");
        NSLog(@"Admob:%@", bannerView.adNetworkClassName);
    }

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
        NSLog(@"Admob:Failed Load Rectangle Ad %lu", (unsigned long)error);
        NSLog(@"Admob:%@", bannerView.adNetworkClassName);
    }

-(void) reloadRectangleAd {
    
}

-(void) removeRectangleAd {
    
}

@end
