﻿//  RankingManager.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.26.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ランキング関連のマネージャ
/// </summary>
public class RankingManager : SingletonMonoBehaviour<RankingManager>
{

	//ネイティブ連携用プラグイン
	#if !NO_NAITIVE
	private RankingPlugin _rankingPlugin;
	#endif

	//リーダーボードを表示した時のイベント
	public event Action OnShowLeaderBoard = delegate{};

	//=================================================================================
	//初期化
	//=================================================================================
	#if !NO_NAITIVE
	protected override void Init ()
	{
		base.Init ();

		//ネイティブ連携用プラグイン初期化
		_rankingPlugin = gameObject.AddComponent<RankingPlugin> ();


		_rankingPlugin.Init ();
		
	}
	#endif
	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// リーダーボード表示
	/// </summary>
	public void ShowLeaderBoard ()
	{
		#if !NO_NAITIVE
		_rankingPlugin.ShowLeaderBoard ();
		#endif
		OnShowLeaderBoard ();
	}

	/// <summary>
	/// スコアをリーダーボードに送信
	/// </summary>
	public void ReportScore (int rankingNo, int score)
	{
		#if !NO_NAITIVE
		_rankingPlugin.ReportScore (rankingNo, score);
		#endif
	}

	public void ReportScore (int rankingNo, float score)
	{
		#if !NO_NAITIVE
		_rankingPlugin.ReportScore (rankingNo, score);
		#endif
	}

	/// <summary>
	/// スコアをリーダーボードに送信
	/// </summary>
	public void ReportScore (int score)
	{
		ReportScore (1, score);
	}

	public void ReportScore (float score)
	{
		ReportScore (1, score * 100f);//小数点2桁まで表示、10fなら1桁、1000fなら3桁
	}

	public void ReportScore (decimal score)
	{
		ReportScore (1, (float)(score * 100m));//小数点2桁まで表示、10fなら1桁、1000fなら3桁
	}

	public void GetRankingValue ()
	{
		#if !NO_NAITIVE
		_rankingPlugin.GetRankingValue ();
		#endif
	}
}