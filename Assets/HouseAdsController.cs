﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseAdsController : MonoBehaviour
{

	[SerializeField]
	GameObject dialog;

	void Start ()
	{
		PlayerPrefs.SetInt (Define.CONTINUE_GAME, 0);
	}

	// Use this for initialization
	void ShowCrossPromotion ()
	{
		dialog.SetActive (true);
	}
}
