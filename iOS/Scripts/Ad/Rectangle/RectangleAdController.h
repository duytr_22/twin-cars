//
//  RectangleAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/30.
//
//  レクタングル広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "CommonConfig.h"

@interface RectangleAdController : NSObject{
    UIView* _rectangleAdView;
    BOOL isEndRect;
    BOOL isFiveAds;
    BOOL isAdmob;
    BOOL isPopup;
}

-(id)init:(BOOL)isBottom isNative:(BOOL)isNative isAdmob:(BOOL)isAdmob isPopup:(BOOL)isPopup;
-(void)updateRectangleAd:(BOOL)enable;

-(UIView *)prepareRectangleAd:(CGRect)frame;
-(UIView *)prepareRectangleAdNative:(CGRect)frame;

-(void) removeRectangleAd;
-(void) reloadRectangleAd;

-(void)dealloc;
@end
