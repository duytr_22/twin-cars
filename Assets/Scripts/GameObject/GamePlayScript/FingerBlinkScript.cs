﻿using UnityEngine;
using System.Collections;

public class FingerBlinkScript : MonoBehaviour
{
	
	[HideInInspector]public UISprite canvas;
	[HideInInspector]public Transform rectTransform;

	private bool isStart;
	private float t;
	private bool isScaleUp;
	private float duration;
	private float minScale;
	private float maxScale;
	private bool _shouldUpdateScale = true;

	// Use this for initialization
	void Start ()
	{
		canvas = GetComponent<UISprite> ();
		rectTransform = transform;

		canvas.alpha = 0;
		this.enabled = false;

		t = 0;

		isStart = false;
		isScaleUp = false;
		duration = 0.4f;
		minScale = 0.8f;
		maxScale = 1.0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!_shouldUpdateScale)
			return;
		
		if (!TutorialManager.Instance.isPause) {
			t += Time.fixedDeltaTime;
			if (t < duration) {
//				t = 0;
//				canvas.SetAlpha (canvas.GetAlpha () == 0 ? 1 : 0);

				float offset = maxScale - minScale;
				float ratio = t / duration;
				float negative = canvas.transform.localScale.x / Mathf.Abs (canvas.transform.localScale.x);
				float scale = 0;
				float scaleX = 0;
				float scaleY = 0;

				if (isScaleUp) {
					scale = minScale + (offset * ratio);
					scaleX = scale * negative;
					scaleY = scale;
					rectTransform.AddLoacalScaleX (scaleX * 0.01f);
					rectTransform.AddLoacalScaleY (scaleY * 0.01f);
				} else {
					scale = maxScale - (offset * ratio);
					scaleX = scale * negative;
					scaleY = scale;
					rectTransform.AddLoacalScaleX (scaleX * -0.01f);
					rectTransform.AddLoacalScaleY (scaleY * -0.01f);
				}
			} else {
				t = 0;
				isScaleUp = !isScaleUp;
			}
		}
	}

	public void startWithoutScale ()
	{
		if (!isStart) {
			_shouldUpdateScale = false;
			canvas.alpha = 1;
			this.enabled = true;
			t = 0;
			isStart = true;
			isScaleUp = false;
			rectTransform.SetLocalScale (rectTransform.localScale.x < 0 ? -1 : 1, 1, 1);
		}
	}

	public void start ()
	{
		if (!isStart) {
			_shouldUpdateScale = true;
			canvas.alpha = 1;
			this.enabled = true;
			t = 0;
			isStart = true;
			isScaleUp = false;
			rectTransform.SetLocalScale (rectTransform.localScale.x < 0 ? -1 : 1, 1, 1);
		}
	}

	public void stop ()
	{
		if (isStart) {
			canvas.alpha = 0;
			this.enabled = false;
			t = 0;
			isStart = false;
			isScaleUp = false;
			rectTransform.SetLocalScale (rectTransform.localScale.x < 0 ? -1 : 1, 1, 1);
		}
	}
}
