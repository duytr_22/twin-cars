﻿/// <summary>
/// Resources以下のディレクトリパスを定数で管理するクラス
/// </summary>
public static class ResourcesDirectoryPath{

	  public const string AUDIO                    = "Audio";
	  public const string AUDIO_BGM                = "Audio/BGM";
	  public const string AUDIO_SE                 = "Audio/SE";
	  public const string DATA                     = "Data";
	  public const string ENLPROJ                  = "en.lproj";
	  public const string PREFAB                   = "Prefab";
	  public const string PREFAB_ATLAS             = "Prefab/Atlas";
	  public const string PREFAB_ATLAS_IMAGE       = "Prefab/Atlas/Image";
	  public const string PREFAB_FNTFONTS          = "Prefab/FNT Fonts";
	  public const string PREFAB_GAME_PLAY_OBJECTS = "Prefab/GamePlayObjects";
	  public const string PREFAB_MATERIALS         = "Prefab/Materials";
	  public const string PREFAB_RECYCLING_OBJECT  = "Prefab/RecyclingObject";
	  public const string PREFAB_SHADERS           = "Prefab/Shaders";
	  public const string PREFAB_TEXTURES          = "Prefab/Textures";
	  public const string PREFAB_UI                = "Prefab/UI";
	  public const string SHADERS                  = "Shaders";

}
