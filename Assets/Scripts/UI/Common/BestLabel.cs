﻿//  BestLabel.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.22.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ベストスコアを表示するラベル
/// </summary>
public class BestLabel : MonoBehaviour
{

	//テキストのフォーマット
	[SerializeField]
	private string _textFormat = "BEST : {0}pts";

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start ()
	{
		UpdateText ();
	}

	//=================================================================================
	//更新
	//=================================================================================

	protected void UpdateText ()
	{
		GetComponent<UILabel> ().text = string.Format (_textFormat, UserData.HighScore);
	}

}