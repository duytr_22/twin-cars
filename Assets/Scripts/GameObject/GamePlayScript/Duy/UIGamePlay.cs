﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class UIGamePlay : SingletonMonoBehaviour<UIGamePlay>
{
    [SerializeField] GameObject _tutorial;
    [SerializeField] UILabel _timeText;

    string timeTextFormat = "0";

    public void ShowTutorial(Action onCompleted)
    {
        Debug.Log("show tutorial");

        var delayDuration = 0.1f;
        var tutorialDuration = 5f;

        //mark to not show tutorial next times playing
        PlayerPrefs.SetInt(Define.IS_SHOW_TUTORIAL_KEY, 1);

        StartCoroutine(this.DelayMethod(delayDuration, () =>
        {
            if (GamePlayScript.Instance.isOver == false)
            {
                _tutorial.SetActive(true);
                Time.timeScale = 0;
            }
        }));
    }

    public void OnClickHideTutorial()
    {
        if (Time.timeScale == 0)
        {
            _tutorial.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void UpdateTimeText(float time)
    {
        _timeText.text = time.ToString(timeTextFormat);
    }
}
