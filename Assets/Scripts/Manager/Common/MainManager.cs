﻿//  MainManager.cs
//  ProductName ReincarnationRPG
//
//  Created by kan.kikuchi on 2016.05.09.

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 最上位のマネージャー、他のマネージャーを生成し、子に持つ
/// </summary>
public class MainManager : SingletonMonoBehaviour<MainManager>{

	public bool showAdWhenRetry = false;

	//=================================================================================
	//初期化
	//=================================================================================

	//シーン実行時に自らを生成する
	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
	private static void CreateSelf(){
		new GameObject("MainManager", typeof(MainManager));
	}

	/// <summary>
	/// 初期化(Awake時かその前の初アクセス、どちらかの一度しか行われない)
	/// </summary>
	protected override void Init(){
		base.Init ();

		DontDestroyOnLoad (this.gameObject);

		Time.timeScale = 1.0f;

		//デバッグでデータ全削除
		#if DATA_RESET
		PlayerPrefs.DeleteAll ();
		#endif

		//FPSを表示するデバッグ
		#if SHOW_FPS
		gameObject.AddComponent<FPSUI>();
		#endif

		//FPS設定
		Application.targetFrameRate = 60;

		//他のマネージャー生成
		GameObject sceneNavigator = gameObject.CreateChild("SceneNavigator");
		sceneNavigator.AddComponent<SceneNavigator> ();

		GameObject audioManager = gameObject.CreateChild("AudioManager");
		audioManager.AddComponent<BGMManager> ();
		audioManager.AddComponent<SEManager> ();

		GameObject nativeManager = gameObject.CreateChild("NativeManager");
		nativeManager.AddComponent<CommonNativeManager> ();
		nativeManager.AddComponent<RankingManager> ();
		nativeManager.AddComponent<ShareManager> ();
		nativeManager.AddComponent<AdManager> ();

		//シーン移動のフェードアウトが完了した時にデータを保存するように
		SceneNavigator.Instance.OnFinishedFadeOut += UserData.Save;
	}

	//=================================================================================
	//データ保存
	//=================================================================================

	//一時停止(ホームへ戻った時とか)or再開時
	private void OnApplicationPause (bool pauseStatus){
		if(pauseStatus){
			UserData.Save ();//データ保存
		}
	}

	//アプリ終了時
	private void OnApplicationQuit (){
		UserData.Save ();//データ保存
	}

}