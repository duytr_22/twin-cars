﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lane : MonoBehaviour
{

	[SerializeField]
	Material[] _material;

	// Use this for initialization
	void Start ()
	{	
		int idx = PlayerPrefs.GetInt (Define.STAGE_SELECTED, 1);

		GetComponent<MeshRenderer> ().material = _material [idx - 1];
	}

}
