﻿/// <summary>
/// レイヤー番号を定数で管理するクラス
/// </summary>
public static class LayerNo{

	  public const int BACK                  = 8;
	  public const int BACK1                 = 15;
	  public const int CUBE                  = 12;
	  public const int DEFAULT               = 0;
	  public const int EFFECT                = 9;
	  public const int END_POINT             = 14;
	  public const int GAME_LAYER            = 10;
	  public const int IGNORE_RAYCAST        = 2;
	  public const int LANE                  = 11;
	  public const int NEW_DEFAULT_COLLISION = 21;
	  public const int NEW_LANE_COLLISION    = 20;
	  public const int PLAYER                = 13;
	  public const int TRANSPARENT_FX        = 1;
	  public const int TUTORIAL              = 18;
	  public const int UI                    = 5;
	  public const int UIBACK                = 16;
	  public const int UIBACK_SCORE          = 17;
	  public const int UITOP                 = 19;
	  public const int WATER                 = 4;

}
