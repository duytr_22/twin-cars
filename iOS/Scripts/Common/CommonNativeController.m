//
//  ShareController.m
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  ネイティブ連携の汎用的な処理を行うクラス

#import "CommonNativeController.h"
#import "AdManager.h"
#import "FlurryHelper.h"
#import <StoreKit/StoreKit.h>

@interface UIViewController () <SKStoreProductViewControllerDelegate>
@end

@implementation CommonNativeController

//=================================================================================
//初期化
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName{
    self = [super init];
    
    reviewPopupShowCount = 1;
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    reviewPopupShowCount = [[userDefault objectForKey:REVIEW_SHOW_COUNT_KEY] integerValue];
    
    return self;
}

//=================================================================================
//内部
//=================================================================================

//アラートのボタンを押した時の処理
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == ARERT_VIEW_TAG_REVIEW) {// レビューポップアップ
        BOOL isReset = NO;
        BOOL isNever = NO;
        switch (buttonIndex) {
            case 0:
                //cancel
                isReset = YES;
                break;
            case 1:
                //書く
                isNever = YES;
                [self moveReviewPage];
                break;
            case 2:
                //表示しない
                isNever = YES;
                break;
                
            default:
                break;
        }
        NSUserDefaults * userDeafult = [NSUserDefaults standardUserDefaults];
        if (isReset == YES) {
            [userDeafult setInteger:1 forKey:REVIEW_SHOW_COUNT_KEY];
        }
        if (isNever == YES) {
            [userDeafult setInteger:10000 forKey:REVIEW_SHOW_COUNT_KEY];
        }
        [userDeafult synchronize];
    }
}

//レビュー催促のアラートを表示
-(void)showReviewAlert{
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:POPUP_TITLE
                                                         message:POPUP_MESSAGE
                                                        delegate:self
                                               cancelButtonTitle:POPUP_OK_CANCEL_BUTTON_TITLE
                                               otherButtonTitles:POPUP_OK_BUTTON_TITLE,POPUP_OK_OTHER_BUTTON_TITLE,
                               nil];
    [alertView setTag:ARERT_VIEW_TAG_REVIEW];
    [alertView show];
}

//レビューページへ移動する
-(void)moveReviewPage{
    NSString * app_id = [NSString stringWithFormat:@"%d",IOS_APP_ID];
    NSString * url = Nil;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f) {
        url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software", app_id];
    }
    else{
        url = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", app_id];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

//=================================================================================
//外部
//=================================================================================


//Flurryにイベント送信
-(void)reportFlurryWithEvent:(const char *)eventName{
    NSString* eventKey = [NSString stringWithUTF8String:eventName];
    [FlurryHelper reportFlurryWithEvent:eventKey];
}

//アプリ内でAppStoreを開く
- (void)showAppStoreInApp:(int)idNo{
    NSLog(@"showAppStoreInApp:%d", idNo);

    NSString *urlString = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%d", IOS_APP_ID];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    [[UIApplication sharedApplication] openURL: url];

//    SKStoreProductViewController *productViewController = [[SKStoreProductViewController alloc] init];
//    productViewController.delegate = self;
//    
//    [UnityGetGLViewController() presentViewController:productViewController animated:YES completion:^() {
//        
//        NSString *productID = [NSString stringWithFormat:@"%d", idNo];
//        NSDictionary *parameters = @{SKStoreProductParameterITunesItemIdentifier:productID};
//        [productViewController loadProductWithParameters:parameters
//                                         completionBlock:^(BOOL result, NSError *error)
//         {
//             // 読み込み完了またはエラーのときの処理
//             // ...
//         }];
//    }];
    
}
// キャンセルボタンが押されたときの処理
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
}


//ウェブページ表示
- (void)showCommonWeb:(BOOL)checkNeedToDisplayGenuineAd{
	
}


//レビュー催促を表示する
- (void)showReviewPopUp{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    reviewPopupShowCount = [userDefault integerForKey:REVIEW_SHOW_COUNT_KEY];
    
    NSLog(@"showReviewPopup REVIEW_SHOW_COUNT:%d reviewPopupShowCount:%ld",REVIEW_SHOW_COUNT, (long)reviewPopupShowCount);
    
#ifdef DEBUG_REVIEW_POPUP
    [self showReviewAlert];
    return;
#endif
    
    if (REVIEW_SHOW_COUNT <= reviewPopupShowCount && reviewPopupShowCount < 10000) {
       [self showReviewAlert];
    }
    else{
        reviewPopupShowCount++;
        [userDefault setInteger:reviewPopupShowCount forKey:REVIEW_SHOW_COUNT_KEY];
        [userDefault synchronize];
    }
}

//レビューページを表示する
- (void)showReviewPage{
    NSString *urlString = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%d", IOS_APP_ID];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    [[UIApplication sharedApplication] openURL: url];
}

@end
