//
//  BannerAdController.h
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Adstirのバナー広告のコントローラー

#import "BannerAdController.h"
#import <IronSource/IronSource.h>

@interface IronSourceBannerHelper : BannerAdController<ISBannerDelegate>

@property (nonatomic, retain) UIView* view;

-(UIView*)prepareBannerAd;

@end
