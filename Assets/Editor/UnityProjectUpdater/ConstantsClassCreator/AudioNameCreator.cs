﻿//  AudioNameCreator.cs
//   
//
//  Created by kan.kikuchi on 2015

using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// オーディオのファイル名を定数で管理するクラスを自動で作成するスクリプト
/// </summary>
public class AudioNameCreator : AssetPostprocessorEx{

	//=================================================================================
	//検知
	//=================================================================================

	//オーディオファイルが編集または追加されたら自動でAUDIO.csを作成する
	private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths){
		//最初の起動時にエラーが出ないように
		if(!UnityProjectSetting.IsLaunchedProject){
			return;
		}


		List<string[]> assetsList = new List<string[]> (){
			importedAssets, deletedAssets, movedAssets, movedFromAssetPaths
		};

		List<string> targetDirectoryNameList = new List<string> (){
			DirectoryPath.TOP_RESOURCES + ResourcesDirectoryPath.AUDIO_BGM, 
			DirectoryPath.TOP_RESOURCES + ResourcesDirectoryPath.AUDIO_SE, 
		};

		if(ExistsDirectoryInAssets(assetsList, targetDirectoryNameList)){
			Create ();
		}

	}

	//=================================================================================
	//作成
	//=================================================================================

	//スクリプトを作成します
	[MenuItem("Tools/Create/Audio Name")]
	private static void Create(){

		Dictionary<string, string> audioDic = Resources.LoadAll(ResourcesDirectoryPath.AUDIO)
			.Select(audioFile => ((AudioClip)audioFile).name)
			.ToDictionary(audioFile => audioFile);

		ConstantsClassCreator.Create ("AudioName", "オーディオ名を定数で管理するクラス", audioDic);
	}


}