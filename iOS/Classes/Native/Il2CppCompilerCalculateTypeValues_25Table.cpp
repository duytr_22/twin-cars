﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Transform
struct Transform_t3600365921;
// UIRect
struct UIRect_t2875960382;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Component
struct Component_t1923634451;
// System.String
struct String_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// Localization/LoadFunction
struct LoadFunction_t2078002637;
// Localization/OnLocalizeNotification
struct OnLocalizeNotification_t3391620158;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,System.String[]>
struct Dictionary_2_t1067045639;
// UnityEngine.Object
struct Object_t631007953;
// System.Type
struct Type_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// BetterList`1<System.String>
struct BetterList_1_t1002471007;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// EventDelegate/Parameter[]
struct ParameterU5BU5D_t2885283399;
// EventDelegate/Callback
struct Callback_t3139336517;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<BMGlyph>
struct List_1_t521991992;
// System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph>
struct Dictionary_2_t2233597877;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t4027761066;
// UIGeometry/OnCustomWrite
struct OnCustomWrite_t2800546165;
// UISpriteData
struct UISpriteData_t900308526;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.AudioListener
struct AudioListener_t2734094699;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t4291797753;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t2223234056;
// System.Collections.Generic.Dictionary`2<System.String,UIWidget>
struct Dictionary_2_t3323778224;
// UIPanel
struct UIPanel_t1716472341;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Comparison`1<UIWidget>
struct Comparison_1_t3313453104;
// System.Comparison`1<UIPanel>
struct Comparison_1_t1491403520;
// UICamera/MouseOrTouch
struct MouseOrTouch_t3052596533;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UIDrawCall
struct UIDrawCall_t1293405319;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UIWidget
struct UIWidget_t3538521925;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// UIFont
struct UIFont_t2766063701;
// UnityEngine.Font
struct Font_t1956802104;
// NGUIText/GlyphInfo
struct GlyphInfo_t1020792323;
// BetterList`1<UnityEngine.Color>
struct BetterList_1_t1710706642;
// BetterList`1<System.Single>
struct BetterList_1_t552287092;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// UIEventListener/VoidDelegate
struct VoidDelegate_t3914127870;
// UIEventListener/BoolDelegate
struct BoolDelegate_t3089012064;
// UIEventListener/FloatDelegate
struct FloatDelegate_t1747458064;
// UIEventListener/VectorDelegate
struct VectorDelegate_t1966661092;
// UIEventListener/ObjectDelegate
struct ObjectDelegate_t2025096746;
// UIEventListener/KeyCodeDelegate
struct KeyCodeDelegate_t675056699;
// SpringPanel/OnFinished
struct OnFinished_t3778785451;
// UIScrollView
struct UIScrollView_t1973404950;
// PropertyReference
struct PropertyReference_t223937415;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t4210400802;
// UnityEngine.Animation
struct Animation_t3648466861;
// UnityEngine.Animator
struct Animator_t434523843;
// BetterList`1<UIDrawCall>
struct BetterList_1_t448425637;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t133425655;
// UIDrawCall/OnCreateDrawCall
struct OnCreateDrawCall_t609469653;
// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t1857321114;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// SpringPosition/OnFinished
struct OnFinished_t3364492952;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UI2DSprite
struct UI2DSprite_t1366157572;
// UIRoot
struct UIRoot_t4022971450;
// System.Collections.Generic.List`1<UISpriteData>
struct List_1_t2372383268;
// System.Collections.Generic.List`1<UIAtlas/Sprite>
struct List_1_t72704565;
// System.Comparison`1<UISpriteData>
struct Comparison_1_t675239705;
// UIRect/AnchorPoint
struct AnchorPoint_t1754718329;
// BetterList`1<UIRect>
struct BetterList_1_t2030980700;
// BetterList`1<UICamera>
struct BetterList_1_t511459189;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2810275146;
// UICamera/GetAxisFunc
struct GetAxisFunc_t2592608932;
// UICamera/GetAnyKeyFunc
struct GetAnyKeyFunc_t1761480072;
// UICamera/GetMouseDelegate
struct GetMouseDelegate_t662790529;
// UICamera/GetTouchDelegate
struct GetTouchDelegate_t4218246285;
// UICamera/RemoveTouchDelegate
struct RemoveTouchDelegate_t2508278027;
// UICamera/OnScreenResize
struct OnScreenResize_t2279991692;
// UICamera/OnCustomInput
struct OnCustomInput_t3508588789;
// UICamera/OnSchemeChange
struct OnSchemeChange_t1701155603;
// UICamera/VoidDelegate
struct VoidDelegate_t3100799918;
// UICamera/BoolDelegate
struct BoolDelegate_t3825226153;
// UICamera/FloatDelegate
struct FloatDelegate_t906524069;
// UICamera/VectorDelegate
struct VectorDelegate_t435795517;
// UICamera/ObjectDelegate
struct ObjectDelegate_t2041570719;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t3064672302;
// UICamera/MoveDelegate
struct MoveDelegate_t16019400;
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t3534648920;
// System.Collections.Generic.List`1<UICamera/MouseOrTouch>
struct List_1_t229703979;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t4078737532;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295;
// UICamera/GetTouchCountCallback
struct GetTouchCountCallback_t3185863032;
// UICamera/GetTouchCallback
struct GetTouchCallback_t97678626;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t2967399990;
// BetterList`1/CompareFunc<UICamera>
struct CompareFunc_t3695088943;
// UITable
struct UITable_t3168834800;
// UnityEngine.Light
struct Light_t3756812086;
// UIWidget/OnDimensionsChanged
struct OnDimensionsChanged_t3101921181;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t2835645043;
// UIWidget/HitCheck
struct HitCheck_t2300079615;
// UIGeometry
struct UIGeometry_t1059483952;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Sprite
struct Sprite_t280657092;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NGUIMATH_T3937908296_H
#define NGUIMATH_T3937908296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIMath
struct  NGUIMath_t3937908296  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NGUIMATH_T3937908296_H
#ifndef ANCHORPOINT_T1754718329_H
#define ANCHORPOINT_T1754718329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect/AnchorPoint
struct  AnchorPoint_t1754718329  : public RuntimeObject
{
public:
	// UnityEngine.Transform UIRect/AnchorPoint::target
	Transform_t3600365921 * ___target_0;
	// System.Single UIRect/AnchorPoint::relative
	float ___relative_1;
	// System.Int32 UIRect/AnchorPoint::absolute
	int32_t ___absolute_2;
	// UIRect UIRect/AnchorPoint::rect
	UIRect_t2875960382 * ___rect_3;
	// UnityEngine.Camera UIRect/AnchorPoint::targetCam
	Camera_t4157153871 * ___targetCam_4;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_relative_1() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___relative_1)); }
	inline float get_relative_1() const { return ___relative_1; }
	inline float* get_address_of_relative_1() { return &___relative_1; }
	inline void set_relative_1(float value)
	{
		___relative_1 = value;
	}

	inline static int32_t get_offset_of_absolute_2() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___absolute_2)); }
	inline int32_t get_absolute_2() const { return ___absolute_2; }
	inline int32_t* get_address_of_absolute_2() { return &___absolute_2; }
	inline void set_absolute_2(int32_t value)
	{
		___absolute_2 = value;
	}

	inline static int32_t get_offset_of_rect_3() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___rect_3)); }
	inline UIRect_t2875960382 * get_rect_3() const { return ___rect_3; }
	inline UIRect_t2875960382 ** get_address_of_rect_3() { return &___rect_3; }
	inline void set_rect_3(UIRect_t2875960382 * value)
	{
		___rect_3 = value;
		Il2CppCodeGenWriteBarrier((&___rect_3), value);
	}

	inline static int32_t get_offset_of_targetCam_4() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___targetCam_4)); }
	inline Camera_t4157153871 * get_targetCam_4() const { return ___targetCam_4; }
	inline Camera_t4157153871 ** get_address_of_targetCam_4() { return &___targetCam_4; }
	inline void set_targetCam_4(Camera_t4157153871 * value)
	{
		___targetCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetCam_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORPOINT_T1754718329_H
#ifndef PROPERTYREFERENCE_T223937415_H
#define PROPERTYREFERENCE_T223937415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PropertyReference
struct  PropertyReference_t223937415  : public RuntimeObject
{
public:
	// UnityEngine.Component PropertyReference::mTarget
	Component_t1923634451 * ___mTarget_0;
	// System.String PropertyReference::mName
	String_t* ___mName_1;
	// System.Reflection.FieldInfo PropertyReference::mField
	FieldInfo_t * ___mField_2;
	// System.Reflection.PropertyInfo PropertyReference::mProperty
	PropertyInfo_t * ___mProperty_3;

public:
	inline static int32_t get_offset_of_mTarget_0() { return static_cast<int32_t>(offsetof(PropertyReference_t223937415, ___mTarget_0)); }
	inline Component_t1923634451 * get_mTarget_0() const { return ___mTarget_0; }
	inline Component_t1923634451 ** get_address_of_mTarget_0() { return &___mTarget_0; }
	inline void set_mTarget_0(Component_t1923634451 * value)
	{
		___mTarget_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTarget_0), value);
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(PropertyReference_t223937415, ___mName_1)); }
	inline String_t* get_mName_1() const { return ___mName_1; }
	inline String_t** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(String_t* value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mName_1), value);
	}

	inline static int32_t get_offset_of_mField_2() { return static_cast<int32_t>(offsetof(PropertyReference_t223937415, ___mField_2)); }
	inline FieldInfo_t * get_mField_2() const { return ___mField_2; }
	inline FieldInfo_t ** get_address_of_mField_2() { return &___mField_2; }
	inline void set_mField_2(FieldInfo_t * value)
	{
		___mField_2 = value;
		Il2CppCodeGenWriteBarrier((&___mField_2), value);
	}

	inline static int32_t get_offset_of_mProperty_3() { return static_cast<int32_t>(offsetof(PropertyReference_t223937415, ___mProperty_3)); }
	inline PropertyInfo_t * get_mProperty_3() const { return ___mProperty_3; }
	inline PropertyInfo_t ** get_address_of_mProperty_3() { return &___mProperty_3; }
	inline void set_mProperty_3(PropertyInfo_t * value)
	{
		___mProperty_3 = value;
		Il2CppCodeGenWriteBarrier((&___mProperty_3), value);
	}
};

struct PropertyReference_t223937415_StaticFields
{
public:
	// System.Int32 PropertyReference::s_Hash
	int32_t ___s_Hash_4;

public:
	inline static int32_t get_offset_of_s_Hash_4() { return static_cast<int32_t>(offsetof(PropertyReference_t223937415_StaticFields, ___s_Hash_4)); }
	inline int32_t get_s_Hash_4() const { return ___s_Hash_4; }
	inline int32_t* get_address_of_s_Hash_4() { return &___s_Hash_4; }
	inline void set_s_Hash_4(int32_t value)
	{
		___s_Hash_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYREFERENCE_T223937415_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LOCALIZATION_T2163216738_H
#define LOCALIZATION_T2163216738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization
struct  Localization_t2163216738  : public RuntimeObject
{
public:

public:
};

struct Localization_t2163216738_StaticFields
{
public:
	// Localization/LoadFunction Localization::loadFunction
	LoadFunction_t2078002637 * ___loadFunction_0;
	// Localization/OnLocalizeNotification Localization::onLocalize
	OnLocalizeNotification_t3391620158 * ___onLocalize_1;
	// System.Boolean Localization::localizationHasBeenSet
	bool ___localizationHasBeenSet_2;
	// System.String[] Localization::mLanguages
	StringU5BU5D_t1281789340* ___mLanguages_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Localization::mOldDictionary
	Dictionary_2_t1632706988 * ___mOldDictionary_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String[]> Localization::mDictionary
	Dictionary_2_t1067045639 * ___mDictionary_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Localization::mReplacement
	Dictionary_2_t1632706988 * ___mReplacement_6;
	// System.Int32 Localization::mLanguageIndex
	int32_t ___mLanguageIndex_7;
	// System.String Localization::mLanguage
	String_t* ___mLanguage_8;
	// System.Boolean Localization::mMerging
	bool ___mMerging_9;

public:
	inline static int32_t get_offset_of_loadFunction_0() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___loadFunction_0)); }
	inline LoadFunction_t2078002637 * get_loadFunction_0() const { return ___loadFunction_0; }
	inline LoadFunction_t2078002637 ** get_address_of_loadFunction_0() { return &___loadFunction_0; }
	inline void set_loadFunction_0(LoadFunction_t2078002637 * value)
	{
		___loadFunction_0 = value;
		Il2CppCodeGenWriteBarrier((&___loadFunction_0), value);
	}

	inline static int32_t get_offset_of_onLocalize_1() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___onLocalize_1)); }
	inline OnLocalizeNotification_t3391620158 * get_onLocalize_1() const { return ___onLocalize_1; }
	inline OnLocalizeNotification_t3391620158 ** get_address_of_onLocalize_1() { return &___onLocalize_1; }
	inline void set_onLocalize_1(OnLocalizeNotification_t3391620158 * value)
	{
		___onLocalize_1 = value;
		Il2CppCodeGenWriteBarrier((&___onLocalize_1), value);
	}

	inline static int32_t get_offset_of_localizationHasBeenSet_2() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___localizationHasBeenSet_2)); }
	inline bool get_localizationHasBeenSet_2() const { return ___localizationHasBeenSet_2; }
	inline bool* get_address_of_localizationHasBeenSet_2() { return &___localizationHasBeenSet_2; }
	inline void set_localizationHasBeenSet_2(bool value)
	{
		___localizationHasBeenSet_2 = value;
	}

	inline static int32_t get_offset_of_mLanguages_3() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___mLanguages_3)); }
	inline StringU5BU5D_t1281789340* get_mLanguages_3() const { return ___mLanguages_3; }
	inline StringU5BU5D_t1281789340** get_address_of_mLanguages_3() { return &___mLanguages_3; }
	inline void set_mLanguages_3(StringU5BU5D_t1281789340* value)
	{
		___mLanguages_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLanguages_3), value);
	}

	inline static int32_t get_offset_of_mOldDictionary_4() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___mOldDictionary_4)); }
	inline Dictionary_2_t1632706988 * get_mOldDictionary_4() const { return ___mOldDictionary_4; }
	inline Dictionary_2_t1632706988 ** get_address_of_mOldDictionary_4() { return &___mOldDictionary_4; }
	inline void set_mOldDictionary_4(Dictionary_2_t1632706988 * value)
	{
		___mOldDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOldDictionary_4), value);
	}

	inline static int32_t get_offset_of_mDictionary_5() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___mDictionary_5)); }
	inline Dictionary_2_t1067045639 * get_mDictionary_5() const { return ___mDictionary_5; }
	inline Dictionary_2_t1067045639 ** get_address_of_mDictionary_5() { return &___mDictionary_5; }
	inline void set_mDictionary_5(Dictionary_2_t1067045639 * value)
	{
		___mDictionary_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDictionary_5), value);
	}

	inline static int32_t get_offset_of_mReplacement_6() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___mReplacement_6)); }
	inline Dictionary_2_t1632706988 * get_mReplacement_6() const { return ___mReplacement_6; }
	inline Dictionary_2_t1632706988 ** get_address_of_mReplacement_6() { return &___mReplacement_6; }
	inline void set_mReplacement_6(Dictionary_2_t1632706988 * value)
	{
		___mReplacement_6 = value;
		Il2CppCodeGenWriteBarrier((&___mReplacement_6), value);
	}

	inline static int32_t get_offset_of_mLanguageIndex_7() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___mLanguageIndex_7)); }
	inline int32_t get_mLanguageIndex_7() const { return ___mLanguageIndex_7; }
	inline int32_t* get_address_of_mLanguageIndex_7() { return &___mLanguageIndex_7; }
	inline void set_mLanguageIndex_7(int32_t value)
	{
		___mLanguageIndex_7 = value;
	}

	inline static int32_t get_offset_of_mLanguage_8() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___mLanguage_8)); }
	inline String_t* get_mLanguage_8() const { return ___mLanguage_8; }
	inline String_t** get_address_of_mLanguage_8() { return &___mLanguage_8; }
	inline void set_mLanguage_8(String_t* value)
	{
		___mLanguage_8 = value;
		Il2CppCodeGenWriteBarrier((&___mLanguage_8), value);
	}

	inline static int32_t get_offset_of_mMerging_9() { return static_cast<int32_t>(offsetof(Localization_t2163216738_StaticFields, ___mMerging_9)); }
	inline bool get_mMerging_9() const { return ___mMerging_9; }
	inline bool* get_address_of_mMerging_9() { return &___mMerging_9; }
	inline void set_mMerging_9(bool value)
	{
		___mMerging_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZATION_T2163216738_H
#ifndef PARAMETER_T2966927026_H
#define PARAMETER_T2966927026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventDelegate/Parameter
struct  Parameter_t2966927026  : public RuntimeObject
{
public:
	// UnityEngine.Object EventDelegate/Parameter::obj
	Object_t631007953 * ___obj_0;
	// System.String EventDelegate/Parameter::field
	String_t* ___field_1;
	// System.Object EventDelegate/Parameter::mValue
	RuntimeObject * ___mValue_2;
	// System.Type EventDelegate/Parameter::expectedType
	Type_t * ___expectedType_3;
	// System.Boolean EventDelegate/Parameter::cached
	bool ___cached_4;
	// System.Reflection.PropertyInfo EventDelegate/Parameter::propInfo
	PropertyInfo_t * ___propInfo_5;
	// System.Reflection.FieldInfo EventDelegate/Parameter::fieldInfo
	FieldInfo_t * ___fieldInfo_6;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(Parameter_t2966927026, ___obj_0)); }
	inline Object_t631007953 * get_obj_0() const { return ___obj_0; }
	inline Object_t631007953 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(Object_t631007953 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_field_1() { return static_cast<int32_t>(offsetof(Parameter_t2966927026, ___field_1)); }
	inline String_t* get_field_1() const { return ___field_1; }
	inline String_t** get_address_of_field_1() { return &___field_1; }
	inline void set_field_1(String_t* value)
	{
		___field_1 = value;
		Il2CppCodeGenWriteBarrier((&___field_1), value);
	}

	inline static int32_t get_offset_of_mValue_2() { return static_cast<int32_t>(offsetof(Parameter_t2966927026, ___mValue_2)); }
	inline RuntimeObject * get_mValue_2() const { return ___mValue_2; }
	inline RuntimeObject ** get_address_of_mValue_2() { return &___mValue_2; }
	inline void set_mValue_2(RuntimeObject * value)
	{
		___mValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___mValue_2), value);
	}

	inline static int32_t get_offset_of_expectedType_3() { return static_cast<int32_t>(offsetof(Parameter_t2966927026, ___expectedType_3)); }
	inline Type_t * get_expectedType_3() const { return ___expectedType_3; }
	inline Type_t ** get_address_of_expectedType_3() { return &___expectedType_3; }
	inline void set_expectedType_3(Type_t * value)
	{
		___expectedType_3 = value;
		Il2CppCodeGenWriteBarrier((&___expectedType_3), value);
	}

	inline static int32_t get_offset_of_cached_4() { return static_cast<int32_t>(offsetof(Parameter_t2966927026, ___cached_4)); }
	inline bool get_cached_4() const { return ___cached_4; }
	inline bool* get_address_of_cached_4() { return &___cached_4; }
	inline void set_cached_4(bool value)
	{
		___cached_4 = value;
	}

	inline static int32_t get_offset_of_propInfo_5() { return static_cast<int32_t>(offsetof(Parameter_t2966927026, ___propInfo_5)); }
	inline PropertyInfo_t * get_propInfo_5() const { return ___propInfo_5; }
	inline PropertyInfo_t ** get_address_of_propInfo_5() { return &___propInfo_5; }
	inline void set_propInfo_5(PropertyInfo_t * value)
	{
		___propInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___propInfo_5), value);
	}

	inline static int32_t get_offset_of_fieldInfo_6() { return static_cast<int32_t>(offsetof(Parameter_t2966927026, ___fieldInfo_6)); }
	inline FieldInfo_t * get_fieldInfo_6() const { return ___fieldInfo_6; }
	inline FieldInfo_t ** get_address_of_fieldInfo_6() { return &___fieldInfo_6; }
	inline void set_fieldInfo_6(FieldInfo_t * value)
	{
		___fieldInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETER_T2966927026_H
#ifndef BYTEREADER_T1539670756_H
#define BYTEREADER_T1539670756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteReader
struct  ByteReader_t1539670756  : public RuntimeObject
{
public:
	// System.Byte[] ByteReader::mBuffer
	ByteU5BU5D_t4116647657* ___mBuffer_0;
	// System.Int32 ByteReader::mOffset
	int32_t ___mOffset_1;

public:
	inline static int32_t get_offset_of_mBuffer_0() { return static_cast<int32_t>(offsetof(ByteReader_t1539670756, ___mBuffer_0)); }
	inline ByteU5BU5D_t4116647657* get_mBuffer_0() const { return ___mBuffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_mBuffer_0() { return &___mBuffer_0; }
	inline void set_mBuffer_0(ByteU5BU5D_t4116647657* value)
	{
		___mBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBuffer_0), value);
	}

	inline static int32_t get_offset_of_mOffset_1() { return static_cast<int32_t>(offsetof(ByteReader_t1539670756, ___mOffset_1)); }
	inline int32_t get_mOffset_1() const { return ___mOffset_1; }
	inline int32_t* get_address_of_mOffset_1() { return &___mOffset_1; }
	inline void set_mOffset_1(int32_t value)
	{
		___mOffset_1 = value;
	}
};

struct ByteReader_t1539670756_StaticFields
{
public:
	// BetterList`1<System.String> ByteReader::mTemp
	BetterList_1_t1002471007 * ___mTemp_2;

public:
	inline static int32_t get_offset_of_mTemp_2() { return static_cast<int32_t>(offsetof(ByteReader_t1539670756_StaticFields, ___mTemp_2)); }
	inline BetterList_1_t1002471007 * get_mTemp_2() const { return ___mTemp_2; }
	inline BetterList_1_t1002471007 ** get_address_of_mTemp_2() { return &___mTemp_2; }
	inline void set_mTemp_2(BetterList_1_t1002471007 * value)
	{
		___mTemp_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTemp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEREADER_T1539670756_H
#ifndef EVENTDELEGATE_T2738326060_H
#define EVENTDELEGATE_T2738326060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventDelegate
struct  EventDelegate_t2738326060  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour EventDelegate::mTarget
	MonoBehaviour_t3962482529 * ___mTarget_0;
	// System.String EventDelegate::mMethodName
	String_t* ___mMethodName_1;
	// EventDelegate/Parameter[] EventDelegate::mParameters
	ParameterU5BU5D_t2885283399* ___mParameters_2;
	// System.Boolean EventDelegate::oneShot
	bool ___oneShot_3;
	// EventDelegate/Callback EventDelegate::mCachedCallback
	Callback_t3139336517 * ___mCachedCallback_4;
	// System.Boolean EventDelegate::mRawDelegate
	bool ___mRawDelegate_5;
	// System.Boolean EventDelegate::mCached
	bool ___mCached_6;
	// System.Reflection.MethodInfo EventDelegate::mMethod
	MethodInfo_t * ___mMethod_7;
	// System.Reflection.ParameterInfo[] EventDelegate::mParameterInfos
	ParameterInfoU5BU5D_t390618515* ___mParameterInfos_8;
	// System.Object[] EventDelegate::mArgs
	ObjectU5BU5D_t2843939325* ___mArgs_9;

public:
	inline static int32_t get_offset_of_mTarget_0() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mTarget_0)); }
	inline MonoBehaviour_t3962482529 * get_mTarget_0() const { return ___mTarget_0; }
	inline MonoBehaviour_t3962482529 ** get_address_of_mTarget_0() { return &___mTarget_0; }
	inline void set_mTarget_0(MonoBehaviour_t3962482529 * value)
	{
		___mTarget_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTarget_0), value);
	}

	inline static int32_t get_offset_of_mMethodName_1() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mMethodName_1)); }
	inline String_t* get_mMethodName_1() const { return ___mMethodName_1; }
	inline String_t** get_address_of_mMethodName_1() { return &___mMethodName_1; }
	inline void set_mMethodName_1(String_t* value)
	{
		___mMethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMethodName_1), value);
	}

	inline static int32_t get_offset_of_mParameters_2() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mParameters_2)); }
	inline ParameterU5BU5D_t2885283399* get_mParameters_2() const { return ___mParameters_2; }
	inline ParameterU5BU5D_t2885283399** get_address_of_mParameters_2() { return &___mParameters_2; }
	inline void set_mParameters_2(ParameterU5BU5D_t2885283399* value)
	{
		___mParameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___mParameters_2), value);
	}

	inline static int32_t get_offset_of_oneShot_3() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___oneShot_3)); }
	inline bool get_oneShot_3() const { return ___oneShot_3; }
	inline bool* get_address_of_oneShot_3() { return &___oneShot_3; }
	inline void set_oneShot_3(bool value)
	{
		___oneShot_3 = value;
	}

	inline static int32_t get_offset_of_mCachedCallback_4() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mCachedCallback_4)); }
	inline Callback_t3139336517 * get_mCachedCallback_4() const { return ___mCachedCallback_4; }
	inline Callback_t3139336517 ** get_address_of_mCachedCallback_4() { return &___mCachedCallback_4; }
	inline void set_mCachedCallback_4(Callback_t3139336517 * value)
	{
		___mCachedCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCachedCallback_4), value);
	}

	inline static int32_t get_offset_of_mRawDelegate_5() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mRawDelegate_5)); }
	inline bool get_mRawDelegate_5() const { return ___mRawDelegate_5; }
	inline bool* get_address_of_mRawDelegate_5() { return &___mRawDelegate_5; }
	inline void set_mRawDelegate_5(bool value)
	{
		___mRawDelegate_5 = value;
	}

	inline static int32_t get_offset_of_mCached_6() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mCached_6)); }
	inline bool get_mCached_6() const { return ___mCached_6; }
	inline bool* get_address_of_mCached_6() { return &___mCached_6; }
	inline void set_mCached_6(bool value)
	{
		___mCached_6 = value;
	}

	inline static int32_t get_offset_of_mMethod_7() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mMethod_7)); }
	inline MethodInfo_t * get_mMethod_7() const { return ___mMethod_7; }
	inline MethodInfo_t ** get_address_of_mMethod_7() { return &___mMethod_7; }
	inline void set_mMethod_7(MethodInfo_t * value)
	{
		___mMethod_7 = value;
		Il2CppCodeGenWriteBarrier((&___mMethod_7), value);
	}

	inline static int32_t get_offset_of_mParameterInfos_8() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mParameterInfos_8)); }
	inline ParameterInfoU5BU5D_t390618515* get_mParameterInfos_8() const { return ___mParameterInfos_8; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_mParameterInfos_8() { return &___mParameterInfos_8; }
	inline void set_mParameterInfos_8(ParameterInfoU5BU5D_t390618515* value)
	{
		___mParameterInfos_8 = value;
		Il2CppCodeGenWriteBarrier((&___mParameterInfos_8), value);
	}

	inline static int32_t get_offset_of_mArgs_9() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mArgs_9)); }
	inline ObjectU5BU5D_t2843939325* get_mArgs_9() const { return ___mArgs_9; }
	inline ObjectU5BU5D_t2843939325** get_address_of_mArgs_9() { return &___mArgs_9; }
	inline void set_mArgs_9(ObjectU5BU5D_t2843939325* value)
	{
		___mArgs_9 = value;
		Il2CppCodeGenWriteBarrier((&___mArgs_9), value);
	}
};

struct EventDelegate_t2738326060_StaticFields
{
public:
	// System.Int32 EventDelegate::s_Hash
	int32_t ___s_Hash_10;

public:
	inline static int32_t get_offset_of_s_Hash_10() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060_StaticFields, ___s_Hash_10)); }
	inline int32_t get_s_Hash_10() const { return ___s_Hash_10; }
	inline int32_t* get_address_of_s_Hash_10() { return &___s_Hash_10; }
	inline void set_s_Hash_10(int32_t value)
	{
		___s_Hash_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDELEGATE_T2738326060_H
#ifndef BMGLYPH_T3344884546_H
#define BMGLYPH_T3344884546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMGlyph
struct  BMGlyph_t3344884546  : public RuntimeObject
{
public:
	// System.Int32 BMGlyph::index
	int32_t ___index_0;
	// System.Int32 BMGlyph::x
	int32_t ___x_1;
	// System.Int32 BMGlyph::y
	int32_t ___y_2;
	// System.Int32 BMGlyph::width
	int32_t ___width_3;
	// System.Int32 BMGlyph::height
	int32_t ___height_4;
	// System.Int32 BMGlyph::offsetX
	int32_t ___offsetX_5;
	// System.Int32 BMGlyph::offsetY
	int32_t ___offsetY_6;
	// System.Int32 BMGlyph::advance
	int32_t ___advance_7;
	// System.Int32 BMGlyph::channel
	int32_t ___channel_8;
	// System.Collections.Generic.List`1<System.Int32> BMGlyph::kerning
	List_1_t128053199 * ___kerning_9;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___x_1)); }
	inline int32_t get_x_1() const { return ___x_1; }
	inline int32_t* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(int32_t value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___y_2)); }
	inline int32_t get_y_2() const { return ___y_2; }
	inline int32_t* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(int32_t value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_offsetX_5() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___offsetX_5)); }
	inline int32_t get_offsetX_5() const { return ___offsetX_5; }
	inline int32_t* get_address_of_offsetX_5() { return &___offsetX_5; }
	inline void set_offsetX_5(int32_t value)
	{
		___offsetX_5 = value;
	}

	inline static int32_t get_offset_of_offsetY_6() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___offsetY_6)); }
	inline int32_t get_offsetY_6() const { return ___offsetY_6; }
	inline int32_t* get_address_of_offsetY_6() { return &___offsetY_6; }
	inline void set_offsetY_6(int32_t value)
	{
		___offsetY_6 = value;
	}

	inline static int32_t get_offset_of_advance_7() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___advance_7)); }
	inline int32_t get_advance_7() const { return ___advance_7; }
	inline int32_t* get_address_of_advance_7() { return &___advance_7; }
	inline void set_advance_7(int32_t value)
	{
		___advance_7 = value;
	}

	inline static int32_t get_offset_of_channel_8() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___channel_8)); }
	inline int32_t get_channel_8() const { return ___channel_8; }
	inline int32_t* get_address_of_channel_8() { return &___channel_8; }
	inline void set_channel_8(int32_t value)
	{
		___channel_8 = value;
	}

	inline static int32_t get_offset_of_kerning_9() { return static_cast<int32_t>(offsetof(BMGlyph_t3344884546, ___kerning_9)); }
	inline List_1_t128053199 * get_kerning_9() const { return ___kerning_9; }
	inline List_1_t128053199 ** get_address_of_kerning_9() { return &___kerning_9; }
	inline void set_kerning_9(List_1_t128053199 * value)
	{
		___kerning_9 = value;
		Il2CppCodeGenWriteBarrier((&___kerning_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BMGLYPH_T3344884546_H
#ifndef BMFONT_T2757936676_H
#define BMFONT_T2757936676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMFont
struct  BMFont_t2757936676  : public RuntimeObject
{
public:
	// System.Int32 BMFont::mSize
	int32_t ___mSize_0;
	// System.Int32 BMFont::mBase
	int32_t ___mBase_1;
	// System.Int32 BMFont::mWidth
	int32_t ___mWidth_2;
	// System.Int32 BMFont::mHeight
	int32_t ___mHeight_3;
	// System.String BMFont::mSpriteName
	String_t* ___mSpriteName_4;
	// System.Collections.Generic.List`1<BMGlyph> BMFont::mSaved
	List_1_t521991992 * ___mSaved_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph> BMFont::mDict
	Dictionary_2_t2233597877 * ___mDict_6;

public:
	inline static int32_t get_offset_of_mSize_0() { return static_cast<int32_t>(offsetof(BMFont_t2757936676, ___mSize_0)); }
	inline int32_t get_mSize_0() const { return ___mSize_0; }
	inline int32_t* get_address_of_mSize_0() { return &___mSize_0; }
	inline void set_mSize_0(int32_t value)
	{
		___mSize_0 = value;
	}

	inline static int32_t get_offset_of_mBase_1() { return static_cast<int32_t>(offsetof(BMFont_t2757936676, ___mBase_1)); }
	inline int32_t get_mBase_1() const { return ___mBase_1; }
	inline int32_t* get_address_of_mBase_1() { return &___mBase_1; }
	inline void set_mBase_1(int32_t value)
	{
		___mBase_1 = value;
	}

	inline static int32_t get_offset_of_mWidth_2() { return static_cast<int32_t>(offsetof(BMFont_t2757936676, ___mWidth_2)); }
	inline int32_t get_mWidth_2() const { return ___mWidth_2; }
	inline int32_t* get_address_of_mWidth_2() { return &___mWidth_2; }
	inline void set_mWidth_2(int32_t value)
	{
		___mWidth_2 = value;
	}

	inline static int32_t get_offset_of_mHeight_3() { return static_cast<int32_t>(offsetof(BMFont_t2757936676, ___mHeight_3)); }
	inline int32_t get_mHeight_3() const { return ___mHeight_3; }
	inline int32_t* get_address_of_mHeight_3() { return &___mHeight_3; }
	inline void set_mHeight_3(int32_t value)
	{
		___mHeight_3 = value;
	}

	inline static int32_t get_offset_of_mSpriteName_4() { return static_cast<int32_t>(offsetof(BMFont_t2757936676, ___mSpriteName_4)); }
	inline String_t* get_mSpriteName_4() const { return ___mSpriteName_4; }
	inline String_t** get_address_of_mSpriteName_4() { return &___mSpriteName_4; }
	inline void set_mSpriteName_4(String_t* value)
	{
		___mSpriteName_4 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteName_4), value);
	}

	inline static int32_t get_offset_of_mSaved_5() { return static_cast<int32_t>(offsetof(BMFont_t2757936676, ___mSaved_5)); }
	inline List_1_t521991992 * get_mSaved_5() const { return ___mSaved_5; }
	inline List_1_t521991992 ** get_address_of_mSaved_5() { return &___mSaved_5; }
	inline void set_mSaved_5(List_1_t521991992 * value)
	{
		___mSaved_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSaved_5), value);
	}

	inline static int32_t get_offset_of_mDict_6() { return static_cast<int32_t>(offsetof(BMFont_t2757936676, ___mDict_6)); }
	inline Dictionary_2_t2233597877 * get_mDict_6() const { return ___mDict_6; }
	inline Dictionary_2_t2233597877 ** get_address_of_mDict_6() { return &___mDict_6; }
	inline void set_mDict_6(Dictionary_2_t2233597877 * value)
	{
		___mDict_6 = value;
		Il2CppCodeGenWriteBarrier((&___mDict_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BMFONT_T2757936676_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef DONOTOBFUSCATENGUI_T4082232512_H
#define DONOTOBFUSCATENGUI_T4082232512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoNotObfuscateNGUI
struct  DoNotObfuscateNGUI_t4082232512  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONOTOBFUSCATENGUI_T4082232512_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SIDE_T3584783117_H
#define SIDE_T3584783117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAnchor/Side
struct  Side_t3584783117 
{
public:
	// System.Int32 UIAnchor/Side::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Side_t3584783117, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIDE_T3584783117_H
#ifndef SPRITE_T2895597119_H
#define SPRITE_T2895597119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAtlas/Sprite
struct  Sprite_t2895597119  : public RuntimeObject
{
public:
	// System.String UIAtlas/Sprite::name
	String_t* ___name_0;
	// UnityEngine.Rect UIAtlas/Sprite::outer
	Rect_t2360479859  ___outer_1;
	// UnityEngine.Rect UIAtlas/Sprite::inner
	Rect_t2360479859  ___inner_2;
	// System.Boolean UIAtlas/Sprite::rotated
	bool ___rotated_3;
	// System.Single UIAtlas/Sprite::paddingLeft
	float ___paddingLeft_4;
	// System.Single UIAtlas/Sprite::paddingRight
	float ___paddingRight_5;
	// System.Single UIAtlas/Sprite::paddingTop
	float ___paddingTop_6;
	// System.Single UIAtlas/Sprite::paddingBottom
	float ___paddingBottom_7;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_outer_1() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___outer_1)); }
	inline Rect_t2360479859  get_outer_1() const { return ___outer_1; }
	inline Rect_t2360479859 * get_address_of_outer_1() { return &___outer_1; }
	inline void set_outer_1(Rect_t2360479859  value)
	{
		___outer_1 = value;
	}

	inline static int32_t get_offset_of_inner_2() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___inner_2)); }
	inline Rect_t2360479859  get_inner_2() const { return ___inner_2; }
	inline Rect_t2360479859 * get_address_of_inner_2() { return &___inner_2; }
	inline void set_inner_2(Rect_t2360479859  value)
	{
		___inner_2 = value;
	}

	inline static int32_t get_offset_of_rotated_3() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___rotated_3)); }
	inline bool get_rotated_3() const { return ___rotated_3; }
	inline bool* get_address_of_rotated_3() { return &___rotated_3; }
	inline void set_rotated_3(bool value)
	{
		___rotated_3 = value;
	}

	inline static int32_t get_offset_of_paddingLeft_4() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___paddingLeft_4)); }
	inline float get_paddingLeft_4() const { return ___paddingLeft_4; }
	inline float* get_address_of_paddingLeft_4() { return &___paddingLeft_4; }
	inline void set_paddingLeft_4(float value)
	{
		___paddingLeft_4 = value;
	}

	inline static int32_t get_offset_of_paddingRight_5() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___paddingRight_5)); }
	inline float get_paddingRight_5() const { return ___paddingRight_5; }
	inline float* get_address_of_paddingRight_5() { return &___paddingRight_5; }
	inline void set_paddingRight_5(float value)
	{
		___paddingRight_5 = value;
	}

	inline static int32_t get_offset_of_paddingTop_6() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___paddingTop_6)); }
	inline float get_paddingTop_6() const { return ___paddingTop_6; }
	inline float* get_address_of_paddingTop_6() { return &___paddingTop_6; }
	inline void set_paddingTop_6(float value)
	{
		___paddingTop_6 = value;
	}

	inline static int32_t get_offset_of_paddingBottom_7() { return static_cast<int32_t>(offsetof(Sprite_t2895597119, ___paddingBottom_7)); }
	inline float get_paddingBottom_7() const { return ___paddingBottom_7; }
	inline float* get_address_of_paddingBottom_7() { return &___paddingBottom_7; }
	inline void set_paddingBottom_7(float value)
	{
		___paddingBottom_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T2895597119_H
#ifndef COLORSPACE_T3453996949_H
#define COLORSPACE_T3453996949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ColorSpace
struct  ColorSpace_t3453996949 
{
public:
	// System.Int32 UnityEngine.ColorSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorSpace_t3453996949, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSPACE_T3453996949_H
#ifndef COORDINATES_T1880298793_H
#define COORDINATES_T1880298793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAtlas/Coordinates
struct  Coordinates_t1880298793 
{
public:
	// System.Int32 UIAtlas/Coordinates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Coordinates_t1880298793, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COORDINATES_T1880298793_H
#ifndef CONTROLSCHEME_T4038670825_H
#define CONTROLSCHEME_T4038670825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ControlScheme
struct  ControlScheme_t4038670825 
{
public:
	// System.Int32 UICamera/ControlScheme::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlScheme_t4038670825, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSCHEME_T4038670825_H
#ifndef EVENTTYPE_T561105572_H
#define EVENTTYPE_T561105572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/EventType
struct  EventType_t561105572 
{
public:
	// System.Int32 UICamera/EventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventType_t561105572, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T561105572_H
#ifndef PIVOT_T1798046373_H
#define PIVOT_T1798046373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/Pivot
struct  Pivot_t1798046373 
{
public:
	// System.Int32 UIWidget/Pivot::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pivot_t1798046373, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOT_T1798046373_H
#ifndef UIGEOMETRY_T1059483952_H
#define UIGEOMETRY_T1059483952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGeometry
struct  UIGeometry_t1059483952  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIGeometry::verts
	List_1_t899420910 * ___verts_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UIGeometry::uvs
	List_1_t3628304265 * ___uvs_1;
	// System.Collections.Generic.List`1<UnityEngine.Color> UIGeometry::cols
	List_1_t4027761066 * ___cols_2;
	// UIGeometry/OnCustomWrite UIGeometry::onCustomWrite
	OnCustomWrite_t2800546165 * ___onCustomWrite_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIGeometry::mRtpVerts
	List_1_t899420910 * ___mRtpVerts_4;
	// UnityEngine.Vector3 UIGeometry::mRtpNormal
	Vector3_t3722313464  ___mRtpNormal_5;
	// UnityEngine.Vector4 UIGeometry::mRtpTan
	Vector4_t3319028937  ___mRtpTan_6;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___verts_0)); }
	inline List_1_t899420910 * get_verts_0() const { return ___verts_0; }
	inline List_1_t899420910 ** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(List_1_t899420910 * value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier((&___verts_0), value);
	}

	inline static int32_t get_offset_of_uvs_1() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___uvs_1)); }
	inline List_1_t3628304265 * get_uvs_1() const { return ___uvs_1; }
	inline List_1_t3628304265 ** get_address_of_uvs_1() { return &___uvs_1; }
	inline void set_uvs_1(List_1_t3628304265 * value)
	{
		___uvs_1 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_1), value);
	}

	inline static int32_t get_offset_of_cols_2() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___cols_2)); }
	inline List_1_t4027761066 * get_cols_2() const { return ___cols_2; }
	inline List_1_t4027761066 ** get_address_of_cols_2() { return &___cols_2; }
	inline void set_cols_2(List_1_t4027761066 * value)
	{
		___cols_2 = value;
		Il2CppCodeGenWriteBarrier((&___cols_2), value);
	}

	inline static int32_t get_offset_of_onCustomWrite_3() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___onCustomWrite_3)); }
	inline OnCustomWrite_t2800546165 * get_onCustomWrite_3() const { return ___onCustomWrite_3; }
	inline OnCustomWrite_t2800546165 ** get_address_of_onCustomWrite_3() { return &___onCustomWrite_3; }
	inline void set_onCustomWrite_3(OnCustomWrite_t2800546165 * value)
	{
		___onCustomWrite_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCustomWrite_3), value);
	}

	inline static int32_t get_offset_of_mRtpVerts_4() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___mRtpVerts_4)); }
	inline List_1_t899420910 * get_mRtpVerts_4() const { return ___mRtpVerts_4; }
	inline List_1_t899420910 ** get_address_of_mRtpVerts_4() { return &___mRtpVerts_4; }
	inline void set_mRtpVerts_4(List_1_t899420910 * value)
	{
		___mRtpVerts_4 = value;
		Il2CppCodeGenWriteBarrier((&___mRtpVerts_4), value);
	}

	inline static int32_t get_offset_of_mRtpNormal_5() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___mRtpNormal_5)); }
	inline Vector3_t3722313464  get_mRtpNormal_5() const { return ___mRtpNormal_5; }
	inline Vector3_t3722313464 * get_address_of_mRtpNormal_5() { return &___mRtpNormal_5; }
	inline void set_mRtpNormal_5(Vector3_t3722313464  value)
	{
		___mRtpNormal_5 = value;
	}

	inline static int32_t get_offset_of_mRtpTan_6() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___mRtpTan_6)); }
	inline Vector4_t3319028937  get_mRtpTan_6() const { return ___mRtpTan_6; }
	inline Vector4_t3319028937 * get_address_of_mRtpTan_6() { return &___mRtpTan_6; }
	inline void set_mRtpTan_6(Vector4_t3319028937  value)
	{
		___mRtpTan_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGEOMETRY_T1059483952_H
#ifndef FONTSTYLE_T82229486_H
#define FONTSTYLE_T82229486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t82229486 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyle_t82229486, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T82229486_H
#ifndef ASPECTRATIOSOURCE_T168813522_H
#define ASPECTRATIOSOURCE_T168813522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/AspectRatioSource
struct  AspectRatioSource_t168813522 
{
public:
	// System.Int32 UIWidget/AspectRatioSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectRatioSource_t168813522, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOSOURCE_T168813522_H
#ifndef ANCHORUPDATE_T1570184075_H
#define ANCHORUPDATE_T1570184075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect/AnchorUpdate
struct  AnchorUpdate_t1570184075 
{
public:
	// System.Int32 UIRect/AnchorUpdate::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnchorUpdate_t1570184075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORUPDATE_T1570184075_H
#ifndef CLICKNOTIFICATION_T3778629126_H
#define CLICKNOTIFICATION_T3778629126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ClickNotification
struct  ClickNotification_t3778629126 
{
public:
	// System.Int32 UICamera/ClickNotification::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClickNotification_t3778629126, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKNOTIFICATION_T3778629126_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CLIPPING_T1109313910_H
#define CLIPPING_T1109313910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/Clipping
struct  Clipping_t1109313910 
{
public:
	// System.Int32 UIDrawCall/Clipping::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Clipping_t1109313910, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_T1109313910_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef TRIGGER_T3745258312_H
#define TRIGGER_T3745258312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Trigger
struct  Trigger_t3745258312 
{
public:
	// System.Int32 AnimationOrTween.Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t3745258312, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T3745258312_H
#ifndef DIRECTION_T2061188385_H
#define DIRECTION_T2061188385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Direction
struct  Direction_t2061188385 
{
public:
	// System.Int32 AnimationOrTween.Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t2061188385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T2061188385_H
#ifndef ENABLECONDITION_T1125033030_H
#define ENABLECONDITION_T1125033030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.EnableCondition
struct  EnableCondition_t1125033030 
{
public:
	// System.Int32 AnimationOrTween.EnableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EnableCondition_t1125033030, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLECONDITION_T1125033030_H
#ifndef DISABLECONDITION_T2151257518_H
#define DISABLECONDITION_T2151257518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.DisableCondition
struct  DisableCondition_t2151257518 
{
public:
	// System.Int32 AnimationOrTween.DisableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisableCondition_t2151257518, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECONDITION_T2151257518_H
#ifndef BMSYMBOL_T1586058841_H
#define BMSYMBOL_T1586058841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMSymbol
struct  BMSymbol_t1586058841  : public RuntimeObject
{
public:
	// System.String BMSymbol::sequence
	String_t* ___sequence_0;
	// System.String BMSymbol::spriteName
	String_t* ___spriteName_1;
	// UISpriteData BMSymbol::mSprite
	UISpriteData_t900308526 * ___mSprite_2;
	// System.Boolean BMSymbol::mIsValid
	bool ___mIsValid_3;
	// System.Int32 BMSymbol::mLength
	int32_t ___mLength_4;
	// System.Int32 BMSymbol::mOffsetX
	int32_t ___mOffsetX_5;
	// System.Int32 BMSymbol::mOffsetY
	int32_t ___mOffsetY_6;
	// System.Int32 BMSymbol::mWidth
	int32_t ___mWidth_7;
	// System.Int32 BMSymbol::mHeight
	int32_t ___mHeight_8;
	// System.Int32 BMSymbol::mAdvance
	int32_t ___mAdvance_9;
	// UnityEngine.Rect BMSymbol::mUV
	Rect_t2360479859  ___mUV_10;

public:
	inline static int32_t get_offset_of_sequence_0() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___sequence_0)); }
	inline String_t* get_sequence_0() const { return ___sequence_0; }
	inline String_t** get_address_of_sequence_0() { return &___sequence_0; }
	inline void set_sequence_0(String_t* value)
	{
		___sequence_0 = value;
		Il2CppCodeGenWriteBarrier((&___sequence_0), value);
	}

	inline static int32_t get_offset_of_spriteName_1() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___spriteName_1)); }
	inline String_t* get_spriteName_1() const { return ___spriteName_1; }
	inline String_t** get_address_of_spriteName_1() { return &___spriteName_1; }
	inline void set_spriteName_1(String_t* value)
	{
		___spriteName_1 = value;
		Il2CppCodeGenWriteBarrier((&___spriteName_1), value);
	}

	inline static int32_t get_offset_of_mSprite_2() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mSprite_2)); }
	inline UISpriteData_t900308526 * get_mSprite_2() const { return ___mSprite_2; }
	inline UISpriteData_t900308526 ** get_address_of_mSprite_2() { return &___mSprite_2; }
	inline void set_mSprite_2(UISpriteData_t900308526 * value)
	{
		___mSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_2), value);
	}

	inline static int32_t get_offset_of_mIsValid_3() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mIsValid_3)); }
	inline bool get_mIsValid_3() const { return ___mIsValid_3; }
	inline bool* get_address_of_mIsValid_3() { return &___mIsValid_3; }
	inline void set_mIsValid_3(bool value)
	{
		___mIsValid_3 = value;
	}

	inline static int32_t get_offset_of_mLength_4() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mLength_4)); }
	inline int32_t get_mLength_4() const { return ___mLength_4; }
	inline int32_t* get_address_of_mLength_4() { return &___mLength_4; }
	inline void set_mLength_4(int32_t value)
	{
		___mLength_4 = value;
	}

	inline static int32_t get_offset_of_mOffsetX_5() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mOffsetX_5)); }
	inline int32_t get_mOffsetX_5() const { return ___mOffsetX_5; }
	inline int32_t* get_address_of_mOffsetX_5() { return &___mOffsetX_5; }
	inline void set_mOffsetX_5(int32_t value)
	{
		___mOffsetX_5 = value;
	}

	inline static int32_t get_offset_of_mOffsetY_6() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mOffsetY_6)); }
	inline int32_t get_mOffsetY_6() const { return ___mOffsetY_6; }
	inline int32_t* get_address_of_mOffsetY_6() { return &___mOffsetY_6; }
	inline void set_mOffsetY_6(int32_t value)
	{
		___mOffsetY_6 = value;
	}

	inline static int32_t get_offset_of_mWidth_7() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mWidth_7)); }
	inline int32_t get_mWidth_7() const { return ___mWidth_7; }
	inline int32_t* get_address_of_mWidth_7() { return &___mWidth_7; }
	inline void set_mWidth_7(int32_t value)
	{
		___mWidth_7 = value;
	}

	inline static int32_t get_offset_of_mHeight_8() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mHeight_8)); }
	inline int32_t get_mHeight_8() const { return ___mHeight_8; }
	inline int32_t* get_address_of_mHeight_8() { return &___mHeight_8; }
	inline void set_mHeight_8(int32_t value)
	{
		___mHeight_8 = value;
	}

	inline static int32_t get_offset_of_mAdvance_9() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mAdvance_9)); }
	inline int32_t get_mAdvance_9() const { return ___mAdvance_9; }
	inline int32_t* get_address_of_mAdvance_9() { return &___mAdvance_9; }
	inline void set_mAdvance_9(int32_t value)
	{
		___mAdvance_9 = value;
	}

	inline static int32_t get_offset_of_mUV_10() { return static_cast<int32_t>(offsetof(BMSymbol_t1586058841, ___mUV_10)); }
	inline Rect_t2360479859  get_mUV_10() const { return ___mUV_10; }
	inline Rect_t2360479859 * get_address_of_mUV_10() { return &___mUV_10; }
	inline void set_mUV_10(Rect_t2360479859  value)
	{
		___mUV_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BMSYMBOL_T1586058841_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef MINMAXRANGEATTRIBUTE_T3652335020_H
#define MINMAXRANGEATTRIBUTE_T3652335020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MinMaxRangeAttribute
struct  MinMaxRangeAttribute_t3652335020  : public PropertyAttribute_t3677895545
{
public:
	// System.Single MinMaxRangeAttribute::minLimit
	float ___minLimit_0;
	// System.Single MinMaxRangeAttribute::maxLimit
	float ___maxLimit_1;

public:
	inline static int32_t get_offset_of_minLimit_0() { return static_cast<int32_t>(offsetof(MinMaxRangeAttribute_t3652335020, ___minLimit_0)); }
	inline float get_minLimit_0() const { return ___minLimit_0; }
	inline float* get_address_of_minLimit_0() { return &___minLimit_0; }
	inline void set_minLimit_0(float value)
	{
		___minLimit_0 = value;
	}

	inline static int32_t get_offset_of_maxLimit_1() { return static_cast<int32_t>(offsetof(MinMaxRangeAttribute_t3652335020, ___maxLimit_1)); }
	inline float get_maxLimit_1() const { return ___maxLimit_1; }
	inline float* get_address_of_maxLimit_1() { return &___maxLimit_1; }
	inline void set_maxLimit_1(float value)
	{
		___maxLimit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXRANGEATTRIBUTE_T3652335020_H
#ifndef SHADOWMODE_T1455171354_H
#define SHADOWMODE_T1455171354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/ShadowMode
struct  ShadowMode_t1455171354 
{
public:
	// System.Int32 UIDrawCall/ShadowMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShadowMode_t1455171354, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWMODE_T1455171354_H
#ifndef ALIGNMENT_T3228070485_H
#define ALIGNMENT_T3228070485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/Alignment
struct  Alignment_t3228070485 
{
public:
	// System.Int32 NGUIText/Alignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Alignment_t3228070485, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T3228070485_H
#ifndef SYMBOLSTYLE_T3792107337_H
#define SYMBOLSTYLE_T3792107337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/SymbolStyle
struct  SymbolStyle_t3792107337 
{
public:
	// System.Int32 NGUIText/SymbolStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SymbolStyle_t3792107337, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSTYLE_T3792107337_H
#ifndef GLYPHINFO_T1020792323_H
#define GLYPHINFO_T1020792323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/GlyphInfo
struct  GlyphInfo_t1020792323  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 NGUIText/GlyphInfo::v0
	Vector2_t2156229523  ___v0_0;
	// UnityEngine.Vector2 NGUIText/GlyphInfo::v1
	Vector2_t2156229523  ___v1_1;
	// UnityEngine.Vector2 NGUIText/GlyphInfo::u0
	Vector2_t2156229523  ___u0_2;
	// UnityEngine.Vector2 NGUIText/GlyphInfo::u1
	Vector2_t2156229523  ___u1_3;
	// UnityEngine.Vector2 NGUIText/GlyphInfo::u2
	Vector2_t2156229523  ___u2_4;
	// UnityEngine.Vector2 NGUIText/GlyphInfo::u3
	Vector2_t2156229523  ___u3_5;
	// System.Single NGUIText/GlyphInfo::advance
	float ___advance_6;
	// System.Int32 NGUIText/GlyphInfo::channel
	int32_t ___channel_7;

public:
	inline static int32_t get_offset_of_v0_0() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___v0_0)); }
	inline Vector2_t2156229523  get_v0_0() const { return ___v0_0; }
	inline Vector2_t2156229523 * get_address_of_v0_0() { return &___v0_0; }
	inline void set_v0_0(Vector2_t2156229523  value)
	{
		___v0_0 = value;
	}

	inline static int32_t get_offset_of_v1_1() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___v1_1)); }
	inline Vector2_t2156229523  get_v1_1() const { return ___v1_1; }
	inline Vector2_t2156229523 * get_address_of_v1_1() { return &___v1_1; }
	inline void set_v1_1(Vector2_t2156229523  value)
	{
		___v1_1 = value;
	}

	inline static int32_t get_offset_of_u0_2() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___u0_2)); }
	inline Vector2_t2156229523  get_u0_2() const { return ___u0_2; }
	inline Vector2_t2156229523 * get_address_of_u0_2() { return &___u0_2; }
	inline void set_u0_2(Vector2_t2156229523  value)
	{
		___u0_2 = value;
	}

	inline static int32_t get_offset_of_u1_3() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___u1_3)); }
	inline Vector2_t2156229523  get_u1_3() const { return ___u1_3; }
	inline Vector2_t2156229523 * get_address_of_u1_3() { return &___u1_3; }
	inline void set_u1_3(Vector2_t2156229523  value)
	{
		___u1_3 = value;
	}

	inline static int32_t get_offset_of_u2_4() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___u2_4)); }
	inline Vector2_t2156229523  get_u2_4() const { return ___u2_4; }
	inline Vector2_t2156229523 * get_address_of_u2_4() { return &___u2_4; }
	inline void set_u2_4(Vector2_t2156229523  value)
	{
		___u2_4 = value;
	}

	inline static int32_t get_offset_of_u3_5() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___u3_5)); }
	inline Vector2_t2156229523  get_u3_5() const { return ___u3_5; }
	inline Vector2_t2156229523 * get_address_of_u3_5() { return &___u3_5; }
	inline void set_u3_5(Vector2_t2156229523  value)
	{
		___u3_5 = value;
	}

	inline static int32_t get_offset_of_advance_6() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___advance_6)); }
	inline float get_advance_6() const { return ___advance_6; }
	inline float* get_address_of_advance_6() { return &___advance_6; }
	inline void set_advance_6(float value)
	{
		___advance_6 = value;
	}

	inline static int32_t get_offset_of_channel_7() { return static_cast<int32_t>(offsetof(GlyphInfo_t1020792323, ___channel_7)); }
	inline int32_t get_channel_7() const { return ___channel_7; }
	inline int32_t* get_address_of_channel_7() { return &___channel_7; }
	inline void set_channel_7(int32_t value)
	{
		___channel_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHINFO_T1020792323_H
#ifndef PLANE_T1000493321_H
#define PLANE_T1000493321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1000493321 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3722313464  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Normal_0)); }
	inline Vector3_t3722313464  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3722313464  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1000493321_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef STYLE_T3120619385_H
#define STYLE_T3120619385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Style
struct  Style_t3120619385 
{
public:
	// System.Int32 UITweener/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t3120619385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T3120619385_H
#ifndef FLIP_T2552321477_H
#define FLIP_T2552321477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/Flip
struct  Flip_t2552321477 
{
public:
	// System.Int32 UIBasicSprite/Flip::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Flip_t2552321477, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLIP_T2552321477_H
#ifndef ADVANCEDTYPE_T3940519926_H
#define ADVANCEDTYPE_T3940519926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/AdvancedType
struct  AdvancedType_t3940519926 
{
public:
	// System.Int32 UIBasicSprite/AdvancedType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvancedType_t3940519926, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDTYPE_T3940519926_H
#ifndef FILLDIRECTION_T904769527_H
#define FILLDIRECTION_T904769527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/FillDirection
struct  FillDirection_t904769527 
{
public:
	// System.Int32 UIBasicSprite/FillDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillDirection_t904769527, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLDIRECTION_T904769527_H
#ifndef TYPE_T4088629396_H
#define TYPE_T4088629396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/Type
struct  Type_t4088629396 
{
public:
	// System.Int32 UIBasicSprite/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t4088629396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T4088629396_H
#ifndef METHOD_T1730494418_H
#define METHOD_T1730494418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Method
struct  Method_t1730494418 
{
public:
	// System.Int32 UITweener/Method::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Method_t1730494418, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T1730494418_H
#ifndef PROCESSEVENTSIN_T702674362_H
#define PROCESSEVENTSIN_T702674362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ProcessEventsIn
struct  ProcessEventsIn_t702674362 
{
public:
	// System.Int32 UICamera/ProcessEventsIn::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProcessEventsIn_t702674362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSEVENTSIN_T702674362_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef DIRECTION_T1681948056_H
#define DIRECTION_T1681948056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PropertyBinding/Direction
struct  Direction_t1681948056 
{
public:
	// System.Int32 PropertyBinding/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1681948056, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1681948056_H
#ifndef UPDATECONDITION_T3398770213_H
#define UPDATECONDITION_T3398770213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PropertyBinding/UpdateCondition
struct  UpdateCondition_t3398770213 
{
public:
	// System.Int32 PropertyBinding/UpdateCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateCondition_t3398770213, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECONDITION_T3398770213_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NGUITOOLS_T1206951095_H
#define NGUITOOLS_T1206951095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUITools
struct  NGUITools_t1206951095  : public RuntimeObject
{
public:

public:
};

struct NGUITools_t1206951095_StaticFields
{
public:
	// UnityEngine.AudioListener NGUITools::mListener
	AudioListener_t2734094699 * ___mListener_0;
	// UnityEngine.AudioSource NGUITools::audioSource
	AudioSource_t3935305588 * ___audioSource_1;
	// System.Boolean NGUITools::mLoaded
	bool ___mLoaded_2;
	// System.Single NGUITools::mGlobalVolume
	float ___mGlobalVolume_3;
	// System.Single NGUITools::mLastTimestamp
	float ___mLastTimestamp_4;
	// UnityEngine.AudioClip NGUITools::mLastClip
	AudioClip_t3680889665 * ___mLastClip_5;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> NGUITools::mTypeNames
	Dictionary_2_t4291797753 * ___mTypeNames_6;
	// UnityEngine.Vector3[] NGUITools::mSides
	Vector3U5BU5D_t1718750761* ___mSides_7;
	// UnityEngine.KeyCode[] NGUITools::keys
	KeyCodeU5BU5D_t2223234056* ___keys_8;
	// System.Collections.Generic.Dictionary`2<System.String,UIWidget> NGUITools::mWidgets
	Dictionary_2_t3323778224 * ___mWidgets_9;
	// UIPanel NGUITools::mRoot
	UIPanel_t1716472341 * ___mRoot_10;
	// UnityEngine.GameObject NGUITools::mGo
	GameObject_t1113636619 * ___mGo_11;
	// UnityEngine.ColorSpace NGUITools::mColorSpace
	int32_t ___mColorSpace_12;
	// System.Comparison`1<UIWidget> NGUITools::<>f__mg$cache0
	Comparison_1_t3313453104 * ___U3CU3Ef__mgU24cache0_13;
	// System.Comparison`1<UIPanel> NGUITools::<>f__mg$cache1
	Comparison_1_t1491403520 * ___U3CU3Ef__mgU24cache1_14;

public:
	inline static int32_t get_offset_of_mListener_0() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mListener_0)); }
	inline AudioListener_t2734094699 * get_mListener_0() const { return ___mListener_0; }
	inline AudioListener_t2734094699 ** get_address_of_mListener_0() { return &___mListener_0; }
	inline void set_mListener_0(AudioListener_t2734094699 * value)
	{
		___mListener_0 = value;
		Il2CppCodeGenWriteBarrier((&___mListener_0), value);
	}

	inline static int32_t get_offset_of_audioSource_1() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___audioSource_1)); }
	inline AudioSource_t3935305588 * get_audioSource_1() const { return ___audioSource_1; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_1() { return &___audioSource_1; }
	inline void set_audioSource_1(AudioSource_t3935305588 * value)
	{
		___audioSource_1 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_1), value);
	}

	inline static int32_t get_offset_of_mLoaded_2() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mLoaded_2)); }
	inline bool get_mLoaded_2() const { return ___mLoaded_2; }
	inline bool* get_address_of_mLoaded_2() { return &___mLoaded_2; }
	inline void set_mLoaded_2(bool value)
	{
		___mLoaded_2 = value;
	}

	inline static int32_t get_offset_of_mGlobalVolume_3() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mGlobalVolume_3)); }
	inline float get_mGlobalVolume_3() const { return ___mGlobalVolume_3; }
	inline float* get_address_of_mGlobalVolume_3() { return &___mGlobalVolume_3; }
	inline void set_mGlobalVolume_3(float value)
	{
		___mGlobalVolume_3 = value;
	}

	inline static int32_t get_offset_of_mLastTimestamp_4() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mLastTimestamp_4)); }
	inline float get_mLastTimestamp_4() const { return ___mLastTimestamp_4; }
	inline float* get_address_of_mLastTimestamp_4() { return &___mLastTimestamp_4; }
	inline void set_mLastTimestamp_4(float value)
	{
		___mLastTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_mLastClip_5() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mLastClip_5)); }
	inline AudioClip_t3680889665 * get_mLastClip_5() const { return ___mLastClip_5; }
	inline AudioClip_t3680889665 ** get_address_of_mLastClip_5() { return &___mLastClip_5; }
	inline void set_mLastClip_5(AudioClip_t3680889665 * value)
	{
		___mLastClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___mLastClip_5), value);
	}

	inline static int32_t get_offset_of_mTypeNames_6() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mTypeNames_6)); }
	inline Dictionary_2_t4291797753 * get_mTypeNames_6() const { return ___mTypeNames_6; }
	inline Dictionary_2_t4291797753 ** get_address_of_mTypeNames_6() { return &___mTypeNames_6; }
	inline void set_mTypeNames_6(Dictionary_2_t4291797753 * value)
	{
		___mTypeNames_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTypeNames_6), value);
	}

	inline static int32_t get_offset_of_mSides_7() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mSides_7)); }
	inline Vector3U5BU5D_t1718750761* get_mSides_7() const { return ___mSides_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mSides_7() { return &___mSides_7; }
	inline void set_mSides_7(Vector3U5BU5D_t1718750761* value)
	{
		___mSides_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSides_7), value);
	}

	inline static int32_t get_offset_of_keys_8() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___keys_8)); }
	inline KeyCodeU5BU5D_t2223234056* get_keys_8() const { return ___keys_8; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keys_8() { return &___keys_8; }
	inline void set_keys_8(KeyCodeU5BU5D_t2223234056* value)
	{
		___keys_8 = value;
		Il2CppCodeGenWriteBarrier((&___keys_8), value);
	}

	inline static int32_t get_offset_of_mWidgets_9() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mWidgets_9)); }
	inline Dictionary_2_t3323778224 * get_mWidgets_9() const { return ___mWidgets_9; }
	inline Dictionary_2_t3323778224 ** get_address_of_mWidgets_9() { return &___mWidgets_9; }
	inline void set_mWidgets_9(Dictionary_2_t3323778224 * value)
	{
		___mWidgets_9 = value;
		Il2CppCodeGenWriteBarrier((&___mWidgets_9), value);
	}

	inline static int32_t get_offset_of_mRoot_10() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mRoot_10)); }
	inline UIPanel_t1716472341 * get_mRoot_10() const { return ___mRoot_10; }
	inline UIPanel_t1716472341 ** get_address_of_mRoot_10() { return &___mRoot_10; }
	inline void set_mRoot_10(UIPanel_t1716472341 * value)
	{
		___mRoot_10 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_10), value);
	}

	inline static int32_t get_offset_of_mGo_11() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mGo_11)); }
	inline GameObject_t1113636619 * get_mGo_11() const { return ___mGo_11; }
	inline GameObject_t1113636619 ** get_address_of_mGo_11() { return &___mGo_11; }
	inline void set_mGo_11(GameObject_t1113636619 * value)
	{
		___mGo_11 = value;
		Il2CppCodeGenWriteBarrier((&___mGo_11), value);
	}

	inline static int32_t get_offset_of_mColorSpace_12() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mColorSpace_12)); }
	inline int32_t get_mColorSpace_12() const { return ___mColorSpace_12; }
	inline int32_t* get_address_of_mColorSpace_12() { return &___mColorSpace_12; }
	inline void set_mColorSpace_12(int32_t value)
	{
		___mColorSpace_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t3313453104 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t3313453104 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t3313453104 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_14() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___U3CU3Ef__mgU24cache1_14)); }
	inline Comparison_1_t1491403520 * get_U3CU3Ef__mgU24cache1_14() const { return ___U3CU3Ef__mgU24cache1_14; }
	inline Comparison_1_t1491403520 ** get_address_of_U3CU3Ef__mgU24cache1_14() { return &___U3CU3Ef__mgU24cache1_14; }
	inline void set_U3CU3Ef__mgU24cache1_14(Comparison_1_t1491403520 * value)
	{
		___U3CU3Ef__mgU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NGUITOOLS_T1206951095_H
#ifndef MOUSEORTOUCH_T3052596533_H
#define MOUSEORTOUCH_T3052596533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/MouseOrTouch
struct  MouseOrTouch_t3052596533  : public RuntimeObject
{
public:
	// UnityEngine.KeyCode UICamera/MouseOrTouch::key
	int32_t ___key_0;
	// UnityEngine.Vector2 UICamera/MouseOrTouch::pos
	Vector2_t2156229523  ___pos_1;
	// UnityEngine.Vector2 UICamera/MouseOrTouch::lastPos
	Vector2_t2156229523  ___lastPos_2;
	// UnityEngine.Vector2 UICamera/MouseOrTouch::delta
	Vector2_t2156229523  ___delta_3;
	// UnityEngine.Vector2 UICamera/MouseOrTouch::totalDelta
	Vector2_t2156229523  ___totalDelta_4;
	// UnityEngine.Camera UICamera/MouseOrTouch::pressedCam
	Camera_t4157153871 * ___pressedCam_5;
	// UnityEngine.GameObject UICamera/MouseOrTouch::last
	GameObject_t1113636619 * ___last_6;
	// UnityEngine.GameObject UICamera/MouseOrTouch::current
	GameObject_t1113636619 * ___current_7;
	// UnityEngine.GameObject UICamera/MouseOrTouch::pressed
	GameObject_t1113636619 * ___pressed_8;
	// UnityEngine.GameObject UICamera/MouseOrTouch::dragged
	GameObject_t1113636619 * ___dragged_9;
	// System.Single UICamera/MouseOrTouch::pressTime
	float ___pressTime_10;
	// System.Single UICamera/MouseOrTouch::clickTime
	float ___clickTime_11;
	// UICamera/ClickNotification UICamera/MouseOrTouch::clickNotification
	int32_t ___clickNotification_12;
	// System.Boolean UICamera/MouseOrTouch::touchBegan
	bool ___touchBegan_13;
	// System.Boolean UICamera/MouseOrTouch::pressStarted
	bool ___pressStarted_14;
	// System.Boolean UICamera/MouseOrTouch::dragStarted
	bool ___dragStarted_15;
	// System.Int32 UICamera/MouseOrTouch::ignoreDelta
	int32_t ___ignoreDelta_16;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___pos_1)); }
	inline Vector2_t2156229523  get_pos_1() const { return ___pos_1; }
	inline Vector2_t2156229523 * get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(Vector2_t2156229523  value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_lastPos_2() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___lastPos_2)); }
	inline Vector2_t2156229523  get_lastPos_2() const { return ___lastPos_2; }
	inline Vector2_t2156229523 * get_address_of_lastPos_2() { return &___lastPos_2; }
	inline void set_lastPos_2(Vector2_t2156229523  value)
	{
		___lastPos_2 = value;
	}

	inline static int32_t get_offset_of_delta_3() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___delta_3)); }
	inline Vector2_t2156229523  get_delta_3() const { return ___delta_3; }
	inline Vector2_t2156229523 * get_address_of_delta_3() { return &___delta_3; }
	inline void set_delta_3(Vector2_t2156229523  value)
	{
		___delta_3 = value;
	}

	inline static int32_t get_offset_of_totalDelta_4() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___totalDelta_4)); }
	inline Vector2_t2156229523  get_totalDelta_4() const { return ___totalDelta_4; }
	inline Vector2_t2156229523 * get_address_of_totalDelta_4() { return &___totalDelta_4; }
	inline void set_totalDelta_4(Vector2_t2156229523  value)
	{
		___totalDelta_4 = value;
	}

	inline static int32_t get_offset_of_pressedCam_5() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___pressedCam_5)); }
	inline Camera_t4157153871 * get_pressedCam_5() const { return ___pressedCam_5; }
	inline Camera_t4157153871 ** get_address_of_pressedCam_5() { return &___pressedCam_5; }
	inline void set_pressedCam_5(Camera_t4157153871 * value)
	{
		___pressedCam_5 = value;
		Il2CppCodeGenWriteBarrier((&___pressedCam_5), value);
	}

	inline static int32_t get_offset_of_last_6() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___last_6)); }
	inline GameObject_t1113636619 * get_last_6() const { return ___last_6; }
	inline GameObject_t1113636619 ** get_address_of_last_6() { return &___last_6; }
	inline void set_last_6(GameObject_t1113636619 * value)
	{
		___last_6 = value;
		Il2CppCodeGenWriteBarrier((&___last_6), value);
	}

	inline static int32_t get_offset_of_current_7() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___current_7)); }
	inline GameObject_t1113636619 * get_current_7() const { return ___current_7; }
	inline GameObject_t1113636619 ** get_address_of_current_7() { return &___current_7; }
	inline void set_current_7(GameObject_t1113636619 * value)
	{
		___current_7 = value;
		Il2CppCodeGenWriteBarrier((&___current_7), value);
	}

	inline static int32_t get_offset_of_pressed_8() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___pressed_8)); }
	inline GameObject_t1113636619 * get_pressed_8() const { return ___pressed_8; }
	inline GameObject_t1113636619 ** get_address_of_pressed_8() { return &___pressed_8; }
	inline void set_pressed_8(GameObject_t1113636619 * value)
	{
		___pressed_8 = value;
		Il2CppCodeGenWriteBarrier((&___pressed_8), value);
	}

	inline static int32_t get_offset_of_dragged_9() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___dragged_9)); }
	inline GameObject_t1113636619 * get_dragged_9() const { return ___dragged_9; }
	inline GameObject_t1113636619 ** get_address_of_dragged_9() { return &___dragged_9; }
	inline void set_dragged_9(GameObject_t1113636619 * value)
	{
		___dragged_9 = value;
		Il2CppCodeGenWriteBarrier((&___dragged_9), value);
	}

	inline static int32_t get_offset_of_pressTime_10() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___pressTime_10)); }
	inline float get_pressTime_10() const { return ___pressTime_10; }
	inline float* get_address_of_pressTime_10() { return &___pressTime_10; }
	inline void set_pressTime_10(float value)
	{
		___pressTime_10 = value;
	}

	inline static int32_t get_offset_of_clickTime_11() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___clickTime_11)); }
	inline float get_clickTime_11() const { return ___clickTime_11; }
	inline float* get_address_of_clickTime_11() { return &___clickTime_11; }
	inline void set_clickTime_11(float value)
	{
		___clickTime_11 = value;
	}

	inline static int32_t get_offset_of_clickNotification_12() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___clickNotification_12)); }
	inline int32_t get_clickNotification_12() const { return ___clickNotification_12; }
	inline int32_t* get_address_of_clickNotification_12() { return &___clickNotification_12; }
	inline void set_clickNotification_12(int32_t value)
	{
		___clickNotification_12 = value;
	}

	inline static int32_t get_offset_of_touchBegan_13() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___touchBegan_13)); }
	inline bool get_touchBegan_13() const { return ___touchBegan_13; }
	inline bool* get_address_of_touchBegan_13() { return &___touchBegan_13; }
	inline void set_touchBegan_13(bool value)
	{
		___touchBegan_13 = value;
	}

	inline static int32_t get_offset_of_pressStarted_14() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___pressStarted_14)); }
	inline bool get_pressStarted_14() const { return ___pressStarted_14; }
	inline bool* get_address_of_pressStarted_14() { return &___pressStarted_14; }
	inline void set_pressStarted_14(bool value)
	{
		___pressStarted_14 = value;
	}

	inline static int32_t get_offset_of_dragStarted_15() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___dragStarted_15)); }
	inline bool get_dragStarted_15() const { return ___dragStarted_15; }
	inline bool* get_address_of_dragStarted_15() { return &___dragStarted_15; }
	inline void set_dragStarted_15(bool value)
	{
		___dragStarted_15 = value;
	}

	inline static int32_t get_offset_of_ignoreDelta_16() { return static_cast<int32_t>(offsetof(MouseOrTouch_t3052596533, ___ignoreDelta_16)); }
	inline int32_t get_ignoreDelta_16() const { return ___ignoreDelta_16; }
	inline int32_t* get_address_of_ignoreDelta_16() { return &___ignoreDelta_16; }
	inline void set_ignoreDelta_16(int32_t value)
	{
		___ignoreDelta_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEORTOUCH_T3052596533_H
#ifndef DEPTHENTRY_T628749918_H
#define DEPTHENTRY_T628749918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/DepthEntry
struct  DepthEntry_t628749918 
{
public:
	// System.Int32 UICamera/DepthEntry::depth
	int32_t ___depth_0;
	// UnityEngine.RaycastHit UICamera/DepthEntry::hit
	RaycastHit_t1056001966  ___hit_1;
	// UnityEngine.Vector3 UICamera/DepthEntry::point
	Vector3_t3722313464  ___point_2;
	// UnityEngine.GameObject UICamera/DepthEntry::go
	GameObject_t1113636619 * ___go_3;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_hit_1() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___hit_1)); }
	inline RaycastHit_t1056001966  get_hit_1() const { return ___hit_1; }
	inline RaycastHit_t1056001966 * get_address_of_hit_1() { return &___hit_1; }
	inline void set_hit_1(RaycastHit_t1056001966  value)
	{
		___hit_1 = value;
	}

	inline static int32_t get_offset_of_point_2() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___point_2)); }
	inline Vector3_t3722313464  get_point_2() const { return ___point_2; }
	inline Vector3_t3722313464 * get_address_of_point_2() { return &___point_2; }
	inline void set_point_2(Vector3_t3722313464  value)
	{
		___point_2 = value;
	}

	inline static int32_t get_offset_of_go_3() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___go_3)); }
	inline GameObject_t1113636619 * get_go_3() const { return ___go_3; }
	inline GameObject_t1113636619 ** get_address_of_go_3() { return &___go_3; }
	inline void set_go_3(GameObject_t1113636619 * value)
	{
		___go_3 = value;
		Il2CppCodeGenWriteBarrier((&___go_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UICamera/DepthEntry
struct DepthEntry_t628749918_marshaled_pinvoke
{
	int32_t ___depth_0;
	RaycastHit_t1056001966_marshaled_pinvoke ___hit_1;
	Vector3_t3722313464  ___point_2;
	GameObject_t1113636619 * ___go_3;
};
// Native definition for COM marshalling of UICamera/DepthEntry
struct DepthEntry_t628749918_marshaled_com
{
	int32_t ___depth_0;
	RaycastHit_t1056001966_marshaled_com ___hit_1;
	Vector3_t3722313464  ___point_2;
	GameObject_t1113636619 * ___go_3;
};
#endif // DEPTHENTRY_T628749918_H
#ifndef CHARACTERINFO_T1228754872_H
#define CHARACTERINFO_T1228754872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CharacterInfo
struct  CharacterInfo_t1228754872 
{
public:
	// System.Int32 UnityEngine.CharacterInfo::index
	int32_t ___index_0;
	// UnityEngine.Rect UnityEngine.CharacterInfo::uv
	Rect_t2360479859  ___uv_1;
	// UnityEngine.Rect UnityEngine.CharacterInfo::vert
	Rect_t2360479859  ___vert_2;
	// System.Single UnityEngine.CharacterInfo::width
	float ___width_3;
	// System.Int32 UnityEngine.CharacterInfo::size
	int32_t ___size_4;
	// UnityEngine.FontStyle UnityEngine.CharacterInfo::style
	int32_t ___style_5;
	// System.Boolean UnityEngine.CharacterInfo::flipped
	bool ___flipped_6;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CharacterInfo_t1228754872, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(CharacterInfo_t1228754872, ___uv_1)); }
	inline Rect_t2360479859  get_uv_1() const { return ___uv_1; }
	inline Rect_t2360479859 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Rect_t2360479859  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_vert_2() { return static_cast<int32_t>(offsetof(CharacterInfo_t1228754872, ___vert_2)); }
	inline Rect_t2360479859  get_vert_2() const { return ___vert_2; }
	inline Rect_t2360479859 * get_address_of_vert_2() { return &___vert_2; }
	inline void set_vert_2(Rect_t2360479859  value)
	{
		___vert_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(CharacterInfo_t1228754872, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(CharacterInfo_t1228754872, ___size_4)); }
	inline int32_t get_size_4() const { return ___size_4; }
	inline int32_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(int32_t value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_style_5() { return static_cast<int32_t>(offsetof(CharacterInfo_t1228754872, ___style_5)); }
	inline int32_t get_style_5() const { return ___style_5; }
	inline int32_t* get_address_of_style_5() { return &___style_5; }
	inline void set_style_5(int32_t value)
	{
		___style_5 = value;
	}

	inline static int32_t get_offset_of_flipped_6() { return static_cast<int32_t>(offsetof(CharacterInfo_t1228754872, ___flipped_6)); }
	inline bool get_flipped_6() const { return ___flipped_6; }
	inline bool* get_address_of_flipped_6() { return &___flipped_6; }
	inline void set_flipped_6(bool value)
	{
		___flipped_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.CharacterInfo
struct CharacterInfo_t1228754872_marshaled_pinvoke
{
	int32_t ___index_0;
	Rect_t2360479859  ___uv_1;
	Rect_t2360479859  ___vert_2;
	float ___width_3;
	int32_t ___size_4;
	int32_t ___style_5;
	int32_t ___flipped_6;
};
// Native definition for COM marshalling of UnityEngine.CharacterInfo
struct CharacterInfo_t1228754872_marshaled_com
{
	int32_t ___index_0;
	Rect_t2360479859  ___uv_1;
	Rect_t2360479859  ___vert_2;
	float ___width_3;
	int32_t ___size_4;
	int32_t ___style_5;
	int32_t ___flipped_6;
};
#endif // CHARACTERINFO_T1228754872_H
#ifndef GETMOUSEDELEGATE_T662790529_H
#define GETMOUSEDELEGATE_T662790529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/GetMouseDelegate
struct  GetMouseDelegate_t662790529  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMOUSEDELEGATE_T662790529_H
#ifndef GETAXISFUNC_T2592608932_H
#define GETAXISFUNC_T2592608932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/GetAxisFunc
struct  GetAxisFunc_t2592608932  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETAXISFUNC_T2592608932_H
#ifndef GETKEYSTATEFUNC_T2810275146_H
#define GETKEYSTATEFUNC_T2810275146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/GetKeyStateFunc
struct  GetKeyStateFunc_t2810275146  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETKEYSTATEFUNC_T2810275146_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GETANYKEYFUNC_T1761480072_H
#define GETANYKEYFUNC_T1761480072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/GetAnyKeyFunc
struct  GetAnyKeyFunc_t1761480072  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANYKEYFUNC_T1761480072_H
#ifndef ONINITIALIZEITEM_T992046894_H
#define ONINITIALIZEITEM_T992046894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWrapContent/OnInitializeItem
struct  OnInitializeItem_t992046894  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINITIALIZEITEM_T992046894_H
#ifndef ONCREATEDRAWCALL_T609469653_H
#define ONCREATEDRAWCALL_T609469653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/OnCreateDrawCall
struct  OnCreateDrawCall_t609469653  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCREATEDRAWCALL_T609469653_H
#ifndef OBJECTDELEGATE_T2025096746_H
#define OBJECTDELEGATE_T2025096746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/ObjectDelegate
struct  ObjectDelegate_t2025096746  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDELEGATE_T2025096746_H
#ifndef ONDIMENSIONSCHANGED_T3101921181_H
#define ONDIMENSIONSCHANGED_T3101921181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/OnDimensionsChanged
struct  OnDimensionsChanged_t3101921181  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDIMENSIONSCHANGED_T3101921181_H
#ifndef ONPOSTFILLCALLBACK_T2835645043_H
#define ONPOSTFILLCALLBACK_T2835645043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/OnPostFillCallback
struct  OnPostFillCallback_t2835645043  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPOSTFILLCALLBACK_T2835645043_H
#ifndef HITCHECK_T2300079615_H
#define HITCHECK_T2300079615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/HitCheck
struct  HitCheck_t2300079615  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITCHECK_T2300079615_H
#ifndef VECTORDELEGATE_T1966661092_H
#define VECTORDELEGATE_T1966661092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/VectorDelegate
struct  VectorDelegate_t1966661092  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORDELEGATE_T1966661092_H
#ifndef FLOATDELEGATE_T1747458064_H
#define FLOATDELEGATE_T1747458064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/FloatDelegate
struct  FloatDelegate_t1747458064  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATDELEGATE_T1747458064_H
#ifndef BOOLDELEGATE_T3089012064_H
#define BOOLDELEGATE_T3089012064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/BoolDelegate
struct  BoolDelegate_t3089012064  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLDELEGATE_T3089012064_H
#ifndef ONFINISHED_T3364492952_H
#define ONFINISHED_T3364492952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPosition/OnFinished
struct  OnFinished_t3364492952  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINISHED_T3364492952_H
#ifndef VOIDDELEGATE_T3914127870_H
#define VOIDDELEGATE_T3914127870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/VoidDelegate
struct  VoidDelegate_t3914127870  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOIDDELEGATE_T3914127870_H
#ifndef KEYCODEDELEGATE_T675056699_H
#define KEYCODEDELEGATE_T675056699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener/KeyCodeDelegate
struct  KeyCodeDelegate_t675056699  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODEDELEGATE_T675056699_H
#ifndef ONRENDERCALLBACK_T133425655_H
#define ONRENDERCALLBACK_T133425655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/OnRenderCallback
struct  OnRenderCallback_t133425655  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRENDERCALLBACK_T133425655_H
#ifndef ONCUSTOMWRITE_T2800546165_H
#define ONCUSTOMWRITE_T2800546165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGeometry/OnCustomWrite
struct  OnCustomWrite_t2800546165  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCUSTOMWRITE_T2800546165_H
#ifndef ONFINISHED_T3778785451_H
#define ONFINISHED_T3778785451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPanel/OnFinished
struct  OnFinished_t3778785451  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINISHED_T3778785451_H
#ifndef NGUITEXT_T3089182085_H
#define NGUITEXT_T3089182085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText
struct  NGUIText_t3089182085  : public RuntimeObject
{
public:

public:
};

struct NGUIText_t3089182085_StaticFields
{
public:
	// UIFont NGUIText::bitmapFont
	UIFont_t2766063701 * ___bitmapFont_0;
	// UnityEngine.Font NGUIText::dynamicFont
	Font_t1956802104 * ___dynamicFont_1;
	// NGUIText/GlyphInfo NGUIText::glyph
	GlyphInfo_t1020792323 * ___glyph_2;
	// System.Int32 NGUIText::fontSize
	int32_t ___fontSize_3;
	// System.Single NGUIText::fontScale
	float ___fontScale_4;
	// System.Single NGUIText::pixelDensity
	float ___pixelDensity_5;
	// UnityEngine.FontStyle NGUIText::fontStyle
	int32_t ___fontStyle_6;
	// NGUIText/Alignment NGUIText::alignment
	int32_t ___alignment_7;
	// UnityEngine.Color NGUIText::tint
	Color_t2555686324  ___tint_8;
	// System.Int32 NGUIText::rectWidth
	int32_t ___rectWidth_9;
	// System.Int32 NGUIText::rectHeight
	int32_t ___rectHeight_10;
	// System.Int32 NGUIText::regionWidth
	int32_t ___regionWidth_11;
	// System.Int32 NGUIText::regionHeight
	int32_t ___regionHeight_12;
	// System.Int32 NGUIText::maxLines
	int32_t ___maxLines_13;
	// System.Boolean NGUIText::gradient
	bool ___gradient_14;
	// UnityEngine.Color NGUIText::gradientBottom
	Color_t2555686324  ___gradientBottom_15;
	// UnityEngine.Color NGUIText::gradientTop
	Color_t2555686324  ___gradientTop_16;
	// System.Boolean NGUIText::encoding
	bool ___encoding_17;
	// System.Single NGUIText::spacingX
	float ___spacingX_18;
	// System.Single NGUIText::spacingY
	float ___spacingY_19;
	// System.Boolean NGUIText::premultiply
	bool ___premultiply_20;
	// NGUIText/SymbolStyle NGUIText::symbolStyle
	int32_t ___symbolStyle_21;
	// System.Int32 NGUIText::finalSize
	int32_t ___finalSize_22;
	// System.Single NGUIText::finalSpacingX
	float ___finalSpacingX_23;
	// System.Single NGUIText::finalLineHeight
	float ___finalLineHeight_24;
	// System.Single NGUIText::baseline
	float ___baseline_25;
	// System.Boolean NGUIText::useSymbols
	bool ___useSymbols_26;
	// UnityEngine.Color NGUIText::mInvisible
	Color_t2555686324  ___mInvisible_27;
	// BetterList`1<UnityEngine.Color> NGUIText::mColors
	BetterList_1_t1710706642 * ___mColors_28;
	// System.Single NGUIText::mAlpha
	float ___mAlpha_29;
	// UnityEngine.CharacterInfo NGUIText::mTempChar
	CharacterInfo_t1228754872  ___mTempChar_30;
	// BetterList`1<System.Single> NGUIText::mSizes
	BetterList_1_t552287092 * ___mSizes_31;
	// UnityEngine.Color NGUIText::s_c0
	Color_t2555686324  ___s_c0_32;
	// UnityEngine.Color NGUIText::s_c1
	Color_t2555686324  ___s_c1_33;
	// System.Single[] NGUIText::mBoldOffset
	SingleU5BU5D_t1444911251* ___mBoldOffset_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> NGUIText::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> NGUIText::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_37;

public:
	inline static int32_t get_offset_of_bitmapFont_0() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___bitmapFont_0)); }
	inline UIFont_t2766063701 * get_bitmapFont_0() const { return ___bitmapFont_0; }
	inline UIFont_t2766063701 ** get_address_of_bitmapFont_0() { return &___bitmapFont_0; }
	inline void set_bitmapFont_0(UIFont_t2766063701 * value)
	{
		___bitmapFont_0 = value;
		Il2CppCodeGenWriteBarrier((&___bitmapFont_0), value);
	}

	inline static int32_t get_offset_of_dynamicFont_1() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___dynamicFont_1)); }
	inline Font_t1956802104 * get_dynamicFont_1() const { return ___dynamicFont_1; }
	inline Font_t1956802104 ** get_address_of_dynamicFont_1() { return &___dynamicFont_1; }
	inline void set_dynamicFont_1(Font_t1956802104 * value)
	{
		___dynamicFont_1 = value;
		Il2CppCodeGenWriteBarrier((&___dynamicFont_1), value);
	}

	inline static int32_t get_offset_of_glyph_2() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___glyph_2)); }
	inline GlyphInfo_t1020792323 * get_glyph_2() const { return ___glyph_2; }
	inline GlyphInfo_t1020792323 ** get_address_of_glyph_2() { return &___glyph_2; }
	inline void set_glyph_2(GlyphInfo_t1020792323 * value)
	{
		___glyph_2 = value;
		Il2CppCodeGenWriteBarrier((&___glyph_2), value);
	}

	inline static int32_t get_offset_of_fontSize_3() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___fontSize_3)); }
	inline int32_t get_fontSize_3() const { return ___fontSize_3; }
	inline int32_t* get_address_of_fontSize_3() { return &___fontSize_3; }
	inline void set_fontSize_3(int32_t value)
	{
		___fontSize_3 = value;
	}

	inline static int32_t get_offset_of_fontScale_4() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___fontScale_4)); }
	inline float get_fontScale_4() const { return ___fontScale_4; }
	inline float* get_address_of_fontScale_4() { return &___fontScale_4; }
	inline void set_fontScale_4(float value)
	{
		___fontScale_4 = value;
	}

	inline static int32_t get_offset_of_pixelDensity_5() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___pixelDensity_5)); }
	inline float get_pixelDensity_5() const { return ___pixelDensity_5; }
	inline float* get_address_of_pixelDensity_5() { return &___pixelDensity_5; }
	inline void set_pixelDensity_5(float value)
	{
		___pixelDensity_5 = value;
	}

	inline static int32_t get_offset_of_fontStyle_6() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___fontStyle_6)); }
	inline int32_t get_fontStyle_6() const { return ___fontStyle_6; }
	inline int32_t* get_address_of_fontStyle_6() { return &___fontStyle_6; }
	inline void set_fontStyle_6(int32_t value)
	{
		___fontStyle_6 = value;
	}

	inline static int32_t get_offset_of_alignment_7() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___alignment_7)); }
	inline int32_t get_alignment_7() const { return ___alignment_7; }
	inline int32_t* get_address_of_alignment_7() { return &___alignment_7; }
	inline void set_alignment_7(int32_t value)
	{
		___alignment_7 = value;
	}

	inline static int32_t get_offset_of_tint_8() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___tint_8)); }
	inline Color_t2555686324  get_tint_8() const { return ___tint_8; }
	inline Color_t2555686324 * get_address_of_tint_8() { return &___tint_8; }
	inline void set_tint_8(Color_t2555686324  value)
	{
		___tint_8 = value;
	}

	inline static int32_t get_offset_of_rectWidth_9() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___rectWidth_9)); }
	inline int32_t get_rectWidth_9() const { return ___rectWidth_9; }
	inline int32_t* get_address_of_rectWidth_9() { return &___rectWidth_9; }
	inline void set_rectWidth_9(int32_t value)
	{
		___rectWidth_9 = value;
	}

	inline static int32_t get_offset_of_rectHeight_10() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___rectHeight_10)); }
	inline int32_t get_rectHeight_10() const { return ___rectHeight_10; }
	inline int32_t* get_address_of_rectHeight_10() { return &___rectHeight_10; }
	inline void set_rectHeight_10(int32_t value)
	{
		___rectHeight_10 = value;
	}

	inline static int32_t get_offset_of_regionWidth_11() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___regionWidth_11)); }
	inline int32_t get_regionWidth_11() const { return ___regionWidth_11; }
	inline int32_t* get_address_of_regionWidth_11() { return &___regionWidth_11; }
	inline void set_regionWidth_11(int32_t value)
	{
		___regionWidth_11 = value;
	}

	inline static int32_t get_offset_of_regionHeight_12() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___regionHeight_12)); }
	inline int32_t get_regionHeight_12() const { return ___regionHeight_12; }
	inline int32_t* get_address_of_regionHeight_12() { return &___regionHeight_12; }
	inline void set_regionHeight_12(int32_t value)
	{
		___regionHeight_12 = value;
	}

	inline static int32_t get_offset_of_maxLines_13() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___maxLines_13)); }
	inline int32_t get_maxLines_13() const { return ___maxLines_13; }
	inline int32_t* get_address_of_maxLines_13() { return &___maxLines_13; }
	inline void set_maxLines_13(int32_t value)
	{
		___maxLines_13 = value;
	}

	inline static int32_t get_offset_of_gradient_14() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___gradient_14)); }
	inline bool get_gradient_14() const { return ___gradient_14; }
	inline bool* get_address_of_gradient_14() { return &___gradient_14; }
	inline void set_gradient_14(bool value)
	{
		___gradient_14 = value;
	}

	inline static int32_t get_offset_of_gradientBottom_15() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___gradientBottom_15)); }
	inline Color_t2555686324  get_gradientBottom_15() const { return ___gradientBottom_15; }
	inline Color_t2555686324 * get_address_of_gradientBottom_15() { return &___gradientBottom_15; }
	inline void set_gradientBottom_15(Color_t2555686324  value)
	{
		___gradientBottom_15 = value;
	}

	inline static int32_t get_offset_of_gradientTop_16() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___gradientTop_16)); }
	inline Color_t2555686324  get_gradientTop_16() const { return ___gradientTop_16; }
	inline Color_t2555686324 * get_address_of_gradientTop_16() { return &___gradientTop_16; }
	inline void set_gradientTop_16(Color_t2555686324  value)
	{
		___gradientTop_16 = value;
	}

	inline static int32_t get_offset_of_encoding_17() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___encoding_17)); }
	inline bool get_encoding_17() const { return ___encoding_17; }
	inline bool* get_address_of_encoding_17() { return &___encoding_17; }
	inline void set_encoding_17(bool value)
	{
		___encoding_17 = value;
	}

	inline static int32_t get_offset_of_spacingX_18() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___spacingX_18)); }
	inline float get_spacingX_18() const { return ___spacingX_18; }
	inline float* get_address_of_spacingX_18() { return &___spacingX_18; }
	inline void set_spacingX_18(float value)
	{
		___spacingX_18 = value;
	}

	inline static int32_t get_offset_of_spacingY_19() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___spacingY_19)); }
	inline float get_spacingY_19() const { return ___spacingY_19; }
	inline float* get_address_of_spacingY_19() { return &___spacingY_19; }
	inline void set_spacingY_19(float value)
	{
		___spacingY_19 = value;
	}

	inline static int32_t get_offset_of_premultiply_20() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___premultiply_20)); }
	inline bool get_premultiply_20() const { return ___premultiply_20; }
	inline bool* get_address_of_premultiply_20() { return &___premultiply_20; }
	inline void set_premultiply_20(bool value)
	{
		___premultiply_20 = value;
	}

	inline static int32_t get_offset_of_symbolStyle_21() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___symbolStyle_21)); }
	inline int32_t get_symbolStyle_21() const { return ___symbolStyle_21; }
	inline int32_t* get_address_of_symbolStyle_21() { return &___symbolStyle_21; }
	inline void set_symbolStyle_21(int32_t value)
	{
		___symbolStyle_21 = value;
	}

	inline static int32_t get_offset_of_finalSize_22() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___finalSize_22)); }
	inline int32_t get_finalSize_22() const { return ___finalSize_22; }
	inline int32_t* get_address_of_finalSize_22() { return &___finalSize_22; }
	inline void set_finalSize_22(int32_t value)
	{
		___finalSize_22 = value;
	}

	inline static int32_t get_offset_of_finalSpacingX_23() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___finalSpacingX_23)); }
	inline float get_finalSpacingX_23() const { return ___finalSpacingX_23; }
	inline float* get_address_of_finalSpacingX_23() { return &___finalSpacingX_23; }
	inline void set_finalSpacingX_23(float value)
	{
		___finalSpacingX_23 = value;
	}

	inline static int32_t get_offset_of_finalLineHeight_24() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___finalLineHeight_24)); }
	inline float get_finalLineHeight_24() const { return ___finalLineHeight_24; }
	inline float* get_address_of_finalLineHeight_24() { return &___finalLineHeight_24; }
	inline void set_finalLineHeight_24(float value)
	{
		___finalLineHeight_24 = value;
	}

	inline static int32_t get_offset_of_baseline_25() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___baseline_25)); }
	inline float get_baseline_25() const { return ___baseline_25; }
	inline float* get_address_of_baseline_25() { return &___baseline_25; }
	inline void set_baseline_25(float value)
	{
		___baseline_25 = value;
	}

	inline static int32_t get_offset_of_useSymbols_26() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___useSymbols_26)); }
	inline bool get_useSymbols_26() const { return ___useSymbols_26; }
	inline bool* get_address_of_useSymbols_26() { return &___useSymbols_26; }
	inline void set_useSymbols_26(bool value)
	{
		___useSymbols_26 = value;
	}

	inline static int32_t get_offset_of_mInvisible_27() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___mInvisible_27)); }
	inline Color_t2555686324  get_mInvisible_27() const { return ___mInvisible_27; }
	inline Color_t2555686324 * get_address_of_mInvisible_27() { return &___mInvisible_27; }
	inline void set_mInvisible_27(Color_t2555686324  value)
	{
		___mInvisible_27 = value;
	}

	inline static int32_t get_offset_of_mColors_28() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___mColors_28)); }
	inline BetterList_1_t1710706642 * get_mColors_28() const { return ___mColors_28; }
	inline BetterList_1_t1710706642 ** get_address_of_mColors_28() { return &___mColors_28; }
	inline void set_mColors_28(BetterList_1_t1710706642 * value)
	{
		___mColors_28 = value;
		Il2CppCodeGenWriteBarrier((&___mColors_28), value);
	}

	inline static int32_t get_offset_of_mAlpha_29() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___mAlpha_29)); }
	inline float get_mAlpha_29() const { return ___mAlpha_29; }
	inline float* get_address_of_mAlpha_29() { return &___mAlpha_29; }
	inline void set_mAlpha_29(float value)
	{
		___mAlpha_29 = value;
	}

	inline static int32_t get_offset_of_mTempChar_30() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___mTempChar_30)); }
	inline CharacterInfo_t1228754872  get_mTempChar_30() const { return ___mTempChar_30; }
	inline CharacterInfo_t1228754872 * get_address_of_mTempChar_30() { return &___mTempChar_30; }
	inline void set_mTempChar_30(CharacterInfo_t1228754872  value)
	{
		___mTempChar_30 = value;
	}

	inline static int32_t get_offset_of_mSizes_31() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___mSizes_31)); }
	inline BetterList_1_t552287092 * get_mSizes_31() const { return ___mSizes_31; }
	inline BetterList_1_t552287092 ** get_address_of_mSizes_31() { return &___mSizes_31; }
	inline void set_mSizes_31(BetterList_1_t552287092 * value)
	{
		___mSizes_31 = value;
		Il2CppCodeGenWriteBarrier((&___mSizes_31), value);
	}

	inline static int32_t get_offset_of_s_c0_32() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___s_c0_32)); }
	inline Color_t2555686324  get_s_c0_32() const { return ___s_c0_32; }
	inline Color_t2555686324 * get_address_of_s_c0_32() { return &___s_c0_32; }
	inline void set_s_c0_32(Color_t2555686324  value)
	{
		___s_c0_32 = value;
	}

	inline static int32_t get_offset_of_s_c1_33() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___s_c1_33)); }
	inline Color_t2555686324  get_s_c1_33() const { return ___s_c1_33; }
	inline Color_t2555686324 * get_address_of_s_c1_33() { return &___s_c1_33; }
	inline void set_s_c1_33(Color_t2555686324  value)
	{
		___s_c1_33 = value;
	}

	inline static int32_t get_offset_of_mBoldOffset_35() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___mBoldOffset_35)); }
	inline SingleU5BU5D_t1444911251* get_mBoldOffset_35() const { return ___mBoldOffset_35; }
	inline SingleU5BU5D_t1444911251** get_address_of_mBoldOffset_35() { return &___mBoldOffset_35; }
	inline void set_mBoldOffset_35(SingleU5BU5D_t1444911251* value)
	{
		___mBoldOffset_35 = value;
		Il2CppCodeGenWriteBarrier((&___mBoldOffset_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_36() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___U3CU3Ef__switchU24map0_36)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_36() const { return ___U3CU3Ef__switchU24map0_36; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_36() { return &___U3CU3Ef__switchU24map0_36; }
	inline void set_U3CU3Ef__switchU24map0_36(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_37() { return static_cast<int32_t>(offsetof(NGUIText_t3089182085_StaticFields, ___U3CU3Ef__switchU24map1_37)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_37() const { return ___U3CU3Ef__switchU24map1_37; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_37() { return &___U3CU3Ef__switchU24map1_37; }
	inline void set_U3CU3Ef__switchU24map1_37(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NGUITEXT_T3089182085_H
#ifndef ONLOCALIZENOTIFICATION_T3391620158_H
#define ONLOCALIZENOTIFICATION_T3391620158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization/OnLocalizeNotification
struct  OnLocalizeNotification_t3391620158  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONLOCALIZENOTIFICATION_T3391620158_H
#ifndef LOADFUNCTION_T2078002637_H
#define LOADFUNCTION_T2078002637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization/LoadFunction
struct  LoadFunction_t2078002637  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADFUNCTION_T2078002637_H
#ifndef CALLBACK_T3139336517_H
#define CALLBACK_T3139336517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventDelegate/Callback
struct  Callback_t3139336517  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACK_T3139336517_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIEVENTLISTENER_T1665237878_H
#define UIEVENTLISTENER_T1665237878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListener
struct  UIEventListener_t1665237878  : public MonoBehaviour_t3962482529
{
public:
	// System.Object UIEventListener::parameter
	RuntimeObject * ___parameter_2;
	// UIEventListener/VoidDelegate UIEventListener::onSubmit
	VoidDelegate_t3914127870 * ___onSubmit_3;
	// UIEventListener/VoidDelegate UIEventListener::onClick
	VoidDelegate_t3914127870 * ___onClick_4;
	// UIEventListener/VoidDelegate UIEventListener::onDoubleClick
	VoidDelegate_t3914127870 * ___onDoubleClick_5;
	// UIEventListener/BoolDelegate UIEventListener::onHover
	BoolDelegate_t3089012064 * ___onHover_6;
	// UIEventListener/BoolDelegate UIEventListener::onPress
	BoolDelegate_t3089012064 * ___onPress_7;
	// UIEventListener/BoolDelegate UIEventListener::onSelect
	BoolDelegate_t3089012064 * ___onSelect_8;
	// UIEventListener/FloatDelegate UIEventListener::onScroll
	FloatDelegate_t1747458064 * ___onScroll_9;
	// UIEventListener/VoidDelegate UIEventListener::onDragStart
	VoidDelegate_t3914127870 * ___onDragStart_10;
	// UIEventListener/VectorDelegate UIEventListener::onDrag
	VectorDelegate_t1966661092 * ___onDrag_11;
	// UIEventListener/VoidDelegate UIEventListener::onDragOver
	VoidDelegate_t3914127870 * ___onDragOver_12;
	// UIEventListener/VoidDelegate UIEventListener::onDragOut
	VoidDelegate_t3914127870 * ___onDragOut_13;
	// UIEventListener/VoidDelegate UIEventListener::onDragEnd
	VoidDelegate_t3914127870 * ___onDragEnd_14;
	// UIEventListener/ObjectDelegate UIEventListener::onDrop
	ObjectDelegate_t2025096746 * ___onDrop_15;
	// UIEventListener/KeyCodeDelegate UIEventListener::onKey
	KeyCodeDelegate_t675056699 * ___onKey_16;
	// UIEventListener/BoolDelegate UIEventListener::onTooltip
	BoolDelegate_t3089012064 * ___onTooltip_17;
	// System.Boolean UIEventListener::needsActiveCollider
	bool ___needsActiveCollider_18;

public:
	inline static int32_t get_offset_of_parameter_2() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___parameter_2)); }
	inline RuntimeObject * get_parameter_2() const { return ___parameter_2; }
	inline RuntimeObject ** get_address_of_parameter_2() { return &___parameter_2; }
	inline void set_parameter_2(RuntimeObject * value)
	{
		___parameter_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_2), value);
	}

	inline static int32_t get_offset_of_onSubmit_3() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onSubmit_3)); }
	inline VoidDelegate_t3914127870 * get_onSubmit_3() const { return ___onSubmit_3; }
	inline VoidDelegate_t3914127870 ** get_address_of_onSubmit_3() { return &___onSubmit_3; }
	inline void set_onSubmit_3(VoidDelegate_t3914127870 * value)
	{
		___onSubmit_3 = value;
		Il2CppCodeGenWriteBarrier((&___onSubmit_3), value);
	}

	inline static int32_t get_offset_of_onClick_4() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onClick_4)); }
	inline VoidDelegate_t3914127870 * get_onClick_4() const { return ___onClick_4; }
	inline VoidDelegate_t3914127870 ** get_address_of_onClick_4() { return &___onClick_4; }
	inline void set_onClick_4(VoidDelegate_t3914127870 * value)
	{
		___onClick_4 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_4), value);
	}

	inline static int32_t get_offset_of_onDoubleClick_5() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onDoubleClick_5)); }
	inline VoidDelegate_t3914127870 * get_onDoubleClick_5() const { return ___onDoubleClick_5; }
	inline VoidDelegate_t3914127870 ** get_address_of_onDoubleClick_5() { return &___onDoubleClick_5; }
	inline void set_onDoubleClick_5(VoidDelegate_t3914127870 * value)
	{
		___onDoubleClick_5 = value;
		Il2CppCodeGenWriteBarrier((&___onDoubleClick_5), value);
	}

	inline static int32_t get_offset_of_onHover_6() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onHover_6)); }
	inline BoolDelegate_t3089012064 * get_onHover_6() const { return ___onHover_6; }
	inline BoolDelegate_t3089012064 ** get_address_of_onHover_6() { return &___onHover_6; }
	inline void set_onHover_6(BoolDelegate_t3089012064 * value)
	{
		___onHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___onHover_6), value);
	}

	inline static int32_t get_offset_of_onPress_7() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onPress_7)); }
	inline BoolDelegate_t3089012064 * get_onPress_7() const { return ___onPress_7; }
	inline BoolDelegate_t3089012064 ** get_address_of_onPress_7() { return &___onPress_7; }
	inline void set_onPress_7(BoolDelegate_t3089012064 * value)
	{
		___onPress_7 = value;
		Il2CppCodeGenWriteBarrier((&___onPress_7), value);
	}

	inline static int32_t get_offset_of_onSelect_8() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onSelect_8)); }
	inline BoolDelegate_t3089012064 * get_onSelect_8() const { return ___onSelect_8; }
	inline BoolDelegate_t3089012064 ** get_address_of_onSelect_8() { return &___onSelect_8; }
	inline void set_onSelect_8(BoolDelegate_t3089012064 * value)
	{
		___onSelect_8 = value;
		Il2CppCodeGenWriteBarrier((&___onSelect_8), value);
	}

	inline static int32_t get_offset_of_onScroll_9() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onScroll_9)); }
	inline FloatDelegate_t1747458064 * get_onScroll_9() const { return ___onScroll_9; }
	inline FloatDelegate_t1747458064 ** get_address_of_onScroll_9() { return &___onScroll_9; }
	inline void set_onScroll_9(FloatDelegate_t1747458064 * value)
	{
		___onScroll_9 = value;
		Il2CppCodeGenWriteBarrier((&___onScroll_9), value);
	}

	inline static int32_t get_offset_of_onDragStart_10() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onDragStart_10)); }
	inline VoidDelegate_t3914127870 * get_onDragStart_10() const { return ___onDragStart_10; }
	inline VoidDelegate_t3914127870 ** get_address_of_onDragStart_10() { return &___onDragStart_10; }
	inline void set_onDragStart_10(VoidDelegate_t3914127870 * value)
	{
		___onDragStart_10 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStart_10), value);
	}

	inline static int32_t get_offset_of_onDrag_11() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onDrag_11)); }
	inline VectorDelegate_t1966661092 * get_onDrag_11() const { return ___onDrag_11; }
	inline VectorDelegate_t1966661092 ** get_address_of_onDrag_11() { return &___onDrag_11; }
	inline void set_onDrag_11(VectorDelegate_t1966661092 * value)
	{
		___onDrag_11 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_11), value);
	}

	inline static int32_t get_offset_of_onDragOver_12() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onDragOver_12)); }
	inline VoidDelegate_t3914127870 * get_onDragOver_12() const { return ___onDragOver_12; }
	inline VoidDelegate_t3914127870 ** get_address_of_onDragOver_12() { return &___onDragOver_12; }
	inline void set_onDragOver_12(VoidDelegate_t3914127870 * value)
	{
		___onDragOver_12 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOver_12), value);
	}

	inline static int32_t get_offset_of_onDragOut_13() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onDragOut_13)); }
	inline VoidDelegate_t3914127870 * get_onDragOut_13() const { return ___onDragOut_13; }
	inline VoidDelegate_t3914127870 ** get_address_of_onDragOut_13() { return &___onDragOut_13; }
	inline void set_onDragOut_13(VoidDelegate_t3914127870 * value)
	{
		___onDragOut_13 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOut_13), value);
	}

	inline static int32_t get_offset_of_onDragEnd_14() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onDragEnd_14)); }
	inline VoidDelegate_t3914127870 * get_onDragEnd_14() const { return ___onDragEnd_14; }
	inline VoidDelegate_t3914127870 ** get_address_of_onDragEnd_14() { return &___onDragEnd_14; }
	inline void set_onDragEnd_14(VoidDelegate_t3914127870 * value)
	{
		___onDragEnd_14 = value;
		Il2CppCodeGenWriteBarrier((&___onDragEnd_14), value);
	}

	inline static int32_t get_offset_of_onDrop_15() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onDrop_15)); }
	inline ObjectDelegate_t2025096746 * get_onDrop_15() const { return ___onDrop_15; }
	inline ObjectDelegate_t2025096746 ** get_address_of_onDrop_15() { return &___onDrop_15; }
	inline void set_onDrop_15(ObjectDelegate_t2025096746 * value)
	{
		___onDrop_15 = value;
		Il2CppCodeGenWriteBarrier((&___onDrop_15), value);
	}

	inline static int32_t get_offset_of_onKey_16() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onKey_16)); }
	inline KeyCodeDelegate_t675056699 * get_onKey_16() const { return ___onKey_16; }
	inline KeyCodeDelegate_t675056699 ** get_address_of_onKey_16() { return &___onKey_16; }
	inline void set_onKey_16(KeyCodeDelegate_t675056699 * value)
	{
		___onKey_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKey_16), value);
	}

	inline static int32_t get_offset_of_onTooltip_17() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___onTooltip_17)); }
	inline BoolDelegate_t3089012064 * get_onTooltip_17() const { return ___onTooltip_17; }
	inline BoolDelegate_t3089012064 ** get_address_of_onTooltip_17() { return &___onTooltip_17; }
	inline void set_onTooltip_17(BoolDelegate_t3089012064 * value)
	{
		___onTooltip_17 = value;
		Il2CppCodeGenWriteBarrier((&___onTooltip_17), value);
	}

	inline static int32_t get_offset_of_needsActiveCollider_18() { return static_cast<int32_t>(offsetof(UIEventListener_t1665237878, ___needsActiveCollider_18)); }
	inline bool get_needsActiveCollider_18() const { return ___needsActiveCollider_18; }
	inline bool* get_address_of_needsActiveCollider_18() { return &___needsActiveCollider_18; }
	inline void set_needsActiveCollider_18(bool value)
	{
		___needsActiveCollider_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTLISTENER_T1665237878_H
#ifndef SPRINGPANEL_T277350554_H
#define SPRINGPANEL_T277350554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPanel
struct  SpringPanel_t277350554  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 SpringPanel::target
	Vector3_t3722313464  ___target_3;
	// System.Single SpringPanel::strength
	float ___strength_4;
	// SpringPanel/OnFinished SpringPanel::onFinished
	OnFinished_t3778785451 * ___onFinished_5;
	// UIPanel SpringPanel::mPanel
	UIPanel_t1716472341 * ___mPanel_6;
	// UnityEngine.Transform SpringPanel::mTrans
	Transform_t3600365921 * ___mTrans_7;
	// UIScrollView SpringPanel::mDrag
	UIScrollView_t1973404950 * ___mDrag_8;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(SpringPanel_t277350554, ___target_3)); }
	inline Vector3_t3722313464  get_target_3() const { return ___target_3; }
	inline Vector3_t3722313464 * get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Vector3_t3722313464  value)
	{
		___target_3 = value;
	}

	inline static int32_t get_offset_of_strength_4() { return static_cast<int32_t>(offsetof(SpringPanel_t277350554, ___strength_4)); }
	inline float get_strength_4() const { return ___strength_4; }
	inline float* get_address_of_strength_4() { return &___strength_4; }
	inline void set_strength_4(float value)
	{
		___strength_4 = value;
	}

	inline static int32_t get_offset_of_onFinished_5() { return static_cast<int32_t>(offsetof(SpringPanel_t277350554, ___onFinished_5)); }
	inline OnFinished_t3778785451 * get_onFinished_5() const { return ___onFinished_5; }
	inline OnFinished_t3778785451 ** get_address_of_onFinished_5() { return &___onFinished_5; }
	inline void set_onFinished_5(OnFinished_t3778785451 * value)
	{
		___onFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_5), value);
	}

	inline static int32_t get_offset_of_mPanel_6() { return static_cast<int32_t>(offsetof(SpringPanel_t277350554, ___mPanel_6)); }
	inline UIPanel_t1716472341 * get_mPanel_6() const { return ___mPanel_6; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_6() { return &___mPanel_6; }
	inline void set_mPanel_6(UIPanel_t1716472341 * value)
	{
		___mPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_6), value);
	}

	inline static int32_t get_offset_of_mTrans_7() { return static_cast<int32_t>(offsetof(SpringPanel_t277350554, ___mTrans_7)); }
	inline Transform_t3600365921 * get_mTrans_7() const { return ___mTrans_7; }
	inline Transform_t3600365921 ** get_address_of_mTrans_7() { return &___mTrans_7; }
	inline void set_mTrans_7(Transform_t3600365921 * value)
	{
		___mTrans_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_7), value);
	}

	inline static int32_t get_offset_of_mDrag_8() { return static_cast<int32_t>(offsetof(SpringPanel_t277350554, ___mDrag_8)); }
	inline UIScrollView_t1973404950 * get_mDrag_8() const { return ___mDrag_8; }
	inline UIScrollView_t1973404950 ** get_address_of_mDrag_8() { return &___mDrag_8; }
	inline void set_mDrag_8(UIScrollView_t1973404950 * value)
	{
		___mDrag_8 = value;
		Il2CppCodeGenWriteBarrier((&___mDrag_8), value);
	}
};

struct SpringPanel_t277350554_StaticFields
{
public:
	// SpringPanel SpringPanel::current
	SpringPanel_t277350554 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(SpringPanel_t277350554_StaticFields, ___current_2)); }
	inline SpringPanel_t277350554 * get_current_2() const { return ___current_2; }
	inline SpringPanel_t277350554 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(SpringPanel_t277350554 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGPANEL_T277350554_H
#ifndef REALTIME_T4034823134_H
#define REALTIME_T4034823134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RealTime
struct  RealTime_t4034823134  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REALTIME_T4034823134_H
#ifndef PROPERTYBINDING_T828139262_H
#define PROPERTYBINDING_T828139262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PropertyBinding
struct  PropertyBinding_t828139262  : public MonoBehaviour_t3962482529
{
public:
	// PropertyReference PropertyBinding::source
	PropertyReference_t223937415 * ___source_2;
	// PropertyReference PropertyBinding::target
	PropertyReference_t223937415 * ___target_3;
	// PropertyBinding/Direction PropertyBinding::direction
	int32_t ___direction_4;
	// PropertyBinding/UpdateCondition PropertyBinding::update
	int32_t ___update_5;
	// System.Boolean PropertyBinding::editMode
	bool ___editMode_6;
	// System.Object PropertyBinding::mLastValue
	RuntimeObject * ___mLastValue_7;

public:
	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(PropertyBinding_t828139262, ___source_2)); }
	inline PropertyReference_t223937415 * get_source_2() const { return ___source_2; }
	inline PropertyReference_t223937415 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(PropertyReference_t223937415 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(PropertyBinding_t828139262, ___target_3)); }
	inline PropertyReference_t223937415 * get_target_3() const { return ___target_3; }
	inline PropertyReference_t223937415 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(PropertyReference_t223937415 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(PropertyBinding_t828139262, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_update_5() { return static_cast<int32_t>(offsetof(PropertyBinding_t828139262, ___update_5)); }
	inline int32_t get_update_5() const { return ___update_5; }
	inline int32_t* get_address_of_update_5() { return &___update_5; }
	inline void set_update_5(int32_t value)
	{
		___update_5 = value;
	}

	inline static int32_t get_offset_of_editMode_6() { return static_cast<int32_t>(offsetof(PropertyBinding_t828139262, ___editMode_6)); }
	inline bool get_editMode_6() const { return ___editMode_6; }
	inline bool* get_address_of_editMode_6() { return &___editMode_6; }
	inline void set_editMode_6(bool value)
	{
		___editMode_6 = value;
	}

	inline static int32_t get_offset_of_mLastValue_7() { return static_cast<int32_t>(offsetof(PropertyBinding_t828139262, ___mLastValue_7)); }
	inline RuntimeObject * get_mLastValue_7() const { return ___mLastValue_7; }
	inline RuntimeObject ** get_address_of_mLastValue_7() { return &___mLastValue_7; }
	inline void set_mLastValue_7(RuntimeObject * value)
	{
		___mLastValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___mLastValue_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYBINDING_T828139262_H
#ifndef NGUIDEBUG_T787955914_H
#define NGUIDEBUG_T787955914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIDebug
struct  NGUIDebug_t787955914  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct NGUIDebug_t787955914_StaticFields
{
public:
	// System.Boolean NGUIDebug::mRayDebug
	bool ___mRayDebug_2;
	// System.Collections.Generic.List`1<System.String> NGUIDebug::mLines
	List_1_t3319525431 * ___mLines_3;
	// NGUIDebug NGUIDebug::mInstance
	NGUIDebug_t787955914 * ___mInstance_4;

public:
	inline static int32_t get_offset_of_mRayDebug_2() { return static_cast<int32_t>(offsetof(NGUIDebug_t787955914_StaticFields, ___mRayDebug_2)); }
	inline bool get_mRayDebug_2() const { return ___mRayDebug_2; }
	inline bool* get_address_of_mRayDebug_2() { return &___mRayDebug_2; }
	inline void set_mRayDebug_2(bool value)
	{
		___mRayDebug_2 = value;
	}

	inline static int32_t get_offset_of_mLines_3() { return static_cast<int32_t>(offsetof(NGUIDebug_t787955914_StaticFields, ___mLines_3)); }
	inline List_1_t3319525431 * get_mLines_3() const { return ___mLines_3; }
	inline List_1_t3319525431 ** get_address_of_mLines_3() { return &___mLines_3; }
	inline void set_mLines_3(List_1_t3319525431 * value)
	{
		___mLines_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLines_3), value);
	}

	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(NGUIDebug_t787955914_StaticFields, ___mInstance_4)); }
	inline NGUIDebug_t787955914 * get_mInstance_4() const { return ___mInstance_4; }
	inline NGUIDebug_t787955914 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(NGUIDebug_t787955914 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NGUIDEBUG_T787955914_H
#ifndef ACTIVEANIMATION_T3475256642_H
#define ACTIVEANIMATION_T3475256642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveAnimation
struct  ActiveAnimation_t3475256642  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<EventDelegate> ActiveAnimation::onFinished
	List_1_t4210400802 * ___onFinished_3;
	// UnityEngine.GameObject ActiveAnimation::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_4;
	// System.String ActiveAnimation::callWhenFinished
	String_t* ___callWhenFinished_5;
	// UnityEngine.Animation ActiveAnimation::mAnim
	Animation_t3648466861 * ___mAnim_6;
	// AnimationOrTween.Direction ActiveAnimation::mLastDirection
	int32_t ___mLastDirection_7;
	// AnimationOrTween.Direction ActiveAnimation::mDisableDirection
	int32_t ___mDisableDirection_8;
	// System.Boolean ActiveAnimation::mNotify
	bool ___mNotify_9;
	// UnityEngine.Animator ActiveAnimation::mAnimator
	Animator_t434523843 * ___mAnimator_10;
	// System.String ActiveAnimation::mClip
	String_t* ___mClip_11;

public:
	inline static int32_t get_offset_of_onFinished_3() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___onFinished_3)); }
	inline List_1_t4210400802 * get_onFinished_3() const { return ___onFinished_3; }
	inline List_1_t4210400802 ** get_address_of_onFinished_3() { return &___onFinished_3; }
	inline void set_onFinished_3(List_1_t4210400802 * value)
	{
		___onFinished_3 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_3), value);
	}

	inline static int32_t get_offset_of_eventReceiver_4() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___eventReceiver_4)); }
	inline GameObject_t1113636619 * get_eventReceiver_4() const { return ___eventReceiver_4; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_4() { return &___eventReceiver_4; }
	inline void set_eventReceiver_4(GameObject_t1113636619 * value)
	{
		___eventReceiver_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_4), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_5() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___callWhenFinished_5)); }
	inline String_t* get_callWhenFinished_5() const { return ___callWhenFinished_5; }
	inline String_t** get_address_of_callWhenFinished_5() { return &___callWhenFinished_5; }
	inline void set_callWhenFinished_5(String_t* value)
	{
		___callWhenFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_5), value);
	}

	inline static int32_t get_offset_of_mAnim_6() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mAnim_6)); }
	inline Animation_t3648466861 * get_mAnim_6() const { return ___mAnim_6; }
	inline Animation_t3648466861 ** get_address_of_mAnim_6() { return &___mAnim_6; }
	inline void set_mAnim_6(Animation_t3648466861 * value)
	{
		___mAnim_6 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_6), value);
	}

	inline static int32_t get_offset_of_mLastDirection_7() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mLastDirection_7)); }
	inline int32_t get_mLastDirection_7() const { return ___mLastDirection_7; }
	inline int32_t* get_address_of_mLastDirection_7() { return &___mLastDirection_7; }
	inline void set_mLastDirection_7(int32_t value)
	{
		___mLastDirection_7 = value;
	}

	inline static int32_t get_offset_of_mDisableDirection_8() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mDisableDirection_8)); }
	inline int32_t get_mDisableDirection_8() const { return ___mDisableDirection_8; }
	inline int32_t* get_address_of_mDisableDirection_8() { return &___mDisableDirection_8; }
	inline void set_mDisableDirection_8(int32_t value)
	{
		___mDisableDirection_8 = value;
	}

	inline static int32_t get_offset_of_mNotify_9() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mNotify_9)); }
	inline bool get_mNotify_9() const { return ___mNotify_9; }
	inline bool* get_address_of_mNotify_9() { return &___mNotify_9; }
	inline void set_mNotify_9(bool value)
	{
		___mNotify_9 = value;
	}

	inline static int32_t get_offset_of_mAnimator_10() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mAnimator_10)); }
	inline Animator_t434523843 * get_mAnimator_10() const { return ___mAnimator_10; }
	inline Animator_t434523843 ** get_address_of_mAnimator_10() { return &___mAnimator_10; }
	inline void set_mAnimator_10(Animator_t434523843 * value)
	{
		___mAnimator_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAnimator_10), value);
	}

	inline static int32_t get_offset_of_mClip_11() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mClip_11)); }
	inline String_t* get_mClip_11() const { return ___mClip_11; }
	inline String_t** get_address_of_mClip_11() { return &___mClip_11; }
	inline void set_mClip_11(String_t* value)
	{
		___mClip_11 = value;
		Il2CppCodeGenWriteBarrier((&___mClip_11), value);
	}
};

struct ActiveAnimation_t3475256642_StaticFields
{
public:
	// ActiveAnimation ActiveAnimation::current
	ActiveAnimation_t3475256642 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642_StaticFields, ___current_2)); }
	inline ActiveAnimation_t3475256642 * get_current_2() const { return ___current_2; }
	inline ActiveAnimation_t3475256642 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(ActiveAnimation_t3475256642 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEANIMATION_T3475256642_H
#ifndef UIDRAWCALL_T1293405319_H
#define UIDRAWCALL_T1293405319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall
struct  UIDrawCall_t1293405319  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UIDrawCall::widgetCount
	int32_t ___widgetCount_4;
	// System.Int32 UIDrawCall::depthStart
	int32_t ___depthStart_5;
	// System.Int32 UIDrawCall::depthEnd
	int32_t ___depthEnd_6;
	// UIPanel UIDrawCall::manager
	UIPanel_t1716472341 * ___manager_7;
	// UIPanel UIDrawCall::panel
	UIPanel_t1716472341 * ___panel_8;
	// UnityEngine.Texture2D UIDrawCall::clipTexture
	Texture2D_t3840446185 * ___clipTexture_9;
	// System.Boolean UIDrawCall::alwaysOnScreen
	bool ___alwaysOnScreen_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIDrawCall::verts
	List_1_t899420910 * ___verts_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIDrawCall::norms
	List_1_t899420910 * ___norms_12;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UIDrawCall::tans
	List_1_t496136383 * ___tans_13;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UIDrawCall::uvs
	List_1_t3628304265 * ___uvs_14;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UIDrawCall::uv2
	List_1_t496136383 * ___uv2_15;
	// System.Collections.Generic.List`1<UnityEngine.Color> UIDrawCall::cols
	List_1_t4027761066 * ___cols_16;
	// UnityEngine.Material UIDrawCall::mMaterial
	Material_t340375123 * ___mMaterial_17;
	// UnityEngine.Texture UIDrawCall::mTexture
	Texture_t3661962703 * ___mTexture_18;
	// UnityEngine.Shader UIDrawCall::mShader
	Shader_t4151988712 * ___mShader_19;
	// System.Int32 UIDrawCall::mClipCount
	int32_t ___mClipCount_20;
	// UnityEngine.Transform UIDrawCall::mTrans
	Transform_t3600365921 * ___mTrans_21;
	// UnityEngine.Mesh UIDrawCall::mMesh
	Mesh_t3648964284 * ___mMesh_22;
	// UnityEngine.MeshFilter UIDrawCall::mFilter
	MeshFilter_t3523625662 * ___mFilter_23;
	// UnityEngine.MeshRenderer UIDrawCall::mRenderer
	MeshRenderer_t587009260 * ___mRenderer_24;
	// UnityEngine.Material UIDrawCall::mDynamicMat
	Material_t340375123 * ___mDynamicMat_25;
	// System.Int32[] UIDrawCall::mIndices
	Int32U5BU5D_t385246372* ___mIndices_26;
	// UIDrawCall/ShadowMode UIDrawCall::mShadowMode
	int32_t ___mShadowMode_27;
	// System.Boolean UIDrawCall::mRebuildMat
	bool ___mRebuildMat_28;
	// System.Boolean UIDrawCall::mLegacyShader
	bool ___mLegacyShader_29;
	// System.Int32 UIDrawCall::mRenderQueue
	int32_t ___mRenderQueue_30;
	// System.Int32 UIDrawCall::mTriangles
	int32_t ___mTriangles_31;
	// System.Boolean UIDrawCall::isDirty
	bool ___isDirty_32;
	// System.Boolean UIDrawCall::mTextureClip
	bool ___mTextureClip_33;
	// System.Boolean UIDrawCall::mIsNew
	bool ___mIsNew_34;
	// UIDrawCall/OnRenderCallback UIDrawCall::onRender
	OnRenderCallback_t133425655 * ___onRender_35;
	// UIDrawCall/OnCreateDrawCall UIDrawCall::onCreateDrawCall
	OnCreateDrawCall_t609469653 * ___onCreateDrawCall_36;
	// System.String UIDrawCall::mSortingLayerName
	String_t* ___mSortingLayerName_37;
	// System.Int32 UIDrawCall::mSortingOrder
	int32_t ___mSortingOrder_38;
	// UnityEngine.MaterialPropertyBlock UIDrawCall::mBlock
	MaterialPropertyBlock_t3213117958 * ___mBlock_42;

public:
	inline static int32_t get_offset_of_widgetCount_4() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___widgetCount_4)); }
	inline int32_t get_widgetCount_4() const { return ___widgetCount_4; }
	inline int32_t* get_address_of_widgetCount_4() { return &___widgetCount_4; }
	inline void set_widgetCount_4(int32_t value)
	{
		___widgetCount_4 = value;
	}

	inline static int32_t get_offset_of_depthStart_5() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___depthStart_5)); }
	inline int32_t get_depthStart_5() const { return ___depthStart_5; }
	inline int32_t* get_address_of_depthStart_5() { return &___depthStart_5; }
	inline void set_depthStart_5(int32_t value)
	{
		___depthStart_5 = value;
	}

	inline static int32_t get_offset_of_depthEnd_6() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___depthEnd_6)); }
	inline int32_t get_depthEnd_6() const { return ___depthEnd_6; }
	inline int32_t* get_address_of_depthEnd_6() { return &___depthEnd_6; }
	inline void set_depthEnd_6(int32_t value)
	{
		___depthEnd_6 = value;
	}

	inline static int32_t get_offset_of_manager_7() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___manager_7)); }
	inline UIPanel_t1716472341 * get_manager_7() const { return ___manager_7; }
	inline UIPanel_t1716472341 ** get_address_of_manager_7() { return &___manager_7; }
	inline void set_manager_7(UIPanel_t1716472341 * value)
	{
		___manager_7 = value;
		Il2CppCodeGenWriteBarrier((&___manager_7), value);
	}

	inline static int32_t get_offset_of_panel_8() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___panel_8)); }
	inline UIPanel_t1716472341 * get_panel_8() const { return ___panel_8; }
	inline UIPanel_t1716472341 ** get_address_of_panel_8() { return &___panel_8; }
	inline void set_panel_8(UIPanel_t1716472341 * value)
	{
		___panel_8 = value;
		Il2CppCodeGenWriteBarrier((&___panel_8), value);
	}

	inline static int32_t get_offset_of_clipTexture_9() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___clipTexture_9)); }
	inline Texture2D_t3840446185 * get_clipTexture_9() const { return ___clipTexture_9; }
	inline Texture2D_t3840446185 ** get_address_of_clipTexture_9() { return &___clipTexture_9; }
	inline void set_clipTexture_9(Texture2D_t3840446185 * value)
	{
		___clipTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___clipTexture_9), value);
	}

	inline static int32_t get_offset_of_alwaysOnScreen_10() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___alwaysOnScreen_10)); }
	inline bool get_alwaysOnScreen_10() const { return ___alwaysOnScreen_10; }
	inline bool* get_address_of_alwaysOnScreen_10() { return &___alwaysOnScreen_10; }
	inline void set_alwaysOnScreen_10(bool value)
	{
		___alwaysOnScreen_10 = value;
	}

	inline static int32_t get_offset_of_verts_11() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___verts_11)); }
	inline List_1_t899420910 * get_verts_11() const { return ___verts_11; }
	inline List_1_t899420910 ** get_address_of_verts_11() { return &___verts_11; }
	inline void set_verts_11(List_1_t899420910 * value)
	{
		___verts_11 = value;
		Il2CppCodeGenWriteBarrier((&___verts_11), value);
	}

	inline static int32_t get_offset_of_norms_12() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___norms_12)); }
	inline List_1_t899420910 * get_norms_12() const { return ___norms_12; }
	inline List_1_t899420910 ** get_address_of_norms_12() { return &___norms_12; }
	inline void set_norms_12(List_1_t899420910 * value)
	{
		___norms_12 = value;
		Il2CppCodeGenWriteBarrier((&___norms_12), value);
	}

	inline static int32_t get_offset_of_tans_13() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___tans_13)); }
	inline List_1_t496136383 * get_tans_13() const { return ___tans_13; }
	inline List_1_t496136383 ** get_address_of_tans_13() { return &___tans_13; }
	inline void set_tans_13(List_1_t496136383 * value)
	{
		___tans_13 = value;
		Il2CppCodeGenWriteBarrier((&___tans_13), value);
	}

	inline static int32_t get_offset_of_uvs_14() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___uvs_14)); }
	inline List_1_t3628304265 * get_uvs_14() const { return ___uvs_14; }
	inline List_1_t3628304265 ** get_address_of_uvs_14() { return &___uvs_14; }
	inline void set_uvs_14(List_1_t3628304265 * value)
	{
		___uvs_14 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_14), value);
	}

	inline static int32_t get_offset_of_uv2_15() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___uv2_15)); }
	inline List_1_t496136383 * get_uv2_15() const { return ___uv2_15; }
	inline List_1_t496136383 ** get_address_of_uv2_15() { return &___uv2_15; }
	inline void set_uv2_15(List_1_t496136383 * value)
	{
		___uv2_15 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_15), value);
	}

	inline static int32_t get_offset_of_cols_16() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___cols_16)); }
	inline List_1_t4027761066 * get_cols_16() const { return ___cols_16; }
	inline List_1_t4027761066 ** get_address_of_cols_16() { return &___cols_16; }
	inline void set_cols_16(List_1_t4027761066 * value)
	{
		___cols_16 = value;
		Il2CppCodeGenWriteBarrier((&___cols_16), value);
	}

	inline static int32_t get_offset_of_mMaterial_17() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mMaterial_17)); }
	inline Material_t340375123 * get_mMaterial_17() const { return ___mMaterial_17; }
	inline Material_t340375123 ** get_address_of_mMaterial_17() { return &___mMaterial_17; }
	inline void set_mMaterial_17(Material_t340375123 * value)
	{
		___mMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___mMaterial_17), value);
	}

	inline static int32_t get_offset_of_mTexture_18() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTexture_18)); }
	inline Texture_t3661962703 * get_mTexture_18() const { return ___mTexture_18; }
	inline Texture_t3661962703 ** get_address_of_mTexture_18() { return &___mTexture_18; }
	inline void set_mTexture_18(Texture_t3661962703 * value)
	{
		___mTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_18), value);
	}

	inline static int32_t get_offset_of_mShader_19() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mShader_19)); }
	inline Shader_t4151988712 * get_mShader_19() const { return ___mShader_19; }
	inline Shader_t4151988712 ** get_address_of_mShader_19() { return &___mShader_19; }
	inline void set_mShader_19(Shader_t4151988712 * value)
	{
		___mShader_19 = value;
		Il2CppCodeGenWriteBarrier((&___mShader_19), value);
	}

	inline static int32_t get_offset_of_mClipCount_20() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mClipCount_20)); }
	inline int32_t get_mClipCount_20() const { return ___mClipCount_20; }
	inline int32_t* get_address_of_mClipCount_20() { return &___mClipCount_20; }
	inline void set_mClipCount_20(int32_t value)
	{
		___mClipCount_20 = value;
	}

	inline static int32_t get_offset_of_mTrans_21() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTrans_21)); }
	inline Transform_t3600365921 * get_mTrans_21() const { return ___mTrans_21; }
	inline Transform_t3600365921 ** get_address_of_mTrans_21() { return &___mTrans_21; }
	inline void set_mTrans_21(Transform_t3600365921 * value)
	{
		___mTrans_21 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_21), value);
	}

	inline static int32_t get_offset_of_mMesh_22() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mMesh_22)); }
	inline Mesh_t3648964284 * get_mMesh_22() const { return ___mMesh_22; }
	inline Mesh_t3648964284 ** get_address_of_mMesh_22() { return &___mMesh_22; }
	inline void set_mMesh_22(Mesh_t3648964284 * value)
	{
		___mMesh_22 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_22), value);
	}

	inline static int32_t get_offset_of_mFilter_23() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mFilter_23)); }
	inline MeshFilter_t3523625662 * get_mFilter_23() const { return ___mFilter_23; }
	inline MeshFilter_t3523625662 ** get_address_of_mFilter_23() { return &___mFilter_23; }
	inline void set_mFilter_23(MeshFilter_t3523625662 * value)
	{
		___mFilter_23 = value;
		Il2CppCodeGenWriteBarrier((&___mFilter_23), value);
	}

	inline static int32_t get_offset_of_mRenderer_24() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRenderer_24)); }
	inline MeshRenderer_t587009260 * get_mRenderer_24() const { return ___mRenderer_24; }
	inline MeshRenderer_t587009260 ** get_address_of_mRenderer_24() { return &___mRenderer_24; }
	inline void set_mRenderer_24(MeshRenderer_t587009260 * value)
	{
		___mRenderer_24 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderer_24), value);
	}

	inline static int32_t get_offset_of_mDynamicMat_25() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mDynamicMat_25)); }
	inline Material_t340375123 * get_mDynamicMat_25() const { return ___mDynamicMat_25; }
	inline Material_t340375123 ** get_address_of_mDynamicMat_25() { return &___mDynamicMat_25; }
	inline void set_mDynamicMat_25(Material_t340375123 * value)
	{
		___mDynamicMat_25 = value;
		Il2CppCodeGenWriteBarrier((&___mDynamicMat_25), value);
	}

	inline static int32_t get_offset_of_mIndices_26() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mIndices_26)); }
	inline Int32U5BU5D_t385246372* get_mIndices_26() const { return ___mIndices_26; }
	inline Int32U5BU5D_t385246372** get_address_of_mIndices_26() { return &___mIndices_26; }
	inline void set_mIndices_26(Int32U5BU5D_t385246372* value)
	{
		___mIndices_26 = value;
		Il2CppCodeGenWriteBarrier((&___mIndices_26), value);
	}

	inline static int32_t get_offset_of_mShadowMode_27() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mShadowMode_27)); }
	inline int32_t get_mShadowMode_27() const { return ___mShadowMode_27; }
	inline int32_t* get_address_of_mShadowMode_27() { return &___mShadowMode_27; }
	inline void set_mShadowMode_27(int32_t value)
	{
		___mShadowMode_27 = value;
	}

	inline static int32_t get_offset_of_mRebuildMat_28() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRebuildMat_28)); }
	inline bool get_mRebuildMat_28() const { return ___mRebuildMat_28; }
	inline bool* get_address_of_mRebuildMat_28() { return &___mRebuildMat_28; }
	inline void set_mRebuildMat_28(bool value)
	{
		___mRebuildMat_28 = value;
	}

	inline static int32_t get_offset_of_mLegacyShader_29() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mLegacyShader_29)); }
	inline bool get_mLegacyShader_29() const { return ___mLegacyShader_29; }
	inline bool* get_address_of_mLegacyShader_29() { return &___mLegacyShader_29; }
	inline void set_mLegacyShader_29(bool value)
	{
		___mLegacyShader_29 = value;
	}

	inline static int32_t get_offset_of_mRenderQueue_30() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRenderQueue_30)); }
	inline int32_t get_mRenderQueue_30() const { return ___mRenderQueue_30; }
	inline int32_t* get_address_of_mRenderQueue_30() { return &___mRenderQueue_30; }
	inline void set_mRenderQueue_30(int32_t value)
	{
		___mRenderQueue_30 = value;
	}

	inline static int32_t get_offset_of_mTriangles_31() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTriangles_31)); }
	inline int32_t get_mTriangles_31() const { return ___mTriangles_31; }
	inline int32_t* get_address_of_mTriangles_31() { return &___mTriangles_31; }
	inline void set_mTriangles_31(int32_t value)
	{
		___mTriangles_31 = value;
	}

	inline static int32_t get_offset_of_isDirty_32() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___isDirty_32)); }
	inline bool get_isDirty_32() const { return ___isDirty_32; }
	inline bool* get_address_of_isDirty_32() { return &___isDirty_32; }
	inline void set_isDirty_32(bool value)
	{
		___isDirty_32 = value;
	}

	inline static int32_t get_offset_of_mTextureClip_33() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTextureClip_33)); }
	inline bool get_mTextureClip_33() const { return ___mTextureClip_33; }
	inline bool* get_address_of_mTextureClip_33() { return &___mTextureClip_33; }
	inline void set_mTextureClip_33(bool value)
	{
		___mTextureClip_33 = value;
	}

	inline static int32_t get_offset_of_mIsNew_34() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mIsNew_34)); }
	inline bool get_mIsNew_34() const { return ___mIsNew_34; }
	inline bool* get_address_of_mIsNew_34() { return &___mIsNew_34; }
	inline void set_mIsNew_34(bool value)
	{
		___mIsNew_34 = value;
	}

	inline static int32_t get_offset_of_onRender_35() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___onRender_35)); }
	inline OnRenderCallback_t133425655 * get_onRender_35() const { return ___onRender_35; }
	inline OnRenderCallback_t133425655 ** get_address_of_onRender_35() { return &___onRender_35; }
	inline void set_onRender_35(OnRenderCallback_t133425655 * value)
	{
		___onRender_35 = value;
		Il2CppCodeGenWriteBarrier((&___onRender_35), value);
	}

	inline static int32_t get_offset_of_onCreateDrawCall_36() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___onCreateDrawCall_36)); }
	inline OnCreateDrawCall_t609469653 * get_onCreateDrawCall_36() const { return ___onCreateDrawCall_36; }
	inline OnCreateDrawCall_t609469653 ** get_address_of_onCreateDrawCall_36() { return &___onCreateDrawCall_36; }
	inline void set_onCreateDrawCall_36(OnCreateDrawCall_t609469653 * value)
	{
		___onCreateDrawCall_36 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateDrawCall_36), value);
	}

	inline static int32_t get_offset_of_mSortingLayerName_37() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mSortingLayerName_37)); }
	inline String_t* get_mSortingLayerName_37() const { return ___mSortingLayerName_37; }
	inline String_t** get_address_of_mSortingLayerName_37() { return &___mSortingLayerName_37; }
	inline void set_mSortingLayerName_37(String_t* value)
	{
		___mSortingLayerName_37 = value;
		Il2CppCodeGenWriteBarrier((&___mSortingLayerName_37), value);
	}

	inline static int32_t get_offset_of_mSortingOrder_38() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mSortingOrder_38)); }
	inline int32_t get_mSortingOrder_38() const { return ___mSortingOrder_38; }
	inline int32_t* get_address_of_mSortingOrder_38() { return &___mSortingOrder_38; }
	inline void set_mSortingOrder_38(int32_t value)
	{
		___mSortingOrder_38 = value;
	}

	inline static int32_t get_offset_of_mBlock_42() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mBlock_42)); }
	inline MaterialPropertyBlock_t3213117958 * get_mBlock_42() const { return ___mBlock_42; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_mBlock_42() { return &___mBlock_42; }
	inline void set_mBlock_42(MaterialPropertyBlock_t3213117958 * value)
	{
		___mBlock_42 = value;
		Il2CppCodeGenWriteBarrier((&___mBlock_42), value);
	}
};

struct UIDrawCall_t1293405319_StaticFields
{
public:
	// BetterList`1<UIDrawCall> UIDrawCall::mActiveList
	BetterList_1_t448425637 * ___mActiveList_2;
	// BetterList`1<UIDrawCall> UIDrawCall::mInactiveList
	BetterList_1_t448425637 * ___mInactiveList_3;
	// UnityEngine.ColorSpace UIDrawCall::mColorSpace
	int32_t ___mColorSpace_39;
	// System.Collections.Generic.List`1<System.Int32[]> UIDrawCall::mCache
	List_1_t1857321114 * ___mCache_41;
	// System.Int32[] UIDrawCall::ClipRange
	Int32U5BU5D_t385246372* ___ClipRange_43;
	// System.Int32[] UIDrawCall::ClipArgs
	Int32U5BU5D_t385246372* ___ClipArgs_44;
	// System.Int32 UIDrawCall::dx9BugWorkaround
	int32_t ___dx9BugWorkaround_45;

public:
	inline static int32_t get_offset_of_mActiveList_2() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mActiveList_2)); }
	inline BetterList_1_t448425637 * get_mActiveList_2() const { return ___mActiveList_2; }
	inline BetterList_1_t448425637 ** get_address_of_mActiveList_2() { return &___mActiveList_2; }
	inline void set_mActiveList_2(BetterList_1_t448425637 * value)
	{
		___mActiveList_2 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveList_2), value);
	}

	inline static int32_t get_offset_of_mInactiveList_3() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mInactiveList_3)); }
	inline BetterList_1_t448425637 * get_mInactiveList_3() const { return ___mInactiveList_3; }
	inline BetterList_1_t448425637 ** get_address_of_mInactiveList_3() { return &___mInactiveList_3; }
	inline void set_mInactiveList_3(BetterList_1_t448425637 * value)
	{
		___mInactiveList_3 = value;
		Il2CppCodeGenWriteBarrier((&___mInactiveList_3), value);
	}

	inline static int32_t get_offset_of_mColorSpace_39() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mColorSpace_39)); }
	inline int32_t get_mColorSpace_39() const { return ___mColorSpace_39; }
	inline int32_t* get_address_of_mColorSpace_39() { return &___mColorSpace_39; }
	inline void set_mColorSpace_39(int32_t value)
	{
		___mColorSpace_39 = value;
	}

	inline static int32_t get_offset_of_mCache_41() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mCache_41)); }
	inline List_1_t1857321114 * get_mCache_41() const { return ___mCache_41; }
	inline List_1_t1857321114 ** get_address_of_mCache_41() { return &___mCache_41; }
	inline void set_mCache_41(List_1_t1857321114 * value)
	{
		___mCache_41 = value;
		Il2CppCodeGenWriteBarrier((&___mCache_41), value);
	}

	inline static int32_t get_offset_of_ClipRange_43() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___ClipRange_43)); }
	inline Int32U5BU5D_t385246372* get_ClipRange_43() const { return ___ClipRange_43; }
	inline Int32U5BU5D_t385246372** get_address_of_ClipRange_43() { return &___ClipRange_43; }
	inline void set_ClipRange_43(Int32U5BU5D_t385246372* value)
	{
		___ClipRange_43 = value;
		Il2CppCodeGenWriteBarrier((&___ClipRange_43), value);
	}

	inline static int32_t get_offset_of_ClipArgs_44() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___ClipArgs_44)); }
	inline Int32U5BU5D_t385246372* get_ClipArgs_44() const { return ___ClipArgs_44; }
	inline Int32U5BU5D_t385246372** get_address_of_ClipArgs_44() { return &___ClipArgs_44; }
	inline void set_ClipArgs_44(Int32U5BU5D_t385246372* value)
	{
		___ClipArgs_44 = value;
		Il2CppCodeGenWriteBarrier((&___ClipArgs_44), value);
	}

	inline static int32_t get_offset_of_dx9BugWorkaround_45() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___dx9BugWorkaround_45)); }
	inline int32_t get_dx9BugWorkaround_45() const { return ___dx9BugWorkaround_45; }
	inline int32_t* get_address_of_dx9BugWorkaround_45() { return &___dx9BugWorkaround_45; }
	inline void set_dx9BugWorkaround_45(int32_t value)
	{
		___dx9BugWorkaround_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWCALL_T1293405319_H
#ifndef UISNAPSHOTPOINT_T2982659727_H
#define UISNAPSHOTPOINT_T2982659727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISnapshotPoint
struct  UISnapshotPoint_t2982659727  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UISnapshotPoint::isOrthographic
	bool ___isOrthographic_2;
	// System.Single UISnapshotPoint::nearClip
	float ___nearClip_3;
	// System.Single UISnapshotPoint::farClip
	float ___farClip_4;
	// System.Int32 UISnapshotPoint::fieldOfView
	int32_t ___fieldOfView_5;
	// System.Single UISnapshotPoint::orthoSize
	float ___orthoSize_6;
	// UnityEngine.Texture2D UISnapshotPoint::thumbnail
	Texture2D_t3840446185 * ___thumbnail_7;

public:
	inline static int32_t get_offset_of_isOrthographic_2() { return static_cast<int32_t>(offsetof(UISnapshotPoint_t2982659727, ___isOrthographic_2)); }
	inline bool get_isOrthographic_2() const { return ___isOrthographic_2; }
	inline bool* get_address_of_isOrthographic_2() { return &___isOrthographic_2; }
	inline void set_isOrthographic_2(bool value)
	{
		___isOrthographic_2 = value;
	}

	inline static int32_t get_offset_of_nearClip_3() { return static_cast<int32_t>(offsetof(UISnapshotPoint_t2982659727, ___nearClip_3)); }
	inline float get_nearClip_3() const { return ___nearClip_3; }
	inline float* get_address_of_nearClip_3() { return &___nearClip_3; }
	inline void set_nearClip_3(float value)
	{
		___nearClip_3 = value;
	}

	inline static int32_t get_offset_of_farClip_4() { return static_cast<int32_t>(offsetof(UISnapshotPoint_t2982659727, ___farClip_4)); }
	inline float get_farClip_4() const { return ___farClip_4; }
	inline float* get_address_of_farClip_4() { return &___farClip_4; }
	inline void set_farClip_4(float value)
	{
		___farClip_4 = value;
	}

	inline static int32_t get_offset_of_fieldOfView_5() { return static_cast<int32_t>(offsetof(UISnapshotPoint_t2982659727, ___fieldOfView_5)); }
	inline int32_t get_fieldOfView_5() const { return ___fieldOfView_5; }
	inline int32_t* get_address_of_fieldOfView_5() { return &___fieldOfView_5; }
	inline void set_fieldOfView_5(int32_t value)
	{
		___fieldOfView_5 = value;
	}

	inline static int32_t get_offset_of_orthoSize_6() { return static_cast<int32_t>(offsetof(UISnapshotPoint_t2982659727, ___orthoSize_6)); }
	inline float get_orthoSize_6() const { return ___orthoSize_6; }
	inline float* get_address_of_orthoSize_6() { return &___orthoSize_6; }
	inline void set_orthoSize_6(float value)
	{
		___orthoSize_6 = value;
	}

	inline static int32_t get_offset_of_thumbnail_7() { return static_cast<int32_t>(offsetof(UISnapshotPoint_t2982659727, ___thumbnail_7)); }
	inline Texture2D_t3840446185 * get_thumbnail_7() const { return ___thumbnail_7; }
	inline Texture2D_t3840446185 ** get_address_of_thumbnail_7() { return &___thumbnail_7; }
	inline void set_thumbnail_7(Texture2D_t3840446185 * value)
	{
		___thumbnail_7 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISNAPSHOTPOINT_T2982659727_H
#ifndef SPRINGPOSITION_T3478173108_H
#define SPRINGPOSITION_T3478173108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpringPosition
struct  SpringPosition_t3478173108  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 SpringPosition::target
	Vector3_t3722313464  ___target_3;
	// System.Single SpringPosition::strength
	float ___strength_4;
	// System.Boolean SpringPosition::worldSpace
	bool ___worldSpace_5;
	// System.Boolean SpringPosition::ignoreTimeScale
	bool ___ignoreTimeScale_6;
	// System.Boolean SpringPosition::updateScrollView
	bool ___updateScrollView_7;
	// SpringPosition/OnFinished SpringPosition::onFinished
	OnFinished_t3364492952 * ___onFinished_8;
	// UnityEngine.GameObject SpringPosition::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_9;
	// System.String SpringPosition::callWhenFinished
	String_t* ___callWhenFinished_10;
	// UnityEngine.Transform SpringPosition::mTrans
	Transform_t3600365921 * ___mTrans_11;
	// System.Single SpringPosition::mThreshold
	float ___mThreshold_12;
	// UIScrollView SpringPosition::mSv
	UIScrollView_t1973404950 * ___mSv_13;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___target_3)); }
	inline Vector3_t3722313464  get_target_3() const { return ___target_3; }
	inline Vector3_t3722313464 * get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Vector3_t3722313464  value)
	{
		___target_3 = value;
	}

	inline static int32_t get_offset_of_strength_4() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___strength_4)); }
	inline float get_strength_4() const { return ___strength_4; }
	inline float* get_address_of_strength_4() { return &___strength_4; }
	inline void set_strength_4(float value)
	{
		___strength_4 = value;
	}

	inline static int32_t get_offset_of_worldSpace_5() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___worldSpace_5)); }
	inline bool get_worldSpace_5() const { return ___worldSpace_5; }
	inline bool* get_address_of_worldSpace_5() { return &___worldSpace_5; }
	inline void set_worldSpace_5(bool value)
	{
		___worldSpace_5 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_6() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___ignoreTimeScale_6)); }
	inline bool get_ignoreTimeScale_6() const { return ___ignoreTimeScale_6; }
	inline bool* get_address_of_ignoreTimeScale_6() { return &___ignoreTimeScale_6; }
	inline void set_ignoreTimeScale_6(bool value)
	{
		___ignoreTimeScale_6 = value;
	}

	inline static int32_t get_offset_of_updateScrollView_7() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___updateScrollView_7)); }
	inline bool get_updateScrollView_7() const { return ___updateScrollView_7; }
	inline bool* get_address_of_updateScrollView_7() { return &___updateScrollView_7; }
	inline void set_updateScrollView_7(bool value)
	{
		___updateScrollView_7 = value;
	}

	inline static int32_t get_offset_of_onFinished_8() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___onFinished_8)); }
	inline OnFinished_t3364492952 * get_onFinished_8() const { return ___onFinished_8; }
	inline OnFinished_t3364492952 ** get_address_of_onFinished_8() { return &___onFinished_8; }
	inline void set_onFinished_8(OnFinished_t3364492952 * value)
	{
		___onFinished_8 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_8), value);
	}

	inline static int32_t get_offset_of_eventReceiver_9() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___eventReceiver_9)); }
	inline GameObject_t1113636619 * get_eventReceiver_9() const { return ___eventReceiver_9; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_9() { return &___eventReceiver_9; }
	inline void set_eventReceiver_9(GameObject_t1113636619 * value)
	{
		___eventReceiver_9 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_9), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_10() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___callWhenFinished_10)); }
	inline String_t* get_callWhenFinished_10() const { return ___callWhenFinished_10; }
	inline String_t** get_address_of_callWhenFinished_10() { return &___callWhenFinished_10; }
	inline void set_callWhenFinished_10(String_t* value)
	{
		___callWhenFinished_10 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_10), value);
	}

	inline static int32_t get_offset_of_mTrans_11() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___mTrans_11)); }
	inline Transform_t3600365921 * get_mTrans_11() const { return ___mTrans_11; }
	inline Transform_t3600365921 ** get_address_of_mTrans_11() { return &___mTrans_11; }
	inline void set_mTrans_11(Transform_t3600365921 * value)
	{
		___mTrans_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_11), value);
	}

	inline static int32_t get_offset_of_mThreshold_12() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___mThreshold_12)); }
	inline float get_mThreshold_12() const { return ___mThreshold_12; }
	inline float* get_address_of_mThreshold_12() { return &___mThreshold_12; }
	inline void set_mThreshold_12(float value)
	{
		___mThreshold_12 = value;
	}

	inline static int32_t get_offset_of_mSv_13() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108, ___mSv_13)); }
	inline UIScrollView_t1973404950 * get_mSv_13() const { return ___mSv_13; }
	inline UIScrollView_t1973404950 ** get_address_of_mSv_13() { return &___mSv_13; }
	inline void set_mSv_13(UIScrollView_t1973404950 * value)
	{
		___mSv_13 = value;
		Il2CppCodeGenWriteBarrier((&___mSv_13), value);
	}
};

struct SpringPosition_t3478173108_StaticFields
{
public:
	// SpringPosition SpringPosition::current
	SpringPosition_t3478173108 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(SpringPosition_t3478173108_StaticFields, ___current_2)); }
	inline SpringPosition_t3478173108 * get_current_2() const { return ___current_2; }
	inline SpringPosition_t3478173108 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(SpringPosition_t3478173108 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRINGPOSITION_T3478173108_H
#ifndef UITWEENER_T260334902_H
#define UITWEENER_T260334902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener
struct  UITweener_t260334902  : public MonoBehaviour_t3962482529
{
public:
	// UITweener/Method UITweener::method
	int32_t ___method_3;
	// UITweener/Style UITweener::style
	int32_t ___style_4;
	// UnityEngine.AnimationCurve UITweener::animationCurve
	AnimationCurve_t3046754366 * ___animationCurve_5;
	// System.Boolean UITweener::ignoreTimeScale
	bool ___ignoreTimeScale_6;
	// System.Single UITweener::delay
	float ___delay_7;
	// System.Single UITweener::duration
	float ___duration_8;
	// System.Boolean UITweener::steeperCurves
	bool ___steeperCurves_9;
	// System.Int32 UITweener::tweenGroup
	int32_t ___tweenGroup_10;
	// System.Boolean UITweener::useFixedUpdate
	bool ___useFixedUpdate_11;
	// System.Collections.Generic.List`1<EventDelegate> UITweener::onFinished
	List_1_t4210400802 * ___onFinished_12;
	// UnityEngine.GameObject UITweener::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_13;
	// System.String UITweener::callWhenFinished
	String_t* ___callWhenFinished_14;
	// System.Single UITweener::timeScale
	float ___timeScale_15;
	// System.Boolean UITweener::mStarted
	bool ___mStarted_16;
	// System.Single UITweener::mStartTime
	float ___mStartTime_17;
	// System.Single UITweener::mDuration
	float ___mDuration_18;
	// System.Single UITweener::mAmountPerDelta
	float ___mAmountPerDelta_19;
	// System.Single UITweener::mFactor
	float ___mFactor_20;
	// System.Collections.Generic.List`1<EventDelegate> UITweener::mTemp
	List_1_t4210400802 * ___mTemp_21;

public:
	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___method_3)); }
	inline int32_t get_method_3() const { return ___method_3; }
	inline int32_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(int32_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_style_4() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___style_4)); }
	inline int32_t get_style_4() const { return ___style_4; }
	inline int32_t* get_address_of_style_4() { return &___style_4; }
	inline void set_style_4(int32_t value)
	{
		___style_4 = value;
	}

	inline static int32_t get_offset_of_animationCurve_5() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___animationCurve_5)); }
	inline AnimationCurve_t3046754366 * get_animationCurve_5() const { return ___animationCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_animationCurve_5() { return &___animationCurve_5; }
	inline void set_animationCurve_5(AnimationCurve_t3046754366 * value)
	{
		___animationCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___animationCurve_5), value);
	}

	inline static int32_t get_offset_of_ignoreTimeScale_6() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___ignoreTimeScale_6)); }
	inline bool get_ignoreTimeScale_6() const { return ___ignoreTimeScale_6; }
	inline bool* get_address_of_ignoreTimeScale_6() { return &___ignoreTimeScale_6; }
	inline void set_ignoreTimeScale_6(bool value)
	{
		___ignoreTimeScale_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_duration_8() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___duration_8)); }
	inline float get_duration_8() const { return ___duration_8; }
	inline float* get_address_of_duration_8() { return &___duration_8; }
	inline void set_duration_8(float value)
	{
		___duration_8 = value;
	}

	inline static int32_t get_offset_of_steeperCurves_9() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___steeperCurves_9)); }
	inline bool get_steeperCurves_9() const { return ___steeperCurves_9; }
	inline bool* get_address_of_steeperCurves_9() { return &___steeperCurves_9; }
	inline void set_steeperCurves_9(bool value)
	{
		___steeperCurves_9 = value;
	}

	inline static int32_t get_offset_of_tweenGroup_10() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___tweenGroup_10)); }
	inline int32_t get_tweenGroup_10() const { return ___tweenGroup_10; }
	inline int32_t* get_address_of_tweenGroup_10() { return &___tweenGroup_10; }
	inline void set_tweenGroup_10(int32_t value)
	{
		___tweenGroup_10 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_11() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___useFixedUpdate_11)); }
	inline bool get_useFixedUpdate_11() const { return ___useFixedUpdate_11; }
	inline bool* get_address_of_useFixedUpdate_11() { return &___useFixedUpdate_11; }
	inline void set_useFixedUpdate_11(bool value)
	{
		___useFixedUpdate_11 = value;
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___onFinished_12)); }
	inline List_1_t4210400802 * get_onFinished_12() const { return ___onFinished_12; }
	inline List_1_t4210400802 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(List_1_t4210400802 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_eventReceiver_13() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___eventReceiver_13)); }
	inline GameObject_t1113636619 * get_eventReceiver_13() const { return ___eventReceiver_13; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_13() { return &___eventReceiver_13; }
	inline void set_eventReceiver_13(GameObject_t1113636619 * value)
	{
		___eventReceiver_13 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_13), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_14() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___callWhenFinished_14)); }
	inline String_t* get_callWhenFinished_14() const { return ___callWhenFinished_14; }
	inline String_t** get_address_of_callWhenFinished_14() { return &___callWhenFinished_14; }
	inline void set_callWhenFinished_14(String_t* value)
	{
		___callWhenFinished_14 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_14), value);
	}

	inline static int32_t get_offset_of_timeScale_15() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___timeScale_15)); }
	inline float get_timeScale_15() const { return ___timeScale_15; }
	inline float* get_address_of_timeScale_15() { return &___timeScale_15; }
	inline void set_timeScale_15(float value)
	{
		___timeScale_15 = value;
	}

	inline static int32_t get_offset_of_mStarted_16() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mStarted_16)); }
	inline bool get_mStarted_16() const { return ___mStarted_16; }
	inline bool* get_address_of_mStarted_16() { return &___mStarted_16; }
	inline void set_mStarted_16(bool value)
	{
		___mStarted_16 = value;
	}

	inline static int32_t get_offset_of_mStartTime_17() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mStartTime_17)); }
	inline float get_mStartTime_17() const { return ___mStartTime_17; }
	inline float* get_address_of_mStartTime_17() { return &___mStartTime_17; }
	inline void set_mStartTime_17(float value)
	{
		___mStartTime_17 = value;
	}

	inline static int32_t get_offset_of_mDuration_18() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mDuration_18)); }
	inline float get_mDuration_18() const { return ___mDuration_18; }
	inline float* get_address_of_mDuration_18() { return &___mDuration_18; }
	inline void set_mDuration_18(float value)
	{
		___mDuration_18 = value;
	}

	inline static int32_t get_offset_of_mAmountPerDelta_19() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mAmountPerDelta_19)); }
	inline float get_mAmountPerDelta_19() const { return ___mAmountPerDelta_19; }
	inline float* get_address_of_mAmountPerDelta_19() { return &___mAmountPerDelta_19; }
	inline void set_mAmountPerDelta_19(float value)
	{
		___mAmountPerDelta_19 = value;
	}

	inline static int32_t get_offset_of_mFactor_20() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mFactor_20)); }
	inline float get_mFactor_20() const { return ___mFactor_20; }
	inline float* get_address_of_mFactor_20() { return &___mFactor_20; }
	inline void set_mFactor_20(float value)
	{
		___mFactor_20 = value;
	}

	inline static int32_t get_offset_of_mTemp_21() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mTemp_21)); }
	inline List_1_t4210400802 * get_mTemp_21() const { return ___mTemp_21; }
	inline List_1_t4210400802 ** get_address_of_mTemp_21() { return &___mTemp_21; }
	inline void set_mTemp_21(List_1_t4210400802 * value)
	{
		___mTemp_21 = value;
		Il2CppCodeGenWriteBarrier((&___mTemp_21), value);
	}
};

struct UITweener_t260334902_StaticFields
{
public:
	// UITweener UITweener::current
	UITweener_t260334902 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UITweener_t260334902_StaticFields, ___current_2)); }
	inline UITweener_t260334902 * get_current_2() const { return ___current_2; }
	inline UITweener_t260334902 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UITweener_t260334902 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITWEENER_T260334902_H
#ifndef ANIMATEDALPHA_T1840762679_H
#define ANIMATEDALPHA_T1840762679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatedAlpha
struct  AnimatedAlpha_t1840762679  : public MonoBehaviour_t3962482529
{
public:
	// System.Single AnimatedAlpha::alpha
	float ___alpha_2;
	// UIWidget AnimatedAlpha::mWidget
	UIWidget_t3538521925 * ___mWidget_3;
	// UIPanel AnimatedAlpha::mPanel
	UIPanel_t1716472341 * ___mPanel_4;

public:
	inline static int32_t get_offset_of_alpha_2() { return static_cast<int32_t>(offsetof(AnimatedAlpha_t1840762679, ___alpha_2)); }
	inline float get_alpha_2() const { return ___alpha_2; }
	inline float* get_address_of_alpha_2() { return &___alpha_2; }
	inline void set_alpha_2(float value)
	{
		___alpha_2 = value;
	}

	inline static int32_t get_offset_of_mWidget_3() { return static_cast<int32_t>(offsetof(AnimatedAlpha_t1840762679, ___mWidget_3)); }
	inline UIWidget_t3538521925 * get_mWidget_3() const { return ___mWidget_3; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_3() { return &___mWidget_3; }
	inline void set_mWidget_3(UIWidget_t3538521925 * value)
	{
		___mWidget_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_3), value);
	}

	inline static int32_t get_offset_of_mPanel_4() { return static_cast<int32_t>(offsetof(AnimatedAlpha_t1840762679, ___mPanel_4)); }
	inline UIPanel_t1716472341 * get_mPanel_4() const { return ___mPanel_4; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_4() { return &___mPanel_4; }
	inline void set_mPanel_4(UIPanel_t1716472341 * value)
	{
		___mPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEDALPHA_T1840762679_H
#ifndef ANIMATEDCOLOR_T3276574810_H
#define ANIMATEDCOLOR_T3276574810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatedColor
struct  AnimatedColor_t3276574810  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color AnimatedColor::color
	Color_t2555686324  ___color_2;
	// UIWidget AnimatedColor::mWidget
	UIWidget_t3538521925 * ___mWidget_3;

public:
	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(AnimatedColor_t3276574810, ___color_2)); }
	inline Color_t2555686324  get_color_2() const { return ___color_2; }
	inline Color_t2555686324 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color_t2555686324  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_mWidget_3() { return static_cast<int32_t>(offsetof(AnimatedColor_t3276574810, ___mWidget_3)); }
	inline UIWidget_t3538521925 * get_mWidget_3() const { return ___mWidget_3; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_3() { return &___mWidget_3; }
	inline void set_mWidget_3(UIWidget_t3538521925 * value)
	{
		___mWidget_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEDCOLOR_T3276574810_H
#ifndef ANIMATEDWIDGET_T1381166569_H
#define ANIMATEDWIDGET_T1381166569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatedWidget
struct  AnimatedWidget_t1381166569  : public MonoBehaviour_t3962482529
{
public:
	// System.Single AnimatedWidget::width
	float ___width_2;
	// System.Single AnimatedWidget::height
	float ___height_3;
	// UIWidget AnimatedWidget::mWidget
	UIWidget_t3538521925 * ___mWidget_4;

public:
	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(AnimatedWidget_t1381166569, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(AnimatedWidget_t1381166569, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_mWidget_4() { return static_cast<int32_t>(offsetof(AnimatedWidget_t1381166569, ___mWidget_4)); }
	inline UIWidget_t3538521925 * get_mWidget_4() const { return ___mWidget_4; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_4() { return &___mWidget_4; }
	inline void set_mWidget_4(UIWidget_t3538521925 * value)
	{
		___mWidget_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEDWIDGET_T1381166569_H
#ifndef UI2DSPRITEANIMATION_T3056508403_H
#define UI2DSPRITEANIMATION_T3056508403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI2DSpriteAnimation
struct  UI2DSpriteAnimation_t3056508403  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UI2DSpriteAnimation::frameIndex
	int32_t ___frameIndex_2;
	// System.Int32 UI2DSpriteAnimation::framerate
	int32_t ___framerate_3;
	// System.Boolean UI2DSpriteAnimation::ignoreTimeScale
	bool ___ignoreTimeScale_4;
	// System.Boolean UI2DSpriteAnimation::loop
	bool ___loop_5;
	// UnityEngine.Sprite[] UI2DSpriteAnimation::frames
	SpriteU5BU5D_t2581906349* ___frames_6;
	// UnityEngine.SpriteRenderer UI2DSpriteAnimation::mUnitySprite
	SpriteRenderer_t3235626157 * ___mUnitySprite_7;
	// UI2DSprite UI2DSpriteAnimation::mNguiSprite
	UI2DSprite_t1366157572 * ___mNguiSprite_8;
	// System.Single UI2DSpriteAnimation::mUpdate
	float ___mUpdate_9;

public:
	inline static int32_t get_offset_of_frameIndex_2() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___frameIndex_2)); }
	inline int32_t get_frameIndex_2() const { return ___frameIndex_2; }
	inline int32_t* get_address_of_frameIndex_2() { return &___frameIndex_2; }
	inline void set_frameIndex_2(int32_t value)
	{
		___frameIndex_2 = value;
	}

	inline static int32_t get_offset_of_framerate_3() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___framerate_3)); }
	inline int32_t get_framerate_3() const { return ___framerate_3; }
	inline int32_t* get_address_of_framerate_3() { return &___framerate_3; }
	inline void set_framerate_3(int32_t value)
	{
		___framerate_3 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_4() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___ignoreTimeScale_4)); }
	inline bool get_ignoreTimeScale_4() const { return ___ignoreTimeScale_4; }
	inline bool* get_address_of_ignoreTimeScale_4() { return &___ignoreTimeScale_4; }
	inline void set_ignoreTimeScale_4(bool value)
	{
		___ignoreTimeScale_4 = value;
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_frames_6() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___frames_6)); }
	inline SpriteU5BU5D_t2581906349* get_frames_6() const { return ___frames_6; }
	inline SpriteU5BU5D_t2581906349** get_address_of_frames_6() { return &___frames_6; }
	inline void set_frames_6(SpriteU5BU5D_t2581906349* value)
	{
		___frames_6 = value;
		Il2CppCodeGenWriteBarrier((&___frames_6), value);
	}

	inline static int32_t get_offset_of_mUnitySprite_7() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___mUnitySprite_7)); }
	inline SpriteRenderer_t3235626157 * get_mUnitySprite_7() const { return ___mUnitySprite_7; }
	inline SpriteRenderer_t3235626157 ** get_address_of_mUnitySprite_7() { return &___mUnitySprite_7; }
	inline void set_mUnitySprite_7(SpriteRenderer_t3235626157 * value)
	{
		___mUnitySprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___mUnitySprite_7), value);
	}

	inline static int32_t get_offset_of_mNguiSprite_8() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___mNguiSprite_8)); }
	inline UI2DSprite_t1366157572 * get_mNguiSprite_8() const { return ___mNguiSprite_8; }
	inline UI2DSprite_t1366157572 ** get_address_of_mNguiSprite_8() { return &___mNguiSprite_8; }
	inline void set_mNguiSprite_8(UI2DSprite_t1366157572 * value)
	{
		___mNguiSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___mNguiSprite_8), value);
	}

	inline static int32_t get_offset_of_mUpdate_9() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimation_t3056508403, ___mUpdate_9)); }
	inline float get_mUpdate_9() const { return ___mUpdate_9; }
	inline float* get_address_of_mUpdate_9() { return &___mUpdate_9; }
	inline void set_mUpdate_9(float value)
	{
		___mUpdate_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI2DSPRITEANIMATION_T3056508403_H
#ifndef UIANCHOR_T2527798900_H
#define UIANCHOR_T2527798900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAnchor
struct  UIAnchor_t2527798900  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UIAnchor::uiCamera
	Camera_t4157153871 * ___uiCamera_2;
	// UnityEngine.GameObject UIAnchor::container
	GameObject_t1113636619 * ___container_3;
	// UIAnchor/Side UIAnchor::side
	int32_t ___side_4;
	// System.Boolean UIAnchor::runOnlyOnce
	bool ___runOnlyOnce_5;
	// UnityEngine.Vector2 UIAnchor::relativeOffset
	Vector2_t2156229523  ___relativeOffset_6;
	// UnityEngine.Vector2 UIAnchor::pixelOffset
	Vector2_t2156229523  ___pixelOffset_7;
	// UIWidget UIAnchor::widgetContainer
	UIWidget_t3538521925 * ___widgetContainer_8;
	// UnityEngine.Transform UIAnchor::mTrans
	Transform_t3600365921 * ___mTrans_9;
	// UnityEngine.Animation UIAnchor::mAnim
	Animation_t3648466861 * ___mAnim_10;
	// UnityEngine.Rect UIAnchor::mRect
	Rect_t2360479859  ___mRect_11;
	// UIRoot UIAnchor::mRoot
	UIRoot_t4022971450 * ___mRoot_12;
	// System.Boolean UIAnchor::mStarted
	bool ___mStarted_13;

public:
	inline static int32_t get_offset_of_uiCamera_2() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___uiCamera_2)); }
	inline Camera_t4157153871 * get_uiCamera_2() const { return ___uiCamera_2; }
	inline Camera_t4157153871 ** get_address_of_uiCamera_2() { return &___uiCamera_2; }
	inline void set_uiCamera_2(Camera_t4157153871 * value)
	{
		___uiCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_2), value);
	}

	inline static int32_t get_offset_of_container_3() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___container_3)); }
	inline GameObject_t1113636619 * get_container_3() const { return ___container_3; }
	inline GameObject_t1113636619 ** get_address_of_container_3() { return &___container_3; }
	inline void set_container_3(GameObject_t1113636619 * value)
	{
		___container_3 = value;
		Il2CppCodeGenWriteBarrier((&___container_3), value);
	}

	inline static int32_t get_offset_of_side_4() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___side_4)); }
	inline int32_t get_side_4() const { return ___side_4; }
	inline int32_t* get_address_of_side_4() { return &___side_4; }
	inline void set_side_4(int32_t value)
	{
		___side_4 = value;
	}

	inline static int32_t get_offset_of_runOnlyOnce_5() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___runOnlyOnce_5)); }
	inline bool get_runOnlyOnce_5() const { return ___runOnlyOnce_5; }
	inline bool* get_address_of_runOnlyOnce_5() { return &___runOnlyOnce_5; }
	inline void set_runOnlyOnce_5(bool value)
	{
		___runOnlyOnce_5 = value;
	}

	inline static int32_t get_offset_of_relativeOffset_6() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___relativeOffset_6)); }
	inline Vector2_t2156229523  get_relativeOffset_6() const { return ___relativeOffset_6; }
	inline Vector2_t2156229523 * get_address_of_relativeOffset_6() { return &___relativeOffset_6; }
	inline void set_relativeOffset_6(Vector2_t2156229523  value)
	{
		___relativeOffset_6 = value;
	}

	inline static int32_t get_offset_of_pixelOffset_7() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___pixelOffset_7)); }
	inline Vector2_t2156229523  get_pixelOffset_7() const { return ___pixelOffset_7; }
	inline Vector2_t2156229523 * get_address_of_pixelOffset_7() { return &___pixelOffset_7; }
	inline void set_pixelOffset_7(Vector2_t2156229523  value)
	{
		___pixelOffset_7 = value;
	}

	inline static int32_t get_offset_of_widgetContainer_8() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___widgetContainer_8)); }
	inline UIWidget_t3538521925 * get_widgetContainer_8() const { return ___widgetContainer_8; }
	inline UIWidget_t3538521925 ** get_address_of_widgetContainer_8() { return &___widgetContainer_8; }
	inline void set_widgetContainer_8(UIWidget_t3538521925 * value)
	{
		___widgetContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___widgetContainer_8), value);
	}

	inline static int32_t get_offset_of_mTrans_9() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___mTrans_9)); }
	inline Transform_t3600365921 * get_mTrans_9() const { return ___mTrans_9; }
	inline Transform_t3600365921 ** get_address_of_mTrans_9() { return &___mTrans_9; }
	inline void set_mTrans_9(Transform_t3600365921 * value)
	{
		___mTrans_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_9), value);
	}

	inline static int32_t get_offset_of_mAnim_10() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___mAnim_10)); }
	inline Animation_t3648466861 * get_mAnim_10() const { return ___mAnim_10; }
	inline Animation_t3648466861 ** get_address_of_mAnim_10() { return &___mAnim_10; }
	inline void set_mAnim_10(Animation_t3648466861 * value)
	{
		___mAnim_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_10), value);
	}

	inline static int32_t get_offset_of_mRect_11() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___mRect_11)); }
	inline Rect_t2360479859  get_mRect_11() const { return ___mRect_11; }
	inline Rect_t2360479859 * get_address_of_mRect_11() { return &___mRect_11; }
	inline void set_mRect_11(Rect_t2360479859  value)
	{
		___mRect_11 = value;
	}

	inline static int32_t get_offset_of_mRoot_12() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___mRoot_12)); }
	inline UIRoot_t4022971450 * get_mRoot_12() const { return ___mRoot_12; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_12() { return &___mRoot_12; }
	inline void set_mRoot_12(UIRoot_t4022971450 * value)
	{
		___mRoot_12 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_12), value);
	}

	inline static int32_t get_offset_of_mStarted_13() { return static_cast<int32_t>(offsetof(UIAnchor_t2527798900, ___mStarted_13)); }
	inline bool get_mStarted_13() const { return ___mStarted_13; }
	inline bool* get_address_of_mStarted_13() { return &___mStarted_13; }
	inline void set_mStarted_13(bool value)
	{
		___mStarted_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANCHOR_T2527798900_H
#ifndef UIATLAS_T3195533529_H
#define UIATLAS_T3195533529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAtlas
struct  UIAtlas_t3195533529  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material UIAtlas::material
	Material_t340375123 * ___material_2;
	// System.Collections.Generic.List`1<UISpriteData> UIAtlas::mSprites
	List_1_t2372383268 * ___mSprites_3;
	// System.Single UIAtlas::mPixelSize
	float ___mPixelSize_4;
	// UIAtlas UIAtlas::mReplacement
	UIAtlas_t3195533529 * ___mReplacement_5;
	// UIAtlas/Coordinates UIAtlas::mCoordinates
	int32_t ___mCoordinates_6;
	// System.Collections.Generic.List`1<UIAtlas/Sprite> UIAtlas::sprites
	List_1_t72704565 * ___sprites_7;
	// System.Int32 UIAtlas::mPMA
	int32_t ___mPMA_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UIAtlas::mSpriteIndices
	Dictionary_2_t2736202052 * ___mSpriteIndices_9;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_mSprites_3() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___mSprites_3)); }
	inline List_1_t2372383268 * get_mSprites_3() const { return ___mSprites_3; }
	inline List_1_t2372383268 ** get_address_of_mSprites_3() { return &___mSprites_3; }
	inline void set_mSprites_3(List_1_t2372383268 * value)
	{
		___mSprites_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSprites_3), value);
	}

	inline static int32_t get_offset_of_mPixelSize_4() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___mPixelSize_4)); }
	inline float get_mPixelSize_4() const { return ___mPixelSize_4; }
	inline float* get_address_of_mPixelSize_4() { return &___mPixelSize_4; }
	inline void set_mPixelSize_4(float value)
	{
		___mPixelSize_4 = value;
	}

	inline static int32_t get_offset_of_mReplacement_5() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___mReplacement_5)); }
	inline UIAtlas_t3195533529 * get_mReplacement_5() const { return ___mReplacement_5; }
	inline UIAtlas_t3195533529 ** get_address_of_mReplacement_5() { return &___mReplacement_5; }
	inline void set_mReplacement_5(UIAtlas_t3195533529 * value)
	{
		___mReplacement_5 = value;
		Il2CppCodeGenWriteBarrier((&___mReplacement_5), value);
	}

	inline static int32_t get_offset_of_mCoordinates_6() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___mCoordinates_6)); }
	inline int32_t get_mCoordinates_6() const { return ___mCoordinates_6; }
	inline int32_t* get_address_of_mCoordinates_6() { return &___mCoordinates_6; }
	inline void set_mCoordinates_6(int32_t value)
	{
		___mCoordinates_6 = value;
	}

	inline static int32_t get_offset_of_sprites_7() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___sprites_7)); }
	inline List_1_t72704565 * get_sprites_7() const { return ___sprites_7; }
	inline List_1_t72704565 ** get_address_of_sprites_7() { return &___sprites_7; }
	inline void set_sprites_7(List_1_t72704565 * value)
	{
		___sprites_7 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_7), value);
	}

	inline static int32_t get_offset_of_mPMA_8() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___mPMA_8)); }
	inline int32_t get_mPMA_8() const { return ___mPMA_8; }
	inline int32_t* get_address_of_mPMA_8() { return &___mPMA_8; }
	inline void set_mPMA_8(int32_t value)
	{
		___mPMA_8 = value;
	}

	inline static int32_t get_offset_of_mSpriteIndices_9() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529, ___mSpriteIndices_9)); }
	inline Dictionary_2_t2736202052 * get_mSpriteIndices_9() const { return ___mSpriteIndices_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_mSpriteIndices_9() { return &___mSpriteIndices_9; }
	inline void set_mSpriteIndices_9(Dictionary_2_t2736202052 * value)
	{
		___mSpriteIndices_9 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteIndices_9), value);
	}
};

struct UIAtlas_t3195533529_StaticFields
{
public:
	// System.Comparison`1<UISpriteData> UIAtlas::<>f__am$cache0
	Comparison_1_t675239705 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(UIAtlas_t3195533529_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Comparison_1_t675239705 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Comparison_1_t675239705 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Comparison_1_t675239705 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIATLAS_T3195533529_H
#ifndef UIRECT_T2875960382_H
#define UIRECT_T2875960382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect
struct  UIRect_t2875960382  : public MonoBehaviour_t3962482529
{
public:
	// UIRect/AnchorPoint UIRect::leftAnchor
	AnchorPoint_t1754718329 * ___leftAnchor_2;
	// UIRect/AnchorPoint UIRect::rightAnchor
	AnchorPoint_t1754718329 * ___rightAnchor_3;
	// UIRect/AnchorPoint UIRect::bottomAnchor
	AnchorPoint_t1754718329 * ___bottomAnchor_4;
	// UIRect/AnchorPoint UIRect::topAnchor
	AnchorPoint_t1754718329 * ___topAnchor_5;
	// UIRect/AnchorUpdate UIRect::updateAnchors
	int32_t ___updateAnchors_6;
	// UnityEngine.GameObject UIRect::mGo
	GameObject_t1113636619 * ___mGo_7;
	// UnityEngine.Transform UIRect::mTrans
	Transform_t3600365921 * ___mTrans_8;
	// BetterList`1<UIRect> UIRect::mChildren
	BetterList_1_t2030980700 * ___mChildren_9;
	// System.Boolean UIRect::mChanged
	bool ___mChanged_10;
	// System.Boolean UIRect::mParentFound
	bool ___mParentFound_11;
	// System.Boolean UIRect::mUpdateAnchors
	bool ___mUpdateAnchors_12;
	// System.Int32 UIRect::mUpdateFrame
	int32_t ___mUpdateFrame_13;
	// System.Boolean UIRect::mAnchorsCached
	bool ___mAnchorsCached_14;
	// UIRoot UIRect::mRoot
	UIRoot_t4022971450 * ___mRoot_15;
	// UIRect UIRect::mParent
	UIRect_t2875960382 * ___mParent_16;
	// System.Boolean UIRect::mRootSet
	bool ___mRootSet_17;
	// UnityEngine.Camera UIRect::mCam
	Camera_t4157153871 * ___mCam_18;
	// System.Boolean UIRect::mStarted
	bool ___mStarted_19;
	// System.Single UIRect::finalAlpha
	float ___finalAlpha_20;

public:
	inline static int32_t get_offset_of_leftAnchor_2() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___leftAnchor_2)); }
	inline AnchorPoint_t1754718329 * get_leftAnchor_2() const { return ___leftAnchor_2; }
	inline AnchorPoint_t1754718329 ** get_address_of_leftAnchor_2() { return &___leftAnchor_2; }
	inline void set_leftAnchor_2(AnchorPoint_t1754718329 * value)
	{
		___leftAnchor_2 = value;
		Il2CppCodeGenWriteBarrier((&___leftAnchor_2), value);
	}

	inline static int32_t get_offset_of_rightAnchor_3() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___rightAnchor_3)); }
	inline AnchorPoint_t1754718329 * get_rightAnchor_3() const { return ___rightAnchor_3; }
	inline AnchorPoint_t1754718329 ** get_address_of_rightAnchor_3() { return &___rightAnchor_3; }
	inline void set_rightAnchor_3(AnchorPoint_t1754718329 * value)
	{
		___rightAnchor_3 = value;
		Il2CppCodeGenWriteBarrier((&___rightAnchor_3), value);
	}

	inline static int32_t get_offset_of_bottomAnchor_4() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___bottomAnchor_4)); }
	inline AnchorPoint_t1754718329 * get_bottomAnchor_4() const { return ___bottomAnchor_4; }
	inline AnchorPoint_t1754718329 ** get_address_of_bottomAnchor_4() { return &___bottomAnchor_4; }
	inline void set_bottomAnchor_4(AnchorPoint_t1754718329 * value)
	{
		___bottomAnchor_4 = value;
		Il2CppCodeGenWriteBarrier((&___bottomAnchor_4), value);
	}

	inline static int32_t get_offset_of_topAnchor_5() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___topAnchor_5)); }
	inline AnchorPoint_t1754718329 * get_topAnchor_5() const { return ___topAnchor_5; }
	inline AnchorPoint_t1754718329 ** get_address_of_topAnchor_5() { return &___topAnchor_5; }
	inline void set_topAnchor_5(AnchorPoint_t1754718329 * value)
	{
		___topAnchor_5 = value;
		Il2CppCodeGenWriteBarrier((&___topAnchor_5), value);
	}

	inline static int32_t get_offset_of_updateAnchors_6() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___updateAnchors_6)); }
	inline int32_t get_updateAnchors_6() const { return ___updateAnchors_6; }
	inline int32_t* get_address_of_updateAnchors_6() { return &___updateAnchors_6; }
	inline void set_updateAnchors_6(int32_t value)
	{
		___updateAnchors_6 = value;
	}

	inline static int32_t get_offset_of_mGo_7() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mGo_7)); }
	inline GameObject_t1113636619 * get_mGo_7() const { return ___mGo_7; }
	inline GameObject_t1113636619 ** get_address_of_mGo_7() { return &___mGo_7; }
	inline void set_mGo_7(GameObject_t1113636619 * value)
	{
		___mGo_7 = value;
		Il2CppCodeGenWriteBarrier((&___mGo_7), value);
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mTrans_8)); }
	inline Transform_t3600365921 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t3600365921 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t3600365921 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mChildren_9() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mChildren_9)); }
	inline BetterList_1_t2030980700 * get_mChildren_9() const { return ___mChildren_9; }
	inline BetterList_1_t2030980700 ** get_address_of_mChildren_9() { return &___mChildren_9; }
	inline void set_mChildren_9(BetterList_1_t2030980700 * value)
	{
		___mChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_9), value);
	}

	inline static int32_t get_offset_of_mChanged_10() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mChanged_10)); }
	inline bool get_mChanged_10() const { return ___mChanged_10; }
	inline bool* get_address_of_mChanged_10() { return &___mChanged_10; }
	inline void set_mChanged_10(bool value)
	{
		___mChanged_10 = value;
	}

	inline static int32_t get_offset_of_mParentFound_11() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mParentFound_11)); }
	inline bool get_mParentFound_11() const { return ___mParentFound_11; }
	inline bool* get_address_of_mParentFound_11() { return &___mParentFound_11; }
	inline void set_mParentFound_11(bool value)
	{
		___mParentFound_11 = value;
	}

	inline static int32_t get_offset_of_mUpdateAnchors_12() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mUpdateAnchors_12)); }
	inline bool get_mUpdateAnchors_12() const { return ___mUpdateAnchors_12; }
	inline bool* get_address_of_mUpdateAnchors_12() { return &___mUpdateAnchors_12; }
	inline void set_mUpdateAnchors_12(bool value)
	{
		___mUpdateAnchors_12 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrame_13() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mUpdateFrame_13)); }
	inline int32_t get_mUpdateFrame_13() const { return ___mUpdateFrame_13; }
	inline int32_t* get_address_of_mUpdateFrame_13() { return &___mUpdateFrame_13; }
	inline void set_mUpdateFrame_13(int32_t value)
	{
		___mUpdateFrame_13 = value;
	}

	inline static int32_t get_offset_of_mAnchorsCached_14() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mAnchorsCached_14)); }
	inline bool get_mAnchorsCached_14() const { return ___mAnchorsCached_14; }
	inline bool* get_address_of_mAnchorsCached_14() { return &___mAnchorsCached_14; }
	inline void set_mAnchorsCached_14(bool value)
	{
		___mAnchorsCached_14 = value;
	}

	inline static int32_t get_offset_of_mRoot_15() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mRoot_15)); }
	inline UIRoot_t4022971450 * get_mRoot_15() const { return ___mRoot_15; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_15() { return &___mRoot_15; }
	inline void set_mRoot_15(UIRoot_t4022971450 * value)
	{
		___mRoot_15 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_15), value);
	}

	inline static int32_t get_offset_of_mParent_16() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mParent_16)); }
	inline UIRect_t2875960382 * get_mParent_16() const { return ___mParent_16; }
	inline UIRect_t2875960382 ** get_address_of_mParent_16() { return &___mParent_16; }
	inline void set_mParent_16(UIRect_t2875960382 * value)
	{
		___mParent_16 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_16), value);
	}

	inline static int32_t get_offset_of_mRootSet_17() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mRootSet_17)); }
	inline bool get_mRootSet_17() const { return ___mRootSet_17; }
	inline bool* get_address_of_mRootSet_17() { return &___mRootSet_17; }
	inline void set_mRootSet_17(bool value)
	{
		___mRootSet_17 = value;
	}

	inline static int32_t get_offset_of_mCam_18() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mCam_18)); }
	inline Camera_t4157153871 * get_mCam_18() const { return ___mCam_18; }
	inline Camera_t4157153871 ** get_address_of_mCam_18() { return &___mCam_18; }
	inline void set_mCam_18(Camera_t4157153871 * value)
	{
		___mCam_18 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_18), value);
	}

	inline static int32_t get_offset_of_mStarted_19() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mStarted_19)); }
	inline bool get_mStarted_19() const { return ___mStarted_19; }
	inline bool* get_address_of_mStarted_19() { return &___mStarted_19; }
	inline void set_mStarted_19(bool value)
	{
		___mStarted_19 = value;
	}

	inline static int32_t get_offset_of_finalAlpha_20() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___finalAlpha_20)); }
	inline float get_finalAlpha_20() const { return ___finalAlpha_20; }
	inline float* get_address_of_finalAlpha_20() { return &___finalAlpha_20; }
	inline void set_finalAlpha_20(float value)
	{
		___finalAlpha_20 = value;
	}
};

struct UIRect_t2875960382_StaticFields
{
public:
	// UnityEngine.Vector3[] UIRect::mSides
	Vector3U5BU5D_t1718750761* ___mSides_21;

public:
	inline static int32_t get_offset_of_mSides_21() { return static_cast<int32_t>(offsetof(UIRect_t2875960382_StaticFields, ___mSides_21)); }
	inline Vector3U5BU5D_t1718750761* get_mSides_21() const { return ___mSides_21; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mSides_21() { return &___mSides_21; }
	inline void set_mSides_21(Vector3U5BU5D_t1718750761* value)
	{
		___mSides_21 = value;
		Il2CppCodeGenWriteBarrier((&___mSides_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIRECT_T2875960382_H
#ifndef UICAMERA_T1356438871_H
#define UICAMERA_T1356438871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera
struct  UICamera_t1356438871  : public MonoBehaviour_t3962482529
{
public:
	// UICamera/EventType UICamera::eventType
	int32_t ___eventType_12;
	// System.Boolean UICamera::eventsGoToColliders
	bool ___eventsGoToColliders_13;
	// UnityEngine.LayerMask UICamera::eventReceiverMask
	LayerMask_t3493934918  ___eventReceiverMask_14;
	// UICamera/ProcessEventsIn UICamera::processEventsIn
	int32_t ___processEventsIn_15;
	// System.Boolean UICamera::debug
	bool ___debug_16;
	// System.Boolean UICamera::useMouse
	bool ___useMouse_17;
	// System.Boolean UICamera::useTouch
	bool ___useTouch_18;
	// System.Boolean UICamera::allowMultiTouch
	bool ___allowMultiTouch_19;
	// System.Boolean UICamera::useKeyboard
	bool ___useKeyboard_20;
	// System.Boolean UICamera::useController
	bool ___useController_21;
	// System.Boolean UICamera::stickyTooltip
	bool ___stickyTooltip_22;
	// System.Single UICamera::tooltipDelay
	float ___tooltipDelay_23;
	// System.Boolean UICamera::longPressTooltip
	bool ___longPressTooltip_24;
	// System.Single UICamera::mouseDragThreshold
	float ___mouseDragThreshold_25;
	// System.Single UICamera::mouseClickThreshold
	float ___mouseClickThreshold_26;
	// System.Single UICamera::touchDragThreshold
	float ___touchDragThreshold_27;
	// System.Single UICamera::touchClickThreshold
	float ___touchClickThreshold_28;
	// System.Single UICamera::rangeDistance
	float ___rangeDistance_29;
	// System.String UICamera::horizontalAxisName
	String_t* ___horizontalAxisName_30;
	// System.String UICamera::verticalAxisName
	String_t* ___verticalAxisName_31;
	// System.String UICamera::horizontalPanAxisName
	String_t* ___horizontalPanAxisName_32;
	// System.String UICamera::verticalPanAxisName
	String_t* ___verticalPanAxisName_33;
	// System.String UICamera::scrollAxisName
	String_t* ___scrollAxisName_34;
	// System.Boolean UICamera::commandClick
	bool ___commandClick_35;
	// UnityEngine.KeyCode UICamera::submitKey0
	int32_t ___submitKey0_36;
	// UnityEngine.KeyCode UICamera::submitKey1
	int32_t ___submitKey1_37;
	// UnityEngine.KeyCode UICamera::cancelKey0
	int32_t ___cancelKey0_38;
	// UnityEngine.KeyCode UICamera::cancelKey1
	int32_t ___cancelKey1_39;
	// System.Boolean UICamera::autoHideCursor
	bool ___autoHideCursor_40;
	// UnityEngine.Camera UICamera::mCam
	Camera_t4157153871 * ___mCam_84;
	// System.Single UICamera::mNextRaycast
	float ___mNextRaycast_86;

public:
	inline static int32_t get_offset_of_eventType_12() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventType_12)); }
	inline int32_t get_eventType_12() const { return ___eventType_12; }
	inline int32_t* get_address_of_eventType_12() { return &___eventType_12; }
	inline void set_eventType_12(int32_t value)
	{
		___eventType_12 = value;
	}

	inline static int32_t get_offset_of_eventsGoToColliders_13() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventsGoToColliders_13)); }
	inline bool get_eventsGoToColliders_13() const { return ___eventsGoToColliders_13; }
	inline bool* get_address_of_eventsGoToColliders_13() { return &___eventsGoToColliders_13; }
	inline void set_eventsGoToColliders_13(bool value)
	{
		___eventsGoToColliders_13 = value;
	}

	inline static int32_t get_offset_of_eventReceiverMask_14() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventReceiverMask_14)); }
	inline LayerMask_t3493934918  get_eventReceiverMask_14() const { return ___eventReceiverMask_14; }
	inline LayerMask_t3493934918 * get_address_of_eventReceiverMask_14() { return &___eventReceiverMask_14; }
	inline void set_eventReceiverMask_14(LayerMask_t3493934918  value)
	{
		___eventReceiverMask_14 = value;
	}

	inline static int32_t get_offset_of_processEventsIn_15() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___processEventsIn_15)); }
	inline int32_t get_processEventsIn_15() const { return ___processEventsIn_15; }
	inline int32_t* get_address_of_processEventsIn_15() { return &___processEventsIn_15; }
	inline void set_processEventsIn_15(int32_t value)
	{
		___processEventsIn_15 = value;
	}

	inline static int32_t get_offset_of_debug_16() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___debug_16)); }
	inline bool get_debug_16() const { return ___debug_16; }
	inline bool* get_address_of_debug_16() { return &___debug_16; }
	inline void set_debug_16(bool value)
	{
		___debug_16 = value;
	}

	inline static int32_t get_offset_of_useMouse_17() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useMouse_17)); }
	inline bool get_useMouse_17() const { return ___useMouse_17; }
	inline bool* get_address_of_useMouse_17() { return &___useMouse_17; }
	inline void set_useMouse_17(bool value)
	{
		___useMouse_17 = value;
	}

	inline static int32_t get_offset_of_useTouch_18() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useTouch_18)); }
	inline bool get_useTouch_18() const { return ___useTouch_18; }
	inline bool* get_address_of_useTouch_18() { return &___useTouch_18; }
	inline void set_useTouch_18(bool value)
	{
		___useTouch_18 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_19() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___allowMultiTouch_19)); }
	inline bool get_allowMultiTouch_19() const { return ___allowMultiTouch_19; }
	inline bool* get_address_of_allowMultiTouch_19() { return &___allowMultiTouch_19; }
	inline void set_allowMultiTouch_19(bool value)
	{
		___allowMultiTouch_19 = value;
	}

	inline static int32_t get_offset_of_useKeyboard_20() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useKeyboard_20)); }
	inline bool get_useKeyboard_20() const { return ___useKeyboard_20; }
	inline bool* get_address_of_useKeyboard_20() { return &___useKeyboard_20; }
	inline void set_useKeyboard_20(bool value)
	{
		___useKeyboard_20 = value;
	}

	inline static int32_t get_offset_of_useController_21() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useController_21)); }
	inline bool get_useController_21() const { return ___useController_21; }
	inline bool* get_address_of_useController_21() { return &___useController_21; }
	inline void set_useController_21(bool value)
	{
		___useController_21 = value;
	}

	inline static int32_t get_offset_of_stickyTooltip_22() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___stickyTooltip_22)); }
	inline bool get_stickyTooltip_22() const { return ___stickyTooltip_22; }
	inline bool* get_address_of_stickyTooltip_22() { return &___stickyTooltip_22; }
	inline void set_stickyTooltip_22(bool value)
	{
		___stickyTooltip_22 = value;
	}

	inline static int32_t get_offset_of_tooltipDelay_23() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___tooltipDelay_23)); }
	inline float get_tooltipDelay_23() const { return ___tooltipDelay_23; }
	inline float* get_address_of_tooltipDelay_23() { return &___tooltipDelay_23; }
	inline void set_tooltipDelay_23(float value)
	{
		___tooltipDelay_23 = value;
	}

	inline static int32_t get_offset_of_longPressTooltip_24() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___longPressTooltip_24)); }
	inline bool get_longPressTooltip_24() const { return ___longPressTooltip_24; }
	inline bool* get_address_of_longPressTooltip_24() { return &___longPressTooltip_24; }
	inline void set_longPressTooltip_24(bool value)
	{
		___longPressTooltip_24 = value;
	}

	inline static int32_t get_offset_of_mouseDragThreshold_25() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mouseDragThreshold_25)); }
	inline float get_mouseDragThreshold_25() const { return ___mouseDragThreshold_25; }
	inline float* get_address_of_mouseDragThreshold_25() { return &___mouseDragThreshold_25; }
	inline void set_mouseDragThreshold_25(float value)
	{
		___mouseDragThreshold_25 = value;
	}

	inline static int32_t get_offset_of_mouseClickThreshold_26() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mouseClickThreshold_26)); }
	inline float get_mouseClickThreshold_26() const { return ___mouseClickThreshold_26; }
	inline float* get_address_of_mouseClickThreshold_26() { return &___mouseClickThreshold_26; }
	inline void set_mouseClickThreshold_26(float value)
	{
		___mouseClickThreshold_26 = value;
	}

	inline static int32_t get_offset_of_touchDragThreshold_27() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___touchDragThreshold_27)); }
	inline float get_touchDragThreshold_27() const { return ___touchDragThreshold_27; }
	inline float* get_address_of_touchDragThreshold_27() { return &___touchDragThreshold_27; }
	inline void set_touchDragThreshold_27(float value)
	{
		___touchDragThreshold_27 = value;
	}

	inline static int32_t get_offset_of_touchClickThreshold_28() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___touchClickThreshold_28)); }
	inline float get_touchClickThreshold_28() const { return ___touchClickThreshold_28; }
	inline float* get_address_of_touchClickThreshold_28() { return &___touchClickThreshold_28; }
	inline void set_touchClickThreshold_28(float value)
	{
		___touchClickThreshold_28 = value;
	}

	inline static int32_t get_offset_of_rangeDistance_29() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___rangeDistance_29)); }
	inline float get_rangeDistance_29() const { return ___rangeDistance_29; }
	inline float* get_address_of_rangeDistance_29() { return &___rangeDistance_29; }
	inline void set_rangeDistance_29(float value)
	{
		___rangeDistance_29 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_30() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___horizontalAxisName_30)); }
	inline String_t* get_horizontalAxisName_30() const { return ___horizontalAxisName_30; }
	inline String_t** get_address_of_horizontalAxisName_30() { return &___horizontalAxisName_30; }
	inline void set_horizontalAxisName_30(String_t* value)
	{
		___horizontalAxisName_30 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_30), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_31() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___verticalAxisName_31)); }
	inline String_t* get_verticalAxisName_31() const { return ___verticalAxisName_31; }
	inline String_t** get_address_of_verticalAxisName_31() { return &___verticalAxisName_31; }
	inline void set_verticalAxisName_31(String_t* value)
	{
		___verticalAxisName_31 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_31), value);
	}

	inline static int32_t get_offset_of_horizontalPanAxisName_32() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___horizontalPanAxisName_32)); }
	inline String_t* get_horizontalPanAxisName_32() const { return ___horizontalPanAxisName_32; }
	inline String_t** get_address_of_horizontalPanAxisName_32() { return &___horizontalPanAxisName_32; }
	inline void set_horizontalPanAxisName_32(String_t* value)
	{
		___horizontalPanAxisName_32 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalPanAxisName_32), value);
	}

	inline static int32_t get_offset_of_verticalPanAxisName_33() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___verticalPanAxisName_33)); }
	inline String_t* get_verticalPanAxisName_33() const { return ___verticalPanAxisName_33; }
	inline String_t** get_address_of_verticalPanAxisName_33() { return &___verticalPanAxisName_33; }
	inline void set_verticalPanAxisName_33(String_t* value)
	{
		___verticalPanAxisName_33 = value;
		Il2CppCodeGenWriteBarrier((&___verticalPanAxisName_33), value);
	}

	inline static int32_t get_offset_of_scrollAxisName_34() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___scrollAxisName_34)); }
	inline String_t* get_scrollAxisName_34() const { return ___scrollAxisName_34; }
	inline String_t** get_address_of_scrollAxisName_34() { return &___scrollAxisName_34; }
	inline void set_scrollAxisName_34(String_t* value)
	{
		___scrollAxisName_34 = value;
		Il2CppCodeGenWriteBarrier((&___scrollAxisName_34), value);
	}

	inline static int32_t get_offset_of_commandClick_35() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___commandClick_35)); }
	inline bool get_commandClick_35() const { return ___commandClick_35; }
	inline bool* get_address_of_commandClick_35() { return &___commandClick_35; }
	inline void set_commandClick_35(bool value)
	{
		___commandClick_35 = value;
	}

	inline static int32_t get_offset_of_submitKey0_36() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___submitKey0_36)); }
	inline int32_t get_submitKey0_36() const { return ___submitKey0_36; }
	inline int32_t* get_address_of_submitKey0_36() { return &___submitKey0_36; }
	inline void set_submitKey0_36(int32_t value)
	{
		___submitKey0_36 = value;
	}

	inline static int32_t get_offset_of_submitKey1_37() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___submitKey1_37)); }
	inline int32_t get_submitKey1_37() const { return ___submitKey1_37; }
	inline int32_t* get_address_of_submitKey1_37() { return &___submitKey1_37; }
	inline void set_submitKey1_37(int32_t value)
	{
		___submitKey1_37 = value;
	}

	inline static int32_t get_offset_of_cancelKey0_38() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___cancelKey0_38)); }
	inline int32_t get_cancelKey0_38() const { return ___cancelKey0_38; }
	inline int32_t* get_address_of_cancelKey0_38() { return &___cancelKey0_38; }
	inline void set_cancelKey0_38(int32_t value)
	{
		___cancelKey0_38 = value;
	}

	inline static int32_t get_offset_of_cancelKey1_39() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___cancelKey1_39)); }
	inline int32_t get_cancelKey1_39() const { return ___cancelKey1_39; }
	inline int32_t* get_address_of_cancelKey1_39() { return &___cancelKey1_39; }
	inline void set_cancelKey1_39(int32_t value)
	{
		___cancelKey1_39 = value;
	}

	inline static int32_t get_offset_of_autoHideCursor_40() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___autoHideCursor_40)); }
	inline bool get_autoHideCursor_40() const { return ___autoHideCursor_40; }
	inline bool* get_address_of_autoHideCursor_40() { return &___autoHideCursor_40; }
	inline void set_autoHideCursor_40(bool value)
	{
		___autoHideCursor_40 = value;
	}

	inline static int32_t get_offset_of_mCam_84() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mCam_84)); }
	inline Camera_t4157153871 * get_mCam_84() const { return ___mCam_84; }
	inline Camera_t4157153871 ** get_address_of_mCam_84() { return &___mCam_84; }
	inline void set_mCam_84(Camera_t4157153871 * value)
	{
		___mCam_84 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_84), value);
	}

	inline static int32_t get_offset_of_mNextRaycast_86() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mNextRaycast_86)); }
	inline float get_mNextRaycast_86() const { return ___mNextRaycast_86; }
	inline float* get_address_of_mNextRaycast_86() { return &___mNextRaycast_86; }
	inline void set_mNextRaycast_86(float value)
	{
		___mNextRaycast_86 = value;
	}
};

struct UICamera_t1356438871_StaticFields
{
public:
	// BetterList`1<UICamera> UICamera::list
	BetterList_1_t511459189 * ___list_2;
	// UICamera/GetKeyStateFunc UICamera::GetKeyDown
	GetKeyStateFunc_t2810275146 * ___GetKeyDown_3;
	// UICamera/GetKeyStateFunc UICamera::GetKeyUp
	GetKeyStateFunc_t2810275146 * ___GetKeyUp_4;
	// UICamera/GetKeyStateFunc UICamera::GetKey
	GetKeyStateFunc_t2810275146 * ___GetKey_5;
	// UICamera/GetAxisFunc UICamera::GetAxis
	GetAxisFunc_t2592608932 * ___GetAxis_6;
	// UICamera/GetAnyKeyFunc UICamera::GetAnyKeyDown
	GetAnyKeyFunc_t1761480072 * ___GetAnyKeyDown_7;
	// UICamera/GetMouseDelegate UICamera::GetMouse
	GetMouseDelegate_t662790529 * ___GetMouse_8;
	// UICamera/GetTouchDelegate UICamera::GetTouch
	GetTouchDelegate_t4218246285 * ___GetTouch_9;
	// UICamera/RemoveTouchDelegate UICamera::RemoveTouch
	RemoveTouchDelegate_t2508278027 * ___RemoveTouch_10;
	// UICamera/OnScreenResize UICamera::onScreenResize
	OnScreenResize_t2279991692 * ___onScreenResize_11;
	// UICamera/OnCustomInput UICamera::onCustomInput
	OnCustomInput_t3508588789 * ___onCustomInput_41;
	// System.Boolean UICamera::showTooltips
	bool ___showTooltips_42;
	// System.Boolean UICamera::ignoreAllEvents
	bool ___ignoreAllEvents_43;
	// System.Boolean UICamera::ignoreControllerInput
	bool ___ignoreControllerInput_44;
	// System.Boolean UICamera::mDisableController
	bool ___mDisableController_45;
	// UnityEngine.Vector2 UICamera::mLastPos
	Vector2_t2156229523  ___mLastPos_46;
	// UnityEngine.Vector3 UICamera::lastWorldPosition
	Vector3_t3722313464  ___lastWorldPosition_47;
	// UnityEngine.Ray UICamera::lastWorldRay
	Ray_t3785851493  ___lastWorldRay_48;
	// UnityEngine.RaycastHit UICamera::lastHit
	RaycastHit_t1056001966  ___lastHit_49;
	// UICamera UICamera::current
	UICamera_t1356438871 * ___current_50;
	// UnityEngine.Camera UICamera::currentCamera
	Camera_t4157153871 * ___currentCamera_51;
	// UICamera/OnSchemeChange UICamera::onSchemeChange
	OnSchemeChange_t1701155603 * ___onSchemeChange_52;
	// UICamera/ControlScheme UICamera::mLastScheme
	int32_t ___mLastScheme_53;
	// System.Int32 UICamera::currentTouchID
	int32_t ___currentTouchID_54;
	// UnityEngine.KeyCode UICamera::mCurrentKey
	int32_t ___mCurrentKey_55;
	// UICamera/MouseOrTouch UICamera::currentTouch
	MouseOrTouch_t3052596533 * ___currentTouch_56;
	// System.Boolean UICamera::mInputFocus
	bool ___mInputFocus_57;
	// UnityEngine.GameObject UICamera::mGenericHandler
	GameObject_t1113636619 * ___mGenericHandler_58;
	// UnityEngine.GameObject UICamera::fallThrough
	GameObject_t1113636619 * ___fallThrough_59;
	// UICamera/VoidDelegate UICamera::onClick
	VoidDelegate_t3100799918 * ___onClick_60;
	// UICamera/VoidDelegate UICamera::onDoubleClick
	VoidDelegate_t3100799918 * ___onDoubleClick_61;
	// UICamera/BoolDelegate UICamera::onHover
	BoolDelegate_t3825226153 * ___onHover_62;
	// UICamera/BoolDelegate UICamera::onPress
	BoolDelegate_t3825226153 * ___onPress_63;
	// UICamera/BoolDelegate UICamera::onSelect
	BoolDelegate_t3825226153 * ___onSelect_64;
	// UICamera/FloatDelegate UICamera::onScroll
	FloatDelegate_t906524069 * ___onScroll_65;
	// UICamera/VectorDelegate UICamera::onDrag
	VectorDelegate_t435795517 * ___onDrag_66;
	// UICamera/VoidDelegate UICamera::onDragStart
	VoidDelegate_t3100799918 * ___onDragStart_67;
	// UICamera/ObjectDelegate UICamera::onDragOver
	ObjectDelegate_t2041570719 * ___onDragOver_68;
	// UICamera/ObjectDelegate UICamera::onDragOut
	ObjectDelegate_t2041570719 * ___onDragOut_69;
	// UICamera/VoidDelegate UICamera::onDragEnd
	VoidDelegate_t3100799918 * ___onDragEnd_70;
	// UICamera/ObjectDelegate UICamera::onDrop
	ObjectDelegate_t2041570719 * ___onDrop_71;
	// UICamera/KeyCodeDelegate UICamera::onKey
	KeyCodeDelegate_t3064672302 * ___onKey_72;
	// UICamera/KeyCodeDelegate UICamera::onNavigate
	KeyCodeDelegate_t3064672302 * ___onNavigate_73;
	// UICamera/VectorDelegate UICamera::onPan
	VectorDelegate_t435795517 * ___onPan_74;
	// UICamera/BoolDelegate UICamera::onTooltip
	BoolDelegate_t3825226153 * ___onTooltip_75;
	// UICamera/MoveDelegate UICamera::onMouseMove
	MoveDelegate_t16019400 * ___onMouseMove_76;
	// UICamera/MouseOrTouch[] UICamera::mMouse
	MouseOrTouchU5BU5D_t3534648920* ___mMouse_77;
	// UICamera/MouseOrTouch UICamera::controller
	MouseOrTouch_t3052596533 * ___controller_78;
	// System.Collections.Generic.List`1<UICamera/MouseOrTouch> UICamera::activeTouches
	List_1_t229703979 * ___activeTouches_79;
	// System.Collections.Generic.List`1<System.Int32> UICamera::mTouchIDs
	List_1_t128053199 * ___mTouchIDs_80;
	// System.Int32 UICamera::mWidth
	int32_t ___mWidth_81;
	// System.Int32 UICamera::mHeight
	int32_t ___mHeight_82;
	// UnityEngine.GameObject UICamera::mTooltip
	GameObject_t1113636619 * ___mTooltip_83;
	// System.Single UICamera::mTooltipTime
	float ___mTooltipTime_85;
	// System.Boolean UICamera::isDragging
	bool ___isDragging_87;
	// System.Int32 UICamera::mLastInteractionCheck
	int32_t ___mLastInteractionCheck_88;
	// System.Boolean UICamera::mLastInteractionResult
	bool ___mLastInteractionResult_89;
	// System.Int32 UICamera::mLastFocusCheck
	int32_t ___mLastFocusCheck_90;
	// System.Boolean UICamera::mLastFocusResult
	bool ___mLastFocusResult_91;
	// System.Int32 UICamera::mLastOverCheck
	int32_t ___mLastOverCheck_92;
	// System.Boolean UICamera::mLastOverResult
	bool ___mLastOverResult_93;
	// UnityEngine.GameObject UICamera::mRayHitObject
	GameObject_t1113636619 * ___mRayHitObject_94;
	// UnityEngine.GameObject UICamera::mHover
	GameObject_t1113636619 * ___mHover_95;
	// UnityEngine.GameObject UICamera::mSelected
	GameObject_t1113636619 * ___mSelected_96;
	// UICamera/DepthEntry UICamera::mHit
	DepthEntry_t628749918  ___mHit_97;
	// BetterList`1<UICamera/DepthEntry> UICamera::mHits
	BetterList_1_t4078737532 * ___mHits_98;
	// UnityEngine.RaycastHit[] UICamera::mRayHits
	RaycastHitU5BU5D_t1690781147* ___mRayHits_99;
	// UnityEngine.Collider2D[] UICamera::mOverlap
	Collider2DU5BU5D_t1693969295* ___mOverlap_100;
	// UnityEngine.Plane UICamera::m2DPlane
	Plane_t1000493321  ___m2DPlane_101;
	// System.Single UICamera::mNextEvent
	float ___mNextEvent_102;
	// System.Int32 UICamera::mNotifying
	int32_t ___mNotifying_103;
	// System.Boolean UICamera::mUsingTouchEvents
	bool ___mUsingTouchEvents_104;
	// UICamera/GetTouchCountCallback UICamera::GetInputTouchCount
	GetTouchCountCallback_t3185863032 * ___GetInputTouchCount_105;
	// UICamera/GetTouchCallback UICamera::GetInputTouch
	GetTouchCallback_t97678626 * ___GetInputTouch_106;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache0
	CompareFunc_t2967399990 * ___U3CU3Ef__amU24cache0_107;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache1
	CompareFunc_t2967399990 * ___U3CU3Ef__amU24cache1_108;
	// BetterList`1/CompareFunc<UICamera> UICamera::<>f__mg$cache0
	CompareFunc_t3695088943 * ___U3CU3Ef__mgU24cache0_109;
	// BetterList`1/CompareFunc<UICamera> UICamera::<>f__mg$cache1
	CompareFunc_t3695088943 * ___U3CU3Ef__mgU24cache1_110;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___list_2)); }
	inline BetterList_1_t511459189 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t511459189 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t511459189 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of_GetKeyDown_3() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKeyDown_3)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKeyDown_3() const { return ___GetKeyDown_3; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKeyDown_3() { return &___GetKeyDown_3; }
	inline void set_GetKeyDown_3(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKeyDown_3 = value;
		Il2CppCodeGenWriteBarrier((&___GetKeyDown_3), value);
	}

	inline static int32_t get_offset_of_GetKeyUp_4() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKeyUp_4)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKeyUp_4() const { return ___GetKeyUp_4; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKeyUp_4() { return &___GetKeyUp_4; }
	inline void set_GetKeyUp_4(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKeyUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___GetKeyUp_4), value);
	}

	inline static int32_t get_offset_of_GetKey_5() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKey_5)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKey_5() const { return ___GetKey_5; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKey_5() { return &___GetKey_5; }
	inline void set_GetKey_5(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___GetKey_5), value);
	}

	inline static int32_t get_offset_of_GetAxis_6() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetAxis_6)); }
	inline GetAxisFunc_t2592608932 * get_GetAxis_6() const { return ___GetAxis_6; }
	inline GetAxisFunc_t2592608932 ** get_address_of_GetAxis_6() { return &___GetAxis_6; }
	inline void set_GetAxis_6(GetAxisFunc_t2592608932 * value)
	{
		___GetAxis_6 = value;
		Il2CppCodeGenWriteBarrier((&___GetAxis_6), value);
	}

	inline static int32_t get_offset_of_GetAnyKeyDown_7() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetAnyKeyDown_7)); }
	inline GetAnyKeyFunc_t1761480072 * get_GetAnyKeyDown_7() const { return ___GetAnyKeyDown_7; }
	inline GetAnyKeyFunc_t1761480072 ** get_address_of_GetAnyKeyDown_7() { return &___GetAnyKeyDown_7; }
	inline void set_GetAnyKeyDown_7(GetAnyKeyFunc_t1761480072 * value)
	{
		___GetAnyKeyDown_7 = value;
		Il2CppCodeGenWriteBarrier((&___GetAnyKeyDown_7), value);
	}

	inline static int32_t get_offset_of_GetMouse_8() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetMouse_8)); }
	inline GetMouseDelegate_t662790529 * get_GetMouse_8() const { return ___GetMouse_8; }
	inline GetMouseDelegate_t662790529 ** get_address_of_GetMouse_8() { return &___GetMouse_8; }
	inline void set_GetMouse_8(GetMouseDelegate_t662790529 * value)
	{
		___GetMouse_8 = value;
		Il2CppCodeGenWriteBarrier((&___GetMouse_8), value);
	}

	inline static int32_t get_offset_of_GetTouch_9() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetTouch_9)); }
	inline GetTouchDelegate_t4218246285 * get_GetTouch_9() const { return ___GetTouch_9; }
	inline GetTouchDelegate_t4218246285 ** get_address_of_GetTouch_9() { return &___GetTouch_9; }
	inline void set_GetTouch_9(GetTouchDelegate_t4218246285 * value)
	{
		___GetTouch_9 = value;
		Il2CppCodeGenWriteBarrier((&___GetTouch_9), value);
	}

	inline static int32_t get_offset_of_RemoveTouch_10() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___RemoveTouch_10)); }
	inline RemoveTouchDelegate_t2508278027 * get_RemoveTouch_10() const { return ___RemoveTouch_10; }
	inline RemoveTouchDelegate_t2508278027 ** get_address_of_RemoveTouch_10() { return &___RemoveTouch_10; }
	inline void set_RemoveTouch_10(RemoveTouchDelegate_t2508278027 * value)
	{
		___RemoveTouch_10 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveTouch_10), value);
	}

	inline static int32_t get_offset_of_onScreenResize_11() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onScreenResize_11)); }
	inline OnScreenResize_t2279991692 * get_onScreenResize_11() const { return ___onScreenResize_11; }
	inline OnScreenResize_t2279991692 ** get_address_of_onScreenResize_11() { return &___onScreenResize_11; }
	inline void set_onScreenResize_11(OnScreenResize_t2279991692 * value)
	{
		___onScreenResize_11 = value;
		Il2CppCodeGenWriteBarrier((&___onScreenResize_11), value);
	}

	inline static int32_t get_offset_of_onCustomInput_41() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onCustomInput_41)); }
	inline OnCustomInput_t3508588789 * get_onCustomInput_41() const { return ___onCustomInput_41; }
	inline OnCustomInput_t3508588789 ** get_address_of_onCustomInput_41() { return &___onCustomInput_41; }
	inline void set_onCustomInput_41(OnCustomInput_t3508588789 * value)
	{
		___onCustomInput_41 = value;
		Il2CppCodeGenWriteBarrier((&___onCustomInput_41), value);
	}

	inline static int32_t get_offset_of_showTooltips_42() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___showTooltips_42)); }
	inline bool get_showTooltips_42() const { return ___showTooltips_42; }
	inline bool* get_address_of_showTooltips_42() { return &___showTooltips_42; }
	inline void set_showTooltips_42(bool value)
	{
		___showTooltips_42 = value;
	}

	inline static int32_t get_offset_of_ignoreAllEvents_43() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___ignoreAllEvents_43)); }
	inline bool get_ignoreAllEvents_43() const { return ___ignoreAllEvents_43; }
	inline bool* get_address_of_ignoreAllEvents_43() { return &___ignoreAllEvents_43; }
	inline void set_ignoreAllEvents_43(bool value)
	{
		___ignoreAllEvents_43 = value;
	}

	inline static int32_t get_offset_of_ignoreControllerInput_44() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___ignoreControllerInput_44)); }
	inline bool get_ignoreControllerInput_44() const { return ___ignoreControllerInput_44; }
	inline bool* get_address_of_ignoreControllerInput_44() { return &___ignoreControllerInput_44; }
	inline void set_ignoreControllerInput_44(bool value)
	{
		___ignoreControllerInput_44 = value;
	}

	inline static int32_t get_offset_of_mDisableController_45() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mDisableController_45)); }
	inline bool get_mDisableController_45() const { return ___mDisableController_45; }
	inline bool* get_address_of_mDisableController_45() { return &___mDisableController_45; }
	inline void set_mDisableController_45(bool value)
	{
		___mDisableController_45 = value;
	}

	inline static int32_t get_offset_of_mLastPos_46() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastPos_46)); }
	inline Vector2_t2156229523  get_mLastPos_46() const { return ___mLastPos_46; }
	inline Vector2_t2156229523 * get_address_of_mLastPos_46() { return &___mLastPos_46; }
	inline void set_mLastPos_46(Vector2_t2156229523  value)
	{
		___mLastPos_46 = value;
	}

	inline static int32_t get_offset_of_lastWorldPosition_47() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastWorldPosition_47)); }
	inline Vector3_t3722313464  get_lastWorldPosition_47() const { return ___lastWorldPosition_47; }
	inline Vector3_t3722313464 * get_address_of_lastWorldPosition_47() { return &___lastWorldPosition_47; }
	inline void set_lastWorldPosition_47(Vector3_t3722313464  value)
	{
		___lastWorldPosition_47 = value;
	}

	inline static int32_t get_offset_of_lastWorldRay_48() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastWorldRay_48)); }
	inline Ray_t3785851493  get_lastWorldRay_48() const { return ___lastWorldRay_48; }
	inline Ray_t3785851493 * get_address_of_lastWorldRay_48() { return &___lastWorldRay_48; }
	inline void set_lastWorldRay_48(Ray_t3785851493  value)
	{
		___lastWorldRay_48 = value;
	}

	inline static int32_t get_offset_of_lastHit_49() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastHit_49)); }
	inline RaycastHit_t1056001966  get_lastHit_49() const { return ___lastHit_49; }
	inline RaycastHit_t1056001966 * get_address_of_lastHit_49() { return &___lastHit_49; }
	inline void set_lastHit_49(RaycastHit_t1056001966  value)
	{
		___lastHit_49 = value;
	}

	inline static int32_t get_offset_of_current_50() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___current_50)); }
	inline UICamera_t1356438871 * get_current_50() const { return ___current_50; }
	inline UICamera_t1356438871 ** get_address_of_current_50() { return &___current_50; }
	inline void set_current_50(UICamera_t1356438871 * value)
	{
		___current_50 = value;
		Il2CppCodeGenWriteBarrier((&___current_50), value);
	}

	inline static int32_t get_offset_of_currentCamera_51() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentCamera_51)); }
	inline Camera_t4157153871 * get_currentCamera_51() const { return ___currentCamera_51; }
	inline Camera_t4157153871 ** get_address_of_currentCamera_51() { return &___currentCamera_51; }
	inline void set_currentCamera_51(Camera_t4157153871 * value)
	{
		___currentCamera_51 = value;
		Il2CppCodeGenWriteBarrier((&___currentCamera_51), value);
	}

	inline static int32_t get_offset_of_onSchemeChange_52() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onSchemeChange_52)); }
	inline OnSchemeChange_t1701155603 * get_onSchemeChange_52() const { return ___onSchemeChange_52; }
	inline OnSchemeChange_t1701155603 ** get_address_of_onSchemeChange_52() { return &___onSchemeChange_52; }
	inline void set_onSchemeChange_52(OnSchemeChange_t1701155603 * value)
	{
		___onSchemeChange_52 = value;
		Il2CppCodeGenWriteBarrier((&___onSchemeChange_52), value);
	}

	inline static int32_t get_offset_of_mLastScheme_53() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastScheme_53)); }
	inline int32_t get_mLastScheme_53() const { return ___mLastScheme_53; }
	inline int32_t* get_address_of_mLastScheme_53() { return &___mLastScheme_53; }
	inline void set_mLastScheme_53(int32_t value)
	{
		___mLastScheme_53 = value;
	}

	inline static int32_t get_offset_of_currentTouchID_54() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentTouchID_54)); }
	inline int32_t get_currentTouchID_54() const { return ___currentTouchID_54; }
	inline int32_t* get_address_of_currentTouchID_54() { return &___currentTouchID_54; }
	inline void set_currentTouchID_54(int32_t value)
	{
		___currentTouchID_54 = value;
	}

	inline static int32_t get_offset_of_mCurrentKey_55() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mCurrentKey_55)); }
	inline int32_t get_mCurrentKey_55() const { return ___mCurrentKey_55; }
	inline int32_t* get_address_of_mCurrentKey_55() { return &___mCurrentKey_55; }
	inline void set_mCurrentKey_55(int32_t value)
	{
		___mCurrentKey_55 = value;
	}

	inline static int32_t get_offset_of_currentTouch_56() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentTouch_56)); }
	inline MouseOrTouch_t3052596533 * get_currentTouch_56() const { return ___currentTouch_56; }
	inline MouseOrTouch_t3052596533 ** get_address_of_currentTouch_56() { return &___currentTouch_56; }
	inline void set_currentTouch_56(MouseOrTouch_t3052596533 * value)
	{
		___currentTouch_56 = value;
		Il2CppCodeGenWriteBarrier((&___currentTouch_56), value);
	}

	inline static int32_t get_offset_of_mInputFocus_57() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mInputFocus_57)); }
	inline bool get_mInputFocus_57() const { return ___mInputFocus_57; }
	inline bool* get_address_of_mInputFocus_57() { return &___mInputFocus_57; }
	inline void set_mInputFocus_57(bool value)
	{
		___mInputFocus_57 = value;
	}

	inline static int32_t get_offset_of_mGenericHandler_58() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mGenericHandler_58)); }
	inline GameObject_t1113636619 * get_mGenericHandler_58() const { return ___mGenericHandler_58; }
	inline GameObject_t1113636619 ** get_address_of_mGenericHandler_58() { return &___mGenericHandler_58; }
	inline void set_mGenericHandler_58(GameObject_t1113636619 * value)
	{
		___mGenericHandler_58 = value;
		Il2CppCodeGenWriteBarrier((&___mGenericHandler_58), value);
	}

	inline static int32_t get_offset_of_fallThrough_59() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___fallThrough_59)); }
	inline GameObject_t1113636619 * get_fallThrough_59() const { return ___fallThrough_59; }
	inline GameObject_t1113636619 ** get_address_of_fallThrough_59() { return &___fallThrough_59; }
	inline void set_fallThrough_59(GameObject_t1113636619 * value)
	{
		___fallThrough_59 = value;
		Il2CppCodeGenWriteBarrier((&___fallThrough_59), value);
	}

	inline static int32_t get_offset_of_onClick_60() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onClick_60)); }
	inline VoidDelegate_t3100799918 * get_onClick_60() const { return ___onClick_60; }
	inline VoidDelegate_t3100799918 ** get_address_of_onClick_60() { return &___onClick_60; }
	inline void set_onClick_60(VoidDelegate_t3100799918 * value)
	{
		___onClick_60 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_60), value);
	}

	inline static int32_t get_offset_of_onDoubleClick_61() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDoubleClick_61)); }
	inline VoidDelegate_t3100799918 * get_onDoubleClick_61() const { return ___onDoubleClick_61; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDoubleClick_61() { return &___onDoubleClick_61; }
	inline void set_onDoubleClick_61(VoidDelegate_t3100799918 * value)
	{
		___onDoubleClick_61 = value;
		Il2CppCodeGenWriteBarrier((&___onDoubleClick_61), value);
	}

	inline static int32_t get_offset_of_onHover_62() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onHover_62)); }
	inline BoolDelegate_t3825226153 * get_onHover_62() const { return ___onHover_62; }
	inline BoolDelegate_t3825226153 ** get_address_of_onHover_62() { return &___onHover_62; }
	inline void set_onHover_62(BoolDelegate_t3825226153 * value)
	{
		___onHover_62 = value;
		Il2CppCodeGenWriteBarrier((&___onHover_62), value);
	}

	inline static int32_t get_offset_of_onPress_63() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onPress_63)); }
	inline BoolDelegate_t3825226153 * get_onPress_63() const { return ___onPress_63; }
	inline BoolDelegate_t3825226153 ** get_address_of_onPress_63() { return &___onPress_63; }
	inline void set_onPress_63(BoolDelegate_t3825226153 * value)
	{
		___onPress_63 = value;
		Il2CppCodeGenWriteBarrier((&___onPress_63), value);
	}

	inline static int32_t get_offset_of_onSelect_64() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onSelect_64)); }
	inline BoolDelegate_t3825226153 * get_onSelect_64() const { return ___onSelect_64; }
	inline BoolDelegate_t3825226153 ** get_address_of_onSelect_64() { return &___onSelect_64; }
	inline void set_onSelect_64(BoolDelegate_t3825226153 * value)
	{
		___onSelect_64 = value;
		Il2CppCodeGenWriteBarrier((&___onSelect_64), value);
	}

	inline static int32_t get_offset_of_onScroll_65() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onScroll_65)); }
	inline FloatDelegate_t906524069 * get_onScroll_65() const { return ___onScroll_65; }
	inline FloatDelegate_t906524069 ** get_address_of_onScroll_65() { return &___onScroll_65; }
	inline void set_onScroll_65(FloatDelegate_t906524069 * value)
	{
		___onScroll_65 = value;
		Il2CppCodeGenWriteBarrier((&___onScroll_65), value);
	}

	inline static int32_t get_offset_of_onDrag_66() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDrag_66)); }
	inline VectorDelegate_t435795517 * get_onDrag_66() const { return ___onDrag_66; }
	inline VectorDelegate_t435795517 ** get_address_of_onDrag_66() { return &___onDrag_66; }
	inline void set_onDrag_66(VectorDelegate_t435795517 * value)
	{
		___onDrag_66 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_66), value);
	}

	inline static int32_t get_offset_of_onDragStart_67() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragStart_67)); }
	inline VoidDelegate_t3100799918 * get_onDragStart_67() const { return ___onDragStart_67; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDragStart_67() { return &___onDragStart_67; }
	inline void set_onDragStart_67(VoidDelegate_t3100799918 * value)
	{
		___onDragStart_67 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStart_67), value);
	}

	inline static int32_t get_offset_of_onDragOver_68() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragOver_68)); }
	inline ObjectDelegate_t2041570719 * get_onDragOver_68() const { return ___onDragOver_68; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDragOver_68() { return &___onDragOver_68; }
	inline void set_onDragOver_68(ObjectDelegate_t2041570719 * value)
	{
		___onDragOver_68 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOver_68), value);
	}

	inline static int32_t get_offset_of_onDragOut_69() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragOut_69)); }
	inline ObjectDelegate_t2041570719 * get_onDragOut_69() const { return ___onDragOut_69; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDragOut_69() { return &___onDragOut_69; }
	inline void set_onDragOut_69(ObjectDelegate_t2041570719 * value)
	{
		___onDragOut_69 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOut_69), value);
	}

	inline static int32_t get_offset_of_onDragEnd_70() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragEnd_70)); }
	inline VoidDelegate_t3100799918 * get_onDragEnd_70() const { return ___onDragEnd_70; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDragEnd_70() { return &___onDragEnd_70; }
	inline void set_onDragEnd_70(VoidDelegate_t3100799918 * value)
	{
		___onDragEnd_70 = value;
		Il2CppCodeGenWriteBarrier((&___onDragEnd_70), value);
	}

	inline static int32_t get_offset_of_onDrop_71() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDrop_71)); }
	inline ObjectDelegate_t2041570719 * get_onDrop_71() const { return ___onDrop_71; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDrop_71() { return &___onDrop_71; }
	inline void set_onDrop_71(ObjectDelegate_t2041570719 * value)
	{
		___onDrop_71 = value;
		Il2CppCodeGenWriteBarrier((&___onDrop_71), value);
	}

	inline static int32_t get_offset_of_onKey_72() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onKey_72)); }
	inline KeyCodeDelegate_t3064672302 * get_onKey_72() const { return ___onKey_72; }
	inline KeyCodeDelegate_t3064672302 ** get_address_of_onKey_72() { return &___onKey_72; }
	inline void set_onKey_72(KeyCodeDelegate_t3064672302 * value)
	{
		___onKey_72 = value;
		Il2CppCodeGenWriteBarrier((&___onKey_72), value);
	}

	inline static int32_t get_offset_of_onNavigate_73() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onNavigate_73)); }
	inline KeyCodeDelegate_t3064672302 * get_onNavigate_73() const { return ___onNavigate_73; }
	inline KeyCodeDelegate_t3064672302 ** get_address_of_onNavigate_73() { return &___onNavigate_73; }
	inline void set_onNavigate_73(KeyCodeDelegate_t3064672302 * value)
	{
		___onNavigate_73 = value;
		Il2CppCodeGenWriteBarrier((&___onNavigate_73), value);
	}

	inline static int32_t get_offset_of_onPan_74() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onPan_74)); }
	inline VectorDelegate_t435795517 * get_onPan_74() const { return ___onPan_74; }
	inline VectorDelegate_t435795517 ** get_address_of_onPan_74() { return &___onPan_74; }
	inline void set_onPan_74(VectorDelegate_t435795517 * value)
	{
		___onPan_74 = value;
		Il2CppCodeGenWriteBarrier((&___onPan_74), value);
	}

	inline static int32_t get_offset_of_onTooltip_75() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onTooltip_75)); }
	inline BoolDelegate_t3825226153 * get_onTooltip_75() const { return ___onTooltip_75; }
	inline BoolDelegate_t3825226153 ** get_address_of_onTooltip_75() { return &___onTooltip_75; }
	inline void set_onTooltip_75(BoolDelegate_t3825226153 * value)
	{
		___onTooltip_75 = value;
		Il2CppCodeGenWriteBarrier((&___onTooltip_75), value);
	}

	inline static int32_t get_offset_of_onMouseMove_76() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onMouseMove_76)); }
	inline MoveDelegate_t16019400 * get_onMouseMove_76() const { return ___onMouseMove_76; }
	inline MoveDelegate_t16019400 ** get_address_of_onMouseMove_76() { return &___onMouseMove_76; }
	inline void set_onMouseMove_76(MoveDelegate_t16019400 * value)
	{
		___onMouseMove_76 = value;
		Il2CppCodeGenWriteBarrier((&___onMouseMove_76), value);
	}

	inline static int32_t get_offset_of_mMouse_77() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mMouse_77)); }
	inline MouseOrTouchU5BU5D_t3534648920* get_mMouse_77() const { return ___mMouse_77; }
	inline MouseOrTouchU5BU5D_t3534648920** get_address_of_mMouse_77() { return &___mMouse_77; }
	inline void set_mMouse_77(MouseOrTouchU5BU5D_t3534648920* value)
	{
		___mMouse_77 = value;
		Il2CppCodeGenWriteBarrier((&___mMouse_77), value);
	}

	inline static int32_t get_offset_of_controller_78() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___controller_78)); }
	inline MouseOrTouch_t3052596533 * get_controller_78() const { return ___controller_78; }
	inline MouseOrTouch_t3052596533 ** get_address_of_controller_78() { return &___controller_78; }
	inline void set_controller_78(MouseOrTouch_t3052596533 * value)
	{
		___controller_78 = value;
		Il2CppCodeGenWriteBarrier((&___controller_78), value);
	}

	inline static int32_t get_offset_of_activeTouches_79() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___activeTouches_79)); }
	inline List_1_t229703979 * get_activeTouches_79() const { return ___activeTouches_79; }
	inline List_1_t229703979 ** get_address_of_activeTouches_79() { return &___activeTouches_79; }
	inline void set_activeTouches_79(List_1_t229703979 * value)
	{
		___activeTouches_79 = value;
		Il2CppCodeGenWriteBarrier((&___activeTouches_79), value);
	}

	inline static int32_t get_offset_of_mTouchIDs_80() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTouchIDs_80)); }
	inline List_1_t128053199 * get_mTouchIDs_80() const { return ___mTouchIDs_80; }
	inline List_1_t128053199 ** get_address_of_mTouchIDs_80() { return &___mTouchIDs_80; }
	inline void set_mTouchIDs_80(List_1_t128053199 * value)
	{
		___mTouchIDs_80 = value;
		Il2CppCodeGenWriteBarrier((&___mTouchIDs_80), value);
	}

	inline static int32_t get_offset_of_mWidth_81() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mWidth_81)); }
	inline int32_t get_mWidth_81() const { return ___mWidth_81; }
	inline int32_t* get_address_of_mWidth_81() { return &___mWidth_81; }
	inline void set_mWidth_81(int32_t value)
	{
		___mWidth_81 = value;
	}

	inline static int32_t get_offset_of_mHeight_82() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHeight_82)); }
	inline int32_t get_mHeight_82() const { return ___mHeight_82; }
	inline int32_t* get_address_of_mHeight_82() { return &___mHeight_82; }
	inline void set_mHeight_82(int32_t value)
	{
		___mHeight_82 = value;
	}

	inline static int32_t get_offset_of_mTooltip_83() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTooltip_83)); }
	inline GameObject_t1113636619 * get_mTooltip_83() const { return ___mTooltip_83; }
	inline GameObject_t1113636619 ** get_address_of_mTooltip_83() { return &___mTooltip_83; }
	inline void set_mTooltip_83(GameObject_t1113636619 * value)
	{
		___mTooltip_83 = value;
		Il2CppCodeGenWriteBarrier((&___mTooltip_83), value);
	}

	inline static int32_t get_offset_of_mTooltipTime_85() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTooltipTime_85)); }
	inline float get_mTooltipTime_85() const { return ___mTooltipTime_85; }
	inline float* get_address_of_mTooltipTime_85() { return &___mTooltipTime_85; }
	inline void set_mTooltipTime_85(float value)
	{
		___mTooltipTime_85 = value;
	}

	inline static int32_t get_offset_of_isDragging_87() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___isDragging_87)); }
	inline bool get_isDragging_87() const { return ___isDragging_87; }
	inline bool* get_address_of_isDragging_87() { return &___isDragging_87; }
	inline void set_isDragging_87(bool value)
	{
		___isDragging_87 = value;
	}

	inline static int32_t get_offset_of_mLastInteractionCheck_88() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastInteractionCheck_88)); }
	inline int32_t get_mLastInteractionCheck_88() const { return ___mLastInteractionCheck_88; }
	inline int32_t* get_address_of_mLastInteractionCheck_88() { return &___mLastInteractionCheck_88; }
	inline void set_mLastInteractionCheck_88(int32_t value)
	{
		___mLastInteractionCheck_88 = value;
	}

	inline static int32_t get_offset_of_mLastInteractionResult_89() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastInteractionResult_89)); }
	inline bool get_mLastInteractionResult_89() const { return ___mLastInteractionResult_89; }
	inline bool* get_address_of_mLastInteractionResult_89() { return &___mLastInteractionResult_89; }
	inline void set_mLastInteractionResult_89(bool value)
	{
		___mLastInteractionResult_89 = value;
	}

	inline static int32_t get_offset_of_mLastFocusCheck_90() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastFocusCheck_90)); }
	inline int32_t get_mLastFocusCheck_90() const { return ___mLastFocusCheck_90; }
	inline int32_t* get_address_of_mLastFocusCheck_90() { return &___mLastFocusCheck_90; }
	inline void set_mLastFocusCheck_90(int32_t value)
	{
		___mLastFocusCheck_90 = value;
	}

	inline static int32_t get_offset_of_mLastFocusResult_91() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastFocusResult_91)); }
	inline bool get_mLastFocusResult_91() const { return ___mLastFocusResult_91; }
	inline bool* get_address_of_mLastFocusResult_91() { return &___mLastFocusResult_91; }
	inline void set_mLastFocusResult_91(bool value)
	{
		___mLastFocusResult_91 = value;
	}

	inline static int32_t get_offset_of_mLastOverCheck_92() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastOverCheck_92)); }
	inline int32_t get_mLastOverCheck_92() const { return ___mLastOverCheck_92; }
	inline int32_t* get_address_of_mLastOverCheck_92() { return &___mLastOverCheck_92; }
	inline void set_mLastOverCheck_92(int32_t value)
	{
		___mLastOverCheck_92 = value;
	}

	inline static int32_t get_offset_of_mLastOverResult_93() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastOverResult_93)); }
	inline bool get_mLastOverResult_93() const { return ___mLastOverResult_93; }
	inline bool* get_address_of_mLastOverResult_93() { return &___mLastOverResult_93; }
	inline void set_mLastOverResult_93(bool value)
	{
		___mLastOverResult_93 = value;
	}

	inline static int32_t get_offset_of_mRayHitObject_94() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mRayHitObject_94)); }
	inline GameObject_t1113636619 * get_mRayHitObject_94() const { return ___mRayHitObject_94; }
	inline GameObject_t1113636619 ** get_address_of_mRayHitObject_94() { return &___mRayHitObject_94; }
	inline void set_mRayHitObject_94(GameObject_t1113636619 * value)
	{
		___mRayHitObject_94 = value;
		Il2CppCodeGenWriteBarrier((&___mRayHitObject_94), value);
	}

	inline static int32_t get_offset_of_mHover_95() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHover_95)); }
	inline GameObject_t1113636619 * get_mHover_95() const { return ___mHover_95; }
	inline GameObject_t1113636619 ** get_address_of_mHover_95() { return &___mHover_95; }
	inline void set_mHover_95(GameObject_t1113636619 * value)
	{
		___mHover_95 = value;
		Il2CppCodeGenWriteBarrier((&___mHover_95), value);
	}

	inline static int32_t get_offset_of_mSelected_96() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mSelected_96)); }
	inline GameObject_t1113636619 * get_mSelected_96() const { return ___mSelected_96; }
	inline GameObject_t1113636619 ** get_address_of_mSelected_96() { return &___mSelected_96; }
	inline void set_mSelected_96(GameObject_t1113636619 * value)
	{
		___mSelected_96 = value;
		Il2CppCodeGenWriteBarrier((&___mSelected_96), value);
	}

	inline static int32_t get_offset_of_mHit_97() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHit_97)); }
	inline DepthEntry_t628749918  get_mHit_97() const { return ___mHit_97; }
	inline DepthEntry_t628749918 * get_address_of_mHit_97() { return &___mHit_97; }
	inline void set_mHit_97(DepthEntry_t628749918  value)
	{
		___mHit_97 = value;
	}

	inline static int32_t get_offset_of_mHits_98() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHits_98)); }
	inline BetterList_1_t4078737532 * get_mHits_98() const { return ___mHits_98; }
	inline BetterList_1_t4078737532 ** get_address_of_mHits_98() { return &___mHits_98; }
	inline void set_mHits_98(BetterList_1_t4078737532 * value)
	{
		___mHits_98 = value;
		Il2CppCodeGenWriteBarrier((&___mHits_98), value);
	}

	inline static int32_t get_offset_of_mRayHits_99() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mRayHits_99)); }
	inline RaycastHitU5BU5D_t1690781147* get_mRayHits_99() const { return ___mRayHits_99; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_mRayHits_99() { return &___mRayHits_99; }
	inline void set_mRayHits_99(RaycastHitU5BU5D_t1690781147* value)
	{
		___mRayHits_99 = value;
		Il2CppCodeGenWriteBarrier((&___mRayHits_99), value);
	}

	inline static int32_t get_offset_of_mOverlap_100() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mOverlap_100)); }
	inline Collider2DU5BU5D_t1693969295* get_mOverlap_100() const { return ___mOverlap_100; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_mOverlap_100() { return &___mOverlap_100; }
	inline void set_mOverlap_100(Collider2DU5BU5D_t1693969295* value)
	{
		___mOverlap_100 = value;
		Il2CppCodeGenWriteBarrier((&___mOverlap_100), value);
	}

	inline static int32_t get_offset_of_m2DPlane_101() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___m2DPlane_101)); }
	inline Plane_t1000493321  get_m2DPlane_101() const { return ___m2DPlane_101; }
	inline Plane_t1000493321 * get_address_of_m2DPlane_101() { return &___m2DPlane_101; }
	inline void set_m2DPlane_101(Plane_t1000493321  value)
	{
		___m2DPlane_101 = value;
	}

	inline static int32_t get_offset_of_mNextEvent_102() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mNextEvent_102)); }
	inline float get_mNextEvent_102() const { return ___mNextEvent_102; }
	inline float* get_address_of_mNextEvent_102() { return &___mNextEvent_102; }
	inline void set_mNextEvent_102(float value)
	{
		___mNextEvent_102 = value;
	}

	inline static int32_t get_offset_of_mNotifying_103() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mNotifying_103)); }
	inline int32_t get_mNotifying_103() const { return ___mNotifying_103; }
	inline int32_t* get_address_of_mNotifying_103() { return &___mNotifying_103; }
	inline void set_mNotifying_103(int32_t value)
	{
		___mNotifying_103 = value;
	}

	inline static int32_t get_offset_of_mUsingTouchEvents_104() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mUsingTouchEvents_104)); }
	inline bool get_mUsingTouchEvents_104() const { return ___mUsingTouchEvents_104; }
	inline bool* get_address_of_mUsingTouchEvents_104() { return &___mUsingTouchEvents_104; }
	inline void set_mUsingTouchEvents_104(bool value)
	{
		___mUsingTouchEvents_104 = value;
	}

	inline static int32_t get_offset_of_GetInputTouchCount_105() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetInputTouchCount_105)); }
	inline GetTouchCountCallback_t3185863032 * get_GetInputTouchCount_105() const { return ___GetInputTouchCount_105; }
	inline GetTouchCountCallback_t3185863032 ** get_address_of_GetInputTouchCount_105() { return &___GetInputTouchCount_105; }
	inline void set_GetInputTouchCount_105(GetTouchCountCallback_t3185863032 * value)
	{
		___GetInputTouchCount_105 = value;
		Il2CppCodeGenWriteBarrier((&___GetInputTouchCount_105), value);
	}

	inline static int32_t get_offset_of_GetInputTouch_106() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetInputTouch_106)); }
	inline GetTouchCallback_t97678626 * get_GetInputTouch_106() const { return ___GetInputTouch_106; }
	inline GetTouchCallback_t97678626 ** get_address_of_GetInputTouch_106() { return &___GetInputTouch_106; }
	inline void set_GetInputTouch_106(GetTouchCallback_t97678626 * value)
	{
		___GetInputTouch_106 = value;
		Il2CppCodeGenWriteBarrier((&___GetInputTouch_106), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_107() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__amU24cache0_107)); }
	inline CompareFunc_t2967399990 * get_U3CU3Ef__amU24cache0_107() const { return ___U3CU3Ef__amU24cache0_107; }
	inline CompareFunc_t2967399990 ** get_address_of_U3CU3Ef__amU24cache0_107() { return &___U3CU3Ef__amU24cache0_107; }
	inline void set_U3CU3Ef__amU24cache0_107(CompareFunc_t2967399990 * value)
	{
		___U3CU3Ef__amU24cache0_107 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_107), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_108() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__amU24cache1_108)); }
	inline CompareFunc_t2967399990 * get_U3CU3Ef__amU24cache1_108() const { return ___U3CU3Ef__amU24cache1_108; }
	inline CompareFunc_t2967399990 ** get_address_of_U3CU3Ef__amU24cache1_108() { return &___U3CU3Ef__amU24cache1_108; }
	inline void set_U3CU3Ef__amU24cache1_108(CompareFunc_t2967399990 * value)
	{
		___U3CU3Ef__amU24cache1_108 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_108), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_109() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache0_109)); }
	inline CompareFunc_t3695088943 * get_U3CU3Ef__mgU24cache0_109() const { return ___U3CU3Ef__mgU24cache0_109; }
	inline CompareFunc_t3695088943 ** get_address_of_U3CU3Ef__mgU24cache0_109() { return &___U3CU3Ef__mgU24cache0_109; }
	inline void set_U3CU3Ef__mgU24cache0_109(CompareFunc_t3695088943 * value)
	{
		___U3CU3Ef__mgU24cache0_109 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_109), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_110() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache1_110)); }
	inline CompareFunc_t3695088943 * get_U3CU3Ef__mgU24cache1_110() const { return ___U3CU3Ef__mgU24cache1_110; }
	inline CompareFunc_t3695088943 ** get_address_of_U3CU3Ef__mgU24cache1_110() { return &___U3CU3Ef__mgU24cache1_110; }
	inline void set_U3CU3Ef__mgU24cache1_110(CompareFunc_t3695088943 * value)
	{
		___U3CU3Ef__mgU24cache1_110 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_110), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICAMERA_T1356438871_H
#ifndef TWEENVOLUME_T3718612080_H
#define TWEENVOLUME_T3718612080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenVolume
struct  TweenVolume_t3718612080  : public UITweener_t260334902
{
public:
	// System.Single TweenVolume::from
	float ___from_22;
	// System.Single TweenVolume::to
	float ___to_23;
	// UnityEngine.AudioSource TweenVolume::mSource
	AudioSource_t3935305588 * ___mSource_24;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenVolume_t3718612080, ___from_22)); }
	inline float get_from_22() const { return ___from_22; }
	inline float* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(float value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenVolume_t3718612080, ___to_23)); }
	inline float get_to_23() const { return ___to_23; }
	inline float* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(float value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_mSource_24() { return static_cast<int32_t>(offsetof(TweenVolume_t3718612080, ___mSource_24)); }
	inline AudioSource_t3935305588 * get_mSource_24() const { return ___mSource_24; }
	inline AudioSource_t3935305588 ** get_address_of_mSource_24() { return &___mSource_24; }
	inline void set_mSource_24(AudioSource_t3935305588 * value)
	{
		___mSource_24 = value;
		Il2CppCodeGenWriteBarrier((&___mSource_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENVOLUME_T3718612080_H
#ifndef TWEENTRANSFORM_T1195296467_H
#define TWEENTRANSFORM_T1195296467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenTransform
struct  TweenTransform_t1195296467  : public UITweener_t260334902
{
public:
	// UnityEngine.Transform TweenTransform::from
	Transform_t3600365921 * ___from_22;
	// UnityEngine.Transform TweenTransform::to
	Transform_t3600365921 * ___to_23;
	// System.Boolean TweenTransform::parentWhenFinished
	bool ___parentWhenFinished_24;
	// UnityEngine.Transform TweenTransform::mTrans
	Transform_t3600365921 * ___mTrans_25;
	// UnityEngine.Vector3 TweenTransform::mPos
	Vector3_t3722313464  ___mPos_26;
	// UnityEngine.Quaternion TweenTransform::mRot
	Quaternion_t2301928331  ___mRot_27;
	// UnityEngine.Vector3 TweenTransform::mScale
	Vector3_t3722313464  ___mScale_28;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenTransform_t1195296467, ___from_22)); }
	inline Transform_t3600365921 * get_from_22() const { return ___from_22; }
	inline Transform_t3600365921 ** get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(Transform_t3600365921 * value)
	{
		___from_22 = value;
		Il2CppCodeGenWriteBarrier((&___from_22), value);
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenTransform_t1195296467, ___to_23)); }
	inline Transform_t3600365921 * get_to_23() const { return ___to_23; }
	inline Transform_t3600365921 ** get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(Transform_t3600365921 * value)
	{
		___to_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_23), value);
	}

	inline static int32_t get_offset_of_parentWhenFinished_24() { return static_cast<int32_t>(offsetof(TweenTransform_t1195296467, ___parentWhenFinished_24)); }
	inline bool get_parentWhenFinished_24() const { return ___parentWhenFinished_24; }
	inline bool* get_address_of_parentWhenFinished_24() { return &___parentWhenFinished_24; }
	inline void set_parentWhenFinished_24(bool value)
	{
		___parentWhenFinished_24 = value;
	}

	inline static int32_t get_offset_of_mTrans_25() { return static_cast<int32_t>(offsetof(TweenTransform_t1195296467, ___mTrans_25)); }
	inline Transform_t3600365921 * get_mTrans_25() const { return ___mTrans_25; }
	inline Transform_t3600365921 ** get_address_of_mTrans_25() { return &___mTrans_25; }
	inline void set_mTrans_25(Transform_t3600365921 * value)
	{
		___mTrans_25 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_25), value);
	}

	inline static int32_t get_offset_of_mPos_26() { return static_cast<int32_t>(offsetof(TweenTransform_t1195296467, ___mPos_26)); }
	inline Vector3_t3722313464  get_mPos_26() const { return ___mPos_26; }
	inline Vector3_t3722313464 * get_address_of_mPos_26() { return &___mPos_26; }
	inline void set_mPos_26(Vector3_t3722313464  value)
	{
		___mPos_26 = value;
	}

	inline static int32_t get_offset_of_mRot_27() { return static_cast<int32_t>(offsetof(TweenTransform_t1195296467, ___mRot_27)); }
	inline Quaternion_t2301928331  get_mRot_27() const { return ___mRot_27; }
	inline Quaternion_t2301928331 * get_address_of_mRot_27() { return &___mRot_27; }
	inline void set_mRot_27(Quaternion_t2301928331  value)
	{
		___mRot_27 = value;
	}

	inline static int32_t get_offset_of_mScale_28() { return static_cast<int32_t>(offsetof(TweenTransform_t1195296467, ___mScale_28)); }
	inline Vector3_t3722313464  get_mScale_28() const { return ___mScale_28; }
	inline Vector3_t3722313464 * get_address_of_mScale_28() { return &___mScale_28; }
	inline void set_mScale_28(Vector3_t3722313464  value)
	{
		___mScale_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTRANSFORM_T1195296467_H
#ifndef TWEENSCALE_T2539309033_H
#define TWEENSCALE_T2539309033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenScale
struct  TweenScale_t2539309033  : public UITweener_t260334902
{
public:
	// UnityEngine.Vector3 TweenScale::from
	Vector3_t3722313464  ___from_22;
	// UnityEngine.Vector3 TweenScale::to
	Vector3_t3722313464  ___to_23;
	// System.Boolean TweenScale::updateTable
	bool ___updateTable_24;
	// UnityEngine.Transform TweenScale::mTrans
	Transform_t3600365921 * ___mTrans_25;
	// UITable TweenScale::mTable
	UITable_t3168834800 * ___mTable_26;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenScale_t2539309033, ___from_22)); }
	inline Vector3_t3722313464  get_from_22() const { return ___from_22; }
	inline Vector3_t3722313464 * get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(Vector3_t3722313464  value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenScale_t2539309033, ___to_23)); }
	inline Vector3_t3722313464  get_to_23() const { return ___to_23; }
	inline Vector3_t3722313464 * get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(Vector3_t3722313464  value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_updateTable_24() { return static_cast<int32_t>(offsetof(TweenScale_t2539309033, ___updateTable_24)); }
	inline bool get_updateTable_24() const { return ___updateTable_24; }
	inline bool* get_address_of_updateTable_24() { return &___updateTable_24; }
	inline void set_updateTable_24(bool value)
	{
		___updateTable_24 = value;
	}

	inline static int32_t get_offset_of_mTrans_25() { return static_cast<int32_t>(offsetof(TweenScale_t2539309033, ___mTrans_25)); }
	inline Transform_t3600365921 * get_mTrans_25() const { return ___mTrans_25; }
	inline Transform_t3600365921 ** get_address_of_mTrans_25() { return &___mTrans_25; }
	inline void set_mTrans_25(Transform_t3600365921 * value)
	{
		___mTrans_25 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_25), value);
	}

	inline static int32_t get_offset_of_mTable_26() { return static_cast<int32_t>(offsetof(TweenScale_t2539309033, ___mTable_26)); }
	inline UITable_t3168834800 * get_mTable_26() const { return ___mTable_26; }
	inline UITable_t3168834800 ** get_address_of_mTable_26() { return &___mTable_26; }
	inline void set_mTable_26(UITable_t3168834800 * value)
	{
		___mTable_26 = value;
		Il2CppCodeGenWriteBarrier((&___mTable_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSCALE_T2539309033_H
#ifndef TWEENROTATION_T3072670746_H
#define TWEENROTATION_T3072670746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenRotation
struct  TweenRotation_t3072670746  : public UITweener_t260334902
{
public:
	// UnityEngine.Vector3 TweenRotation::from
	Vector3_t3722313464  ___from_22;
	// UnityEngine.Vector3 TweenRotation::to
	Vector3_t3722313464  ___to_23;
	// System.Boolean TweenRotation::quaternionLerp
	bool ___quaternionLerp_24;
	// UnityEngine.Transform TweenRotation::mTrans
	Transform_t3600365921 * ___mTrans_25;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenRotation_t3072670746, ___from_22)); }
	inline Vector3_t3722313464  get_from_22() const { return ___from_22; }
	inline Vector3_t3722313464 * get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(Vector3_t3722313464  value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenRotation_t3072670746, ___to_23)); }
	inline Vector3_t3722313464  get_to_23() const { return ___to_23; }
	inline Vector3_t3722313464 * get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(Vector3_t3722313464  value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_quaternionLerp_24() { return static_cast<int32_t>(offsetof(TweenRotation_t3072670746, ___quaternionLerp_24)); }
	inline bool get_quaternionLerp_24() const { return ___quaternionLerp_24; }
	inline bool* get_address_of_quaternionLerp_24() { return &___quaternionLerp_24; }
	inline void set_quaternionLerp_24(bool value)
	{
		___quaternionLerp_24 = value;
	}

	inline static int32_t get_offset_of_mTrans_25() { return static_cast<int32_t>(offsetof(TweenRotation_t3072670746, ___mTrans_25)); }
	inline Transform_t3600365921 * get_mTrans_25() const { return ___mTrans_25; }
	inline Transform_t3600365921 ** get_address_of_mTrans_25() { return &___mTrans_25; }
	inline void set_mTrans_25(Transform_t3600365921 * value)
	{
		___mTrans_25 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENROTATION_T3072670746_H
#ifndef TWEENPOSITION_T1378762002_H
#define TWEENPOSITION_T1378762002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenPosition
struct  TweenPosition_t1378762002  : public UITweener_t260334902
{
public:
	// UnityEngine.Vector3 TweenPosition::from
	Vector3_t3722313464  ___from_22;
	// UnityEngine.Vector3 TweenPosition::to
	Vector3_t3722313464  ___to_23;
	// System.Boolean TweenPosition::worldSpace
	bool ___worldSpace_24;
	// UnityEngine.Transform TweenPosition::mTrans
	Transform_t3600365921 * ___mTrans_25;
	// UIRect TweenPosition::mRect
	UIRect_t2875960382 * ___mRect_26;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenPosition_t1378762002, ___from_22)); }
	inline Vector3_t3722313464  get_from_22() const { return ___from_22; }
	inline Vector3_t3722313464 * get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(Vector3_t3722313464  value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenPosition_t1378762002, ___to_23)); }
	inline Vector3_t3722313464  get_to_23() const { return ___to_23; }
	inline Vector3_t3722313464 * get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(Vector3_t3722313464  value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_worldSpace_24() { return static_cast<int32_t>(offsetof(TweenPosition_t1378762002, ___worldSpace_24)); }
	inline bool get_worldSpace_24() const { return ___worldSpace_24; }
	inline bool* get_address_of_worldSpace_24() { return &___worldSpace_24; }
	inline void set_worldSpace_24(bool value)
	{
		___worldSpace_24 = value;
	}

	inline static int32_t get_offset_of_mTrans_25() { return static_cast<int32_t>(offsetof(TweenPosition_t1378762002, ___mTrans_25)); }
	inline Transform_t3600365921 * get_mTrans_25() const { return ___mTrans_25; }
	inline Transform_t3600365921 ** get_address_of_mTrans_25() { return &___mTrans_25; }
	inline void set_mTrans_25(Transform_t3600365921 * value)
	{
		___mTrans_25 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_25), value);
	}

	inline static int32_t get_offset_of_mRect_26() { return static_cast<int32_t>(offsetof(TweenPosition_t1378762002, ___mRect_26)); }
	inline UIRect_t2875960382 * get_mRect_26() const { return ___mRect_26; }
	inline UIRect_t2875960382 ** get_address_of_mRect_26() { return &___mRect_26; }
	inline void set_mRect_26(UIRect_t2875960382 * value)
	{
		___mRect_26 = value;
		Il2CppCodeGenWriteBarrier((&___mRect_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENPOSITION_T1378762002_H
#ifndef TWEENORTHOSIZE_T2102937296_H
#define TWEENORTHOSIZE_T2102937296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenOrthoSize
struct  TweenOrthoSize_t2102937296  : public UITweener_t260334902
{
public:
	// System.Single TweenOrthoSize::from
	float ___from_22;
	// System.Single TweenOrthoSize::to
	float ___to_23;
	// UnityEngine.Camera TweenOrthoSize::mCam
	Camera_t4157153871 * ___mCam_24;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenOrthoSize_t2102937296, ___from_22)); }
	inline float get_from_22() const { return ___from_22; }
	inline float* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(float value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenOrthoSize_t2102937296, ___to_23)); }
	inline float get_to_23() const { return ___to_23; }
	inline float* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(float value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_mCam_24() { return static_cast<int32_t>(offsetof(TweenOrthoSize_t2102937296, ___mCam_24)); }
	inline Camera_t4157153871 * get_mCam_24() const { return ___mCam_24; }
	inline Camera_t4157153871 ** get_address_of_mCam_24() { return &___mCam_24; }
	inline void set_mCam_24(Camera_t4157153871 * value)
	{
		___mCam_24 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENORTHOSIZE_T2102937296_H
#ifndef TWEENHEIGHT_T4009371699_H
#define TWEENHEIGHT_T4009371699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenHeight
struct  TweenHeight_t4009371699  : public UITweener_t260334902
{
public:
	// System.Int32 TweenHeight::from
	int32_t ___from_22;
	// System.Int32 TweenHeight::to
	int32_t ___to_23;
	// System.Boolean TweenHeight::updateTable
	bool ___updateTable_24;
	// UIWidget TweenHeight::mWidget
	UIWidget_t3538521925 * ___mWidget_25;
	// UITable TweenHeight::mTable
	UITable_t3168834800 * ___mTable_26;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenHeight_t4009371699, ___from_22)); }
	inline int32_t get_from_22() const { return ___from_22; }
	inline int32_t* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(int32_t value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenHeight_t4009371699, ___to_23)); }
	inline int32_t get_to_23() const { return ___to_23; }
	inline int32_t* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(int32_t value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_updateTable_24() { return static_cast<int32_t>(offsetof(TweenHeight_t4009371699, ___updateTable_24)); }
	inline bool get_updateTable_24() const { return ___updateTable_24; }
	inline bool* get_address_of_updateTable_24() { return &___updateTable_24; }
	inline void set_updateTable_24(bool value)
	{
		___updateTable_24 = value;
	}

	inline static int32_t get_offset_of_mWidget_25() { return static_cast<int32_t>(offsetof(TweenHeight_t4009371699, ___mWidget_25)); }
	inline UIWidget_t3538521925 * get_mWidget_25() const { return ___mWidget_25; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_25() { return &___mWidget_25; }
	inline void set_mWidget_25(UIWidget_t3538521925 * value)
	{
		___mWidget_25 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_25), value);
	}

	inline static int32_t get_offset_of_mTable_26() { return static_cast<int32_t>(offsetof(TweenHeight_t4009371699, ___mTable_26)); }
	inline UITable_t3168834800 * get_mTable_26() const { return ___mTable_26; }
	inline UITable_t3168834800 ** get_address_of_mTable_26() { return &___mTable_26; }
	inline void set_mTable_26(UITable_t3168834800 * value)
	{
		___mTable_26 = value;
		Il2CppCodeGenWriteBarrier((&___mTable_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENHEIGHT_T4009371699_H
#ifndef TWEENFOV_T2203484580_H
#define TWEENFOV_T2203484580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenFOV
struct  TweenFOV_t2203484580  : public UITweener_t260334902
{
public:
	// System.Single TweenFOV::from
	float ___from_22;
	// System.Single TweenFOV::to
	float ___to_23;
	// UnityEngine.Camera TweenFOV::mCam
	Camera_t4157153871 * ___mCam_24;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenFOV_t2203484580, ___from_22)); }
	inline float get_from_22() const { return ___from_22; }
	inline float* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(float value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenFOV_t2203484580, ___to_23)); }
	inline float get_to_23() const { return ___to_23; }
	inline float* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(float value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_mCam_24() { return static_cast<int32_t>(offsetof(TweenFOV_t2203484580, ___mCam_24)); }
	inline Camera_t4157153871 * get_mCam_24() const { return ___mCam_24; }
	inline Camera_t4157153871 ** get_address_of_mCam_24() { return &___mCam_24; }
	inline void set_mCam_24(Camera_t4157153871 * value)
	{
		___mCam_24 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENFOV_T2203484580_H
#ifndef TWEENCOLOR_T2112002648_H
#define TWEENCOLOR_T2112002648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenColor
struct  TweenColor_t2112002648  : public UITweener_t260334902
{
public:
	// UnityEngine.Color TweenColor::from
	Color_t2555686324  ___from_22;
	// UnityEngine.Color TweenColor::to
	Color_t2555686324  ___to_23;
	// System.Boolean TweenColor::mCached
	bool ___mCached_24;
	// UIWidget TweenColor::mWidget
	UIWidget_t3538521925 * ___mWidget_25;
	// UnityEngine.Material TweenColor::mMat
	Material_t340375123 * ___mMat_26;
	// UnityEngine.Light TweenColor::mLight
	Light_t3756812086 * ___mLight_27;
	// UnityEngine.SpriteRenderer TweenColor::mSr
	SpriteRenderer_t3235626157 * ___mSr_28;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenColor_t2112002648, ___from_22)); }
	inline Color_t2555686324  get_from_22() const { return ___from_22; }
	inline Color_t2555686324 * get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(Color_t2555686324  value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenColor_t2112002648, ___to_23)); }
	inline Color_t2555686324  get_to_23() const { return ___to_23; }
	inline Color_t2555686324 * get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(Color_t2555686324  value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_mCached_24() { return static_cast<int32_t>(offsetof(TweenColor_t2112002648, ___mCached_24)); }
	inline bool get_mCached_24() const { return ___mCached_24; }
	inline bool* get_address_of_mCached_24() { return &___mCached_24; }
	inline void set_mCached_24(bool value)
	{
		___mCached_24 = value;
	}

	inline static int32_t get_offset_of_mWidget_25() { return static_cast<int32_t>(offsetof(TweenColor_t2112002648, ___mWidget_25)); }
	inline UIWidget_t3538521925 * get_mWidget_25() const { return ___mWidget_25; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_25() { return &___mWidget_25; }
	inline void set_mWidget_25(UIWidget_t3538521925 * value)
	{
		___mWidget_25 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_25), value);
	}

	inline static int32_t get_offset_of_mMat_26() { return static_cast<int32_t>(offsetof(TweenColor_t2112002648, ___mMat_26)); }
	inline Material_t340375123 * get_mMat_26() const { return ___mMat_26; }
	inline Material_t340375123 ** get_address_of_mMat_26() { return &___mMat_26; }
	inline void set_mMat_26(Material_t340375123 * value)
	{
		___mMat_26 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_26), value);
	}

	inline static int32_t get_offset_of_mLight_27() { return static_cast<int32_t>(offsetof(TweenColor_t2112002648, ___mLight_27)); }
	inline Light_t3756812086 * get_mLight_27() const { return ___mLight_27; }
	inline Light_t3756812086 ** get_address_of_mLight_27() { return &___mLight_27; }
	inline void set_mLight_27(Light_t3756812086 * value)
	{
		___mLight_27 = value;
		Il2CppCodeGenWriteBarrier((&___mLight_27), value);
	}

	inline static int32_t get_offset_of_mSr_28() { return static_cast<int32_t>(offsetof(TweenColor_t2112002648, ___mSr_28)); }
	inline SpriteRenderer_t3235626157 * get_mSr_28() const { return ___mSr_28; }
	inline SpriteRenderer_t3235626157 ** get_address_of_mSr_28() { return &___mSr_28; }
	inline void set_mSr_28(SpriteRenderer_t3235626157 * value)
	{
		___mSr_28 = value;
		Il2CppCodeGenWriteBarrier((&___mSr_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENCOLOR_T2112002648_H
#ifndef TWEENALPHA_T3706845226_H
#define TWEENALPHA_T3706845226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAlpha
struct  TweenAlpha_t3706845226  : public UITweener_t260334902
{
public:
	// System.Single TweenAlpha::from
	float ___from_22;
	// System.Single TweenAlpha::to
	float ___to_23;
	// System.Boolean TweenAlpha::mCached
	bool ___mCached_24;
	// UIRect TweenAlpha::mRect
	UIRect_t2875960382 * ___mRect_25;
	// UnityEngine.Material TweenAlpha::mMat
	Material_t340375123 * ___mMat_26;
	// UnityEngine.Light TweenAlpha::mLight
	Light_t3756812086 * ___mLight_27;
	// UnityEngine.SpriteRenderer TweenAlpha::mSr
	SpriteRenderer_t3235626157 * ___mSr_28;
	// System.Single TweenAlpha::mBaseIntensity
	float ___mBaseIntensity_29;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___from_22)); }
	inline float get_from_22() const { return ___from_22; }
	inline float* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(float value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___to_23)); }
	inline float get_to_23() const { return ___to_23; }
	inline float* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(float value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_mCached_24() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mCached_24)); }
	inline bool get_mCached_24() const { return ___mCached_24; }
	inline bool* get_address_of_mCached_24() { return &___mCached_24; }
	inline void set_mCached_24(bool value)
	{
		___mCached_24 = value;
	}

	inline static int32_t get_offset_of_mRect_25() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mRect_25)); }
	inline UIRect_t2875960382 * get_mRect_25() const { return ___mRect_25; }
	inline UIRect_t2875960382 ** get_address_of_mRect_25() { return &___mRect_25; }
	inline void set_mRect_25(UIRect_t2875960382 * value)
	{
		___mRect_25 = value;
		Il2CppCodeGenWriteBarrier((&___mRect_25), value);
	}

	inline static int32_t get_offset_of_mMat_26() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mMat_26)); }
	inline Material_t340375123 * get_mMat_26() const { return ___mMat_26; }
	inline Material_t340375123 ** get_address_of_mMat_26() { return &___mMat_26; }
	inline void set_mMat_26(Material_t340375123 * value)
	{
		___mMat_26 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_26), value);
	}

	inline static int32_t get_offset_of_mLight_27() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mLight_27)); }
	inline Light_t3756812086 * get_mLight_27() const { return ___mLight_27; }
	inline Light_t3756812086 ** get_address_of_mLight_27() { return &___mLight_27; }
	inline void set_mLight_27(Light_t3756812086 * value)
	{
		___mLight_27 = value;
		Il2CppCodeGenWriteBarrier((&___mLight_27), value);
	}

	inline static int32_t get_offset_of_mSr_28() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mSr_28)); }
	inline SpriteRenderer_t3235626157 * get_mSr_28() const { return ___mSr_28; }
	inline SpriteRenderer_t3235626157 ** get_address_of_mSr_28() { return &___mSr_28; }
	inline void set_mSr_28(SpriteRenderer_t3235626157 * value)
	{
		___mSr_28 = value;
		Il2CppCodeGenWriteBarrier((&___mSr_28), value);
	}

	inline static int32_t get_offset_of_mBaseIntensity_29() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mBaseIntensity_29)); }
	inline float get_mBaseIntensity_29() const { return ___mBaseIntensity_29; }
	inline float* get_address_of_mBaseIntensity_29() { return &___mBaseIntensity_29; }
	inline void set_mBaseIntensity_29(float value)
	{
		___mBaseIntensity_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENALPHA_T3706845226_H
#ifndef UIWIDGET_T3538521925_H
#define UIWIDGET_T3538521925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget
struct  UIWidget_t3538521925  : public UIRect_t2875960382
{
public:
	// UnityEngine.Color UIWidget::mColor
	Color_t2555686324  ___mColor_22;
	// UIWidget/Pivot UIWidget::mPivot
	int32_t ___mPivot_23;
	// System.Int32 UIWidget::mWidth
	int32_t ___mWidth_24;
	// System.Int32 UIWidget::mHeight
	int32_t ___mHeight_25;
	// System.Int32 UIWidget::mDepth
	int32_t ___mDepth_26;
	// UnityEngine.Material UIWidget::mMat
	Material_t340375123 * ___mMat_27;
	// UIWidget/OnDimensionsChanged UIWidget::onChange
	OnDimensionsChanged_t3101921181 * ___onChange_28;
	// UIWidget/OnPostFillCallback UIWidget::onPostFill
	OnPostFillCallback_t2835645043 * ___onPostFill_29;
	// UIDrawCall/OnRenderCallback UIWidget::mOnRender
	OnRenderCallback_t133425655 * ___mOnRender_30;
	// System.Boolean UIWidget::autoResizeBoxCollider
	bool ___autoResizeBoxCollider_31;
	// System.Boolean UIWidget::hideIfOffScreen
	bool ___hideIfOffScreen_32;
	// UIWidget/AspectRatioSource UIWidget::keepAspectRatio
	int32_t ___keepAspectRatio_33;
	// System.Single UIWidget::aspectRatio
	float ___aspectRatio_34;
	// UIWidget/HitCheck UIWidget::hitCheck
	HitCheck_t2300079615 * ___hitCheck_35;
	// UIPanel UIWidget::panel
	UIPanel_t1716472341 * ___panel_36;
	// UIGeometry UIWidget::geometry
	UIGeometry_t1059483952 * ___geometry_37;
	// System.Boolean UIWidget::fillGeometry
	bool ___fillGeometry_38;
	// System.Boolean UIWidget::mPlayMode
	bool ___mPlayMode_39;
	// UnityEngine.Vector4 UIWidget::mDrawRegion
	Vector4_t3319028937  ___mDrawRegion_40;
	// UnityEngine.Matrix4x4 UIWidget::mLocalToPanel
	Matrix4x4_t1817901843  ___mLocalToPanel_41;
	// System.Boolean UIWidget::mIsVisibleByAlpha
	bool ___mIsVisibleByAlpha_42;
	// System.Boolean UIWidget::mIsVisibleByPanel
	bool ___mIsVisibleByPanel_43;
	// System.Boolean UIWidget::mIsInFront
	bool ___mIsInFront_44;
	// System.Single UIWidget::mLastAlpha
	float ___mLastAlpha_45;
	// System.Boolean UIWidget::mMoved
	bool ___mMoved_46;
	// UIDrawCall UIWidget::drawCall
	UIDrawCall_t1293405319 * ___drawCall_47;
	// UnityEngine.Vector3[] UIWidget::mCorners
	Vector3U5BU5D_t1718750761* ___mCorners_48;
	// System.Int32 UIWidget::mAlphaFrameID
	int32_t ___mAlphaFrameID_49;
	// System.Int32 UIWidget::mMatrixFrame
	int32_t ___mMatrixFrame_50;
	// UnityEngine.Vector3 UIWidget::mOldV0
	Vector3_t3722313464  ___mOldV0_51;
	// UnityEngine.Vector3 UIWidget::mOldV1
	Vector3_t3722313464  ___mOldV1_52;

public:
	inline static int32_t get_offset_of_mColor_22() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mColor_22)); }
	inline Color_t2555686324  get_mColor_22() const { return ___mColor_22; }
	inline Color_t2555686324 * get_address_of_mColor_22() { return &___mColor_22; }
	inline void set_mColor_22(Color_t2555686324  value)
	{
		___mColor_22 = value;
	}

	inline static int32_t get_offset_of_mPivot_23() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mPivot_23)); }
	inline int32_t get_mPivot_23() const { return ___mPivot_23; }
	inline int32_t* get_address_of_mPivot_23() { return &___mPivot_23; }
	inline void set_mPivot_23(int32_t value)
	{
		___mPivot_23 = value;
	}

	inline static int32_t get_offset_of_mWidth_24() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mWidth_24)); }
	inline int32_t get_mWidth_24() const { return ___mWidth_24; }
	inline int32_t* get_address_of_mWidth_24() { return &___mWidth_24; }
	inline void set_mWidth_24(int32_t value)
	{
		___mWidth_24 = value;
	}

	inline static int32_t get_offset_of_mHeight_25() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mHeight_25)); }
	inline int32_t get_mHeight_25() const { return ___mHeight_25; }
	inline int32_t* get_address_of_mHeight_25() { return &___mHeight_25; }
	inline void set_mHeight_25(int32_t value)
	{
		___mHeight_25 = value;
	}

	inline static int32_t get_offset_of_mDepth_26() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mDepth_26)); }
	inline int32_t get_mDepth_26() const { return ___mDepth_26; }
	inline int32_t* get_address_of_mDepth_26() { return &___mDepth_26; }
	inline void set_mDepth_26(int32_t value)
	{
		___mDepth_26 = value;
	}

	inline static int32_t get_offset_of_mMat_27() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMat_27)); }
	inline Material_t340375123 * get_mMat_27() const { return ___mMat_27; }
	inline Material_t340375123 ** get_address_of_mMat_27() { return &___mMat_27; }
	inline void set_mMat_27(Material_t340375123 * value)
	{
		___mMat_27 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_27), value);
	}

	inline static int32_t get_offset_of_onChange_28() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___onChange_28)); }
	inline OnDimensionsChanged_t3101921181 * get_onChange_28() const { return ___onChange_28; }
	inline OnDimensionsChanged_t3101921181 ** get_address_of_onChange_28() { return &___onChange_28; }
	inline void set_onChange_28(OnDimensionsChanged_t3101921181 * value)
	{
		___onChange_28 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_28), value);
	}

	inline static int32_t get_offset_of_onPostFill_29() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___onPostFill_29)); }
	inline OnPostFillCallback_t2835645043 * get_onPostFill_29() const { return ___onPostFill_29; }
	inline OnPostFillCallback_t2835645043 ** get_address_of_onPostFill_29() { return &___onPostFill_29; }
	inline void set_onPostFill_29(OnPostFillCallback_t2835645043 * value)
	{
		___onPostFill_29 = value;
		Il2CppCodeGenWriteBarrier((&___onPostFill_29), value);
	}

	inline static int32_t get_offset_of_mOnRender_30() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOnRender_30)); }
	inline OnRenderCallback_t133425655 * get_mOnRender_30() const { return ___mOnRender_30; }
	inline OnRenderCallback_t133425655 ** get_address_of_mOnRender_30() { return &___mOnRender_30; }
	inline void set_mOnRender_30(OnRenderCallback_t133425655 * value)
	{
		___mOnRender_30 = value;
		Il2CppCodeGenWriteBarrier((&___mOnRender_30), value);
	}

	inline static int32_t get_offset_of_autoResizeBoxCollider_31() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___autoResizeBoxCollider_31)); }
	inline bool get_autoResizeBoxCollider_31() const { return ___autoResizeBoxCollider_31; }
	inline bool* get_address_of_autoResizeBoxCollider_31() { return &___autoResizeBoxCollider_31; }
	inline void set_autoResizeBoxCollider_31(bool value)
	{
		___autoResizeBoxCollider_31 = value;
	}

	inline static int32_t get_offset_of_hideIfOffScreen_32() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___hideIfOffScreen_32)); }
	inline bool get_hideIfOffScreen_32() const { return ___hideIfOffScreen_32; }
	inline bool* get_address_of_hideIfOffScreen_32() { return &___hideIfOffScreen_32; }
	inline void set_hideIfOffScreen_32(bool value)
	{
		___hideIfOffScreen_32 = value;
	}

	inline static int32_t get_offset_of_keepAspectRatio_33() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___keepAspectRatio_33)); }
	inline int32_t get_keepAspectRatio_33() const { return ___keepAspectRatio_33; }
	inline int32_t* get_address_of_keepAspectRatio_33() { return &___keepAspectRatio_33; }
	inline void set_keepAspectRatio_33(int32_t value)
	{
		___keepAspectRatio_33 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_34() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___aspectRatio_34)); }
	inline float get_aspectRatio_34() const { return ___aspectRatio_34; }
	inline float* get_address_of_aspectRatio_34() { return &___aspectRatio_34; }
	inline void set_aspectRatio_34(float value)
	{
		___aspectRatio_34 = value;
	}

	inline static int32_t get_offset_of_hitCheck_35() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___hitCheck_35)); }
	inline HitCheck_t2300079615 * get_hitCheck_35() const { return ___hitCheck_35; }
	inline HitCheck_t2300079615 ** get_address_of_hitCheck_35() { return &___hitCheck_35; }
	inline void set_hitCheck_35(HitCheck_t2300079615 * value)
	{
		___hitCheck_35 = value;
		Il2CppCodeGenWriteBarrier((&___hitCheck_35), value);
	}

	inline static int32_t get_offset_of_panel_36() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___panel_36)); }
	inline UIPanel_t1716472341 * get_panel_36() const { return ___panel_36; }
	inline UIPanel_t1716472341 ** get_address_of_panel_36() { return &___panel_36; }
	inline void set_panel_36(UIPanel_t1716472341 * value)
	{
		___panel_36 = value;
		Il2CppCodeGenWriteBarrier((&___panel_36), value);
	}

	inline static int32_t get_offset_of_geometry_37() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___geometry_37)); }
	inline UIGeometry_t1059483952 * get_geometry_37() const { return ___geometry_37; }
	inline UIGeometry_t1059483952 ** get_address_of_geometry_37() { return &___geometry_37; }
	inline void set_geometry_37(UIGeometry_t1059483952 * value)
	{
		___geometry_37 = value;
		Il2CppCodeGenWriteBarrier((&___geometry_37), value);
	}

	inline static int32_t get_offset_of_fillGeometry_38() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___fillGeometry_38)); }
	inline bool get_fillGeometry_38() const { return ___fillGeometry_38; }
	inline bool* get_address_of_fillGeometry_38() { return &___fillGeometry_38; }
	inline void set_fillGeometry_38(bool value)
	{
		___fillGeometry_38 = value;
	}

	inline static int32_t get_offset_of_mPlayMode_39() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mPlayMode_39)); }
	inline bool get_mPlayMode_39() const { return ___mPlayMode_39; }
	inline bool* get_address_of_mPlayMode_39() { return &___mPlayMode_39; }
	inline void set_mPlayMode_39(bool value)
	{
		___mPlayMode_39 = value;
	}

	inline static int32_t get_offset_of_mDrawRegion_40() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mDrawRegion_40)); }
	inline Vector4_t3319028937  get_mDrawRegion_40() const { return ___mDrawRegion_40; }
	inline Vector4_t3319028937 * get_address_of_mDrawRegion_40() { return &___mDrawRegion_40; }
	inline void set_mDrawRegion_40(Vector4_t3319028937  value)
	{
		___mDrawRegion_40 = value;
	}

	inline static int32_t get_offset_of_mLocalToPanel_41() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mLocalToPanel_41)); }
	inline Matrix4x4_t1817901843  get_mLocalToPanel_41() const { return ___mLocalToPanel_41; }
	inline Matrix4x4_t1817901843 * get_address_of_mLocalToPanel_41() { return &___mLocalToPanel_41; }
	inline void set_mLocalToPanel_41(Matrix4x4_t1817901843  value)
	{
		___mLocalToPanel_41 = value;
	}

	inline static int32_t get_offset_of_mIsVisibleByAlpha_42() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsVisibleByAlpha_42)); }
	inline bool get_mIsVisibleByAlpha_42() const { return ___mIsVisibleByAlpha_42; }
	inline bool* get_address_of_mIsVisibleByAlpha_42() { return &___mIsVisibleByAlpha_42; }
	inline void set_mIsVisibleByAlpha_42(bool value)
	{
		___mIsVisibleByAlpha_42 = value;
	}

	inline static int32_t get_offset_of_mIsVisibleByPanel_43() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsVisibleByPanel_43)); }
	inline bool get_mIsVisibleByPanel_43() const { return ___mIsVisibleByPanel_43; }
	inline bool* get_address_of_mIsVisibleByPanel_43() { return &___mIsVisibleByPanel_43; }
	inline void set_mIsVisibleByPanel_43(bool value)
	{
		___mIsVisibleByPanel_43 = value;
	}

	inline static int32_t get_offset_of_mIsInFront_44() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsInFront_44)); }
	inline bool get_mIsInFront_44() const { return ___mIsInFront_44; }
	inline bool* get_address_of_mIsInFront_44() { return &___mIsInFront_44; }
	inline void set_mIsInFront_44(bool value)
	{
		___mIsInFront_44 = value;
	}

	inline static int32_t get_offset_of_mLastAlpha_45() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mLastAlpha_45)); }
	inline float get_mLastAlpha_45() const { return ___mLastAlpha_45; }
	inline float* get_address_of_mLastAlpha_45() { return &___mLastAlpha_45; }
	inline void set_mLastAlpha_45(float value)
	{
		___mLastAlpha_45 = value;
	}

	inline static int32_t get_offset_of_mMoved_46() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMoved_46)); }
	inline bool get_mMoved_46() const { return ___mMoved_46; }
	inline bool* get_address_of_mMoved_46() { return &___mMoved_46; }
	inline void set_mMoved_46(bool value)
	{
		___mMoved_46 = value;
	}

	inline static int32_t get_offset_of_drawCall_47() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___drawCall_47)); }
	inline UIDrawCall_t1293405319 * get_drawCall_47() const { return ___drawCall_47; }
	inline UIDrawCall_t1293405319 ** get_address_of_drawCall_47() { return &___drawCall_47; }
	inline void set_drawCall_47(UIDrawCall_t1293405319 * value)
	{
		___drawCall_47 = value;
		Il2CppCodeGenWriteBarrier((&___drawCall_47), value);
	}

	inline static int32_t get_offset_of_mCorners_48() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mCorners_48)); }
	inline Vector3U5BU5D_t1718750761* get_mCorners_48() const { return ___mCorners_48; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mCorners_48() { return &___mCorners_48; }
	inline void set_mCorners_48(Vector3U5BU5D_t1718750761* value)
	{
		___mCorners_48 = value;
		Il2CppCodeGenWriteBarrier((&___mCorners_48), value);
	}

	inline static int32_t get_offset_of_mAlphaFrameID_49() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mAlphaFrameID_49)); }
	inline int32_t get_mAlphaFrameID_49() const { return ___mAlphaFrameID_49; }
	inline int32_t* get_address_of_mAlphaFrameID_49() { return &___mAlphaFrameID_49; }
	inline void set_mAlphaFrameID_49(int32_t value)
	{
		___mAlphaFrameID_49 = value;
	}

	inline static int32_t get_offset_of_mMatrixFrame_50() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMatrixFrame_50)); }
	inline int32_t get_mMatrixFrame_50() const { return ___mMatrixFrame_50; }
	inline int32_t* get_address_of_mMatrixFrame_50() { return &___mMatrixFrame_50; }
	inline void set_mMatrixFrame_50(int32_t value)
	{
		___mMatrixFrame_50 = value;
	}

	inline static int32_t get_offset_of_mOldV0_51() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOldV0_51)); }
	inline Vector3_t3722313464  get_mOldV0_51() const { return ___mOldV0_51; }
	inline Vector3_t3722313464 * get_address_of_mOldV0_51() { return &___mOldV0_51; }
	inline void set_mOldV0_51(Vector3_t3722313464  value)
	{
		___mOldV0_51 = value;
	}

	inline static int32_t get_offset_of_mOldV1_52() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOldV1_52)); }
	inline Vector3_t3722313464  get_mOldV1_52() const { return ___mOldV1_52; }
	inline Vector3_t3722313464 * get_address_of_mOldV1_52() { return &___mOldV1_52; }
	inline void set_mOldV1_52(Vector3_t3722313464  value)
	{
		___mOldV1_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWIDGET_T3538521925_H
#ifndef TWEENWIDTH_T2861389279_H
#define TWEENWIDTH_T2861389279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenWidth
struct  TweenWidth_t2861389279  : public UITweener_t260334902
{
public:
	// System.Int32 TweenWidth::from
	int32_t ___from_22;
	// System.Int32 TweenWidth::to
	int32_t ___to_23;
	// System.Boolean TweenWidth::updateTable
	bool ___updateTable_24;
	// UIWidget TweenWidth::mWidget
	UIWidget_t3538521925 * ___mWidget_25;
	// UITable TweenWidth::mTable
	UITable_t3168834800 * ___mTable_26;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenWidth_t2861389279, ___from_22)); }
	inline int32_t get_from_22() const { return ___from_22; }
	inline int32_t* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(int32_t value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenWidth_t2861389279, ___to_23)); }
	inline int32_t get_to_23() const { return ___to_23; }
	inline int32_t* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(int32_t value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_updateTable_24() { return static_cast<int32_t>(offsetof(TweenWidth_t2861389279, ___updateTable_24)); }
	inline bool get_updateTable_24() const { return ___updateTable_24; }
	inline bool* get_address_of_updateTable_24() { return &___updateTable_24; }
	inline void set_updateTable_24(bool value)
	{
		___updateTable_24 = value;
	}

	inline static int32_t get_offset_of_mWidget_25() { return static_cast<int32_t>(offsetof(TweenWidth_t2861389279, ___mWidget_25)); }
	inline UIWidget_t3538521925 * get_mWidget_25() const { return ___mWidget_25; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_25() { return &___mWidget_25; }
	inline void set_mWidget_25(UIWidget_t3538521925 * value)
	{
		___mWidget_25 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_25), value);
	}

	inline static int32_t get_offset_of_mTable_26() { return static_cast<int32_t>(offsetof(TweenWidth_t2861389279, ___mTable_26)); }
	inline UITable_t3168834800 * get_mTable_26() const { return ___mTable_26; }
	inline UITable_t3168834800 ** get_address_of_mTable_26() { return &___mTable_26; }
	inline void set_mTable_26(UITable_t3168834800 * value)
	{
		___mTable_26 = value;
		Il2CppCodeGenWriteBarrier((&___mTable_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENWIDTH_T2861389279_H
#ifndef UIBASICSPRITE_T1521297657_H
#define UIBASICSPRITE_T1521297657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite
struct  UIBasicSprite_t1521297657  : public UIWidget_t3538521925
{
public:
	// UIBasicSprite/Type UIBasicSprite::mType
	int32_t ___mType_53;
	// UIBasicSprite/FillDirection UIBasicSprite::mFillDirection
	int32_t ___mFillDirection_54;
	// System.Single UIBasicSprite::mFillAmount
	float ___mFillAmount_55;
	// System.Boolean UIBasicSprite::mInvert
	bool ___mInvert_56;
	// UIBasicSprite/Flip UIBasicSprite::mFlip
	int32_t ___mFlip_57;
	// System.Boolean UIBasicSprite::mApplyGradient
	bool ___mApplyGradient_58;
	// UnityEngine.Color UIBasicSprite::mGradientTop
	Color_t2555686324  ___mGradientTop_59;
	// UnityEngine.Color UIBasicSprite::mGradientBottom
	Color_t2555686324  ___mGradientBottom_60;
	// UnityEngine.Rect UIBasicSprite::mInnerUV
	Rect_t2360479859  ___mInnerUV_61;
	// UnityEngine.Rect UIBasicSprite::mOuterUV
	Rect_t2360479859  ___mOuterUV_62;
	// UIBasicSprite/AdvancedType UIBasicSprite::centerType
	int32_t ___centerType_63;
	// UIBasicSprite/AdvancedType UIBasicSprite::leftType
	int32_t ___leftType_64;
	// UIBasicSprite/AdvancedType UIBasicSprite::rightType
	int32_t ___rightType_65;
	// UIBasicSprite/AdvancedType UIBasicSprite::bottomType
	int32_t ___bottomType_66;
	// UIBasicSprite/AdvancedType UIBasicSprite::topType
	int32_t ___topType_67;

public:
	inline static int32_t get_offset_of_mType_53() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mType_53)); }
	inline int32_t get_mType_53() const { return ___mType_53; }
	inline int32_t* get_address_of_mType_53() { return &___mType_53; }
	inline void set_mType_53(int32_t value)
	{
		___mType_53 = value;
	}

	inline static int32_t get_offset_of_mFillDirection_54() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFillDirection_54)); }
	inline int32_t get_mFillDirection_54() const { return ___mFillDirection_54; }
	inline int32_t* get_address_of_mFillDirection_54() { return &___mFillDirection_54; }
	inline void set_mFillDirection_54(int32_t value)
	{
		___mFillDirection_54 = value;
	}

	inline static int32_t get_offset_of_mFillAmount_55() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFillAmount_55)); }
	inline float get_mFillAmount_55() const { return ___mFillAmount_55; }
	inline float* get_address_of_mFillAmount_55() { return &___mFillAmount_55; }
	inline void set_mFillAmount_55(float value)
	{
		___mFillAmount_55 = value;
	}

	inline static int32_t get_offset_of_mInvert_56() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mInvert_56)); }
	inline bool get_mInvert_56() const { return ___mInvert_56; }
	inline bool* get_address_of_mInvert_56() { return &___mInvert_56; }
	inline void set_mInvert_56(bool value)
	{
		___mInvert_56 = value;
	}

	inline static int32_t get_offset_of_mFlip_57() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFlip_57)); }
	inline int32_t get_mFlip_57() const { return ___mFlip_57; }
	inline int32_t* get_address_of_mFlip_57() { return &___mFlip_57; }
	inline void set_mFlip_57(int32_t value)
	{
		___mFlip_57 = value;
	}

	inline static int32_t get_offset_of_mApplyGradient_58() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mApplyGradient_58)); }
	inline bool get_mApplyGradient_58() const { return ___mApplyGradient_58; }
	inline bool* get_address_of_mApplyGradient_58() { return &___mApplyGradient_58; }
	inline void set_mApplyGradient_58(bool value)
	{
		___mApplyGradient_58 = value;
	}

	inline static int32_t get_offset_of_mGradientTop_59() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mGradientTop_59)); }
	inline Color_t2555686324  get_mGradientTop_59() const { return ___mGradientTop_59; }
	inline Color_t2555686324 * get_address_of_mGradientTop_59() { return &___mGradientTop_59; }
	inline void set_mGradientTop_59(Color_t2555686324  value)
	{
		___mGradientTop_59 = value;
	}

	inline static int32_t get_offset_of_mGradientBottom_60() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mGradientBottom_60)); }
	inline Color_t2555686324  get_mGradientBottom_60() const { return ___mGradientBottom_60; }
	inline Color_t2555686324 * get_address_of_mGradientBottom_60() { return &___mGradientBottom_60; }
	inline void set_mGradientBottom_60(Color_t2555686324  value)
	{
		___mGradientBottom_60 = value;
	}

	inline static int32_t get_offset_of_mInnerUV_61() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mInnerUV_61)); }
	inline Rect_t2360479859  get_mInnerUV_61() const { return ___mInnerUV_61; }
	inline Rect_t2360479859 * get_address_of_mInnerUV_61() { return &___mInnerUV_61; }
	inline void set_mInnerUV_61(Rect_t2360479859  value)
	{
		___mInnerUV_61 = value;
	}

	inline static int32_t get_offset_of_mOuterUV_62() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mOuterUV_62)); }
	inline Rect_t2360479859  get_mOuterUV_62() const { return ___mOuterUV_62; }
	inline Rect_t2360479859 * get_address_of_mOuterUV_62() { return &___mOuterUV_62; }
	inline void set_mOuterUV_62(Rect_t2360479859  value)
	{
		___mOuterUV_62 = value;
	}

	inline static int32_t get_offset_of_centerType_63() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___centerType_63)); }
	inline int32_t get_centerType_63() const { return ___centerType_63; }
	inline int32_t* get_address_of_centerType_63() { return &___centerType_63; }
	inline void set_centerType_63(int32_t value)
	{
		___centerType_63 = value;
	}

	inline static int32_t get_offset_of_leftType_64() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___leftType_64)); }
	inline int32_t get_leftType_64() const { return ___leftType_64; }
	inline int32_t* get_address_of_leftType_64() { return &___leftType_64; }
	inline void set_leftType_64(int32_t value)
	{
		___leftType_64 = value;
	}

	inline static int32_t get_offset_of_rightType_65() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___rightType_65)); }
	inline int32_t get_rightType_65() const { return ___rightType_65; }
	inline int32_t* get_address_of_rightType_65() { return &___rightType_65; }
	inline void set_rightType_65(int32_t value)
	{
		___rightType_65 = value;
	}

	inline static int32_t get_offset_of_bottomType_66() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___bottomType_66)); }
	inline int32_t get_bottomType_66() const { return ___bottomType_66; }
	inline int32_t* get_address_of_bottomType_66() { return &___bottomType_66; }
	inline void set_bottomType_66(int32_t value)
	{
		___bottomType_66 = value;
	}

	inline static int32_t get_offset_of_topType_67() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___topType_67)); }
	inline int32_t get_topType_67() const { return ___topType_67; }
	inline int32_t* get_address_of_topType_67() { return &___topType_67; }
	inline void set_topType_67(int32_t value)
	{
		___topType_67 = value;
	}
};

struct UIBasicSprite_t1521297657_StaticFields
{
public:
	// UnityEngine.Vector2[] UIBasicSprite::mTempPos
	Vector2U5BU5D_t1457185986* ___mTempPos_68;
	// UnityEngine.Vector2[] UIBasicSprite::mTempUVs
	Vector2U5BU5D_t1457185986* ___mTempUVs_69;

public:
	inline static int32_t get_offset_of_mTempPos_68() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657_StaticFields, ___mTempPos_68)); }
	inline Vector2U5BU5D_t1457185986* get_mTempPos_68() const { return ___mTempPos_68; }
	inline Vector2U5BU5D_t1457185986** get_address_of_mTempPos_68() { return &___mTempPos_68; }
	inline void set_mTempPos_68(Vector2U5BU5D_t1457185986* value)
	{
		___mTempPos_68 = value;
		Il2CppCodeGenWriteBarrier((&___mTempPos_68), value);
	}

	inline static int32_t get_offset_of_mTempUVs_69() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657_StaticFields, ___mTempUVs_69)); }
	inline Vector2U5BU5D_t1457185986* get_mTempUVs_69() const { return ___mTempUVs_69; }
	inline Vector2U5BU5D_t1457185986** get_address_of_mTempUVs_69() { return &___mTempUVs_69; }
	inline void set_mTempUVs_69(Vector2U5BU5D_t1457185986* value)
	{
		___mTempUVs_69 = value;
		Il2CppCodeGenWriteBarrier((&___mTempUVs_69), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBASICSPRITE_T1521297657_H
#ifndef UI2DSPRITE_T1366157572_H
#define UI2DSPRITE_T1366157572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI2DSprite
struct  UI2DSprite_t1366157572  : public UIBasicSprite_t1521297657
{
public:
	// UnityEngine.Sprite UI2DSprite::mSprite
	Sprite_t280657092 * ___mSprite_70;
	// UnityEngine.Shader UI2DSprite::mShader
	Shader_t4151988712 * ___mShader_71;
	// UnityEngine.Vector4 UI2DSprite::mBorder
	Vector4_t3319028937  ___mBorder_72;
	// System.Boolean UI2DSprite::mFixedAspect
	bool ___mFixedAspect_73;
	// System.Single UI2DSprite::mPixelSize
	float ___mPixelSize_74;
	// UnityEngine.Sprite UI2DSprite::nextSprite
	Sprite_t280657092 * ___nextSprite_75;
	// System.Int32 UI2DSprite::mPMA
	int32_t ___mPMA_76;

public:
	inline static int32_t get_offset_of_mSprite_70() { return static_cast<int32_t>(offsetof(UI2DSprite_t1366157572, ___mSprite_70)); }
	inline Sprite_t280657092 * get_mSprite_70() const { return ___mSprite_70; }
	inline Sprite_t280657092 ** get_address_of_mSprite_70() { return &___mSprite_70; }
	inline void set_mSprite_70(Sprite_t280657092 * value)
	{
		___mSprite_70 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_70), value);
	}

	inline static int32_t get_offset_of_mShader_71() { return static_cast<int32_t>(offsetof(UI2DSprite_t1366157572, ___mShader_71)); }
	inline Shader_t4151988712 * get_mShader_71() const { return ___mShader_71; }
	inline Shader_t4151988712 ** get_address_of_mShader_71() { return &___mShader_71; }
	inline void set_mShader_71(Shader_t4151988712 * value)
	{
		___mShader_71 = value;
		Il2CppCodeGenWriteBarrier((&___mShader_71), value);
	}

	inline static int32_t get_offset_of_mBorder_72() { return static_cast<int32_t>(offsetof(UI2DSprite_t1366157572, ___mBorder_72)); }
	inline Vector4_t3319028937  get_mBorder_72() const { return ___mBorder_72; }
	inline Vector4_t3319028937 * get_address_of_mBorder_72() { return &___mBorder_72; }
	inline void set_mBorder_72(Vector4_t3319028937  value)
	{
		___mBorder_72 = value;
	}

	inline static int32_t get_offset_of_mFixedAspect_73() { return static_cast<int32_t>(offsetof(UI2DSprite_t1366157572, ___mFixedAspect_73)); }
	inline bool get_mFixedAspect_73() const { return ___mFixedAspect_73; }
	inline bool* get_address_of_mFixedAspect_73() { return &___mFixedAspect_73; }
	inline void set_mFixedAspect_73(bool value)
	{
		___mFixedAspect_73 = value;
	}

	inline static int32_t get_offset_of_mPixelSize_74() { return static_cast<int32_t>(offsetof(UI2DSprite_t1366157572, ___mPixelSize_74)); }
	inline float get_mPixelSize_74() const { return ___mPixelSize_74; }
	inline float* get_address_of_mPixelSize_74() { return &___mPixelSize_74; }
	inline void set_mPixelSize_74(float value)
	{
		___mPixelSize_74 = value;
	}

	inline static int32_t get_offset_of_nextSprite_75() { return static_cast<int32_t>(offsetof(UI2DSprite_t1366157572, ___nextSprite_75)); }
	inline Sprite_t280657092 * get_nextSprite_75() const { return ___nextSprite_75; }
	inline Sprite_t280657092 ** get_address_of_nextSprite_75() { return &___nextSprite_75; }
	inline void set_nextSprite_75(Sprite_t280657092 * value)
	{
		___nextSprite_75 = value;
		Il2CppCodeGenWriteBarrier((&___nextSprite_75), value);
	}

	inline static int32_t get_offset_of_mPMA_76() { return static_cast<int32_t>(offsetof(UI2DSprite_t1366157572, ___mPMA_76)); }
	inline int32_t get_mPMA_76() const { return ___mPMA_76; }
	inline int32_t* get_address_of_mPMA_76() { return &___mPMA_76; }
	inline void set_mPMA_76(int32_t value)
	{
		___mPMA_76 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI2DSPRITE_T1366157572_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (OnInitializeItem_t992046894), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (ActiveAnimation_t3475256642), -1, sizeof(ActiveAnimation_t3475256642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2501[10] = 
{
	ActiveAnimation_t3475256642_StaticFields::get_offset_of_current_2(),
	ActiveAnimation_t3475256642::get_offset_of_onFinished_3(),
	ActiveAnimation_t3475256642::get_offset_of_eventReceiver_4(),
	ActiveAnimation_t3475256642::get_offset_of_callWhenFinished_5(),
	ActiveAnimation_t3475256642::get_offset_of_mAnim_6(),
	ActiveAnimation_t3475256642::get_offset_of_mLastDirection_7(),
	ActiveAnimation_t3475256642::get_offset_of_mDisableDirection_8(),
	ActiveAnimation_t3475256642::get_offset_of_mNotify_9(),
	ActiveAnimation_t3475256642::get_offset_of_mAnimator_10(),
	ActiveAnimation_t3475256642::get_offset_of_mClip_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (Trigger_t3745258312)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2502[15] = 
{
	Trigger_t3745258312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (Direction_t2061188385)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[4] = 
{
	Direction_t2061188385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (EnableCondition_t1125033030)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[4] = 
{
	EnableCondition_t1125033030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (DisableCondition_t2151257518)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[4] = 
{
	DisableCondition_t2151257518::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (BMFont_t2757936676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[7] = 
{
	BMFont_t2757936676::get_offset_of_mSize_0(),
	BMFont_t2757936676::get_offset_of_mBase_1(),
	BMFont_t2757936676::get_offset_of_mWidth_2(),
	BMFont_t2757936676::get_offset_of_mHeight_3(),
	BMFont_t2757936676::get_offset_of_mSpriteName_4(),
	BMFont_t2757936676::get_offset_of_mSaved_5(),
	BMFont_t2757936676::get_offset_of_mDict_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (BMGlyph_t3344884546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[10] = 
{
	BMGlyph_t3344884546::get_offset_of_index_0(),
	BMGlyph_t3344884546::get_offset_of_x_1(),
	BMGlyph_t3344884546::get_offset_of_y_2(),
	BMGlyph_t3344884546::get_offset_of_width_3(),
	BMGlyph_t3344884546::get_offset_of_height_4(),
	BMGlyph_t3344884546::get_offset_of_offsetX_5(),
	BMGlyph_t3344884546::get_offset_of_offsetY_6(),
	BMGlyph_t3344884546::get_offset_of_advance_7(),
	BMGlyph_t3344884546::get_offset_of_channel_8(),
	BMGlyph_t3344884546::get_offset_of_kerning_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (BMSymbol_t1586058841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[11] = 
{
	BMSymbol_t1586058841::get_offset_of_sequence_0(),
	BMSymbol_t1586058841::get_offset_of_spriteName_1(),
	BMSymbol_t1586058841::get_offset_of_mSprite_2(),
	BMSymbol_t1586058841::get_offset_of_mIsValid_3(),
	BMSymbol_t1586058841::get_offset_of_mLength_4(),
	BMSymbol_t1586058841::get_offset_of_mOffsetX_5(),
	BMSymbol_t1586058841::get_offset_of_mOffsetY_6(),
	BMSymbol_t1586058841::get_offset_of_mWidth_7(),
	BMSymbol_t1586058841::get_offset_of_mHeight_8(),
	BMSymbol_t1586058841::get_offset_of_mAdvance_9(),
	BMSymbol_t1586058841::get_offset_of_mUV_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (ByteReader_t1539670756), -1, sizeof(ByteReader_t1539670756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[3] = 
{
	ByteReader_t1539670756::get_offset_of_mBuffer_0(),
	ByteReader_t1539670756::get_offset_of_mOffset_1(),
	ByteReader_t1539670756_StaticFields::get_offset_of_mTemp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (EventDelegate_t2738326060), -1, sizeof(EventDelegate_t2738326060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2513[11] = 
{
	EventDelegate_t2738326060::get_offset_of_mTarget_0(),
	EventDelegate_t2738326060::get_offset_of_mMethodName_1(),
	EventDelegate_t2738326060::get_offset_of_mParameters_2(),
	EventDelegate_t2738326060::get_offset_of_oneShot_3(),
	EventDelegate_t2738326060::get_offset_of_mCachedCallback_4(),
	EventDelegate_t2738326060::get_offset_of_mRawDelegate_5(),
	EventDelegate_t2738326060::get_offset_of_mCached_6(),
	EventDelegate_t2738326060::get_offset_of_mMethod_7(),
	EventDelegate_t2738326060::get_offset_of_mParameterInfos_8(),
	EventDelegate_t2738326060::get_offset_of_mArgs_9(),
	EventDelegate_t2738326060_StaticFields::get_offset_of_s_Hash_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (Parameter_t2966927026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[7] = 
{
	Parameter_t2966927026::get_offset_of_obj_0(),
	Parameter_t2966927026::get_offset_of_field_1(),
	Parameter_t2966927026::get_offset_of_mValue_2(),
	Parameter_t2966927026::get_offset_of_expectedType_3(),
	Parameter_t2966927026::get_offset_of_cached_4(),
	Parameter_t2966927026::get_offset_of_propInfo_5(),
	Parameter_t2966927026::get_offset_of_fieldInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (Callback_t3139336517), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Localization_t2163216738), -1, sizeof(Localization_t2163216738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2516[10] = 
{
	Localization_t2163216738_StaticFields::get_offset_of_loadFunction_0(),
	Localization_t2163216738_StaticFields::get_offset_of_onLocalize_1(),
	Localization_t2163216738_StaticFields::get_offset_of_localizationHasBeenSet_2(),
	Localization_t2163216738_StaticFields::get_offset_of_mLanguages_3(),
	Localization_t2163216738_StaticFields::get_offset_of_mOldDictionary_4(),
	Localization_t2163216738_StaticFields::get_offset_of_mDictionary_5(),
	Localization_t2163216738_StaticFields::get_offset_of_mReplacement_6(),
	Localization_t2163216738_StaticFields::get_offset_of_mLanguageIndex_7(),
	Localization_t2163216738_StaticFields::get_offset_of_mLanguage_8(),
	Localization_t2163216738_StaticFields::get_offset_of_mMerging_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (LoadFunction_t2078002637), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (OnLocalizeNotification_t3391620158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (MinMaxRangeAttribute_t3652335020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[2] = 
{
	MinMaxRangeAttribute_t3652335020::get_offset_of_minLimit_0(),
	MinMaxRangeAttribute_t3652335020::get_offset_of_maxLimit_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (NGUIDebug_t787955914), -1, sizeof(NGUIDebug_t787955914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2520[3] = 
{
	NGUIDebug_t787955914_StaticFields::get_offset_of_mRayDebug_2(),
	NGUIDebug_t787955914_StaticFields::get_offset_of_mLines_3(),
	NGUIDebug_t787955914_StaticFields::get_offset_of_mInstance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (NGUIMath_t3937908296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (NGUIText_t3089182085), -1, sizeof(NGUIText_t3089182085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2522[38] = 
{
	NGUIText_t3089182085_StaticFields::get_offset_of_bitmapFont_0(),
	NGUIText_t3089182085_StaticFields::get_offset_of_dynamicFont_1(),
	NGUIText_t3089182085_StaticFields::get_offset_of_glyph_2(),
	NGUIText_t3089182085_StaticFields::get_offset_of_fontSize_3(),
	NGUIText_t3089182085_StaticFields::get_offset_of_fontScale_4(),
	NGUIText_t3089182085_StaticFields::get_offset_of_pixelDensity_5(),
	NGUIText_t3089182085_StaticFields::get_offset_of_fontStyle_6(),
	NGUIText_t3089182085_StaticFields::get_offset_of_alignment_7(),
	NGUIText_t3089182085_StaticFields::get_offset_of_tint_8(),
	NGUIText_t3089182085_StaticFields::get_offset_of_rectWidth_9(),
	NGUIText_t3089182085_StaticFields::get_offset_of_rectHeight_10(),
	NGUIText_t3089182085_StaticFields::get_offset_of_regionWidth_11(),
	NGUIText_t3089182085_StaticFields::get_offset_of_regionHeight_12(),
	NGUIText_t3089182085_StaticFields::get_offset_of_maxLines_13(),
	NGUIText_t3089182085_StaticFields::get_offset_of_gradient_14(),
	NGUIText_t3089182085_StaticFields::get_offset_of_gradientBottom_15(),
	NGUIText_t3089182085_StaticFields::get_offset_of_gradientTop_16(),
	NGUIText_t3089182085_StaticFields::get_offset_of_encoding_17(),
	NGUIText_t3089182085_StaticFields::get_offset_of_spacingX_18(),
	NGUIText_t3089182085_StaticFields::get_offset_of_spacingY_19(),
	NGUIText_t3089182085_StaticFields::get_offset_of_premultiply_20(),
	NGUIText_t3089182085_StaticFields::get_offset_of_symbolStyle_21(),
	NGUIText_t3089182085_StaticFields::get_offset_of_finalSize_22(),
	NGUIText_t3089182085_StaticFields::get_offset_of_finalSpacingX_23(),
	NGUIText_t3089182085_StaticFields::get_offset_of_finalLineHeight_24(),
	NGUIText_t3089182085_StaticFields::get_offset_of_baseline_25(),
	NGUIText_t3089182085_StaticFields::get_offset_of_useSymbols_26(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mInvisible_27(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mColors_28(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mAlpha_29(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mTempChar_30(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mSizes_31(),
	NGUIText_t3089182085_StaticFields::get_offset_of_s_c0_32(),
	NGUIText_t3089182085_StaticFields::get_offset_of_s_c1_33(),
	0,
	NGUIText_t3089182085_StaticFields::get_offset_of_mBoldOffset_35(),
	NGUIText_t3089182085_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_36(),
	NGUIText_t3089182085_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (Alignment_t3228070485)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[6] = 
{
	Alignment_t3228070485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (SymbolStyle_t3792107337)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2524[5] = 
{
	SymbolStyle_t3792107337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (GlyphInfo_t1020792323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[8] = 
{
	GlyphInfo_t1020792323::get_offset_of_v0_0(),
	GlyphInfo_t1020792323::get_offset_of_v1_1(),
	GlyphInfo_t1020792323::get_offset_of_u0_2(),
	GlyphInfo_t1020792323::get_offset_of_u1_3(),
	GlyphInfo_t1020792323::get_offset_of_u2_4(),
	GlyphInfo_t1020792323::get_offset_of_u3_5(),
	GlyphInfo_t1020792323::get_offset_of_advance_6(),
	GlyphInfo_t1020792323::get_offset_of_channel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (DoNotObfuscateNGUI_t4082232512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (NGUITools_t1206951095), -1, sizeof(NGUITools_t1206951095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2527[15] = 
{
	NGUITools_t1206951095_StaticFields::get_offset_of_mListener_0(),
	NGUITools_t1206951095_StaticFields::get_offset_of_audioSource_1(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mLoaded_2(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mGlobalVolume_3(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mLastTimestamp_4(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mLastClip_5(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mTypeNames_6(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mSides_7(),
	NGUITools_t1206951095_StaticFields::get_offset_of_keys_8(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mWidgets_9(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mRoot_10(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mGo_11(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mColorSpace_12(),
	NGUITools_t1206951095_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
	NGUITools_t1206951095_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (PropertyBinding_t828139262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[6] = 
{
	PropertyBinding_t828139262::get_offset_of_source_2(),
	PropertyBinding_t828139262::get_offset_of_target_3(),
	PropertyBinding_t828139262::get_offset_of_direction_4(),
	PropertyBinding_t828139262::get_offset_of_update_5(),
	PropertyBinding_t828139262::get_offset_of_editMode_6(),
	PropertyBinding_t828139262::get_offset_of_mLastValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (UpdateCondition_t3398770213)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2530[5] = 
{
	UpdateCondition_t3398770213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (Direction_t1681948056)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	Direction_t1681948056::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (PropertyReference_t223937415), -1, sizeof(PropertyReference_t223937415_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[5] = 
{
	PropertyReference_t223937415::get_offset_of_mTarget_0(),
	PropertyReference_t223937415::get_offset_of_mName_1(),
	PropertyReference_t223937415::get_offset_of_mField_2(),
	PropertyReference_t223937415::get_offset_of_mProperty_3(),
	PropertyReference_t223937415_StaticFields::get_offset_of_s_Hash_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (RealTime_t4034823134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (SpringPanel_t277350554), -1, sizeof(SpringPanel_t277350554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2534[7] = 
{
	SpringPanel_t277350554_StaticFields::get_offset_of_current_2(),
	SpringPanel_t277350554::get_offset_of_target_3(),
	SpringPanel_t277350554::get_offset_of_strength_4(),
	SpringPanel_t277350554::get_offset_of_onFinished_5(),
	SpringPanel_t277350554::get_offset_of_mPanel_6(),
	SpringPanel_t277350554::get_offset_of_mTrans_7(),
	SpringPanel_t277350554::get_offset_of_mDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (OnFinished_t3778785451), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (UIBasicSprite_t1521297657), -1, sizeof(UIBasicSprite_t1521297657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2536[17] = 
{
	UIBasicSprite_t1521297657::get_offset_of_mType_53(),
	UIBasicSprite_t1521297657::get_offset_of_mFillDirection_54(),
	UIBasicSprite_t1521297657::get_offset_of_mFillAmount_55(),
	UIBasicSprite_t1521297657::get_offset_of_mInvert_56(),
	UIBasicSprite_t1521297657::get_offset_of_mFlip_57(),
	UIBasicSprite_t1521297657::get_offset_of_mApplyGradient_58(),
	UIBasicSprite_t1521297657::get_offset_of_mGradientTop_59(),
	UIBasicSprite_t1521297657::get_offset_of_mGradientBottom_60(),
	UIBasicSprite_t1521297657::get_offset_of_mInnerUV_61(),
	UIBasicSprite_t1521297657::get_offset_of_mOuterUV_62(),
	UIBasicSprite_t1521297657::get_offset_of_centerType_63(),
	UIBasicSprite_t1521297657::get_offset_of_leftType_64(),
	UIBasicSprite_t1521297657::get_offset_of_rightType_65(),
	UIBasicSprite_t1521297657::get_offset_of_bottomType_66(),
	UIBasicSprite_t1521297657::get_offset_of_topType_67(),
	UIBasicSprite_t1521297657_StaticFields::get_offset_of_mTempPos_68(),
	UIBasicSprite_t1521297657_StaticFields::get_offset_of_mTempUVs_69(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (Type_t4088629396)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2537[6] = 
{
	Type_t4088629396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (FillDirection_t904769527)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2538[6] = 
{
	FillDirection_t904769527::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (AdvancedType_t3940519926)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	AdvancedType_t3940519926::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (Flip_t2552321477)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2540[5] = 
{
	Flip_t2552321477::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (UIDrawCall_t1293405319), -1, sizeof(UIDrawCall_t1293405319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2541[44] = 
{
	UIDrawCall_t1293405319_StaticFields::get_offset_of_mActiveList_2(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_mInactiveList_3(),
	UIDrawCall_t1293405319::get_offset_of_widgetCount_4(),
	UIDrawCall_t1293405319::get_offset_of_depthStart_5(),
	UIDrawCall_t1293405319::get_offset_of_depthEnd_6(),
	UIDrawCall_t1293405319::get_offset_of_manager_7(),
	UIDrawCall_t1293405319::get_offset_of_panel_8(),
	UIDrawCall_t1293405319::get_offset_of_clipTexture_9(),
	UIDrawCall_t1293405319::get_offset_of_alwaysOnScreen_10(),
	UIDrawCall_t1293405319::get_offset_of_verts_11(),
	UIDrawCall_t1293405319::get_offset_of_norms_12(),
	UIDrawCall_t1293405319::get_offset_of_tans_13(),
	UIDrawCall_t1293405319::get_offset_of_uvs_14(),
	UIDrawCall_t1293405319::get_offset_of_uv2_15(),
	UIDrawCall_t1293405319::get_offset_of_cols_16(),
	UIDrawCall_t1293405319::get_offset_of_mMaterial_17(),
	UIDrawCall_t1293405319::get_offset_of_mTexture_18(),
	UIDrawCall_t1293405319::get_offset_of_mShader_19(),
	UIDrawCall_t1293405319::get_offset_of_mClipCount_20(),
	UIDrawCall_t1293405319::get_offset_of_mTrans_21(),
	UIDrawCall_t1293405319::get_offset_of_mMesh_22(),
	UIDrawCall_t1293405319::get_offset_of_mFilter_23(),
	UIDrawCall_t1293405319::get_offset_of_mRenderer_24(),
	UIDrawCall_t1293405319::get_offset_of_mDynamicMat_25(),
	UIDrawCall_t1293405319::get_offset_of_mIndices_26(),
	UIDrawCall_t1293405319::get_offset_of_mShadowMode_27(),
	UIDrawCall_t1293405319::get_offset_of_mRebuildMat_28(),
	UIDrawCall_t1293405319::get_offset_of_mLegacyShader_29(),
	UIDrawCall_t1293405319::get_offset_of_mRenderQueue_30(),
	UIDrawCall_t1293405319::get_offset_of_mTriangles_31(),
	UIDrawCall_t1293405319::get_offset_of_isDirty_32(),
	UIDrawCall_t1293405319::get_offset_of_mTextureClip_33(),
	UIDrawCall_t1293405319::get_offset_of_mIsNew_34(),
	UIDrawCall_t1293405319::get_offset_of_onRender_35(),
	UIDrawCall_t1293405319::get_offset_of_onCreateDrawCall_36(),
	UIDrawCall_t1293405319::get_offset_of_mSortingLayerName_37(),
	UIDrawCall_t1293405319::get_offset_of_mSortingOrder_38(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_mColorSpace_39(),
	0,
	UIDrawCall_t1293405319_StaticFields::get_offset_of_mCache_41(),
	UIDrawCall_t1293405319::get_offset_of_mBlock_42(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_ClipRange_43(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_ClipArgs_44(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_dx9BugWorkaround_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (Clipping_t1109313910)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2542[5] = 
{
	Clipping_t1109313910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (OnRenderCallback_t133425655), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (OnCreateDrawCall_t609469653), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (ShadowMode_t1455171354)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2545[4] = 
{
	ShadowMode_t1455171354::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (UIEventListener_t1665237878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[17] = 
{
	UIEventListener_t1665237878::get_offset_of_parameter_2(),
	UIEventListener_t1665237878::get_offset_of_onSubmit_3(),
	UIEventListener_t1665237878::get_offset_of_onClick_4(),
	UIEventListener_t1665237878::get_offset_of_onDoubleClick_5(),
	UIEventListener_t1665237878::get_offset_of_onHover_6(),
	UIEventListener_t1665237878::get_offset_of_onPress_7(),
	UIEventListener_t1665237878::get_offset_of_onSelect_8(),
	UIEventListener_t1665237878::get_offset_of_onScroll_9(),
	UIEventListener_t1665237878::get_offset_of_onDragStart_10(),
	UIEventListener_t1665237878::get_offset_of_onDrag_11(),
	UIEventListener_t1665237878::get_offset_of_onDragOver_12(),
	UIEventListener_t1665237878::get_offset_of_onDragOut_13(),
	UIEventListener_t1665237878::get_offset_of_onDragEnd_14(),
	UIEventListener_t1665237878::get_offset_of_onDrop_15(),
	UIEventListener_t1665237878::get_offset_of_onKey_16(),
	UIEventListener_t1665237878::get_offset_of_onTooltip_17(),
	UIEventListener_t1665237878::get_offset_of_needsActiveCollider_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (VoidDelegate_t3914127870), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (BoolDelegate_t3089012064), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (FloatDelegate_t1747458064), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (VectorDelegate_t1966661092), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (ObjectDelegate_t2025096746), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (KeyCodeDelegate_t675056699), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (UIGeometry_t1059483952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[7] = 
{
	UIGeometry_t1059483952::get_offset_of_verts_0(),
	UIGeometry_t1059483952::get_offset_of_uvs_1(),
	UIGeometry_t1059483952::get_offset_of_cols_2(),
	UIGeometry_t1059483952::get_offset_of_onCustomWrite_3(),
	UIGeometry_t1059483952::get_offset_of_mRtpVerts_4(),
	UIGeometry_t1059483952::get_offset_of_mRtpNormal_5(),
	UIGeometry_t1059483952::get_offset_of_mRtpTan_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (OnCustomWrite_t2800546165), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (UIRect_t2875960382), -1, sizeof(UIRect_t2875960382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[20] = 
{
	UIRect_t2875960382::get_offset_of_leftAnchor_2(),
	UIRect_t2875960382::get_offset_of_rightAnchor_3(),
	UIRect_t2875960382::get_offset_of_bottomAnchor_4(),
	UIRect_t2875960382::get_offset_of_topAnchor_5(),
	UIRect_t2875960382::get_offset_of_updateAnchors_6(),
	UIRect_t2875960382::get_offset_of_mGo_7(),
	UIRect_t2875960382::get_offset_of_mTrans_8(),
	UIRect_t2875960382::get_offset_of_mChildren_9(),
	UIRect_t2875960382::get_offset_of_mChanged_10(),
	UIRect_t2875960382::get_offset_of_mParentFound_11(),
	UIRect_t2875960382::get_offset_of_mUpdateAnchors_12(),
	UIRect_t2875960382::get_offset_of_mUpdateFrame_13(),
	UIRect_t2875960382::get_offset_of_mAnchorsCached_14(),
	UIRect_t2875960382::get_offset_of_mRoot_15(),
	UIRect_t2875960382::get_offset_of_mParent_16(),
	UIRect_t2875960382::get_offset_of_mRootSet_17(),
	UIRect_t2875960382::get_offset_of_mCam_18(),
	UIRect_t2875960382::get_offset_of_mStarted_19(),
	UIRect_t2875960382::get_offset_of_finalAlpha_20(),
	UIRect_t2875960382_StaticFields::get_offset_of_mSides_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (AnchorPoint_t1754718329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[5] = 
{
	AnchorPoint_t1754718329::get_offset_of_target_0(),
	AnchorPoint_t1754718329::get_offset_of_relative_1(),
	AnchorPoint_t1754718329::get_offset_of_absolute_2(),
	AnchorPoint_t1754718329::get_offset_of_rect_3(),
	AnchorPoint_t1754718329::get_offset_of_targetCam_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (AnchorUpdate_t1570184075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2557[4] = 
{
	AnchorUpdate_t1570184075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (UISnapshotPoint_t2982659727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[6] = 
{
	UISnapshotPoint_t2982659727::get_offset_of_isOrthographic_2(),
	UISnapshotPoint_t2982659727::get_offset_of_nearClip_3(),
	UISnapshotPoint_t2982659727::get_offset_of_farClip_4(),
	UISnapshotPoint_t2982659727::get_offset_of_fieldOfView_5(),
	UISnapshotPoint_t2982659727::get_offset_of_orthoSize_6(),
	UISnapshotPoint_t2982659727::get_offset_of_thumbnail_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (UIWidget_t3538521925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[31] = 
{
	UIWidget_t3538521925::get_offset_of_mColor_22(),
	UIWidget_t3538521925::get_offset_of_mPivot_23(),
	UIWidget_t3538521925::get_offset_of_mWidth_24(),
	UIWidget_t3538521925::get_offset_of_mHeight_25(),
	UIWidget_t3538521925::get_offset_of_mDepth_26(),
	UIWidget_t3538521925::get_offset_of_mMat_27(),
	UIWidget_t3538521925::get_offset_of_onChange_28(),
	UIWidget_t3538521925::get_offset_of_onPostFill_29(),
	UIWidget_t3538521925::get_offset_of_mOnRender_30(),
	UIWidget_t3538521925::get_offset_of_autoResizeBoxCollider_31(),
	UIWidget_t3538521925::get_offset_of_hideIfOffScreen_32(),
	UIWidget_t3538521925::get_offset_of_keepAspectRatio_33(),
	UIWidget_t3538521925::get_offset_of_aspectRatio_34(),
	UIWidget_t3538521925::get_offset_of_hitCheck_35(),
	UIWidget_t3538521925::get_offset_of_panel_36(),
	UIWidget_t3538521925::get_offset_of_geometry_37(),
	UIWidget_t3538521925::get_offset_of_fillGeometry_38(),
	UIWidget_t3538521925::get_offset_of_mPlayMode_39(),
	UIWidget_t3538521925::get_offset_of_mDrawRegion_40(),
	UIWidget_t3538521925::get_offset_of_mLocalToPanel_41(),
	UIWidget_t3538521925::get_offset_of_mIsVisibleByAlpha_42(),
	UIWidget_t3538521925::get_offset_of_mIsVisibleByPanel_43(),
	UIWidget_t3538521925::get_offset_of_mIsInFront_44(),
	UIWidget_t3538521925::get_offset_of_mLastAlpha_45(),
	UIWidget_t3538521925::get_offset_of_mMoved_46(),
	UIWidget_t3538521925::get_offset_of_drawCall_47(),
	UIWidget_t3538521925::get_offset_of_mCorners_48(),
	UIWidget_t3538521925::get_offset_of_mAlphaFrameID_49(),
	UIWidget_t3538521925::get_offset_of_mMatrixFrame_50(),
	UIWidget_t3538521925::get_offset_of_mOldV0_51(),
	UIWidget_t3538521925::get_offset_of_mOldV1_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (Pivot_t1798046373)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2560[10] = 
{
	Pivot_t1798046373::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (OnDimensionsChanged_t3101921181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (OnPostFillCallback_t2835645043), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (AspectRatioSource_t168813522)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2563[4] = 
{
	AspectRatioSource_t168813522::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (HitCheck_t2300079615), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (AnimatedAlpha_t1840762679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[3] = 
{
	AnimatedAlpha_t1840762679::get_offset_of_alpha_2(),
	AnimatedAlpha_t1840762679::get_offset_of_mWidget_3(),
	AnimatedAlpha_t1840762679::get_offset_of_mPanel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (AnimatedColor_t3276574810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[2] = 
{
	AnimatedColor_t3276574810::get_offset_of_color_2(),
	AnimatedColor_t3276574810::get_offset_of_mWidget_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (AnimatedWidget_t1381166569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[3] = 
{
	AnimatedWidget_t1381166569::get_offset_of_width_2(),
	AnimatedWidget_t1381166569::get_offset_of_height_3(),
	AnimatedWidget_t1381166569::get_offset_of_mWidget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (SpringPosition_t3478173108), -1, sizeof(SpringPosition_t3478173108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2568[12] = 
{
	SpringPosition_t3478173108_StaticFields::get_offset_of_current_2(),
	SpringPosition_t3478173108::get_offset_of_target_3(),
	SpringPosition_t3478173108::get_offset_of_strength_4(),
	SpringPosition_t3478173108::get_offset_of_worldSpace_5(),
	SpringPosition_t3478173108::get_offset_of_ignoreTimeScale_6(),
	SpringPosition_t3478173108::get_offset_of_updateScrollView_7(),
	SpringPosition_t3478173108::get_offset_of_onFinished_8(),
	SpringPosition_t3478173108::get_offset_of_eventReceiver_9(),
	SpringPosition_t3478173108::get_offset_of_callWhenFinished_10(),
	SpringPosition_t3478173108::get_offset_of_mTrans_11(),
	SpringPosition_t3478173108::get_offset_of_mThreshold_12(),
	SpringPosition_t3478173108::get_offset_of_mSv_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (OnFinished_t3364492952), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (TweenAlpha_t3706845226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[8] = 
{
	TweenAlpha_t3706845226::get_offset_of_from_22(),
	TweenAlpha_t3706845226::get_offset_of_to_23(),
	TweenAlpha_t3706845226::get_offset_of_mCached_24(),
	TweenAlpha_t3706845226::get_offset_of_mRect_25(),
	TweenAlpha_t3706845226::get_offset_of_mMat_26(),
	TweenAlpha_t3706845226::get_offset_of_mLight_27(),
	TweenAlpha_t3706845226::get_offset_of_mSr_28(),
	TweenAlpha_t3706845226::get_offset_of_mBaseIntensity_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (TweenColor_t2112002648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[7] = 
{
	TweenColor_t2112002648::get_offset_of_from_22(),
	TweenColor_t2112002648::get_offset_of_to_23(),
	TweenColor_t2112002648::get_offset_of_mCached_24(),
	TweenColor_t2112002648::get_offset_of_mWidget_25(),
	TweenColor_t2112002648::get_offset_of_mMat_26(),
	TweenColor_t2112002648::get_offset_of_mLight_27(),
	TweenColor_t2112002648::get_offset_of_mSr_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (TweenFOV_t2203484580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[3] = 
{
	TweenFOV_t2203484580::get_offset_of_from_22(),
	TweenFOV_t2203484580::get_offset_of_to_23(),
	TweenFOV_t2203484580::get_offset_of_mCam_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (TweenHeight_t4009371699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[5] = 
{
	TweenHeight_t4009371699::get_offset_of_from_22(),
	TweenHeight_t4009371699::get_offset_of_to_23(),
	TweenHeight_t4009371699::get_offset_of_updateTable_24(),
	TweenHeight_t4009371699::get_offset_of_mWidget_25(),
	TweenHeight_t4009371699::get_offset_of_mTable_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (TweenOrthoSize_t2102937296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[3] = 
{
	TweenOrthoSize_t2102937296::get_offset_of_from_22(),
	TweenOrthoSize_t2102937296::get_offset_of_to_23(),
	TweenOrthoSize_t2102937296::get_offset_of_mCam_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (TweenPosition_t1378762002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[5] = 
{
	TweenPosition_t1378762002::get_offset_of_from_22(),
	TweenPosition_t1378762002::get_offset_of_to_23(),
	TweenPosition_t1378762002::get_offset_of_worldSpace_24(),
	TweenPosition_t1378762002::get_offset_of_mTrans_25(),
	TweenPosition_t1378762002::get_offset_of_mRect_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (TweenRotation_t3072670746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[4] = 
{
	TweenRotation_t3072670746::get_offset_of_from_22(),
	TweenRotation_t3072670746::get_offset_of_to_23(),
	TweenRotation_t3072670746::get_offset_of_quaternionLerp_24(),
	TweenRotation_t3072670746::get_offset_of_mTrans_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (TweenScale_t2539309033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[5] = 
{
	TweenScale_t2539309033::get_offset_of_from_22(),
	TweenScale_t2539309033::get_offset_of_to_23(),
	TweenScale_t2539309033::get_offset_of_updateTable_24(),
	TweenScale_t2539309033::get_offset_of_mTrans_25(),
	TweenScale_t2539309033::get_offset_of_mTable_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (TweenTransform_t1195296467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[7] = 
{
	TweenTransform_t1195296467::get_offset_of_from_22(),
	TweenTransform_t1195296467::get_offset_of_to_23(),
	TweenTransform_t1195296467::get_offset_of_parentWhenFinished_24(),
	TweenTransform_t1195296467::get_offset_of_mTrans_25(),
	TweenTransform_t1195296467::get_offset_of_mPos_26(),
	TweenTransform_t1195296467::get_offset_of_mRot_27(),
	TweenTransform_t1195296467::get_offset_of_mScale_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (TweenVolume_t3718612080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[3] = 
{
	TweenVolume_t3718612080::get_offset_of_from_22(),
	TweenVolume_t3718612080::get_offset_of_to_23(),
	TweenVolume_t3718612080::get_offset_of_mSource_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (TweenWidth_t2861389279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[5] = 
{
	TweenWidth_t2861389279::get_offset_of_from_22(),
	TweenWidth_t2861389279::get_offset_of_to_23(),
	TweenWidth_t2861389279::get_offset_of_updateTable_24(),
	TweenWidth_t2861389279::get_offset_of_mWidget_25(),
	TweenWidth_t2861389279::get_offset_of_mTable_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (UITweener_t260334902), -1, sizeof(UITweener_t260334902_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2581[20] = 
{
	UITweener_t260334902_StaticFields::get_offset_of_current_2(),
	UITweener_t260334902::get_offset_of_method_3(),
	UITweener_t260334902::get_offset_of_style_4(),
	UITweener_t260334902::get_offset_of_animationCurve_5(),
	UITweener_t260334902::get_offset_of_ignoreTimeScale_6(),
	UITweener_t260334902::get_offset_of_delay_7(),
	UITweener_t260334902::get_offset_of_duration_8(),
	UITweener_t260334902::get_offset_of_steeperCurves_9(),
	UITweener_t260334902::get_offset_of_tweenGroup_10(),
	UITweener_t260334902::get_offset_of_useFixedUpdate_11(),
	UITweener_t260334902::get_offset_of_onFinished_12(),
	UITweener_t260334902::get_offset_of_eventReceiver_13(),
	UITweener_t260334902::get_offset_of_callWhenFinished_14(),
	UITweener_t260334902::get_offset_of_timeScale_15(),
	UITweener_t260334902::get_offset_of_mStarted_16(),
	UITweener_t260334902::get_offset_of_mStartTime_17(),
	UITweener_t260334902::get_offset_of_mDuration_18(),
	UITweener_t260334902::get_offset_of_mAmountPerDelta_19(),
	UITweener_t260334902::get_offset_of_mFactor_20(),
	UITweener_t260334902::get_offset_of_mTemp_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (Method_t1730494418)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2582[7] = 
{
	Method_t1730494418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (Style_t3120619385)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2583[4] = 
{
	Style_t3120619385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (UI2DSprite_t1366157572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[7] = 
{
	UI2DSprite_t1366157572::get_offset_of_mSprite_70(),
	UI2DSprite_t1366157572::get_offset_of_mShader_71(),
	UI2DSprite_t1366157572::get_offset_of_mBorder_72(),
	UI2DSprite_t1366157572::get_offset_of_mFixedAspect_73(),
	UI2DSprite_t1366157572::get_offset_of_mPixelSize_74(),
	UI2DSprite_t1366157572::get_offset_of_nextSprite_75(),
	UI2DSprite_t1366157572::get_offset_of_mPMA_76(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (UI2DSpriteAnimation_t3056508403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[8] = 
{
	UI2DSpriteAnimation_t3056508403::get_offset_of_frameIndex_2(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_framerate_3(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_ignoreTimeScale_4(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_loop_5(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_frames_6(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_mUnitySprite_7(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_mNguiSprite_8(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_mUpdate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (UIAnchor_t2527798900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[12] = 
{
	UIAnchor_t2527798900::get_offset_of_uiCamera_2(),
	UIAnchor_t2527798900::get_offset_of_container_3(),
	UIAnchor_t2527798900::get_offset_of_side_4(),
	UIAnchor_t2527798900::get_offset_of_runOnlyOnce_5(),
	UIAnchor_t2527798900::get_offset_of_relativeOffset_6(),
	UIAnchor_t2527798900::get_offset_of_pixelOffset_7(),
	UIAnchor_t2527798900::get_offset_of_widgetContainer_8(),
	UIAnchor_t2527798900::get_offset_of_mTrans_9(),
	UIAnchor_t2527798900::get_offset_of_mAnim_10(),
	UIAnchor_t2527798900::get_offset_of_mRect_11(),
	UIAnchor_t2527798900::get_offset_of_mRoot_12(),
	UIAnchor_t2527798900::get_offset_of_mStarted_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Side_t3584783117)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2587[10] = 
{
	Side_t3584783117::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (UIAtlas_t3195533529), -1, sizeof(UIAtlas_t3195533529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2588[9] = 
{
	UIAtlas_t3195533529::get_offset_of_material_2(),
	UIAtlas_t3195533529::get_offset_of_mSprites_3(),
	UIAtlas_t3195533529::get_offset_of_mPixelSize_4(),
	UIAtlas_t3195533529::get_offset_of_mReplacement_5(),
	UIAtlas_t3195533529::get_offset_of_mCoordinates_6(),
	UIAtlas_t3195533529::get_offset_of_sprites_7(),
	UIAtlas_t3195533529::get_offset_of_mPMA_8(),
	UIAtlas_t3195533529::get_offset_of_mSpriteIndices_9(),
	UIAtlas_t3195533529_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Sprite_t2895597119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[8] = 
{
	Sprite_t2895597119::get_offset_of_name_0(),
	Sprite_t2895597119::get_offset_of_outer_1(),
	Sprite_t2895597119::get_offset_of_inner_2(),
	Sprite_t2895597119::get_offset_of_rotated_3(),
	Sprite_t2895597119::get_offset_of_paddingLeft_4(),
	Sprite_t2895597119::get_offset_of_paddingRight_5(),
	Sprite_t2895597119::get_offset_of_paddingTop_6(),
	Sprite_t2895597119::get_offset_of_paddingBottom_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (Coordinates_t1880298793)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2590[3] = 
{
	Coordinates_t1880298793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (UICamera_t1356438871), -1, sizeof(UICamera_t1356438871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[109] = 
{
	UICamera_t1356438871_StaticFields::get_offset_of_list_2(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetKeyDown_3(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetKeyUp_4(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetKey_5(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetAxis_6(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetAnyKeyDown_7(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetMouse_8(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetTouch_9(),
	UICamera_t1356438871_StaticFields::get_offset_of_RemoveTouch_10(),
	UICamera_t1356438871_StaticFields::get_offset_of_onScreenResize_11(),
	UICamera_t1356438871::get_offset_of_eventType_12(),
	UICamera_t1356438871::get_offset_of_eventsGoToColliders_13(),
	UICamera_t1356438871::get_offset_of_eventReceiverMask_14(),
	UICamera_t1356438871::get_offset_of_processEventsIn_15(),
	UICamera_t1356438871::get_offset_of_debug_16(),
	UICamera_t1356438871::get_offset_of_useMouse_17(),
	UICamera_t1356438871::get_offset_of_useTouch_18(),
	UICamera_t1356438871::get_offset_of_allowMultiTouch_19(),
	UICamera_t1356438871::get_offset_of_useKeyboard_20(),
	UICamera_t1356438871::get_offset_of_useController_21(),
	UICamera_t1356438871::get_offset_of_stickyTooltip_22(),
	UICamera_t1356438871::get_offset_of_tooltipDelay_23(),
	UICamera_t1356438871::get_offset_of_longPressTooltip_24(),
	UICamera_t1356438871::get_offset_of_mouseDragThreshold_25(),
	UICamera_t1356438871::get_offset_of_mouseClickThreshold_26(),
	UICamera_t1356438871::get_offset_of_touchDragThreshold_27(),
	UICamera_t1356438871::get_offset_of_touchClickThreshold_28(),
	UICamera_t1356438871::get_offset_of_rangeDistance_29(),
	UICamera_t1356438871::get_offset_of_horizontalAxisName_30(),
	UICamera_t1356438871::get_offset_of_verticalAxisName_31(),
	UICamera_t1356438871::get_offset_of_horizontalPanAxisName_32(),
	UICamera_t1356438871::get_offset_of_verticalPanAxisName_33(),
	UICamera_t1356438871::get_offset_of_scrollAxisName_34(),
	UICamera_t1356438871::get_offset_of_commandClick_35(),
	UICamera_t1356438871::get_offset_of_submitKey0_36(),
	UICamera_t1356438871::get_offset_of_submitKey1_37(),
	UICamera_t1356438871::get_offset_of_cancelKey0_38(),
	UICamera_t1356438871::get_offset_of_cancelKey1_39(),
	UICamera_t1356438871::get_offset_of_autoHideCursor_40(),
	UICamera_t1356438871_StaticFields::get_offset_of_onCustomInput_41(),
	UICamera_t1356438871_StaticFields::get_offset_of_showTooltips_42(),
	UICamera_t1356438871_StaticFields::get_offset_of_ignoreAllEvents_43(),
	UICamera_t1356438871_StaticFields::get_offset_of_ignoreControllerInput_44(),
	UICamera_t1356438871_StaticFields::get_offset_of_mDisableController_45(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastPos_46(),
	UICamera_t1356438871_StaticFields::get_offset_of_lastWorldPosition_47(),
	UICamera_t1356438871_StaticFields::get_offset_of_lastWorldRay_48(),
	UICamera_t1356438871_StaticFields::get_offset_of_lastHit_49(),
	UICamera_t1356438871_StaticFields::get_offset_of_current_50(),
	UICamera_t1356438871_StaticFields::get_offset_of_currentCamera_51(),
	UICamera_t1356438871_StaticFields::get_offset_of_onSchemeChange_52(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastScheme_53(),
	UICamera_t1356438871_StaticFields::get_offset_of_currentTouchID_54(),
	UICamera_t1356438871_StaticFields::get_offset_of_mCurrentKey_55(),
	UICamera_t1356438871_StaticFields::get_offset_of_currentTouch_56(),
	UICamera_t1356438871_StaticFields::get_offset_of_mInputFocus_57(),
	UICamera_t1356438871_StaticFields::get_offset_of_mGenericHandler_58(),
	UICamera_t1356438871_StaticFields::get_offset_of_fallThrough_59(),
	UICamera_t1356438871_StaticFields::get_offset_of_onClick_60(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDoubleClick_61(),
	UICamera_t1356438871_StaticFields::get_offset_of_onHover_62(),
	UICamera_t1356438871_StaticFields::get_offset_of_onPress_63(),
	UICamera_t1356438871_StaticFields::get_offset_of_onSelect_64(),
	UICamera_t1356438871_StaticFields::get_offset_of_onScroll_65(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDrag_66(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragStart_67(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragOver_68(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragOut_69(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragEnd_70(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDrop_71(),
	UICamera_t1356438871_StaticFields::get_offset_of_onKey_72(),
	UICamera_t1356438871_StaticFields::get_offset_of_onNavigate_73(),
	UICamera_t1356438871_StaticFields::get_offset_of_onPan_74(),
	UICamera_t1356438871_StaticFields::get_offset_of_onTooltip_75(),
	UICamera_t1356438871_StaticFields::get_offset_of_onMouseMove_76(),
	UICamera_t1356438871_StaticFields::get_offset_of_mMouse_77(),
	UICamera_t1356438871_StaticFields::get_offset_of_controller_78(),
	UICamera_t1356438871_StaticFields::get_offset_of_activeTouches_79(),
	UICamera_t1356438871_StaticFields::get_offset_of_mTouchIDs_80(),
	UICamera_t1356438871_StaticFields::get_offset_of_mWidth_81(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHeight_82(),
	UICamera_t1356438871_StaticFields::get_offset_of_mTooltip_83(),
	UICamera_t1356438871::get_offset_of_mCam_84(),
	UICamera_t1356438871_StaticFields::get_offset_of_mTooltipTime_85(),
	UICamera_t1356438871::get_offset_of_mNextRaycast_86(),
	UICamera_t1356438871_StaticFields::get_offset_of_isDragging_87(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastInteractionCheck_88(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastInteractionResult_89(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastFocusCheck_90(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastFocusResult_91(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastOverCheck_92(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastOverResult_93(),
	UICamera_t1356438871_StaticFields::get_offset_of_mRayHitObject_94(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHover_95(),
	UICamera_t1356438871_StaticFields::get_offset_of_mSelected_96(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHit_97(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHits_98(),
	UICamera_t1356438871_StaticFields::get_offset_of_mRayHits_99(),
	UICamera_t1356438871_StaticFields::get_offset_of_mOverlap_100(),
	UICamera_t1356438871_StaticFields::get_offset_of_m2DPlane_101(),
	UICamera_t1356438871_StaticFields::get_offset_of_mNextEvent_102(),
	UICamera_t1356438871_StaticFields::get_offset_of_mNotifying_103(),
	UICamera_t1356438871_StaticFields::get_offset_of_mUsingTouchEvents_104(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetInputTouchCount_105(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetInputTouch_106(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_107(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_108(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_109(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_110(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (ControlScheme_t4038670825)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2592[4] = 
{
	ControlScheme_t4038670825::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (ClickNotification_t3778629126)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2593[4] = 
{
	ClickNotification_t3778629126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (MouseOrTouch_t3052596533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[17] = 
{
	MouseOrTouch_t3052596533::get_offset_of_key_0(),
	MouseOrTouch_t3052596533::get_offset_of_pos_1(),
	MouseOrTouch_t3052596533::get_offset_of_lastPos_2(),
	MouseOrTouch_t3052596533::get_offset_of_delta_3(),
	MouseOrTouch_t3052596533::get_offset_of_totalDelta_4(),
	MouseOrTouch_t3052596533::get_offset_of_pressedCam_5(),
	MouseOrTouch_t3052596533::get_offset_of_last_6(),
	MouseOrTouch_t3052596533::get_offset_of_current_7(),
	MouseOrTouch_t3052596533::get_offset_of_pressed_8(),
	MouseOrTouch_t3052596533::get_offset_of_dragged_9(),
	MouseOrTouch_t3052596533::get_offset_of_pressTime_10(),
	MouseOrTouch_t3052596533::get_offset_of_clickTime_11(),
	MouseOrTouch_t3052596533::get_offset_of_clickNotification_12(),
	MouseOrTouch_t3052596533::get_offset_of_touchBegan_13(),
	MouseOrTouch_t3052596533::get_offset_of_pressStarted_14(),
	MouseOrTouch_t3052596533::get_offset_of_dragStarted_15(),
	MouseOrTouch_t3052596533::get_offset_of_ignoreDelta_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (EventType_t561105572)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2595[5] = 
{
	EventType_t561105572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (GetKeyStateFunc_t2810275146), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (GetAxisFunc_t2592608932), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (GetAnyKeyFunc_t1761480072), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (GetMouseDelegate_t662790529), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
