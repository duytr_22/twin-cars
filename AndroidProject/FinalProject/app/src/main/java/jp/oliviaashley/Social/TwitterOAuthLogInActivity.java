package jp.oliviaashley.Social;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import jp.oliviaashley.Twins.R;


public class TwitterOAuthLogInActivity extends Activity {
    private static final String TAG = "TwitterOAuthLogInAct";

    public final static String TWITTERWEB_VERIFIER = "oauth_verifier";


    private Intent mIntent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String url;
        {
            setContentView(R.layout.twitter_webview);
            mIntent = getIntent();
            url = (String) mIntent.getExtras().get("URL");
        }
        Log.d(TAG, "onCreate() url: " + url);

        WebViewClient client = new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains(getString(R.string.twitter_callback_url))) {
                    Uri uri = Uri.parse(url);
                    String oauthVerifier = uri.getQueryParameter(TWITTERWEB_VERIFIER);

                    Log.d(TAG, "shouldOverrideUrlLoading() url: " + url);
                    Log.d(TAG, "shouldOverrideUrlLoading() oauthVerifier: " + oauthVerifier);

                    mIntent.putExtra("oauth_verifier", oauthVerifier);
                    setResult(RESULT_OK, mIntent);
                    finish();

                    return true;
                }

                return false;
            }
        };

        //WebViewで表示
        {
            WebView webView = (WebView) findViewById(R.id.webview);

            webView.setWebViewClient(client);
            webView.loadUrl(url);
        }
    }
}
