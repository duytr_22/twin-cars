﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXRegister : MonoBehaviour
{
	[SerializeField] VFXSystem.VFXCode ID;

	void Start()
	{
		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeGame)
			VFXSystem.Instance.RegisterItemManually(gameObject, ID);
	}
}