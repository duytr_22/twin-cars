﻿//  NextLevelButtonAssistant.cs
//  ProductName うどん
//
//  Created by kan.kikuchi on 2016.01.08.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 次のレベルボタンのアシスタントクラス
/// </summary>
public class NextLevelButtonAssistant : ButtonAssistant
{

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		CacheData.Entity.StageNo++;
		SceneNavigator.Instance.MoveScene (SceneName.GAME);
	}
}
