﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseRectButtonAssisstant : ButtonAssistant
{
	[SerializeField]
	GameObject _dialogRect;

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		_dialogRect.SetActive (false);
		AdManager.Instance.HidePopupRect ();
	}

}
