﻿//  AndroidStudioProjectUpdater.cs
//  ProductName Template
//
//  Created by Aaron on 2016.05.17.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;



/// <summary>
/// Updates the Android Studio Project files overriding libraries and assets.
/// </summary>
public class AndroidStudioProjectUpdater : MonoBehaviour {

	private static string FINAL_PATH = "AndroidProject/FinalProject";

	private static string MANIFEST_PATH = Path.Combine(FINAL_PATH, "app/src/main/AndroidManifest.xml");

	private static string GRADLE_CONFIG_PATH = Path.Combine(FINAL_PATH, "app/build.gradle");

	private static string[][] REPLACE_FILES_AND_DIRECTORIES = new string[][]{
		new string[] {"replace_file", "libs/unity-classes.jar", "app/libs/unity-classes.jar"},
		new string[] {"replace_file", "libs/armeabi-v7a/libmain.so", "app/src/main/jniLibs/armeabi-v7a/libmain.so"},
		new string[] {"replace_file", "libs/armeabi-v7a/libmono.so", "app/src/main/jniLibs/armeabi-v7a/libmono.so"},
		new string[] {"replace_file", "libs/armeabi-v7a/libunity.so", "app/src/main/jniLibs/armeabi-v7a/libunity.so"},
		new string[] {"replace_file", "libs/x86/libmain.so", "app/src/main/jniLibs/x86/libmain.so"},
		new string[] {"replace_file", "libs/x86/libmono.so", "app/src/main/jniLibs/x86/libmono.so"},
		new string[] {"replace_file", "libs/x86/libunity.so", "app/src/main/jniLibs/x86/libunity.so"},
		new string[] {"replace_file", "res/drawable/app_icon.png", "app/src/main/res/drawable/app_icon.png"},
		new string[] {"replace_file", "res/drawable/app_icon.png", "app/src/main/res/drawable-mdpi/app_icon.png"},
		new string[] {"replace_file", "res/drawable-hdpi/app_icon.png", "app/src/main/res/drawable-hdpi/app_icon.png"},
		new string[] {"replace_file", "res/drawable-ldpi/app_icon.png", "app/src/main/res/drawable-ldpi/app_icon.png"},
		new string[] {"replace_file", "res/drawable-xhdpi/app_icon.png", "app/src/main/res/drawable-xhdpi/app_icon.png"},
		new string[] {"replace_file", "res/drawable-xxhdpi/app_icon.png", "app/src/main/res/drawable-xxhdpi/app_icon.png"},
		new string[] {"replace_file", "res/drawable-xxxhdpi/app_icon.png", "app/src/main/res/drawable-xxxhdpi/app_icon.png"},
		new string[] {"replace_directory", "assets", "app/src/main/assets"}
	};

	private static string[] REPLACE_APP_NAME = new string[]{
		"app/src/main/res/values/strings.xml",
		"app/src/main/res/values-en/strings.xml",
		"app/src/main/res/values-ja/strings.xml"
	};

	[PostProcessBuildAttribute(1)]
	private static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject){

		if (target != BuildTarget.Android){
			return;
		}

		string sourcePath = Path.Combine (pathToBuiltProject, PlayerSettings.productName);

		Debug.Log( "OnPostprocessBuild ANDROID <" + sourcePath + ">");

		Debug.Log ("Copying and reaplacing libraries, assets and icons...");

		foreach (string[] r in REPLACE_FILES_AND_DIRECTORIES) {
			if (r [0] == "replace_file") {
//				Debug.Log (r[0] + " " + r[1] + " "+ r[2]);
				FileUtil.ReplaceFile (Path.Combine(sourcePath, r [1]), Path.Combine(FINAL_PATH, r [2]));
			} else if (r [0] == "replace_directory") {
//				Debug.Log (r[0] + " " + r[1] + " "+ r[2]);
				FileUtil.ReplaceDirectory (Path.Combine(sourcePath, r [1]), Path.Combine(FINAL_PATH, r [2]));
			}
		}

		Debug.Log ("Updating manifest package name and intent filter...");

		{
			XmlDocument xmlDocument = new XmlDocument ();
			xmlDocument.Load (MANIFEST_PATH);
			xmlDocument.DocumentElement.Attributes ["package"].Value = PlayerSettings.applicationIdentifier;

			List<string> scheme = PlayerSettings.applicationIdentifier.Split(".");

			foreach(XmlNode n in xmlDocument.DocumentElement.GetElementsByTagName("data")) {
				if (Regex.Match(n.Attributes ["android:host"].Value, "jp.co.goodia.*").Success) {
					n.Attributes ["android:host"].Value = PlayerSettings.applicationIdentifier;
					n.Attributes ["android:scheme"].Value = scheme[scheme.Count -1].ToLower();
				}
			}

			xmlDocument.Save (MANIFEST_PATH);
		}

		Debug.Log ("Updating gradle app ID...");
		{
			string reference = File.ReadAllText(GRADLE_CONFIG_PATH);
			string pattern = "(applicationId \".*\")";
			string evaluator = "applicationId \"" + PlayerSettings.applicationIdentifier + "\"";
			string patata = Regex.Replace (reference, pattern, evaluator);
			if (reference != patata) {
				File.WriteAllText (GRADLE_CONFIG_PATH, patata);
				Debug.LogWarning ("Updated gradle configuration files. May be necesssary to sync gradle in Android Studio again");
			}
		}

		Debug.Log ("Updating string app name...");

		foreach (string path in REPLACE_APP_NAME) {
			string myPath = Path.Combine (FINAL_PATH, path);
			XmlDocument xmlDocument = new XmlDocument ();
			xmlDocument.Load (myPath);
			foreach(XmlNode n in xmlDocument.DocumentElement.GetElementsByTagName("string")) {
				if (n.Attributes ["name"].Value == "app_name") {
					n.InnerXml = PlayerSettings.productName;
				}
			}
			xmlDocument.Save (myPath);
		}

		Debug.Log ("Deleting temporary files...");

		FileUtil.DeleteFileOrDirectory (sourcePath);

		Debug.Log ("OnPostprocessBuild END");
	}

}