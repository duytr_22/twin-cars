﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "CarsRenderPattern", menuName = "twin-cars/CRP.", order = 0)]
public class CarsRenderPattern : ScriptableObject
{
    public enum PatternID
    {
        Pattern1 = 0,
        Pattern2a = 1,
        Pattern2b = 2,
        Pattern3a = 3,
        Pattern3b = 4,
    }

    public bool isPlayer;

    [Serializable]
    public struct PatternData
    {
        public PatternID ID;
        public List<GameObject> prefabList;
        public List<GameObject> driverList;
        bool isError;

        PatternData(PatternID id, List<GameObject> listOfPrefabs, List<GameObject> listOfDrivers, bool err)
        {
            ID = id;
            prefabList = listOfPrefabs;
            driverList = listOfDrivers;
            isError = err;
        }

        public static PatternData Error
        {
            get => new PatternData(PatternID.Pattern1, null, null, true);
        }

        public GameObject GetPrefab()
        {
            return prefabList[Random.Range(0, prefabList.Count)];
        }

        public GameObject GetDriver()
        {
            return driverList.Count < 1 ? null : driverList[Random.Range(0, driverList.Count)];
        }

        public static bool operator ==(PatternData a, PatternData b)
        {
            return (a.isError == b.isError) && (a.ID == b.ID);
        }

        public static bool operator !=(PatternData a, PatternData b)
        {
            return !(a == b);
        }
    }

    internal PatternData GetPattern(PatternID id)
    {
        var conv = (int)id;
        Debug.Log(conv);
        if (conv > _patternList.Count)
            return PatternData.Error;
        return _patternList[conv];
    }

    [SerializeField] List<PatternData> _patternList;
}