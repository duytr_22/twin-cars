package jp.oliviaashley.Advertising.Providers;

import jp.oliviaashley.Twins.R;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.FrameLayout;

import com.google.android.gms.ads.*;
import android.view.Gravity;
import android.view.ViewGroup;

import jp.oliviaashley.Advertising.SplashAdManager;

/**
 * <p/>
 * AndroidManifest.xml:
 * <uses-permission android:name="android.permission.INTERNET"/>
 * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 * <p/>
 * <application>
 * <meta-data android:name="com.google.android.gms.version"
 * android:value="@integer/google_play_services_version" />
 * </application>
 *
 * @author Aaron Santin - 2014
 * @version 1.0
 */
public class AdmobHelper {
    // XXX iMobile also has wall, banner, bigbanner, rectanglebanner, implement
    // it in the future

    private static final String TAG = "AdmobHelper";

    // Default values for icons
    private static final String titleColor = "#FFFFFF";
    private static final boolean titleVisible = false;
    private static final int icons = 1;
    private static final int iconSize = 57;
    private static InterstitialAd mInterstitialAd  = null;
    private static AdView mBanner  = null;
    // 緊急対応(LinearLayout.VERTICAL未対応)
    // Notice: 1 LayoutView needs 1 spotID!! OMG!!
    private static int spotIdIndex = 0;

    /**
     * Put this method on your activity onCreate method to initialize the
     * iMobile provider
     *
     * @param activity The main activity
     */
    public static void doOnCreate(Activity activity) {
        Log.v(TAG, "doOnCreate()");
        MobileAds.initialize(activity, activity.getResources().getString(R.string.Admob_AppID));
        initFullScreenAdmob(activity);
    }

    /**
     * Put this method on your activity onPause method
     */
    public static void doOnPause() {
    }

    /**
     * Put this method on your activity onResume method
     */
    public static void doOnResume() {
    }

    /**
     * Put this method on your activity onDestroy method
     */
    public static void doOnDestroy() {
        Log.v(TAG, "doOnDestroy()");
        // for leaked windows
    }
    public static LinearLayout getBannerView(Activity activity){

        Log.v(TAG, "Load banner");
        LinearLayout layout = new LinearLayout(activity);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        mBanner = new AdView(activity);
        mBanner.setAdSize(AdSize.BANNER);
        mBanner.setAdUnitId(activity.getResources().getString(R.string.Admob_Banner));
        mBanner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.v(TAG, "Load success " + mBanner.getMediationAdapterClassName());
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.v(TAG, "Load failed " + mBanner.getMediationAdapterClassName());
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        mBanner.loadAd(adRequest);

        layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layout.addView(mBanner, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layout;
    }

    public static LinearLayout getAdRectangleView(Activity activity){
        Log.v(TAG, "Load Rectangle");
        LinearLayout layout = new LinearLayout(activity);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        final AdView adView = new AdView(activity);
        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        adView.setAdUnitId(activity.getResources().getString(R.string.Admob_Rectangle));
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.v(TAG, "Load rectangle success " + adView.getMediationAdapterClassName());
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.v(TAG, "Load rectangle failed " + adView.getMediationAdapterClassName());
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layout.addView(adView, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layout;
    }

    public static LinearLayout getAdRectanglePopupView(Activity activity){
        Log.v(TAG, "Load Rectangle");
        LinearLayout layout = new LinearLayout(activity);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        final AdView adView = new AdView(activity);
        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        adView.setAdUnitId(activity.getResources().getString(R.string.Admob_Rectangle_Popup));
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.v(TAG, "Load rectangle success " + adView.getMediationAdapterClassName());
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.v(TAG, "Load rectangle failed " + adView.getMediationAdapterClassName());
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layout.addView(adView, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layout;
    }

    public static LinearLayout getAdRectangleEndView(Activity activity) {
        Log.v(TAG, "Load Rectangle Ending");
        LinearLayout layout = new LinearLayout(activity);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        final AdView adView = new AdView(activity);
        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        adView.setAdUnitId(activity.getResources().getString(R.string.Admob_Rectangle_End));
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.v(TAG, "Load rectangle ending success " + adView.getMediationAdapterClassName());
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.v(TAG, "Load rectangle ending failed " + adView.getMediationAdapterClassName());
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layout.addView(adView, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layout;
    }
    private static void initFullScreenAdmob(Activity activity) {

        mInterstitialAd = new InterstitialAd(activity);
        mInterstitialAd.setAdUnitId(activity.getResources().getString(R.string.Admob_InterstitialID));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.v(TAG, "Load interstitial success " + mInterstitialAd.getMediationAdapterClassName());
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.v(TAG, "Load interstitial failed with error " + errorCode);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.v(TAG, "interstitial close");

                if(mInterstitialAd != null)
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());

                if(mBanner != null)
                    mBanner.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public static void showAd(final Activity activity, final SplashAdManager.SplashAdListener listener){

        activity.runOnUiThread(new Runnable() {
            @Override public void run() {
                if (mInterstitialAd.isLoaded()) {
                    Log.v(TAG, "Load show success");
                    mInterstitialAd.show();
                }
            }
        });
    }
}
