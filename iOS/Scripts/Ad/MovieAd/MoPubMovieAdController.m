//
//  AdstirMovieAdController.m
//  Unity-iPhone
//
//  Created  on 2015/06/30.
//
//  Adstirの動画広告のコントローラー

#import "MoPubMovieAdController.h"

@implementation MoPubMovieAdController


//=================================================================================
//共通処理
//=================================================================================

//初期化
- (id)init{
    self = [super init];
    
    _isReceivedReward = NO;
    
//    [MoPub sharedInstance] initializeRewardedVideoWithGlobalMediationSettings:nil delegate:self];
    
    // Fetch the rewarded video ad.    
//    [MPRewardedVideo loadRewardedVideoAdWithAdUnitID:MOPUB_VIDEO_ID withMediationSettings:nil];
    //動画を読み込む
    [self loadVideoRandomly];
    
    return self;
}

-(BOOL)canShow
{
    return  [MPRewardedVideo hasAdAvailableForAdUnitID:MOPUB_VIDEO_ID];
}
//表示
-(void)show{
    // 広告の表示
    if([MPRewardedVideo hasAdAvailableForAdUnitID:MOPUB_VIDEO_ID])
    {
        [MPRewardedVideo presentRewardedVideoAdForAdUnitID:MOPUB_VIDEO_ID fromViewController:UnityGetGLViewController() withReward:nil];
    }else {
        [self loadVideoRandomly];
    }
}

//=================================================================================
//内部処理
//=================================================================================

/** ランダムな間隔で動画を読み込むメソッド */
- (void)loadVideoRandomly{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((arc4random() % 10) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       [MPRewardedVideo loadRewardedVideoAdWithAdUnitID:MOPUB_VIDEO_ID withMediationSettings:nil];
        [MPRewardedVideo setDelegate:self forAdUnitId:MOPUB_VIDEO_ID];
    });
}

#pragma mark - Delegate
- (void)rewardedVideoAdDidLoadForAdUnitID:(NSString *)adUnitID
{
    NSLog(@"MoPub Reward Video: did load with ID %@", adUnitID);
    _isReceivedReward = true;
}

- (void)rewardedVideoAdDidFailToLoadForAdUnitID:(NSString *)adUnitID error:(NSError *)error
{
    NSLog(@"MoPub Reward Video: fail to load with ID %@ error %@", adUnitID, error.description);
    _isReceivedReward = false;
}

- (void)rewardedVideoAdDidDisappearForAdUnitID:(NSString *)adUnitID
{
    [MovieAdController sendFinishedPlayingMessage:_isReceivedReward];
    [self loadVideoRandomly];
}

- (void)rewardedVideoAdDidExpireForAdUnitID:(NSString *)adUnitID
{
    NSLog(@"MoPub Reward Video: exprire with ID %@", adUnitID);
}

- (void)rewardedVideoAdDidFailToPlayForAdUnitID:(NSString *)adUnitID error:(NSError *)error
{
    NSLog(@"MoPub Reward Video: play failed with ID %@", adUnitID);
}
@end
