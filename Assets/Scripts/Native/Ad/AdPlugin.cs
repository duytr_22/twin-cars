﻿//  AdPlugin.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.27.

//モバイルの時だけMOBILE_DEVICEを宣言する
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL || UNITY_WEBPLAYER
#undef MOBILE_DEVICE
#else
#define MOBILE_DEVICE
#endif

// #undef MOBILE_DEVICE//lazycat cheat

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// 広告関連のネイティブプラグイン
/// </summary>
public class AdPlugin : NativePlugin
{

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		base.Init (_AdPlugin_Init, "");
		#elif UNITY_ANDROID && MOBILE_DEVICE
		base.Init (null, "");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern IntPtr _AdPlugin_Init (string gameObjectName);
	#endif

	//=================================================================================
	//両OSの処理
	//=================================================================================

	/// <summary>
	/// シーンに合わせて広告の更新
	/// </summary>
	public void UpdateAdByScene (int scene)
	{
		Debug.Log ("UpdateAdByScene : " + scene);

		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_UpdateAdByScene(_nativeInstance, scene);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("updateAd", scene);
		#endif

	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _UpdateAdByScene (IntPtr instance, int scene);
	#endif

	/// <summary>
	/// スプラッシュ広告を表示
	/// </summary>
	public void ShowSplashAd (int scene, int playCount)
	{	
		Debug.Log ("ShowSplashAd : " + scene);

		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowSplashAd(_nativeInstance, scene, playCount);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		if(scene == SceneNo.TITLE){
			CommonNativeManager.Instance.CommonAndroidPlugin.Call("showOptionalAdRank");
		}
		else{
			CommonNativeManager.Instance.CommonAndroidPlugin.Call("showOptionalAd", playCount);
		}
		#endif

	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _ShowSplashAd (IntPtr instance, int scene, int playCount);
	#endif

	/// <summary>
	/// リーダーボードを表示した時に広告を重ねる
	/// </summary>
	public void AddAdToLeaderBoard ()
	{
		Debug.Log ("AddAdToLeaderBoard");

		#if UNITY_IPHONE && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_AddAdToLeaderBoard(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("showOptionalAdRank");
		#endif
	}
	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _AddAdToLeaderBoard (IntPtr instance);
	#endif

	//=================================================================================
	//Android
	//=================================================================================

	/// <summary>
	/// ウォール広告表示
	/// </summary>
	public void ShowWallAd ()
	{
		Debug.Log ("ShowWallAd");

		#if UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call ("showWallAd");
		#endif		
	}

	#region Reward video

	public bool CanShowRewardVideo ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return false;
		}
		else{
			return _CanShowRewardVideo(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		return CommonNativeManager.Instance.CommonAndroidPlugin.CallStatic<bool>("_CanShowRewardVideo");
		#endif
		return false;
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern bool _CanShowRewardVideo (IntPtr instance);
	#endif

	public void ShowRewardVideo ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowRewardVideo(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("ShowRewardVideo");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _ShowRewardVideo (IntPtr instance);
	#endif

	#endregion

	public void HideAllAds ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_HideAllAds(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("HideAllAds");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _HideAllAds (IntPtr instance);
	#endif

	public void ShowIndicator ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowIndicator(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("ShowIndicator");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _ShowIndicator (IntPtr instance);
	#endif

	public void HideIndicator ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_HideIndicator(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("HideIndicator");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _HideIndicator (IntPtr instance);
	#endif

	public void ShowGameRect ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowGameRect(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("ShowGameRect");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _ShowGameRect (IntPtr instance);
	#endif

	public void HideGameRect ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_HideGameRect(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("HideGameRect");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _HideGameRect (IntPtr instance);
	#endif


	// Android IAP
	public void Buy ()
	{
		#if UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("Buy");
		#endif
	}

	public void Restore ()
	{
		#if UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("Restore");
		#endif
	}

	public void HidePopupRect ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_HidePopupRect(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("HidePopupRect");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _HidePopupRect (IntPtr instance);
	#endif

	public void ShowPopupRect ()
	{
		#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowPopupRect(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("ShowPopupRect");
		#endif
	}

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _ShowPopupRect (IntPtr instance);
	#endif
}