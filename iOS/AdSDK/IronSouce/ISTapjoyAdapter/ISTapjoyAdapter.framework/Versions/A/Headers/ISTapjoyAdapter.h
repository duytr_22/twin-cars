//
//  ISTapjoyAdapter.h
//  ISTapjoyAdapter
//
//  Created by Daniil Bystrov on 4/13/16.
//  Copyright © 2016 IronSource. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IronSource/ISBaseAdapter+Internal.h"

//System Frameworks For Tapjoy Adapter

@import CoreMotion;
@import Security;
@import CoreData;
@import CFNetwork;
@import CoreGraphics;
@import CoreLocation;
@import EventKitUI;
@import EventKit;
@import Foundation;
@import MapKit;
@import MediaPlayer;
@import MessageUI;
@import MobileCoreServices;
@import QuartzCore;
@import SystemConfiguration;
@import UIKit;
@import AdSupport;
@import CoreTelephony;
@import Social;
@import StoreKit;
@import ImageIO;

@interface ISTapjoyAdapter : ISBaseAdapter

@end
