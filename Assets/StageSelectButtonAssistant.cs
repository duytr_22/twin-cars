﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSelectButtonAssistant : ButtonAssistant
{
	[SerializeField]
	GameObject _icon, _check;

	private UISprite _sprite;

	private int idx;
	private string name = "skin_stage0{0}";
	private string nameLock = "skin_stage0{0}_lock";

	void Start ()
	{
		idx = int.Parse (gameObject.name);

		_sprite = GetComponent<UISprite> ();

		BallController.Instance.StageList.Add (this);

		// Check available
		Refresh ();
	}

	void Refresh ()
	{
		int unlock = PlayerPrefs.GetInt (string.Format (Define.STAGE_UNLOCK, idx), 0);

		if (idx == 1) // First stage
			unlock = 2;

		if (unlock == 0) { // Lock

			DisableButton ();

			_sprite.spriteName = string.Format (nameLock, idx);

			UpdateButton (_sprite.spriteName);

			_icon.SetActive (false);
			_check.SetActive (false);

		} else if (unlock == 1) { // New

			_sprite.spriteName = string.Format (name, idx);

			EnableButton ();

			_icon.SetActive (true);
			_check.SetActive (false);

		} else if (unlock == 2) { // Unlock

			EnableButton ();

			_sprite.spriteName = string.Format (name, idx);

			UpdateButton (_sprite.spriteName);

			_icon.SetActive (false);
			_check.SetActive (false);
		}

		if (PlayerPrefs.GetInt (Define.STAGE_SELECTED, 1) == idx) {

			_check.SetActive (true);
		}
	}

	protected override void OnClickButton ()
	{
		BallController.Instance.UncheckAllStage ();

		PlayerPrefs.SetInt (string.Format (Define.STAGE_UNLOCK, idx), 2);

		PlayerPrefs.SetInt (Define.STAGE_SELECTED, idx);

		PlayerPrefs.Save ();

		Refresh ();

		BallController.Instance.CheckShowIcon ();
	}

	public void Uncheck ()
	{
		_check.SetActive (false);
	}
}
