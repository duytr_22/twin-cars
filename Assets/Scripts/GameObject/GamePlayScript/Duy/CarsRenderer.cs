﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarsRenderer : MonoBehaviour
{
    [SerializeField] List<GameObject> _carModels;
    [SerializeField] CarCrashingPhysiscsBehavior _carCrashPhysics;
    Animator _modelAnimator;
    GameObject _mainModel;

    public GameObject MainModel
    {
        get => _mainModel;
        set => _mainModel = value;
    }

    void Awake()
    {
        var id = UnityEngine.Random.Range(0, _carModels.Count);
        MainModel = _carModels[id];
        MainModel.SetActive(true);
        _modelAnimator = MainModel.GetComponent<Animator>();
        // MainModel.GetComponent<BoxCollider>().enabled = false;
    }

    void Update()
    {
        if (GameManager.Instance.IsPlayingGame == false && _modelAnimator.enabled == true)
            _modelAnimator.enabled = false;
    }

    void OnEnable()
    {
        _carCrashPhysics.onCollisionEnterEvent += OnCarCrashed;
    }

    void OnCarCrashed()
    {
        _modelAnimator.enabled = false;
    }
}
