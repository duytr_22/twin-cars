﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultController : SingletonMonoBehaviour<ResultController>
{

	[SerializeField]
	GameObject _dialogRect, closeButton;

	[SerializeField]
	GameObject _blockRect;

	private bool _showInterstitial = true;
	public bool ShowInterstial{
		get{ return _showInterstitial;}
		set{ _showInterstitial = value;}
	}
	// Use this for initialization
	IEnumerator Start ()
	{
//		PlayerPrefs.SetInt (Define.CONTINUE_GAME, 0);
		
		if (UserData.PlayCount % 3 == 0 && PlayerPrefs.GetInt (Define.NO_ADS, 0) == 0 && SceneNavigator.Instance.BeforeSceneName != SceneName.BALL) {
		
			yield return new WaitForSeconds (0.01f);
//
//			AdManager.Instance.ShowPopupRect ();
//
//			_dialogRect.SetActive (true);
//
//			_showInterstitial = false;
//			float delayTime = 1.5f;
//
//			UIButton button = closeButton.GetComponent<UIButton> ();
//			button.isEnabled = false;
//
//			//演出が終わったらリザルトへ
//			StartCoroutine (this.DelayMethod (delayTime, () => {
//
//				button.isEnabled = true;
//				button.defaultColor = Color.white;
//			}));

			_blockRect.SetActive (false);

		} else {

			_blockRect.SetActive (false);
		}
	}
}
