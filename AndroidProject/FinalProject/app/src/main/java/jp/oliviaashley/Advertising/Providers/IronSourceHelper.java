package jp.oliviaashley.Advertising.Providers;


import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.BannerListener;

import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.unity3d.player.UnityPlayer;

import jp.oliviaashley.Advertising.SplashAdManager;
import jp.oliviaashley.Twins.R;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Library dependencies:
 * 		ADG-2.0.0.jar
 * 		google-play-services
 *
 * AndroidManifest.xml:
 * 		<uses-permission android:name="android.permission.INTERNET"/>
 * 		<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 * 		<application>
 * 			<meta-data android:name="com.google.android.gms.version"
 * 				android:value="@integer/google_play_services_version" />
 *          <activity android:name="com.socdm.d.adgeneration.ADGClickWebView"
 *        		android:configChanges="orientation|keyboardHidden" />
 *		 </application>
 *
 * @author Aaron Santin - 2014
 * @version 1.0
 */
public class IronSourceHelper {

	// XXX AdGeneration also has banners, implement it

	private static final String TAG = "IronSourceHelper";

	// ADGRectangle
	private static IronSourceBannerLayout banner  = null;

	private static Activity _activity = null;
	/**
	 * Put this on your activity's onCreate method to preload intertitials
	 *
	 * @param activity
	 * 				the main activity
	 */
	public static void doOnCreate(Activity activity) {

		_activity = activity;
		Log.v(TAG, "do on create..");
		banner = IronSource.createBanner(activity, ISBannerSize.BANNER);

		IronSource.setInterstitialListener(new InterstitialListener() {
			@Override
			public void onInterstitialAdReady() {

				Log.v(TAG, "Interstitial Ad Ready...");
			}

			@Override
			public void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {

				Log.v(TAG, "Interstitial Ad Failed ..." + ironSourceError);
				IronSource.loadInterstitial();
			}

			@Override
			public void onInterstitialAdOpened() {

				Log.v(TAG, "Interstitial Ad Open...");
				UnityPlayer.UnitySendMessage("NativeManager", "PauseGame", "");
				UnityPlayer.UnitySendMessage("NativeManager", "startInterstital", "");
			}

			@Override
			public void onInterstitialAdClosed() {

				Log.v(TAG, "Interstitial Ad Close...");
				IronSource.loadInterstitial();
				UnityPlayer.UnitySendMessage("NativeManager", "interstitalCompleted", "");

				// Save last mil
				SharedPreferences sharedPreferences = _activity.getPreferences(Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.putLong("Interstitial_delay", System.currentTimeMillis());
				editor.commit();
			}

			@Override
			public void onInterstitialAdShowSucceeded() {

				Log.v(TAG, "Interstitial Ad Show Success...");
			}

			@Override
			public void onInterstitialAdShowFailed(IronSourceError ironSourceError) {

				Log.v(TAG, "Interstitial Ad Show Failed ..." + ironSourceError);
				UnityPlayer.UnitySendMessage("NativeManager", "interstitalCompleted", "");
			}

			@Override
			public void onInterstitialAdClicked() {

				Log.v(TAG, "Interstitial Ad Clicked...");
			}
		});
	}

	public static void doOnStop(){
	}

	public static void doOnResume() {
		// 広告表示/ローテーション再開
		if (banner!= null) {

		}
	}
	public static void doOnDestroy() {
		Log.v(TAG, "doOnDestroy()");
		// for leaked windows
		if (banner != null) {

			IronSource.destroyBanner(banner);
		}
	}

	public static void doOnPause() {
		// ローテーション停止
	}

	public static LinearLayout getBannerView(Activity activity){
		Log.v("IronSourceHelper", "Showing IronSource Banner ...");
		LinearLayout layout = new LinearLayout(activity);
		layout.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));

		layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
		layout.addView(banner, new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		banner.setBannerListener(new BannerListener() {
			@Override
			public void onBannerAdLoaded() {

				Log.v(TAG, "Banner On ReceiveAD ...");
			}

			@Override
			public void onBannerAdLoadFailed(IronSourceError ironSourceError) {

				Log.v(TAG, "Banner On failed with error..." + ironSourceError);
			}

			@Override
			public void onBannerAdClicked() {

				Log.v(TAG, "Banner On Clicked...");
			}

			@Override
			public void onBannerAdScreenPresented() {

				Log.v(TAG, "Banner On Screen Presented...");
			}

			@Override
			public void onBannerAdScreenDismissed() {

				Log.v(TAG, "Banner On Screen Dismissed...");
			}

			@Override
			public void onBannerAdLeftApplication() {

				Log.v(TAG, "Banner Left Application...");
			}
		});
		IronSource.loadBanner(banner);
		return layout;
	}

	public static void loadBanner()
	{
		Log.v(TAG, "loading banner");
		IronSource.loadBanner(banner);

		if (!IronSource.isInterstitialReady()) {

			IronSource.loadInterstitial();
		}
	}

	public static void showAd(final Activity activity, final SplashAdManager.SplashAdListener listener) {
		Log.v(TAG, "Showing interstitial...");
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		boolean defaultValue = sharedPref.getBoolean(activity.getResources().getString(R.string.remove_ads), false);
		if (defaultValue){
			return;
		}

		if (IronSource.isInterstitialReady()){

			Log.v(TAG, "Show");

			SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
			long currentMil = System.currentTimeMillis();
			long lastMil = sharedPreferences.getLong("Interstitial_delay", 0);

			Log.d("Applovin", "Start" + (currentMil - lastMil)/1000);
			if((currentMil - lastMil) / 1000 >= 30){

				IronSource.showInterstitial();
			}
		}else {

			IronSource.loadInterstitial();
		}
	}
	/**
	 * Get a simple row AdGeneration AdGenerationRectangleAd with a custom color title and orientation
	 * @param activity
	 * 				the main activity
	 * @return
	 * 				the AdGenerationAdIconLayout to include in a layout
	 */
}
