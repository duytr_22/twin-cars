﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericVirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericVirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericVirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericVirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct InterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericInterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericInterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// UIToggle
struct UIToggle_t4192126258;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t4210400802;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UIWidgetContainer
struct UIWidgetContainer_t30162560;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// BetterList`1<UIToggle>
struct BetterList_1_t3347146576;
// BetterList`1<System.Object>
struct BetterList_1_t2235126482;
// UIToggle/Validate
struct Validate_t3702293971;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// TweenAlpha
struct TweenAlpha_t3706845226;
// System.String
struct String_t;
// ActiveAnimation
struct ActiveAnimation_t3475256642;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.Animation
struct Animation_t3648466861;
// UITweener[]
struct UITweenerU5BU5D_t3440034099;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UITweener
struct UITweener_t260334902;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UIToggledComponents
struct UIToggledComponents_t772955118;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>
struct List_1_t1139589975;
// EventDelegate/Callback
struct Callback_t3139336517;
// EventDelegate
struct EventDelegate_t2738326060;
// UIToggledObjects
struct UIToggledObjects_t3502557910;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UITooltip
struct UITooltip_t30236576;
// UnityEngine.Transform
struct Transform_t3600365921;
// UIWidget[]
struct UIWidgetU5BU5D_t2950248968;
// UnityEngine.Camera
struct Camera_t4157153871;
// UIWidget
struct UIWidget_t3538521925;
// UILabel
struct UILabel_t3248798549;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// UIViewport
struct UIViewport_t1918760134;
// UIGeometry
struct UIGeometry_t1059483952;
// UIRect
struct UIRect_t2875960382;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t133425655;
// System.Delegate
struct Delegate_t1188392813;
// UIPanel
struct UIPanel_t1716472341;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Type
struct Type_t;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t3581341831;
// UIRect/AnchorPoint
struct AnchorPoint_t1754718329;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// UIWidget/OnDimensionsChanged
struct OnDimensionsChanged_t3101921181;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t4027761066;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// UIWidget/HitCheck
struct HitCheck_t2300079615;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t2835645043;
// UIWrapContent
struct UIWrapContent_t1188558554;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UIPanel/OnClippingMoved
struct OnClippingMoved_t476625095;
// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t3375297100;
// System.Comparison`1<System.Object>
struct Comparison_1_t2855037343;
// UIScrollView
struct UIScrollView_t1973404950;
// UIWrapContent/OnInitializeItem
struct OnInitializeItem_t992046894;
// UnityProjectSetting
struct UnityProjectSetting_t2579824760;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// System.Collections.Generic.Dictionary`2<ShareLocation,System.String>
struct Dictionary_2_t3173579796;
// System.Collections.Generic.Dictionary`2<ShareLocation,System.Object>
struct Dictionary_2_t111267975;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// System.Collections.Generic.IDictionary`2<System.String,System.Int32>
struct IDictionary_2_t1200053443;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int32>
struct IDictionary_2_t2762203428;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// Velocity2DTmp
struct Velocity2DTmp_t2283591314;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// VelocityTmp
struct VelocityTmp_t3274308841;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3530625384;
// ShareLocation[]
struct ShareLocationU5BU5D_t2822662148;
// System.Collections.Generic.IEqualityComparer`1<ShareLocation>
struct IEqualityComparer_1_t1416014251;
// System.Collections.Generic.Dictionary`2/Transform`1<ShareLocation,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t3679379800;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// EventDelegate/Parameter[]
struct ParameterU5BU5D_t2885283399;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// UIToggle[]
struct UIToggleU5BU5D_t3308279239;
// System.Char[]
struct CharU5BU5D_t3528271667;
// EventDelegate[]
struct EventDelegateU5BU5D_t2229996453;
// System.Void
struct Void_t1185182177;
// UIGeometry/OnCustomWrite
struct OnCustomWrite_t2800546165;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// BetterList`1<UIRect>
struct BetterList_1_t2030980700;
// UIRoot
struct UIRoot_t4022971450;
// BetterList`1<UICamera>
struct BetterList_1_t511459189;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2810275146;
// UICamera/GetAxisFunc
struct GetAxisFunc_t2592608932;
// UICamera/GetAnyKeyFunc
struct GetAnyKeyFunc_t1761480072;
// UICamera/GetMouseDelegate
struct GetMouseDelegate_t662790529;
// UICamera/GetTouchDelegate
struct GetTouchDelegate_t4218246285;
// UICamera/RemoveTouchDelegate
struct RemoveTouchDelegate_t2508278027;
// UICamera/OnScreenResize
struct OnScreenResize_t2279991692;
// UICamera/OnCustomInput
struct OnCustomInput_t3508588789;
// UICamera/OnSchemeChange
struct OnSchemeChange_t1701155603;
// UICamera/MouseOrTouch
struct MouseOrTouch_t3052596533;
// UICamera/VoidDelegate
struct VoidDelegate_t3100799918;
// UICamera/BoolDelegate
struct BoolDelegate_t3825226153;
// UICamera/FloatDelegate
struct FloatDelegate_t906524069;
// UICamera/VectorDelegate
struct VectorDelegate_t435795517;
// UICamera/ObjectDelegate
struct ObjectDelegate_t2041570719;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t3064672302;
// UICamera/MoveDelegate
struct MoveDelegate_t16019400;
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t3534648920;
// System.Collections.Generic.List`1<UICamera/MouseOrTouch>
struct List_1_t229703979;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t4078737532;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295;
// UICamera/GetTouchCountCallback
struct GetTouchCountCallback_t3185863032;
// UICamera/GetTouchCallback
struct GetTouchCallback_t97678626;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t2967399990;
// BetterList`1/CompareFunc<UICamera>
struct CompareFunc_t3695088943;
// UISprite
struct UISprite_t194114938;
// BetterList`1<UIDrawCall>
struct BetterList_1_t448425637;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UIDrawCall/OnCreateDrawCall
struct OnCreateDrawCall_t609469653;
// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t1857321114;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// BetterList`1<UIScrollView>
struct BetterList_1_t1128425268;
// UIProgressBar
struct UIProgressBar_t1222110469;
// UIScrollView/OnDragNotification
struct OnDragNotification_t1437737811;
// UICenterOnChild
struct UICenterOnChild_t253063637;
// System.Collections.Generic.List`1<UIPanel>
struct List_1_t3188547083;
// UIPanel/OnGeometryUpdated
struct OnGeometryUpdated_t2462438111;
// System.Collections.Generic.List`1<UIWidget>
struct List_1_t715629371;
// System.Collections.Generic.List`1<UIDrawCall>
struct List_1_t2765480061;
// UIPanel/OnCreateMaterial
struct OnCreateMaterial_t2961246930;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Comparison`1<UIPanel>
struct Comparison_1_t1491403520;
// System.Comparison`1<UIWidget>
struct Comparison_1_t3313453104;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UIDrawCall
struct UIDrawCall_t1293405319;
// UIGrid/OnReposition
struct OnReposition_t1372889220;
// UnityEngine.Font
struct Font_t1956802104;
// UIFont
struct UIFont_t2766063701;
// UILabel/ModifierFunc
struct ModifierFunc_t2998065802;
// BetterList`1<UILabel>
struct BetterList_1_t2403818867;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Int32>
struct Dictionary_2_t2853457297;
// System.Action`1<UnityEngine.Font>
struct Action_1_t2129269699;
// UIAtlas
struct UIAtlas_t3195533529;
// UISpriteData
struct UISpriteData_t900308526;

extern RuntimeClass* List_1_t4210400802_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2541037942_RuntimeMethod_var;
extern String_t* _stringLiteral416754645;
extern const uint32_t UIToggle__ctor_m243404431_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisCollider_t1773347010_m4226749020_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCollider2D_t2806799626_m3391829639_RuntimeMethod_var;
extern const uint32_t UIToggle_get_isColliderEnabled_m2508236936_MetadataUsageId;
extern RuntimeClass* UIToggle_t4192126258_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BetterList_1_get_Item_m2983367427_RuntimeMethod_var;
extern const uint32_t UIToggle_GetActiveToggle_m1985772813_MetadataUsageId;
extern const RuntimeMethod* BetterList_1_Add_m1919439375_RuntimeMethod_var;
extern const uint32_t UIToggle_OnEnable_m3551203455_MetadataUsageId;
extern const RuntimeMethod* BetterList_1_Remove_m2601698983_RuntimeMethod_var;
extern const uint32_t UIToggle_OnDisable_m2874800824_MetadataUsageId;
extern RuntimeClass* EventDelegate_t2738326060_il2cpp_TypeInfo_var;
extern const uint32_t UIToggle_Start_m4167712475_MetadataUsageId;
extern RuntimeClass* UICamera_t1356438871_il2cpp_TypeInfo_var;
extern const uint32_t UIToggle_OnClick_m1132153859_MetadataUsageId;
extern RuntimeClass* NGUITools_t1206951095_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisUITweener_t260334902_m2859153738_RuntimeMethod_var;
extern const uint32_t UIToggle_Set_m2784184886_MetadataUsageId;
extern RuntimeClass* BetterList_1_t3347146576_il2cpp_TypeInfo_var;
extern const RuntimeMethod* BetterList_1__ctor_m2278349088_RuntimeMethod_var;
extern const uint32_t UIToggle__cctor_m2392246648_MetadataUsageId;
extern const uint32_t Validate_BeginInvoke_m1750573099_MetadataUsageId;
extern RuntimeClass* Callback_t3139336517_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m430733211_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m421447557_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisUIToggle_t4192126258_m3922355289_RuntimeMethod_var;
extern const RuntimeMethod* UIToggledComponents_Toggle_m3061868441_RuntimeMethod_var;
extern const uint32_t UIToggledComponents_Awake_m3830670224_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m3720941323_RuntimeMethod_var;
extern const uint32_t UIToggledComponents_Toggle_m3061868441_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m2812834599_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2765963565_RuntimeMethod_var;
extern const RuntimeMethod* UIToggledObjects_Toggle_m1261088717_RuntimeMethod_var;
extern const uint32_t UIToggledObjects_Awake_m1805886258_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m3743125852_RuntimeMethod_var;
extern const uint32_t UIToggledObjects_Toggle_m1261088717_MetadataUsageId;
extern const uint32_t UIToggledObjects_Set_m2281364091_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip__ctor_m3883812056_MetadataUsageId;
extern RuntimeClass* UITooltip_t30236576_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_get_isVisible_m2569275211_MetadataUsageId;
extern const uint32_t UITooltip_Awake_m2533168584_MetadataUsageId;
extern const uint32_t UITooltip_OnDestroy_m3731988078_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisUIWidget_t3538521925_m3698995734_RuntimeMethod_var;
extern const uint32_t UITooltip_Start_m589901658_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_Update_m2144972122_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1789911004;
extern const uint32_t UITooltip_SetText_m869056702_MetadataUsageId;
extern const uint32_t UITooltip_ShowText_m571599872_MetadataUsageId;
extern const uint32_t UITooltip_Show_m110090701_MetadataUsageId;
extern const uint32_t UITooltip_Hide_m3169225227_MetadataUsageId;
extern RuntimeClass* KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var;
extern RuntimeClass* AnimationCurve_t3046754366_il2cpp_TypeInfo_var;
extern const uint32_t UITweener__ctor_m3207690653_MetadataUsageId;
extern const uint32_t UITweener_get_amountPerDelta_m1343789098_MetadataUsageId;
extern const uint32_t UITweener_set_tweenFactor_m3630546225_MetadataUsageId;
extern RuntimeClass* UITweener_t260334902_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m1629109689_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1118803847_RuntimeMethod_var;
extern const uint32_t UITweener_DoUpdate_m1102478613_MetadataUsageId;
extern const uint32_t UITweener_SetOnFinished_m2969301297_MetadataUsageId;
extern const uint32_t UITweener_SetOnFinished_m1889836421_MetadataUsageId;
extern const uint32_t UITweener_AddOnFinished_m4181903323_MetadataUsageId;
extern const uint32_t UITweener_AddOnFinished_m3187080535_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m1907737694_RuntimeMethod_var;
extern const uint32_t UITweener_RemoveOnFinished_m2850141943_MetadataUsageId;
extern const uint32_t UITweener_Sample_m928485241_MetadataUsageId;
extern const uint32_t UITweener_Play_m646427819_MetadataUsageId;
extern const uint32_t UITweener_Toggle_m2162660133_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var;
extern const uint32_t UIViewport_Start_m2127192097_MetadataUsageId;
extern const uint32_t UIViewport_LateUpdate_m3432131260_MetadataUsageId;
extern RuntimeClass* UIGeometry_t1059483952_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern RuntimeClass* UIRect_t2875960382_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget__ctor_m2452914675_MetadataUsageId;
extern RuntimeClass* OnRenderCallback_t133425655_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_onRender_m176877109_MetadataUsageId;
extern RuntimeClass* Vector4_t3319028937_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_drawRegion_m1955755355_MetadataUsageId;
extern const uint32_t UIWidget_set_width_m181008468_MetadataUsageId;
extern const uint32_t UIWidget_set_height_m878974275_MetadataUsageId;
extern const uint32_t UIWidget_get_isVisible_m524426372_MetadataUsageId;
extern const uint32_t UIWidget_set_pivot_m951060879_MetadataUsageId;
extern const uint32_t UIWidget_set_depth_m2124062109_MetadataUsageId;
extern const uint32_t UIWidget_get_raycastDepth_m12826239_MetadataUsageId;
extern const uint32_t UIWidget_get_localSize_m3820549068_MetadataUsageId;
extern const uint32_t UIWidget_get_localCenter_m3035087122_MetadataUsageId;
extern const uint32_t UIWidget_get_drawingDimensions_m2099475420_MetadataUsageId;
extern const uint32_t UIWidget_set_material_m3876747158_MetadataUsageId;
extern const uint32_t UIWidget_get_mainTexture_m3193712698_MetadataUsageId;
extern RuntimeClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UIWidget_set_mainTexture_m1065044862_RuntimeMethod_var;
extern String_t* _stringLiteral4204125540;
extern const uint32_t UIWidget_set_mainTexture_m1065044862_MetadataUsageId;
extern const uint32_t UIWidget_get_shader_m4140967340_MetadataUsageId;
extern const RuntimeMethod* UIWidget_set_shader_m1925003059_RuntimeMethod_var;
extern String_t* _stringLiteral723286479;
extern const uint32_t UIWidget_set_shader_m1925003059_MetadataUsageId;
extern const uint32_t UIWidget_get_relativeSize_m887221726_MetadataUsageId;
extern RuntimeClass* BoxCollider_t1640800422_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273_RuntimeMethod_var;
extern const uint32_t UIWidget_get_hasBoxCollider_m3448657381_MetadataUsageId;
extern const uint32_t UIWidget_SetDimensions_m2627586629_MetadataUsageId;
extern const uint32_t UIWidget_GetSides_m2621800934_MetadataUsageId;
extern const uint32_t UIWidget_UpdateFinalAlpha_m4075768229_MetadataUsageId;
extern const uint32_t UIWidget_Invalidate_m1464153160_MetadataUsageId;
extern const uint32_t UIWidget_CalculateCumulativeAlpha_m49421948_MetadataUsageId;
extern const uint32_t UIWidget_SetRect_m1100895450_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisBoxCollider_t1640800422_m4104100802_RuntimeMethod_var;
extern const uint32_t UIWidget_ResizeCollider_m1303972682_MetadataUsageId;
extern RuntimeClass* UIPanel_t1716472341_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_FullCompareFunc_m1006404042_MetadataUsageId;
extern const uint32_t UIWidget_PanelCompareFunc_m3717063172_MetadataUsageId;
extern const uint32_t UIWidget_CalculateBounds_m3008782085_MetadataUsageId;
extern const uint32_t UIWidget_SetDirty_m2297015651_MetadataUsageId;
extern const uint32_t UIWidget_RemoveFromPanel_m725257632_MetadataUsageId;
extern const uint32_t UIWidget_MarkAsChanged_m786570369_MetadataUsageId;
extern const uint32_t UIWidget_CreatePanel_m976889642_MetadataUsageId;
extern String_t* _stringLiteral2595132038;
extern const uint32_t UIWidget_CheckLayer_m2684013386_MetadataUsageId;
extern const uint32_t UIWidget_ParentHasChanged_m2262919468_MetadataUsageId;
extern const uint32_t UIWidget_UpgradeFrom265_m4222449532_MetadataUsageId;
extern const uint32_t UIWidget_OnAnchor_m4074470753_MetadataUsageId;
extern const uint32_t UIWidget_OnUpdate_m3837236151_MetadataUsageId;
extern const uint32_t UIWidget_UpdateTransform_m3758311267_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpdateGeometry_m145703376_MetadataUsageId;
extern const uint32_t UIWidget_MakePixelPerfect_m194260931_MetadataUsageId;
extern const uint32_t UIWidget_get_border_m1908907599_MetadataUsageId;
extern const uint32_t HitCheck_BeginInvoke_m1081756051_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const uint32_t OnPostFillCallback_BeginInvoke_m1173000092_MetadataUsageId;
extern RuntimeClass* List_1_t777473367_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2885667311_RuntimeMethod_var;
extern const uint32_t UIWrapContent__ctor_m3120265429_MetadataUsageId;
extern RuntimeClass* OnClippingMoved_t476625095_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisUIPanel_t1716472341_m3980802358_RuntimeMethod_var;
extern const uint32_t UIWrapContent_Start_m878423027_MetadataUsageId;
extern RuntimeClass* UIWrapContent_t1188558554_il2cpp_TypeInfo_var;
extern RuntimeClass* Comparison_1_t3375297100_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Clear_m3082658015_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m4073477735_RuntimeMethod_var;
extern const RuntimeMethod* UIGrid_SortHorizontal_m510606556_RuntimeMethod_var;
extern const RuntimeMethod* Comparison_1__ctor_m2800470641_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Sort_m620079343_RuntimeMethod_var;
extern const RuntimeMethod* UIGrid_SortVertical_m1233841256_RuntimeMethod_var;
extern const uint32_t UIWrapContent_SortBasedOnScrollMovement_m1287634130_MetadataUsageId;
extern const RuntimeMethod* UIGrid_SortByName_m2291677436_RuntimeMethod_var;
extern const uint32_t UIWrapContent_SortAlphabetically_m2142146348_MetadataUsageId;
extern const RuntimeMethod* NGUITools_FindInParents_TisUIPanel_t1716472341_m1961265283_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisUIScrollView_t1973404950_m746780642_RuntimeMethod_var;
extern const uint32_t UIWrapContent_CacheScrollView_m1573533213_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m3787308655_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3022113929_RuntimeMethod_var;
extern const uint32_t UIWrapContent_ResetChildPositions_m832715332_MetadataUsageId;
extern const uint32_t UIWrapContent_WrapContent_m967226172_MetadataUsageId;
extern const uint32_t UIWrapContent_UpdateItem_m2557257775_MetadataUsageId;
extern const uint32_t OnInitializeItem_BeginInvoke_m3043049668_MetadataUsageId;
extern String_t* _stringLiteral1845752016;
extern String_t* _stringLiteral850945763;
extern String_t* _stringLiteral2735947596;
extern String_t* _stringLiteral2653172448;
extern String_t* _stringLiteral1403141053;
extern String_t* _stringLiteral1465845074;
extern const uint32_t UnityProjectSetting__ctor_m3803001152_MetadataUsageId;
extern RuntimeClass* UnityProjectSetting_t2579824760_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Resources_Load_TisUnityProjectSetting_t2579824760_m3201061709_RuntimeMethod_var;
extern String_t* _stringLiteral1472809214;
extern String_t* _stringLiteral544349615;
extern const uint32_t UnityProjectSetting_get_Entity_m2785360446_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t3173579796_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3989434104_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m3554053801_RuntimeMethod_var;
extern const uint32_t UnityProjectSetting_get_ShareFormatDict_m3368659902_MetadataUsageId;
extern const uint32_t UnityProjectSetting_get_ShareFormatDictEnglish_m1615427313_MetadataUsageId;
extern RuntimeClass* UserData_t2822117362_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PlayerPrefsUtility_SaveDict_TisString_t_TisInt32_t2950945753_m1477132359_RuntimeMethod_var;
extern String_t* _stringLiteral1838617347;
extern String_t* _stringLiteral1453243796;
extern String_t* _stringLiteral3021547732;
extern String_t* _stringLiteral3474414062;
extern String_t* _stringLiteral311406783;
extern String_t* _stringLiteral3236165112;
extern const uint32_t UserData_Save_m1334545219_MetadataUsageId;
extern const uint32_t UserData_get_PlayCount_m3442301827_MetadataUsageId;
extern const uint32_t UserData_set_PlayCount_m3841829882_MetadataUsageId;
extern const RuntimeMethod* PlayerPrefsUtility_LoadDict_TisString_t_TisInt32_t2950945753_m1536884824_RuntimeMethod_var;
extern const uint32_t UserData_get_HighScoreDict_m1377977320_MetadataUsageId;
extern const uint32_t UserData_set_HighScoreDict_m2375964171_MetadataUsageId;
extern const RuntimeMethod* DictionaryExtensions_GetValueOrDefault_TisString_t_TisInt32_t2950945753_m3168452452_RuntimeMethod_var;
extern String_t* _stringLiteral3452614543;
extern const uint32_t UserData_get_HighScore_m2475012517_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_set_Item_m3800595820_RuntimeMethod_var;
extern const uint32_t UserData_set_HighScore_m700482015_MetadataUsageId;
extern RuntimeClass* Action_1_t269755560_il2cpp_TypeInfo_var;
extern const uint32_t UserData_add_OnChangeMuteSetting_m564281592_MetadataUsageId;
extern const uint32_t UserData_remove_OnChangeMuteSetting_m3825023625_MetadataUsageId;
extern const uint32_t UserData_get_IsMute_m2788164951_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m1933767679_RuntimeMethod_var;
extern const uint32_t UserData_set_IsMute_m3373741355_MetadataUsageId;
extern const uint32_t UserData_add_OnChangeFirstTime_m2478173862_MetadataUsageId;
extern const uint32_t UserData_remove_OnChangeFirstTime_m1822527583_MetadataUsageId;
extern const uint32_t UserData_get_IsFisrtTime_m903176408_MetadataUsageId;
extern const uint32_t UserData_set_IsFisrtTime_m19282104_MetadataUsageId;
extern const uint32_t UserData_get_TimeOfPushedHouseAdButton_m195643557_MetadataUsageId;
extern const uint32_t UserData_set_TimeOfPushedHouseAdButton_m977932569_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t2736202052_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3200964102_RuntimeMethod_var;
extern const RuntimeMethod* UserData_U3COnChangeMuteSettingU3Em__0_m2939914662_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m981112613_RuntimeMethod_var;
extern const RuntimeMethod* UserData_U3COnChangeFirstTimeU3Em__1_m4114381407_RuntimeMethod_var;
extern const uint32_t UserData__cctor_m656705242_MetadataUsageId;
extern const uint32_t VectorCalculator_GetAim_m745570127_MetadataUsageId;
extern const uint32_t VectorCalculator_GetDirection_m207383374_MetadataUsageId;

struct UITweenerU5BU5D_t3440034099;
struct UIWidgetU5BU5D_t2950248968;
struct KeyframeU5BU5D_t1068524471;
struct Vector3U5BU5D_t1718750761;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef VECTORCALCULATOR_T2681497494_H
#define VECTORCALCULATOR_T2681497494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VectorCalculator
struct  VectorCalculator_t2681497494  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORCALCULATOR_T2681497494_H
#ifndef DICTIONARY_2_T2736202052_H
#define DICTIONARY_2_T2736202052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_t2736202052  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Int32U5BU5D_t385246372* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___valueSlots_7)); }
	inline Int32U5BU5D_t385246372* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Int32U5BU5D_t385246372** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Int32U5BU5D_t385246372* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2736202052_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3530625384 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3530625384 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3530625384 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3530625384 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2736202052_H
#ifndef DICTIONARY_2_T3173579796_H
#define DICTIONARY_2_T3173579796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<ShareLocation,System.String>
struct  Dictionary_2_t3173579796  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ShareLocationU5BU5D_t2822662148* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1281789340* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___keySlots_6)); }
	inline ShareLocationU5BU5D_t2822662148* get_keySlots_6() const { return ___keySlots_6; }
	inline ShareLocationU5BU5D_t2822662148** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ShareLocationU5BU5D_t2822662148* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___valueSlots_7)); }
	inline StringU5BU5D_t1281789340* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1281789340** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1281789340* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3173579796_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3679379800 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3173579796_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3679379800 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3679379800 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3679379800 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3173579796_H
#ifndef LIST_1_T777473367_H
#define LIST_1_T777473367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct  List_1_t777473367  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t807237628* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____items_1)); }
	inline TransformU5BU5D_t807237628* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t807237628** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t807237628* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t777473367_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TransformU5BU5D_t807237628* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t777473367_StaticFields, ___EmptyArray_4)); }
	inline TransformU5BU5D_t807237628* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TransformU5BU5D_t807237628** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TransformU5BU5D_t807237628* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T777473367_H
#ifndef LIST_1_T496136383_H
#define LIST_1_T496136383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_t496136383  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t934056436* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t496136383, ____items_1)); }
	inline Vector4U5BU5D_t934056436* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t934056436** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t934056436* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t496136383, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t496136383, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t496136383_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector4U5BU5D_t934056436* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t496136383_StaticFields, ___EmptyArray_4)); }
	inline Vector4U5BU5D_t934056436* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector4U5BU5D_t934056436** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector4U5BU5D_t934056436* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T496136383_H
#ifndef LIST_1_T4027761066_H
#define LIST_1_T4027761066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color>
struct  List_1_t4027761066  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ColorU5BU5D_t941916413* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4027761066, ____items_1)); }
	inline ColorU5BU5D_t941916413* get__items_1() const { return ____items_1; }
	inline ColorU5BU5D_t941916413** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ColorU5BU5D_t941916413* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4027761066, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4027761066, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4027761066_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ColorU5BU5D_t941916413* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4027761066_StaticFields, ___EmptyArray_4)); }
	inline ColorU5BU5D_t941916413* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ColorU5BU5D_t941916413** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ColorU5BU5D_t941916413* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4027761066_H
#ifndef LIST_1_T3628304265_H
#define LIST_1_T3628304265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t3628304265  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_t1457185986* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3628304265, ____items_1)); }
	inline Vector2U5BU5D_t1457185986* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_t1457185986** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_t1457185986* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3628304265, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3628304265, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3628304265_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector2U5BU5D_t1457185986* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3628304265_StaticFields, ___EmptyArray_4)); }
	inline Vector2U5BU5D_t1457185986* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector2U5BU5D_t1457185986** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector2U5BU5D_t1457185986* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3628304265_H
#ifndef LIST_1_T899420910_H
#define LIST_1_T899420910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t899420910  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1718750761* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____items_1)); }
	inline Vector3U5BU5D_t1718750761* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1718750761* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t899420910_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t1718750761* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t899420910_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t1718750761* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t1718750761* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T899420910_H
#ifndef ANCHORPOINT_T1754718329_H
#define ANCHORPOINT_T1754718329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect/AnchorPoint
struct  AnchorPoint_t1754718329  : public RuntimeObject
{
public:
	// UnityEngine.Transform UIRect/AnchorPoint::target
	Transform_t3600365921 * ___target_0;
	// System.Single UIRect/AnchorPoint::relative
	float ___relative_1;
	// System.Int32 UIRect/AnchorPoint::absolute
	int32_t ___absolute_2;
	// UIRect UIRect/AnchorPoint::rect
	UIRect_t2875960382 * ___rect_3;
	// UnityEngine.Camera UIRect/AnchorPoint::targetCam
	Camera_t4157153871 * ___targetCam_4;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_relative_1() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___relative_1)); }
	inline float get_relative_1() const { return ___relative_1; }
	inline float* get_address_of_relative_1() { return &___relative_1; }
	inline void set_relative_1(float value)
	{
		___relative_1 = value;
	}

	inline static int32_t get_offset_of_absolute_2() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___absolute_2)); }
	inline int32_t get_absolute_2() const { return ___absolute_2; }
	inline int32_t* get_address_of_absolute_2() { return &___absolute_2; }
	inline void set_absolute_2(int32_t value)
	{
		___absolute_2 = value;
	}

	inline static int32_t get_offset_of_rect_3() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___rect_3)); }
	inline UIRect_t2875960382 * get_rect_3() const { return ___rect_3; }
	inline UIRect_t2875960382 ** get_address_of_rect_3() { return &___rect_3; }
	inline void set_rect_3(UIRect_t2875960382 * value)
	{
		___rect_3 = value;
		Il2CppCodeGenWriteBarrier((&___rect_3), value);
	}

	inline static int32_t get_offset_of_targetCam_4() { return static_cast<int32_t>(offsetof(AnchorPoint_t1754718329, ___targetCam_4)); }
	inline Camera_t4157153871 * get_targetCam_4() const { return ___targetCam_4; }
	inline Camera_t4157153871 ** get_address_of_targetCam_4() { return &___targetCam_4; }
	inline void set_targetCam_4(Camera_t4157153871 * value)
	{
		___targetCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetCam_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORPOINT_T1754718329_H
#ifndef LIST_1_T2585711361_H
#define LIST_1_T2585711361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t2585711361  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3328599146* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____items_1)); }
	inline GameObjectU5BU5D_t3328599146* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3328599146* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2585711361_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3328599146* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2585711361_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3328599146* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3328599146* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2585711361_H
#ifndef EVENTDELEGATE_T2738326060_H
#define EVENTDELEGATE_T2738326060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventDelegate
struct  EventDelegate_t2738326060  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour EventDelegate::mTarget
	MonoBehaviour_t3962482529 * ___mTarget_0;
	// System.String EventDelegate::mMethodName
	String_t* ___mMethodName_1;
	// EventDelegate/Parameter[] EventDelegate::mParameters
	ParameterU5BU5D_t2885283399* ___mParameters_2;
	// System.Boolean EventDelegate::oneShot
	bool ___oneShot_3;
	// EventDelegate/Callback EventDelegate::mCachedCallback
	Callback_t3139336517 * ___mCachedCallback_4;
	// System.Boolean EventDelegate::mRawDelegate
	bool ___mRawDelegate_5;
	// System.Boolean EventDelegate::mCached
	bool ___mCached_6;
	// System.Reflection.MethodInfo EventDelegate::mMethod
	MethodInfo_t * ___mMethod_7;
	// System.Reflection.ParameterInfo[] EventDelegate::mParameterInfos
	ParameterInfoU5BU5D_t390618515* ___mParameterInfos_8;
	// System.Object[] EventDelegate::mArgs
	ObjectU5BU5D_t2843939325* ___mArgs_9;

public:
	inline static int32_t get_offset_of_mTarget_0() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mTarget_0)); }
	inline MonoBehaviour_t3962482529 * get_mTarget_0() const { return ___mTarget_0; }
	inline MonoBehaviour_t3962482529 ** get_address_of_mTarget_0() { return &___mTarget_0; }
	inline void set_mTarget_0(MonoBehaviour_t3962482529 * value)
	{
		___mTarget_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTarget_0), value);
	}

	inline static int32_t get_offset_of_mMethodName_1() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mMethodName_1)); }
	inline String_t* get_mMethodName_1() const { return ___mMethodName_1; }
	inline String_t** get_address_of_mMethodName_1() { return &___mMethodName_1; }
	inline void set_mMethodName_1(String_t* value)
	{
		___mMethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMethodName_1), value);
	}

	inline static int32_t get_offset_of_mParameters_2() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mParameters_2)); }
	inline ParameterU5BU5D_t2885283399* get_mParameters_2() const { return ___mParameters_2; }
	inline ParameterU5BU5D_t2885283399** get_address_of_mParameters_2() { return &___mParameters_2; }
	inline void set_mParameters_2(ParameterU5BU5D_t2885283399* value)
	{
		___mParameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___mParameters_2), value);
	}

	inline static int32_t get_offset_of_oneShot_3() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___oneShot_3)); }
	inline bool get_oneShot_3() const { return ___oneShot_3; }
	inline bool* get_address_of_oneShot_3() { return &___oneShot_3; }
	inline void set_oneShot_3(bool value)
	{
		___oneShot_3 = value;
	}

	inline static int32_t get_offset_of_mCachedCallback_4() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mCachedCallback_4)); }
	inline Callback_t3139336517 * get_mCachedCallback_4() const { return ___mCachedCallback_4; }
	inline Callback_t3139336517 ** get_address_of_mCachedCallback_4() { return &___mCachedCallback_4; }
	inline void set_mCachedCallback_4(Callback_t3139336517 * value)
	{
		___mCachedCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCachedCallback_4), value);
	}

	inline static int32_t get_offset_of_mRawDelegate_5() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mRawDelegate_5)); }
	inline bool get_mRawDelegate_5() const { return ___mRawDelegate_5; }
	inline bool* get_address_of_mRawDelegate_5() { return &___mRawDelegate_5; }
	inline void set_mRawDelegate_5(bool value)
	{
		___mRawDelegate_5 = value;
	}

	inline static int32_t get_offset_of_mCached_6() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mCached_6)); }
	inline bool get_mCached_6() const { return ___mCached_6; }
	inline bool* get_address_of_mCached_6() { return &___mCached_6; }
	inline void set_mCached_6(bool value)
	{
		___mCached_6 = value;
	}

	inline static int32_t get_offset_of_mMethod_7() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mMethod_7)); }
	inline MethodInfo_t * get_mMethod_7() const { return ___mMethod_7; }
	inline MethodInfo_t ** get_address_of_mMethod_7() { return &___mMethod_7; }
	inline void set_mMethod_7(MethodInfo_t * value)
	{
		___mMethod_7 = value;
		Il2CppCodeGenWriteBarrier((&___mMethod_7), value);
	}

	inline static int32_t get_offset_of_mParameterInfos_8() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mParameterInfos_8)); }
	inline ParameterInfoU5BU5D_t390618515* get_mParameterInfos_8() const { return ___mParameterInfos_8; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_mParameterInfos_8() { return &___mParameterInfos_8; }
	inline void set_mParameterInfos_8(ParameterInfoU5BU5D_t390618515* value)
	{
		___mParameterInfos_8 = value;
		Il2CppCodeGenWriteBarrier((&___mParameterInfos_8), value);
	}

	inline static int32_t get_offset_of_mArgs_9() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060, ___mArgs_9)); }
	inline ObjectU5BU5D_t2843939325* get_mArgs_9() const { return ___mArgs_9; }
	inline ObjectU5BU5D_t2843939325** get_address_of_mArgs_9() { return &___mArgs_9; }
	inline void set_mArgs_9(ObjectU5BU5D_t2843939325* value)
	{
		___mArgs_9 = value;
		Il2CppCodeGenWriteBarrier((&___mArgs_9), value);
	}
};

struct EventDelegate_t2738326060_StaticFields
{
public:
	// System.Int32 EventDelegate::s_Hash
	int32_t ___s_Hash_10;

public:
	inline static int32_t get_offset_of_s_Hash_10() { return static_cast<int32_t>(offsetof(EventDelegate_t2738326060_StaticFields, ___s_Hash_10)); }
	inline int32_t get_s_Hash_10() const { return ___s_Hash_10; }
	inline int32_t* get_address_of_s_Hash_10() { return &___s_Hash_10; }
	inline void set_s_Hash_10(int32_t value)
	{
		___s_Hash_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDELEGATE_T2738326060_H
#ifndef LIST_1_T1139589975_H
#define LIST_1_T1139589975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>
struct  List_1_t1139589975  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MonoBehaviourU5BU5D_t2007329276* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1139589975, ____items_1)); }
	inline MonoBehaviourU5BU5D_t2007329276* get__items_1() const { return ____items_1; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MonoBehaviourU5BU5D_t2007329276* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1139589975, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1139589975, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1139589975_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MonoBehaviourU5BU5D_t2007329276* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1139589975_StaticFields, ___EmptyArray_4)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1139589975_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef BETTERLIST_1_T3347146576_H
#define BETTERLIST_1_T3347146576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BetterList`1<UIToggle>
struct  BetterList_1_t3347146576  : public RuntimeObject
{
public:
	// T[] BetterList`1::buffer
	UIToggleU5BU5D_t3308279239* ___buffer_0;
	// System.Int32 BetterList`1::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BetterList_1_t3347146576, ___buffer_0)); }
	inline UIToggleU5BU5D_t3308279239* get_buffer_0() const { return ___buffer_0; }
	inline UIToggleU5BU5D_t3308279239** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(UIToggleU5BU5D_t3308279239* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(BetterList_1_t3347146576, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BETTERLIST_1_T3347146576_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T4210400802_H
#define LIST_1_T4210400802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<EventDelegate>
struct  List_1_t4210400802  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	EventDelegateU5BU5D_t2229996453* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4210400802, ____items_1)); }
	inline EventDelegateU5BU5D_t2229996453* get__items_1() const { return ____items_1; }
	inline EventDelegateU5BU5D_t2229996453** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(EventDelegateU5BU5D_t2229996453* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4210400802, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4210400802, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4210400802_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	EventDelegateU5BU5D_t2229996453* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4210400802_StaticFields, ___EmptyArray_4)); }
	inline EventDelegateU5BU5D_t2229996453* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline EventDelegateU5BU5D_t2229996453** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(EventDelegateU5BU5D_t2229996453* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4210400802_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef KEYFRAME_T4206410242_H
#define KEYFRAME_T4206410242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t4206410242 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T4206410242_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#define NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t3489357830  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#ifndef OVERFLOW_T884389639_H
#define OVERFLOW_T884389639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Overflow
struct  Overflow_t884389639 
{
public:
	// System.Int32 UILabel/Overflow::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Overflow_t884389639, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERFLOW_T884389639_H
#ifndef SYMBOLSTYLE_T3792107337_H
#define SYMBOLSTYLE_T3792107337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/SymbolStyle
struct  SymbolStyle_t3792107337 
{
public:
	// System.Int32 NGUIText/SymbolStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SymbolStyle_t3792107337, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSTYLE_T3792107337_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef ADVANCEDTYPE_T3940519926_H
#define ADVANCEDTYPE_T3940519926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/AdvancedType
struct  AdvancedType_t3940519926 
{
public:
	// System.Int32 UIBasicSprite/AdvancedType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvancedType_t3940519926, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDTYPE_T3940519926_H
#ifndef EFFECT_T2533209744_H
#define EFFECT_T2533209744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Effect
struct  Effect_t2533209744 
{
public:
	// System.Int32 UILabel/Effect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Effect_t2533209744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECT_T2533209744_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef PROCESSEVENTSIN_T702674362_H
#define PROCESSEVENTSIN_T702674362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ProcessEventsIn
struct  ProcessEventsIn_t702674362 
{
public:
	// System.Int32 UICamera/ProcessEventsIn::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProcessEventsIn_t702674362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSEVENTSIN_T702674362_H
#ifndef EVENTTYPE_T561105572_H
#define EVENTTYPE_T561105572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/EventType
struct  EventType_t561105572 
{
public:
	// System.Int32 UICamera/EventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventType_t561105572, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T561105572_H
#ifndef ALIGNMENT_T3228070485_H
#define ALIGNMENT_T3228070485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/Alignment
struct  Alignment_t3228070485 
{
public:
	// System.Int32 NGUIText/Alignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Alignment_t3228070485, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T3228070485_H
#ifndef FONTSTYLE_T82229486_H
#define FONTSTYLE_T82229486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t82229486 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyle_t82229486, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T82229486_H
#ifndef ANCHORUPDATE_T1570184075_H
#define ANCHORUPDATE_T1570184075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect/AnchorUpdate
struct  AnchorUpdate_t1570184075 
{
public:
	// System.Int32 UIRect/AnchorUpdate::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnchorUpdate_t1570184075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORUPDATE_T1570184075_H
#ifndef CRISPNESS_T2029465680_H
#define CRISPNESS_T2029465680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Crispness
struct  Crispness_t2029465680 
{
public:
	// System.Int32 UILabel/Crispness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Crispness_t2029465680, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRISPNESS_T2029465680_H
#ifndef MOVEMENT_T247277292_H
#define MOVEMENT_T247277292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView/Movement
struct  Movement_t247277292 
{
public:
	// System.Int32 UIScrollView/Movement::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Movement_t247277292, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENT_T247277292_H
#ifndef GAMETYPE_T3504513020_H
#define GAMETYPE_T3504513020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameType
struct  GameType_t3504513020 
{
public:
	// System.Int32 GameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameType_t3504513020, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMETYPE_T3504513020_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef PLANE_T1000493321_H
#define PLANE_T1000493321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1000493321 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3722313464  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Normal_0)); }
	inline Vector3_t3722313464  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3722313464  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1000493321_H
#ifndef SHARELOCATION_T3603649529_H
#define SHARELOCATION_T3603649529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShareLocation
struct  ShareLocation_t3603649529 
{
public:
	// System.Int32 ShareLocation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShareLocation_t3603649529, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARELOCATION_T3603649529_H
#ifndef RUNTIMEPLATFORM_T4159857903_H
#define RUNTIMEPLATFORM_T4159857903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t4159857903 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t4159857903, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T4159857903_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef MODIFIER_T3840764400_H
#define MODIFIER_T3840764400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Modifier
struct  Modifier_t3840764400 
{
public:
	// System.Int32 UILabel/Modifier::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Modifier_t3840764400, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIER_T3840764400_H
#ifndef CONTROLSCHEME_T4038670825_H
#define CONTROLSCHEME_T4038670825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ControlScheme
struct  ControlScheme_t4038670825 
{
public:
	// System.Int32 UICamera/ControlScheme::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlScheme_t4038670825, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSCHEME_T4038670825_H
#ifndef DIRECTION_T2061188385_H
#define DIRECTION_T2061188385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Direction
struct  Direction_t2061188385 
{
public:
	// System.Int32 AnimationOrTween.Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t2061188385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T2061188385_H
#ifndef ASPECTRATIOSOURCE_T168813522_H
#define ASPECTRATIOSOURCE_T168813522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/AspectRatioSource
struct  AspectRatioSource_t168813522 
{
public:
	// System.Int32 UIWidget/AspectRatioSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectRatioSource_t168813522, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOSOURCE_T168813522_H
#ifndef SHOWCONDITION_T1535012424_H
#define SHOWCONDITION_T1535012424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView/ShowCondition
struct  ShowCondition_t1535012424 
{
public:
	// System.Int32 UIScrollView/ShowCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShowCondition_t1535012424, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWCONDITION_T1535012424_H
#ifndef ARRANGEMENT_T1850956547_H
#define ARRANGEMENT_T1850956547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid/Arrangement
struct  Arrangement_t1850956547 
{
public:
	// System.Int32 UIGrid/Arrangement::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Arrangement_t1850956547, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRANGEMENT_T1850956547_H
#ifndef SORTING_T533260699_H
#define SORTING_T533260699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid/Sorting
struct  Sorting_t533260699 
{
public:
	// System.Int32 UIGrid/Sorting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sorting_t533260699, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTING_T533260699_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef DISABLECONDITION_T2151257518_H
#define DISABLECONDITION_T2151257518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.DisableCondition
struct  DisableCondition_t2151257518 
{
public:
	// System.Int32 AnimationOrTween.DisableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisableCondition_t2151257518, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECONDITION_T2151257518_H
#ifndef CLIPPING_T1109313910_H
#define CLIPPING_T1109313910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/Clipping
struct  Clipping_t1109313910 
{
public:
	// System.Int32 UIDrawCall/Clipping::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Clipping_t1109313910, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_T1109313910_H
#ifndef ENABLECONDITION_T1125033030_H
#define ENABLECONDITION_T1125033030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.EnableCondition
struct  EnableCondition_t1125033030 
{
public:
	// System.Int32 AnimationOrTween.EnableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EnableCondition_t1125033030, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLECONDITION_T1125033030_H
#ifndef RENDERQUEUE_T2721716586_H
#define RENDERQUEUE_T2721716586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel/RenderQueue
struct  RenderQueue_t2721716586 
{
public:
	// System.Int32 UIPanel/RenderQueue::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderQueue_t2721716586, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERQUEUE_T2721716586_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef DRAGEFFECT_T491601944_H
#define DRAGEFFECT_T491601944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView/DragEffect
struct  DragEffect_t491601944 
{
public:
	// System.Int32 UIScrollView/DragEffect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DragEffect_t491601944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEFFECT_T491601944_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef TYPE_T4088629396_H
#define TYPE_T4088629396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/Type
struct  Type_t4088629396 
{
public:
	// System.Int32 UIBasicSprite/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t4088629396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T4088629396_H
#ifndef METHOD_T1730494418_H
#define METHOD_T1730494418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Method
struct  Method_t1730494418 
{
public:
	// System.Int32 UITweener/Method::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Method_t1730494418, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T1730494418_H
#ifndef COLORSPACE_T3453996949_H
#define COLORSPACE_T3453996949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ColorSpace
struct  ColorSpace_t3453996949 
{
public:
	// System.Int32 UnityEngine.ColorSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorSpace_t3453996949, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSPACE_T3453996949_H
#ifndef PIVOT_T1798046373_H
#define PIVOT_T1798046373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/Pivot
struct  Pivot_t1798046373 
{
public:
	// System.Int32 UIWidget/Pivot::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pivot_t1798046373, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOT_T1798046373_H
#ifndef UIGEOMETRY_T1059483952_H
#define UIGEOMETRY_T1059483952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGeometry
struct  UIGeometry_t1059483952  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIGeometry::verts
	List_1_t899420910 * ___verts_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UIGeometry::uvs
	List_1_t3628304265 * ___uvs_1;
	// System.Collections.Generic.List`1<UnityEngine.Color> UIGeometry::cols
	List_1_t4027761066 * ___cols_2;
	// UIGeometry/OnCustomWrite UIGeometry::onCustomWrite
	OnCustomWrite_t2800546165 * ___onCustomWrite_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIGeometry::mRtpVerts
	List_1_t899420910 * ___mRtpVerts_4;
	// UnityEngine.Vector3 UIGeometry::mRtpNormal
	Vector3_t3722313464  ___mRtpNormal_5;
	// UnityEngine.Vector4 UIGeometry::mRtpTan
	Vector4_t3319028937  ___mRtpTan_6;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___verts_0)); }
	inline List_1_t899420910 * get_verts_0() const { return ___verts_0; }
	inline List_1_t899420910 ** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(List_1_t899420910 * value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier((&___verts_0), value);
	}

	inline static int32_t get_offset_of_uvs_1() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___uvs_1)); }
	inline List_1_t3628304265 * get_uvs_1() const { return ___uvs_1; }
	inline List_1_t3628304265 ** get_address_of_uvs_1() { return &___uvs_1; }
	inline void set_uvs_1(List_1_t3628304265 * value)
	{
		___uvs_1 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_1), value);
	}

	inline static int32_t get_offset_of_cols_2() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___cols_2)); }
	inline List_1_t4027761066 * get_cols_2() const { return ___cols_2; }
	inline List_1_t4027761066 ** get_address_of_cols_2() { return &___cols_2; }
	inline void set_cols_2(List_1_t4027761066 * value)
	{
		___cols_2 = value;
		Il2CppCodeGenWriteBarrier((&___cols_2), value);
	}

	inline static int32_t get_offset_of_onCustomWrite_3() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___onCustomWrite_3)); }
	inline OnCustomWrite_t2800546165 * get_onCustomWrite_3() const { return ___onCustomWrite_3; }
	inline OnCustomWrite_t2800546165 ** get_address_of_onCustomWrite_3() { return &___onCustomWrite_3; }
	inline void set_onCustomWrite_3(OnCustomWrite_t2800546165 * value)
	{
		___onCustomWrite_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCustomWrite_3), value);
	}

	inline static int32_t get_offset_of_mRtpVerts_4() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___mRtpVerts_4)); }
	inline List_1_t899420910 * get_mRtpVerts_4() const { return ___mRtpVerts_4; }
	inline List_1_t899420910 ** get_address_of_mRtpVerts_4() { return &___mRtpVerts_4; }
	inline void set_mRtpVerts_4(List_1_t899420910 * value)
	{
		___mRtpVerts_4 = value;
		Il2CppCodeGenWriteBarrier((&___mRtpVerts_4), value);
	}

	inline static int32_t get_offset_of_mRtpNormal_5() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___mRtpNormal_5)); }
	inline Vector3_t3722313464  get_mRtpNormal_5() const { return ___mRtpNormal_5; }
	inline Vector3_t3722313464 * get_address_of_mRtpNormal_5() { return &___mRtpNormal_5; }
	inline void set_mRtpNormal_5(Vector3_t3722313464  value)
	{
		___mRtpNormal_5 = value;
	}

	inline static int32_t get_offset_of_mRtpTan_6() { return static_cast<int32_t>(offsetof(UIGeometry_t1059483952, ___mRtpTan_6)); }
	inline Vector4_t3319028937  get_mRtpTan_6() const { return ___mRtpTan_6; }
	inline Vector4_t3319028937 * get_address_of_mRtpTan_6() { return &___mRtpTan_6; }
	inline void set_mRtpTan_6(Vector4_t3319028937  value)
	{
		___mRtpTan_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGEOMETRY_T1059483952_H
#ifndef SENDMESSAGEOPTIONS_T3580193095_H
#define SENDMESSAGEOPTIONS_T3580193095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t3580193095 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t3580193095, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T3580193095_H
#ifndef FILLDIRECTION_T904769527_H
#define FILLDIRECTION_T904769527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/FillDirection
struct  FillDirection_t904769527 
{
public:
	// System.Int32 UIBasicSprite/FillDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillDirection_t904769527, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLDIRECTION_T904769527_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef FLIP_T2552321477_H
#define FLIP_T2552321477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/Flip
struct  Flip_t2552321477 
{
public:
	// System.Int32 UIBasicSprite/Flip::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Flip_t2552321477, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLIP_T2552321477_H
#ifndef SHADOWMODE_T1455171354_H
#define SHADOWMODE_T1455171354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/ShadowMode
struct  ShadowMode_t1455171354 
{
public:
	// System.Int32 UIDrawCall/ShadowMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShadowMode_t1455171354, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWMODE_T1455171354_H
#ifndef STYLE_T3120619385_H
#define STYLE_T3120619385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Style
struct  Style_t3120619385 
{
public:
	// System.Int32 UITweener/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t3120619385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T3120619385_H
#ifndef DEPTHENTRY_T628749918_H
#define DEPTHENTRY_T628749918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/DepthEntry
struct  DepthEntry_t628749918 
{
public:
	// System.Int32 UICamera/DepthEntry::depth
	int32_t ___depth_0;
	// UnityEngine.RaycastHit UICamera/DepthEntry::hit
	RaycastHit_t1056001966  ___hit_1;
	// UnityEngine.Vector3 UICamera/DepthEntry::point
	Vector3_t3722313464  ___point_2;
	// UnityEngine.GameObject UICamera/DepthEntry::go
	GameObject_t1113636619 * ___go_3;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_hit_1() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___hit_1)); }
	inline RaycastHit_t1056001966  get_hit_1() const { return ___hit_1; }
	inline RaycastHit_t1056001966 * get_address_of_hit_1() { return &___hit_1; }
	inline void set_hit_1(RaycastHit_t1056001966  value)
	{
		___hit_1 = value;
	}

	inline static int32_t get_offset_of_point_2() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___point_2)); }
	inline Vector3_t3722313464  get_point_2() const { return ___point_2; }
	inline Vector3_t3722313464 * get_address_of_point_2() { return &___point_2; }
	inline void set_point_2(Vector3_t3722313464  value)
	{
		___point_2 = value;
	}

	inline static int32_t get_offset_of_go_3() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___go_3)); }
	inline GameObject_t1113636619 * get_go_3() const { return ___go_3; }
	inline GameObject_t1113636619 ** get_address_of_go_3() { return &___go_3; }
	inline void set_go_3(GameObject_t1113636619 * value)
	{
		___go_3 = value;
		Il2CppCodeGenWriteBarrier((&___go_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UICamera/DepthEntry
struct DepthEntry_t628749918_marshaled_pinvoke
{
	int32_t ___depth_0;
	RaycastHit_t1056001966_marshaled_pinvoke ___hit_1;
	Vector3_t3722313464  ___point_2;
	GameObject_t1113636619 * ___go_3;
};
// Native definition for COM marshalling of UICamera/DepthEntry
struct DepthEntry_t628749918_marshaled_com
{
	int32_t ___depth_0;
	RaycastHit_t1056001966_marshaled_com ___hit_1;
	Vector3_t3722313464  ___point_2;
	GameObject_t1113636619 * ___go_3;
};
#endif // DEPTHENTRY_T628749918_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SHADER_T4151988712_H
#define SHADER_T4151988712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t4151988712  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T4151988712_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef CALLBACK_T3139336517_H
#define CALLBACK_T3139336517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventDelegate/Callback
struct  Callback_t3139336517  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACK_T3139336517_H
#ifndef ONRENDERCALLBACK_T133425655_H
#define ONRENDERCALLBACK_T133425655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/OnRenderCallback
struct  OnRenderCallback_t133425655  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRENDERCALLBACK_T133425655_H
#ifndef ONDIMENSIONSCHANGED_T3101921181_H
#define ONDIMENSIONSCHANGED_T3101921181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/OnDimensionsChanged
struct  OnDimensionsChanged_t3101921181  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDIMENSIONSCHANGED_T3101921181_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef HITCHECK_T2300079615_H
#define HITCHECK_T2300079615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/HitCheck
struct  HitCheck_t2300079615  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITCHECK_T2300079615_H
#ifndef ONPOSTFILLCALLBACK_T2835645043_H
#define ONPOSTFILLCALLBACK_T2835645043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/OnPostFillCallback
struct  OnPostFillCallback_t2835645043  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPOSTFILLCALLBACK_T2835645043_H
#ifndef RIGIDBODY_T3916780224_H
#define RIGIDBODY_T3916780224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t3916780224  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T3916780224_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ONCLIPPINGMOVED_T476625095_H
#define ONCLIPPINGMOVED_T476625095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel/OnClippingMoved
struct  OnClippingMoved_t476625095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLIPPINGMOVED_T476625095_H
#ifndef VALIDATE_T3702293971_H
#define VALIDATE_T3702293971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggle/Validate
struct  Validate_t3702293971  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATE_T3702293971_H
#ifndef COMPARISON_1_T3375297100_H
#define COMPARISON_1_T3375297100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Comparison`1<UnityEngine.Transform>
struct  Comparison_1_t3375297100  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARISON_1_T3375297100_H
#ifndef ACTION_1_T269755560_H
#define ACTION_1_T269755560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Boolean>
struct  Action_1_t269755560  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T269755560_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef USERDATA_T2822117362_H
#define USERDATA_T2822117362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserData
struct  UserData_t2822117362  : public RuntimeObject
{
public:

public:
};

struct UserData_t2822117362_StaticFields
{
public:
	// System.Boolean UserData::_isInitializedPlayCount
	bool ____isInitializedPlayCount_1;
	// System.Int32 UserData::_playCount
	int32_t ____playCount_2;
	// System.Boolean UserData::_isInitializedHighScoreDict
	bool ____isInitializedHighScoreDict_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UserData::_highScoreDict
	Dictionary_2_t2736202052 * ____highScoreDict_5;
	// System.Boolean UserData::_isInitializedMuteSetting
	bool ____isInitializedMuteSetting_7;
	// System.Boolean UserData::_isMute
	bool ____isMute_8;
	// System.Action`1<System.Boolean> UserData::OnChangeMuteSetting
	Action_1_t269755560 * ___OnChangeMuteSetting_9;
	// System.Boolean UserData::_isInitFirstTimeOpenApp
	bool ____isInitFirstTimeOpenApp_11;
	// System.Boolean UserData::_isFirstTimeOpenApp
	bool ____isFirstTimeOpenApp_12;
	// System.Action`1<System.Boolean> UserData::OnChangeFirstTime
	Action_1_t269755560 * ___OnChangeFirstTime_13;
	// System.Boolean UserData::_isInitializedTimeOfPushedHouseAdButton
	bool ____isInitializedTimeOfPushedHouseAdButton_15;
	// System.DateTime UserData::_timeOfPushedHouseAdButton
	DateTime_t3738529785  ____timeOfPushedHouseAdButton_16;

public:
	inline static int32_t get_offset_of__isInitializedPlayCount_1() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedPlayCount_1)); }
	inline bool get__isInitializedPlayCount_1() const { return ____isInitializedPlayCount_1; }
	inline bool* get_address_of__isInitializedPlayCount_1() { return &____isInitializedPlayCount_1; }
	inline void set__isInitializedPlayCount_1(bool value)
	{
		____isInitializedPlayCount_1 = value;
	}

	inline static int32_t get_offset_of__playCount_2() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____playCount_2)); }
	inline int32_t get__playCount_2() const { return ____playCount_2; }
	inline int32_t* get_address_of__playCount_2() { return &____playCount_2; }
	inline void set__playCount_2(int32_t value)
	{
		____playCount_2 = value;
	}

	inline static int32_t get_offset_of__isInitializedHighScoreDict_4() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedHighScoreDict_4)); }
	inline bool get__isInitializedHighScoreDict_4() const { return ____isInitializedHighScoreDict_4; }
	inline bool* get_address_of__isInitializedHighScoreDict_4() { return &____isInitializedHighScoreDict_4; }
	inline void set__isInitializedHighScoreDict_4(bool value)
	{
		____isInitializedHighScoreDict_4 = value;
	}

	inline static int32_t get_offset_of__highScoreDict_5() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____highScoreDict_5)); }
	inline Dictionary_2_t2736202052 * get__highScoreDict_5() const { return ____highScoreDict_5; }
	inline Dictionary_2_t2736202052 ** get_address_of__highScoreDict_5() { return &____highScoreDict_5; }
	inline void set__highScoreDict_5(Dictionary_2_t2736202052 * value)
	{
		____highScoreDict_5 = value;
		Il2CppCodeGenWriteBarrier((&____highScoreDict_5), value);
	}

	inline static int32_t get_offset_of__isInitializedMuteSetting_7() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedMuteSetting_7)); }
	inline bool get__isInitializedMuteSetting_7() const { return ____isInitializedMuteSetting_7; }
	inline bool* get_address_of__isInitializedMuteSetting_7() { return &____isInitializedMuteSetting_7; }
	inline void set__isInitializedMuteSetting_7(bool value)
	{
		____isInitializedMuteSetting_7 = value;
	}

	inline static int32_t get_offset_of__isMute_8() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isMute_8)); }
	inline bool get__isMute_8() const { return ____isMute_8; }
	inline bool* get_address_of__isMute_8() { return &____isMute_8; }
	inline void set__isMute_8(bool value)
	{
		____isMute_8 = value;
	}

	inline static int32_t get_offset_of_OnChangeMuteSetting_9() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ___OnChangeMuteSetting_9)); }
	inline Action_1_t269755560 * get_OnChangeMuteSetting_9() const { return ___OnChangeMuteSetting_9; }
	inline Action_1_t269755560 ** get_address_of_OnChangeMuteSetting_9() { return &___OnChangeMuteSetting_9; }
	inline void set_OnChangeMuteSetting_9(Action_1_t269755560 * value)
	{
		___OnChangeMuteSetting_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnChangeMuteSetting_9), value);
	}

	inline static int32_t get_offset_of__isInitFirstTimeOpenApp_11() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitFirstTimeOpenApp_11)); }
	inline bool get__isInitFirstTimeOpenApp_11() const { return ____isInitFirstTimeOpenApp_11; }
	inline bool* get_address_of__isInitFirstTimeOpenApp_11() { return &____isInitFirstTimeOpenApp_11; }
	inline void set__isInitFirstTimeOpenApp_11(bool value)
	{
		____isInitFirstTimeOpenApp_11 = value;
	}

	inline static int32_t get_offset_of__isFirstTimeOpenApp_12() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isFirstTimeOpenApp_12)); }
	inline bool get__isFirstTimeOpenApp_12() const { return ____isFirstTimeOpenApp_12; }
	inline bool* get_address_of__isFirstTimeOpenApp_12() { return &____isFirstTimeOpenApp_12; }
	inline void set__isFirstTimeOpenApp_12(bool value)
	{
		____isFirstTimeOpenApp_12 = value;
	}

	inline static int32_t get_offset_of_OnChangeFirstTime_13() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ___OnChangeFirstTime_13)); }
	inline Action_1_t269755560 * get_OnChangeFirstTime_13() const { return ___OnChangeFirstTime_13; }
	inline Action_1_t269755560 ** get_address_of_OnChangeFirstTime_13() { return &___OnChangeFirstTime_13; }
	inline void set_OnChangeFirstTime_13(Action_1_t269755560 * value)
	{
		___OnChangeFirstTime_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnChangeFirstTime_13), value);
	}

	inline static int32_t get_offset_of__isInitializedTimeOfPushedHouseAdButton_15() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedTimeOfPushedHouseAdButton_15)); }
	inline bool get__isInitializedTimeOfPushedHouseAdButton_15() const { return ____isInitializedTimeOfPushedHouseAdButton_15; }
	inline bool* get_address_of__isInitializedTimeOfPushedHouseAdButton_15() { return &____isInitializedTimeOfPushedHouseAdButton_15; }
	inline void set__isInitializedTimeOfPushedHouseAdButton_15(bool value)
	{
		____isInitializedTimeOfPushedHouseAdButton_15 = value;
	}

	inline static int32_t get_offset_of__timeOfPushedHouseAdButton_16() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____timeOfPushedHouseAdButton_16)); }
	inline DateTime_t3738529785  get__timeOfPushedHouseAdButton_16() const { return ____timeOfPushedHouseAdButton_16; }
	inline DateTime_t3738529785 * get_address_of__timeOfPushedHouseAdButton_16() { return &____timeOfPushedHouseAdButton_16; }
	inline void set__timeOfPushedHouseAdButton_16(DateTime_t3738529785  value)
	{
		____timeOfPushedHouseAdButton_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDATA_T2822117362_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UNITYPROJECTSETTING_T2579824760_H
#define UNITYPROJECTSETTING_T2579824760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityProjectSetting
struct  UnityProjectSetting_t2579824760  : public ScriptableObject_t2528358522
{
public:
	// GameType UnityProjectSetting::_gameType
	int32_t ____gameType_4;
	// System.Int32 UnityProjectSetting::StageNum
	int32_t ___StageNum_5;
	// System.Int32 UnityProjectSetting::_loadStageNo
	int32_t ____loadStageNo_6;
	// System.String UnityProjectSetting::_shareFormatInTitle
	String_t* ____shareFormatInTitle_7;
	// System.String UnityProjectSetting::_shareFormatInClear
	String_t* ____shareFormatInClear_8;
	// System.String UnityProjectSetting::_shareFormatInMiss
	String_t* ____shareFormatInMiss_9;
	// System.Collections.Generic.Dictionary`2<ShareLocation,System.String> UnityProjectSetting::_shareFormatDict
	Dictionary_2_t3173579796 * ____shareFormatDict_10;
	// System.String UnityProjectSetting::_shareFormatInTitleEnglish
	String_t* ____shareFormatInTitleEnglish_11;
	// System.String UnityProjectSetting::_shareFormatInClearEnglish
	String_t* ____shareFormatInClearEnglish_12;
	// System.String UnityProjectSetting::_shareFormatInMissEnglish
	String_t* ____shareFormatInMissEnglish_13;
	// System.Collections.Generic.Dictionary`2<ShareLocation,System.String> UnityProjectSetting::_shareFormatDictEnglish
	Dictionary_2_t3173579796 * ____shareFormatDictEnglish_14;
	// System.String UnityProjectSetting::_tag
	String_t* ____tag_15;
	// System.String UnityProjectSetting::_iosStoreURL
	String_t* ____iosStoreURL_16;
	// System.String UnityProjectSetting::_androidStoreURL
	String_t* ____androidStoreURL_17;

public:
	inline static int32_t get_offset_of__gameType_4() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____gameType_4)); }
	inline int32_t get__gameType_4() const { return ____gameType_4; }
	inline int32_t* get_address_of__gameType_4() { return &____gameType_4; }
	inline void set__gameType_4(int32_t value)
	{
		____gameType_4 = value;
	}

	inline static int32_t get_offset_of_StageNum_5() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ___StageNum_5)); }
	inline int32_t get_StageNum_5() const { return ___StageNum_5; }
	inline int32_t* get_address_of_StageNum_5() { return &___StageNum_5; }
	inline void set_StageNum_5(int32_t value)
	{
		___StageNum_5 = value;
	}

	inline static int32_t get_offset_of__loadStageNo_6() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____loadStageNo_6)); }
	inline int32_t get__loadStageNo_6() const { return ____loadStageNo_6; }
	inline int32_t* get_address_of__loadStageNo_6() { return &____loadStageNo_6; }
	inline void set__loadStageNo_6(int32_t value)
	{
		____loadStageNo_6 = value;
	}

	inline static int32_t get_offset_of__shareFormatInTitle_7() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInTitle_7)); }
	inline String_t* get__shareFormatInTitle_7() const { return ____shareFormatInTitle_7; }
	inline String_t** get_address_of__shareFormatInTitle_7() { return &____shareFormatInTitle_7; }
	inline void set__shareFormatInTitle_7(String_t* value)
	{
		____shareFormatInTitle_7 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInTitle_7), value);
	}

	inline static int32_t get_offset_of__shareFormatInClear_8() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInClear_8)); }
	inline String_t* get__shareFormatInClear_8() const { return ____shareFormatInClear_8; }
	inline String_t** get_address_of__shareFormatInClear_8() { return &____shareFormatInClear_8; }
	inline void set__shareFormatInClear_8(String_t* value)
	{
		____shareFormatInClear_8 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInClear_8), value);
	}

	inline static int32_t get_offset_of__shareFormatInMiss_9() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInMiss_9)); }
	inline String_t* get__shareFormatInMiss_9() const { return ____shareFormatInMiss_9; }
	inline String_t** get_address_of__shareFormatInMiss_9() { return &____shareFormatInMiss_9; }
	inline void set__shareFormatInMiss_9(String_t* value)
	{
		____shareFormatInMiss_9 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInMiss_9), value);
	}

	inline static int32_t get_offset_of__shareFormatDict_10() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatDict_10)); }
	inline Dictionary_2_t3173579796 * get__shareFormatDict_10() const { return ____shareFormatDict_10; }
	inline Dictionary_2_t3173579796 ** get_address_of__shareFormatDict_10() { return &____shareFormatDict_10; }
	inline void set__shareFormatDict_10(Dictionary_2_t3173579796 * value)
	{
		____shareFormatDict_10 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatDict_10), value);
	}

	inline static int32_t get_offset_of__shareFormatInTitleEnglish_11() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInTitleEnglish_11)); }
	inline String_t* get__shareFormatInTitleEnglish_11() const { return ____shareFormatInTitleEnglish_11; }
	inline String_t** get_address_of__shareFormatInTitleEnglish_11() { return &____shareFormatInTitleEnglish_11; }
	inline void set__shareFormatInTitleEnglish_11(String_t* value)
	{
		____shareFormatInTitleEnglish_11 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInTitleEnglish_11), value);
	}

	inline static int32_t get_offset_of__shareFormatInClearEnglish_12() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInClearEnglish_12)); }
	inline String_t* get__shareFormatInClearEnglish_12() const { return ____shareFormatInClearEnglish_12; }
	inline String_t** get_address_of__shareFormatInClearEnglish_12() { return &____shareFormatInClearEnglish_12; }
	inline void set__shareFormatInClearEnglish_12(String_t* value)
	{
		____shareFormatInClearEnglish_12 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInClearEnglish_12), value);
	}

	inline static int32_t get_offset_of__shareFormatInMissEnglish_13() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInMissEnglish_13)); }
	inline String_t* get__shareFormatInMissEnglish_13() const { return ____shareFormatInMissEnglish_13; }
	inline String_t** get_address_of__shareFormatInMissEnglish_13() { return &____shareFormatInMissEnglish_13; }
	inline void set__shareFormatInMissEnglish_13(String_t* value)
	{
		____shareFormatInMissEnglish_13 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInMissEnglish_13), value);
	}

	inline static int32_t get_offset_of__shareFormatDictEnglish_14() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatDictEnglish_14)); }
	inline Dictionary_2_t3173579796 * get__shareFormatDictEnglish_14() const { return ____shareFormatDictEnglish_14; }
	inline Dictionary_2_t3173579796 ** get_address_of__shareFormatDictEnglish_14() { return &____shareFormatDictEnglish_14; }
	inline void set__shareFormatDictEnglish_14(Dictionary_2_t3173579796 * value)
	{
		____shareFormatDictEnglish_14 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatDictEnglish_14), value);
	}

	inline static int32_t get_offset_of__tag_15() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____tag_15)); }
	inline String_t* get__tag_15() const { return ____tag_15; }
	inline String_t** get_address_of__tag_15() { return &____tag_15; }
	inline void set__tag_15(String_t* value)
	{
		____tag_15 = value;
		Il2CppCodeGenWriteBarrier((&____tag_15), value);
	}

	inline static int32_t get_offset_of__iosStoreURL_16() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____iosStoreURL_16)); }
	inline String_t* get__iosStoreURL_16() const { return ____iosStoreURL_16; }
	inline String_t** get_address_of__iosStoreURL_16() { return &____iosStoreURL_16; }
	inline void set__iosStoreURL_16(String_t* value)
	{
		____iosStoreURL_16 = value;
		Il2CppCodeGenWriteBarrier((&____iosStoreURL_16), value);
	}

	inline static int32_t get_offset_of__androidStoreURL_17() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____androidStoreURL_17)); }
	inline String_t* get__androidStoreURL_17() const { return ____androidStoreURL_17; }
	inline String_t** get_address_of__androidStoreURL_17() { return &____androidStoreURL_17; }
	inline void set__androidStoreURL_17(String_t* value)
	{
		____androidStoreURL_17 = value;
		Il2CppCodeGenWriteBarrier((&____androidStoreURL_17), value);
	}
};

struct UnityProjectSetting_t2579824760_StaticFields
{
public:
	// UnityProjectSetting UnityProjectSetting::_entity
	UnityProjectSetting_t2579824760 * ____entity_3;

public:
	inline static int32_t get_offset_of__entity_3() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760_StaticFields, ____entity_3)); }
	inline UnityProjectSetting_t2579824760 * get__entity_3() const { return ____entity_3; }
	inline UnityProjectSetting_t2579824760 ** get_address_of__entity_3() { return &____entity_3; }
	inline void set__entity_3(UnityProjectSetting_t2579824760 * value)
	{
		____entity_3 = value;
		Il2CppCodeGenWriteBarrier((&____entity_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPROJECTSETTING_T2579824760_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef ONINITIALIZEITEM_T992046894_H
#define ONINITIALIZEITEM_T992046894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWrapContent/OnInitializeItem
struct  OnInitializeItem_t992046894  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINITIALIZEITEM_T992046894_H
#ifndef ANIMATION_T3648466861_H
#define ANIMATION_T3648466861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation
struct  Animation_t3648466861  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T3648466861_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef BOXCOLLIDER_T1640800422_H
#define BOXCOLLIDER_T1640800422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider
struct  BoxCollider_t1640800422  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER_T1640800422_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIRECT_T2875960382_H
#define UIRECT_T2875960382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect
struct  UIRect_t2875960382  : public MonoBehaviour_t3962482529
{
public:
	// UIRect/AnchorPoint UIRect::leftAnchor
	AnchorPoint_t1754718329 * ___leftAnchor_2;
	// UIRect/AnchorPoint UIRect::rightAnchor
	AnchorPoint_t1754718329 * ___rightAnchor_3;
	// UIRect/AnchorPoint UIRect::bottomAnchor
	AnchorPoint_t1754718329 * ___bottomAnchor_4;
	// UIRect/AnchorPoint UIRect::topAnchor
	AnchorPoint_t1754718329 * ___topAnchor_5;
	// UIRect/AnchorUpdate UIRect::updateAnchors
	int32_t ___updateAnchors_6;
	// UnityEngine.GameObject UIRect::mGo
	GameObject_t1113636619 * ___mGo_7;
	// UnityEngine.Transform UIRect::mTrans
	Transform_t3600365921 * ___mTrans_8;
	// BetterList`1<UIRect> UIRect::mChildren
	BetterList_1_t2030980700 * ___mChildren_9;
	// System.Boolean UIRect::mChanged
	bool ___mChanged_10;
	// System.Boolean UIRect::mParentFound
	bool ___mParentFound_11;
	// System.Boolean UIRect::mUpdateAnchors
	bool ___mUpdateAnchors_12;
	// System.Int32 UIRect::mUpdateFrame
	int32_t ___mUpdateFrame_13;
	// System.Boolean UIRect::mAnchorsCached
	bool ___mAnchorsCached_14;
	// UIRoot UIRect::mRoot
	UIRoot_t4022971450 * ___mRoot_15;
	// UIRect UIRect::mParent
	UIRect_t2875960382 * ___mParent_16;
	// System.Boolean UIRect::mRootSet
	bool ___mRootSet_17;
	// UnityEngine.Camera UIRect::mCam
	Camera_t4157153871 * ___mCam_18;
	// System.Boolean UIRect::mStarted
	bool ___mStarted_19;
	// System.Single UIRect::finalAlpha
	float ___finalAlpha_20;

public:
	inline static int32_t get_offset_of_leftAnchor_2() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___leftAnchor_2)); }
	inline AnchorPoint_t1754718329 * get_leftAnchor_2() const { return ___leftAnchor_2; }
	inline AnchorPoint_t1754718329 ** get_address_of_leftAnchor_2() { return &___leftAnchor_2; }
	inline void set_leftAnchor_2(AnchorPoint_t1754718329 * value)
	{
		___leftAnchor_2 = value;
		Il2CppCodeGenWriteBarrier((&___leftAnchor_2), value);
	}

	inline static int32_t get_offset_of_rightAnchor_3() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___rightAnchor_3)); }
	inline AnchorPoint_t1754718329 * get_rightAnchor_3() const { return ___rightAnchor_3; }
	inline AnchorPoint_t1754718329 ** get_address_of_rightAnchor_3() { return &___rightAnchor_3; }
	inline void set_rightAnchor_3(AnchorPoint_t1754718329 * value)
	{
		___rightAnchor_3 = value;
		Il2CppCodeGenWriteBarrier((&___rightAnchor_3), value);
	}

	inline static int32_t get_offset_of_bottomAnchor_4() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___bottomAnchor_4)); }
	inline AnchorPoint_t1754718329 * get_bottomAnchor_4() const { return ___bottomAnchor_4; }
	inline AnchorPoint_t1754718329 ** get_address_of_bottomAnchor_4() { return &___bottomAnchor_4; }
	inline void set_bottomAnchor_4(AnchorPoint_t1754718329 * value)
	{
		___bottomAnchor_4 = value;
		Il2CppCodeGenWriteBarrier((&___bottomAnchor_4), value);
	}

	inline static int32_t get_offset_of_topAnchor_5() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___topAnchor_5)); }
	inline AnchorPoint_t1754718329 * get_topAnchor_5() const { return ___topAnchor_5; }
	inline AnchorPoint_t1754718329 ** get_address_of_topAnchor_5() { return &___topAnchor_5; }
	inline void set_topAnchor_5(AnchorPoint_t1754718329 * value)
	{
		___topAnchor_5 = value;
		Il2CppCodeGenWriteBarrier((&___topAnchor_5), value);
	}

	inline static int32_t get_offset_of_updateAnchors_6() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___updateAnchors_6)); }
	inline int32_t get_updateAnchors_6() const { return ___updateAnchors_6; }
	inline int32_t* get_address_of_updateAnchors_6() { return &___updateAnchors_6; }
	inline void set_updateAnchors_6(int32_t value)
	{
		___updateAnchors_6 = value;
	}

	inline static int32_t get_offset_of_mGo_7() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mGo_7)); }
	inline GameObject_t1113636619 * get_mGo_7() const { return ___mGo_7; }
	inline GameObject_t1113636619 ** get_address_of_mGo_7() { return &___mGo_7; }
	inline void set_mGo_7(GameObject_t1113636619 * value)
	{
		___mGo_7 = value;
		Il2CppCodeGenWriteBarrier((&___mGo_7), value);
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mTrans_8)); }
	inline Transform_t3600365921 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t3600365921 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t3600365921 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mChildren_9() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mChildren_9)); }
	inline BetterList_1_t2030980700 * get_mChildren_9() const { return ___mChildren_9; }
	inline BetterList_1_t2030980700 ** get_address_of_mChildren_9() { return &___mChildren_9; }
	inline void set_mChildren_9(BetterList_1_t2030980700 * value)
	{
		___mChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_9), value);
	}

	inline static int32_t get_offset_of_mChanged_10() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mChanged_10)); }
	inline bool get_mChanged_10() const { return ___mChanged_10; }
	inline bool* get_address_of_mChanged_10() { return &___mChanged_10; }
	inline void set_mChanged_10(bool value)
	{
		___mChanged_10 = value;
	}

	inline static int32_t get_offset_of_mParentFound_11() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mParentFound_11)); }
	inline bool get_mParentFound_11() const { return ___mParentFound_11; }
	inline bool* get_address_of_mParentFound_11() { return &___mParentFound_11; }
	inline void set_mParentFound_11(bool value)
	{
		___mParentFound_11 = value;
	}

	inline static int32_t get_offset_of_mUpdateAnchors_12() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mUpdateAnchors_12)); }
	inline bool get_mUpdateAnchors_12() const { return ___mUpdateAnchors_12; }
	inline bool* get_address_of_mUpdateAnchors_12() { return &___mUpdateAnchors_12; }
	inline void set_mUpdateAnchors_12(bool value)
	{
		___mUpdateAnchors_12 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrame_13() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mUpdateFrame_13)); }
	inline int32_t get_mUpdateFrame_13() const { return ___mUpdateFrame_13; }
	inline int32_t* get_address_of_mUpdateFrame_13() { return &___mUpdateFrame_13; }
	inline void set_mUpdateFrame_13(int32_t value)
	{
		___mUpdateFrame_13 = value;
	}

	inline static int32_t get_offset_of_mAnchorsCached_14() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mAnchorsCached_14)); }
	inline bool get_mAnchorsCached_14() const { return ___mAnchorsCached_14; }
	inline bool* get_address_of_mAnchorsCached_14() { return &___mAnchorsCached_14; }
	inline void set_mAnchorsCached_14(bool value)
	{
		___mAnchorsCached_14 = value;
	}

	inline static int32_t get_offset_of_mRoot_15() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mRoot_15)); }
	inline UIRoot_t4022971450 * get_mRoot_15() const { return ___mRoot_15; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_15() { return &___mRoot_15; }
	inline void set_mRoot_15(UIRoot_t4022971450 * value)
	{
		___mRoot_15 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_15), value);
	}

	inline static int32_t get_offset_of_mParent_16() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mParent_16)); }
	inline UIRect_t2875960382 * get_mParent_16() const { return ___mParent_16; }
	inline UIRect_t2875960382 ** get_address_of_mParent_16() { return &___mParent_16; }
	inline void set_mParent_16(UIRect_t2875960382 * value)
	{
		___mParent_16 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_16), value);
	}

	inline static int32_t get_offset_of_mRootSet_17() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mRootSet_17)); }
	inline bool get_mRootSet_17() const { return ___mRootSet_17; }
	inline bool* get_address_of_mRootSet_17() { return &___mRootSet_17; }
	inline void set_mRootSet_17(bool value)
	{
		___mRootSet_17 = value;
	}

	inline static int32_t get_offset_of_mCam_18() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mCam_18)); }
	inline Camera_t4157153871 * get_mCam_18() const { return ___mCam_18; }
	inline Camera_t4157153871 ** get_address_of_mCam_18() { return &___mCam_18; }
	inline void set_mCam_18(Camera_t4157153871 * value)
	{
		___mCam_18 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_18), value);
	}

	inline static int32_t get_offset_of_mStarted_19() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mStarted_19)); }
	inline bool get_mStarted_19() const { return ___mStarted_19; }
	inline bool* get_address_of_mStarted_19() { return &___mStarted_19; }
	inline void set_mStarted_19(bool value)
	{
		___mStarted_19 = value;
	}

	inline static int32_t get_offset_of_finalAlpha_20() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___finalAlpha_20)); }
	inline float get_finalAlpha_20() const { return ___finalAlpha_20; }
	inline float* get_address_of_finalAlpha_20() { return &___finalAlpha_20; }
	inline void set_finalAlpha_20(float value)
	{
		___finalAlpha_20 = value;
	}
};

struct UIRect_t2875960382_StaticFields
{
public:
	// UnityEngine.Vector3[] UIRect::mSides
	Vector3U5BU5D_t1718750761* ___mSides_21;

public:
	inline static int32_t get_offset_of_mSides_21() { return static_cast<int32_t>(offsetof(UIRect_t2875960382_StaticFields, ___mSides_21)); }
	inline Vector3U5BU5D_t1718750761* get_mSides_21() const { return ___mSides_21; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mSides_21() { return &___mSides_21; }
	inline void set_mSides_21(Vector3U5BU5D_t1718750761* value)
	{
		___mSides_21 = value;
		Il2CppCodeGenWriteBarrier((&___mSides_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIRECT_T2875960382_H
#ifndef UICAMERA_T1356438871_H
#define UICAMERA_T1356438871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera
struct  UICamera_t1356438871  : public MonoBehaviour_t3962482529
{
public:
	// UICamera/EventType UICamera::eventType
	int32_t ___eventType_12;
	// System.Boolean UICamera::eventsGoToColliders
	bool ___eventsGoToColliders_13;
	// UnityEngine.LayerMask UICamera::eventReceiverMask
	LayerMask_t3493934918  ___eventReceiverMask_14;
	// UICamera/ProcessEventsIn UICamera::processEventsIn
	int32_t ___processEventsIn_15;
	// System.Boolean UICamera::debug
	bool ___debug_16;
	// System.Boolean UICamera::useMouse
	bool ___useMouse_17;
	// System.Boolean UICamera::useTouch
	bool ___useTouch_18;
	// System.Boolean UICamera::allowMultiTouch
	bool ___allowMultiTouch_19;
	// System.Boolean UICamera::useKeyboard
	bool ___useKeyboard_20;
	// System.Boolean UICamera::useController
	bool ___useController_21;
	// System.Boolean UICamera::stickyTooltip
	bool ___stickyTooltip_22;
	// System.Single UICamera::tooltipDelay
	float ___tooltipDelay_23;
	// System.Boolean UICamera::longPressTooltip
	bool ___longPressTooltip_24;
	// System.Single UICamera::mouseDragThreshold
	float ___mouseDragThreshold_25;
	// System.Single UICamera::mouseClickThreshold
	float ___mouseClickThreshold_26;
	// System.Single UICamera::touchDragThreshold
	float ___touchDragThreshold_27;
	// System.Single UICamera::touchClickThreshold
	float ___touchClickThreshold_28;
	// System.Single UICamera::rangeDistance
	float ___rangeDistance_29;
	// System.String UICamera::horizontalAxisName
	String_t* ___horizontalAxisName_30;
	// System.String UICamera::verticalAxisName
	String_t* ___verticalAxisName_31;
	// System.String UICamera::horizontalPanAxisName
	String_t* ___horizontalPanAxisName_32;
	// System.String UICamera::verticalPanAxisName
	String_t* ___verticalPanAxisName_33;
	// System.String UICamera::scrollAxisName
	String_t* ___scrollAxisName_34;
	// System.Boolean UICamera::commandClick
	bool ___commandClick_35;
	// UnityEngine.KeyCode UICamera::submitKey0
	int32_t ___submitKey0_36;
	// UnityEngine.KeyCode UICamera::submitKey1
	int32_t ___submitKey1_37;
	// UnityEngine.KeyCode UICamera::cancelKey0
	int32_t ___cancelKey0_38;
	// UnityEngine.KeyCode UICamera::cancelKey1
	int32_t ___cancelKey1_39;
	// System.Boolean UICamera::autoHideCursor
	bool ___autoHideCursor_40;
	// UnityEngine.Camera UICamera::mCam
	Camera_t4157153871 * ___mCam_84;
	// System.Single UICamera::mNextRaycast
	float ___mNextRaycast_86;

public:
	inline static int32_t get_offset_of_eventType_12() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventType_12)); }
	inline int32_t get_eventType_12() const { return ___eventType_12; }
	inline int32_t* get_address_of_eventType_12() { return &___eventType_12; }
	inline void set_eventType_12(int32_t value)
	{
		___eventType_12 = value;
	}

	inline static int32_t get_offset_of_eventsGoToColliders_13() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventsGoToColliders_13)); }
	inline bool get_eventsGoToColliders_13() const { return ___eventsGoToColliders_13; }
	inline bool* get_address_of_eventsGoToColliders_13() { return &___eventsGoToColliders_13; }
	inline void set_eventsGoToColliders_13(bool value)
	{
		___eventsGoToColliders_13 = value;
	}

	inline static int32_t get_offset_of_eventReceiverMask_14() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventReceiverMask_14)); }
	inline LayerMask_t3493934918  get_eventReceiverMask_14() const { return ___eventReceiverMask_14; }
	inline LayerMask_t3493934918 * get_address_of_eventReceiverMask_14() { return &___eventReceiverMask_14; }
	inline void set_eventReceiverMask_14(LayerMask_t3493934918  value)
	{
		___eventReceiverMask_14 = value;
	}

	inline static int32_t get_offset_of_processEventsIn_15() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___processEventsIn_15)); }
	inline int32_t get_processEventsIn_15() const { return ___processEventsIn_15; }
	inline int32_t* get_address_of_processEventsIn_15() { return &___processEventsIn_15; }
	inline void set_processEventsIn_15(int32_t value)
	{
		___processEventsIn_15 = value;
	}

	inline static int32_t get_offset_of_debug_16() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___debug_16)); }
	inline bool get_debug_16() const { return ___debug_16; }
	inline bool* get_address_of_debug_16() { return &___debug_16; }
	inline void set_debug_16(bool value)
	{
		___debug_16 = value;
	}

	inline static int32_t get_offset_of_useMouse_17() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useMouse_17)); }
	inline bool get_useMouse_17() const { return ___useMouse_17; }
	inline bool* get_address_of_useMouse_17() { return &___useMouse_17; }
	inline void set_useMouse_17(bool value)
	{
		___useMouse_17 = value;
	}

	inline static int32_t get_offset_of_useTouch_18() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useTouch_18)); }
	inline bool get_useTouch_18() const { return ___useTouch_18; }
	inline bool* get_address_of_useTouch_18() { return &___useTouch_18; }
	inline void set_useTouch_18(bool value)
	{
		___useTouch_18 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_19() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___allowMultiTouch_19)); }
	inline bool get_allowMultiTouch_19() const { return ___allowMultiTouch_19; }
	inline bool* get_address_of_allowMultiTouch_19() { return &___allowMultiTouch_19; }
	inline void set_allowMultiTouch_19(bool value)
	{
		___allowMultiTouch_19 = value;
	}

	inline static int32_t get_offset_of_useKeyboard_20() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useKeyboard_20)); }
	inline bool get_useKeyboard_20() const { return ___useKeyboard_20; }
	inline bool* get_address_of_useKeyboard_20() { return &___useKeyboard_20; }
	inline void set_useKeyboard_20(bool value)
	{
		___useKeyboard_20 = value;
	}

	inline static int32_t get_offset_of_useController_21() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useController_21)); }
	inline bool get_useController_21() const { return ___useController_21; }
	inline bool* get_address_of_useController_21() { return &___useController_21; }
	inline void set_useController_21(bool value)
	{
		___useController_21 = value;
	}

	inline static int32_t get_offset_of_stickyTooltip_22() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___stickyTooltip_22)); }
	inline bool get_stickyTooltip_22() const { return ___stickyTooltip_22; }
	inline bool* get_address_of_stickyTooltip_22() { return &___stickyTooltip_22; }
	inline void set_stickyTooltip_22(bool value)
	{
		___stickyTooltip_22 = value;
	}

	inline static int32_t get_offset_of_tooltipDelay_23() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___tooltipDelay_23)); }
	inline float get_tooltipDelay_23() const { return ___tooltipDelay_23; }
	inline float* get_address_of_tooltipDelay_23() { return &___tooltipDelay_23; }
	inline void set_tooltipDelay_23(float value)
	{
		___tooltipDelay_23 = value;
	}

	inline static int32_t get_offset_of_longPressTooltip_24() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___longPressTooltip_24)); }
	inline bool get_longPressTooltip_24() const { return ___longPressTooltip_24; }
	inline bool* get_address_of_longPressTooltip_24() { return &___longPressTooltip_24; }
	inline void set_longPressTooltip_24(bool value)
	{
		___longPressTooltip_24 = value;
	}

	inline static int32_t get_offset_of_mouseDragThreshold_25() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mouseDragThreshold_25)); }
	inline float get_mouseDragThreshold_25() const { return ___mouseDragThreshold_25; }
	inline float* get_address_of_mouseDragThreshold_25() { return &___mouseDragThreshold_25; }
	inline void set_mouseDragThreshold_25(float value)
	{
		___mouseDragThreshold_25 = value;
	}

	inline static int32_t get_offset_of_mouseClickThreshold_26() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mouseClickThreshold_26)); }
	inline float get_mouseClickThreshold_26() const { return ___mouseClickThreshold_26; }
	inline float* get_address_of_mouseClickThreshold_26() { return &___mouseClickThreshold_26; }
	inline void set_mouseClickThreshold_26(float value)
	{
		___mouseClickThreshold_26 = value;
	}

	inline static int32_t get_offset_of_touchDragThreshold_27() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___touchDragThreshold_27)); }
	inline float get_touchDragThreshold_27() const { return ___touchDragThreshold_27; }
	inline float* get_address_of_touchDragThreshold_27() { return &___touchDragThreshold_27; }
	inline void set_touchDragThreshold_27(float value)
	{
		___touchDragThreshold_27 = value;
	}

	inline static int32_t get_offset_of_touchClickThreshold_28() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___touchClickThreshold_28)); }
	inline float get_touchClickThreshold_28() const { return ___touchClickThreshold_28; }
	inline float* get_address_of_touchClickThreshold_28() { return &___touchClickThreshold_28; }
	inline void set_touchClickThreshold_28(float value)
	{
		___touchClickThreshold_28 = value;
	}

	inline static int32_t get_offset_of_rangeDistance_29() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___rangeDistance_29)); }
	inline float get_rangeDistance_29() const { return ___rangeDistance_29; }
	inline float* get_address_of_rangeDistance_29() { return &___rangeDistance_29; }
	inline void set_rangeDistance_29(float value)
	{
		___rangeDistance_29 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_30() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___horizontalAxisName_30)); }
	inline String_t* get_horizontalAxisName_30() const { return ___horizontalAxisName_30; }
	inline String_t** get_address_of_horizontalAxisName_30() { return &___horizontalAxisName_30; }
	inline void set_horizontalAxisName_30(String_t* value)
	{
		___horizontalAxisName_30 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_30), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_31() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___verticalAxisName_31)); }
	inline String_t* get_verticalAxisName_31() const { return ___verticalAxisName_31; }
	inline String_t** get_address_of_verticalAxisName_31() { return &___verticalAxisName_31; }
	inline void set_verticalAxisName_31(String_t* value)
	{
		___verticalAxisName_31 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_31), value);
	}

	inline static int32_t get_offset_of_horizontalPanAxisName_32() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___horizontalPanAxisName_32)); }
	inline String_t* get_horizontalPanAxisName_32() const { return ___horizontalPanAxisName_32; }
	inline String_t** get_address_of_horizontalPanAxisName_32() { return &___horizontalPanAxisName_32; }
	inline void set_horizontalPanAxisName_32(String_t* value)
	{
		___horizontalPanAxisName_32 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalPanAxisName_32), value);
	}

	inline static int32_t get_offset_of_verticalPanAxisName_33() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___verticalPanAxisName_33)); }
	inline String_t* get_verticalPanAxisName_33() const { return ___verticalPanAxisName_33; }
	inline String_t** get_address_of_verticalPanAxisName_33() { return &___verticalPanAxisName_33; }
	inline void set_verticalPanAxisName_33(String_t* value)
	{
		___verticalPanAxisName_33 = value;
		Il2CppCodeGenWriteBarrier((&___verticalPanAxisName_33), value);
	}

	inline static int32_t get_offset_of_scrollAxisName_34() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___scrollAxisName_34)); }
	inline String_t* get_scrollAxisName_34() const { return ___scrollAxisName_34; }
	inline String_t** get_address_of_scrollAxisName_34() { return &___scrollAxisName_34; }
	inline void set_scrollAxisName_34(String_t* value)
	{
		___scrollAxisName_34 = value;
		Il2CppCodeGenWriteBarrier((&___scrollAxisName_34), value);
	}

	inline static int32_t get_offset_of_commandClick_35() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___commandClick_35)); }
	inline bool get_commandClick_35() const { return ___commandClick_35; }
	inline bool* get_address_of_commandClick_35() { return &___commandClick_35; }
	inline void set_commandClick_35(bool value)
	{
		___commandClick_35 = value;
	}

	inline static int32_t get_offset_of_submitKey0_36() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___submitKey0_36)); }
	inline int32_t get_submitKey0_36() const { return ___submitKey0_36; }
	inline int32_t* get_address_of_submitKey0_36() { return &___submitKey0_36; }
	inline void set_submitKey0_36(int32_t value)
	{
		___submitKey0_36 = value;
	}

	inline static int32_t get_offset_of_submitKey1_37() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___submitKey1_37)); }
	inline int32_t get_submitKey1_37() const { return ___submitKey1_37; }
	inline int32_t* get_address_of_submitKey1_37() { return &___submitKey1_37; }
	inline void set_submitKey1_37(int32_t value)
	{
		___submitKey1_37 = value;
	}

	inline static int32_t get_offset_of_cancelKey0_38() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___cancelKey0_38)); }
	inline int32_t get_cancelKey0_38() const { return ___cancelKey0_38; }
	inline int32_t* get_address_of_cancelKey0_38() { return &___cancelKey0_38; }
	inline void set_cancelKey0_38(int32_t value)
	{
		___cancelKey0_38 = value;
	}

	inline static int32_t get_offset_of_cancelKey1_39() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___cancelKey1_39)); }
	inline int32_t get_cancelKey1_39() const { return ___cancelKey1_39; }
	inline int32_t* get_address_of_cancelKey1_39() { return &___cancelKey1_39; }
	inline void set_cancelKey1_39(int32_t value)
	{
		___cancelKey1_39 = value;
	}

	inline static int32_t get_offset_of_autoHideCursor_40() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___autoHideCursor_40)); }
	inline bool get_autoHideCursor_40() const { return ___autoHideCursor_40; }
	inline bool* get_address_of_autoHideCursor_40() { return &___autoHideCursor_40; }
	inline void set_autoHideCursor_40(bool value)
	{
		___autoHideCursor_40 = value;
	}

	inline static int32_t get_offset_of_mCam_84() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mCam_84)); }
	inline Camera_t4157153871 * get_mCam_84() const { return ___mCam_84; }
	inline Camera_t4157153871 ** get_address_of_mCam_84() { return &___mCam_84; }
	inline void set_mCam_84(Camera_t4157153871 * value)
	{
		___mCam_84 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_84), value);
	}

	inline static int32_t get_offset_of_mNextRaycast_86() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mNextRaycast_86)); }
	inline float get_mNextRaycast_86() const { return ___mNextRaycast_86; }
	inline float* get_address_of_mNextRaycast_86() { return &___mNextRaycast_86; }
	inline void set_mNextRaycast_86(float value)
	{
		___mNextRaycast_86 = value;
	}
};

struct UICamera_t1356438871_StaticFields
{
public:
	// BetterList`1<UICamera> UICamera::list
	BetterList_1_t511459189 * ___list_2;
	// UICamera/GetKeyStateFunc UICamera::GetKeyDown
	GetKeyStateFunc_t2810275146 * ___GetKeyDown_3;
	// UICamera/GetKeyStateFunc UICamera::GetKeyUp
	GetKeyStateFunc_t2810275146 * ___GetKeyUp_4;
	// UICamera/GetKeyStateFunc UICamera::GetKey
	GetKeyStateFunc_t2810275146 * ___GetKey_5;
	// UICamera/GetAxisFunc UICamera::GetAxis
	GetAxisFunc_t2592608932 * ___GetAxis_6;
	// UICamera/GetAnyKeyFunc UICamera::GetAnyKeyDown
	GetAnyKeyFunc_t1761480072 * ___GetAnyKeyDown_7;
	// UICamera/GetMouseDelegate UICamera::GetMouse
	GetMouseDelegate_t662790529 * ___GetMouse_8;
	// UICamera/GetTouchDelegate UICamera::GetTouch
	GetTouchDelegate_t4218246285 * ___GetTouch_9;
	// UICamera/RemoveTouchDelegate UICamera::RemoveTouch
	RemoveTouchDelegate_t2508278027 * ___RemoveTouch_10;
	// UICamera/OnScreenResize UICamera::onScreenResize
	OnScreenResize_t2279991692 * ___onScreenResize_11;
	// UICamera/OnCustomInput UICamera::onCustomInput
	OnCustomInput_t3508588789 * ___onCustomInput_41;
	// System.Boolean UICamera::showTooltips
	bool ___showTooltips_42;
	// System.Boolean UICamera::ignoreAllEvents
	bool ___ignoreAllEvents_43;
	// System.Boolean UICamera::ignoreControllerInput
	bool ___ignoreControllerInput_44;
	// System.Boolean UICamera::mDisableController
	bool ___mDisableController_45;
	// UnityEngine.Vector2 UICamera::mLastPos
	Vector2_t2156229523  ___mLastPos_46;
	// UnityEngine.Vector3 UICamera::lastWorldPosition
	Vector3_t3722313464  ___lastWorldPosition_47;
	// UnityEngine.Ray UICamera::lastWorldRay
	Ray_t3785851493  ___lastWorldRay_48;
	// UnityEngine.RaycastHit UICamera::lastHit
	RaycastHit_t1056001966  ___lastHit_49;
	// UICamera UICamera::current
	UICamera_t1356438871 * ___current_50;
	// UnityEngine.Camera UICamera::currentCamera
	Camera_t4157153871 * ___currentCamera_51;
	// UICamera/OnSchemeChange UICamera::onSchemeChange
	OnSchemeChange_t1701155603 * ___onSchemeChange_52;
	// UICamera/ControlScheme UICamera::mLastScheme
	int32_t ___mLastScheme_53;
	// System.Int32 UICamera::currentTouchID
	int32_t ___currentTouchID_54;
	// UnityEngine.KeyCode UICamera::mCurrentKey
	int32_t ___mCurrentKey_55;
	// UICamera/MouseOrTouch UICamera::currentTouch
	MouseOrTouch_t3052596533 * ___currentTouch_56;
	// System.Boolean UICamera::mInputFocus
	bool ___mInputFocus_57;
	// UnityEngine.GameObject UICamera::mGenericHandler
	GameObject_t1113636619 * ___mGenericHandler_58;
	// UnityEngine.GameObject UICamera::fallThrough
	GameObject_t1113636619 * ___fallThrough_59;
	// UICamera/VoidDelegate UICamera::onClick
	VoidDelegate_t3100799918 * ___onClick_60;
	// UICamera/VoidDelegate UICamera::onDoubleClick
	VoidDelegate_t3100799918 * ___onDoubleClick_61;
	// UICamera/BoolDelegate UICamera::onHover
	BoolDelegate_t3825226153 * ___onHover_62;
	// UICamera/BoolDelegate UICamera::onPress
	BoolDelegate_t3825226153 * ___onPress_63;
	// UICamera/BoolDelegate UICamera::onSelect
	BoolDelegate_t3825226153 * ___onSelect_64;
	// UICamera/FloatDelegate UICamera::onScroll
	FloatDelegate_t906524069 * ___onScroll_65;
	// UICamera/VectorDelegate UICamera::onDrag
	VectorDelegate_t435795517 * ___onDrag_66;
	// UICamera/VoidDelegate UICamera::onDragStart
	VoidDelegate_t3100799918 * ___onDragStart_67;
	// UICamera/ObjectDelegate UICamera::onDragOver
	ObjectDelegate_t2041570719 * ___onDragOver_68;
	// UICamera/ObjectDelegate UICamera::onDragOut
	ObjectDelegate_t2041570719 * ___onDragOut_69;
	// UICamera/VoidDelegate UICamera::onDragEnd
	VoidDelegate_t3100799918 * ___onDragEnd_70;
	// UICamera/ObjectDelegate UICamera::onDrop
	ObjectDelegate_t2041570719 * ___onDrop_71;
	// UICamera/KeyCodeDelegate UICamera::onKey
	KeyCodeDelegate_t3064672302 * ___onKey_72;
	// UICamera/KeyCodeDelegate UICamera::onNavigate
	KeyCodeDelegate_t3064672302 * ___onNavigate_73;
	// UICamera/VectorDelegate UICamera::onPan
	VectorDelegate_t435795517 * ___onPan_74;
	// UICamera/BoolDelegate UICamera::onTooltip
	BoolDelegate_t3825226153 * ___onTooltip_75;
	// UICamera/MoveDelegate UICamera::onMouseMove
	MoveDelegate_t16019400 * ___onMouseMove_76;
	// UICamera/MouseOrTouch[] UICamera::mMouse
	MouseOrTouchU5BU5D_t3534648920* ___mMouse_77;
	// UICamera/MouseOrTouch UICamera::controller
	MouseOrTouch_t3052596533 * ___controller_78;
	// System.Collections.Generic.List`1<UICamera/MouseOrTouch> UICamera::activeTouches
	List_1_t229703979 * ___activeTouches_79;
	// System.Collections.Generic.List`1<System.Int32> UICamera::mTouchIDs
	List_1_t128053199 * ___mTouchIDs_80;
	// System.Int32 UICamera::mWidth
	int32_t ___mWidth_81;
	// System.Int32 UICamera::mHeight
	int32_t ___mHeight_82;
	// UnityEngine.GameObject UICamera::mTooltip
	GameObject_t1113636619 * ___mTooltip_83;
	// System.Single UICamera::mTooltipTime
	float ___mTooltipTime_85;
	// System.Boolean UICamera::isDragging
	bool ___isDragging_87;
	// System.Int32 UICamera::mLastInteractionCheck
	int32_t ___mLastInteractionCheck_88;
	// System.Boolean UICamera::mLastInteractionResult
	bool ___mLastInteractionResult_89;
	// System.Int32 UICamera::mLastFocusCheck
	int32_t ___mLastFocusCheck_90;
	// System.Boolean UICamera::mLastFocusResult
	bool ___mLastFocusResult_91;
	// System.Int32 UICamera::mLastOverCheck
	int32_t ___mLastOverCheck_92;
	// System.Boolean UICamera::mLastOverResult
	bool ___mLastOverResult_93;
	// UnityEngine.GameObject UICamera::mRayHitObject
	GameObject_t1113636619 * ___mRayHitObject_94;
	// UnityEngine.GameObject UICamera::mHover
	GameObject_t1113636619 * ___mHover_95;
	// UnityEngine.GameObject UICamera::mSelected
	GameObject_t1113636619 * ___mSelected_96;
	// UICamera/DepthEntry UICamera::mHit
	DepthEntry_t628749918  ___mHit_97;
	// BetterList`1<UICamera/DepthEntry> UICamera::mHits
	BetterList_1_t4078737532 * ___mHits_98;
	// UnityEngine.RaycastHit[] UICamera::mRayHits
	RaycastHitU5BU5D_t1690781147* ___mRayHits_99;
	// UnityEngine.Collider2D[] UICamera::mOverlap
	Collider2DU5BU5D_t1693969295* ___mOverlap_100;
	// UnityEngine.Plane UICamera::m2DPlane
	Plane_t1000493321  ___m2DPlane_101;
	// System.Single UICamera::mNextEvent
	float ___mNextEvent_102;
	// System.Int32 UICamera::mNotifying
	int32_t ___mNotifying_103;
	// System.Boolean UICamera::mUsingTouchEvents
	bool ___mUsingTouchEvents_104;
	// UICamera/GetTouchCountCallback UICamera::GetInputTouchCount
	GetTouchCountCallback_t3185863032 * ___GetInputTouchCount_105;
	// UICamera/GetTouchCallback UICamera::GetInputTouch
	GetTouchCallback_t97678626 * ___GetInputTouch_106;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache0
	CompareFunc_t2967399990 * ___U3CU3Ef__amU24cache0_107;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache1
	CompareFunc_t2967399990 * ___U3CU3Ef__amU24cache1_108;
	// BetterList`1/CompareFunc<UICamera> UICamera::<>f__mg$cache0
	CompareFunc_t3695088943 * ___U3CU3Ef__mgU24cache0_109;
	// BetterList`1/CompareFunc<UICamera> UICamera::<>f__mg$cache1
	CompareFunc_t3695088943 * ___U3CU3Ef__mgU24cache1_110;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___list_2)); }
	inline BetterList_1_t511459189 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t511459189 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t511459189 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of_GetKeyDown_3() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKeyDown_3)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKeyDown_3() const { return ___GetKeyDown_3; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKeyDown_3() { return &___GetKeyDown_3; }
	inline void set_GetKeyDown_3(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKeyDown_3 = value;
		Il2CppCodeGenWriteBarrier((&___GetKeyDown_3), value);
	}

	inline static int32_t get_offset_of_GetKeyUp_4() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKeyUp_4)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKeyUp_4() const { return ___GetKeyUp_4; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKeyUp_4() { return &___GetKeyUp_4; }
	inline void set_GetKeyUp_4(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKeyUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___GetKeyUp_4), value);
	}

	inline static int32_t get_offset_of_GetKey_5() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKey_5)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKey_5() const { return ___GetKey_5; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKey_5() { return &___GetKey_5; }
	inline void set_GetKey_5(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___GetKey_5), value);
	}

	inline static int32_t get_offset_of_GetAxis_6() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetAxis_6)); }
	inline GetAxisFunc_t2592608932 * get_GetAxis_6() const { return ___GetAxis_6; }
	inline GetAxisFunc_t2592608932 ** get_address_of_GetAxis_6() { return &___GetAxis_6; }
	inline void set_GetAxis_6(GetAxisFunc_t2592608932 * value)
	{
		___GetAxis_6 = value;
		Il2CppCodeGenWriteBarrier((&___GetAxis_6), value);
	}

	inline static int32_t get_offset_of_GetAnyKeyDown_7() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetAnyKeyDown_7)); }
	inline GetAnyKeyFunc_t1761480072 * get_GetAnyKeyDown_7() const { return ___GetAnyKeyDown_7; }
	inline GetAnyKeyFunc_t1761480072 ** get_address_of_GetAnyKeyDown_7() { return &___GetAnyKeyDown_7; }
	inline void set_GetAnyKeyDown_7(GetAnyKeyFunc_t1761480072 * value)
	{
		___GetAnyKeyDown_7 = value;
		Il2CppCodeGenWriteBarrier((&___GetAnyKeyDown_7), value);
	}

	inline static int32_t get_offset_of_GetMouse_8() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetMouse_8)); }
	inline GetMouseDelegate_t662790529 * get_GetMouse_8() const { return ___GetMouse_8; }
	inline GetMouseDelegate_t662790529 ** get_address_of_GetMouse_8() { return &___GetMouse_8; }
	inline void set_GetMouse_8(GetMouseDelegate_t662790529 * value)
	{
		___GetMouse_8 = value;
		Il2CppCodeGenWriteBarrier((&___GetMouse_8), value);
	}

	inline static int32_t get_offset_of_GetTouch_9() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetTouch_9)); }
	inline GetTouchDelegate_t4218246285 * get_GetTouch_9() const { return ___GetTouch_9; }
	inline GetTouchDelegate_t4218246285 ** get_address_of_GetTouch_9() { return &___GetTouch_9; }
	inline void set_GetTouch_9(GetTouchDelegate_t4218246285 * value)
	{
		___GetTouch_9 = value;
		Il2CppCodeGenWriteBarrier((&___GetTouch_9), value);
	}

	inline static int32_t get_offset_of_RemoveTouch_10() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___RemoveTouch_10)); }
	inline RemoveTouchDelegate_t2508278027 * get_RemoveTouch_10() const { return ___RemoveTouch_10; }
	inline RemoveTouchDelegate_t2508278027 ** get_address_of_RemoveTouch_10() { return &___RemoveTouch_10; }
	inline void set_RemoveTouch_10(RemoveTouchDelegate_t2508278027 * value)
	{
		___RemoveTouch_10 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveTouch_10), value);
	}

	inline static int32_t get_offset_of_onScreenResize_11() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onScreenResize_11)); }
	inline OnScreenResize_t2279991692 * get_onScreenResize_11() const { return ___onScreenResize_11; }
	inline OnScreenResize_t2279991692 ** get_address_of_onScreenResize_11() { return &___onScreenResize_11; }
	inline void set_onScreenResize_11(OnScreenResize_t2279991692 * value)
	{
		___onScreenResize_11 = value;
		Il2CppCodeGenWriteBarrier((&___onScreenResize_11), value);
	}

	inline static int32_t get_offset_of_onCustomInput_41() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onCustomInput_41)); }
	inline OnCustomInput_t3508588789 * get_onCustomInput_41() const { return ___onCustomInput_41; }
	inline OnCustomInput_t3508588789 ** get_address_of_onCustomInput_41() { return &___onCustomInput_41; }
	inline void set_onCustomInput_41(OnCustomInput_t3508588789 * value)
	{
		___onCustomInput_41 = value;
		Il2CppCodeGenWriteBarrier((&___onCustomInput_41), value);
	}

	inline static int32_t get_offset_of_showTooltips_42() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___showTooltips_42)); }
	inline bool get_showTooltips_42() const { return ___showTooltips_42; }
	inline bool* get_address_of_showTooltips_42() { return &___showTooltips_42; }
	inline void set_showTooltips_42(bool value)
	{
		___showTooltips_42 = value;
	}

	inline static int32_t get_offset_of_ignoreAllEvents_43() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___ignoreAllEvents_43)); }
	inline bool get_ignoreAllEvents_43() const { return ___ignoreAllEvents_43; }
	inline bool* get_address_of_ignoreAllEvents_43() { return &___ignoreAllEvents_43; }
	inline void set_ignoreAllEvents_43(bool value)
	{
		___ignoreAllEvents_43 = value;
	}

	inline static int32_t get_offset_of_ignoreControllerInput_44() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___ignoreControllerInput_44)); }
	inline bool get_ignoreControllerInput_44() const { return ___ignoreControllerInput_44; }
	inline bool* get_address_of_ignoreControllerInput_44() { return &___ignoreControllerInput_44; }
	inline void set_ignoreControllerInput_44(bool value)
	{
		___ignoreControllerInput_44 = value;
	}

	inline static int32_t get_offset_of_mDisableController_45() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mDisableController_45)); }
	inline bool get_mDisableController_45() const { return ___mDisableController_45; }
	inline bool* get_address_of_mDisableController_45() { return &___mDisableController_45; }
	inline void set_mDisableController_45(bool value)
	{
		___mDisableController_45 = value;
	}

	inline static int32_t get_offset_of_mLastPos_46() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastPos_46)); }
	inline Vector2_t2156229523  get_mLastPos_46() const { return ___mLastPos_46; }
	inline Vector2_t2156229523 * get_address_of_mLastPos_46() { return &___mLastPos_46; }
	inline void set_mLastPos_46(Vector2_t2156229523  value)
	{
		___mLastPos_46 = value;
	}

	inline static int32_t get_offset_of_lastWorldPosition_47() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastWorldPosition_47)); }
	inline Vector3_t3722313464  get_lastWorldPosition_47() const { return ___lastWorldPosition_47; }
	inline Vector3_t3722313464 * get_address_of_lastWorldPosition_47() { return &___lastWorldPosition_47; }
	inline void set_lastWorldPosition_47(Vector3_t3722313464  value)
	{
		___lastWorldPosition_47 = value;
	}

	inline static int32_t get_offset_of_lastWorldRay_48() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastWorldRay_48)); }
	inline Ray_t3785851493  get_lastWorldRay_48() const { return ___lastWorldRay_48; }
	inline Ray_t3785851493 * get_address_of_lastWorldRay_48() { return &___lastWorldRay_48; }
	inline void set_lastWorldRay_48(Ray_t3785851493  value)
	{
		___lastWorldRay_48 = value;
	}

	inline static int32_t get_offset_of_lastHit_49() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastHit_49)); }
	inline RaycastHit_t1056001966  get_lastHit_49() const { return ___lastHit_49; }
	inline RaycastHit_t1056001966 * get_address_of_lastHit_49() { return &___lastHit_49; }
	inline void set_lastHit_49(RaycastHit_t1056001966  value)
	{
		___lastHit_49 = value;
	}

	inline static int32_t get_offset_of_current_50() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___current_50)); }
	inline UICamera_t1356438871 * get_current_50() const { return ___current_50; }
	inline UICamera_t1356438871 ** get_address_of_current_50() { return &___current_50; }
	inline void set_current_50(UICamera_t1356438871 * value)
	{
		___current_50 = value;
		Il2CppCodeGenWriteBarrier((&___current_50), value);
	}

	inline static int32_t get_offset_of_currentCamera_51() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentCamera_51)); }
	inline Camera_t4157153871 * get_currentCamera_51() const { return ___currentCamera_51; }
	inline Camera_t4157153871 ** get_address_of_currentCamera_51() { return &___currentCamera_51; }
	inline void set_currentCamera_51(Camera_t4157153871 * value)
	{
		___currentCamera_51 = value;
		Il2CppCodeGenWriteBarrier((&___currentCamera_51), value);
	}

	inline static int32_t get_offset_of_onSchemeChange_52() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onSchemeChange_52)); }
	inline OnSchemeChange_t1701155603 * get_onSchemeChange_52() const { return ___onSchemeChange_52; }
	inline OnSchemeChange_t1701155603 ** get_address_of_onSchemeChange_52() { return &___onSchemeChange_52; }
	inline void set_onSchemeChange_52(OnSchemeChange_t1701155603 * value)
	{
		___onSchemeChange_52 = value;
		Il2CppCodeGenWriteBarrier((&___onSchemeChange_52), value);
	}

	inline static int32_t get_offset_of_mLastScheme_53() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastScheme_53)); }
	inline int32_t get_mLastScheme_53() const { return ___mLastScheme_53; }
	inline int32_t* get_address_of_mLastScheme_53() { return &___mLastScheme_53; }
	inline void set_mLastScheme_53(int32_t value)
	{
		___mLastScheme_53 = value;
	}

	inline static int32_t get_offset_of_currentTouchID_54() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentTouchID_54)); }
	inline int32_t get_currentTouchID_54() const { return ___currentTouchID_54; }
	inline int32_t* get_address_of_currentTouchID_54() { return &___currentTouchID_54; }
	inline void set_currentTouchID_54(int32_t value)
	{
		___currentTouchID_54 = value;
	}

	inline static int32_t get_offset_of_mCurrentKey_55() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mCurrentKey_55)); }
	inline int32_t get_mCurrentKey_55() const { return ___mCurrentKey_55; }
	inline int32_t* get_address_of_mCurrentKey_55() { return &___mCurrentKey_55; }
	inline void set_mCurrentKey_55(int32_t value)
	{
		___mCurrentKey_55 = value;
	}

	inline static int32_t get_offset_of_currentTouch_56() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentTouch_56)); }
	inline MouseOrTouch_t3052596533 * get_currentTouch_56() const { return ___currentTouch_56; }
	inline MouseOrTouch_t3052596533 ** get_address_of_currentTouch_56() { return &___currentTouch_56; }
	inline void set_currentTouch_56(MouseOrTouch_t3052596533 * value)
	{
		___currentTouch_56 = value;
		Il2CppCodeGenWriteBarrier((&___currentTouch_56), value);
	}

	inline static int32_t get_offset_of_mInputFocus_57() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mInputFocus_57)); }
	inline bool get_mInputFocus_57() const { return ___mInputFocus_57; }
	inline bool* get_address_of_mInputFocus_57() { return &___mInputFocus_57; }
	inline void set_mInputFocus_57(bool value)
	{
		___mInputFocus_57 = value;
	}

	inline static int32_t get_offset_of_mGenericHandler_58() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mGenericHandler_58)); }
	inline GameObject_t1113636619 * get_mGenericHandler_58() const { return ___mGenericHandler_58; }
	inline GameObject_t1113636619 ** get_address_of_mGenericHandler_58() { return &___mGenericHandler_58; }
	inline void set_mGenericHandler_58(GameObject_t1113636619 * value)
	{
		___mGenericHandler_58 = value;
		Il2CppCodeGenWriteBarrier((&___mGenericHandler_58), value);
	}

	inline static int32_t get_offset_of_fallThrough_59() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___fallThrough_59)); }
	inline GameObject_t1113636619 * get_fallThrough_59() const { return ___fallThrough_59; }
	inline GameObject_t1113636619 ** get_address_of_fallThrough_59() { return &___fallThrough_59; }
	inline void set_fallThrough_59(GameObject_t1113636619 * value)
	{
		___fallThrough_59 = value;
		Il2CppCodeGenWriteBarrier((&___fallThrough_59), value);
	}

	inline static int32_t get_offset_of_onClick_60() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onClick_60)); }
	inline VoidDelegate_t3100799918 * get_onClick_60() const { return ___onClick_60; }
	inline VoidDelegate_t3100799918 ** get_address_of_onClick_60() { return &___onClick_60; }
	inline void set_onClick_60(VoidDelegate_t3100799918 * value)
	{
		___onClick_60 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_60), value);
	}

	inline static int32_t get_offset_of_onDoubleClick_61() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDoubleClick_61)); }
	inline VoidDelegate_t3100799918 * get_onDoubleClick_61() const { return ___onDoubleClick_61; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDoubleClick_61() { return &___onDoubleClick_61; }
	inline void set_onDoubleClick_61(VoidDelegate_t3100799918 * value)
	{
		___onDoubleClick_61 = value;
		Il2CppCodeGenWriteBarrier((&___onDoubleClick_61), value);
	}

	inline static int32_t get_offset_of_onHover_62() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onHover_62)); }
	inline BoolDelegate_t3825226153 * get_onHover_62() const { return ___onHover_62; }
	inline BoolDelegate_t3825226153 ** get_address_of_onHover_62() { return &___onHover_62; }
	inline void set_onHover_62(BoolDelegate_t3825226153 * value)
	{
		___onHover_62 = value;
		Il2CppCodeGenWriteBarrier((&___onHover_62), value);
	}

	inline static int32_t get_offset_of_onPress_63() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onPress_63)); }
	inline BoolDelegate_t3825226153 * get_onPress_63() const { return ___onPress_63; }
	inline BoolDelegate_t3825226153 ** get_address_of_onPress_63() { return &___onPress_63; }
	inline void set_onPress_63(BoolDelegate_t3825226153 * value)
	{
		___onPress_63 = value;
		Il2CppCodeGenWriteBarrier((&___onPress_63), value);
	}

	inline static int32_t get_offset_of_onSelect_64() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onSelect_64)); }
	inline BoolDelegate_t3825226153 * get_onSelect_64() const { return ___onSelect_64; }
	inline BoolDelegate_t3825226153 ** get_address_of_onSelect_64() { return &___onSelect_64; }
	inline void set_onSelect_64(BoolDelegate_t3825226153 * value)
	{
		___onSelect_64 = value;
		Il2CppCodeGenWriteBarrier((&___onSelect_64), value);
	}

	inline static int32_t get_offset_of_onScroll_65() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onScroll_65)); }
	inline FloatDelegate_t906524069 * get_onScroll_65() const { return ___onScroll_65; }
	inline FloatDelegate_t906524069 ** get_address_of_onScroll_65() { return &___onScroll_65; }
	inline void set_onScroll_65(FloatDelegate_t906524069 * value)
	{
		___onScroll_65 = value;
		Il2CppCodeGenWriteBarrier((&___onScroll_65), value);
	}

	inline static int32_t get_offset_of_onDrag_66() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDrag_66)); }
	inline VectorDelegate_t435795517 * get_onDrag_66() const { return ___onDrag_66; }
	inline VectorDelegate_t435795517 ** get_address_of_onDrag_66() { return &___onDrag_66; }
	inline void set_onDrag_66(VectorDelegate_t435795517 * value)
	{
		___onDrag_66 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_66), value);
	}

	inline static int32_t get_offset_of_onDragStart_67() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragStart_67)); }
	inline VoidDelegate_t3100799918 * get_onDragStart_67() const { return ___onDragStart_67; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDragStart_67() { return &___onDragStart_67; }
	inline void set_onDragStart_67(VoidDelegate_t3100799918 * value)
	{
		___onDragStart_67 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStart_67), value);
	}

	inline static int32_t get_offset_of_onDragOver_68() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragOver_68)); }
	inline ObjectDelegate_t2041570719 * get_onDragOver_68() const { return ___onDragOver_68; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDragOver_68() { return &___onDragOver_68; }
	inline void set_onDragOver_68(ObjectDelegate_t2041570719 * value)
	{
		___onDragOver_68 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOver_68), value);
	}

	inline static int32_t get_offset_of_onDragOut_69() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragOut_69)); }
	inline ObjectDelegate_t2041570719 * get_onDragOut_69() const { return ___onDragOut_69; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDragOut_69() { return &___onDragOut_69; }
	inline void set_onDragOut_69(ObjectDelegate_t2041570719 * value)
	{
		___onDragOut_69 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOut_69), value);
	}

	inline static int32_t get_offset_of_onDragEnd_70() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragEnd_70)); }
	inline VoidDelegate_t3100799918 * get_onDragEnd_70() const { return ___onDragEnd_70; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDragEnd_70() { return &___onDragEnd_70; }
	inline void set_onDragEnd_70(VoidDelegate_t3100799918 * value)
	{
		___onDragEnd_70 = value;
		Il2CppCodeGenWriteBarrier((&___onDragEnd_70), value);
	}

	inline static int32_t get_offset_of_onDrop_71() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDrop_71)); }
	inline ObjectDelegate_t2041570719 * get_onDrop_71() const { return ___onDrop_71; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDrop_71() { return &___onDrop_71; }
	inline void set_onDrop_71(ObjectDelegate_t2041570719 * value)
	{
		___onDrop_71 = value;
		Il2CppCodeGenWriteBarrier((&___onDrop_71), value);
	}

	inline static int32_t get_offset_of_onKey_72() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onKey_72)); }
	inline KeyCodeDelegate_t3064672302 * get_onKey_72() const { return ___onKey_72; }
	inline KeyCodeDelegate_t3064672302 ** get_address_of_onKey_72() { return &___onKey_72; }
	inline void set_onKey_72(KeyCodeDelegate_t3064672302 * value)
	{
		___onKey_72 = value;
		Il2CppCodeGenWriteBarrier((&___onKey_72), value);
	}

	inline static int32_t get_offset_of_onNavigate_73() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onNavigate_73)); }
	inline KeyCodeDelegate_t3064672302 * get_onNavigate_73() const { return ___onNavigate_73; }
	inline KeyCodeDelegate_t3064672302 ** get_address_of_onNavigate_73() { return &___onNavigate_73; }
	inline void set_onNavigate_73(KeyCodeDelegate_t3064672302 * value)
	{
		___onNavigate_73 = value;
		Il2CppCodeGenWriteBarrier((&___onNavigate_73), value);
	}

	inline static int32_t get_offset_of_onPan_74() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onPan_74)); }
	inline VectorDelegate_t435795517 * get_onPan_74() const { return ___onPan_74; }
	inline VectorDelegate_t435795517 ** get_address_of_onPan_74() { return &___onPan_74; }
	inline void set_onPan_74(VectorDelegate_t435795517 * value)
	{
		___onPan_74 = value;
		Il2CppCodeGenWriteBarrier((&___onPan_74), value);
	}

	inline static int32_t get_offset_of_onTooltip_75() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onTooltip_75)); }
	inline BoolDelegate_t3825226153 * get_onTooltip_75() const { return ___onTooltip_75; }
	inline BoolDelegate_t3825226153 ** get_address_of_onTooltip_75() { return &___onTooltip_75; }
	inline void set_onTooltip_75(BoolDelegate_t3825226153 * value)
	{
		___onTooltip_75 = value;
		Il2CppCodeGenWriteBarrier((&___onTooltip_75), value);
	}

	inline static int32_t get_offset_of_onMouseMove_76() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onMouseMove_76)); }
	inline MoveDelegate_t16019400 * get_onMouseMove_76() const { return ___onMouseMove_76; }
	inline MoveDelegate_t16019400 ** get_address_of_onMouseMove_76() { return &___onMouseMove_76; }
	inline void set_onMouseMove_76(MoveDelegate_t16019400 * value)
	{
		___onMouseMove_76 = value;
		Il2CppCodeGenWriteBarrier((&___onMouseMove_76), value);
	}

	inline static int32_t get_offset_of_mMouse_77() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mMouse_77)); }
	inline MouseOrTouchU5BU5D_t3534648920* get_mMouse_77() const { return ___mMouse_77; }
	inline MouseOrTouchU5BU5D_t3534648920** get_address_of_mMouse_77() { return &___mMouse_77; }
	inline void set_mMouse_77(MouseOrTouchU5BU5D_t3534648920* value)
	{
		___mMouse_77 = value;
		Il2CppCodeGenWriteBarrier((&___mMouse_77), value);
	}

	inline static int32_t get_offset_of_controller_78() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___controller_78)); }
	inline MouseOrTouch_t3052596533 * get_controller_78() const { return ___controller_78; }
	inline MouseOrTouch_t3052596533 ** get_address_of_controller_78() { return &___controller_78; }
	inline void set_controller_78(MouseOrTouch_t3052596533 * value)
	{
		___controller_78 = value;
		Il2CppCodeGenWriteBarrier((&___controller_78), value);
	}

	inline static int32_t get_offset_of_activeTouches_79() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___activeTouches_79)); }
	inline List_1_t229703979 * get_activeTouches_79() const { return ___activeTouches_79; }
	inline List_1_t229703979 ** get_address_of_activeTouches_79() { return &___activeTouches_79; }
	inline void set_activeTouches_79(List_1_t229703979 * value)
	{
		___activeTouches_79 = value;
		Il2CppCodeGenWriteBarrier((&___activeTouches_79), value);
	}

	inline static int32_t get_offset_of_mTouchIDs_80() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTouchIDs_80)); }
	inline List_1_t128053199 * get_mTouchIDs_80() const { return ___mTouchIDs_80; }
	inline List_1_t128053199 ** get_address_of_mTouchIDs_80() { return &___mTouchIDs_80; }
	inline void set_mTouchIDs_80(List_1_t128053199 * value)
	{
		___mTouchIDs_80 = value;
		Il2CppCodeGenWriteBarrier((&___mTouchIDs_80), value);
	}

	inline static int32_t get_offset_of_mWidth_81() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mWidth_81)); }
	inline int32_t get_mWidth_81() const { return ___mWidth_81; }
	inline int32_t* get_address_of_mWidth_81() { return &___mWidth_81; }
	inline void set_mWidth_81(int32_t value)
	{
		___mWidth_81 = value;
	}

	inline static int32_t get_offset_of_mHeight_82() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHeight_82)); }
	inline int32_t get_mHeight_82() const { return ___mHeight_82; }
	inline int32_t* get_address_of_mHeight_82() { return &___mHeight_82; }
	inline void set_mHeight_82(int32_t value)
	{
		___mHeight_82 = value;
	}

	inline static int32_t get_offset_of_mTooltip_83() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTooltip_83)); }
	inline GameObject_t1113636619 * get_mTooltip_83() const { return ___mTooltip_83; }
	inline GameObject_t1113636619 ** get_address_of_mTooltip_83() { return &___mTooltip_83; }
	inline void set_mTooltip_83(GameObject_t1113636619 * value)
	{
		___mTooltip_83 = value;
		Il2CppCodeGenWriteBarrier((&___mTooltip_83), value);
	}

	inline static int32_t get_offset_of_mTooltipTime_85() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTooltipTime_85)); }
	inline float get_mTooltipTime_85() const { return ___mTooltipTime_85; }
	inline float* get_address_of_mTooltipTime_85() { return &___mTooltipTime_85; }
	inline void set_mTooltipTime_85(float value)
	{
		___mTooltipTime_85 = value;
	}

	inline static int32_t get_offset_of_isDragging_87() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___isDragging_87)); }
	inline bool get_isDragging_87() const { return ___isDragging_87; }
	inline bool* get_address_of_isDragging_87() { return &___isDragging_87; }
	inline void set_isDragging_87(bool value)
	{
		___isDragging_87 = value;
	}

	inline static int32_t get_offset_of_mLastInteractionCheck_88() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastInteractionCheck_88)); }
	inline int32_t get_mLastInteractionCheck_88() const { return ___mLastInteractionCheck_88; }
	inline int32_t* get_address_of_mLastInteractionCheck_88() { return &___mLastInteractionCheck_88; }
	inline void set_mLastInteractionCheck_88(int32_t value)
	{
		___mLastInteractionCheck_88 = value;
	}

	inline static int32_t get_offset_of_mLastInteractionResult_89() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastInteractionResult_89)); }
	inline bool get_mLastInteractionResult_89() const { return ___mLastInteractionResult_89; }
	inline bool* get_address_of_mLastInteractionResult_89() { return &___mLastInteractionResult_89; }
	inline void set_mLastInteractionResult_89(bool value)
	{
		___mLastInteractionResult_89 = value;
	}

	inline static int32_t get_offset_of_mLastFocusCheck_90() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastFocusCheck_90)); }
	inline int32_t get_mLastFocusCheck_90() const { return ___mLastFocusCheck_90; }
	inline int32_t* get_address_of_mLastFocusCheck_90() { return &___mLastFocusCheck_90; }
	inline void set_mLastFocusCheck_90(int32_t value)
	{
		___mLastFocusCheck_90 = value;
	}

	inline static int32_t get_offset_of_mLastFocusResult_91() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastFocusResult_91)); }
	inline bool get_mLastFocusResult_91() const { return ___mLastFocusResult_91; }
	inline bool* get_address_of_mLastFocusResult_91() { return &___mLastFocusResult_91; }
	inline void set_mLastFocusResult_91(bool value)
	{
		___mLastFocusResult_91 = value;
	}

	inline static int32_t get_offset_of_mLastOverCheck_92() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastOverCheck_92)); }
	inline int32_t get_mLastOverCheck_92() const { return ___mLastOverCheck_92; }
	inline int32_t* get_address_of_mLastOverCheck_92() { return &___mLastOverCheck_92; }
	inline void set_mLastOverCheck_92(int32_t value)
	{
		___mLastOverCheck_92 = value;
	}

	inline static int32_t get_offset_of_mLastOverResult_93() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastOverResult_93)); }
	inline bool get_mLastOverResult_93() const { return ___mLastOverResult_93; }
	inline bool* get_address_of_mLastOverResult_93() { return &___mLastOverResult_93; }
	inline void set_mLastOverResult_93(bool value)
	{
		___mLastOverResult_93 = value;
	}

	inline static int32_t get_offset_of_mRayHitObject_94() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mRayHitObject_94)); }
	inline GameObject_t1113636619 * get_mRayHitObject_94() const { return ___mRayHitObject_94; }
	inline GameObject_t1113636619 ** get_address_of_mRayHitObject_94() { return &___mRayHitObject_94; }
	inline void set_mRayHitObject_94(GameObject_t1113636619 * value)
	{
		___mRayHitObject_94 = value;
		Il2CppCodeGenWriteBarrier((&___mRayHitObject_94), value);
	}

	inline static int32_t get_offset_of_mHover_95() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHover_95)); }
	inline GameObject_t1113636619 * get_mHover_95() const { return ___mHover_95; }
	inline GameObject_t1113636619 ** get_address_of_mHover_95() { return &___mHover_95; }
	inline void set_mHover_95(GameObject_t1113636619 * value)
	{
		___mHover_95 = value;
		Il2CppCodeGenWriteBarrier((&___mHover_95), value);
	}

	inline static int32_t get_offset_of_mSelected_96() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mSelected_96)); }
	inline GameObject_t1113636619 * get_mSelected_96() const { return ___mSelected_96; }
	inline GameObject_t1113636619 ** get_address_of_mSelected_96() { return &___mSelected_96; }
	inline void set_mSelected_96(GameObject_t1113636619 * value)
	{
		___mSelected_96 = value;
		Il2CppCodeGenWriteBarrier((&___mSelected_96), value);
	}

	inline static int32_t get_offset_of_mHit_97() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHit_97)); }
	inline DepthEntry_t628749918  get_mHit_97() const { return ___mHit_97; }
	inline DepthEntry_t628749918 * get_address_of_mHit_97() { return &___mHit_97; }
	inline void set_mHit_97(DepthEntry_t628749918  value)
	{
		___mHit_97 = value;
	}

	inline static int32_t get_offset_of_mHits_98() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHits_98)); }
	inline BetterList_1_t4078737532 * get_mHits_98() const { return ___mHits_98; }
	inline BetterList_1_t4078737532 ** get_address_of_mHits_98() { return &___mHits_98; }
	inline void set_mHits_98(BetterList_1_t4078737532 * value)
	{
		___mHits_98 = value;
		Il2CppCodeGenWriteBarrier((&___mHits_98), value);
	}

	inline static int32_t get_offset_of_mRayHits_99() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mRayHits_99)); }
	inline RaycastHitU5BU5D_t1690781147* get_mRayHits_99() const { return ___mRayHits_99; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_mRayHits_99() { return &___mRayHits_99; }
	inline void set_mRayHits_99(RaycastHitU5BU5D_t1690781147* value)
	{
		___mRayHits_99 = value;
		Il2CppCodeGenWriteBarrier((&___mRayHits_99), value);
	}

	inline static int32_t get_offset_of_mOverlap_100() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mOverlap_100)); }
	inline Collider2DU5BU5D_t1693969295* get_mOverlap_100() const { return ___mOverlap_100; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_mOverlap_100() { return &___mOverlap_100; }
	inline void set_mOverlap_100(Collider2DU5BU5D_t1693969295* value)
	{
		___mOverlap_100 = value;
		Il2CppCodeGenWriteBarrier((&___mOverlap_100), value);
	}

	inline static int32_t get_offset_of_m2DPlane_101() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___m2DPlane_101)); }
	inline Plane_t1000493321  get_m2DPlane_101() const { return ___m2DPlane_101; }
	inline Plane_t1000493321 * get_address_of_m2DPlane_101() { return &___m2DPlane_101; }
	inline void set_m2DPlane_101(Plane_t1000493321  value)
	{
		___m2DPlane_101 = value;
	}

	inline static int32_t get_offset_of_mNextEvent_102() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mNextEvent_102)); }
	inline float get_mNextEvent_102() const { return ___mNextEvent_102; }
	inline float* get_address_of_mNextEvent_102() { return &___mNextEvent_102; }
	inline void set_mNextEvent_102(float value)
	{
		___mNextEvent_102 = value;
	}

	inline static int32_t get_offset_of_mNotifying_103() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mNotifying_103)); }
	inline int32_t get_mNotifying_103() const { return ___mNotifying_103; }
	inline int32_t* get_address_of_mNotifying_103() { return &___mNotifying_103; }
	inline void set_mNotifying_103(int32_t value)
	{
		___mNotifying_103 = value;
	}

	inline static int32_t get_offset_of_mUsingTouchEvents_104() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mUsingTouchEvents_104)); }
	inline bool get_mUsingTouchEvents_104() const { return ___mUsingTouchEvents_104; }
	inline bool* get_address_of_mUsingTouchEvents_104() { return &___mUsingTouchEvents_104; }
	inline void set_mUsingTouchEvents_104(bool value)
	{
		___mUsingTouchEvents_104 = value;
	}

	inline static int32_t get_offset_of_GetInputTouchCount_105() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetInputTouchCount_105)); }
	inline GetTouchCountCallback_t3185863032 * get_GetInputTouchCount_105() const { return ___GetInputTouchCount_105; }
	inline GetTouchCountCallback_t3185863032 ** get_address_of_GetInputTouchCount_105() { return &___GetInputTouchCount_105; }
	inline void set_GetInputTouchCount_105(GetTouchCountCallback_t3185863032 * value)
	{
		___GetInputTouchCount_105 = value;
		Il2CppCodeGenWriteBarrier((&___GetInputTouchCount_105), value);
	}

	inline static int32_t get_offset_of_GetInputTouch_106() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetInputTouch_106)); }
	inline GetTouchCallback_t97678626 * get_GetInputTouch_106() const { return ___GetInputTouch_106; }
	inline GetTouchCallback_t97678626 ** get_address_of_GetInputTouch_106() { return &___GetInputTouch_106; }
	inline void set_GetInputTouch_106(GetTouchCallback_t97678626 * value)
	{
		___GetInputTouch_106 = value;
		Il2CppCodeGenWriteBarrier((&___GetInputTouch_106), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_107() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__amU24cache0_107)); }
	inline CompareFunc_t2967399990 * get_U3CU3Ef__amU24cache0_107() const { return ___U3CU3Ef__amU24cache0_107; }
	inline CompareFunc_t2967399990 ** get_address_of_U3CU3Ef__amU24cache0_107() { return &___U3CU3Ef__amU24cache0_107; }
	inline void set_U3CU3Ef__amU24cache0_107(CompareFunc_t2967399990 * value)
	{
		___U3CU3Ef__amU24cache0_107 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_107), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_108() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__amU24cache1_108)); }
	inline CompareFunc_t2967399990 * get_U3CU3Ef__amU24cache1_108() const { return ___U3CU3Ef__amU24cache1_108; }
	inline CompareFunc_t2967399990 ** get_address_of_U3CU3Ef__amU24cache1_108() { return &___U3CU3Ef__amU24cache1_108; }
	inline void set_U3CU3Ef__amU24cache1_108(CompareFunc_t2967399990 * value)
	{
		___U3CU3Ef__amU24cache1_108 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_108), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_109() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache0_109)); }
	inline CompareFunc_t3695088943 * get_U3CU3Ef__mgU24cache0_109() const { return ___U3CU3Ef__mgU24cache0_109; }
	inline CompareFunc_t3695088943 ** get_address_of_U3CU3Ef__mgU24cache0_109() { return &___U3CU3Ef__mgU24cache0_109; }
	inline void set_U3CU3Ef__mgU24cache0_109(CompareFunc_t3695088943 * value)
	{
		___U3CU3Ef__mgU24cache0_109 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_109), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_110() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache1_110)); }
	inline CompareFunc_t3695088943 * get_U3CU3Ef__mgU24cache1_110() const { return ___U3CU3Ef__mgU24cache1_110; }
	inline CompareFunc_t3695088943 ** get_address_of_U3CU3Ef__mgU24cache1_110() { return &___U3CU3Ef__mgU24cache1_110; }
	inline void set_U3CU3Ef__mgU24cache1_110(CompareFunc_t3695088943 * value)
	{
		___U3CU3Ef__mgU24cache1_110 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_110), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICAMERA_T1356438871_H
#ifndef ACTIVEANIMATION_T3475256642_H
#define ACTIVEANIMATION_T3475256642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveAnimation
struct  ActiveAnimation_t3475256642  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<EventDelegate> ActiveAnimation::onFinished
	List_1_t4210400802 * ___onFinished_3;
	// UnityEngine.GameObject ActiveAnimation::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_4;
	// System.String ActiveAnimation::callWhenFinished
	String_t* ___callWhenFinished_5;
	// UnityEngine.Animation ActiveAnimation::mAnim
	Animation_t3648466861 * ___mAnim_6;
	// AnimationOrTween.Direction ActiveAnimation::mLastDirection
	int32_t ___mLastDirection_7;
	// AnimationOrTween.Direction ActiveAnimation::mDisableDirection
	int32_t ___mDisableDirection_8;
	// System.Boolean ActiveAnimation::mNotify
	bool ___mNotify_9;
	// UnityEngine.Animator ActiveAnimation::mAnimator
	Animator_t434523843 * ___mAnimator_10;
	// System.String ActiveAnimation::mClip
	String_t* ___mClip_11;

public:
	inline static int32_t get_offset_of_onFinished_3() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___onFinished_3)); }
	inline List_1_t4210400802 * get_onFinished_3() const { return ___onFinished_3; }
	inline List_1_t4210400802 ** get_address_of_onFinished_3() { return &___onFinished_3; }
	inline void set_onFinished_3(List_1_t4210400802 * value)
	{
		___onFinished_3 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_3), value);
	}

	inline static int32_t get_offset_of_eventReceiver_4() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___eventReceiver_4)); }
	inline GameObject_t1113636619 * get_eventReceiver_4() const { return ___eventReceiver_4; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_4() { return &___eventReceiver_4; }
	inline void set_eventReceiver_4(GameObject_t1113636619 * value)
	{
		___eventReceiver_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_4), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_5() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___callWhenFinished_5)); }
	inline String_t* get_callWhenFinished_5() const { return ___callWhenFinished_5; }
	inline String_t** get_address_of_callWhenFinished_5() { return &___callWhenFinished_5; }
	inline void set_callWhenFinished_5(String_t* value)
	{
		___callWhenFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_5), value);
	}

	inline static int32_t get_offset_of_mAnim_6() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mAnim_6)); }
	inline Animation_t3648466861 * get_mAnim_6() const { return ___mAnim_6; }
	inline Animation_t3648466861 ** get_address_of_mAnim_6() { return &___mAnim_6; }
	inline void set_mAnim_6(Animation_t3648466861 * value)
	{
		___mAnim_6 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_6), value);
	}

	inline static int32_t get_offset_of_mLastDirection_7() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mLastDirection_7)); }
	inline int32_t get_mLastDirection_7() const { return ___mLastDirection_7; }
	inline int32_t* get_address_of_mLastDirection_7() { return &___mLastDirection_7; }
	inline void set_mLastDirection_7(int32_t value)
	{
		___mLastDirection_7 = value;
	}

	inline static int32_t get_offset_of_mDisableDirection_8() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mDisableDirection_8)); }
	inline int32_t get_mDisableDirection_8() const { return ___mDisableDirection_8; }
	inline int32_t* get_address_of_mDisableDirection_8() { return &___mDisableDirection_8; }
	inline void set_mDisableDirection_8(int32_t value)
	{
		___mDisableDirection_8 = value;
	}

	inline static int32_t get_offset_of_mNotify_9() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mNotify_9)); }
	inline bool get_mNotify_9() const { return ___mNotify_9; }
	inline bool* get_address_of_mNotify_9() { return &___mNotify_9; }
	inline void set_mNotify_9(bool value)
	{
		___mNotify_9 = value;
	}

	inline static int32_t get_offset_of_mAnimator_10() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mAnimator_10)); }
	inline Animator_t434523843 * get_mAnimator_10() const { return ___mAnimator_10; }
	inline Animator_t434523843 ** get_address_of_mAnimator_10() { return &___mAnimator_10; }
	inline void set_mAnimator_10(Animator_t434523843 * value)
	{
		___mAnimator_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAnimator_10), value);
	}

	inline static int32_t get_offset_of_mClip_11() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642, ___mClip_11)); }
	inline String_t* get_mClip_11() const { return ___mClip_11; }
	inline String_t** get_address_of_mClip_11() { return &___mClip_11; }
	inline void set_mClip_11(String_t* value)
	{
		___mClip_11 = value;
		Il2CppCodeGenWriteBarrier((&___mClip_11), value);
	}
};

struct ActiveAnimation_t3475256642_StaticFields
{
public:
	// ActiveAnimation ActiveAnimation::current
	ActiveAnimation_t3475256642 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(ActiveAnimation_t3475256642_StaticFields, ___current_2)); }
	inline ActiveAnimation_t3475256642 * get_current_2() const { return ___current_2; }
	inline ActiveAnimation_t3475256642 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(ActiveAnimation_t3475256642 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEANIMATION_T3475256642_H
#ifndef UITWEENER_T260334902_H
#define UITWEENER_T260334902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener
struct  UITweener_t260334902  : public MonoBehaviour_t3962482529
{
public:
	// UITweener/Method UITweener::method
	int32_t ___method_3;
	// UITweener/Style UITweener::style
	int32_t ___style_4;
	// UnityEngine.AnimationCurve UITweener::animationCurve
	AnimationCurve_t3046754366 * ___animationCurve_5;
	// System.Boolean UITweener::ignoreTimeScale
	bool ___ignoreTimeScale_6;
	// System.Single UITweener::delay
	float ___delay_7;
	// System.Single UITweener::duration
	float ___duration_8;
	// System.Boolean UITweener::steeperCurves
	bool ___steeperCurves_9;
	// System.Int32 UITweener::tweenGroup
	int32_t ___tweenGroup_10;
	// System.Boolean UITweener::useFixedUpdate
	bool ___useFixedUpdate_11;
	// System.Collections.Generic.List`1<EventDelegate> UITweener::onFinished
	List_1_t4210400802 * ___onFinished_12;
	// UnityEngine.GameObject UITweener::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_13;
	// System.String UITweener::callWhenFinished
	String_t* ___callWhenFinished_14;
	// System.Single UITweener::timeScale
	float ___timeScale_15;
	// System.Boolean UITweener::mStarted
	bool ___mStarted_16;
	// System.Single UITweener::mStartTime
	float ___mStartTime_17;
	// System.Single UITweener::mDuration
	float ___mDuration_18;
	// System.Single UITweener::mAmountPerDelta
	float ___mAmountPerDelta_19;
	// System.Single UITweener::mFactor
	float ___mFactor_20;
	// System.Collections.Generic.List`1<EventDelegate> UITweener::mTemp
	List_1_t4210400802 * ___mTemp_21;

public:
	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___method_3)); }
	inline int32_t get_method_3() const { return ___method_3; }
	inline int32_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(int32_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_style_4() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___style_4)); }
	inline int32_t get_style_4() const { return ___style_4; }
	inline int32_t* get_address_of_style_4() { return &___style_4; }
	inline void set_style_4(int32_t value)
	{
		___style_4 = value;
	}

	inline static int32_t get_offset_of_animationCurve_5() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___animationCurve_5)); }
	inline AnimationCurve_t3046754366 * get_animationCurve_5() const { return ___animationCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_animationCurve_5() { return &___animationCurve_5; }
	inline void set_animationCurve_5(AnimationCurve_t3046754366 * value)
	{
		___animationCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___animationCurve_5), value);
	}

	inline static int32_t get_offset_of_ignoreTimeScale_6() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___ignoreTimeScale_6)); }
	inline bool get_ignoreTimeScale_6() const { return ___ignoreTimeScale_6; }
	inline bool* get_address_of_ignoreTimeScale_6() { return &___ignoreTimeScale_6; }
	inline void set_ignoreTimeScale_6(bool value)
	{
		___ignoreTimeScale_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_duration_8() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___duration_8)); }
	inline float get_duration_8() const { return ___duration_8; }
	inline float* get_address_of_duration_8() { return &___duration_8; }
	inline void set_duration_8(float value)
	{
		___duration_8 = value;
	}

	inline static int32_t get_offset_of_steeperCurves_9() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___steeperCurves_9)); }
	inline bool get_steeperCurves_9() const { return ___steeperCurves_9; }
	inline bool* get_address_of_steeperCurves_9() { return &___steeperCurves_9; }
	inline void set_steeperCurves_9(bool value)
	{
		___steeperCurves_9 = value;
	}

	inline static int32_t get_offset_of_tweenGroup_10() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___tweenGroup_10)); }
	inline int32_t get_tweenGroup_10() const { return ___tweenGroup_10; }
	inline int32_t* get_address_of_tweenGroup_10() { return &___tweenGroup_10; }
	inline void set_tweenGroup_10(int32_t value)
	{
		___tweenGroup_10 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_11() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___useFixedUpdate_11)); }
	inline bool get_useFixedUpdate_11() const { return ___useFixedUpdate_11; }
	inline bool* get_address_of_useFixedUpdate_11() { return &___useFixedUpdate_11; }
	inline void set_useFixedUpdate_11(bool value)
	{
		___useFixedUpdate_11 = value;
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___onFinished_12)); }
	inline List_1_t4210400802 * get_onFinished_12() const { return ___onFinished_12; }
	inline List_1_t4210400802 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(List_1_t4210400802 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_eventReceiver_13() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___eventReceiver_13)); }
	inline GameObject_t1113636619 * get_eventReceiver_13() const { return ___eventReceiver_13; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_13() { return &___eventReceiver_13; }
	inline void set_eventReceiver_13(GameObject_t1113636619 * value)
	{
		___eventReceiver_13 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_13), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_14() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___callWhenFinished_14)); }
	inline String_t* get_callWhenFinished_14() const { return ___callWhenFinished_14; }
	inline String_t** get_address_of_callWhenFinished_14() { return &___callWhenFinished_14; }
	inline void set_callWhenFinished_14(String_t* value)
	{
		___callWhenFinished_14 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_14), value);
	}

	inline static int32_t get_offset_of_timeScale_15() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___timeScale_15)); }
	inline float get_timeScale_15() const { return ___timeScale_15; }
	inline float* get_address_of_timeScale_15() { return &___timeScale_15; }
	inline void set_timeScale_15(float value)
	{
		___timeScale_15 = value;
	}

	inline static int32_t get_offset_of_mStarted_16() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mStarted_16)); }
	inline bool get_mStarted_16() const { return ___mStarted_16; }
	inline bool* get_address_of_mStarted_16() { return &___mStarted_16; }
	inline void set_mStarted_16(bool value)
	{
		___mStarted_16 = value;
	}

	inline static int32_t get_offset_of_mStartTime_17() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mStartTime_17)); }
	inline float get_mStartTime_17() const { return ___mStartTime_17; }
	inline float* get_address_of_mStartTime_17() { return &___mStartTime_17; }
	inline void set_mStartTime_17(float value)
	{
		___mStartTime_17 = value;
	}

	inline static int32_t get_offset_of_mDuration_18() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mDuration_18)); }
	inline float get_mDuration_18() const { return ___mDuration_18; }
	inline float* get_address_of_mDuration_18() { return &___mDuration_18; }
	inline void set_mDuration_18(float value)
	{
		___mDuration_18 = value;
	}

	inline static int32_t get_offset_of_mAmountPerDelta_19() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mAmountPerDelta_19)); }
	inline float get_mAmountPerDelta_19() const { return ___mAmountPerDelta_19; }
	inline float* get_address_of_mAmountPerDelta_19() { return &___mAmountPerDelta_19; }
	inline void set_mAmountPerDelta_19(float value)
	{
		___mAmountPerDelta_19 = value;
	}

	inline static int32_t get_offset_of_mFactor_20() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mFactor_20)); }
	inline float get_mFactor_20() const { return ___mFactor_20; }
	inline float* get_address_of_mFactor_20() { return &___mFactor_20; }
	inline void set_mFactor_20(float value)
	{
		___mFactor_20 = value;
	}

	inline static int32_t get_offset_of_mTemp_21() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mTemp_21)); }
	inline List_1_t4210400802 * get_mTemp_21() const { return ___mTemp_21; }
	inline List_1_t4210400802 ** get_address_of_mTemp_21() { return &___mTemp_21; }
	inline void set_mTemp_21(List_1_t4210400802 * value)
	{
		___mTemp_21 = value;
		Il2CppCodeGenWriteBarrier((&___mTemp_21), value);
	}
};

struct UITweener_t260334902_StaticFields
{
public:
	// UITweener UITweener::current
	UITweener_t260334902 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UITweener_t260334902_StaticFields, ___current_2)); }
	inline UITweener_t260334902 * get_current_2() const { return ___current_2; }
	inline UITweener_t260334902 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UITweener_t260334902 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITWEENER_T260334902_H
#ifndef UITOGGLEDCOMPONENTS_T772955118_H
#define UITOGGLEDCOMPONENTS_T772955118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggledComponents
struct  UIToggledComponents_t772955118  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour> UIToggledComponents::activate
	List_1_t1139589975 * ___activate_2;
	// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour> UIToggledComponents::deactivate
	List_1_t1139589975 * ___deactivate_3;
	// UnityEngine.MonoBehaviour UIToggledComponents::target
	MonoBehaviour_t3962482529 * ___target_4;
	// System.Boolean UIToggledComponents::inverse
	bool ___inverse_5;

public:
	inline static int32_t get_offset_of_activate_2() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___activate_2)); }
	inline List_1_t1139589975 * get_activate_2() const { return ___activate_2; }
	inline List_1_t1139589975 ** get_address_of_activate_2() { return &___activate_2; }
	inline void set_activate_2(List_1_t1139589975 * value)
	{
		___activate_2 = value;
		Il2CppCodeGenWriteBarrier((&___activate_2), value);
	}

	inline static int32_t get_offset_of_deactivate_3() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___deactivate_3)); }
	inline List_1_t1139589975 * get_deactivate_3() const { return ___deactivate_3; }
	inline List_1_t1139589975 ** get_address_of_deactivate_3() { return &___deactivate_3; }
	inline void set_deactivate_3(List_1_t1139589975 * value)
	{
		___deactivate_3 = value;
		Il2CppCodeGenWriteBarrier((&___deactivate_3), value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___target_4)); }
	inline MonoBehaviour_t3962482529 * get_target_4() const { return ___target_4; }
	inline MonoBehaviour_t3962482529 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(MonoBehaviour_t3962482529 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_inverse_5() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___inverse_5)); }
	inline bool get_inverse_5() const { return ___inverse_5; }
	inline bool* get_address_of_inverse_5() { return &___inverse_5; }
	inline void set_inverse_5(bool value)
	{
		___inverse_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEDCOMPONENTS_T772955118_H
#ifndef UITOGGLEDOBJECTS_T3502557910_H
#define UITOGGLEDOBJECTS_T3502557910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggledObjects
struct  UIToggledObjects_t3502557910  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UIToggledObjects::activate
	List_1_t2585711361 * ___activate_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UIToggledObjects::deactivate
	List_1_t2585711361 * ___deactivate_3;
	// UnityEngine.GameObject UIToggledObjects::target
	GameObject_t1113636619 * ___target_4;
	// System.Boolean UIToggledObjects::inverse
	bool ___inverse_5;

public:
	inline static int32_t get_offset_of_activate_2() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___activate_2)); }
	inline List_1_t2585711361 * get_activate_2() const { return ___activate_2; }
	inline List_1_t2585711361 ** get_address_of_activate_2() { return &___activate_2; }
	inline void set_activate_2(List_1_t2585711361 * value)
	{
		___activate_2 = value;
		Il2CppCodeGenWriteBarrier((&___activate_2), value);
	}

	inline static int32_t get_offset_of_deactivate_3() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___deactivate_3)); }
	inline List_1_t2585711361 * get_deactivate_3() const { return ___deactivate_3; }
	inline List_1_t2585711361 ** get_address_of_deactivate_3() { return &___deactivate_3; }
	inline void set_deactivate_3(List_1_t2585711361 * value)
	{
		___deactivate_3 = value;
		Il2CppCodeGenWriteBarrier((&___deactivate_3), value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___target_4)); }
	inline GameObject_t1113636619 * get_target_4() const { return ___target_4; }
	inline GameObject_t1113636619 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_t1113636619 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_inverse_5() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___inverse_5)); }
	inline bool get_inverse_5() const { return ___inverse_5; }
	inline bool* get_address_of_inverse_5() { return &___inverse_5; }
	inline void set_inverse_5(bool value)
	{
		___inverse_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEDOBJECTS_T3502557910_H
#ifndef UITOOLTIP_T30236576_H
#define UITOOLTIP_T30236576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITooltip
struct  UITooltip_t30236576  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UITooltip::uiCamera
	Camera_t4157153871 * ___uiCamera_3;
	// UILabel UITooltip::text
	UILabel_t3248798549 * ___text_4;
	// UnityEngine.GameObject UITooltip::tooltipRoot
	GameObject_t1113636619 * ___tooltipRoot_5;
	// UISprite UITooltip::background
	UISprite_t194114938 * ___background_6;
	// System.Single UITooltip::appearSpeed
	float ___appearSpeed_7;
	// System.Boolean UITooltip::scalingTransitions
	bool ___scalingTransitions_8;
	// UnityEngine.GameObject UITooltip::mTooltip
	GameObject_t1113636619 * ___mTooltip_9;
	// UnityEngine.Transform UITooltip::mTrans
	Transform_t3600365921 * ___mTrans_10;
	// System.Single UITooltip::mTarget
	float ___mTarget_11;
	// System.Single UITooltip::mCurrent
	float ___mCurrent_12;
	// UnityEngine.Vector3 UITooltip::mPos
	Vector3_t3722313464  ___mPos_13;
	// UnityEngine.Vector3 UITooltip::mSize
	Vector3_t3722313464  ___mSize_14;
	// UIWidget[] UITooltip::mWidgets
	UIWidgetU5BU5D_t2950248968* ___mWidgets_15;

public:
	inline static int32_t get_offset_of_uiCamera_3() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___uiCamera_3)); }
	inline Camera_t4157153871 * get_uiCamera_3() const { return ___uiCamera_3; }
	inline Camera_t4157153871 ** get_address_of_uiCamera_3() { return &___uiCamera_3; }
	inline void set_uiCamera_3(Camera_t4157153871 * value)
	{
		___uiCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___text_4)); }
	inline UILabel_t3248798549 * get_text_4() const { return ___text_4; }
	inline UILabel_t3248798549 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(UILabel_t3248798549 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_tooltipRoot_5() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___tooltipRoot_5)); }
	inline GameObject_t1113636619 * get_tooltipRoot_5() const { return ___tooltipRoot_5; }
	inline GameObject_t1113636619 ** get_address_of_tooltipRoot_5() { return &___tooltipRoot_5; }
	inline void set_tooltipRoot_5(GameObject_t1113636619 * value)
	{
		___tooltipRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___tooltipRoot_5), value);
	}

	inline static int32_t get_offset_of_background_6() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___background_6)); }
	inline UISprite_t194114938 * get_background_6() const { return ___background_6; }
	inline UISprite_t194114938 ** get_address_of_background_6() { return &___background_6; }
	inline void set_background_6(UISprite_t194114938 * value)
	{
		___background_6 = value;
		Il2CppCodeGenWriteBarrier((&___background_6), value);
	}

	inline static int32_t get_offset_of_appearSpeed_7() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___appearSpeed_7)); }
	inline float get_appearSpeed_7() const { return ___appearSpeed_7; }
	inline float* get_address_of_appearSpeed_7() { return &___appearSpeed_7; }
	inline void set_appearSpeed_7(float value)
	{
		___appearSpeed_7 = value;
	}

	inline static int32_t get_offset_of_scalingTransitions_8() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___scalingTransitions_8)); }
	inline bool get_scalingTransitions_8() const { return ___scalingTransitions_8; }
	inline bool* get_address_of_scalingTransitions_8() { return &___scalingTransitions_8; }
	inline void set_scalingTransitions_8(bool value)
	{
		___scalingTransitions_8 = value;
	}

	inline static int32_t get_offset_of_mTooltip_9() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mTooltip_9)); }
	inline GameObject_t1113636619 * get_mTooltip_9() const { return ___mTooltip_9; }
	inline GameObject_t1113636619 ** get_address_of_mTooltip_9() { return &___mTooltip_9; }
	inline void set_mTooltip_9(GameObject_t1113636619 * value)
	{
		___mTooltip_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTooltip_9), value);
	}

	inline static int32_t get_offset_of_mTrans_10() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mTrans_10)); }
	inline Transform_t3600365921 * get_mTrans_10() const { return ___mTrans_10; }
	inline Transform_t3600365921 ** get_address_of_mTrans_10() { return &___mTrans_10; }
	inline void set_mTrans_10(Transform_t3600365921 * value)
	{
		___mTrans_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_10), value);
	}

	inline static int32_t get_offset_of_mTarget_11() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mTarget_11)); }
	inline float get_mTarget_11() const { return ___mTarget_11; }
	inline float* get_address_of_mTarget_11() { return &___mTarget_11; }
	inline void set_mTarget_11(float value)
	{
		___mTarget_11 = value;
	}

	inline static int32_t get_offset_of_mCurrent_12() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mCurrent_12)); }
	inline float get_mCurrent_12() const { return ___mCurrent_12; }
	inline float* get_address_of_mCurrent_12() { return &___mCurrent_12; }
	inline void set_mCurrent_12(float value)
	{
		___mCurrent_12 = value;
	}

	inline static int32_t get_offset_of_mPos_13() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mPos_13)); }
	inline Vector3_t3722313464  get_mPos_13() const { return ___mPos_13; }
	inline Vector3_t3722313464 * get_address_of_mPos_13() { return &___mPos_13; }
	inline void set_mPos_13(Vector3_t3722313464  value)
	{
		___mPos_13 = value;
	}

	inline static int32_t get_offset_of_mSize_14() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mSize_14)); }
	inline Vector3_t3722313464  get_mSize_14() const { return ___mSize_14; }
	inline Vector3_t3722313464 * get_address_of_mSize_14() { return &___mSize_14; }
	inline void set_mSize_14(Vector3_t3722313464  value)
	{
		___mSize_14 = value;
	}

	inline static int32_t get_offset_of_mWidgets_15() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mWidgets_15)); }
	inline UIWidgetU5BU5D_t2950248968* get_mWidgets_15() const { return ___mWidgets_15; }
	inline UIWidgetU5BU5D_t2950248968** get_address_of_mWidgets_15() { return &___mWidgets_15; }
	inline void set_mWidgets_15(UIWidgetU5BU5D_t2950248968* value)
	{
		___mWidgets_15 = value;
		Il2CppCodeGenWriteBarrier((&___mWidgets_15), value);
	}
};

struct UITooltip_t30236576_StaticFields
{
public:
	// UITooltip UITooltip::mInstance
	UITooltip_t30236576 * ___mInstance_2;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(UITooltip_t30236576_StaticFields, ___mInstance_2)); }
	inline UITooltip_t30236576 * get_mInstance_2() const { return ___mInstance_2; }
	inline UITooltip_t30236576 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(UITooltip_t30236576 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOOLTIP_T30236576_H
#ifndef VELOCITYTMP_T3274308841_H
#define VELOCITYTMP_T3274308841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VelocityTmp
struct  VelocityTmp_t3274308841  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 VelocityTmp::_angularVelocity
	Vector3_t3722313464  ____angularVelocity_2;
	// UnityEngine.Vector3 VelocityTmp::_velocity
	Vector3_t3722313464  ____velocity_3;

public:
	inline static int32_t get_offset_of__angularVelocity_2() { return static_cast<int32_t>(offsetof(VelocityTmp_t3274308841, ____angularVelocity_2)); }
	inline Vector3_t3722313464  get__angularVelocity_2() const { return ____angularVelocity_2; }
	inline Vector3_t3722313464 * get_address_of__angularVelocity_2() { return &____angularVelocity_2; }
	inline void set__angularVelocity_2(Vector3_t3722313464  value)
	{
		____angularVelocity_2 = value;
	}

	inline static int32_t get_offset_of__velocity_3() { return static_cast<int32_t>(offsetof(VelocityTmp_t3274308841, ____velocity_3)); }
	inline Vector3_t3722313464  get__velocity_3() const { return ____velocity_3; }
	inline Vector3_t3722313464 * get_address_of__velocity_3() { return &____velocity_3; }
	inline void set__velocity_3(Vector3_t3722313464  value)
	{
		____velocity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VELOCITYTMP_T3274308841_H
#ifndef UIVIEWPORT_T1918760134_H
#define UIVIEWPORT_T1918760134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIViewport
struct  UIViewport_t1918760134  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UIViewport::sourceCamera
	Camera_t4157153871 * ___sourceCamera_2;
	// UnityEngine.Transform UIViewport::topLeft
	Transform_t3600365921 * ___topLeft_3;
	// UnityEngine.Transform UIViewport::bottomRight
	Transform_t3600365921 * ___bottomRight_4;
	// System.Single UIViewport::fullSize
	float ___fullSize_5;
	// UnityEngine.Camera UIViewport::mCam
	Camera_t4157153871 * ___mCam_6;

public:
	inline static int32_t get_offset_of_sourceCamera_2() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___sourceCamera_2)); }
	inline Camera_t4157153871 * get_sourceCamera_2() const { return ___sourceCamera_2; }
	inline Camera_t4157153871 ** get_address_of_sourceCamera_2() { return &___sourceCamera_2; }
	inline void set_sourceCamera_2(Camera_t4157153871 * value)
	{
		___sourceCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceCamera_2), value);
	}

	inline static int32_t get_offset_of_topLeft_3() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___topLeft_3)); }
	inline Transform_t3600365921 * get_topLeft_3() const { return ___topLeft_3; }
	inline Transform_t3600365921 ** get_address_of_topLeft_3() { return &___topLeft_3; }
	inline void set_topLeft_3(Transform_t3600365921 * value)
	{
		___topLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___topLeft_3), value);
	}

	inline static int32_t get_offset_of_bottomRight_4() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___bottomRight_4)); }
	inline Transform_t3600365921 * get_bottomRight_4() const { return ___bottomRight_4; }
	inline Transform_t3600365921 ** get_address_of_bottomRight_4() { return &___bottomRight_4; }
	inline void set_bottomRight_4(Transform_t3600365921 * value)
	{
		___bottomRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___bottomRight_4), value);
	}

	inline static int32_t get_offset_of_fullSize_5() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___fullSize_5)); }
	inline float get_fullSize_5() const { return ___fullSize_5; }
	inline float* get_address_of_fullSize_5() { return &___fullSize_5; }
	inline void set_fullSize_5(float value)
	{
		___fullSize_5 = value;
	}

	inline static int32_t get_offset_of_mCam_6() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___mCam_6)); }
	inline Camera_t4157153871 * get_mCam_6() const { return ___mCam_6; }
	inline Camera_t4157153871 ** get_address_of_mCam_6() { return &___mCam_6; }
	inline void set_mCam_6(Camera_t4157153871 * value)
	{
		___mCam_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWPORT_T1918760134_H
#ifndef UIDRAWCALL_T1293405319_H
#define UIDRAWCALL_T1293405319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall
struct  UIDrawCall_t1293405319  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UIDrawCall::widgetCount
	int32_t ___widgetCount_4;
	// System.Int32 UIDrawCall::depthStart
	int32_t ___depthStart_5;
	// System.Int32 UIDrawCall::depthEnd
	int32_t ___depthEnd_6;
	// UIPanel UIDrawCall::manager
	UIPanel_t1716472341 * ___manager_7;
	// UIPanel UIDrawCall::panel
	UIPanel_t1716472341 * ___panel_8;
	// UnityEngine.Texture2D UIDrawCall::clipTexture
	Texture2D_t3840446185 * ___clipTexture_9;
	// System.Boolean UIDrawCall::alwaysOnScreen
	bool ___alwaysOnScreen_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIDrawCall::verts
	List_1_t899420910 * ___verts_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UIDrawCall::norms
	List_1_t899420910 * ___norms_12;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UIDrawCall::tans
	List_1_t496136383 * ___tans_13;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UIDrawCall::uvs
	List_1_t3628304265 * ___uvs_14;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UIDrawCall::uv2
	List_1_t496136383 * ___uv2_15;
	// System.Collections.Generic.List`1<UnityEngine.Color> UIDrawCall::cols
	List_1_t4027761066 * ___cols_16;
	// UnityEngine.Material UIDrawCall::mMaterial
	Material_t340375123 * ___mMaterial_17;
	// UnityEngine.Texture UIDrawCall::mTexture
	Texture_t3661962703 * ___mTexture_18;
	// UnityEngine.Shader UIDrawCall::mShader
	Shader_t4151988712 * ___mShader_19;
	// System.Int32 UIDrawCall::mClipCount
	int32_t ___mClipCount_20;
	// UnityEngine.Transform UIDrawCall::mTrans
	Transform_t3600365921 * ___mTrans_21;
	// UnityEngine.Mesh UIDrawCall::mMesh
	Mesh_t3648964284 * ___mMesh_22;
	// UnityEngine.MeshFilter UIDrawCall::mFilter
	MeshFilter_t3523625662 * ___mFilter_23;
	// UnityEngine.MeshRenderer UIDrawCall::mRenderer
	MeshRenderer_t587009260 * ___mRenderer_24;
	// UnityEngine.Material UIDrawCall::mDynamicMat
	Material_t340375123 * ___mDynamicMat_25;
	// System.Int32[] UIDrawCall::mIndices
	Int32U5BU5D_t385246372* ___mIndices_26;
	// UIDrawCall/ShadowMode UIDrawCall::mShadowMode
	int32_t ___mShadowMode_27;
	// System.Boolean UIDrawCall::mRebuildMat
	bool ___mRebuildMat_28;
	// System.Boolean UIDrawCall::mLegacyShader
	bool ___mLegacyShader_29;
	// System.Int32 UIDrawCall::mRenderQueue
	int32_t ___mRenderQueue_30;
	// System.Int32 UIDrawCall::mTriangles
	int32_t ___mTriangles_31;
	// System.Boolean UIDrawCall::isDirty
	bool ___isDirty_32;
	// System.Boolean UIDrawCall::mTextureClip
	bool ___mTextureClip_33;
	// System.Boolean UIDrawCall::mIsNew
	bool ___mIsNew_34;
	// UIDrawCall/OnRenderCallback UIDrawCall::onRender
	OnRenderCallback_t133425655 * ___onRender_35;
	// UIDrawCall/OnCreateDrawCall UIDrawCall::onCreateDrawCall
	OnCreateDrawCall_t609469653 * ___onCreateDrawCall_36;
	// System.String UIDrawCall::mSortingLayerName
	String_t* ___mSortingLayerName_37;
	// System.Int32 UIDrawCall::mSortingOrder
	int32_t ___mSortingOrder_38;
	// UnityEngine.MaterialPropertyBlock UIDrawCall::mBlock
	MaterialPropertyBlock_t3213117958 * ___mBlock_42;

public:
	inline static int32_t get_offset_of_widgetCount_4() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___widgetCount_4)); }
	inline int32_t get_widgetCount_4() const { return ___widgetCount_4; }
	inline int32_t* get_address_of_widgetCount_4() { return &___widgetCount_4; }
	inline void set_widgetCount_4(int32_t value)
	{
		___widgetCount_4 = value;
	}

	inline static int32_t get_offset_of_depthStart_5() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___depthStart_5)); }
	inline int32_t get_depthStart_5() const { return ___depthStart_5; }
	inline int32_t* get_address_of_depthStart_5() { return &___depthStart_5; }
	inline void set_depthStart_5(int32_t value)
	{
		___depthStart_5 = value;
	}

	inline static int32_t get_offset_of_depthEnd_6() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___depthEnd_6)); }
	inline int32_t get_depthEnd_6() const { return ___depthEnd_6; }
	inline int32_t* get_address_of_depthEnd_6() { return &___depthEnd_6; }
	inline void set_depthEnd_6(int32_t value)
	{
		___depthEnd_6 = value;
	}

	inline static int32_t get_offset_of_manager_7() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___manager_7)); }
	inline UIPanel_t1716472341 * get_manager_7() const { return ___manager_7; }
	inline UIPanel_t1716472341 ** get_address_of_manager_7() { return &___manager_7; }
	inline void set_manager_7(UIPanel_t1716472341 * value)
	{
		___manager_7 = value;
		Il2CppCodeGenWriteBarrier((&___manager_7), value);
	}

	inline static int32_t get_offset_of_panel_8() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___panel_8)); }
	inline UIPanel_t1716472341 * get_panel_8() const { return ___panel_8; }
	inline UIPanel_t1716472341 ** get_address_of_panel_8() { return &___panel_8; }
	inline void set_panel_8(UIPanel_t1716472341 * value)
	{
		___panel_8 = value;
		Il2CppCodeGenWriteBarrier((&___panel_8), value);
	}

	inline static int32_t get_offset_of_clipTexture_9() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___clipTexture_9)); }
	inline Texture2D_t3840446185 * get_clipTexture_9() const { return ___clipTexture_9; }
	inline Texture2D_t3840446185 ** get_address_of_clipTexture_9() { return &___clipTexture_9; }
	inline void set_clipTexture_9(Texture2D_t3840446185 * value)
	{
		___clipTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___clipTexture_9), value);
	}

	inline static int32_t get_offset_of_alwaysOnScreen_10() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___alwaysOnScreen_10)); }
	inline bool get_alwaysOnScreen_10() const { return ___alwaysOnScreen_10; }
	inline bool* get_address_of_alwaysOnScreen_10() { return &___alwaysOnScreen_10; }
	inline void set_alwaysOnScreen_10(bool value)
	{
		___alwaysOnScreen_10 = value;
	}

	inline static int32_t get_offset_of_verts_11() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___verts_11)); }
	inline List_1_t899420910 * get_verts_11() const { return ___verts_11; }
	inline List_1_t899420910 ** get_address_of_verts_11() { return &___verts_11; }
	inline void set_verts_11(List_1_t899420910 * value)
	{
		___verts_11 = value;
		Il2CppCodeGenWriteBarrier((&___verts_11), value);
	}

	inline static int32_t get_offset_of_norms_12() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___norms_12)); }
	inline List_1_t899420910 * get_norms_12() const { return ___norms_12; }
	inline List_1_t899420910 ** get_address_of_norms_12() { return &___norms_12; }
	inline void set_norms_12(List_1_t899420910 * value)
	{
		___norms_12 = value;
		Il2CppCodeGenWriteBarrier((&___norms_12), value);
	}

	inline static int32_t get_offset_of_tans_13() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___tans_13)); }
	inline List_1_t496136383 * get_tans_13() const { return ___tans_13; }
	inline List_1_t496136383 ** get_address_of_tans_13() { return &___tans_13; }
	inline void set_tans_13(List_1_t496136383 * value)
	{
		___tans_13 = value;
		Il2CppCodeGenWriteBarrier((&___tans_13), value);
	}

	inline static int32_t get_offset_of_uvs_14() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___uvs_14)); }
	inline List_1_t3628304265 * get_uvs_14() const { return ___uvs_14; }
	inline List_1_t3628304265 ** get_address_of_uvs_14() { return &___uvs_14; }
	inline void set_uvs_14(List_1_t3628304265 * value)
	{
		___uvs_14 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_14), value);
	}

	inline static int32_t get_offset_of_uv2_15() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___uv2_15)); }
	inline List_1_t496136383 * get_uv2_15() const { return ___uv2_15; }
	inline List_1_t496136383 ** get_address_of_uv2_15() { return &___uv2_15; }
	inline void set_uv2_15(List_1_t496136383 * value)
	{
		___uv2_15 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_15), value);
	}

	inline static int32_t get_offset_of_cols_16() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___cols_16)); }
	inline List_1_t4027761066 * get_cols_16() const { return ___cols_16; }
	inline List_1_t4027761066 ** get_address_of_cols_16() { return &___cols_16; }
	inline void set_cols_16(List_1_t4027761066 * value)
	{
		___cols_16 = value;
		Il2CppCodeGenWriteBarrier((&___cols_16), value);
	}

	inline static int32_t get_offset_of_mMaterial_17() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mMaterial_17)); }
	inline Material_t340375123 * get_mMaterial_17() const { return ___mMaterial_17; }
	inline Material_t340375123 ** get_address_of_mMaterial_17() { return &___mMaterial_17; }
	inline void set_mMaterial_17(Material_t340375123 * value)
	{
		___mMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___mMaterial_17), value);
	}

	inline static int32_t get_offset_of_mTexture_18() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTexture_18)); }
	inline Texture_t3661962703 * get_mTexture_18() const { return ___mTexture_18; }
	inline Texture_t3661962703 ** get_address_of_mTexture_18() { return &___mTexture_18; }
	inline void set_mTexture_18(Texture_t3661962703 * value)
	{
		___mTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_18), value);
	}

	inline static int32_t get_offset_of_mShader_19() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mShader_19)); }
	inline Shader_t4151988712 * get_mShader_19() const { return ___mShader_19; }
	inline Shader_t4151988712 ** get_address_of_mShader_19() { return &___mShader_19; }
	inline void set_mShader_19(Shader_t4151988712 * value)
	{
		___mShader_19 = value;
		Il2CppCodeGenWriteBarrier((&___mShader_19), value);
	}

	inline static int32_t get_offset_of_mClipCount_20() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mClipCount_20)); }
	inline int32_t get_mClipCount_20() const { return ___mClipCount_20; }
	inline int32_t* get_address_of_mClipCount_20() { return &___mClipCount_20; }
	inline void set_mClipCount_20(int32_t value)
	{
		___mClipCount_20 = value;
	}

	inline static int32_t get_offset_of_mTrans_21() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTrans_21)); }
	inline Transform_t3600365921 * get_mTrans_21() const { return ___mTrans_21; }
	inline Transform_t3600365921 ** get_address_of_mTrans_21() { return &___mTrans_21; }
	inline void set_mTrans_21(Transform_t3600365921 * value)
	{
		___mTrans_21 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_21), value);
	}

	inline static int32_t get_offset_of_mMesh_22() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mMesh_22)); }
	inline Mesh_t3648964284 * get_mMesh_22() const { return ___mMesh_22; }
	inline Mesh_t3648964284 ** get_address_of_mMesh_22() { return &___mMesh_22; }
	inline void set_mMesh_22(Mesh_t3648964284 * value)
	{
		___mMesh_22 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_22), value);
	}

	inline static int32_t get_offset_of_mFilter_23() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mFilter_23)); }
	inline MeshFilter_t3523625662 * get_mFilter_23() const { return ___mFilter_23; }
	inline MeshFilter_t3523625662 ** get_address_of_mFilter_23() { return &___mFilter_23; }
	inline void set_mFilter_23(MeshFilter_t3523625662 * value)
	{
		___mFilter_23 = value;
		Il2CppCodeGenWriteBarrier((&___mFilter_23), value);
	}

	inline static int32_t get_offset_of_mRenderer_24() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRenderer_24)); }
	inline MeshRenderer_t587009260 * get_mRenderer_24() const { return ___mRenderer_24; }
	inline MeshRenderer_t587009260 ** get_address_of_mRenderer_24() { return &___mRenderer_24; }
	inline void set_mRenderer_24(MeshRenderer_t587009260 * value)
	{
		___mRenderer_24 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderer_24), value);
	}

	inline static int32_t get_offset_of_mDynamicMat_25() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mDynamicMat_25)); }
	inline Material_t340375123 * get_mDynamicMat_25() const { return ___mDynamicMat_25; }
	inline Material_t340375123 ** get_address_of_mDynamicMat_25() { return &___mDynamicMat_25; }
	inline void set_mDynamicMat_25(Material_t340375123 * value)
	{
		___mDynamicMat_25 = value;
		Il2CppCodeGenWriteBarrier((&___mDynamicMat_25), value);
	}

	inline static int32_t get_offset_of_mIndices_26() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mIndices_26)); }
	inline Int32U5BU5D_t385246372* get_mIndices_26() const { return ___mIndices_26; }
	inline Int32U5BU5D_t385246372** get_address_of_mIndices_26() { return &___mIndices_26; }
	inline void set_mIndices_26(Int32U5BU5D_t385246372* value)
	{
		___mIndices_26 = value;
		Il2CppCodeGenWriteBarrier((&___mIndices_26), value);
	}

	inline static int32_t get_offset_of_mShadowMode_27() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mShadowMode_27)); }
	inline int32_t get_mShadowMode_27() const { return ___mShadowMode_27; }
	inline int32_t* get_address_of_mShadowMode_27() { return &___mShadowMode_27; }
	inline void set_mShadowMode_27(int32_t value)
	{
		___mShadowMode_27 = value;
	}

	inline static int32_t get_offset_of_mRebuildMat_28() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRebuildMat_28)); }
	inline bool get_mRebuildMat_28() const { return ___mRebuildMat_28; }
	inline bool* get_address_of_mRebuildMat_28() { return &___mRebuildMat_28; }
	inline void set_mRebuildMat_28(bool value)
	{
		___mRebuildMat_28 = value;
	}

	inline static int32_t get_offset_of_mLegacyShader_29() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mLegacyShader_29)); }
	inline bool get_mLegacyShader_29() const { return ___mLegacyShader_29; }
	inline bool* get_address_of_mLegacyShader_29() { return &___mLegacyShader_29; }
	inline void set_mLegacyShader_29(bool value)
	{
		___mLegacyShader_29 = value;
	}

	inline static int32_t get_offset_of_mRenderQueue_30() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRenderQueue_30)); }
	inline int32_t get_mRenderQueue_30() const { return ___mRenderQueue_30; }
	inline int32_t* get_address_of_mRenderQueue_30() { return &___mRenderQueue_30; }
	inline void set_mRenderQueue_30(int32_t value)
	{
		___mRenderQueue_30 = value;
	}

	inline static int32_t get_offset_of_mTriangles_31() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTriangles_31)); }
	inline int32_t get_mTriangles_31() const { return ___mTriangles_31; }
	inline int32_t* get_address_of_mTriangles_31() { return &___mTriangles_31; }
	inline void set_mTriangles_31(int32_t value)
	{
		___mTriangles_31 = value;
	}

	inline static int32_t get_offset_of_isDirty_32() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___isDirty_32)); }
	inline bool get_isDirty_32() const { return ___isDirty_32; }
	inline bool* get_address_of_isDirty_32() { return &___isDirty_32; }
	inline void set_isDirty_32(bool value)
	{
		___isDirty_32 = value;
	}

	inline static int32_t get_offset_of_mTextureClip_33() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTextureClip_33)); }
	inline bool get_mTextureClip_33() const { return ___mTextureClip_33; }
	inline bool* get_address_of_mTextureClip_33() { return &___mTextureClip_33; }
	inline void set_mTextureClip_33(bool value)
	{
		___mTextureClip_33 = value;
	}

	inline static int32_t get_offset_of_mIsNew_34() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mIsNew_34)); }
	inline bool get_mIsNew_34() const { return ___mIsNew_34; }
	inline bool* get_address_of_mIsNew_34() { return &___mIsNew_34; }
	inline void set_mIsNew_34(bool value)
	{
		___mIsNew_34 = value;
	}

	inline static int32_t get_offset_of_onRender_35() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___onRender_35)); }
	inline OnRenderCallback_t133425655 * get_onRender_35() const { return ___onRender_35; }
	inline OnRenderCallback_t133425655 ** get_address_of_onRender_35() { return &___onRender_35; }
	inline void set_onRender_35(OnRenderCallback_t133425655 * value)
	{
		___onRender_35 = value;
		Il2CppCodeGenWriteBarrier((&___onRender_35), value);
	}

	inline static int32_t get_offset_of_onCreateDrawCall_36() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___onCreateDrawCall_36)); }
	inline OnCreateDrawCall_t609469653 * get_onCreateDrawCall_36() const { return ___onCreateDrawCall_36; }
	inline OnCreateDrawCall_t609469653 ** get_address_of_onCreateDrawCall_36() { return &___onCreateDrawCall_36; }
	inline void set_onCreateDrawCall_36(OnCreateDrawCall_t609469653 * value)
	{
		___onCreateDrawCall_36 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateDrawCall_36), value);
	}

	inline static int32_t get_offset_of_mSortingLayerName_37() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mSortingLayerName_37)); }
	inline String_t* get_mSortingLayerName_37() const { return ___mSortingLayerName_37; }
	inline String_t** get_address_of_mSortingLayerName_37() { return &___mSortingLayerName_37; }
	inline void set_mSortingLayerName_37(String_t* value)
	{
		___mSortingLayerName_37 = value;
		Il2CppCodeGenWriteBarrier((&___mSortingLayerName_37), value);
	}

	inline static int32_t get_offset_of_mSortingOrder_38() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mSortingOrder_38)); }
	inline int32_t get_mSortingOrder_38() const { return ___mSortingOrder_38; }
	inline int32_t* get_address_of_mSortingOrder_38() { return &___mSortingOrder_38; }
	inline void set_mSortingOrder_38(int32_t value)
	{
		___mSortingOrder_38 = value;
	}

	inline static int32_t get_offset_of_mBlock_42() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mBlock_42)); }
	inline MaterialPropertyBlock_t3213117958 * get_mBlock_42() const { return ___mBlock_42; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_mBlock_42() { return &___mBlock_42; }
	inline void set_mBlock_42(MaterialPropertyBlock_t3213117958 * value)
	{
		___mBlock_42 = value;
		Il2CppCodeGenWriteBarrier((&___mBlock_42), value);
	}
};

struct UIDrawCall_t1293405319_StaticFields
{
public:
	// BetterList`1<UIDrawCall> UIDrawCall::mActiveList
	BetterList_1_t448425637 * ___mActiveList_2;
	// BetterList`1<UIDrawCall> UIDrawCall::mInactiveList
	BetterList_1_t448425637 * ___mInactiveList_3;
	// UnityEngine.ColorSpace UIDrawCall::mColorSpace
	int32_t ___mColorSpace_39;
	// System.Collections.Generic.List`1<System.Int32[]> UIDrawCall::mCache
	List_1_t1857321114 * ___mCache_41;
	// System.Int32[] UIDrawCall::ClipRange
	Int32U5BU5D_t385246372* ___ClipRange_43;
	// System.Int32[] UIDrawCall::ClipArgs
	Int32U5BU5D_t385246372* ___ClipArgs_44;
	// System.Int32 UIDrawCall::dx9BugWorkaround
	int32_t ___dx9BugWorkaround_45;

public:
	inline static int32_t get_offset_of_mActiveList_2() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mActiveList_2)); }
	inline BetterList_1_t448425637 * get_mActiveList_2() const { return ___mActiveList_2; }
	inline BetterList_1_t448425637 ** get_address_of_mActiveList_2() { return &___mActiveList_2; }
	inline void set_mActiveList_2(BetterList_1_t448425637 * value)
	{
		___mActiveList_2 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveList_2), value);
	}

	inline static int32_t get_offset_of_mInactiveList_3() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mInactiveList_3)); }
	inline BetterList_1_t448425637 * get_mInactiveList_3() const { return ___mInactiveList_3; }
	inline BetterList_1_t448425637 ** get_address_of_mInactiveList_3() { return &___mInactiveList_3; }
	inline void set_mInactiveList_3(BetterList_1_t448425637 * value)
	{
		___mInactiveList_3 = value;
		Il2CppCodeGenWriteBarrier((&___mInactiveList_3), value);
	}

	inline static int32_t get_offset_of_mColorSpace_39() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mColorSpace_39)); }
	inline int32_t get_mColorSpace_39() const { return ___mColorSpace_39; }
	inline int32_t* get_address_of_mColorSpace_39() { return &___mColorSpace_39; }
	inline void set_mColorSpace_39(int32_t value)
	{
		___mColorSpace_39 = value;
	}

	inline static int32_t get_offset_of_mCache_41() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mCache_41)); }
	inline List_1_t1857321114 * get_mCache_41() const { return ___mCache_41; }
	inline List_1_t1857321114 ** get_address_of_mCache_41() { return &___mCache_41; }
	inline void set_mCache_41(List_1_t1857321114 * value)
	{
		___mCache_41 = value;
		Il2CppCodeGenWriteBarrier((&___mCache_41), value);
	}

	inline static int32_t get_offset_of_ClipRange_43() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___ClipRange_43)); }
	inline Int32U5BU5D_t385246372* get_ClipRange_43() const { return ___ClipRange_43; }
	inline Int32U5BU5D_t385246372** get_address_of_ClipRange_43() { return &___ClipRange_43; }
	inline void set_ClipRange_43(Int32U5BU5D_t385246372* value)
	{
		___ClipRange_43 = value;
		Il2CppCodeGenWriteBarrier((&___ClipRange_43), value);
	}

	inline static int32_t get_offset_of_ClipArgs_44() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___ClipArgs_44)); }
	inline Int32U5BU5D_t385246372* get_ClipArgs_44() const { return ___ClipArgs_44; }
	inline Int32U5BU5D_t385246372** get_address_of_ClipArgs_44() { return &___ClipArgs_44; }
	inline void set_ClipArgs_44(Int32U5BU5D_t385246372* value)
	{
		___ClipArgs_44 = value;
		Il2CppCodeGenWriteBarrier((&___ClipArgs_44), value);
	}

	inline static int32_t get_offset_of_dx9BugWorkaround_45() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___dx9BugWorkaround_45)); }
	inline int32_t get_dx9BugWorkaround_45() const { return ___dx9BugWorkaround_45; }
	inline int32_t* get_address_of_dx9BugWorkaround_45() { return &___dx9BugWorkaround_45; }
	inline void set_dx9BugWorkaround_45(int32_t value)
	{
		___dx9BugWorkaround_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWCALL_T1293405319_H
#ifndef BOXCOLLIDER2D_T3581341831_H
#define BOXCOLLIDER2D_T3581341831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider2D
struct  BoxCollider2D_t3581341831  : public Collider2D_t2806799626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER2D_T3581341831_H
#ifndef UIWIDGETCONTAINER_T30162560_H
#define UIWIDGETCONTAINER_T30162560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidgetContainer
struct  UIWidgetContainer_t30162560  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWIDGETCONTAINER_T30162560_H
#ifndef UISCROLLVIEW_T1973404950_H
#define UISCROLLVIEW_T1973404950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView
struct  UIScrollView_t1973404950  : public MonoBehaviour_t3962482529
{
public:
	// UIScrollView/Movement UIScrollView::movement
	int32_t ___movement_3;
	// UIScrollView/DragEffect UIScrollView::dragEffect
	int32_t ___dragEffect_4;
	// System.Boolean UIScrollView::restrictWithinPanel
	bool ___restrictWithinPanel_5;
	// System.Boolean UIScrollView::constrainOnDrag
	bool ___constrainOnDrag_6;
	// System.Boolean UIScrollView::disableDragIfFits
	bool ___disableDragIfFits_7;
	// System.Boolean UIScrollView::smoothDragStart
	bool ___smoothDragStart_8;
	// System.Boolean UIScrollView::iOSDragEmulation
	bool ___iOSDragEmulation_9;
	// System.Single UIScrollView::scrollWheelFactor
	float ___scrollWheelFactor_10;
	// System.Single UIScrollView::momentumAmount
	float ___momentumAmount_11;
	// System.Single UIScrollView::dampenStrength
	float ___dampenStrength_12;
	// UIProgressBar UIScrollView::horizontalScrollBar
	UIProgressBar_t1222110469 * ___horizontalScrollBar_13;
	// UIProgressBar UIScrollView::verticalScrollBar
	UIProgressBar_t1222110469 * ___verticalScrollBar_14;
	// UIScrollView/ShowCondition UIScrollView::showScrollBars
	int32_t ___showScrollBars_15;
	// UnityEngine.Vector2 UIScrollView::customMovement
	Vector2_t2156229523  ___customMovement_16;
	// UIWidget/Pivot UIScrollView::contentPivot
	int32_t ___contentPivot_17;
	// UIScrollView/OnDragNotification UIScrollView::onDragStarted
	OnDragNotification_t1437737811 * ___onDragStarted_18;
	// UIScrollView/OnDragNotification UIScrollView::onDragFinished
	OnDragNotification_t1437737811 * ___onDragFinished_19;
	// UIScrollView/OnDragNotification UIScrollView::onMomentumMove
	OnDragNotification_t1437737811 * ___onMomentumMove_20;
	// UIScrollView/OnDragNotification UIScrollView::onStoppedMoving
	OnDragNotification_t1437737811 * ___onStoppedMoving_21;
	// UnityEngine.Vector3 UIScrollView::scale
	Vector3_t3722313464  ___scale_22;
	// UnityEngine.Vector2 UIScrollView::relativePositionOnReset
	Vector2_t2156229523  ___relativePositionOnReset_23;
	// UnityEngine.Transform UIScrollView::mTrans
	Transform_t3600365921 * ___mTrans_24;
	// UIPanel UIScrollView::mPanel
	UIPanel_t1716472341 * ___mPanel_25;
	// UnityEngine.Plane UIScrollView::mPlane
	Plane_t1000493321  ___mPlane_26;
	// UnityEngine.Vector3 UIScrollView::mLastPos
	Vector3_t3722313464  ___mLastPos_27;
	// System.Boolean UIScrollView::mPressed
	bool ___mPressed_28;
	// UnityEngine.Vector3 UIScrollView::mMomentum
	Vector3_t3722313464  ___mMomentum_29;
	// System.Single UIScrollView::mScroll
	float ___mScroll_30;
	// UnityEngine.Bounds UIScrollView::mBounds
	Bounds_t2266837910  ___mBounds_31;
	// System.Boolean UIScrollView::mCalculatedBounds
	bool ___mCalculatedBounds_32;
	// System.Boolean UIScrollView::mShouldMove
	bool ___mShouldMove_33;
	// System.Boolean UIScrollView::mIgnoreCallbacks
	bool ___mIgnoreCallbacks_34;
	// System.Int32 UIScrollView::mDragID
	int32_t ___mDragID_35;
	// UnityEngine.Vector2 UIScrollView::mDragStartOffset
	Vector2_t2156229523  ___mDragStartOffset_36;
	// System.Boolean UIScrollView::mDragStarted
	bool ___mDragStarted_37;
	// System.Boolean UIScrollView::mStarted
	bool ___mStarted_38;
	// UICenterOnChild UIScrollView::centerOnChild
	UICenterOnChild_t253063637 * ___centerOnChild_39;

public:
	inline static int32_t get_offset_of_movement_3() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___movement_3)); }
	inline int32_t get_movement_3() const { return ___movement_3; }
	inline int32_t* get_address_of_movement_3() { return &___movement_3; }
	inline void set_movement_3(int32_t value)
	{
		___movement_3 = value;
	}

	inline static int32_t get_offset_of_dragEffect_4() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___dragEffect_4)); }
	inline int32_t get_dragEffect_4() const { return ___dragEffect_4; }
	inline int32_t* get_address_of_dragEffect_4() { return &___dragEffect_4; }
	inline void set_dragEffect_4(int32_t value)
	{
		___dragEffect_4 = value;
	}

	inline static int32_t get_offset_of_restrictWithinPanel_5() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___restrictWithinPanel_5)); }
	inline bool get_restrictWithinPanel_5() const { return ___restrictWithinPanel_5; }
	inline bool* get_address_of_restrictWithinPanel_5() { return &___restrictWithinPanel_5; }
	inline void set_restrictWithinPanel_5(bool value)
	{
		___restrictWithinPanel_5 = value;
	}

	inline static int32_t get_offset_of_constrainOnDrag_6() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___constrainOnDrag_6)); }
	inline bool get_constrainOnDrag_6() const { return ___constrainOnDrag_6; }
	inline bool* get_address_of_constrainOnDrag_6() { return &___constrainOnDrag_6; }
	inline void set_constrainOnDrag_6(bool value)
	{
		___constrainOnDrag_6 = value;
	}

	inline static int32_t get_offset_of_disableDragIfFits_7() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___disableDragIfFits_7)); }
	inline bool get_disableDragIfFits_7() const { return ___disableDragIfFits_7; }
	inline bool* get_address_of_disableDragIfFits_7() { return &___disableDragIfFits_7; }
	inline void set_disableDragIfFits_7(bool value)
	{
		___disableDragIfFits_7 = value;
	}

	inline static int32_t get_offset_of_smoothDragStart_8() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___smoothDragStart_8)); }
	inline bool get_smoothDragStart_8() const { return ___smoothDragStart_8; }
	inline bool* get_address_of_smoothDragStart_8() { return &___smoothDragStart_8; }
	inline void set_smoothDragStart_8(bool value)
	{
		___smoothDragStart_8 = value;
	}

	inline static int32_t get_offset_of_iOSDragEmulation_9() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___iOSDragEmulation_9)); }
	inline bool get_iOSDragEmulation_9() const { return ___iOSDragEmulation_9; }
	inline bool* get_address_of_iOSDragEmulation_9() { return &___iOSDragEmulation_9; }
	inline void set_iOSDragEmulation_9(bool value)
	{
		___iOSDragEmulation_9 = value;
	}

	inline static int32_t get_offset_of_scrollWheelFactor_10() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___scrollWheelFactor_10)); }
	inline float get_scrollWheelFactor_10() const { return ___scrollWheelFactor_10; }
	inline float* get_address_of_scrollWheelFactor_10() { return &___scrollWheelFactor_10; }
	inline void set_scrollWheelFactor_10(float value)
	{
		___scrollWheelFactor_10 = value;
	}

	inline static int32_t get_offset_of_momentumAmount_11() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___momentumAmount_11)); }
	inline float get_momentumAmount_11() const { return ___momentumAmount_11; }
	inline float* get_address_of_momentumAmount_11() { return &___momentumAmount_11; }
	inline void set_momentumAmount_11(float value)
	{
		___momentumAmount_11 = value;
	}

	inline static int32_t get_offset_of_dampenStrength_12() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___dampenStrength_12)); }
	inline float get_dampenStrength_12() const { return ___dampenStrength_12; }
	inline float* get_address_of_dampenStrength_12() { return &___dampenStrength_12; }
	inline void set_dampenStrength_12(float value)
	{
		___dampenStrength_12 = value;
	}

	inline static int32_t get_offset_of_horizontalScrollBar_13() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___horizontalScrollBar_13)); }
	inline UIProgressBar_t1222110469 * get_horizontalScrollBar_13() const { return ___horizontalScrollBar_13; }
	inline UIProgressBar_t1222110469 ** get_address_of_horizontalScrollBar_13() { return &___horizontalScrollBar_13; }
	inline void set_horizontalScrollBar_13(UIProgressBar_t1222110469 * value)
	{
		___horizontalScrollBar_13 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalScrollBar_13), value);
	}

	inline static int32_t get_offset_of_verticalScrollBar_14() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___verticalScrollBar_14)); }
	inline UIProgressBar_t1222110469 * get_verticalScrollBar_14() const { return ___verticalScrollBar_14; }
	inline UIProgressBar_t1222110469 ** get_address_of_verticalScrollBar_14() { return &___verticalScrollBar_14; }
	inline void set_verticalScrollBar_14(UIProgressBar_t1222110469 * value)
	{
		___verticalScrollBar_14 = value;
		Il2CppCodeGenWriteBarrier((&___verticalScrollBar_14), value);
	}

	inline static int32_t get_offset_of_showScrollBars_15() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___showScrollBars_15)); }
	inline int32_t get_showScrollBars_15() const { return ___showScrollBars_15; }
	inline int32_t* get_address_of_showScrollBars_15() { return &___showScrollBars_15; }
	inline void set_showScrollBars_15(int32_t value)
	{
		___showScrollBars_15 = value;
	}

	inline static int32_t get_offset_of_customMovement_16() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___customMovement_16)); }
	inline Vector2_t2156229523  get_customMovement_16() const { return ___customMovement_16; }
	inline Vector2_t2156229523 * get_address_of_customMovement_16() { return &___customMovement_16; }
	inline void set_customMovement_16(Vector2_t2156229523  value)
	{
		___customMovement_16 = value;
	}

	inline static int32_t get_offset_of_contentPivot_17() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___contentPivot_17)); }
	inline int32_t get_contentPivot_17() const { return ___contentPivot_17; }
	inline int32_t* get_address_of_contentPivot_17() { return &___contentPivot_17; }
	inline void set_contentPivot_17(int32_t value)
	{
		___contentPivot_17 = value;
	}

	inline static int32_t get_offset_of_onDragStarted_18() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onDragStarted_18)); }
	inline OnDragNotification_t1437737811 * get_onDragStarted_18() const { return ___onDragStarted_18; }
	inline OnDragNotification_t1437737811 ** get_address_of_onDragStarted_18() { return &___onDragStarted_18; }
	inline void set_onDragStarted_18(OnDragNotification_t1437737811 * value)
	{
		___onDragStarted_18 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStarted_18), value);
	}

	inline static int32_t get_offset_of_onDragFinished_19() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onDragFinished_19)); }
	inline OnDragNotification_t1437737811 * get_onDragFinished_19() const { return ___onDragFinished_19; }
	inline OnDragNotification_t1437737811 ** get_address_of_onDragFinished_19() { return &___onDragFinished_19; }
	inline void set_onDragFinished_19(OnDragNotification_t1437737811 * value)
	{
		___onDragFinished_19 = value;
		Il2CppCodeGenWriteBarrier((&___onDragFinished_19), value);
	}

	inline static int32_t get_offset_of_onMomentumMove_20() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onMomentumMove_20)); }
	inline OnDragNotification_t1437737811 * get_onMomentumMove_20() const { return ___onMomentumMove_20; }
	inline OnDragNotification_t1437737811 ** get_address_of_onMomentumMove_20() { return &___onMomentumMove_20; }
	inline void set_onMomentumMove_20(OnDragNotification_t1437737811 * value)
	{
		___onMomentumMove_20 = value;
		Il2CppCodeGenWriteBarrier((&___onMomentumMove_20), value);
	}

	inline static int32_t get_offset_of_onStoppedMoving_21() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onStoppedMoving_21)); }
	inline OnDragNotification_t1437737811 * get_onStoppedMoving_21() const { return ___onStoppedMoving_21; }
	inline OnDragNotification_t1437737811 ** get_address_of_onStoppedMoving_21() { return &___onStoppedMoving_21; }
	inline void set_onStoppedMoving_21(OnDragNotification_t1437737811 * value)
	{
		___onStoppedMoving_21 = value;
		Il2CppCodeGenWriteBarrier((&___onStoppedMoving_21), value);
	}

	inline static int32_t get_offset_of_scale_22() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___scale_22)); }
	inline Vector3_t3722313464  get_scale_22() const { return ___scale_22; }
	inline Vector3_t3722313464 * get_address_of_scale_22() { return &___scale_22; }
	inline void set_scale_22(Vector3_t3722313464  value)
	{
		___scale_22 = value;
	}

	inline static int32_t get_offset_of_relativePositionOnReset_23() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___relativePositionOnReset_23)); }
	inline Vector2_t2156229523  get_relativePositionOnReset_23() const { return ___relativePositionOnReset_23; }
	inline Vector2_t2156229523 * get_address_of_relativePositionOnReset_23() { return &___relativePositionOnReset_23; }
	inline void set_relativePositionOnReset_23(Vector2_t2156229523  value)
	{
		___relativePositionOnReset_23 = value;
	}

	inline static int32_t get_offset_of_mTrans_24() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mTrans_24)); }
	inline Transform_t3600365921 * get_mTrans_24() const { return ___mTrans_24; }
	inline Transform_t3600365921 ** get_address_of_mTrans_24() { return &___mTrans_24; }
	inline void set_mTrans_24(Transform_t3600365921 * value)
	{
		___mTrans_24 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_24), value);
	}

	inline static int32_t get_offset_of_mPanel_25() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mPanel_25)); }
	inline UIPanel_t1716472341 * get_mPanel_25() const { return ___mPanel_25; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_25() { return &___mPanel_25; }
	inline void set_mPanel_25(UIPanel_t1716472341 * value)
	{
		___mPanel_25 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_25), value);
	}

	inline static int32_t get_offset_of_mPlane_26() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mPlane_26)); }
	inline Plane_t1000493321  get_mPlane_26() const { return ___mPlane_26; }
	inline Plane_t1000493321 * get_address_of_mPlane_26() { return &___mPlane_26; }
	inline void set_mPlane_26(Plane_t1000493321  value)
	{
		___mPlane_26 = value;
	}

	inline static int32_t get_offset_of_mLastPos_27() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mLastPos_27)); }
	inline Vector3_t3722313464  get_mLastPos_27() const { return ___mLastPos_27; }
	inline Vector3_t3722313464 * get_address_of_mLastPos_27() { return &___mLastPos_27; }
	inline void set_mLastPos_27(Vector3_t3722313464  value)
	{
		___mLastPos_27 = value;
	}

	inline static int32_t get_offset_of_mPressed_28() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mPressed_28)); }
	inline bool get_mPressed_28() const { return ___mPressed_28; }
	inline bool* get_address_of_mPressed_28() { return &___mPressed_28; }
	inline void set_mPressed_28(bool value)
	{
		___mPressed_28 = value;
	}

	inline static int32_t get_offset_of_mMomentum_29() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mMomentum_29)); }
	inline Vector3_t3722313464  get_mMomentum_29() const { return ___mMomentum_29; }
	inline Vector3_t3722313464 * get_address_of_mMomentum_29() { return &___mMomentum_29; }
	inline void set_mMomentum_29(Vector3_t3722313464  value)
	{
		___mMomentum_29 = value;
	}

	inline static int32_t get_offset_of_mScroll_30() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mScroll_30)); }
	inline float get_mScroll_30() const { return ___mScroll_30; }
	inline float* get_address_of_mScroll_30() { return &___mScroll_30; }
	inline void set_mScroll_30(float value)
	{
		___mScroll_30 = value;
	}

	inline static int32_t get_offset_of_mBounds_31() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mBounds_31)); }
	inline Bounds_t2266837910  get_mBounds_31() const { return ___mBounds_31; }
	inline Bounds_t2266837910 * get_address_of_mBounds_31() { return &___mBounds_31; }
	inline void set_mBounds_31(Bounds_t2266837910  value)
	{
		___mBounds_31 = value;
	}

	inline static int32_t get_offset_of_mCalculatedBounds_32() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mCalculatedBounds_32)); }
	inline bool get_mCalculatedBounds_32() const { return ___mCalculatedBounds_32; }
	inline bool* get_address_of_mCalculatedBounds_32() { return &___mCalculatedBounds_32; }
	inline void set_mCalculatedBounds_32(bool value)
	{
		___mCalculatedBounds_32 = value;
	}

	inline static int32_t get_offset_of_mShouldMove_33() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mShouldMove_33)); }
	inline bool get_mShouldMove_33() const { return ___mShouldMove_33; }
	inline bool* get_address_of_mShouldMove_33() { return &___mShouldMove_33; }
	inline void set_mShouldMove_33(bool value)
	{
		___mShouldMove_33 = value;
	}

	inline static int32_t get_offset_of_mIgnoreCallbacks_34() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mIgnoreCallbacks_34)); }
	inline bool get_mIgnoreCallbacks_34() const { return ___mIgnoreCallbacks_34; }
	inline bool* get_address_of_mIgnoreCallbacks_34() { return &___mIgnoreCallbacks_34; }
	inline void set_mIgnoreCallbacks_34(bool value)
	{
		___mIgnoreCallbacks_34 = value;
	}

	inline static int32_t get_offset_of_mDragID_35() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mDragID_35)); }
	inline int32_t get_mDragID_35() const { return ___mDragID_35; }
	inline int32_t* get_address_of_mDragID_35() { return &___mDragID_35; }
	inline void set_mDragID_35(int32_t value)
	{
		___mDragID_35 = value;
	}

	inline static int32_t get_offset_of_mDragStartOffset_36() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mDragStartOffset_36)); }
	inline Vector2_t2156229523  get_mDragStartOffset_36() const { return ___mDragStartOffset_36; }
	inline Vector2_t2156229523 * get_address_of_mDragStartOffset_36() { return &___mDragStartOffset_36; }
	inline void set_mDragStartOffset_36(Vector2_t2156229523  value)
	{
		___mDragStartOffset_36 = value;
	}

	inline static int32_t get_offset_of_mDragStarted_37() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mDragStarted_37)); }
	inline bool get_mDragStarted_37() const { return ___mDragStarted_37; }
	inline bool* get_address_of_mDragStarted_37() { return &___mDragStarted_37; }
	inline void set_mDragStarted_37(bool value)
	{
		___mDragStarted_37 = value;
	}

	inline static int32_t get_offset_of_mStarted_38() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mStarted_38)); }
	inline bool get_mStarted_38() const { return ___mStarted_38; }
	inline bool* get_address_of_mStarted_38() { return &___mStarted_38; }
	inline void set_mStarted_38(bool value)
	{
		___mStarted_38 = value;
	}

	inline static int32_t get_offset_of_centerOnChild_39() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___centerOnChild_39)); }
	inline UICenterOnChild_t253063637 * get_centerOnChild_39() const { return ___centerOnChild_39; }
	inline UICenterOnChild_t253063637 ** get_address_of_centerOnChild_39() { return &___centerOnChild_39; }
	inline void set_centerOnChild_39(UICenterOnChild_t253063637 * value)
	{
		___centerOnChild_39 = value;
		Il2CppCodeGenWriteBarrier((&___centerOnChild_39), value);
	}
};

struct UIScrollView_t1973404950_StaticFields
{
public:
	// BetterList`1<UIScrollView> UIScrollView::list
	BetterList_1_t1128425268 * ___list_2;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950_StaticFields, ___list_2)); }
	inline BetterList_1_t1128425268 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t1128425268 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t1128425268 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLVIEW_T1973404950_H
#ifndef VELOCITY2DTMP_T2283591314_H
#define VELOCITY2DTMP_T2283591314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Velocity2DTmp
struct  Velocity2DTmp_t2283591314  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Velocity2DTmp::_angularVelocity
	float ____angularVelocity_2;
	// UnityEngine.Vector2 Velocity2DTmp::_velocity
	Vector2_t2156229523  ____velocity_3;

public:
	inline static int32_t get_offset_of__angularVelocity_2() { return static_cast<int32_t>(offsetof(Velocity2DTmp_t2283591314, ____angularVelocity_2)); }
	inline float get__angularVelocity_2() const { return ____angularVelocity_2; }
	inline float* get_address_of__angularVelocity_2() { return &____angularVelocity_2; }
	inline void set__angularVelocity_2(float value)
	{
		____angularVelocity_2 = value;
	}

	inline static int32_t get_offset_of__velocity_3() { return static_cast<int32_t>(offsetof(Velocity2DTmp_t2283591314, ____velocity_3)); }
	inline Vector2_t2156229523  get__velocity_3() const { return ____velocity_3; }
	inline Vector2_t2156229523 * get_address_of__velocity_3() { return &____velocity_3; }
	inline void set__velocity_3(Vector2_t2156229523  value)
	{
		____velocity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VELOCITY2DTMP_T2283591314_H
#ifndef UIWRAPCONTENT_T1188558554_H
#define UIWRAPCONTENT_T1188558554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWrapContent
struct  UIWrapContent_t1188558554  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UIWrapContent::itemSize
	int32_t ___itemSize_2;
	// System.Boolean UIWrapContent::cullContent
	bool ___cullContent_3;
	// System.Int32 UIWrapContent::minIndex
	int32_t ___minIndex_4;
	// System.Int32 UIWrapContent::maxIndex
	int32_t ___maxIndex_5;
	// System.Boolean UIWrapContent::hideInactive
	bool ___hideInactive_6;
	// UIWrapContent/OnInitializeItem UIWrapContent::onInitializeItem
	OnInitializeItem_t992046894 * ___onInitializeItem_7;
	// UnityEngine.Transform UIWrapContent::mTrans
	Transform_t3600365921 * ___mTrans_8;
	// UIPanel UIWrapContent::mPanel
	UIPanel_t1716472341 * ___mPanel_9;
	// UIScrollView UIWrapContent::mScroll
	UIScrollView_t1973404950 * ___mScroll_10;
	// System.Boolean UIWrapContent::mHorizontal
	bool ___mHorizontal_11;
	// System.Boolean UIWrapContent::mFirstTime
	bool ___mFirstTime_12;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UIWrapContent::mChildren
	List_1_t777473367 * ___mChildren_13;

public:
	inline static int32_t get_offset_of_itemSize_2() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___itemSize_2)); }
	inline int32_t get_itemSize_2() const { return ___itemSize_2; }
	inline int32_t* get_address_of_itemSize_2() { return &___itemSize_2; }
	inline void set_itemSize_2(int32_t value)
	{
		___itemSize_2 = value;
	}

	inline static int32_t get_offset_of_cullContent_3() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___cullContent_3)); }
	inline bool get_cullContent_3() const { return ___cullContent_3; }
	inline bool* get_address_of_cullContent_3() { return &___cullContent_3; }
	inline void set_cullContent_3(bool value)
	{
		___cullContent_3 = value;
	}

	inline static int32_t get_offset_of_minIndex_4() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___minIndex_4)); }
	inline int32_t get_minIndex_4() const { return ___minIndex_4; }
	inline int32_t* get_address_of_minIndex_4() { return &___minIndex_4; }
	inline void set_minIndex_4(int32_t value)
	{
		___minIndex_4 = value;
	}

	inline static int32_t get_offset_of_maxIndex_5() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___maxIndex_5)); }
	inline int32_t get_maxIndex_5() const { return ___maxIndex_5; }
	inline int32_t* get_address_of_maxIndex_5() { return &___maxIndex_5; }
	inline void set_maxIndex_5(int32_t value)
	{
		___maxIndex_5 = value;
	}

	inline static int32_t get_offset_of_hideInactive_6() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___hideInactive_6)); }
	inline bool get_hideInactive_6() const { return ___hideInactive_6; }
	inline bool* get_address_of_hideInactive_6() { return &___hideInactive_6; }
	inline void set_hideInactive_6(bool value)
	{
		___hideInactive_6 = value;
	}

	inline static int32_t get_offset_of_onInitializeItem_7() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___onInitializeItem_7)); }
	inline OnInitializeItem_t992046894 * get_onInitializeItem_7() const { return ___onInitializeItem_7; }
	inline OnInitializeItem_t992046894 ** get_address_of_onInitializeItem_7() { return &___onInitializeItem_7; }
	inline void set_onInitializeItem_7(OnInitializeItem_t992046894 * value)
	{
		___onInitializeItem_7 = value;
		Il2CppCodeGenWriteBarrier((&___onInitializeItem_7), value);
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mTrans_8)); }
	inline Transform_t3600365921 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t3600365921 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t3600365921 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mPanel_9() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mPanel_9)); }
	inline UIPanel_t1716472341 * get_mPanel_9() const { return ___mPanel_9; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_9() { return &___mPanel_9; }
	inline void set_mPanel_9(UIPanel_t1716472341 * value)
	{
		___mPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_9), value);
	}

	inline static int32_t get_offset_of_mScroll_10() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mScroll_10)); }
	inline UIScrollView_t1973404950 * get_mScroll_10() const { return ___mScroll_10; }
	inline UIScrollView_t1973404950 ** get_address_of_mScroll_10() { return &___mScroll_10; }
	inline void set_mScroll_10(UIScrollView_t1973404950 * value)
	{
		___mScroll_10 = value;
		Il2CppCodeGenWriteBarrier((&___mScroll_10), value);
	}

	inline static int32_t get_offset_of_mHorizontal_11() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mHorizontal_11)); }
	inline bool get_mHorizontal_11() const { return ___mHorizontal_11; }
	inline bool* get_address_of_mHorizontal_11() { return &___mHorizontal_11; }
	inline void set_mHorizontal_11(bool value)
	{
		___mHorizontal_11 = value;
	}

	inline static int32_t get_offset_of_mFirstTime_12() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mFirstTime_12)); }
	inline bool get_mFirstTime_12() const { return ___mFirstTime_12; }
	inline bool* get_address_of_mFirstTime_12() { return &___mFirstTime_12; }
	inline void set_mFirstTime_12(bool value)
	{
		___mFirstTime_12 = value;
	}

	inline static int32_t get_offset_of_mChildren_13() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mChildren_13)); }
	inline List_1_t777473367 * get_mChildren_13() const { return ___mChildren_13; }
	inline List_1_t777473367 ** get_address_of_mChildren_13() { return &___mChildren_13; }
	inline void set_mChildren_13(List_1_t777473367 * value)
	{
		___mChildren_13 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_13), value);
	}
};

struct UIWrapContent_t1188558554_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.Transform> UIWrapContent::<>f__mg$cache0
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache0_14;
	// System.Comparison`1<UnityEngine.Transform> UIWrapContent::<>f__mg$cache1
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache1_15;
	// System.Comparison`1<UnityEngine.Transform> UIWrapContent::<>f__mg$cache2
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache2_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_14() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554_StaticFields, ___U3CU3Ef__mgU24cache0_14)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache0_14() const { return ___U3CU3Ef__mgU24cache0_14; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache0_14() { return &___U3CU3Ef__mgU24cache0_14; }
	inline void set_U3CU3Ef__mgU24cache0_14(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_15() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554_StaticFields, ___U3CU3Ef__mgU24cache1_15)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache1_15() const { return ___U3CU3Ef__mgU24cache1_15; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache1_15() { return &___U3CU3Ef__mgU24cache1_15; }
	inline void set_U3CU3Ef__mgU24cache1_15(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_16() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554_StaticFields, ___U3CU3Ef__mgU24cache2_16)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache2_16() const { return ___U3CU3Ef__mgU24cache2_16; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache2_16() { return &___U3CU3Ef__mgU24cache2_16; }
	inline void set_U3CU3Ef__mgU24cache2_16(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWRAPCONTENT_T1188558554_H
#ifndef UITOGGLE_T4192126258_H
#define UITOGGLE_T4192126258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggle
struct  UIToggle_t4192126258  : public UIWidgetContainer_t30162560
{
public:
	// System.Int32 UIToggle::group
	int32_t ___group_4;
	// UIWidget UIToggle::activeSprite
	UIWidget_t3538521925 * ___activeSprite_5;
	// System.Boolean UIToggle::invertSpriteState
	bool ___invertSpriteState_6;
	// UnityEngine.Animation UIToggle::activeAnimation
	Animation_t3648466861 * ___activeAnimation_7;
	// UnityEngine.Animator UIToggle::animator
	Animator_t434523843 * ___animator_8;
	// UITweener UIToggle::tween
	UITweener_t260334902 * ___tween_9;
	// System.Boolean UIToggle::startsActive
	bool ___startsActive_10;
	// System.Boolean UIToggle::instantTween
	bool ___instantTween_11;
	// System.Boolean UIToggle::optionCanBeNone
	bool ___optionCanBeNone_12;
	// System.Collections.Generic.List`1<EventDelegate> UIToggle::onChange
	List_1_t4210400802 * ___onChange_13;
	// UIToggle/Validate UIToggle::validator
	Validate_t3702293971 * ___validator_14;
	// UISprite UIToggle::checkSprite
	UISprite_t194114938 * ___checkSprite_15;
	// UnityEngine.Animation UIToggle::checkAnimation
	Animation_t3648466861 * ___checkAnimation_16;
	// UnityEngine.GameObject UIToggle::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_17;
	// System.String UIToggle::functionName
	String_t* ___functionName_18;
	// System.Boolean UIToggle::startsChecked
	bool ___startsChecked_19;
	// System.Boolean UIToggle::mIsActive
	bool ___mIsActive_20;
	// System.Boolean UIToggle::mStarted
	bool ___mStarted_21;

public:
	inline static int32_t get_offset_of_group_4() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___group_4)); }
	inline int32_t get_group_4() const { return ___group_4; }
	inline int32_t* get_address_of_group_4() { return &___group_4; }
	inline void set_group_4(int32_t value)
	{
		___group_4 = value;
	}

	inline static int32_t get_offset_of_activeSprite_5() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___activeSprite_5)); }
	inline UIWidget_t3538521925 * get_activeSprite_5() const { return ___activeSprite_5; }
	inline UIWidget_t3538521925 ** get_address_of_activeSprite_5() { return &___activeSprite_5; }
	inline void set_activeSprite_5(UIWidget_t3538521925 * value)
	{
		___activeSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___activeSprite_5), value);
	}

	inline static int32_t get_offset_of_invertSpriteState_6() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___invertSpriteState_6)); }
	inline bool get_invertSpriteState_6() const { return ___invertSpriteState_6; }
	inline bool* get_address_of_invertSpriteState_6() { return &___invertSpriteState_6; }
	inline void set_invertSpriteState_6(bool value)
	{
		___invertSpriteState_6 = value;
	}

	inline static int32_t get_offset_of_activeAnimation_7() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___activeAnimation_7)); }
	inline Animation_t3648466861 * get_activeAnimation_7() const { return ___activeAnimation_7; }
	inline Animation_t3648466861 ** get_address_of_activeAnimation_7() { return &___activeAnimation_7; }
	inline void set_activeAnimation_7(Animation_t3648466861 * value)
	{
		___activeAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___activeAnimation_7), value);
	}

	inline static int32_t get_offset_of_animator_8() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___animator_8)); }
	inline Animator_t434523843 * get_animator_8() const { return ___animator_8; }
	inline Animator_t434523843 ** get_address_of_animator_8() { return &___animator_8; }
	inline void set_animator_8(Animator_t434523843 * value)
	{
		___animator_8 = value;
		Il2CppCodeGenWriteBarrier((&___animator_8), value);
	}

	inline static int32_t get_offset_of_tween_9() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___tween_9)); }
	inline UITweener_t260334902 * get_tween_9() const { return ___tween_9; }
	inline UITweener_t260334902 ** get_address_of_tween_9() { return &___tween_9; }
	inline void set_tween_9(UITweener_t260334902 * value)
	{
		___tween_9 = value;
		Il2CppCodeGenWriteBarrier((&___tween_9), value);
	}

	inline static int32_t get_offset_of_startsActive_10() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___startsActive_10)); }
	inline bool get_startsActive_10() const { return ___startsActive_10; }
	inline bool* get_address_of_startsActive_10() { return &___startsActive_10; }
	inline void set_startsActive_10(bool value)
	{
		___startsActive_10 = value;
	}

	inline static int32_t get_offset_of_instantTween_11() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___instantTween_11)); }
	inline bool get_instantTween_11() const { return ___instantTween_11; }
	inline bool* get_address_of_instantTween_11() { return &___instantTween_11; }
	inline void set_instantTween_11(bool value)
	{
		___instantTween_11 = value;
	}

	inline static int32_t get_offset_of_optionCanBeNone_12() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___optionCanBeNone_12)); }
	inline bool get_optionCanBeNone_12() const { return ___optionCanBeNone_12; }
	inline bool* get_address_of_optionCanBeNone_12() { return &___optionCanBeNone_12; }
	inline void set_optionCanBeNone_12(bool value)
	{
		___optionCanBeNone_12 = value;
	}

	inline static int32_t get_offset_of_onChange_13() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___onChange_13)); }
	inline List_1_t4210400802 * get_onChange_13() const { return ___onChange_13; }
	inline List_1_t4210400802 ** get_address_of_onChange_13() { return &___onChange_13; }
	inline void set_onChange_13(List_1_t4210400802 * value)
	{
		___onChange_13 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_13), value);
	}

	inline static int32_t get_offset_of_validator_14() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___validator_14)); }
	inline Validate_t3702293971 * get_validator_14() const { return ___validator_14; }
	inline Validate_t3702293971 ** get_address_of_validator_14() { return &___validator_14; }
	inline void set_validator_14(Validate_t3702293971 * value)
	{
		___validator_14 = value;
		Il2CppCodeGenWriteBarrier((&___validator_14), value);
	}

	inline static int32_t get_offset_of_checkSprite_15() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___checkSprite_15)); }
	inline UISprite_t194114938 * get_checkSprite_15() const { return ___checkSprite_15; }
	inline UISprite_t194114938 ** get_address_of_checkSprite_15() { return &___checkSprite_15; }
	inline void set_checkSprite_15(UISprite_t194114938 * value)
	{
		___checkSprite_15 = value;
		Il2CppCodeGenWriteBarrier((&___checkSprite_15), value);
	}

	inline static int32_t get_offset_of_checkAnimation_16() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___checkAnimation_16)); }
	inline Animation_t3648466861 * get_checkAnimation_16() const { return ___checkAnimation_16; }
	inline Animation_t3648466861 ** get_address_of_checkAnimation_16() { return &___checkAnimation_16; }
	inline void set_checkAnimation_16(Animation_t3648466861 * value)
	{
		___checkAnimation_16 = value;
		Il2CppCodeGenWriteBarrier((&___checkAnimation_16), value);
	}

	inline static int32_t get_offset_of_eventReceiver_17() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___eventReceiver_17)); }
	inline GameObject_t1113636619 * get_eventReceiver_17() const { return ___eventReceiver_17; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_17() { return &___eventReceiver_17; }
	inline void set_eventReceiver_17(GameObject_t1113636619 * value)
	{
		___eventReceiver_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_17), value);
	}

	inline static int32_t get_offset_of_functionName_18() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___functionName_18)); }
	inline String_t* get_functionName_18() const { return ___functionName_18; }
	inline String_t** get_address_of_functionName_18() { return &___functionName_18; }
	inline void set_functionName_18(String_t* value)
	{
		___functionName_18 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_18), value);
	}

	inline static int32_t get_offset_of_startsChecked_19() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___startsChecked_19)); }
	inline bool get_startsChecked_19() const { return ___startsChecked_19; }
	inline bool* get_address_of_startsChecked_19() { return &___startsChecked_19; }
	inline void set_startsChecked_19(bool value)
	{
		___startsChecked_19 = value;
	}

	inline static int32_t get_offset_of_mIsActive_20() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___mIsActive_20)); }
	inline bool get_mIsActive_20() const { return ___mIsActive_20; }
	inline bool* get_address_of_mIsActive_20() { return &___mIsActive_20; }
	inline void set_mIsActive_20(bool value)
	{
		___mIsActive_20 = value;
	}

	inline static int32_t get_offset_of_mStarted_21() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___mStarted_21)); }
	inline bool get_mStarted_21() const { return ___mStarted_21; }
	inline bool* get_address_of_mStarted_21() { return &___mStarted_21; }
	inline void set_mStarted_21(bool value)
	{
		___mStarted_21 = value;
	}
};

struct UIToggle_t4192126258_StaticFields
{
public:
	// BetterList`1<UIToggle> UIToggle::list
	BetterList_1_t3347146576 * ___list_2;
	// UIToggle UIToggle::current
	UIToggle_t4192126258 * ___current_3;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258_StaticFields, ___list_2)); }
	inline BetterList_1_t3347146576 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t3347146576 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t3347146576 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258_StaticFields, ___current_3)); }
	inline UIToggle_t4192126258 * get_current_3() const { return ___current_3; }
	inline UIToggle_t4192126258 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UIToggle_t4192126258 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLE_T4192126258_H
#ifndef UIPANEL_T1716472341_H
#define UIPANEL_T1716472341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel
struct  UIPanel_t1716472341  : public UIRect_t2875960382
{
public:
	// UIPanel/OnGeometryUpdated UIPanel::onGeometryUpdated
	OnGeometryUpdated_t2462438111 * ___onGeometryUpdated_23;
	// System.Boolean UIPanel::showInPanelTool
	bool ___showInPanelTool_24;
	// System.Boolean UIPanel::generateNormals
	bool ___generateNormals_25;
	// System.Boolean UIPanel::generateUV2
	bool ___generateUV2_26;
	// UIDrawCall/ShadowMode UIPanel::shadowMode
	int32_t ___shadowMode_27;
	// System.Boolean UIPanel::widgetsAreStatic
	bool ___widgetsAreStatic_28;
	// System.Boolean UIPanel::cullWhileDragging
	bool ___cullWhileDragging_29;
	// System.Boolean UIPanel::alwaysOnScreen
	bool ___alwaysOnScreen_30;
	// System.Boolean UIPanel::anchorOffset
	bool ___anchorOffset_31;
	// System.Boolean UIPanel::softBorderPadding
	bool ___softBorderPadding_32;
	// UIPanel/RenderQueue UIPanel::renderQueue
	int32_t ___renderQueue_33;
	// System.Int32 UIPanel::startingRenderQueue
	int32_t ___startingRenderQueue_34;
	// System.Collections.Generic.List`1<UIWidget> UIPanel::widgets
	List_1_t715629371 * ___widgets_35;
	// System.Collections.Generic.List`1<UIDrawCall> UIPanel::drawCalls
	List_1_t2765480061 * ___drawCalls_36;
	// UnityEngine.Matrix4x4 UIPanel::worldToLocal
	Matrix4x4_t1817901843  ___worldToLocal_37;
	// UnityEngine.Vector4 UIPanel::drawCallClipRange
	Vector4_t3319028937  ___drawCallClipRange_38;
	// UIPanel/OnClippingMoved UIPanel::onClipMove
	OnClippingMoved_t476625095 * ___onClipMove_39;
	// UIPanel/OnCreateMaterial UIPanel::onCreateMaterial
	OnCreateMaterial_t2961246930 * ___onCreateMaterial_40;
	// UIDrawCall/OnCreateDrawCall UIPanel::onCreateDrawCall
	OnCreateDrawCall_t609469653 * ___onCreateDrawCall_41;
	// UnityEngine.Texture2D UIPanel::mClipTexture
	Texture2D_t3840446185 * ___mClipTexture_42;
	// System.Single UIPanel::mAlpha
	float ___mAlpha_43;
	// UIDrawCall/Clipping UIPanel::mClipping
	int32_t ___mClipping_44;
	// UnityEngine.Vector4 UIPanel::mClipRange
	Vector4_t3319028937  ___mClipRange_45;
	// UnityEngine.Vector2 UIPanel::mClipSoftness
	Vector2_t2156229523  ___mClipSoftness_46;
	// System.Int32 UIPanel::mDepth
	int32_t ___mDepth_47;
	// System.Int32 UIPanel::mSortingOrder
	int32_t ___mSortingOrder_48;
	// System.String UIPanel::mSortingLayerName
	String_t* ___mSortingLayerName_49;
	// System.Boolean UIPanel::mRebuild
	bool ___mRebuild_50;
	// System.Boolean UIPanel::mResized
	bool ___mResized_51;
	// UnityEngine.Vector2 UIPanel::mClipOffset
	Vector2_t2156229523  ___mClipOffset_52;
	// System.Int32 UIPanel::mMatrixFrame
	int32_t ___mMatrixFrame_53;
	// System.Int32 UIPanel::mAlphaFrameID
	int32_t ___mAlphaFrameID_54;
	// System.Int32 UIPanel::mLayer
	int32_t ___mLayer_55;
	// UnityEngine.Vector2 UIPanel::mMin
	Vector2_t2156229523  ___mMin_57;
	// UnityEngine.Vector2 UIPanel::mMax
	Vector2_t2156229523  ___mMax_58;
	// System.Boolean UIPanel::mSortWidgets
	bool ___mSortWidgets_59;
	// System.Boolean UIPanel::mUpdateScroll
	bool ___mUpdateScroll_60;
	// System.Boolean UIPanel::useSortingOrder
	bool ___useSortingOrder_61;
	// UIPanel UIPanel::mParentPanel
	UIPanel_t1716472341 * ___mParentPanel_62;
	// System.Boolean UIPanel::mHasMoved
	bool ___mHasMoved_65;
	// UIDrawCall/OnRenderCallback UIPanel::mOnRender
	OnRenderCallback_t133425655 * ___mOnRender_66;
	// System.Boolean UIPanel::mForced
	bool ___mForced_67;

public:
	inline static int32_t get_offset_of_onGeometryUpdated_23() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onGeometryUpdated_23)); }
	inline OnGeometryUpdated_t2462438111 * get_onGeometryUpdated_23() const { return ___onGeometryUpdated_23; }
	inline OnGeometryUpdated_t2462438111 ** get_address_of_onGeometryUpdated_23() { return &___onGeometryUpdated_23; }
	inline void set_onGeometryUpdated_23(OnGeometryUpdated_t2462438111 * value)
	{
		___onGeometryUpdated_23 = value;
		Il2CppCodeGenWriteBarrier((&___onGeometryUpdated_23), value);
	}

	inline static int32_t get_offset_of_showInPanelTool_24() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___showInPanelTool_24)); }
	inline bool get_showInPanelTool_24() const { return ___showInPanelTool_24; }
	inline bool* get_address_of_showInPanelTool_24() { return &___showInPanelTool_24; }
	inline void set_showInPanelTool_24(bool value)
	{
		___showInPanelTool_24 = value;
	}

	inline static int32_t get_offset_of_generateNormals_25() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___generateNormals_25)); }
	inline bool get_generateNormals_25() const { return ___generateNormals_25; }
	inline bool* get_address_of_generateNormals_25() { return &___generateNormals_25; }
	inline void set_generateNormals_25(bool value)
	{
		___generateNormals_25 = value;
	}

	inline static int32_t get_offset_of_generateUV2_26() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___generateUV2_26)); }
	inline bool get_generateUV2_26() const { return ___generateUV2_26; }
	inline bool* get_address_of_generateUV2_26() { return &___generateUV2_26; }
	inline void set_generateUV2_26(bool value)
	{
		___generateUV2_26 = value;
	}

	inline static int32_t get_offset_of_shadowMode_27() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___shadowMode_27)); }
	inline int32_t get_shadowMode_27() const { return ___shadowMode_27; }
	inline int32_t* get_address_of_shadowMode_27() { return &___shadowMode_27; }
	inline void set_shadowMode_27(int32_t value)
	{
		___shadowMode_27 = value;
	}

	inline static int32_t get_offset_of_widgetsAreStatic_28() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___widgetsAreStatic_28)); }
	inline bool get_widgetsAreStatic_28() const { return ___widgetsAreStatic_28; }
	inline bool* get_address_of_widgetsAreStatic_28() { return &___widgetsAreStatic_28; }
	inline void set_widgetsAreStatic_28(bool value)
	{
		___widgetsAreStatic_28 = value;
	}

	inline static int32_t get_offset_of_cullWhileDragging_29() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___cullWhileDragging_29)); }
	inline bool get_cullWhileDragging_29() const { return ___cullWhileDragging_29; }
	inline bool* get_address_of_cullWhileDragging_29() { return &___cullWhileDragging_29; }
	inline void set_cullWhileDragging_29(bool value)
	{
		___cullWhileDragging_29 = value;
	}

	inline static int32_t get_offset_of_alwaysOnScreen_30() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___alwaysOnScreen_30)); }
	inline bool get_alwaysOnScreen_30() const { return ___alwaysOnScreen_30; }
	inline bool* get_address_of_alwaysOnScreen_30() { return &___alwaysOnScreen_30; }
	inline void set_alwaysOnScreen_30(bool value)
	{
		___alwaysOnScreen_30 = value;
	}

	inline static int32_t get_offset_of_anchorOffset_31() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___anchorOffset_31)); }
	inline bool get_anchorOffset_31() const { return ___anchorOffset_31; }
	inline bool* get_address_of_anchorOffset_31() { return &___anchorOffset_31; }
	inline void set_anchorOffset_31(bool value)
	{
		___anchorOffset_31 = value;
	}

	inline static int32_t get_offset_of_softBorderPadding_32() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___softBorderPadding_32)); }
	inline bool get_softBorderPadding_32() const { return ___softBorderPadding_32; }
	inline bool* get_address_of_softBorderPadding_32() { return &___softBorderPadding_32; }
	inline void set_softBorderPadding_32(bool value)
	{
		___softBorderPadding_32 = value;
	}

	inline static int32_t get_offset_of_renderQueue_33() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___renderQueue_33)); }
	inline int32_t get_renderQueue_33() const { return ___renderQueue_33; }
	inline int32_t* get_address_of_renderQueue_33() { return &___renderQueue_33; }
	inline void set_renderQueue_33(int32_t value)
	{
		___renderQueue_33 = value;
	}

	inline static int32_t get_offset_of_startingRenderQueue_34() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___startingRenderQueue_34)); }
	inline int32_t get_startingRenderQueue_34() const { return ___startingRenderQueue_34; }
	inline int32_t* get_address_of_startingRenderQueue_34() { return &___startingRenderQueue_34; }
	inline void set_startingRenderQueue_34(int32_t value)
	{
		___startingRenderQueue_34 = value;
	}

	inline static int32_t get_offset_of_widgets_35() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___widgets_35)); }
	inline List_1_t715629371 * get_widgets_35() const { return ___widgets_35; }
	inline List_1_t715629371 ** get_address_of_widgets_35() { return &___widgets_35; }
	inline void set_widgets_35(List_1_t715629371 * value)
	{
		___widgets_35 = value;
		Il2CppCodeGenWriteBarrier((&___widgets_35), value);
	}

	inline static int32_t get_offset_of_drawCalls_36() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___drawCalls_36)); }
	inline List_1_t2765480061 * get_drawCalls_36() const { return ___drawCalls_36; }
	inline List_1_t2765480061 ** get_address_of_drawCalls_36() { return &___drawCalls_36; }
	inline void set_drawCalls_36(List_1_t2765480061 * value)
	{
		___drawCalls_36 = value;
		Il2CppCodeGenWriteBarrier((&___drawCalls_36), value);
	}

	inline static int32_t get_offset_of_worldToLocal_37() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___worldToLocal_37)); }
	inline Matrix4x4_t1817901843  get_worldToLocal_37() const { return ___worldToLocal_37; }
	inline Matrix4x4_t1817901843 * get_address_of_worldToLocal_37() { return &___worldToLocal_37; }
	inline void set_worldToLocal_37(Matrix4x4_t1817901843  value)
	{
		___worldToLocal_37 = value;
	}

	inline static int32_t get_offset_of_drawCallClipRange_38() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___drawCallClipRange_38)); }
	inline Vector4_t3319028937  get_drawCallClipRange_38() const { return ___drawCallClipRange_38; }
	inline Vector4_t3319028937 * get_address_of_drawCallClipRange_38() { return &___drawCallClipRange_38; }
	inline void set_drawCallClipRange_38(Vector4_t3319028937  value)
	{
		___drawCallClipRange_38 = value;
	}

	inline static int32_t get_offset_of_onClipMove_39() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onClipMove_39)); }
	inline OnClippingMoved_t476625095 * get_onClipMove_39() const { return ___onClipMove_39; }
	inline OnClippingMoved_t476625095 ** get_address_of_onClipMove_39() { return &___onClipMove_39; }
	inline void set_onClipMove_39(OnClippingMoved_t476625095 * value)
	{
		___onClipMove_39 = value;
		Il2CppCodeGenWriteBarrier((&___onClipMove_39), value);
	}

	inline static int32_t get_offset_of_onCreateMaterial_40() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onCreateMaterial_40)); }
	inline OnCreateMaterial_t2961246930 * get_onCreateMaterial_40() const { return ___onCreateMaterial_40; }
	inline OnCreateMaterial_t2961246930 ** get_address_of_onCreateMaterial_40() { return &___onCreateMaterial_40; }
	inline void set_onCreateMaterial_40(OnCreateMaterial_t2961246930 * value)
	{
		___onCreateMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateMaterial_40), value);
	}

	inline static int32_t get_offset_of_onCreateDrawCall_41() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onCreateDrawCall_41)); }
	inline OnCreateDrawCall_t609469653 * get_onCreateDrawCall_41() const { return ___onCreateDrawCall_41; }
	inline OnCreateDrawCall_t609469653 ** get_address_of_onCreateDrawCall_41() { return &___onCreateDrawCall_41; }
	inline void set_onCreateDrawCall_41(OnCreateDrawCall_t609469653 * value)
	{
		___onCreateDrawCall_41 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateDrawCall_41), value);
	}

	inline static int32_t get_offset_of_mClipTexture_42() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipTexture_42)); }
	inline Texture2D_t3840446185 * get_mClipTexture_42() const { return ___mClipTexture_42; }
	inline Texture2D_t3840446185 ** get_address_of_mClipTexture_42() { return &___mClipTexture_42; }
	inline void set_mClipTexture_42(Texture2D_t3840446185 * value)
	{
		___mClipTexture_42 = value;
		Il2CppCodeGenWriteBarrier((&___mClipTexture_42), value);
	}

	inline static int32_t get_offset_of_mAlpha_43() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mAlpha_43)); }
	inline float get_mAlpha_43() const { return ___mAlpha_43; }
	inline float* get_address_of_mAlpha_43() { return &___mAlpha_43; }
	inline void set_mAlpha_43(float value)
	{
		___mAlpha_43 = value;
	}

	inline static int32_t get_offset_of_mClipping_44() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipping_44)); }
	inline int32_t get_mClipping_44() const { return ___mClipping_44; }
	inline int32_t* get_address_of_mClipping_44() { return &___mClipping_44; }
	inline void set_mClipping_44(int32_t value)
	{
		___mClipping_44 = value;
	}

	inline static int32_t get_offset_of_mClipRange_45() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipRange_45)); }
	inline Vector4_t3319028937  get_mClipRange_45() const { return ___mClipRange_45; }
	inline Vector4_t3319028937 * get_address_of_mClipRange_45() { return &___mClipRange_45; }
	inline void set_mClipRange_45(Vector4_t3319028937  value)
	{
		___mClipRange_45 = value;
	}

	inline static int32_t get_offset_of_mClipSoftness_46() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipSoftness_46)); }
	inline Vector2_t2156229523  get_mClipSoftness_46() const { return ___mClipSoftness_46; }
	inline Vector2_t2156229523 * get_address_of_mClipSoftness_46() { return &___mClipSoftness_46; }
	inline void set_mClipSoftness_46(Vector2_t2156229523  value)
	{
		___mClipSoftness_46 = value;
	}

	inline static int32_t get_offset_of_mDepth_47() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mDepth_47)); }
	inline int32_t get_mDepth_47() const { return ___mDepth_47; }
	inline int32_t* get_address_of_mDepth_47() { return &___mDepth_47; }
	inline void set_mDepth_47(int32_t value)
	{
		___mDepth_47 = value;
	}

	inline static int32_t get_offset_of_mSortingOrder_48() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mSortingOrder_48)); }
	inline int32_t get_mSortingOrder_48() const { return ___mSortingOrder_48; }
	inline int32_t* get_address_of_mSortingOrder_48() { return &___mSortingOrder_48; }
	inline void set_mSortingOrder_48(int32_t value)
	{
		___mSortingOrder_48 = value;
	}

	inline static int32_t get_offset_of_mSortingLayerName_49() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mSortingLayerName_49)); }
	inline String_t* get_mSortingLayerName_49() const { return ___mSortingLayerName_49; }
	inline String_t** get_address_of_mSortingLayerName_49() { return &___mSortingLayerName_49; }
	inline void set_mSortingLayerName_49(String_t* value)
	{
		___mSortingLayerName_49 = value;
		Il2CppCodeGenWriteBarrier((&___mSortingLayerName_49), value);
	}

	inline static int32_t get_offset_of_mRebuild_50() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mRebuild_50)); }
	inline bool get_mRebuild_50() const { return ___mRebuild_50; }
	inline bool* get_address_of_mRebuild_50() { return &___mRebuild_50; }
	inline void set_mRebuild_50(bool value)
	{
		___mRebuild_50 = value;
	}

	inline static int32_t get_offset_of_mResized_51() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mResized_51)); }
	inline bool get_mResized_51() const { return ___mResized_51; }
	inline bool* get_address_of_mResized_51() { return &___mResized_51; }
	inline void set_mResized_51(bool value)
	{
		___mResized_51 = value;
	}

	inline static int32_t get_offset_of_mClipOffset_52() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipOffset_52)); }
	inline Vector2_t2156229523  get_mClipOffset_52() const { return ___mClipOffset_52; }
	inline Vector2_t2156229523 * get_address_of_mClipOffset_52() { return &___mClipOffset_52; }
	inline void set_mClipOffset_52(Vector2_t2156229523  value)
	{
		___mClipOffset_52 = value;
	}

	inline static int32_t get_offset_of_mMatrixFrame_53() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mMatrixFrame_53)); }
	inline int32_t get_mMatrixFrame_53() const { return ___mMatrixFrame_53; }
	inline int32_t* get_address_of_mMatrixFrame_53() { return &___mMatrixFrame_53; }
	inline void set_mMatrixFrame_53(int32_t value)
	{
		___mMatrixFrame_53 = value;
	}

	inline static int32_t get_offset_of_mAlphaFrameID_54() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mAlphaFrameID_54)); }
	inline int32_t get_mAlphaFrameID_54() const { return ___mAlphaFrameID_54; }
	inline int32_t* get_address_of_mAlphaFrameID_54() { return &___mAlphaFrameID_54; }
	inline void set_mAlphaFrameID_54(int32_t value)
	{
		___mAlphaFrameID_54 = value;
	}

	inline static int32_t get_offset_of_mLayer_55() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mLayer_55)); }
	inline int32_t get_mLayer_55() const { return ___mLayer_55; }
	inline int32_t* get_address_of_mLayer_55() { return &___mLayer_55; }
	inline void set_mLayer_55(int32_t value)
	{
		___mLayer_55 = value;
	}

	inline static int32_t get_offset_of_mMin_57() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mMin_57)); }
	inline Vector2_t2156229523  get_mMin_57() const { return ___mMin_57; }
	inline Vector2_t2156229523 * get_address_of_mMin_57() { return &___mMin_57; }
	inline void set_mMin_57(Vector2_t2156229523  value)
	{
		___mMin_57 = value;
	}

	inline static int32_t get_offset_of_mMax_58() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mMax_58)); }
	inline Vector2_t2156229523  get_mMax_58() const { return ___mMax_58; }
	inline Vector2_t2156229523 * get_address_of_mMax_58() { return &___mMax_58; }
	inline void set_mMax_58(Vector2_t2156229523  value)
	{
		___mMax_58 = value;
	}

	inline static int32_t get_offset_of_mSortWidgets_59() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mSortWidgets_59)); }
	inline bool get_mSortWidgets_59() const { return ___mSortWidgets_59; }
	inline bool* get_address_of_mSortWidgets_59() { return &___mSortWidgets_59; }
	inline void set_mSortWidgets_59(bool value)
	{
		___mSortWidgets_59 = value;
	}

	inline static int32_t get_offset_of_mUpdateScroll_60() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mUpdateScroll_60)); }
	inline bool get_mUpdateScroll_60() const { return ___mUpdateScroll_60; }
	inline bool* get_address_of_mUpdateScroll_60() { return &___mUpdateScroll_60; }
	inline void set_mUpdateScroll_60(bool value)
	{
		___mUpdateScroll_60 = value;
	}

	inline static int32_t get_offset_of_useSortingOrder_61() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___useSortingOrder_61)); }
	inline bool get_useSortingOrder_61() const { return ___useSortingOrder_61; }
	inline bool* get_address_of_useSortingOrder_61() { return &___useSortingOrder_61; }
	inline void set_useSortingOrder_61(bool value)
	{
		___useSortingOrder_61 = value;
	}

	inline static int32_t get_offset_of_mParentPanel_62() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mParentPanel_62)); }
	inline UIPanel_t1716472341 * get_mParentPanel_62() const { return ___mParentPanel_62; }
	inline UIPanel_t1716472341 ** get_address_of_mParentPanel_62() { return &___mParentPanel_62; }
	inline void set_mParentPanel_62(UIPanel_t1716472341 * value)
	{
		___mParentPanel_62 = value;
		Il2CppCodeGenWriteBarrier((&___mParentPanel_62), value);
	}

	inline static int32_t get_offset_of_mHasMoved_65() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mHasMoved_65)); }
	inline bool get_mHasMoved_65() const { return ___mHasMoved_65; }
	inline bool* get_address_of_mHasMoved_65() { return &___mHasMoved_65; }
	inline void set_mHasMoved_65(bool value)
	{
		___mHasMoved_65 = value;
	}

	inline static int32_t get_offset_of_mOnRender_66() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mOnRender_66)); }
	inline OnRenderCallback_t133425655 * get_mOnRender_66() const { return ___mOnRender_66; }
	inline OnRenderCallback_t133425655 ** get_address_of_mOnRender_66() { return &___mOnRender_66; }
	inline void set_mOnRender_66(OnRenderCallback_t133425655 * value)
	{
		___mOnRender_66 = value;
		Il2CppCodeGenWriteBarrier((&___mOnRender_66), value);
	}

	inline static int32_t get_offset_of_mForced_67() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mForced_67)); }
	inline bool get_mForced_67() const { return ___mForced_67; }
	inline bool* get_address_of_mForced_67() { return &___mForced_67; }
	inline void set_mForced_67(bool value)
	{
		___mForced_67 = value;
	}
};

struct UIPanel_t1716472341_StaticFields
{
public:
	// System.Collections.Generic.List`1<UIPanel> UIPanel::list
	List_1_t3188547083 * ___list_22;
	// System.Single[] UIPanel::mTemp
	SingleU5BU5D_t1444911251* ___mTemp_56;
	// UnityEngine.Vector3[] UIPanel::mCorners
	Vector3U5BU5D_t1718750761* ___mCorners_63;
	// System.Int32 UIPanel::mUpdateFrame
	int32_t ___mUpdateFrame_64;
	// System.Comparison`1<UIPanel> UIPanel::<>f__mg$cache0
	Comparison_1_t1491403520 * ___U3CU3Ef__mgU24cache0_68;
	// System.Comparison`1<UIPanel> UIPanel::<>f__mg$cache1
	Comparison_1_t1491403520 * ___U3CU3Ef__mgU24cache1_69;
	// System.Comparison`1<UIWidget> UIPanel::<>f__mg$cache2
	Comparison_1_t3313453104 * ___U3CU3Ef__mgU24cache2_70;

public:
	inline static int32_t get_offset_of_list_22() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___list_22)); }
	inline List_1_t3188547083 * get_list_22() const { return ___list_22; }
	inline List_1_t3188547083 ** get_address_of_list_22() { return &___list_22; }
	inline void set_list_22(List_1_t3188547083 * value)
	{
		___list_22 = value;
		Il2CppCodeGenWriteBarrier((&___list_22), value);
	}

	inline static int32_t get_offset_of_mTemp_56() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___mTemp_56)); }
	inline SingleU5BU5D_t1444911251* get_mTemp_56() const { return ___mTemp_56; }
	inline SingleU5BU5D_t1444911251** get_address_of_mTemp_56() { return &___mTemp_56; }
	inline void set_mTemp_56(SingleU5BU5D_t1444911251* value)
	{
		___mTemp_56 = value;
		Il2CppCodeGenWriteBarrier((&___mTemp_56), value);
	}

	inline static int32_t get_offset_of_mCorners_63() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___mCorners_63)); }
	inline Vector3U5BU5D_t1718750761* get_mCorners_63() const { return ___mCorners_63; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mCorners_63() { return &___mCorners_63; }
	inline void set_mCorners_63(Vector3U5BU5D_t1718750761* value)
	{
		___mCorners_63 = value;
		Il2CppCodeGenWriteBarrier((&___mCorners_63), value);
	}

	inline static int32_t get_offset_of_mUpdateFrame_64() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___mUpdateFrame_64)); }
	inline int32_t get_mUpdateFrame_64() const { return ___mUpdateFrame_64; }
	inline int32_t* get_address_of_mUpdateFrame_64() { return &___mUpdateFrame_64; }
	inline void set_mUpdateFrame_64(int32_t value)
	{
		___mUpdateFrame_64 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_68() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___U3CU3Ef__mgU24cache0_68)); }
	inline Comparison_1_t1491403520 * get_U3CU3Ef__mgU24cache0_68() const { return ___U3CU3Ef__mgU24cache0_68; }
	inline Comparison_1_t1491403520 ** get_address_of_U3CU3Ef__mgU24cache0_68() { return &___U3CU3Ef__mgU24cache0_68; }
	inline void set_U3CU3Ef__mgU24cache0_68(Comparison_1_t1491403520 * value)
	{
		___U3CU3Ef__mgU24cache0_68 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_68), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_69() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___U3CU3Ef__mgU24cache1_69)); }
	inline Comparison_1_t1491403520 * get_U3CU3Ef__mgU24cache1_69() const { return ___U3CU3Ef__mgU24cache1_69; }
	inline Comparison_1_t1491403520 ** get_address_of_U3CU3Ef__mgU24cache1_69() { return &___U3CU3Ef__mgU24cache1_69; }
	inline void set_U3CU3Ef__mgU24cache1_69(Comparison_1_t1491403520 * value)
	{
		___U3CU3Ef__mgU24cache1_69 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_69), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_70() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___U3CU3Ef__mgU24cache2_70)); }
	inline Comparison_1_t3313453104 * get_U3CU3Ef__mgU24cache2_70() const { return ___U3CU3Ef__mgU24cache2_70; }
	inline Comparison_1_t3313453104 ** get_address_of_U3CU3Ef__mgU24cache2_70() { return &___U3CU3Ef__mgU24cache2_70; }
	inline void set_U3CU3Ef__mgU24cache2_70(Comparison_1_t3313453104 * value)
	{
		___U3CU3Ef__mgU24cache2_70 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_70), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPANEL_T1716472341_H
#ifndef TWEENALPHA_T3706845226_H
#define TWEENALPHA_T3706845226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAlpha
struct  TweenAlpha_t3706845226  : public UITweener_t260334902
{
public:
	// System.Single TweenAlpha::from
	float ___from_22;
	// System.Single TweenAlpha::to
	float ___to_23;
	// System.Boolean TweenAlpha::mCached
	bool ___mCached_24;
	// UIRect TweenAlpha::mRect
	UIRect_t2875960382 * ___mRect_25;
	// UnityEngine.Material TweenAlpha::mMat
	Material_t340375123 * ___mMat_26;
	// UnityEngine.Light TweenAlpha::mLight
	Light_t3756812086 * ___mLight_27;
	// UnityEngine.SpriteRenderer TweenAlpha::mSr
	SpriteRenderer_t3235626157 * ___mSr_28;
	// System.Single TweenAlpha::mBaseIntensity
	float ___mBaseIntensity_29;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___from_22)); }
	inline float get_from_22() const { return ___from_22; }
	inline float* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(float value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___to_23)); }
	inline float get_to_23() const { return ___to_23; }
	inline float* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(float value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_mCached_24() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mCached_24)); }
	inline bool get_mCached_24() const { return ___mCached_24; }
	inline bool* get_address_of_mCached_24() { return &___mCached_24; }
	inline void set_mCached_24(bool value)
	{
		___mCached_24 = value;
	}

	inline static int32_t get_offset_of_mRect_25() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mRect_25)); }
	inline UIRect_t2875960382 * get_mRect_25() const { return ___mRect_25; }
	inline UIRect_t2875960382 ** get_address_of_mRect_25() { return &___mRect_25; }
	inline void set_mRect_25(UIRect_t2875960382 * value)
	{
		___mRect_25 = value;
		Il2CppCodeGenWriteBarrier((&___mRect_25), value);
	}

	inline static int32_t get_offset_of_mMat_26() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mMat_26)); }
	inline Material_t340375123 * get_mMat_26() const { return ___mMat_26; }
	inline Material_t340375123 ** get_address_of_mMat_26() { return &___mMat_26; }
	inline void set_mMat_26(Material_t340375123 * value)
	{
		___mMat_26 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_26), value);
	}

	inline static int32_t get_offset_of_mLight_27() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mLight_27)); }
	inline Light_t3756812086 * get_mLight_27() const { return ___mLight_27; }
	inline Light_t3756812086 ** get_address_of_mLight_27() { return &___mLight_27; }
	inline void set_mLight_27(Light_t3756812086 * value)
	{
		___mLight_27 = value;
		Il2CppCodeGenWriteBarrier((&___mLight_27), value);
	}

	inline static int32_t get_offset_of_mSr_28() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mSr_28)); }
	inline SpriteRenderer_t3235626157 * get_mSr_28() const { return ___mSr_28; }
	inline SpriteRenderer_t3235626157 ** get_address_of_mSr_28() { return &___mSr_28; }
	inline void set_mSr_28(SpriteRenderer_t3235626157 * value)
	{
		___mSr_28 = value;
		Il2CppCodeGenWriteBarrier((&___mSr_28), value);
	}

	inline static int32_t get_offset_of_mBaseIntensity_29() { return static_cast<int32_t>(offsetof(TweenAlpha_t3706845226, ___mBaseIntensity_29)); }
	inline float get_mBaseIntensity_29() const { return ___mBaseIntensity_29; }
	inline float* get_address_of_mBaseIntensity_29() { return &___mBaseIntensity_29; }
	inline void set_mBaseIntensity_29(float value)
	{
		___mBaseIntensity_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENALPHA_T3706845226_H
#ifndef UIWIDGET_T3538521925_H
#define UIWIDGET_T3538521925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget
struct  UIWidget_t3538521925  : public UIRect_t2875960382
{
public:
	// UnityEngine.Color UIWidget::mColor
	Color_t2555686324  ___mColor_22;
	// UIWidget/Pivot UIWidget::mPivot
	int32_t ___mPivot_23;
	// System.Int32 UIWidget::mWidth
	int32_t ___mWidth_24;
	// System.Int32 UIWidget::mHeight
	int32_t ___mHeight_25;
	// System.Int32 UIWidget::mDepth
	int32_t ___mDepth_26;
	// UnityEngine.Material UIWidget::mMat
	Material_t340375123 * ___mMat_27;
	// UIWidget/OnDimensionsChanged UIWidget::onChange
	OnDimensionsChanged_t3101921181 * ___onChange_28;
	// UIWidget/OnPostFillCallback UIWidget::onPostFill
	OnPostFillCallback_t2835645043 * ___onPostFill_29;
	// UIDrawCall/OnRenderCallback UIWidget::mOnRender
	OnRenderCallback_t133425655 * ___mOnRender_30;
	// System.Boolean UIWidget::autoResizeBoxCollider
	bool ___autoResizeBoxCollider_31;
	// System.Boolean UIWidget::hideIfOffScreen
	bool ___hideIfOffScreen_32;
	// UIWidget/AspectRatioSource UIWidget::keepAspectRatio
	int32_t ___keepAspectRatio_33;
	// System.Single UIWidget::aspectRatio
	float ___aspectRatio_34;
	// UIWidget/HitCheck UIWidget::hitCheck
	HitCheck_t2300079615 * ___hitCheck_35;
	// UIPanel UIWidget::panel
	UIPanel_t1716472341 * ___panel_36;
	// UIGeometry UIWidget::geometry
	UIGeometry_t1059483952 * ___geometry_37;
	// System.Boolean UIWidget::fillGeometry
	bool ___fillGeometry_38;
	// System.Boolean UIWidget::mPlayMode
	bool ___mPlayMode_39;
	// UnityEngine.Vector4 UIWidget::mDrawRegion
	Vector4_t3319028937  ___mDrawRegion_40;
	// UnityEngine.Matrix4x4 UIWidget::mLocalToPanel
	Matrix4x4_t1817901843  ___mLocalToPanel_41;
	// System.Boolean UIWidget::mIsVisibleByAlpha
	bool ___mIsVisibleByAlpha_42;
	// System.Boolean UIWidget::mIsVisibleByPanel
	bool ___mIsVisibleByPanel_43;
	// System.Boolean UIWidget::mIsInFront
	bool ___mIsInFront_44;
	// System.Single UIWidget::mLastAlpha
	float ___mLastAlpha_45;
	// System.Boolean UIWidget::mMoved
	bool ___mMoved_46;
	// UIDrawCall UIWidget::drawCall
	UIDrawCall_t1293405319 * ___drawCall_47;
	// UnityEngine.Vector3[] UIWidget::mCorners
	Vector3U5BU5D_t1718750761* ___mCorners_48;
	// System.Int32 UIWidget::mAlphaFrameID
	int32_t ___mAlphaFrameID_49;
	// System.Int32 UIWidget::mMatrixFrame
	int32_t ___mMatrixFrame_50;
	// UnityEngine.Vector3 UIWidget::mOldV0
	Vector3_t3722313464  ___mOldV0_51;
	// UnityEngine.Vector3 UIWidget::mOldV1
	Vector3_t3722313464  ___mOldV1_52;

public:
	inline static int32_t get_offset_of_mColor_22() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mColor_22)); }
	inline Color_t2555686324  get_mColor_22() const { return ___mColor_22; }
	inline Color_t2555686324 * get_address_of_mColor_22() { return &___mColor_22; }
	inline void set_mColor_22(Color_t2555686324  value)
	{
		___mColor_22 = value;
	}

	inline static int32_t get_offset_of_mPivot_23() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mPivot_23)); }
	inline int32_t get_mPivot_23() const { return ___mPivot_23; }
	inline int32_t* get_address_of_mPivot_23() { return &___mPivot_23; }
	inline void set_mPivot_23(int32_t value)
	{
		___mPivot_23 = value;
	}

	inline static int32_t get_offset_of_mWidth_24() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mWidth_24)); }
	inline int32_t get_mWidth_24() const { return ___mWidth_24; }
	inline int32_t* get_address_of_mWidth_24() { return &___mWidth_24; }
	inline void set_mWidth_24(int32_t value)
	{
		___mWidth_24 = value;
	}

	inline static int32_t get_offset_of_mHeight_25() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mHeight_25)); }
	inline int32_t get_mHeight_25() const { return ___mHeight_25; }
	inline int32_t* get_address_of_mHeight_25() { return &___mHeight_25; }
	inline void set_mHeight_25(int32_t value)
	{
		___mHeight_25 = value;
	}

	inline static int32_t get_offset_of_mDepth_26() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mDepth_26)); }
	inline int32_t get_mDepth_26() const { return ___mDepth_26; }
	inline int32_t* get_address_of_mDepth_26() { return &___mDepth_26; }
	inline void set_mDepth_26(int32_t value)
	{
		___mDepth_26 = value;
	}

	inline static int32_t get_offset_of_mMat_27() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMat_27)); }
	inline Material_t340375123 * get_mMat_27() const { return ___mMat_27; }
	inline Material_t340375123 ** get_address_of_mMat_27() { return &___mMat_27; }
	inline void set_mMat_27(Material_t340375123 * value)
	{
		___mMat_27 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_27), value);
	}

	inline static int32_t get_offset_of_onChange_28() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___onChange_28)); }
	inline OnDimensionsChanged_t3101921181 * get_onChange_28() const { return ___onChange_28; }
	inline OnDimensionsChanged_t3101921181 ** get_address_of_onChange_28() { return &___onChange_28; }
	inline void set_onChange_28(OnDimensionsChanged_t3101921181 * value)
	{
		___onChange_28 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_28), value);
	}

	inline static int32_t get_offset_of_onPostFill_29() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___onPostFill_29)); }
	inline OnPostFillCallback_t2835645043 * get_onPostFill_29() const { return ___onPostFill_29; }
	inline OnPostFillCallback_t2835645043 ** get_address_of_onPostFill_29() { return &___onPostFill_29; }
	inline void set_onPostFill_29(OnPostFillCallback_t2835645043 * value)
	{
		___onPostFill_29 = value;
		Il2CppCodeGenWriteBarrier((&___onPostFill_29), value);
	}

	inline static int32_t get_offset_of_mOnRender_30() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOnRender_30)); }
	inline OnRenderCallback_t133425655 * get_mOnRender_30() const { return ___mOnRender_30; }
	inline OnRenderCallback_t133425655 ** get_address_of_mOnRender_30() { return &___mOnRender_30; }
	inline void set_mOnRender_30(OnRenderCallback_t133425655 * value)
	{
		___mOnRender_30 = value;
		Il2CppCodeGenWriteBarrier((&___mOnRender_30), value);
	}

	inline static int32_t get_offset_of_autoResizeBoxCollider_31() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___autoResizeBoxCollider_31)); }
	inline bool get_autoResizeBoxCollider_31() const { return ___autoResizeBoxCollider_31; }
	inline bool* get_address_of_autoResizeBoxCollider_31() { return &___autoResizeBoxCollider_31; }
	inline void set_autoResizeBoxCollider_31(bool value)
	{
		___autoResizeBoxCollider_31 = value;
	}

	inline static int32_t get_offset_of_hideIfOffScreen_32() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___hideIfOffScreen_32)); }
	inline bool get_hideIfOffScreen_32() const { return ___hideIfOffScreen_32; }
	inline bool* get_address_of_hideIfOffScreen_32() { return &___hideIfOffScreen_32; }
	inline void set_hideIfOffScreen_32(bool value)
	{
		___hideIfOffScreen_32 = value;
	}

	inline static int32_t get_offset_of_keepAspectRatio_33() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___keepAspectRatio_33)); }
	inline int32_t get_keepAspectRatio_33() const { return ___keepAspectRatio_33; }
	inline int32_t* get_address_of_keepAspectRatio_33() { return &___keepAspectRatio_33; }
	inline void set_keepAspectRatio_33(int32_t value)
	{
		___keepAspectRatio_33 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_34() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___aspectRatio_34)); }
	inline float get_aspectRatio_34() const { return ___aspectRatio_34; }
	inline float* get_address_of_aspectRatio_34() { return &___aspectRatio_34; }
	inline void set_aspectRatio_34(float value)
	{
		___aspectRatio_34 = value;
	}

	inline static int32_t get_offset_of_hitCheck_35() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___hitCheck_35)); }
	inline HitCheck_t2300079615 * get_hitCheck_35() const { return ___hitCheck_35; }
	inline HitCheck_t2300079615 ** get_address_of_hitCheck_35() { return &___hitCheck_35; }
	inline void set_hitCheck_35(HitCheck_t2300079615 * value)
	{
		___hitCheck_35 = value;
		Il2CppCodeGenWriteBarrier((&___hitCheck_35), value);
	}

	inline static int32_t get_offset_of_panel_36() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___panel_36)); }
	inline UIPanel_t1716472341 * get_panel_36() const { return ___panel_36; }
	inline UIPanel_t1716472341 ** get_address_of_panel_36() { return &___panel_36; }
	inline void set_panel_36(UIPanel_t1716472341 * value)
	{
		___panel_36 = value;
		Il2CppCodeGenWriteBarrier((&___panel_36), value);
	}

	inline static int32_t get_offset_of_geometry_37() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___geometry_37)); }
	inline UIGeometry_t1059483952 * get_geometry_37() const { return ___geometry_37; }
	inline UIGeometry_t1059483952 ** get_address_of_geometry_37() { return &___geometry_37; }
	inline void set_geometry_37(UIGeometry_t1059483952 * value)
	{
		___geometry_37 = value;
		Il2CppCodeGenWriteBarrier((&___geometry_37), value);
	}

	inline static int32_t get_offset_of_fillGeometry_38() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___fillGeometry_38)); }
	inline bool get_fillGeometry_38() const { return ___fillGeometry_38; }
	inline bool* get_address_of_fillGeometry_38() { return &___fillGeometry_38; }
	inline void set_fillGeometry_38(bool value)
	{
		___fillGeometry_38 = value;
	}

	inline static int32_t get_offset_of_mPlayMode_39() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mPlayMode_39)); }
	inline bool get_mPlayMode_39() const { return ___mPlayMode_39; }
	inline bool* get_address_of_mPlayMode_39() { return &___mPlayMode_39; }
	inline void set_mPlayMode_39(bool value)
	{
		___mPlayMode_39 = value;
	}

	inline static int32_t get_offset_of_mDrawRegion_40() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mDrawRegion_40)); }
	inline Vector4_t3319028937  get_mDrawRegion_40() const { return ___mDrawRegion_40; }
	inline Vector4_t3319028937 * get_address_of_mDrawRegion_40() { return &___mDrawRegion_40; }
	inline void set_mDrawRegion_40(Vector4_t3319028937  value)
	{
		___mDrawRegion_40 = value;
	}

	inline static int32_t get_offset_of_mLocalToPanel_41() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mLocalToPanel_41)); }
	inline Matrix4x4_t1817901843  get_mLocalToPanel_41() const { return ___mLocalToPanel_41; }
	inline Matrix4x4_t1817901843 * get_address_of_mLocalToPanel_41() { return &___mLocalToPanel_41; }
	inline void set_mLocalToPanel_41(Matrix4x4_t1817901843  value)
	{
		___mLocalToPanel_41 = value;
	}

	inline static int32_t get_offset_of_mIsVisibleByAlpha_42() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsVisibleByAlpha_42)); }
	inline bool get_mIsVisibleByAlpha_42() const { return ___mIsVisibleByAlpha_42; }
	inline bool* get_address_of_mIsVisibleByAlpha_42() { return &___mIsVisibleByAlpha_42; }
	inline void set_mIsVisibleByAlpha_42(bool value)
	{
		___mIsVisibleByAlpha_42 = value;
	}

	inline static int32_t get_offset_of_mIsVisibleByPanel_43() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsVisibleByPanel_43)); }
	inline bool get_mIsVisibleByPanel_43() const { return ___mIsVisibleByPanel_43; }
	inline bool* get_address_of_mIsVisibleByPanel_43() { return &___mIsVisibleByPanel_43; }
	inline void set_mIsVisibleByPanel_43(bool value)
	{
		___mIsVisibleByPanel_43 = value;
	}

	inline static int32_t get_offset_of_mIsInFront_44() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsInFront_44)); }
	inline bool get_mIsInFront_44() const { return ___mIsInFront_44; }
	inline bool* get_address_of_mIsInFront_44() { return &___mIsInFront_44; }
	inline void set_mIsInFront_44(bool value)
	{
		___mIsInFront_44 = value;
	}

	inline static int32_t get_offset_of_mLastAlpha_45() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mLastAlpha_45)); }
	inline float get_mLastAlpha_45() const { return ___mLastAlpha_45; }
	inline float* get_address_of_mLastAlpha_45() { return &___mLastAlpha_45; }
	inline void set_mLastAlpha_45(float value)
	{
		___mLastAlpha_45 = value;
	}

	inline static int32_t get_offset_of_mMoved_46() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMoved_46)); }
	inline bool get_mMoved_46() const { return ___mMoved_46; }
	inline bool* get_address_of_mMoved_46() { return &___mMoved_46; }
	inline void set_mMoved_46(bool value)
	{
		___mMoved_46 = value;
	}

	inline static int32_t get_offset_of_drawCall_47() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___drawCall_47)); }
	inline UIDrawCall_t1293405319 * get_drawCall_47() const { return ___drawCall_47; }
	inline UIDrawCall_t1293405319 ** get_address_of_drawCall_47() { return &___drawCall_47; }
	inline void set_drawCall_47(UIDrawCall_t1293405319 * value)
	{
		___drawCall_47 = value;
		Il2CppCodeGenWriteBarrier((&___drawCall_47), value);
	}

	inline static int32_t get_offset_of_mCorners_48() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mCorners_48)); }
	inline Vector3U5BU5D_t1718750761* get_mCorners_48() const { return ___mCorners_48; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mCorners_48() { return &___mCorners_48; }
	inline void set_mCorners_48(Vector3U5BU5D_t1718750761* value)
	{
		___mCorners_48 = value;
		Il2CppCodeGenWriteBarrier((&___mCorners_48), value);
	}

	inline static int32_t get_offset_of_mAlphaFrameID_49() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mAlphaFrameID_49)); }
	inline int32_t get_mAlphaFrameID_49() const { return ___mAlphaFrameID_49; }
	inline int32_t* get_address_of_mAlphaFrameID_49() { return &___mAlphaFrameID_49; }
	inline void set_mAlphaFrameID_49(int32_t value)
	{
		___mAlphaFrameID_49 = value;
	}

	inline static int32_t get_offset_of_mMatrixFrame_50() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMatrixFrame_50)); }
	inline int32_t get_mMatrixFrame_50() const { return ___mMatrixFrame_50; }
	inline int32_t* get_address_of_mMatrixFrame_50() { return &___mMatrixFrame_50; }
	inline void set_mMatrixFrame_50(int32_t value)
	{
		___mMatrixFrame_50 = value;
	}

	inline static int32_t get_offset_of_mOldV0_51() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOldV0_51)); }
	inline Vector3_t3722313464  get_mOldV0_51() const { return ___mOldV0_51; }
	inline Vector3_t3722313464 * get_address_of_mOldV0_51() { return &___mOldV0_51; }
	inline void set_mOldV0_51(Vector3_t3722313464  value)
	{
		___mOldV0_51 = value;
	}

	inline static int32_t get_offset_of_mOldV1_52() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOldV1_52)); }
	inline Vector3_t3722313464  get_mOldV1_52() const { return ___mOldV1_52; }
	inline Vector3_t3722313464 * get_address_of_mOldV1_52() { return &___mOldV1_52; }
	inline void set_mOldV1_52(Vector3_t3722313464  value)
	{
		___mOldV1_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWIDGET_T3538521925_H
#ifndef UIGRID_T1536638187_H
#define UIGRID_T1536638187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid
struct  UIGrid_t1536638187  : public UIWidgetContainer_t30162560
{
public:
	// UIGrid/Arrangement UIGrid::arrangement
	int32_t ___arrangement_2;
	// UIGrid/Sorting UIGrid::sorting
	int32_t ___sorting_3;
	// UIWidget/Pivot UIGrid::pivot
	int32_t ___pivot_4;
	// System.Int32 UIGrid::maxPerLine
	int32_t ___maxPerLine_5;
	// System.Single UIGrid::cellWidth
	float ___cellWidth_6;
	// System.Single UIGrid::cellHeight
	float ___cellHeight_7;
	// System.Boolean UIGrid::animateSmoothly
	bool ___animateSmoothly_8;
	// System.Boolean UIGrid::hideInactive
	bool ___hideInactive_9;
	// System.Boolean UIGrid::keepWithinPanel
	bool ___keepWithinPanel_10;
	// UIGrid/OnReposition UIGrid::onReposition
	OnReposition_t1372889220 * ___onReposition_11;
	// System.Comparison`1<UnityEngine.Transform> UIGrid::onCustomSort
	Comparison_1_t3375297100 * ___onCustomSort_12;
	// System.Boolean UIGrid::sorted
	bool ___sorted_13;
	// System.Boolean UIGrid::mReposition
	bool ___mReposition_14;
	// UIPanel UIGrid::mPanel
	UIPanel_t1716472341 * ___mPanel_15;
	// System.Boolean UIGrid::mInitDone
	bool ___mInitDone_16;

public:
	inline static int32_t get_offset_of_arrangement_2() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___arrangement_2)); }
	inline int32_t get_arrangement_2() const { return ___arrangement_2; }
	inline int32_t* get_address_of_arrangement_2() { return &___arrangement_2; }
	inline void set_arrangement_2(int32_t value)
	{
		___arrangement_2 = value;
	}

	inline static int32_t get_offset_of_sorting_3() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___sorting_3)); }
	inline int32_t get_sorting_3() const { return ___sorting_3; }
	inline int32_t* get_address_of_sorting_3() { return &___sorting_3; }
	inline void set_sorting_3(int32_t value)
	{
		___sorting_3 = value;
	}

	inline static int32_t get_offset_of_pivot_4() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___pivot_4)); }
	inline int32_t get_pivot_4() const { return ___pivot_4; }
	inline int32_t* get_address_of_pivot_4() { return &___pivot_4; }
	inline void set_pivot_4(int32_t value)
	{
		___pivot_4 = value;
	}

	inline static int32_t get_offset_of_maxPerLine_5() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___maxPerLine_5)); }
	inline int32_t get_maxPerLine_5() const { return ___maxPerLine_5; }
	inline int32_t* get_address_of_maxPerLine_5() { return &___maxPerLine_5; }
	inline void set_maxPerLine_5(int32_t value)
	{
		___maxPerLine_5 = value;
	}

	inline static int32_t get_offset_of_cellWidth_6() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___cellWidth_6)); }
	inline float get_cellWidth_6() const { return ___cellWidth_6; }
	inline float* get_address_of_cellWidth_6() { return &___cellWidth_6; }
	inline void set_cellWidth_6(float value)
	{
		___cellWidth_6 = value;
	}

	inline static int32_t get_offset_of_cellHeight_7() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___cellHeight_7)); }
	inline float get_cellHeight_7() const { return ___cellHeight_7; }
	inline float* get_address_of_cellHeight_7() { return &___cellHeight_7; }
	inline void set_cellHeight_7(float value)
	{
		___cellHeight_7 = value;
	}

	inline static int32_t get_offset_of_animateSmoothly_8() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___animateSmoothly_8)); }
	inline bool get_animateSmoothly_8() const { return ___animateSmoothly_8; }
	inline bool* get_address_of_animateSmoothly_8() { return &___animateSmoothly_8; }
	inline void set_animateSmoothly_8(bool value)
	{
		___animateSmoothly_8 = value;
	}

	inline static int32_t get_offset_of_hideInactive_9() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___hideInactive_9)); }
	inline bool get_hideInactive_9() const { return ___hideInactive_9; }
	inline bool* get_address_of_hideInactive_9() { return &___hideInactive_9; }
	inline void set_hideInactive_9(bool value)
	{
		___hideInactive_9 = value;
	}

	inline static int32_t get_offset_of_keepWithinPanel_10() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___keepWithinPanel_10)); }
	inline bool get_keepWithinPanel_10() const { return ___keepWithinPanel_10; }
	inline bool* get_address_of_keepWithinPanel_10() { return &___keepWithinPanel_10; }
	inline void set_keepWithinPanel_10(bool value)
	{
		___keepWithinPanel_10 = value;
	}

	inline static int32_t get_offset_of_onReposition_11() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___onReposition_11)); }
	inline OnReposition_t1372889220 * get_onReposition_11() const { return ___onReposition_11; }
	inline OnReposition_t1372889220 ** get_address_of_onReposition_11() { return &___onReposition_11; }
	inline void set_onReposition_11(OnReposition_t1372889220 * value)
	{
		___onReposition_11 = value;
		Il2CppCodeGenWriteBarrier((&___onReposition_11), value);
	}

	inline static int32_t get_offset_of_onCustomSort_12() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___onCustomSort_12)); }
	inline Comparison_1_t3375297100 * get_onCustomSort_12() const { return ___onCustomSort_12; }
	inline Comparison_1_t3375297100 ** get_address_of_onCustomSort_12() { return &___onCustomSort_12; }
	inline void set_onCustomSort_12(Comparison_1_t3375297100 * value)
	{
		___onCustomSort_12 = value;
		Il2CppCodeGenWriteBarrier((&___onCustomSort_12), value);
	}

	inline static int32_t get_offset_of_sorted_13() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___sorted_13)); }
	inline bool get_sorted_13() const { return ___sorted_13; }
	inline bool* get_address_of_sorted_13() { return &___sorted_13; }
	inline void set_sorted_13(bool value)
	{
		___sorted_13 = value;
	}

	inline static int32_t get_offset_of_mReposition_14() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___mReposition_14)); }
	inline bool get_mReposition_14() const { return ___mReposition_14; }
	inline bool* get_address_of_mReposition_14() { return &___mReposition_14; }
	inline void set_mReposition_14(bool value)
	{
		___mReposition_14 = value;
	}

	inline static int32_t get_offset_of_mPanel_15() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___mPanel_15)); }
	inline UIPanel_t1716472341 * get_mPanel_15() const { return ___mPanel_15; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_15() { return &___mPanel_15; }
	inline void set_mPanel_15(UIPanel_t1716472341 * value)
	{
		___mPanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_15), value);
	}

	inline static int32_t get_offset_of_mInitDone_16() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___mInitDone_16)); }
	inline bool get_mInitDone_16() const { return ___mInitDone_16; }
	inline bool* get_address_of_mInitDone_16() { return &___mInitDone_16; }
	inline void set_mInitDone_16(bool value)
	{
		___mInitDone_16 = value;
	}
};

struct UIGrid_t1536638187_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.Transform> UIGrid::<>f__mg$cache0
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache0_17;
	// System.Comparison`1<UnityEngine.Transform> UIGrid::<>f__mg$cache1
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache1_18;
	// System.Comparison`1<UnityEngine.Transform> UIGrid::<>f__mg$cache2
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache2_19;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_17() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187_StaticFields, ___U3CU3Ef__mgU24cache0_17)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache0_17() const { return ___U3CU3Ef__mgU24cache0_17; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache0_17() { return &___U3CU3Ef__mgU24cache0_17; }
	inline void set_U3CU3Ef__mgU24cache0_17(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_18() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187_StaticFields, ___U3CU3Ef__mgU24cache1_18)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache1_18() const { return ___U3CU3Ef__mgU24cache1_18; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache1_18() { return &___U3CU3Ef__mgU24cache1_18; }
	inline void set_U3CU3Ef__mgU24cache1_18(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_19() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187_StaticFields, ___U3CU3Ef__mgU24cache2_19)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache2_19() const { return ___U3CU3Ef__mgU24cache2_19; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache2_19() { return &___U3CU3Ef__mgU24cache2_19; }
	inline void set_U3CU3Ef__mgU24cache2_19(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache2_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRID_T1536638187_H
#ifndef UIBASICSPRITE_T1521297657_H
#define UIBASICSPRITE_T1521297657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite
struct  UIBasicSprite_t1521297657  : public UIWidget_t3538521925
{
public:
	// UIBasicSprite/Type UIBasicSprite::mType
	int32_t ___mType_53;
	// UIBasicSprite/FillDirection UIBasicSprite::mFillDirection
	int32_t ___mFillDirection_54;
	// System.Single UIBasicSprite::mFillAmount
	float ___mFillAmount_55;
	// System.Boolean UIBasicSprite::mInvert
	bool ___mInvert_56;
	// UIBasicSprite/Flip UIBasicSprite::mFlip
	int32_t ___mFlip_57;
	// System.Boolean UIBasicSprite::mApplyGradient
	bool ___mApplyGradient_58;
	// UnityEngine.Color UIBasicSprite::mGradientTop
	Color_t2555686324  ___mGradientTop_59;
	// UnityEngine.Color UIBasicSprite::mGradientBottom
	Color_t2555686324  ___mGradientBottom_60;
	// UnityEngine.Rect UIBasicSprite::mInnerUV
	Rect_t2360479859  ___mInnerUV_61;
	// UnityEngine.Rect UIBasicSprite::mOuterUV
	Rect_t2360479859  ___mOuterUV_62;
	// UIBasicSprite/AdvancedType UIBasicSprite::centerType
	int32_t ___centerType_63;
	// UIBasicSprite/AdvancedType UIBasicSprite::leftType
	int32_t ___leftType_64;
	// UIBasicSprite/AdvancedType UIBasicSprite::rightType
	int32_t ___rightType_65;
	// UIBasicSprite/AdvancedType UIBasicSprite::bottomType
	int32_t ___bottomType_66;
	// UIBasicSprite/AdvancedType UIBasicSprite::topType
	int32_t ___topType_67;

public:
	inline static int32_t get_offset_of_mType_53() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mType_53)); }
	inline int32_t get_mType_53() const { return ___mType_53; }
	inline int32_t* get_address_of_mType_53() { return &___mType_53; }
	inline void set_mType_53(int32_t value)
	{
		___mType_53 = value;
	}

	inline static int32_t get_offset_of_mFillDirection_54() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFillDirection_54)); }
	inline int32_t get_mFillDirection_54() const { return ___mFillDirection_54; }
	inline int32_t* get_address_of_mFillDirection_54() { return &___mFillDirection_54; }
	inline void set_mFillDirection_54(int32_t value)
	{
		___mFillDirection_54 = value;
	}

	inline static int32_t get_offset_of_mFillAmount_55() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFillAmount_55)); }
	inline float get_mFillAmount_55() const { return ___mFillAmount_55; }
	inline float* get_address_of_mFillAmount_55() { return &___mFillAmount_55; }
	inline void set_mFillAmount_55(float value)
	{
		___mFillAmount_55 = value;
	}

	inline static int32_t get_offset_of_mInvert_56() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mInvert_56)); }
	inline bool get_mInvert_56() const { return ___mInvert_56; }
	inline bool* get_address_of_mInvert_56() { return &___mInvert_56; }
	inline void set_mInvert_56(bool value)
	{
		___mInvert_56 = value;
	}

	inline static int32_t get_offset_of_mFlip_57() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFlip_57)); }
	inline int32_t get_mFlip_57() const { return ___mFlip_57; }
	inline int32_t* get_address_of_mFlip_57() { return &___mFlip_57; }
	inline void set_mFlip_57(int32_t value)
	{
		___mFlip_57 = value;
	}

	inline static int32_t get_offset_of_mApplyGradient_58() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mApplyGradient_58)); }
	inline bool get_mApplyGradient_58() const { return ___mApplyGradient_58; }
	inline bool* get_address_of_mApplyGradient_58() { return &___mApplyGradient_58; }
	inline void set_mApplyGradient_58(bool value)
	{
		___mApplyGradient_58 = value;
	}

	inline static int32_t get_offset_of_mGradientTop_59() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mGradientTop_59)); }
	inline Color_t2555686324  get_mGradientTop_59() const { return ___mGradientTop_59; }
	inline Color_t2555686324 * get_address_of_mGradientTop_59() { return &___mGradientTop_59; }
	inline void set_mGradientTop_59(Color_t2555686324  value)
	{
		___mGradientTop_59 = value;
	}

	inline static int32_t get_offset_of_mGradientBottom_60() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mGradientBottom_60)); }
	inline Color_t2555686324  get_mGradientBottom_60() const { return ___mGradientBottom_60; }
	inline Color_t2555686324 * get_address_of_mGradientBottom_60() { return &___mGradientBottom_60; }
	inline void set_mGradientBottom_60(Color_t2555686324  value)
	{
		___mGradientBottom_60 = value;
	}

	inline static int32_t get_offset_of_mInnerUV_61() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mInnerUV_61)); }
	inline Rect_t2360479859  get_mInnerUV_61() const { return ___mInnerUV_61; }
	inline Rect_t2360479859 * get_address_of_mInnerUV_61() { return &___mInnerUV_61; }
	inline void set_mInnerUV_61(Rect_t2360479859  value)
	{
		___mInnerUV_61 = value;
	}

	inline static int32_t get_offset_of_mOuterUV_62() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mOuterUV_62)); }
	inline Rect_t2360479859  get_mOuterUV_62() const { return ___mOuterUV_62; }
	inline Rect_t2360479859 * get_address_of_mOuterUV_62() { return &___mOuterUV_62; }
	inline void set_mOuterUV_62(Rect_t2360479859  value)
	{
		___mOuterUV_62 = value;
	}

	inline static int32_t get_offset_of_centerType_63() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___centerType_63)); }
	inline int32_t get_centerType_63() const { return ___centerType_63; }
	inline int32_t* get_address_of_centerType_63() { return &___centerType_63; }
	inline void set_centerType_63(int32_t value)
	{
		___centerType_63 = value;
	}

	inline static int32_t get_offset_of_leftType_64() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___leftType_64)); }
	inline int32_t get_leftType_64() const { return ___leftType_64; }
	inline int32_t* get_address_of_leftType_64() { return &___leftType_64; }
	inline void set_leftType_64(int32_t value)
	{
		___leftType_64 = value;
	}

	inline static int32_t get_offset_of_rightType_65() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___rightType_65)); }
	inline int32_t get_rightType_65() const { return ___rightType_65; }
	inline int32_t* get_address_of_rightType_65() { return &___rightType_65; }
	inline void set_rightType_65(int32_t value)
	{
		___rightType_65 = value;
	}

	inline static int32_t get_offset_of_bottomType_66() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___bottomType_66)); }
	inline int32_t get_bottomType_66() const { return ___bottomType_66; }
	inline int32_t* get_address_of_bottomType_66() { return &___bottomType_66; }
	inline void set_bottomType_66(int32_t value)
	{
		___bottomType_66 = value;
	}

	inline static int32_t get_offset_of_topType_67() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___topType_67)); }
	inline int32_t get_topType_67() const { return ___topType_67; }
	inline int32_t* get_address_of_topType_67() { return &___topType_67; }
	inline void set_topType_67(int32_t value)
	{
		___topType_67 = value;
	}
};

struct UIBasicSprite_t1521297657_StaticFields
{
public:
	// UnityEngine.Vector2[] UIBasicSprite::mTempPos
	Vector2U5BU5D_t1457185986* ___mTempPos_68;
	// UnityEngine.Vector2[] UIBasicSprite::mTempUVs
	Vector2U5BU5D_t1457185986* ___mTempUVs_69;

public:
	inline static int32_t get_offset_of_mTempPos_68() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657_StaticFields, ___mTempPos_68)); }
	inline Vector2U5BU5D_t1457185986* get_mTempPos_68() const { return ___mTempPos_68; }
	inline Vector2U5BU5D_t1457185986** get_address_of_mTempPos_68() { return &___mTempPos_68; }
	inline void set_mTempPos_68(Vector2U5BU5D_t1457185986* value)
	{
		___mTempPos_68 = value;
		Il2CppCodeGenWriteBarrier((&___mTempPos_68), value);
	}

	inline static int32_t get_offset_of_mTempUVs_69() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657_StaticFields, ___mTempUVs_69)); }
	inline Vector2U5BU5D_t1457185986* get_mTempUVs_69() const { return ___mTempUVs_69; }
	inline Vector2U5BU5D_t1457185986** get_address_of_mTempUVs_69() { return &___mTempUVs_69; }
	inline void set_mTempUVs_69(Vector2U5BU5D_t1457185986* value)
	{
		___mTempUVs_69 = value;
		Il2CppCodeGenWriteBarrier((&___mTempUVs_69), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBASICSPRITE_T1521297657_H
#ifndef UILABEL_T3248798549_H
#define UILABEL_T3248798549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel
struct  UILabel_t3248798549  : public UIWidget_t3538521925
{
public:
	// UILabel/Crispness UILabel::keepCrispWhenShrunk
	int32_t ___keepCrispWhenShrunk_53;
	// UnityEngine.Font UILabel::mTrueTypeFont
	Font_t1956802104 * ___mTrueTypeFont_54;
	// UIFont UILabel::mFont
	UIFont_t2766063701 * ___mFont_55;
	// System.String UILabel::mText
	String_t* ___mText_56;
	// System.Int32 UILabel::mFontSize
	int32_t ___mFontSize_57;
	// UnityEngine.FontStyle UILabel::mFontStyle
	int32_t ___mFontStyle_58;
	// NGUIText/Alignment UILabel::mAlignment
	int32_t ___mAlignment_59;
	// System.Boolean UILabel::mEncoding
	bool ___mEncoding_60;
	// System.Int32 UILabel::mMaxLineCount
	int32_t ___mMaxLineCount_61;
	// UILabel/Effect UILabel::mEffectStyle
	int32_t ___mEffectStyle_62;
	// UnityEngine.Color UILabel::mEffectColor
	Color_t2555686324  ___mEffectColor_63;
	// NGUIText/SymbolStyle UILabel::mSymbols
	int32_t ___mSymbols_64;
	// UnityEngine.Vector2 UILabel::mEffectDistance
	Vector2_t2156229523  ___mEffectDistance_65;
	// UILabel/Overflow UILabel::mOverflow
	int32_t ___mOverflow_66;
	// System.Boolean UILabel::mApplyGradient
	bool ___mApplyGradient_67;
	// UnityEngine.Color UILabel::mGradientTop
	Color_t2555686324  ___mGradientTop_68;
	// UnityEngine.Color UILabel::mGradientBottom
	Color_t2555686324  ___mGradientBottom_69;
	// System.Int32 UILabel::mSpacingX
	int32_t ___mSpacingX_70;
	// System.Int32 UILabel::mSpacingY
	int32_t ___mSpacingY_71;
	// System.Boolean UILabel::mUseFloatSpacing
	bool ___mUseFloatSpacing_72;
	// System.Single UILabel::mFloatSpacingX
	float ___mFloatSpacingX_73;
	// System.Single UILabel::mFloatSpacingY
	float ___mFloatSpacingY_74;
	// System.Boolean UILabel::mOverflowEllipsis
	bool ___mOverflowEllipsis_75;
	// System.Int32 UILabel::mOverflowWidth
	int32_t ___mOverflowWidth_76;
	// System.Int32 UILabel::mOverflowHeight
	int32_t ___mOverflowHeight_77;
	// UILabel/Modifier UILabel::mModifier
	int32_t ___mModifier_78;
	// System.Boolean UILabel::mShrinkToFit
	bool ___mShrinkToFit_79;
	// System.Int32 UILabel::mMaxLineWidth
	int32_t ___mMaxLineWidth_80;
	// System.Int32 UILabel::mMaxLineHeight
	int32_t ___mMaxLineHeight_81;
	// System.Single UILabel::mLineWidth
	float ___mLineWidth_82;
	// System.Boolean UILabel::mMultiline
	bool ___mMultiline_83;
	// UnityEngine.Font UILabel::mActiveTTF
	Font_t1956802104 * ___mActiveTTF_84;
	// System.Single UILabel::mDensity
	float ___mDensity_85;
	// System.Boolean UILabel::mShouldBeProcessed
	bool ___mShouldBeProcessed_86;
	// System.String UILabel::mProcessedText
	String_t* ___mProcessedText_87;
	// System.Boolean UILabel::mPremultiply
	bool ___mPremultiply_88;
	// UnityEngine.Vector2 UILabel::mCalculatedSize
	Vector2_t2156229523  ___mCalculatedSize_89;
	// System.Single UILabel::mScale
	float ___mScale_90;
	// System.Int32 UILabel::mFinalFontSize
	int32_t ___mFinalFontSize_91;
	// System.Int32 UILabel::mLastWidth
	int32_t ___mLastWidth_92;
	// System.Int32 UILabel::mLastHeight
	int32_t ___mLastHeight_93;
	// UILabel/ModifierFunc UILabel::customModifier
	ModifierFunc_t2998065802 * ___customModifier_94;

public:
	inline static int32_t get_offset_of_keepCrispWhenShrunk_53() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___keepCrispWhenShrunk_53)); }
	inline int32_t get_keepCrispWhenShrunk_53() const { return ___keepCrispWhenShrunk_53; }
	inline int32_t* get_address_of_keepCrispWhenShrunk_53() { return &___keepCrispWhenShrunk_53; }
	inline void set_keepCrispWhenShrunk_53(int32_t value)
	{
		___keepCrispWhenShrunk_53 = value;
	}

	inline static int32_t get_offset_of_mTrueTypeFont_54() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mTrueTypeFont_54)); }
	inline Font_t1956802104 * get_mTrueTypeFont_54() const { return ___mTrueTypeFont_54; }
	inline Font_t1956802104 ** get_address_of_mTrueTypeFont_54() { return &___mTrueTypeFont_54; }
	inline void set_mTrueTypeFont_54(Font_t1956802104 * value)
	{
		___mTrueTypeFont_54 = value;
		Il2CppCodeGenWriteBarrier((&___mTrueTypeFont_54), value);
	}

	inline static int32_t get_offset_of_mFont_55() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFont_55)); }
	inline UIFont_t2766063701 * get_mFont_55() const { return ___mFont_55; }
	inline UIFont_t2766063701 ** get_address_of_mFont_55() { return &___mFont_55; }
	inline void set_mFont_55(UIFont_t2766063701 * value)
	{
		___mFont_55 = value;
		Il2CppCodeGenWriteBarrier((&___mFont_55), value);
	}

	inline static int32_t get_offset_of_mText_56() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mText_56)); }
	inline String_t* get_mText_56() const { return ___mText_56; }
	inline String_t** get_address_of_mText_56() { return &___mText_56; }
	inline void set_mText_56(String_t* value)
	{
		___mText_56 = value;
		Il2CppCodeGenWriteBarrier((&___mText_56), value);
	}

	inline static int32_t get_offset_of_mFontSize_57() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFontSize_57)); }
	inline int32_t get_mFontSize_57() const { return ___mFontSize_57; }
	inline int32_t* get_address_of_mFontSize_57() { return &___mFontSize_57; }
	inline void set_mFontSize_57(int32_t value)
	{
		___mFontSize_57 = value;
	}

	inline static int32_t get_offset_of_mFontStyle_58() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFontStyle_58)); }
	inline int32_t get_mFontStyle_58() const { return ___mFontStyle_58; }
	inline int32_t* get_address_of_mFontStyle_58() { return &___mFontStyle_58; }
	inline void set_mFontStyle_58(int32_t value)
	{
		___mFontStyle_58 = value;
	}

	inline static int32_t get_offset_of_mAlignment_59() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mAlignment_59)); }
	inline int32_t get_mAlignment_59() const { return ___mAlignment_59; }
	inline int32_t* get_address_of_mAlignment_59() { return &___mAlignment_59; }
	inline void set_mAlignment_59(int32_t value)
	{
		___mAlignment_59 = value;
	}

	inline static int32_t get_offset_of_mEncoding_60() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEncoding_60)); }
	inline bool get_mEncoding_60() const { return ___mEncoding_60; }
	inline bool* get_address_of_mEncoding_60() { return &___mEncoding_60; }
	inline void set_mEncoding_60(bool value)
	{
		___mEncoding_60 = value;
	}

	inline static int32_t get_offset_of_mMaxLineCount_61() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMaxLineCount_61)); }
	inline int32_t get_mMaxLineCount_61() const { return ___mMaxLineCount_61; }
	inline int32_t* get_address_of_mMaxLineCount_61() { return &___mMaxLineCount_61; }
	inline void set_mMaxLineCount_61(int32_t value)
	{
		___mMaxLineCount_61 = value;
	}

	inline static int32_t get_offset_of_mEffectStyle_62() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEffectStyle_62)); }
	inline int32_t get_mEffectStyle_62() const { return ___mEffectStyle_62; }
	inline int32_t* get_address_of_mEffectStyle_62() { return &___mEffectStyle_62; }
	inline void set_mEffectStyle_62(int32_t value)
	{
		___mEffectStyle_62 = value;
	}

	inline static int32_t get_offset_of_mEffectColor_63() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEffectColor_63)); }
	inline Color_t2555686324  get_mEffectColor_63() const { return ___mEffectColor_63; }
	inline Color_t2555686324 * get_address_of_mEffectColor_63() { return &___mEffectColor_63; }
	inline void set_mEffectColor_63(Color_t2555686324  value)
	{
		___mEffectColor_63 = value;
	}

	inline static int32_t get_offset_of_mSymbols_64() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mSymbols_64)); }
	inline int32_t get_mSymbols_64() const { return ___mSymbols_64; }
	inline int32_t* get_address_of_mSymbols_64() { return &___mSymbols_64; }
	inline void set_mSymbols_64(int32_t value)
	{
		___mSymbols_64 = value;
	}

	inline static int32_t get_offset_of_mEffectDistance_65() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEffectDistance_65)); }
	inline Vector2_t2156229523  get_mEffectDistance_65() const { return ___mEffectDistance_65; }
	inline Vector2_t2156229523 * get_address_of_mEffectDistance_65() { return &___mEffectDistance_65; }
	inline void set_mEffectDistance_65(Vector2_t2156229523  value)
	{
		___mEffectDistance_65 = value;
	}

	inline static int32_t get_offset_of_mOverflow_66() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflow_66)); }
	inline int32_t get_mOverflow_66() const { return ___mOverflow_66; }
	inline int32_t* get_address_of_mOverflow_66() { return &___mOverflow_66; }
	inline void set_mOverflow_66(int32_t value)
	{
		___mOverflow_66 = value;
	}

	inline static int32_t get_offset_of_mApplyGradient_67() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mApplyGradient_67)); }
	inline bool get_mApplyGradient_67() const { return ___mApplyGradient_67; }
	inline bool* get_address_of_mApplyGradient_67() { return &___mApplyGradient_67; }
	inline void set_mApplyGradient_67(bool value)
	{
		___mApplyGradient_67 = value;
	}

	inline static int32_t get_offset_of_mGradientTop_68() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mGradientTop_68)); }
	inline Color_t2555686324  get_mGradientTop_68() const { return ___mGradientTop_68; }
	inline Color_t2555686324 * get_address_of_mGradientTop_68() { return &___mGradientTop_68; }
	inline void set_mGradientTop_68(Color_t2555686324  value)
	{
		___mGradientTop_68 = value;
	}

	inline static int32_t get_offset_of_mGradientBottom_69() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mGradientBottom_69)); }
	inline Color_t2555686324  get_mGradientBottom_69() const { return ___mGradientBottom_69; }
	inline Color_t2555686324 * get_address_of_mGradientBottom_69() { return &___mGradientBottom_69; }
	inline void set_mGradientBottom_69(Color_t2555686324  value)
	{
		___mGradientBottom_69 = value;
	}

	inline static int32_t get_offset_of_mSpacingX_70() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mSpacingX_70)); }
	inline int32_t get_mSpacingX_70() const { return ___mSpacingX_70; }
	inline int32_t* get_address_of_mSpacingX_70() { return &___mSpacingX_70; }
	inline void set_mSpacingX_70(int32_t value)
	{
		___mSpacingX_70 = value;
	}

	inline static int32_t get_offset_of_mSpacingY_71() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mSpacingY_71)); }
	inline int32_t get_mSpacingY_71() const { return ___mSpacingY_71; }
	inline int32_t* get_address_of_mSpacingY_71() { return &___mSpacingY_71; }
	inline void set_mSpacingY_71(int32_t value)
	{
		___mSpacingY_71 = value;
	}

	inline static int32_t get_offset_of_mUseFloatSpacing_72() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mUseFloatSpacing_72)); }
	inline bool get_mUseFloatSpacing_72() const { return ___mUseFloatSpacing_72; }
	inline bool* get_address_of_mUseFloatSpacing_72() { return &___mUseFloatSpacing_72; }
	inline void set_mUseFloatSpacing_72(bool value)
	{
		___mUseFloatSpacing_72 = value;
	}

	inline static int32_t get_offset_of_mFloatSpacingX_73() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFloatSpacingX_73)); }
	inline float get_mFloatSpacingX_73() const { return ___mFloatSpacingX_73; }
	inline float* get_address_of_mFloatSpacingX_73() { return &___mFloatSpacingX_73; }
	inline void set_mFloatSpacingX_73(float value)
	{
		___mFloatSpacingX_73 = value;
	}

	inline static int32_t get_offset_of_mFloatSpacingY_74() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFloatSpacingY_74)); }
	inline float get_mFloatSpacingY_74() const { return ___mFloatSpacingY_74; }
	inline float* get_address_of_mFloatSpacingY_74() { return &___mFloatSpacingY_74; }
	inline void set_mFloatSpacingY_74(float value)
	{
		___mFloatSpacingY_74 = value;
	}

	inline static int32_t get_offset_of_mOverflowEllipsis_75() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflowEllipsis_75)); }
	inline bool get_mOverflowEllipsis_75() const { return ___mOverflowEllipsis_75; }
	inline bool* get_address_of_mOverflowEllipsis_75() { return &___mOverflowEllipsis_75; }
	inline void set_mOverflowEllipsis_75(bool value)
	{
		___mOverflowEllipsis_75 = value;
	}

	inline static int32_t get_offset_of_mOverflowWidth_76() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflowWidth_76)); }
	inline int32_t get_mOverflowWidth_76() const { return ___mOverflowWidth_76; }
	inline int32_t* get_address_of_mOverflowWidth_76() { return &___mOverflowWidth_76; }
	inline void set_mOverflowWidth_76(int32_t value)
	{
		___mOverflowWidth_76 = value;
	}

	inline static int32_t get_offset_of_mOverflowHeight_77() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflowHeight_77)); }
	inline int32_t get_mOverflowHeight_77() const { return ___mOverflowHeight_77; }
	inline int32_t* get_address_of_mOverflowHeight_77() { return &___mOverflowHeight_77; }
	inline void set_mOverflowHeight_77(int32_t value)
	{
		___mOverflowHeight_77 = value;
	}

	inline static int32_t get_offset_of_mModifier_78() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mModifier_78)); }
	inline int32_t get_mModifier_78() const { return ___mModifier_78; }
	inline int32_t* get_address_of_mModifier_78() { return &___mModifier_78; }
	inline void set_mModifier_78(int32_t value)
	{
		___mModifier_78 = value;
	}

	inline static int32_t get_offset_of_mShrinkToFit_79() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mShrinkToFit_79)); }
	inline bool get_mShrinkToFit_79() const { return ___mShrinkToFit_79; }
	inline bool* get_address_of_mShrinkToFit_79() { return &___mShrinkToFit_79; }
	inline void set_mShrinkToFit_79(bool value)
	{
		___mShrinkToFit_79 = value;
	}

	inline static int32_t get_offset_of_mMaxLineWidth_80() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMaxLineWidth_80)); }
	inline int32_t get_mMaxLineWidth_80() const { return ___mMaxLineWidth_80; }
	inline int32_t* get_address_of_mMaxLineWidth_80() { return &___mMaxLineWidth_80; }
	inline void set_mMaxLineWidth_80(int32_t value)
	{
		___mMaxLineWidth_80 = value;
	}

	inline static int32_t get_offset_of_mMaxLineHeight_81() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMaxLineHeight_81)); }
	inline int32_t get_mMaxLineHeight_81() const { return ___mMaxLineHeight_81; }
	inline int32_t* get_address_of_mMaxLineHeight_81() { return &___mMaxLineHeight_81; }
	inline void set_mMaxLineHeight_81(int32_t value)
	{
		___mMaxLineHeight_81 = value;
	}

	inline static int32_t get_offset_of_mLineWidth_82() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mLineWidth_82)); }
	inline float get_mLineWidth_82() const { return ___mLineWidth_82; }
	inline float* get_address_of_mLineWidth_82() { return &___mLineWidth_82; }
	inline void set_mLineWidth_82(float value)
	{
		___mLineWidth_82 = value;
	}

	inline static int32_t get_offset_of_mMultiline_83() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMultiline_83)); }
	inline bool get_mMultiline_83() const { return ___mMultiline_83; }
	inline bool* get_address_of_mMultiline_83() { return &___mMultiline_83; }
	inline void set_mMultiline_83(bool value)
	{
		___mMultiline_83 = value;
	}

	inline static int32_t get_offset_of_mActiveTTF_84() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mActiveTTF_84)); }
	inline Font_t1956802104 * get_mActiveTTF_84() const { return ___mActiveTTF_84; }
	inline Font_t1956802104 ** get_address_of_mActiveTTF_84() { return &___mActiveTTF_84; }
	inline void set_mActiveTTF_84(Font_t1956802104 * value)
	{
		___mActiveTTF_84 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveTTF_84), value);
	}

	inline static int32_t get_offset_of_mDensity_85() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mDensity_85)); }
	inline float get_mDensity_85() const { return ___mDensity_85; }
	inline float* get_address_of_mDensity_85() { return &___mDensity_85; }
	inline void set_mDensity_85(float value)
	{
		___mDensity_85 = value;
	}

	inline static int32_t get_offset_of_mShouldBeProcessed_86() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mShouldBeProcessed_86)); }
	inline bool get_mShouldBeProcessed_86() const { return ___mShouldBeProcessed_86; }
	inline bool* get_address_of_mShouldBeProcessed_86() { return &___mShouldBeProcessed_86; }
	inline void set_mShouldBeProcessed_86(bool value)
	{
		___mShouldBeProcessed_86 = value;
	}

	inline static int32_t get_offset_of_mProcessedText_87() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mProcessedText_87)); }
	inline String_t* get_mProcessedText_87() const { return ___mProcessedText_87; }
	inline String_t** get_address_of_mProcessedText_87() { return &___mProcessedText_87; }
	inline void set_mProcessedText_87(String_t* value)
	{
		___mProcessedText_87 = value;
		Il2CppCodeGenWriteBarrier((&___mProcessedText_87), value);
	}

	inline static int32_t get_offset_of_mPremultiply_88() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mPremultiply_88)); }
	inline bool get_mPremultiply_88() const { return ___mPremultiply_88; }
	inline bool* get_address_of_mPremultiply_88() { return &___mPremultiply_88; }
	inline void set_mPremultiply_88(bool value)
	{
		___mPremultiply_88 = value;
	}

	inline static int32_t get_offset_of_mCalculatedSize_89() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mCalculatedSize_89)); }
	inline Vector2_t2156229523  get_mCalculatedSize_89() const { return ___mCalculatedSize_89; }
	inline Vector2_t2156229523 * get_address_of_mCalculatedSize_89() { return &___mCalculatedSize_89; }
	inline void set_mCalculatedSize_89(Vector2_t2156229523  value)
	{
		___mCalculatedSize_89 = value;
	}

	inline static int32_t get_offset_of_mScale_90() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mScale_90)); }
	inline float get_mScale_90() const { return ___mScale_90; }
	inline float* get_address_of_mScale_90() { return &___mScale_90; }
	inline void set_mScale_90(float value)
	{
		___mScale_90 = value;
	}

	inline static int32_t get_offset_of_mFinalFontSize_91() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFinalFontSize_91)); }
	inline int32_t get_mFinalFontSize_91() const { return ___mFinalFontSize_91; }
	inline int32_t* get_address_of_mFinalFontSize_91() { return &___mFinalFontSize_91; }
	inline void set_mFinalFontSize_91(int32_t value)
	{
		___mFinalFontSize_91 = value;
	}

	inline static int32_t get_offset_of_mLastWidth_92() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mLastWidth_92)); }
	inline int32_t get_mLastWidth_92() const { return ___mLastWidth_92; }
	inline int32_t* get_address_of_mLastWidth_92() { return &___mLastWidth_92; }
	inline void set_mLastWidth_92(int32_t value)
	{
		___mLastWidth_92 = value;
	}

	inline static int32_t get_offset_of_mLastHeight_93() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mLastHeight_93)); }
	inline int32_t get_mLastHeight_93() const { return ___mLastHeight_93; }
	inline int32_t* get_address_of_mLastHeight_93() { return &___mLastHeight_93; }
	inline void set_mLastHeight_93(int32_t value)
	{
		___mLastHeight_93 = value;
	}

	inline static int32_t get_offset_of_customModifier_94() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___customModifier_94)); }
	inline ModifierFunc_t2998065802 * get_customModifier_94() const { return ___customModifier_94; }
	inline ModifierFunc_t2998065802 ** get_address_of_customModifier_94() { return &___customModifier_94; }
	inline void set_customModifier_94(ModifierFunc_t2998065802 * value)
	{
		___customModifier_94 = value;
		Il2CppCodeGenWriteBarrier((&___customModifier_94), value);
	}
};

struct UILabel_t3248798549_StaticFields
{
public:
	// BetterList`1<UILabel> UILabel::mList
	BetterList_1_t2403818867 * ___mList_95;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Int32> UILabel::mFontUsage
	Dictionary_2_t2853457297 * ___mFontUsage_96;
	// BetterList`1<UIDrawCall> UILabel::mTempDrawcalls
	BetterList_1_t448425637 * ___mTempDrawcalls_97;
	// System.Boolean UILabel::mTexRebuildAdded
	bool ___mTexRebuildAdded_98;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UILabel::mTempVerts
	List_1_t899420910 * ___mTempVerts_99;
	// System.Collections.Generic.List`1<System.Int32> UILabel::mTempIndices
	List_1_t128053199 * ___mTempIndices_100;
	// System.Action`1<UnityEngine.Font> UILabel::<>f__mg$cache0
	Action_1_t2129269699 * ___U3CU3Ef__mgU24cache0_101;

public:
	inline static int32_t get_offset_of_mList_95() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mList_95)); }
	inline BetterList_1_t2403818867 * get_mList_95() const { return ___mList_95; }
	inline BetterList_1_t2403818867 ** get_address_of_mList_95() { return &___mList_95; }
	inline void set_mList_95(BetterList_1_t2403818867 * value)
	{
		___mList_95 = value;
		Il2CppCodeGenWriteBarrier((&___mList_95), value);
	}

	inline static int32_t get_offset_of_mFontUsage_96() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mFontUsage_96)); }
	inline Dictionary_2_t2853457297 * get_mFontUsage_96() const { return ___mFontUsage_96; }
	inline Dictionary_2_t2853457297 ** get_address_of_mFontUsage_96() { return &___mFontUsage_96; }
	inline void set_mFontUsage_96(Dictionary_2_t2853457297 * value)
	{
		___mFontUsage_96 = value;
		Il2CppCodeGenWriteBarrier((&___mFontUsage_96), value);
	}

	inline static int32_t get_offset_of_mTempDrawcalls_97() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTempDrawcalls_97)); }
	inline BetterList_1_t448425637 * get_mTempDrawcalls_97() const { return ___mTempDrawcalls_97; }
	inline BetterList_1_t448425637 ** get_address_of_mTempDrawcalls_97() { return &___mTempDrawcalls_97; }
	inline void set_mTempDrawcalls_97(BetterList_1_t448425637 * value)
	{
		___mTempDrawcalls_97 = value;
		Il2CppCodeGenWriteBarrier((&___mTempDrawcalls_97), value);
	}

	inline static int32_t get_offset_of_mTexRebuildAdded_98() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTexRebuildAdded_98)); }
	inline bool get_mTexRebuildAdded_98() const { return ___mTexRebuildAdded_98; }
	inline bool* get_address_of_mTexRebuildAdded_98() { return &___mTexRebuildAdded_98; }
	inline void set_mTexRebuildAdded_98(bool value)
	{
		___mTexRebuildAdded_98 = value;
	}

	inline static int32_t get_offset_of_mTempVerts_99() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTempVerts_99)); }
	inline List_1_t899420910 * get_mTempVerts_99() const { return ___mTempVerts_99; }
	inline List_1_t899420910 ** get_address_of_mTempVerts_99() { return &___mTempVerts_99; }
	inline void set_mTempVerts_99(List_1_t899420910 * value)
	{
		___mTempVerts_99 = value;
		Il2CppCodeGenWriteBarrier((&___mTempVerts_99), value);
	}

	inline static int32_t get_offset_of_mTempIndices_100() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTempIndices_100)); }
	inline List_1_t128053199 * get_mTempIndices_100() const { return ___mTempIndices_100; }
	inline List_1_t128053199 ** get_address_of_mTempIndices_100() { return &___mTempIndices_100; }
	inline void set_mTempIndices_100(List_1_t128053199 * value)
	{
		___mTempIndices_100 = value;
		Il2CppCodeGenWriteBarrier((&___mTempIndices_100), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_101() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___U3CU3Ef__mgU24cache0_101)); }
	inline Action_1_t2129269699 * get_U3CU3Ef__mgU24cache0_101() const { return ___U3CU3Ef__mgU24cache0_101; }
	inline Action_1_t2129269699 ** get_address_of_U3CU3Ef__mgU24cache0_101() { return &___U3CU3Ef__mgU24cache0_101; }
	inline void set_U3CU3Ef__mgU24cache0_101(Action_1_t2129269699 * value)
	{
		___U3CU3Ef__mgU24cache0_101 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_101), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILABEL_T3248798549_H
#ifndef UISPRITE_T194114938_H
#define UISPRITE_T194114938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISprite
struct  UISprite_t194114938  : public UIBasicSprite_t1521297657
{
public:
	// UIAtlas UISprite::mAtlas
	UIAtlas_t3195533529 * ___mAtlas_70;
	// System.String UISprite::mSpriteName
	String_t* ___mSpriteName_71;
	// System.Boolean UISprite::mFillCenter
	bool ___mFillCenter_72;
	// UISpriteData UISprite::mSprite
	UISpriteData_t900308526 * ___mSprite_73;
	// System.Boolean UISprite::mSpriteSet
	bool ___mSpriteSet_74;

public:
	inline static int32_t get_offset_of_mAtlas_70() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mAtlas_70)); }
	inline UIAtlas_t3195533529 * get_mAtlas_70() const { return ___mAtlas_70; }
	inline UIAtlas_t3195533529 ** get_address_of_mAtlas_70() { return &___mAtlas_70; }
	inline void set_mAtlas_70(UIAtlas_t3195533529 * value)
	{
		___mAtlas_70 = value;
		Il2CppCodeGenWriteBarrier((&___mAtlas_70), value);
	}

	inline static int32_t get_offset_of_mSpriteName_71() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mSpriteName_71)); }
	inline String_t* get_mSpriteName_71() const { return ___mSpriteName_71; }
	inline String_t** get_address_of_mSpriteName_71() { return &___mSpriteName_71; }
	inline void set_mSpriteName_71(String_t* value)
	{
		___mSpriteName_71 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteName_71), value);
	}

	inline static int32_t get_offset_of_mFillCenter_72() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mFillCenter_72)); }
	inline bool get_mFillCenter_72() const { return ___mFillCenter_72; }
	inline bool* get_address_of_mFillCenter_72() { return &___mFillCenter_72; }
	inline void set_mFillCenter_72(bool value)
	{
		___mFillCenter_72 = value;
	}

	inline static int32_t get_offset_of_mSprite_73() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mSprite_73)); }
	inline UISpriteData_t900308526 * get_mSprite_73() const { return ___mSprite_73; }
	inline UISpriteData_t900308526 ** get_address_of_mSprite_73() { return &___mSprite_73; }
	inline void set_mSprite_73(UISpriteData_t900308526 * value)
	{
		___mSprite_73 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_73), value);
	}

	inline static int32_t get_offset_of_mSpriteSet_74() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mSpriteSet_74)); }
	inline bool get_mSpriteSet_74() const { return ___mSpriteSet_74; }
	inline bool* get_address_of_mSpriteSet_74() { return &___mSpriteSet_74; }
	inline void set_mSpriteSet_74(bool value)
	{
		___mSpriteSet_74 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISPRITE_T194114938_H
// UITweener[]
struct UITweenerU5BU5D_t3440034099  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UITweener_t260334902 * m_Items[1];

public:
	inline UITweener_t260334902 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UITweener_t260334902 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UITweener_t260334902 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline UITweener_t260334902 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UITweener_t260334902 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UITweener_t260334902 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIWidget[]
struct UIWidgetU5BU5D_t2950248968  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UIWidget_t3538521925 * m_Items[1];

public:
	inline UIWidget_t3538521925 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UIWidget_t3538521925 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UIWidget_t3538521925 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline UIWidget_t3538521925 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UIWidget_t3538521925 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UIWidget_t3538521925 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_t4206410242  m_Items[1];

public:
	inline Keyframe_t4206410242  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t4206410242  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// T BetterList`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * BetterList_1_get_Item_m1187708413_gshared (BetterList_1_t2235126482 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void BetterList`1<System.Object>::Add(T)
extern "C"  void BetterList_1_Add_m1521034418_gshared (BetterList_1_t2235126482 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean BetterList`1<System.Object>::Remove(T)
extern "C"  bool BetterList_1_Remove_m375183283_gshared (BetterList_1_t2235126482 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisRuntimeObject_m2468776128_gshared (Component_t1923634451 * __this, bool p0, const RuntimeMethod* method);
// System.Void BetterList`1<System.Object>::.ctor()
extern "C"  void BetterList_1__ctor_m445033979_gshared (BetterList_1_t2235126482 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisRuntimeObject_m1308288322_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m1416767016_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m793514796_gshared (Comparison_1_t2855037343 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<!0>)
extern "C"  void List_1_Sort_m2076177611_gshared (List_1_t257213610 * __this, Comparison_1_t2855037343 * p0, const RuntimeMethod* method);
// T NGUITools::FindInParents<System.Object>(UnityEngine.GameObject)
extern "C"  RuntimeObject * NGUITools_FindInParents_TisRuntimeObject_m780518502_gshared (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  RuntimeObject * Resources_Load_TisRuntimeObject_m597869152_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<ShareLocation,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1989985705_gshared (Dictionary_2_t111267975 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<ShareLocation,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m3965601654_gshared (Dictionary_2_t111267975 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void PlayerPrefsUtility::SaveDict<System.Object,System.Int32>(System.String,System.Collections.Generic.Dictionary`2<Key,Value>)
extern "C"  void PlayerPrefsUtility_SaveDict_TisRuntimeObject_TisInt32_t2950945753_m2988034944_gshared (RuntimeObject * __this /* static, unused */, String_t* ___key0, Dictionary_2_t3384741 * ___value1, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<Key,Value> PlayerPrefsUtility::LoadDict<System.Object,System.Int32>(System.String)
extern "C"  Dictionary_2_t3384741 * PlayerPrefsUtility_LoadDict_TisRuntimeObject_TisInt32_t2950945753_m3311238834_gshared (RuntimeObject * __this /* static, unused */, String_t* ___key0, const RuntimeMethod* method);
// TValue DictionaryExtensions::GetValueOrDefault<System.Object,System.Int32>(System.Collections.Generic.IDictionary`2<TKey,TValue>,TKey,TValue)
extern "C"  int32_t DictionaryExtensions_GetValueOrDefault_TisRuntimeObject_TisInt32_t2950945753_m2660330963_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, RuntimeObject * ___key1, int32_t ___defaultValue2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m411961606_gshared (Dictionary_2_t3384741 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
extern "C"  void Action_1_Invoke_m1933767679_gshared (Action_1_t269755560 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m2253601317_gshared (Dictionary_2_t3384741 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m981112613_gshared (Action_1_t269755560 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<EventDelegate>::.ctor()
#define List_1__ctor_m2541037942(__this, method) ((  void (*) (List_1_t4210400802 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void UIWidgetContainer::.ctor()
extern "C"  void UIWidgetContainer__ctor_m155952688 (UIWidgetContainer_t30162560 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::Set(System.Boolean,System.Boolean)
extern "C"  void UIToggle_Set_m2784184886 (UIToggle_t4192126258 * __this, bool ___state0, bool ___notify1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t1773347010_m4226749020(__this, method) ((  Collider_t1773347010 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider::get_enabled()
extern "C"  bool Collider_get_enabled_m3096904824 (Collider_t1773347010 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider2D>()
#define Component_GetComponent_TisCollider2D_t2806799626_m3391829639(__this, method) ((  Collider2D_t2806799626 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m753527255 (Behaviour_t1437897464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle::get_value()
extern "C"  bool UIToggle_get_value_m2062763629 (UIToggle_t4192126258 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::set_value(System.Boolean)
extern "C"  void UIToggle_set_value_m3790403027 (UIToggle_t4192126258 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T BetterList`1<UIToggle>::get_Item(System.Int32)
#define BetterList_1_get_Item_m2983367427(__this, p0, method) ((  UIToggle_t4192126258 * (*) (BetterList_1_t3347146576 *, int32_t, const RuntimeMethod*))BetterList_1_get_Item_m1187708413_gshared)(__this, p0, method)
// System.Void BetterList`1<UIToggle>::Add(T)
#define BetterList_1_Add_m1919439375(__this, p0, method) ((  void (*) (BetterList_1_t3347146576 *, UIToggle_t4192126258 *, const RuntimeMethod*))BetterList_1_Add_m1521034418_gshared)(__this, p0, method)
// System.Boolean BetterList`1<UIToggle>::Remove(T)
#define BetterList_1_Remove_m2601698983(__this, p0, method) ((  bool (*) (BetterList_1_t3347146576 *, UIToggle_t4192126258 *, const RuntimeMethod*))BetterList_1_Remove_m375183283_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m100394690 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegate::IsValid(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  bool EventDelegate_IsValid_m1359220059 (RuntimeObject * __this /* static, unused */, List_1_t4210400802 * ___list0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle::get_isColliderEnabled()
extern "C"  bool UIToggle_get_isColliderEnabled_m2508236936 (UIToggle_t4192126258 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle/Validate::Invoke(System.Boolean)
extern "C"  bool Validate_Invoke_m3691890711 (Validate_t3702293971 * __this, bool ___choice0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITools::GetActive(UnityEngine.Behaviour)
extern "C"  bool NGUITools_GetActive_m3370498562 (RuntimeObject * __this /* static, unused */, Behaviour_t1437897464 * ___mb0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// TweenAlpha TweenAlpha::Begin(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern "C"  TweenAlpha_t3706845226 * TweenAlpha_Begin_m799587892 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, float ___duration1, float ___alpha2, float ___delay3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegate::Execute(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  void EventDelegate_Execute_m2610939864 (RuntimeObject * __this /* static, unused */, List_1_t4210400802 * ___list0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m3720186693 (GameObject_t1113636619 * __this, String_t* p0, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animator,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern "C"  ActiveAnimation_t3475256642 * ActiveAnimation_Play_m2569601364 (RuntimeObject * __this /* static, unused */, Animator_t434523843 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, int32_t ___enableBeforePlay3, int32_t ___disableCondition4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::Finish()
extern "C"  void ActiveAnimation_Finish_m3064184920 (ActiveAnimation_t3475256642 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern "C"  ActiveAnimation_t3475256642 * ActiveAnimation_Play_m2479515133 (RuntimeObject * __this /* static, unused */, Animation_t3648466861 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, int32_t ___enableBeforePlay3, int32_t ___disableCondition4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UITweener>(System.Boolean)
#define Component_GetComponentsInChildren_TisUITweener_t260334902_m2859153738(__this, p0, method) ((  UITweenerU5BU5D_t3440034099* (*) (Component_t1923634451 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m2468776128_gshared)(__this, p0, method)
// System.Void UITweener::set_tweenFactor(System.Single)
extern "C"  void UITweener_set_tweenFactor_m3630546225 (UITweener_t260334902 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void BetterList`1<UIToggle>::.ctor()
#define BetterList_1__ctor_m2278349088(__this, method) ((  void (*) (BetterList_1_t3347146576 *, const RuntimeMethod*))BetterList_1__ctor_m445033979_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Count()
#define List_1_get_Count_m430733211(__this, method) ((  int32_t (*) (List_1_t1139589975 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::Add(!0)
#define List_1_Add_m421447557(__this, p0, method) ((  void (*) (List_1_t1139589975 *, MonoBehaviour_t3962482529 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UIToggle>()
#define Component_GetComponent_TisUIToggle_t4192126258_m3922355289(__this, method) ((  UIToggle_t4192126258 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void EventDelegate/Callback::.ctor(System.Object,System.IntPtr)
extern "C"  void Callback__ctor_m3761074887 (Callback_t3139336517 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// EventDelegate EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t2738326060 * EventDelegate_Add_m1448281318 (RuntimeObject * __this /* static, unused */, List_1_t4210400802 * ___list0, Callback_t3139336517 * ___callback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m3720941323(__this, p0, method) ((  MonoBehaviour_t3962482529 * (*) (List_1_t1139589975 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
#define List_1_get_Count_m2812834599(__this, method) ((  int32_t (*) (List_1_t2585711361 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
#define List_1_Add_m2765963565(__this, p0, method) ((  void (*) (List_1_t2585711361 *, GameObject_t1113636619 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m3743125852(__this, p0, method) ((  GameObject_t1113636619 * (*) (List_1_t2585711361 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void UIToggledObjects::Set(UnityEngine.GameObject,System.Boolean)
extern "C"  void UIToggledObjects_Set_m2281364091 (UIToggledObjects_t3502557910 * __this, GameObject_t1113636619 * ___go0, bool ___state1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITools::SetActive(UnityEngine.GameObject,System.Boolean)
extern "C"  void NGUITools_SetActive_m3543859753 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, bool ___state1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UIWidget>()
#define Component_GetComponentsInChildren_TisUIWidget_t3538521925_m3698995734(__this, method) ((  UIWidgetU5BU5D_t2950248968* (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m1308288322_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t3722313464  Transform_get_localPosition_m4234289348 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m4158800245 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera NGUITools::FindCameraForLayer(System.Int32)
extern "C"  Camera_t4157153871 * NGUITools_FindCameraForLayer_m709671062 (RuntimeObject * __this /* static, unused */, int32_t ___layer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_tooltipObject()
extern "C"  GameObject_t1113636619 * UICamera_get_tooltipObject_m2829094911 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single RealTime::get_deltaTime()
extern "C"  float RealTime_get_deltaTime_m3253921619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1004423579 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t3722313464  Vector3_get_one_m1629952498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m3053443106 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIWidget::get_color()
extern "C"  Color_t2555686324  UIWidget_get_color_m3938892930 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_color(UnityEngine.Color)
extern "C"  void UIWidget_set_color_m2288988844 (UIWidget_t3538521925 * __this, Color_t2555686324  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UILabel::set_text(System.String)
extern "C"  void UILabel_set_text_m1071532778 (UILabel_t3248798549 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UICamera::get_lastEventPosition()
extern "C"  Vector2_t2156229523  UICamera_get_lastEventPosition_m1448007412 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t3722313464  Transform_get_localScale_m129152068 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UILabel::get_printedSize()
extern "C"  Vector2_t2156229523  UILabel_get_printedSize_m1383441570 (UILabel_t3248798549 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m1874334613 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_width(System.Int32)
extern "C"  void UIWidget_set_width_m181008468 (UIWidget_t3538521925 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_height(System.Int32)
extern "C"  void UIWidget_set_height_m878974275 (UIWidget_t3538521925 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m56433566 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m3903216845 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t3722313464  Transform_get_lossyScale_m465496651 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1073399594 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3146388979 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_ViewportToWorldPoint_m4277738824 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::BroadcastMessage(System.String)
extern "C"  void GameObject_BroadcastMessage_m217296818 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::BroadcastMessage(System.String)
extern "C"  void Component_BroadcastMessage_m1248810302 (Component_t1923634451 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Keyframe__ctor_m1259609478 (Keyframe_t4206410242 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m1565662948 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m3457838305 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::get_amountPerDelta()
extern "C"  float UITweener_get_amountPerDelta_m1343789098 (UITweener_t260334902 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::DoUpdate()
extern "C"  void UITweener_DoUpdate_m1102478613 (UITweener_t260334902 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m4270080131 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m3457564332 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Sample(System.Single,System.Boolean)
extern "C"  void UITweener_Sample_m928485241 (UITweener_t260334902 * __this, float ___factor0, bool ___isFinished1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32)
#define List_1_get_Item_m1629109689(__this, p0, method) ((  EventDelegate_t2738326060 * (*) (List_1_t4210400802 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate,System.Boolean)
extern "C"  void EventDelegate_Add_m1750872839 (RuntimeObject * __this /* static, unused */, List_1_t4210400802 * ___list0, EventDelegate_t2738326060 * ___ev1, bool ___oneShot2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count()
#define List_1_get_Count_m1118803847(__this, method) ((  int32_t (*) (List_1_t4210400802 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// EventDelegate EventDelegate::Set(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t2738326060 * EventDelegate_Set_m3113464886 (RuntimeObject * __this /* static, unused */, List_1_t4210400802 * ___list0, Callback_t3139336517 * ___callback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegate::Set(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern "C"  void EventDelegate_Set_m3840208201 (RuntimeObject * __this /* static, unused */, List_1_t4210400802 * ___list0, EventDelegate_t2738326060 * ___del1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern "C"  void EventDelegate_Add_m2810218290 (RuntimeObject * __this /* static, unused */, List_1_t4210400802 * ___list0, EventDelegate_t2738326060 * ___ev1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<EventDelegate>::Remove(!0)
#define List_1_Remove_m1907737694(__this, p0, method) ((  bool (*) (List_1_t4210400802 *, EventDelegate_t2738326060 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Single UITweener::BounceLogic(System.Single)
extern "C"  float UITweener_BounceLogic_m1320068922 (UITweener_t260334902 * __this, float ___val0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m2125563588 (AnimationCurve_t3046754366 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m2006396688 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_WorldToScreenPoint_m3726311023 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m1358425599 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_rect()
extern "C"  Rect_t2360479859  Camera_get_rect_m2458154151 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m51778115 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, Rect_t2360479859  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern "C"  void Camera_set_rect_m521006799 (Camera_t4157153871 * __this, Rect_t2360479859  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m76971700 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometry::.ctor()
extern "C"  void UIGeometry__ctor_m3724890878 (UIGeometry_t1059483952 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::.ctor()
extern "C"  void UIRect__ctor_m1261317954 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Delegate::op_Inequality(System.Delegate,System.Delegate)
extern "C"  bool Delegate_op_Inequality_m1112333615 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Inequality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Inequality_m3625339929 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ResizeCollider()
extern "C"  void UIWidget_ResizeCollider_m1303972682 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UIWidget/Pivot UIWidget::get_pivot()
extern "C"  int32_t UIWidget_get_pivot_m3232390905 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 NGUIMath::GetPivotOffset(UIWidget/Pivot)
extern "C"  Vector2_t2156229523  NGUIMath_GetPivotOffset_m3409460361 (RuntimeObject * __this /* static, unused */, int32_t ___pv0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMath::AdjustWidget(UIWidget,System.Single,System.Single,System.Single,System.Single)
extern "C"  void NGUIMath_AdjustWidget_m1901830214 (RuntimeObject * __this /* static, unused */, UIWidget_t3538521925 * ___w0, float ___left1, float ___bottom2, float ___right3, float ___top4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::SetDimensions(System.Int32,System.Int32)
extern "C"  void UIWidget_SetDimensions_m2627586629 (UIWidget_t3538521925 * __this, int32_t ___w0, int32_t ___h1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::op_Inequality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Inequality_m3353772181 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGeometry::get_hasVertices()
extern "C"  bool UIGeometry_get_hasVertices_m3715418080 (UIGeometry_t1059483952 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIRect::get_cachedTransform()
extern "C"  Transform_t3600365921 * UIRect_get_cachedTransform_m3314958558 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanel::RemoveWidget(UIWidget)
extern "C"  void UIPanel_RemoveWidget_m2262424736 (UIPanel_t1716472341 * __this, UIWidget_t3538521925 * ___w0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanel::AddWidget(UIWidget)
extern "C"  void UIPanel_AddWidget_m4245952570 (UIPanel_t1716472341 * __this, UIWidget_t3538521925 * ___w0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanel::SortWidgets()
extern "C"  void UIPanel_SortWidgets_m346874278 (UIPanel_t1716472341 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanel::RebuildAllDrawCalls()
extern "C"  void UIPanel_RebuildAllDrawCalls_m3198038699 (UIPanel_t1716472341 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UIPanel UIWidget::CreatePanel()
extern "C"  UIPanel_t1716472341 * UIWidget_CreatePanel_m976889642 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPanel::get_depth()
extern "C"  int32_t UIPanel_get_depth_m1723958116 (UIPanel_t1716472341 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWidget::get_pivotOffset()
extern "C"  Vector2_t2156229523  UIWidget_get_pivotOffset_m1997789874 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m1719387948 (Vector3_t3722313464 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(System.Single,System.Single,System.Single)
extern "C"  Vector3_t3722313464  Transform_TransformPoint_m4024714202 (Transform_t3600365921 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIWidget::get_localCenter()
extern "C"  Vector3_t3722313464  UIWidget_get_localCenter_m3035087122 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_TransformPoint_m226827784 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::RemoveFromPanel()
extern "C"  void UIWidget_RemoveFromPanel_m725257632 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern "C"  Texture_t3661962703 * Material_get_mainTexture_m692510677 (Material_t340375123 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor(System.String)
extern "C"  void NotImplementedException__ctor_m3095902440 (NotImplementedException_t3489357830 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C"  Shader_t4151988712 * Material_get_shader_m1331119247 (Material_t340375123 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2156229523  Vector2_get_one_m738793577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273(__this, method) ((  BoxCollider2D_t3581341831 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_InverseTransformPoint_m1343916000 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::UpdateFinalAlpha(System.Int32)
extern "C"  void UIWidget_UpdateFinalAlpha_m4075768229 (UIWidget_t3538521925 * __this, int32_t ___frameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UIRect UIRect::get_parent()
extern "C"  UIRect_t2875960382 * UIRect_get_parent_m3902033658 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanel::get_hasCumulativeClipping()
extern "C"  bool UIPanel_get_hasCumulativeClipping_m1863233802 (UIPanel_t1716472341 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanel::IsVisible(UIWidget)
extern "C"  bool UIPanel_IsVisible_m4063952194 (UIPanel_t1716472341 * __this, UIWidget_t3538521925 * ___w0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C"  int32_t Time_get_frameCount_m1220035214 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::CalculateCumulativeAlpha(System.Int32)
extern "C"  float UIWidget_CalculateCumulativeAlpha_m49421948 (UIWidget_t3538521925 * __this, int32_t ___frameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::UpdateVisibility(System.Boolean,System.Boolean)
extern "C"  bool UIWidget_UpdateVisibility_m2152012452 (UIWidget_t3538521925 * __this, bool ___visibleByAlpha0, bool ___visibleByPanel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::Invalidate(System.Boolean)
extern "C"  void UIRect_Invalidate_m3183458831 (UIRect_t2875960382 * __this, bool ___includeChildren0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m1870542928 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRect::get_isAnchored()
extern "C"  bool UIRect_get_isAnchored_m1025914852 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect/AnchorPoint::SetHorizontal(UnityEngine.Transform,System.Single)
extern "C"  void AnchorPoint_SetHorizontal_m3420307353 (AnchorPoint_t1754718329 * __this, Transform_t3600365921 * ___parent0, float ___localPos1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect/AnchorPoint::SetVertical(UnityEngine.Transform,System.Single)
extern "C"  void AnchorPoint_SetVertical_m4045229769 (AnchorPoint_t1754718329 * __this, Transform_t3600365921 * ___parent0, float ___localPos1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider>()
#define Component_GetComponent_TisBoxCollider_t1640800422_m4104100802(__this, method) ((  BoxCollider_t1640800422 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void NGUITools::UpdateWidgetCollider(UIWidget,UnityEngine.BoxCollider)
extern "C"  void NGUITools_UpdateWidgetCollider_m3438335504 (RuntimeObject * __this /* static, unused */, UIWidget_t3538521925 * ___w0, BoxCollider_t1640800422 * ___box1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITools::UpdateWidgetCollider(UIWidget,UnityEngine.BoxCollider2D)
extern "C"  void NGUITools_UpdateWidgetCollider_m1925672297 (RuntimeObject * __this /* static, unused */, UIWidget_t3538521925 * ___w0, BoxCollider2D_t3581341831 * ___box1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPanel::CompareFunc(UIPanel,UIPanel)
extern "C"  int32_t UIPanel_CompareFunc_m2640156642 (RuntimeObject * __this /* static, unused */, UIPanel_t1716472341 * ___a0, UIPanel_t1716472341 * ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::PanelCompareFunc(UIWidget,UIWidget)
extern "C"  int32_t UIWidget_PanelCompareFunc_m3717063172 (RuntimeObject * __this /* static, unused */, UIWidget_t3538521925 * ___left0, UIWidget_t3538521925 * ___right1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m1255174761 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIWidget::CalculateBounds(UnityEngine.Transform)
extern "C"  Bounds_t2266837910  UIWidget_CalculateBounds_m3008782085 (UIWidget_t3538521925 * __this, Transform_t3600365921 * ___relativeParent0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m1937678907 (Bounds_t2266837910 * __this, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m3553480203 (Bounds_t2266837910 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t1817901843  Transform_get_worldToLocalMatrix_m399704877 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Matrix4x4_MultiplyPoint3x4_m4145063176 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::get_isVisible()
extern "C"  bool UIWidget_get_isVisible_m524426372 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::get_hasVertices()
extern "C"  bool UIWidget_get_hasVertices_m2576649473 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITools::GetActive(UnityEngine.GameObject)
extern "C"  bool NGUITools_GetActive_m1538523522 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::SetDirty()
extern "C"  void UIWidget_SetDirty_m2297015651 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::CheckLayer()
extern "C"  void UIWidget_CheckLayer_m2684013386 (UIWidget_t3538521925 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIRect::get_cachedGameObject()
extern "C"  GameObject_t1113636619 * UIRect_get_cachedGameObject_m4173353535 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UIPanel UIPanel::Find(UnityEngine.Transform,System.Boolean,System.Int32)
extern "C"  UIPanel_t1716472341 * UIPanel_Find_m92918396 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___trans0, bool ___createIfMissing1, int32_t ___layer2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m2186869729 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t631007953 * ___context1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m3294992795 (GameObject_t1113636619 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ParentHasChanged()
extern "C"  void UIRect_ParentHasChanged_m4139252803 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::Awake()
extern "C"  void UIRect_Awake_m1429700095 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::OnInit()
extern "C"  void UIRect_OnInit_m1393697898 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::Update()
extern "C"  void UIRect_Update_m267147630 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern "C"  int32_t Mathf_Abs_m2460432655 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITools::UpdateWidgetCollider(UnityEngine.GameObject,System.Boolean)
extern "C"  void NGUITools_UpdateWidgetCollider_m2045900173 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, bool ___considerInactive1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIRect/AnchorPoint::GetSides(UnityEngine.Transform)
extern "C"  Vector3U5BU5D_t1718750761* AnchorPoint_GetSides_m3751877420 (AnchorPoint_t1754718329 * __this, Transform_t3600365921 * ___relativeTo0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single NGUIMath::Lerp(System.Single,System.Single,System.Single)
extern "C"  float NGUIMath_Lerp_m466078211 (RuntimeObject * __this /* static, unused */, float ___from0, float ___to1, float ___factor2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIRect::GetLocalPos(UIRect/AnchorPoint,UnityEngine.Transform)
extern "C"  Vector3_t3722313464  UIRect_GetLocalPos_m1776190051 (UIRect_t2875960382 * __this, AnchorPoint_t1754718329 * ___ac0, Transform_t3600365921 * ___trans1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m3025115945 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::OnDisable()
extern "C"  void UIRect_OnDisable_m201565937 (UIRect_t2875960382 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_hasChanged(System.Boolean)
extern "C"  void Transform_set_hasChanged_m4213723989 (Transform_t3600365921 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Transform::get_hasChanged()
extern "C"  bool Transform_get_hasChanged_m186929804 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget/OnDimensionsChanged::Invoke()
extern "C"  void OnDimensionsChanged_Invoke_m1136944693 (OnDimensionsChanged_t3101921181 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometry::Clear()
extern "C"  void UIGeometry_Clear_m3150037314 (UIGeometry_t1059483952 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t1817901843  Transform_get_localToWorldMatrix_m4155710351 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t1817901843  Matrix4x4_op_Multiply_m1876492807 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometry::ApplyTransform(UnityEngine.Matrix4x4,System.Boolean)
extern "C"  void UIGeometry_ApplyTransform_m8241091 (UIGeometry_t1059483952 * __this, Matrix4x4_t1817901843  ___widgetToPanel0, bool ___generateNormals1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometry::WriteToBuffers(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void UIGeometry_WriteToBuffers_m139827744 (UIGeometry_t1059483952 * __this, List_1_t899420910 * ___v0, List_1_t3628304265 * ___u1, List_1_t4027761066 * ___c2, List_1_t899420910 * ___n3, List_1_t496136383 * ___t4, List_1_t496136383 * ___u25, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t3319028937  Vector4_get_zero_m1422399515 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget/HitCheck::Invoke(UnityEngine.Vector3)
extern "C"  bool HitCheck_Invoke_m902107116 (HitCheck_t2300079615 * __this, Vector3_t3722313464  ___worldPos0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget/OnPostFillCallback::Invoke(UIWidget,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern "C"  void OnPostFillCallback_Invoke_m394319234 (OnPostFillCallback_t2835645043 * __this, UIWidget_t3538521925 * ___widget0, int32_t ___bufferOffset1, List_1_t899420910 * ___verts2, List_1_t3628304265 * ___uvs3, List_1_t4027761066 * ___cols4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
#define List_1__ctor_m2885667311(__this, method) ((  void (*) (List_1_t777473367 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIPanel>()
#define Component_GetComponent_TisUIPanel_t1716472341_m3980802358(__this, method) ((  UIPanel_t1716472341 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UIPanel/OnClippingMoved::.ctor(System.Object,System.IntPtr)
extern "C"  void OnClippingMoved__ctor_m1106797729 (OnClippingMoved_t476625095 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWrapContent::CacheScrollView()
extern "C"  bool UIWrapContent_CacheScrollView_m1573533213 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Clear()
#define List_1_Clear_m3082658015(__this, method) ((  void (*) (List_1_t777473367 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0)
#define List_1_Add_m4073477735(__this, p0, method) ((  void (*) (List_1_t777473367 *, Transform_t3600365921 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m3145433196 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Comparison`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m2800470641(__this, p0, p1, method) ((  void (*) (Comparison_1_t3375297100 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Comparison_1__ctor_m793514796_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Sort(System.Comparison`1<!0>)
#define List_1_Sort_m620079343(__this, p0, method) ((  void (*) (List_1_t777473367 *, Comparison_1_t3375297100 *, const RuntimeMethod*))List_1_Sort_m2076177611_gshared)(__this, p0, method)
// T NGUITools::FindInParents<UIPanel>(UnityEngine.GameObject)
#define NGUITools_FindInParents_TisUIPanel_t1716472341_m1961265283(__this /* static, unused */, ___go0, method) ((  UIPanel_t1716472341 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, const RuntimeMethod*))NGUITools_FindInParents_TisRuntimeObject_m780518502_gshared)(__this /* static, unused */, ___go0, method)
// !!0 UnityEngine.Component::GetComponent<UIScrollView>()
#define Component_GetComponent_TisUIScrollView_t1973404950_m746780642(__this, method) ((  UIScrollView_t1973404950 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
#define List_1_get_Count_m3787308655(__this, method) ((  int32_t (*) (List_1_t777473367 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
#define List_1_get_Item_m3022113929(__this, p0, method) ((  Transform_t3600365921 * (*) (List_1_t777473367 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// UnityEngine.Vector2 UIPanel::get_clipOffset()
extern "C"  Vector2_t2156229523  UIPanel_get_clipOffset_m110674167 (UIPanel_t1716472341 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsPressed(UnityEngine.GameObject)
extern "C"  bool UICamera_IsPressed_m2472947029 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITools::SetActive(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern "C"  void NGUITools_SetActive_m1909334044 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, bool ___state1, bool ___compatibilityMode2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::InvalidateBounds()
extern "C"  void UIScrollView_InvalidateBounds_m1519753606 (UIScrollView_t1973404950 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent/OnInitializeItem::Invoke(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void OnInitializeItem_Invoke_m4231678144 (OnInitializeItem_t992046894 * __this, GameObject_t1113636619 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Resources::Load<UnityProjectSetting>(System.String)
#define Resources_Load_TisUnityProjectSetting_t2579824760_m3201061709(__this /* static, unused */, p0, method) ((  UnityProjectSetting_t2579824760 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m597869152_gshared)(__this /* static, unused */, p0, method)
// System.Void Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m1051815641 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t631007953 * ___context1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<ShareLocation,System.String>::.ctor()
#define Dictionary_2__ctor_m3989434104(__this, method) ((  void (*) (Dictionary_2_t3173579796 *, const RuntimeMethod*))Dictionary_2__ctor_m1989985705_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ShareLocation,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m3554053801(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3173579796 *, int32_t, String_t*, const RuntimeMethod*))Dictionary_2_Add_m3965601654_gshared)(__this, p0, p1, method)
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m2150679437 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UserData::get_PlayCount()
extern "C"  int32_t UserData_get_PlayCount_m3442301827 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::Save(System.String,System.Int32)
extern "C"  void PlayerPrefsUtility_Save_m1276614609 (RuntimeObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UserData::get_IsMute()
extern "C"  bool UserData_get_IsMute_m2788164951 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::Save(System.String,System.Boolean)
extern "C"  void PlayerPrefsUtility_Save_m3487088120 (RuntimeObject * __this /* static, unused */, String_t* ___key0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime UserData::get_TimeOfPushedHouseAdButton()
extern "C"  DateTime_t3738529785  UserData_get_TimeOfPushedHouseAdButton_m195643557 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::Save(System.String,System.DateTime)
extern "C"  void PlayerPrefsUtility_Save_m2171612309 (RuntimeObject * __this /* static, unused */, String_t* ___key0, DateTime_t3738529785  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UserData::get_HighScoreDict()
extern "C"  Dictionary_2_t2736202052 * UserData_get_HighScoreDict_m1377977320 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SaveDict<System.String,System.Int32>(System.String,System.Collections.Generic.Dictionary`2<Key,Value>)
#define PlayerPrefsUtility_SaveDict_TisString_t_TisInt32_t2950945753_m1477132359(__this /* static, unused */, ___key0, ___value1, method) ((  void (*) (RuntimeObject * /* static, unused */, String_t*, Dictionary_2_t2736202052 *, const RuntimeMethod*))PlayerPrefsUtility_SaveDict_TisRuntimeObject_TisInt32_t2950945753_m2988034944_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C"  void PlayerPrefs_Save_m2701929462 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Debug::Log(System.Object,UnityEngine.Object)
extern "C"  void Debug_Log_m3276510664 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t631007953 * ___context1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayerPrefsUtility::Load(System.String,System.Int32)
extern "C"  int32_t PlayerPrefsUtility_Load_m3492684851 (RuntimeObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<Key,Value> PlayerPrefsUtility::LoadDict<System.String,System.Int32>(System.String)
#define PlayerPrefsUtility_LoadDict_TisString_t_TisInt32_t2950945753_m1536884824(__this /* static, unused */, ___key0, method) ((  Dictionary_2_t2736202052 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))PlayerPrefsUtility_LoadDict_TisRuntimeObject_TisInt32_t2950945753_m3311238834_gshared)(__this /* static, unused */, ___key0, method)
// TValue DictionaryExtensions::GetValueOrDefault<System.String,System.Int32>(System.Collections.Generic.IDictionary`2<TKey,TValue>,TKey,TValue)
#define DictionaryExtensions_GetValueOrDefault_TisString_t_TisInt32_t2950945753_m3168452452(__this /* static, unused */, ___source0, ___key1, ___defaultValue2, method) ((  int32_t (*) (RuntimeObject * /* static, unused */, RuntimeObject*, String_t*, int32_t, const RuntimeMethod*))DictionaryExtensions_GetValueOrDefault_TisRuntimeObject_TisInt32_t2950945753_m2660330963_gshared)(__this /* static, unused */, ___source0, ___key1, ___defaultValue2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3800595820(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2736202052 *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_set_Item_m411961606_gshared)(__this, p0, p1, method)
// System.Boolean PlayerPrefsUtility::Load(System.String,System.Boolean)
extern "C"  bool PlayerPrefsUtility_Load_m3631663835 (RuntimeObject * __this /* static, unused */, String_t* ___key0, bool ___defaultValue1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
#define Action_1_Invoke_m1933767679(__this, p0, method) ((  void (*) (Action_1_t269755560 *, bool, const RuntimeMethod*))Action_1_Invoke_m1933767679_gshared)(__this, p0, method)
// System.DateTime PlayerPrefsUtility::Load(System.String,System.DateTime)
extern "C"  DateTime_t3738529785  PlayerPrefsUtility_Load_m3694695221 (RuntimeObject * __this /* static, unused */, String_t* ___key0, DateTime_t3738529785  ___defaultValue1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor()
#define Dictionary_2__ctor_m3200964102(__this, method) ((  void (*) (Dictionary_2_t2736202052 *, const RuntimeMethod*))Dictionary_2__ctor_m2253601317_gshared)(__this, method)
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m981112613(__this, p0, p1, method) ((  void (*) (Action_1_t269755560 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m981112613_gshared)(__this, p0, p1, method)
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.DateTimeKind)
extern "C"  void DateTime__ctor_m2956360140 (DateTime_t3738529785 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Division_m510815599 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern "C"  float Rigidbody2D_get_angularVelocity_m1959705066 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t2156229523  Rigidbody2D_get_velocity_m366589732 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
extern "C"  Vector3_t3722313464  Rigidbody_get_angularVelocity_m191123884 (Rigidbody_t3916780224 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t3722313464  Rigidbody_get_velocity_m2993632669 (Rigidbody_t3916780224 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIToggle::.ctor()
extern "C"  void UIToggle__ctor_m243404431 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle__ctor_m243404431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4210400802 * L_0 = (List_1_t4210400802 *)il2cpp_codegen_object_new(List_1_t4210400802_il2cpp_TypeInfo_var);
		List_1__ctor_m2541037942(L_0, /*hidden argument*/List_1__ctor_m2541037942_RuntimeMethod_var);
		__this->set_onChange_13(L_0);
		__this->set_functionName_18(_stringLiteral416754645);
		__this->set_mIsActive_20((bool)1);
		UIWidgetContainer__ctor_m155952688(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIToggle::get_value()
extern "C"  bool UIToggle_get_value_m2062763629 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	bool G_B3_0 = false;
	{
		bool L_0 = __this->get_mStarted_21();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_mIsActive_20();
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		bool L_2 = __this->get_startsActive_10();
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UIToggle::set_value(System.Boolean)
extern "C"  void UIToggle_set_value_m3790403027 (UIToggle_t4192126258 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mStarted_21();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = ___value0;
		__this->set_startsActive_10(L_1);
		goto IL_0046;
	}

IL_0017:
	{
		int32_t L_2 = __this->get_group_4();
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		bool L_3 = ___value0;
		if (L_3)
		{
			goto IL_003e;
		}
	}
	{
		bool L_4 = __this->get_optionCanBeNone_12();
		if (L_4)
		{
			goto IL_003e;
		}
	}
	{
		bool L_5 = __this->get_mStarted_21();
		if (L_5)
		{
			goto IL_0046;
		}
	}

IL_003e:
	{
		bool L_6 = ___value0;
		UIToggle_Set_m2784184886(__this, L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Boolean UIToggle::get_isColliderEnabled()
extern "C"  bool UIToggle_get_isColliderEnabled_m2508236936 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_get_isColliderEnabled_m2508236936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider_t1773347010 * V_0 = NULL;
	Collider2D_t2806799626 * V_1 = NULL;
	int32_t G_B5_0 = 0;
	{
		Collider_t1773347010 * L_0 = Component_GetComponent_TisCollider_t1773347010_m4226749020(__this, /*hidden argument*/Component_GetComponent_TisCollider_t1773347010_m4226749020_RuntimeMethod_var);
		V_0 = L_0;
		Collider_t1773347010 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Collider_t1773347010 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Collider_get_enabled_m3096904824(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t2806799626 * L_5 = Component_GetComponent_TisCollider2D_t2806799626_m3391829639(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t2806799626_m3391829639_RuntimeMethod_var);
		V_1 = L_5;
		Collider2D_t2806799626 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		Collider2D_t2806799626 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m753527255(L_8, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_9));
		goto IL_0036;
	}

IL_0035:
	{
		G_B5_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UIToggle::get_isChecked()
extern "C"  bool UIToggle_get_isChecked_m3963534214 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = UIToggle_get_value_m2062763629(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UIToggle::set_isChecked(System.Boolean)
extern "C"  void UIToggle_set_isChecked_m1431487896 (UIToggle_t4192126258 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		UIToggle_set_value_m3790403027(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UIToggle UIToggle::GetActiveToggle(System.Int32)
extern "C"  UIToggle_t4192126258 * UIToggle_GetActiveToggle_m1985772813 (RuntimeObject * __this /* static, unused */, int32_t ___group0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_GetActiveToggle_m1985772813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	UIToggle_t4192126258 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_0 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		UIToggle_t4192126258 * L_2 = BetterList_1_get_Item_m2983367427(L_0, L_1, /*hidden argument*/BetterList_1_get_Item_m2983367427_RuntimeMethod_var);
		V_1 = L_2;
		UIToggle_t4192126258 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		UIToggle_t4192126258 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_group_4();
		int32_t L_7 = ___group0;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0038;
		}
	}
	{
		UIToggle_t4192126258 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = L_8->get_mIsActive_20();
		if (!L_9)
		{
			goto IL_0038;
		}
	}
	{
		UIToggle_t4192126258 * L_10 = V_1;
		return L_10;
	}

IL_0038:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_003c:
	{
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_13 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_size_1();
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return (UIToggle_t4192126258 *)NULL;
	}
}
// System.Void UIToggle::OnEnable()
extern "C"  void UIToggle_OnEnable_m3551203455 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_OnEnable_m3551203455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_0 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		NullCheck(L_0);
		BetterList_1_Add_m1919439375(L_0, __this, /*hidden argument*/BetterList_1_Add_m1919439375_RuntimeMethod_var);
		return;
	}
}
// System.Void UIToggle::OnDisable()
extern "C"  void UIToggle_OnDisable_m2874800824 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_OnDisable_m2874800824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_0 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		NullCheck(L_0);
		BetterList_1_Remove_m2601698983(L_0, __this, /*hidden argument*/BetterList_1_Remove_m2601698983_RuntimeMethod_var);
		return;
	}
}
// System.Void UIToggle::Start()
extern "C"  void UIToggle_Start_m4167712475 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_Start_m4167712475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	UIWidget_t3538521925 * G_B18_0 = NULL;
	UIWidget_t3538521925 * G_B14_0 = NULL;
	UIWidget_t3538521925 * G_B16_0 = NULL;
	UIWidget_t3538521925 * G_B15_0 = NULL;
	float G_B17_0 = 0.0f;
	UIWidget_t3538521925 * G_B17_1 = NULL;
	float G_B21_0 = 0.0f;
	UIWidget_t3538521925 * G_B21_1 = NULL;
	UIWidget_t3538521925 * G_B20_0 = NULL;
	UIWidget_t3538521925 * G_B19_0 = NULL;
	{
		bool L_0 = __this->get_mStarted_21();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = __this->get_startsChecked_19();
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		__this->set_startsChecked_19((bool)0);
		__this->set_startsActive_10((bool)1);
	}

IL_0025:
	{
		bool L_2 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0126;
		}
	}
	{
		UISprite_t194114938 * L_3 = __this->get_checkSprite_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0064;
		}
	}
	{
		UIWidget_t3538521925 * L_5 = __this->get_activeSprite_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0064;
		}
	}
	{
		UISprite_t194114938 * L_7 = __this->get_checkSprite_15();
		__this->set_activeSprite_5(L_7);
		__this->set_checkSprite_15((UISprite_t194114938 *)NULL);
	}

IL_0064:
	{
		Animation_t3648466861 * L_8 = __this->get_checkAnimation_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0099;
		}
	}
	{
		Animation_t3648466861 * L_10 = __this->get_activeAnimation_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_10, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0099;
		}
	}
	{
		Animation_t3648466861 * L_12 = __this->get_checkAnimation_16();
		__this->set_activeAnimation_7(L_12);
		__this->set_checkAnimation_16((Animation_t3648466861 *)NULL);
	}

IL_0099:
	{
		bool L_13 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0103;
		}
	}
	{
		UIWidget_t3538521925 * L_14 = __this->get_activeSprite_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_14, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0103;
		}
	}
	{
		UIWidget_t3538521925 * L_16 = __this->get_activeSprite_5();
		bool L_17 = __this->get_invertSpriteState_6();
		G_B14_0 = L_16;
		if (!L_17)
		{
			G_B18_0 = L_16;
			goto IL_00e4;
		}
	}
	{
		bool L_18 = __this->get_startsActive_10();
		G_B15_0 = G_B14_0;
		if (!L_18)
		{
			G_B16_0 = G_B14_0;
			goto IL_00da;
		}
	}
	{
		G_B17_0 = (0.0f);
		G_B17_1 = G_B15_0;
		goto IL_00df;
	}

IL_00da:
	{
		G_B17_0 = (1.0f);
		G_B17_1 = G_B16_0;
	}

IL_00df:
	{
		G_B21_0 = G_B17_0;
		G_B21_1 = G_B17_1;
		goto IL_00fe;
	}

IL_00e4:
	{
		bool L_19 = __this->get_startsActive_10();
		G_B19_0 = G_B18_0;
		if (!L_19)
		{
			G_B20_0 = G_B18_0;
			goto IL_00f9;
		}
	}
	{
		G_B21_0 = (1.0f);
		G_B21_1 = G_B19_0;
		goto IL_00fe;
	}

IL_00f9:
	{
		G_B21_0 = (0.0f);
		G_B21_1 = G_B20_0;
	}

IL_00fe:
	{
		NullCheck(G_B21_1);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIRect::set_alpha(System.Single) */, G_B21_1, G_B21_0);
	}

IL_0103:
	{
		List_1_t4210400802 * L_20 = __this->get_onChange_13();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		bool L_21 = EventDelegate_IsValid_m1359220059(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0121;
		}
	}
	{
		__this->set_eventReceiver_17((GameObject_t1113636619 *)NULL);
		__this->set_functionName_18((String_t*)NULL);
	}

IL_0121:
	{
		goto IL_015e;
	}

IL_0126:
	{
		bool L_22 = __this->get_startsActive_10();
		__this->set_mIsActive_20((bool)((((int32_t)L_22) == ((int32_t)0))? 1 : 0));
		__this->set_mStarted_21((bool)1);
		bool L_23 = __this->get_instantTween_11();
		V_0 = L_23;
		__this->set_instantTween_11((bool)1);
		bool L_24 = __this->get_startsActive_10();
		UIToggle_Set_m2784184886(__this, L_24, (bool)1, /*hidden argument*/NULL);
		bool L_25 = V_0;
		__this->set_instantTween_11(L_25);
	}

IL_015e:
	{
		return;
	}
}
// System.Void UIToggle::OnClick()
extern "C"  void UIToggle_OnClick_m1132153859 (UIToggle_t4192126258 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_OnClick_m1132153859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		bool L_1 = UIToggle_get_isColliderEnabled_m2508236936(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t1356438871_il2cpp_TypeInfo_var);
		int32_t L_2 = ((UICamera_t1356438871_StaticFields*)il2cpp_codegen_static_fields_for(UICamera_t1356438871_il2cpp_TypeInfo_var))->get_currentTouchID_54();
		if ((((int32_t)L_2) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0031;
		}
	}
	{
		bool L_3 = UIToggle_get_value_m2062763629(__this, /*hidden argument*/NULL);
		UIToggle_set_value_m3790403027(__this, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UIToggle::Set(System.Boolean,System.Boolean)
extern "C"  void UIToggle_Set_m2784184886 (UIToggle_t4192126258 * __this, bool ___state0, bool ___notify1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_Set_m2784184886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	UIToggle_t4192126258 * V_2 = NULL;
	UIToggle_t4192126258 * V_3 = NULL;
	ActiveAnimation_t3475256642 * V_4 = NULL;
	ActiveAnimation_t3475256642 * V_5 = NULL;
	bool V_6 = false;
	UITweenerU5BU5D_t3440034099* V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	UITweener_t260334902 * V_10 = NULL;
	UIWidget_t3538521925 * G_B10_0 = NULL;
	UIWidget_t3538521925 * G_B6_0 = NULL;
	UIWidget_t3538521925 * G_B8_0 = NULL;
	UIWidget_t3538521925 * G_B7_0 = NULL;
	float G_B9_0 = 0.0f;
	UIWidget_t3538521925 * G_B9_1 = NULL;
	float G_B13_0 = 0.0f;
	UIWidget_t3538521925 * G_B13_1 = NULL;
	UIWidget_t3538521925 * G_B12_0 = NULL;
	UIWidget_t3538521925 * G_B11_0 = NULL;
	UIWidget_t3538521925 * G_B34_0 = NULL;
	UIWidget_t3538521925 * G_B30_0 = NULL;
	UIWidget_t3538521925 * G_B32_0 = NULL;
	UIWidget_t3538521925 * G_B31_0 = NULL;
	float G_B33_0 = 0.0f;
	UIWidget_t3538521925 * G_B33_1 = NULL;
	float G_B37_0 = 0.0f;
	UIWidget_t3538521925 * G_B37_1 = NULL;
	UIWidget_t3538521925 * G_B36_0 = NULL;
	UIWidget_t3538521925 * G_B35_0 = NULL;
	float G_B43_0 = 0.0f;
	GameObject_t1113636619 * G_B43_1 = NULL;
	float G_B39_0 = 0.0f;
	GameObject_t1113636619 * G_B39_1 = NULL;
	float G_B41_0 = 0.0f;
	GameObject_t1113636619 * G_B41_1 = NULL;
	float G_B40_0 = 0.0f;
	GameObject_t1113636619 * G_B40_1 = NULL;
	float G_B42_0 = 0.0f;
	float G_B42_1 = 0.0f;
	GameObject_t1113636619 * G_B42_2 = NULL;
	float G_B46_0 = 0.0f;
	float G_B46_1 = 0.0f;
	GameObject_t1113636619 * G_B46_2 = NULL;
	float G_B45_0 = 0.0f;
	GameObject_t1113636619 * G_B45_1 = NULL;
	float G_B44_0 = 0.0f;
	GameObject_t1113636619 * G_B44_1 = NULL;
	RuntimeObject * G_B58_0 = NULL;
	Animator_t434523843 * G_B58_1 = NULL;
	RuntimeObject * G_B57_0 = NULL;
	Animator_t434523843 * G_B57_1 = NULL;
	int32_t G_B59_0 = 0;
	RuntimeObject * G_B59_1 = NULL;
	Animator_t434523843 * G_B59_2 = NULL;
	RuntimeObject * G_B67_0 = NULL;
	Animation_t3648466861 * G_B67_1 = NULL;
	RuntimeObject * G_B66_0 = NULL;
	Animation_t3648466861 * G_B66_1 = NULL;
	int32_t G_B68_0 = 0;
	RuntimeObject * G_B68_1 = NULL;
	Animation_t3648466861 * G_B68_2 = NULL;
	UITweener_t260334902 * G_B81_0 = NULL;
	UITweener_t260334902 * G_B80_0 = NULL;
	float G_B82_0 = 0.0f;
	UITweener_t260334902 * G_B82_1 = NULL;
	UITweener_t260334902 * G_B90_0 = NULL;
	UITweener_t260334902 * G_B89_0 = NULL;
	float G_B91_0 = 0.0f;
	UITweener_t260334902 * G_B91_1 = NULL;
	{
		Validate_t3702293971 * L_0 = __this->get_validator_14();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Validate_t3702293971 * L_1 = __this->get_validator_14();
		bool L_2 = ___state0;
		NullCheck(L_1);
		bool L_3 = Validate_Invoke_m3691890711(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		bool L_4 = __this->get_mStarted_21();
		if (L_4)
		{
			goto IL_0091;
		}
	}
	{
		bool L_5 = ___state0;
		__this->set_mIsActive_20(L_5);
		bool L_6 = ___state0;
		__this->set_startsActive_10(L_6);
		UIWidget_t3538521925 * L_7 = __this->get_activeSprite_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_7, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_008c;
		}
	}
	{
		UIWidget_t3538521925 * L_9 = __this->get_activeSprite_5();
		bool L_10 = __this->get_invertSpriteState_6();
		G_B6_0 = L_9;
		if (!L_10)
		{
			G_B10_0 = L_9;
			goto IL_0072;
		}
	}
	{
		bool L_11 = ___state0;
		G_B7_0 = G_B6_0;
		if (!L_11)
		{
			G_B8_0 = G_B6_0;
			goto IL_0068;
		}
	}
	{
		G_B9_0 = (0.0f);
		G_B9_1 = G_B7_0;
		goto IL_006d;
	}

IL_0068:
	{
		G_B9_0 = (1.0f);
		G_B9_1 = G_B8_0;
	}

IL_006d:
	{
		G_B13_0 = G_B9_0;
		G_B13_1 = G_B9_1;
		goto IL_0087;
	}

IL_0072:
	{
		bool L_12 = ___state0;
		G_B11_0 = G_B10_0;
		if (!L_12)
		{
			G_B12_0 = G_B10_0;
			goto IL_0082;
		}
	}
	{
		G_B13_0 = (1.0f);
		G_B13_1 = G_B11_0;
		goto IL_0087;
	}

IL_0082:
	{
		G_B13_0 = (0.0f);
		G_B13_1 = G_B12_0;
	}

IL_0087:
	{
		NullCheck(G_B13_1);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIRect::set_alpha(System.Single) */, G_B13_1, G_B13_0);
	}

IL_008c:
	{
		goto IL_042a;
	}

IL_0091:
	{
		bool L_13 = __this->get_mIsActive_20();
		bool L_14 = ___state0;
		if ((((int32_t)L_13) == ((int32_t)L_14)))
		{
			goto IL_042a;
		}
	}
	{
		int32_t L_15 = __this->get_group_4();
		if (!L_15)
		{
			goto IL_011e;
		}
	}
	{
		bool L_16 = ___state0;
		if (!L_16)
		{
			goto IL_011e;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_17 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		NullCheck(L_17);
		int32_t L_18 = L_17->get_size_1();
		V_1 = L_18;
		goto IL_0117;
	}

IL_00c0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_19 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		UIToggle_t4192126258 * L_21 = BetterList_1_get_Item_m2983367427(L_19, L_20, /*hidden argument*/BetterList_1_get_Item_m2983367427_RuntimeMethod_var);
		V_2 = L_21;
		UIToggle_t4192126258 * L_22 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_23 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_22, __this, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00f1;
		}
	}
	{
		UIToggle_t4192126258 * L_24 = V_2;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_group_4();
		int32_t L_26 = __this->get_group_4();
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_00f1;
		}
	}
	{
		UIToggle_t4192126258 * L_27 = V_2;
		NullCheck(L_27);
		UIToggle_Set_m2784184886(L_27, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_28 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		NullCheck(L_28);
		int32_t L_29 = L_28->get_size_1();
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) == ((int32_t)L_30)))
		{
			goto IL_0113;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		BetterList_1_t3347146576 * L_31 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_list_2();
		NullCheck(L_31);
		int32_t L_32 = L_31->get_size_1();
		V_1 = L_32;
		V_0 = 0;
		goto IL_0117;
	}

IL_0113:
	{
		int32_t L_33 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_0117:
	{
		int32_t L_34 = V_0;
		int32_t L_35 = V_1;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_00c0;
		}
	}

IL_011e:
	{
		bool L_36 = ___state0;
		__this->set_mIsActive_20(L_36);
		UIWidget_t3538521925 * L_37 = __this->get_activeSprite_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_37, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_01ff;
		}
	}
	{
		bool L_39 = __this->get_instantTween_11();
		if (L_39)
		{
			goto IL_014c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_40 = NGUITools_GetActive_m3370498562(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_01a0;
		}
	}

IL_014c:
	{
		UIWidget_t3538521925 * L_41 = __this->get_activeSprite_5();
		bool L_42 = __this->get_invertSpriteState_6();
		G_B30_0 = L_41;
		if (!L_42)
		{
			G_B34_0 = L_41;
			goto IL_017c;
		}
	}
	{
		bool L_43 = __this->get_mIsActive_20();
		G_B31_0 = G_B30_0;
		if (!L_43)
		{
			G_B32_0 = G_B30_0;
			goto IL_0172;
		}
	}
	{
		G_B33_0 = (0.0f);
		G_B33_1 = G_B31_0;
		goto IL_0177;
	}

IL_0172:
	{
		G_B33_0 = (1.0f);
		G_B33_1 = G_B32_0;
	}

IL_0177:
	{
		G_B37_0 = G_B33_0;
		G_B37_1 = G_B33_1;
		goto IL_0196;
	}

IL_017c:
	{
		bool L_44 = __this->get_mIsActive_20();
		G_B35_0 = G_B34_0;
		if (!L_44)
		{
			G_B36_0 = G_B34_0;
			goto IL_0191;
		}
	}
	{
		G_B37_0 = (1.0f);
		G_B37_1 = G_B35_0;
		goto IL_0196;
	}

IL_0191:
	{
		G_B37_0 = (0.0f);
		G_B37_1 = G_B36_0;
	}

IL_0196:
	{
		NullCheck(G_B37_1);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIRect::set_alpha(System.Single) */, G_B37_1, G_B37_0);
		goto IL_01ff;
	}

IL_01a0:
	{
		UIWidget_t3538521925 * L_45 = __this->get_activeSprite_5();
		NullCheck(L_45);
		GameObject_t1113636619 * L_46 = Component_get_gameObject_m442555142(L_45, /*hidden argument*/NULL);
		bool L_47 = __this->get_invertSpriteState_6();
		G_B39_0 = (0.15f);
		G_B39_1 = L_46;
		if (!L_47)
		{
			G_B43_0 = (0.15f);
			G_B43_1 = L_46;
			goto IL_01da;
		}
	}
	{
		bool L_48 = __this->get_mIsActive_20();
		G_B40_0 = G_B39_0;
		G_B40_1 = G_B39_1;
		if (!L_48)
		{
			G_B41_0 = G_B39_0;
			G_B41_1 = G_B39_1;
			goto IL_01d0;
		}
	}
	{
		G_B42_0 = (0.0f);
		G_B42_1 = G_B40_0;
		G_B42_2 = G_B40_1;
		goto IL_01d5;
	}

IL_01d0:
	{
		G_B42_0 = (1.0f);
		G_B42_1 = G_B41_0;
		G_B42_2 = G_B41_1;
	}

IL_01d5:
	{
		G_B46_0 = G_B42_0;
		G_B46_1 = G_B42_1;
		G_B46_2 = G_B42_2;
		goto IL_01f4;
	}

IL_01da:
	{
		bool L_49 = __this->get_mIsActive_20();
		G_B44_0 = G_B43_0;
		G_B44_1 = G_B43_1;
		if (!L_49)
		{
			G_B45_0 = G_B43_0;
			G_B45_1 = G_B43_1;
			goto IL_01ef;
		}
	}
	{
		G_B46_0 = (1.0f);
		G_B46_1 = G_B44_0;
		G_B46_2 = G_B44_1;
		goto IL_01f4;
	}

IL_01ef:
	{
		G_B46_0 = (0.0f);
		G_B46_1 = G_B45_0;
		G_B46_2 = G_B45_1;
	}

IL_01f4:
	{
		TweenAlpha_Begin_m799587892(NULL /*static, unused*/, G_B46_2, G_B46_1, G_B46_0, (0.0f), /*hidden argument*/NULL);
	}

IL_01ff:
	{
		bool L_50 = ___notify1;
		if (!L_50)
		{
			goto IL_0285;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		UIToggle_t4192126258 * L_51 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_current_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_52 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_51, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0285;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		UIToggle_t4192126258 * L_53 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_current_3();
		V_3 = L_53;
		((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->set_current_3(__this);
		List_1_t4210400802 * L_54 = __this->get_onChange_13();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		bool L_55 = EventDelegate_IsValid_m1359220059(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0241;
		}
	}
	{
		List_1_t4210400802 * L_56 = __this->get_onChange_13();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Execute_m2610939864(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		goto IL_027f;
	}

IL_0241:
	{
		GameObject_t1113636619 * L_57 = __this->get_eventReceiver_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_58 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_57, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_027f;
		}
	}
	{
		String_t* L_59 = __this->get_functionName_18();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_027f;
		}
	}
	{
		GameObject_t1113636619 * L_61 = __this->get_eventReceiver_17();
		String_t* L_62 = __this->get_functionName_18();
		bool L_63 = __this->get_mIsActive_20();
		bool L_64 = L_63;
		RuntimeObject * L_65 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_61);
		GameObject_SendMessage_m3720186693(L_61, L_62, L_65, 1, /*hidden argument*/NULL);
	}

IL_027f:
	{
		UIToggle_t4192126258 * L_66 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->set_current_3(L_66);
	}

IL_0285:
	{
		Animator_t434523843 * L_67 = __this->get_animator_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_68 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_67, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_02e2;
		}
	}
	{
		Animator_t434523843 * L_69 = __this->get_animator_8();
		bool L_70 = ___state0;
		G_B57_0 = NULL;
		G_B57_1 = L_69;
		if (!L_70)
		{
			G_B58_0 = NULL;
			G_B58_1 = L_69;
			goto IL_02a9;
		}
	}
	{
		G_B59_0 = 1;
		G_B59_1 = G_B57_0;
		G_B59_2 = G_B57_1;
		goto IL_02aa;
	}

IL_02a9:
	{
		G_B59_0 = (-1);
		G_B59_1 = G_B58_0;
		G_B59_2 = G_B58_1;
	}

IL_02aa:
	{
		ActiveAnimation_t3475256642 * L_71 = ActiveAnimation_Play_m2569601364(NULL /*static, unused*/, G_B59_2, (String_t*)G_B59_1, G_B59_0, 2, 0, /*hidden argument*/NULL);
		V_4 = L_71;
		ActiveAnimation_t3475256642 * L_72 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_73 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_72, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_02dd;
		}
	}
	{
		bool L_74 = __this->get_instantTween_11();
		if (L_74)
		{
			goto IL_02d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_75 = NGUITools_GetActive_m3370498562(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_75)
		{
			goto IL_02dd;
		}
	}

IL_02d6:
	{
		ActiveAnimation_t3475256642 * L_76 = V_4;
		NullCheck(L_76);
		ActiveAnimation_Finish_m3064184920(L_76, /*hidden argument*/NULL);
	}

IL_02dd:
	{
		goto IL_042a;
	}

IL_02e2:
	{
		Animation_t3648466861 * L_77 = __this->get_activeAnimation_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_78 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_77, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_033f;
		}
	}
	{
		Animation_t3648466861 * L_79 = __this->get_activeAnimation_7();
		bool L_80 = ___state0;
		G_B66_0 = NULL;
		G_B66_1 = L_79;
		if (!L_80)
		{
			G_B67_0 = NULL;
			G_B67_1 = L_79;
			goto IL_0306;
		}
	}
	{
		G_B68_0 = 1;
		G_B68_1 = G_B66_0;
		G_B68_2 = G_B66_1;
		goto IL_0307;
	}

IL_0306:
	{
		G_B68_0 = (-1);
		G_B68_1 = G_B67_0;
		G_B68_2 = G_B67_1;
	}

IL_0307:
	{
		ActiveAnimation_t3475256642 * L_81 = ActiveAnimation_Play_m2479515133(NULL /*static, unused*/, G_B68_2, (String_t*)G_B68_1, G_B68_0, 2, 0, /*hidden argument*/NULL);
		V_5 = L_81;
		ActiveAnimation_t3475256642 * L_82 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_83 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_82, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_83)
		{
			goto IL_033a;
		}
	}
	{
		bool L_84 = __this->get_instantTween_11();
		if (L_84)
		{
			goto IL_0333;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_85 = NGUITools_GetActive_m3370498562(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_033a;
		}
	}

IL_0333:
	{
		ActiveAnimation_t3475256642 * L_86 = V_5;
		NullCheck(L_86);
		ActiveAnimation_Finish_m3064184920(L_86, /*hidden argument*/NULL);
	}

IL_033a:
	{
		goto IL_042a;
	}

IL_033f:
	{
		UITweener_t260334902 * L_87 = __this->get_tween_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_88 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_87, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_042a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_89 = NGUITools_GetActive_m3370498562(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_6 = L_89;
		UITweener_t260334902 * L_90 = __this->get_tween_9();
		NullCheck(L_90);
		int32_t L_91 = L_90->get_tweenGroup_10();
		if (!L_91)
		{
			goto IL_03ec;
		}
	}
	{
		UITweener_t260334902 * L_92 = __this->get_tween_9();
		NullCheck(L_92);
		UITweenerU5BU5D_t3440034099* L_93 = Component_GetComponentsInChildren_TisUITweener_t260334902_m2859153738(L_92, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisUITweener_t260334902_m2859153738_RuntimeMethod_var);
		V_7 = L_93;
		V_8 = 0;
		UITweenerU5BU5D_t3440034099* L_94 = V_7;
		NullCheck(L_94);
		V_9 = (((int32_t)((int32_t)(((RuntimeArray *)L_94)->max_length))));
		goto IL_03de;
	}

IL_0384:
	{
		UITweenerU5BU5D_t3440034099* L_95 = V_7;
		int32_t L_96 = V_8;
		NullCheck(L_95);
		int32_t L_97 = L_96;
		UITweener_t260334902 * L_98 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		V_10 = L_98;
		UITweener_t260334902 * L_99 = V_10;
		NullCheck(L_99);
		int32_t L_100 = L_99->get_tweenGroup_10();
		UITweener_t260334902 * L_101 = __this->get_tween_9();
		NullCheck(L_101);
		int32_t L_102 = L_101->get_tweenGroup_10();
		if ((!(((uint32_t)L_100) == ((uint32_t)L_102))))
		{
			goto IL_03d8;
		}
	}
	{
		UITweener_t260334902 * L_103 = V_10;
		bool L_104 = ___state0;
		NullCheck(L_103);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void UITweener::Play(System.Boolean) */, L_103, L_104);
		bool L_105 = __this->get_instantTween_11();
		if (L_105)
		{
			goto IL_03bc;
		}
	}
	{
		bool L_106 = V_6;
		if (L_106)
		{
			goto IL_03d8;
		}
	}

IL_03bc:
	{
		UITweener_t260334902 * L_107 = V_10;
		bool L_108 = ___state0;
		G_B80_0 = L_107;
		if (!L_108)
		{
			G_B81_0 = L_107;
			goto IL_03ce;
		}
	}
	{
		G_B82_0 = (1.0f);
		G_B82_1 = G_B80_0;
		goto IL_03d3;
	}

IL_03ce:
	{
		G_B82_0 = (0.0f);
		G_B82_1 = G_B81_0;
	}

IL_03d3:
	{
		NullCheck(G_B82_1);
		UITweener_set_tweenFactor_m3630546225(G_B82_1, G_B82_0, /*hidden argument*/NULL);
	}

IL_03d8:
	{
		int32_t L_109 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_109, (int32_t)1));
	}

IL_03de:
	{
		int32_t L_110 = V_8;
		int32_t L_111 = V_9;
		if ((((int32_t)L_110) < ((int32_t)L_111)))
		{
			goto IL_0384;
		}
	}
	{
		goto IL_042a;
	}

IL_03ec:
	{
		UITweener_t260334902 * L_112 = __this->get_tween_9();
		bool L_113 = ___state0;
		NullCheck(L_112);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void UITweener::Play(System.Boolean) */, L_112, L_113);
		bool L_114 = __this->get_instantTween_11();
		if (L_114)
		{
			goto IL_040a;
		}
	}
	{
		bool L_115 = V_6;
		if (L_115)
		{
			goto IL_042a;
		}
	}

IL_040a:
	{
		UITweener_t260334902 * L_116 = __this->get_tween_9();
		bool L_117 = ___state0;
		G_B89_0 = L_116;
		if (!L_117)
		{
			G_B90_0 = L_116;
			goto IL_0420;
		}
	}
	{
		G_B91_0 = (1.0f);
		G_B91_1 = G_B89_0;
		goto IL_0425;
	}

IL_0420:
	{
		G_B91_0 = (0.0f);
		G_B91_1 = G_B90_0;
	}

IL_0425:
	{
		NullCheck(G_B91_1);
		UITweener_set_tweenFactor_m3630546225(G_B91_1, G_B91_0, /*hidden argument*/NULL);
	}

IL_042a:
	{
		return;
	}
}
// System.Void UIToggle::.cctor()
extern "C"  void UIToggle__cctor_m2392246648 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggle__cctor_m2392246648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BetterList_1_t3347146576 * L_0 = (BetterList_1_t3347146576 *)il2cpp_codegen_object_new(BetterList_1_t3347146576_il2cpp_TypeInfo_var);
		BetterList_1__ctor_m2278349088(L_0, /*hidden argument*/BetterList_1__ctor_m2278349088_RuntimeMethod_var);
		((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->set_list_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  bool DelegatePInvokeWrapper_Validate_t3702293971 (Validate_t3702293971 * __this, bool ___choice0, const RuntimeMethod* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(static_cast<int32_t>(___choice0));

	return static_cast<bool>(returnValue);
}
// System.Void UIToggle/Validate::.ctor(System.Object,System.IntPtr)
extern "C"  void Validate__ctor_m1924015496 (Validate_t3702293971 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UIToggle/Validate::Invoke(System.Boolean)
extern "C"  bool Validate_Invoke_m3691890711 (Validate_t3702293971 * __this, bool ___choice0, const RuntimeMethod* method)
{
	bool result = false;
	if(__this->get_prev_9() != NULL)
	{
		Validate_Invoke_m3691890711((Validate_t3702293971 *)__this->get_prev_9(), ___choice0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef bool (*FunctionPointerType) (RuntimeObject *, bool, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(NULL, ___choice0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef bool (*FunctionPointerType) (RuntimeObject *, void*, bool, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___choice0, targetMethod);
			}
		}
	}
	else
	{
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< bool, bool >::Invoke(targetMethod, targetThis, ___choice0);
					else
						result = GenericVirtFuncInvoker1< bool, bool >::Invoke(targetMethod, targetThis, ___choice0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< bool, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___choice0);
					else
						result = VirtFuncInvoker1< bool, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___choice0);
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (void*, bool, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___choice0, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult UIToggle/Validate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Validate_BeginInvoke_m1750573099 (Validate_t3702293971 * __this, bool ___choice0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Validate_BeginInvoke_m1750573099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &___choice0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean UIToggle/Validate::EndInvoke(System.IAsyncResult)
extern "C"  bool Validate_EndInvoke_m820566977 (Validate_t3702293971 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIToggledComponents::.ctor()
extern "C"  void UIToggledComponents__ctor_m1400085656 (UIToggledComponents_t772955118 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledComponents::Awake()
extern "C"  void UIToggledComponents_Awake_m3830670224 (UIToggledComponents_t772955118 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggledComponents_Awake_m3830670224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIToggle_t4192126258 * V_0 = NULL;
	{
		MonoBehaviour_t3962482529 * L_0 = __this->get_target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006f;
		}
	}
	{
		List_1_t1139589975 * L_2 = __this->get_activate_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m430733211(L_2, /*hidden argument*/List_1_get_Count_m430733211_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_0068;
		}
	}
	{
		List_1_t1139589975 * L_4 = __this->get_deactivate_3();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m430733211(L_4, /*hidden argument*/List_1_get_Count_m430733211_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0068;
		}
	}
	{
		bool L_6 = __this->get_inverse_5();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		List_1_t1139589975 * L_7 = __this->get_deactivate_3();
		MonoBehaviour_t3962482529 * L_8 = __this->get_target_4();
		NullCheck(L_7);
		List_1_Add_m421447557(L_7, L_8, /*hidden argument*/List_1_Add_m421447557_RuntimeMethod_var);
		goto IL_0063;
	}

IL_0052:
	{
		List_1_t1139589975 * L_9 = __this->get_activate_2();
		MonoBehaviour_t3962482529 * L_10 = __this->get_target_4();
		NullCheck(L_9);
		List_1_Add_m421447557(L_9, L_10, /*hidden argument*/List_1_Add_m421447557_RuntimeMethod_var);
	}

IL_0063:
	{
		goto IL_006f;
	}

IL_0068:
	{
		__this->set_target_4((MonoBehaviour_t3962482529 *)NULL);
	}

IL_006f:
	{
		UIToggle_t4192126258 * L_11 = Component_GetComponent_TisUIToggle_t4192126258_m3922355289(__this, /*hidden argument*/Component_GetComponent_TisUIToggle_t4192126258_m3922355289_RuntimeMethod_var);
		V_0 = L_11;
		UIToggle_t4192126258 * L_12 = V_0;
		NullCheck(L_12);
		List_1_t4210400802 * L_13 = L_12->get_onChange_13();
		intptr_t L_14 = (intptr_t)UIToggledComponents_Toggle_m3061868441_RuntimeMethod_var;
		Callback_t3139336517 * L_15 = (Callback_t3139336517 *)il2cpp_codegen_object_new(Callback_t3139336517_il2cpp_TypeInfo_var);
		Callback__ctor_m3761074887(L_15, __this, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1448281318(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledComponents::Toggle()
extern "C"  void UIToggledComponents_Toggle_m3061868441 (UIToggledComponents_t772955118 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggledComponents_Toggle_m3061868441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MonoBehaviour_t3962482529 * V_1 = NULL;
	int32_t V_2 = 0;
	MonoBehaviour_t3962482529 * V_3 = NULL;
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0080;
		}
	}
	{
		V_0 = 0;
		goto IL_0033;
	}

IL_0012:
	{
		List_1_t1139589975 * L_1 = __this->get_activate_2();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		MonoBehaviour_t3962482529 * L_3 = List_1_get_Item_m3720941323(L_1, L_2, /*hidden argument*/List_1_get_Item_m3720941323_RuntimeMethod_var);
		V_1 = L_3;
		MonoBehaviour_t3962482529 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		UIToggle_t4192126258 * L_5 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_current_3();
		NullCheck(L_5);
		bool L_6 = UIToggle_get_value_m2062763629(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Behaviour_set_enabled_m20417929(L_4, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		List_1_t1139589975 * L_9 = __this->get_activate_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m430733211(L_9, /*hidden argument*/List_1_get_Count_m430733211_RuntimeMethod_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0012;
		}
	}
	{
		V_2 = 0;
		goto IL_006f;
	}

IL_004b:
	{
		List_1_t1139589975 * L_11 = __this->get_deactivate_3();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		MonoBehaviour_t3962482529 * L_13 = List_1_get_Item_m3720941323(L_11, L_12, /*hidden argument*/List_1_get_Item_m3720941323_RuntimeMethod_var);
		V_3 = L_13;
		MonoBehaviour_t3962482529 * L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		UIToggle_t4192126258 * L_15 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_current_3();
		NullCheck(L_15);
		bool L_16 = UIToggle_get_value_m2062763629(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Behaviour_set_enabled_m20417929(L_14, (bool)((((int32_t)L_16) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_006f:
	{
		int32_t L_18 = V_2;
		List_1_t1139589975 * L_19 = __this->get_deactivate_3();
		NullCheck(L_19);
		int32_t L_20 = List_1_get_Count_m430733211(L_19, /*hidden argument*/List_1_get_Count_m430733211_RuntimeMethod_var);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_004b;
		}
	}

IL_0080:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIToggledObjects::.ctor()
extern "C"  void UIToggledObjects__ctor_m3748195939 (UIToggledObjects_t3502557910 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledObjects::Awake()
extern "C"  void UIToggledObjects_Awake_m1805886258 (UIToggledObjects_t3502557910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggledObjects_Awake_m1805886258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIToggle_t4192126258 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006f;
		}
	}
	{
		List_1_t2585711361 * L_2 = __this->get_activate_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2812834599(L_2, /*hidden argument*/List_1_get_Count_m2812834599_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_0068;
		}
	}
	{
		List_1_t2585711361 * L_4 = __this->get_deactivate_3();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m2812834599(L_4, /*hidden argument*/List_1_get_Count_m2812834599_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0068;
		}
	}
	{
		bool L_6 = __this->get_inverse_5();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		List_1_t2585711361 * L_7 = __this->get_deactivate_3();
		GameObject_t1113636619 * L_8 = __this->get_target_4();
		NullCheck(L_7);
		List_1_Add_m2765963565(L_7, L_8, /*hidden argument*/List_1_Add_m2765963565_RuntimeMethod_var);
		goto IL_0063;
	}

IL_0052:
	{
		List_1_t2585711361 * L_9 = __this->get_activate_2();
		GameObject_t1113636619 * L_10 = __this->get_target_4();
		NullCheck(L_9);
		List_1_Add_m2765963565(L_9, L_10, /*hidden argument*/List_1_Add_m2765963565_RuntimeMethod_var);
	}

IL_0063:
	{
		goto IL_006f;
	}

IL_0068:
	{
		__this->set_target_4((GameObject_t1113636619 *)NULL);
	}

IL_006f:
	{
		UIToggle_t4192126258 * L_11 = Component_GetComponent_TisUIToggle_t4192126258_m3922355289(__this, /*hidden argument*/Component_GetComponent_TisUIToggle_t4192126258_m3922355289_RuntimeMethod_var);
		V_0 = L_11;
		UIToggle_t4192126258 * L_12 = V_0;
		NullCheck(L_12);
		List_1_t4210400802 * L_13 = L_12->get_onChange_13();
		intptr_t L_14 = (intptr_t)UIToggledObjects_Toggle_m1261088717_RuntimeMethod_var;
		Callback_t3139336517 * L_15 = (Callback_t3139336517 *)il2cpp_codegen_object_new(Callback_t3139336517_il2cpp_TypeInfo_var);
		Callback__ctor_m3761074887(L_15, __this, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1448281318(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledObjects::Toggle()
extern "C"  void UIToggledObjects_Toggle_m1261088717 (UIToggledObjects_t3502557910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggledObjects_Toggle_m1261088717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t4192126258_il2cpp_TypeInfo_var);
		UIToggle_t4192126258 * L_0 = ((UIToggle_t4192126258_StaticFields*)il2cpp_codegen_static_fields_for(UIToggle_t4192126258_il2cpp_TypeInfo_var))->get_current_3();
		NullCheck(L_0);
		bool L_1 = UIToggle_get_value_m2062763629(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0077;
		}
	}
	{
		V_1 = 0;
		goto IL_0034;
	}

IL_001d:
	{
		List_1_t2585711361 * L_3 = __this->get_activate_2();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		GameObject_t1113636619 * L_5 = List_1_get_Item_m3743125852(L_3, L_4, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		bool L_6 = V_0;
		UIToggledObjects_Set_m2281364091(__this, L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0034:
	{
		int32_t L_8 = V_1;
		List_1_t2585711361 * L_9 = __this->get_activate_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m2812834599(L_9, /*hidden argument*/List_1_get_Count_m2812834599_RuntimeMethod_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_001d;
		}
	}
	{
		V_2 = 0;
		goto IL_0066;
	}

IL_004c:
	{
		List_1_t2585711361 * L_11 = __this->get_deactivate_3();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		GameObject_t1113636619 * L_13 = List_1_get_Item_m3743125852(L_11, L_12, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		bool L_14 = V_0;
		UIToggledObjects_Set_m2281364091(__this, L_13, (bool)((((int32_t)L_14) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_2;
		List_1_t2585711361 * L_17 = __this->get_deactivate_3();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2812834599(L_17, /*hidden argument*/List_1_get_Count_m2812834599_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_004c;
		}
	}

IL_0077:
	{
		return;
	}
}
// System.Void UIToggledObjects::Set(UnityEngine.GameObject,System.Boolean)
extern "C"  void UIToggledObjects_Set_m2281364091 (UIToggledObjects_t3502557910 * __this, GameObject_t1113636619 * ___go0, bool ___state1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIToggledObjects_Set_m2281364091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		GameObject_t1113636619 * L_2 = ___go0;
		bool L_3 = ___state1;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m3543859753(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UITooltip::.ctor()
extern "C"  void UITooltip__ctor_m3883812056 (UITooltip_t30236576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip__ctor_m3883812056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_appearSpeed_7((10.0f));
		__this->set_scalingTransitions_8((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mSize_14(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UITooltip::get_isVisible()
extern "C"  bool UITooltip_get_isVisible_m2569275211 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_get_isVisible_m2569275211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		UITooltip_t30236576 * L_0 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		UITooltip_t30236576 * L_2 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		NullCheck(L_2);
		float L_3 = L_2->get_mTarget_11();
		G_B3_0 = ((((float)L_3) == ((float)(1.0f)))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UITooltip::Awake()
extern "C"  void UITooltip_Awake_m2533168584 (UITooltip_t30236576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Awake_m2533168584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->set_mInstance_2(__this);
		return;
	}
}
// System.Void UITooltip::OnDestroy()
extern "C"  void UITooltip_OnDestroy_m3731988078 (UITooltip_t30236576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_OnDestroy_m3731988078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->set_mInstance_2((UITooltip_t30236576 *)NULL);
		return;
	}
}
// System.Void UITooltip::Start()
extern "C"  void UITooltip_Start_m589901658 (UITooltip_t30236576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Start_m589901658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_10(L_0);
		UIWidgetU5BU5D_t2950248968* L_1 = Component_GetComponentsInChildren_TisUIWidget_t3538521925_m3698995734(__this, /*hidden argument*/Component_GetComponentsInChildren_TisUIWidget_t3538521925_m3698995734_RuntimeMethod_var);
		__this->set_mWidgets_15(L_1);
		Transform_t3600365921 * L_2 = __this->get_mTrans_10();
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_localPosition_m4234289348(L_2, /*hidden argument*/NULL);
		__this->set_mPos_13(L_3);
		Camera_t4157153871 * L_4 = __this->get_uiCamera_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = GameObject_get_layer_m4158800245(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		Camera_t4157153871 * L_8 = NGUITools_FindCameraForLayer_m709671062(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_uiCamera_3(L_8);
	}

IL_0050:
	{
		VirtActionInvoker1< float >::Invoke(6 /* System.Void UITooltip::SetAlpha(System.Single) */, __this, (0.0f));
		return;
	}
}
// System.Void UITooltip::Update()
extern "C"  void UITooltip_Update_m2144972122 (UITooltip_t30236576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Update_m2144972122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GameObject_t1113636619 * L_0 = __this->get_mTooltip_9();
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t1356438871_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_1 = UICamera_get_tooltipObject_m2829094911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_mTooltip_9((GameObject_t1113636619 *)NULL);
		__this->set_mTarget_11((0.0f));
	}

IL_0027:
	{
		float L_3 = __this->get_mCurrent_12();
		float L_4 = __this->get_mTarget_11();
		if ((((float)L_3) == ((float)L_4)))
		{
			goto IL_0114;
		}
	}
	{
		float L_5 = __this->get_mCurrent_12();
		float L_6 = __this->get_mTarget_11();
		float L_7 = RealTime_get_deltaTime_m3253921619(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_appearSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_5, L_6, ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		__this->set_mCurrent_12(L_9);
		float L_10 = __this->get_mCurrent_12();
		float L_11 = __this->get_mTarget_11();
		float L_12 = fabsf(((float)il2cpp_codegen_subtract((float)L_10, (float)L_11)));
		if ((!(((float)L_12) < ((float)(0.001f)))))
		{
			goto IL_0083;
		}
	}
	{
		float L_13 = __this->get_mTarget_11();
		__this->set_mCurrent_12(L_13);
	}

IL_0083:
	{
		float L_14 = __this->get_mCurrent_12();
		float L_15 = __this->get_mCurrent_12();
		VirtActionInvoker1< float >::Invoke(6 /* System.Void UITooltip::SetAlpha(System.Single) */, __this, ((float)il2cpp_codegen_multiply((float)L_14, (float)L_15)));
		bool L_16 = __this->get_scalingTransitions_8();
		if (!L_16)
		{
			goto IL_0114;
		}
	}
	{
		Vector3_t3722313464  L_17 = __this->get_mSize_14();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_18 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_17, (0.25f), /*hidden argument*/NULL);
		V_0 = L_18;
		float L_19 = (&V_0)->get_y_2();
		(&V_0)->set_y_2(((-L_19)));
		Vector3_t3722313464  L_20 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = __this->get_mCurrent_12();
		Vector3_t3722313464  L_22 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_20, ((float)il2cpp_codegen_subtract((float)(1.5f), (float)((float)il2cpp_codegen_multiply((float)L_21, (float)(0.5f))))), /*hidden argument*/NULL);
		V_1 = L_22;
		Vector3_t3722313464  L_23 = __this->get_mPos_13();
		Vector3_t3722313464  L_24 = V_0;
		Vector3_t3722313464  L_25 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Vector3_t3722313464  L_26 = __this->get_mPos_13();
		float L_27 = __this->get_mCurrent_12();
		Vector3_t3722313464  L_28 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		Transform_t3600365921 * L_29 = __this->get_mTrans_10();
		Vector3_t3722313464  L_30 = V_2;
		NullCheck(L_29);
		Transform_set_localPosition_m4128471975(L_29, L_30, /*hidden argument*/NULL);
		Transform_t3600365921 * L_31 = __this->get_mTrans_10();
		Vector3_t3722313464  L_32 = V_1;
		NullCheck(L_31);
		Transform_set_localScale_m3053443106(L_31, L_32, /*hidden argument*/NULL);
	}

IL_0114:
	{
		return;
	}
}
// System.Void UITooltip::SetAlpha(System.Single)
extern "C"  void UITooltip_SetAlpha_m1761138400 (UITooltip_t30236576 * __this, float ___val0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	UIWidget_t3538521925 * V_2 = NULL;
	Color_t2555686324  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = 0;
		UIWidgetU5BU5D_t2950248968* L_0 = __this->get_mWidgets_15();
		NullCheck(L_0);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))));
		goto IL_0033;
	}

IL_0010:
	{
		UIWidgetU5BU5D_t2950248968* L_1 = __this->get_mWidgets_15();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		UIWidget_t3538521925 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		UIWidget_t3538521925 * L_5 = V_2;
		NullCheck(L_5);
		Color_t2555686324  L_6 = UIWidget_get_color_m3938892930(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ___val0;
		(&V_3)->set_a_3(L_7);
		UIWidget_t3538521925 * L_8 = V_2;
		Color_t2555686324  L_9 = V_3;
		NullCheck(L_8);
		UIWidget_set_color_m2288988844(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void UITooltip::SetText(System.String)
extern "C"  void UITooltip_SetText_m869056702 (UITooltip_t30236576 * __this, String_t* ___tooltipText0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_SetText_m869056702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t3319028937  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		UILabel_t3248798549 * L_0 = __this->get_text_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_03e0;
		}
	}
	{
		String_t* L_2 = ___tooltipText0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_03e0;
		}
	}
	{
		__this->set_mTarget_11((1.0f));
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t1356438871_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_4 = UICamera_get_tooltipObject_m2829094911(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mTooltip_9(L_4);
		UILabel_t3248798549 * L_5 = __this->get_text_4();
		String_t* L_6 = ___tooltipText0;
		NullCheck(L_5);
		UILabel_set_text_m1071532778(L_5, L_6, /*hidden argument*/NULL);
		Vector2_t2156229523  L_7 = UICamera_get_lastEventPosition_m1448007412(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_mPos_13(L_8);
		UILabel_t3248798549 * L_9 = __this->get_text_4();
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Transform_t3600365921 * L_11 = V_0;
		NullCheck(L_11);
		Vector3_t3722313464  L_12 = Transform_get_localPosition_m4234289348(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Transform_t3600365921 * L_13 = V_0;
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = Transform_get_localScale_m129152068(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		UILabel_t3248798549 * L_15 = __this->get_text_4();
		NullCheck(L_15);
		Vector2_t2156229523  L_16 = UILabel_get_printedSize_m1383441570(L_15, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		__this->set_mSize_14(L_17);
		Vector3_t3722313464 * L_18 = __this->get_address_of_mSize_14();
		Vector3_t3722313464 * L_19 = L_18;
		float L_20 = L_19->get_x_1();
		float L_21 = (&V_2)->get_x_1();
		L_19->set_x_1(((float)il2cpp_codegen_multiply((float)L_20, (float)L_21)));
		Vector3_t3722313464 * L_22 = __this->get_address_of_mSize_14();
		Vector3_t3722313464 * L_23 = L_22;
		float L_24 = L_23->get_y_2();
		float L_25 = (&V_2)->get_y_2();
		L_23->set_y_2(((float)il2cpp_codegen_multiply((float)L_24, (float)L_25)));
		UISprite_t194114938 * L_26 = __this->get_background_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_26, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0172;
		}
	}
	{
		UISprite_t194114938 * L_28 = __this->get_background_6();
		NullCheck(L_28);
		Vector4_t3319028937  L_29 = VirtFuncInvoker0< Vector4_t3319028937  >::Invoke(36 /* UnityEngine.Vector4 UIWidget::get_border() */, L_28);
		V_3 = L_29;
		Vector3_t3722313464 * L_30 = __this->get_address_of_mSize_14();
		Vector3_t3722313464 * L_31 = L_30;
		float L_32 = L_31->get_x_1();
		float L_33 = (&V_3)->get_x_1();
		float L_34 = (&V_3)->get_z_3();
		float L_35 = (&V_1)->get_x_1();
		float L_36 = (&V_3)->get_x_1();
		L_31->set_x_1(((float)il2cpp_codegen_add((float)L_32, (float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_33, (float)L_34)), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_35, (float)L_36)), (float)(2.0f))))))));
		Vector3_t3722313464 * L_37 = __this->get_address_of_mSize_14();
		Vector3_t3722313464 * L_38 = L_37;
		float L_39 = L_38->get_y_2();
		float L_40 = (&V_3)->get_y_2();
		float L_41 = (&V_3)->get_w_4();
		float L_42 = (&V_1)->get_y_2();
		float L_43 = (&V_3)->get_y_2();
		L_38->set_y_2(((float)il2cpp_codegen_add((float)L_39, (float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_40, (float)L_41)), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)((-L_42)), (float)L_43)), (float)(2.0f))))))));
		UISprite_t194114938 * L_44 = __this->get_background_6();
		Vector3_t3722313464 * L_45 = __this->get_address_of_mSize_14();
		float L_46 = L_45->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_47 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		UIWidget_set_width_m181008468(L_44, L_47, /*hidden argument*/NULL);
		UISprite_t194114938 * L_48 = __this->get_background_6();
		Vector3_t3722313464 * L_49 = __this->get_address_of_mSize_14();
		float L_50 = L_49->get_y_2();
		int32_t L_51 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		UIWidget_set_height_m878974275(L_48, L_51, /*hidden argument*/NULL);
	}

IL_0172:
	{
		Camera_t4157153871 * L_52 = __this->get_uiCamera_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_53 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_52, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_02e2;
		}
	}
	{
		Vector3_t3722313464 * L_54 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_55 = __this->get_address_of_mPos_13();
		float L_56 = L_55->get_x_1();
		int32_t L_57 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_58 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)((float)L_56/(float)(((float)((float)L_57))))), /*hidden argument*/NULL);
		L_54->set_x_1(L_58);
		Vector3_t3722313464 * L_59 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_60 = __this->get_address_of_mPos_13();
		float L_61 = L_60->get_y_2();
		int32_t L_62 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_63 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)((float)L_61/(float)(((float)((float)L_62))))), /*hidden argument*/NULL);
		L_59->set_y_2(L_63);
		Camera_t4157153871 * L_64 = __this->get_uiCamera_3();
		NullCheck(L_64);
		float L_65 = Camera_get_orthographicSize_m3903216845(L_64, /*hidden argument*/NULL);
		Transform_t3600365921 * L_66 = __this->get_mTrans_10();
		NullCheck(L_66);
		Transform_t3600365921 * L_67 = Transform_get_parent_m835071599(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		Vector3_t3722313464  L_68 = Transform_get_lossyScale_m465496651(L_67, /*hidden argument*/NULL);
		V_5 = L_68;
		float L_69 = (&V_5)->get_y_2();
		V_4 = ((float)((float)L_65/(float)L_69));
		int32_t L_70 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_71 = V_4;
		V_6 = ((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_70))), (float)(0.5f)))/(float)L_71));
		float L_72 = V_6;
		Vector3_t3722313464 * L_73 = __this->get_address_of_mSize_14();
		float L_74 = L_73->get_x_1();
		int32_t L_75 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_76 = V_6;
		Vector3_t3722313464 * L_77 = __this->get_address_of_mSize_14();
		float L_78 = L_77->get_y_2();
		int32_t L_79 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m3970636864((&V_7), ((float)((float)((float)il2cpp_codegen_multiply((float)L_72, (float)L_74))/(float)(((float)((float)L_75))))), ((float)((float)((float)il2cpp_codegen_multiply((float)L_76, (float)L_78))/(float)(((float)((float)L_79))))), /*hidden argument*/NULL);
		Vector3_t3722313464 * L_80 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_81 = __this->get_address_of_mPos_13();
		float L_82 = L_81->get_x_1();
		float L_83 = (&V_7)->get_x_0();
		float L_84 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_82, ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_83)), /*hidden argument*/NULL);
		L_80->set_x_1(L_84);
		Vector3_t3722313464 * L_85 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_86 = __this->get_address_of_mPos_13();
		float L_87 = L_86->get_y_2();
		float L_88 = (&V_7)->get_y_1();
		float L_89 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_87, L_88, /*hidden argument*/NULL);
		L_85->set_y_2(L_89);
		Transform_t3600365921 * L_90 = __this->get_mTrans_10();
		Camera_t4157153871 * L_91 = __this->get_uiCamera_3();
		Vector3_t3722313464  L_92 = __this->get_mPos_13();
		NullCheck(L_91);
		Vector3_t3722313464  L_93 = Camera_ViewportToWorldPoint_m4277738824(L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_90);
		Transform_set_position_m3387557959(L_90, L_93, /*hidden argument*/NULL);
		Transform_t3600365921 * L_94 = __this->get_mTrans_10();
		NullCheck(L_94);
		Vector3_t3722313464  L_95 = Transform_get_localPosition_m4234289348(L_94, /*hidden argument*/NULL);
		__this->set_mPos_13(L_95);
		Vector3_t3722313464 * L_96 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_97 = __this->get_address_of_mPos_13();
		float L_98 = L_97->get_x_1();
		float L_99 = bankers_roundf(L_98);
		L_96->set_x_1(L_99);
		Vector3_t3722313464 * L_100 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_101 = __this->get_address_of_mPos_13();
		float L_102 = L_101->get_y_2();
		float L_103 = bankers_roundf(L_102);
		L_100->set_y_2(L_103);
		goto IL_0394;
	}

IL_02e2:
	{
		Vector3_t3722313464 * L_104 = __this->get_address_of_mPos_13();
		float L_105 = L_104->get_x_1();
		Vector3_t3722313464 * L_106 = __this->get_address_of_mSize_14();
		float L_107 = L_106->get_x_1();
		int32_t L_108 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_105, (float)L_107))) > ((float)(((float)((float)L_108)))))))
		{
			goto IL_0321;
		}
	}
	{
		Vector3_t3722313464 * L_109 = __this->get_address_of_mPos_13();
		int32_t L_110 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464 * L_111 = __this->get_address_of_mSize_14();
		float L_112 = L_111->get_x_1();
		L_109->set_x_1(((float)il2cpp_codegen_subtract((float)(((float)((float)L_110))), (float)L_112)));
	}

IL_0321:
	{
		Vector3_t3722313464 * L_113 = __this->get_address_of_mPos_13();
		float L_114 = L_113->get_y_2();
		Vector3_t3722313464 * L_115 = __this->get_address_of_mSize_14();
		float L_116 = L_115->get_y_2();
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_114, (float)L_116))) < ((float)(0.0f)))))
		{
			goto IL_0358;
		}
	}
	{
		Vector3_t3722313464 * L_117 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_118 = __this->get_address_of_mSize_14();
		float L_119 = L_118->get_y_2();
		L_117->set_y_2(L_119);
	}

IL_0358:
	{
		Vector3_t3722313464 * L_120 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_121 = L_120;
		float L_122 = L_121->get_x_1();
		int32_t L_123 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_121->set_x_1(((float)il2cpp_codegen_subtract((float)L_122, (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_123))), (float)(0.5f))))));
		Vector3_t3722313464 * L_124 = __this->get_address_of_mPos_13();
		Vector3_t3722313464 * L_125 = L_124;
		float L_126 = L_125->get_y_2();
		int32_t L_127 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_125->set_y_2(((float)il2cpp_codegen_subtract((float)L_126, (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_127))), (float)(0.5f))))));
	}

IL_0394:
	{
		Transform_t3600365921 * L_128 = __this->get_mTrans_10();
		Vector3_t3722313464  L_129 = __this->get_mPos_13();
		NullCheck(L_128);
		Transform_set_localPosition_m4128471975(L_128, L_129, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_130 = __this->get_tooltipRoot_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_131 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_130, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_03cb;
		}
	}
	{
		GameObject_t1113636619 * L_132 = __this->get_tooltipRoot_5();
		NullCheck(L_132);
		GameObject_BroadcastMessage_m217296818(L_132, _stringLiteral1789911004, /*hidden argument*/NULL);
		goto IL_03db;
	}

IL_03cb:
	{
		UILabel_t3248798549 * L_133 = __this->get_text_4();
		NullCheck(L_133);
		Component_BroadcastMessage_m1248810302(L_133, _stringLiteral1789911004, /*hidden argument*/NULL);
	}

IL_03db:
	{
		goto IL_03f2;
	}

IL_03e0:
	{
		__this->set_mTooltip_9((GameObject_t1113636619 *)NULL);
		__this->set_mTarget_11((0.0f));
	}

IL_03f2:
	{
		return;
	}
}
// System.Void UITooltip::ShowText(System.String)
extern "C"  void UITooltip_ShowText_m571599872 (RuntimeObject * __this /* static, unused */, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_ShowText_m571599872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UITooltip_t30236576 * L_0 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UITooltip_t30236576 * L_2 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void UITooltip::SetText(System.String) */, L_2, L_3);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UITooltip::Show(System.String)
extern "C"  void UITooltip_Show_m110090701 (RuntimeObject * __this /* static, unused */, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Show_m110090701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UITooltip_t30236576 * L_0 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UITooltip_t30236576 * L_2 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void UITooltip::SetText(System.String) */, L_2, L_3);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UITooltip::Hide()
extern "C"  void UITooltip_Hide_m3169225227 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Hide_m3169225227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UITooltip_t30236576 * L_0 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		UITooltip_t30236576 * L_2 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		NullCheck(L_2);
		L_2->set_mTooltip_9((GameObject_t1113636619 *)NULL);
		UITooltip_t30236576 * L_3 = ((UITooltip_t30236576_StaticFields*)il2cpp_codegen_static_fields_for(UITooltip_t30236576_il2cpp_TypeInfo_var))->get_mInstance_2();
		NullCheck(L_3);
		L_3->set_mTarget_11((0.0f));
	}

IL_002a:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UITweener::.ctor()
extern "C"  void UITweener__ctor_m3207690653 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener__ctor_m3207690653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeyframeU5BU5D_t1068524471* L_0 = ((KeyframeU5BU5D_t1068524471*)SZArrayNew(KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		Keyframe_t4206410242  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m1259609478((&L_1), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		*(Keyframe_t4206410242 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_1;
		KeyframeU5BU5D_t1068524471* L_2 = L_0;
		NullCheck(L_2);
		Keyframe_t4206410242  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m1259609478((&L_3), (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		*(Keyframe_t4206410242 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_3;
		AnimationCurve_t3046754366 * L_4 = (AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m1565662948(L_4, L_2, /*hidden argument*/NULL);
		__this->set_animationCurve_5(L_4);
		__this->set_ignoreTimeScale_6((bool)1);
		__this->set_duration_8((1.0f));
		List_1_t4210400802 * L_5 = (List_1_t4210400802 *)il2cpp_codegen_object_new(List_1_t4210400802_il2cpp_TypeInfo_var);
		List_1__ctor_m2541037942(L_5, /*hidden argument*/List_1__ctor_m2541037942_RuntimeMethod_var);
		__this->set_onFinished_12(L_5);
		__this->set_timeScale_15((1.0f));
		__this->set_mAmountPerDelta_19((1000.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UITweener::get_amountPerDelta()
extern "C"  float UITweener_get_amountPerDelta_m1343789098 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_get_amountPerDelta_m1343789098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_duration_8();
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		return (1000.0f);
	}

IL_0016:
	{
		float L_1 = __this->get_mDuration_18();
		float L_2 = __this->get_duration_8();
		if ((((float)L_1) == ((float)L_2)))
		{
			goto IL_0056;
		}
	}
	{
		float L_3 = __this->get_duration_8();
		__this->set_mDuration_18(L_3);
		float L_4 = __this->get_duration_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_5 = fabsf(((float)((float)(1.0f)/(float)L_4)));
		float L_6 = __this->get_mAmountPerDelta_19();
		float L_7 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_mAmountPerDelta_19(((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)));
	}

IL_0056:
	{
		float L_8 = __this->get_mAmountPerDelta_19();
		return L_8;
	}
}
// System.Single UITweener::get_tweenFactor()
extern "C"  float UITweener_get_tweenFactor_m3909759543 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_mFactor_20();
		return L_0;
	}
}
// System.Void UITweener::set_tweenFactor(System.Single)
extern "C"  void UITweener_set_tweenFactor_m3630546225 (UITweener_t260334902 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_set_tweenFactor_m3630546225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_mFactor_20(L_1);
		return;
	}
}
// AnimationOrTween.Direction UITweener::get_direction()
extern "C"  int32_t UITweener_get_direction_m913175971 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = UITweener_get_amountPerDelta_m1343789098(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 1;
	}

IL_0017:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.Void UITweener::Reset()
extern "C"  void UITweener_Reset_m254358569 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mStarted_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		VirtActionInvoker0::Invoke(7 /* System.Void UITweener::SetStartToCurrentValue() */, __this);
		VirtActionInvoker0::Invoke(8 /* System.Void UITweener::SetEndToCurrentValue() */, __this);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UITweener::Start()
extern "C"  void UITweener_Start_m3441143879 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		UITweener_DoUpdate_m1102478613(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Update()
extern "C"  void UITweener_Update_m2219002051 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_useFixedUpdate_11();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		UITweener_DoUpdate_m1102478613(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void UITweener::FixedUpdate()
extern "C"  void UITweener_FixedUpdate_m1243691336 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_useFixedUpdate_11();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		UITweener_DoUpdate_m1102478613(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void UITweener::DoUpdate()
extern "C"  void UITweener_DoUpdate_m1102478613 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_DoUpdate_m1102478613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	UITweener_t260334902 * V_2 = NULL;
	int32_t V_3 = 0;
	EventDelegate_t2738326060 * V_4 = NULL;
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	float G_B14_0 = 0.0f;
	UITweener_t260334902 * G_B14_1 = NULL;
	float G_B13_0 = 0.0f;
	UITweener_t260334902 * G_B13_1 = NULL;
	float G_B15_0 = 0.0f;
	float G_B15_1 = 0.0f;
	UITweener_t260334902 * G_B15_2 = NULL;
	{
		bool L_0 = __this->get_ignoreTimeScale_6();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		bool L_1 = __this->get_useFixedUpdate_11();
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		float L_2 = Time_get_unscaledDeltaTime_m4270080131(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_2;
		goto IL_0025;
	}

IL_0020:
	{
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_3;
	}

IL_0025:
	{
		V_0 = G_B4_0;
		bool L_4 = __this->get_ignoreTimeScale_6();
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		bool L_5 = __this->get_useFixedUpdate_11();
		if (L_5)
		{
			goto IL_0046;
		}
	}
	{
		float L_6 = Time_get_unscaledTime_m3457564332(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_6;
		goto IL_004b;
	}

IL_0046:
	{
		float L_7 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
	}

IL_004b:
	{
		V_1 = G_B8_0;
		bool L_8 = __this->get_mStarted_16();
		if (L_8)
		{
			goto IL_0072;
		}
	}
	{
		V_0 = (0.0f);
		__this->set_mStarted_16((bool)1);
		float L_9 = V_1;
		float L_10 = __this->get_delay_7();
		__this->set_mStartTime_17(((float)il2cpp_codegen_add((float)L_9, (float)L_10)));
	}

IL_0072:
	{
		float L_11 = V_1;
		float L_12 = __this->get_mStartTime_17();
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_007f;
		}
	}
	{
		return;
	}

IL_007f:
	{
		float L_13 = __this->get_mFactor_20();
		float L_14 = __this->get_duration_8();
		G_B13_0 = L_13;
		G_B13_1 = __this;
		if ((!(((float)L_14) == ((float)(0.0f)))))
		{
			G_B14_0 = L_13;
			G_B14_1 = __this;
			goto IL_00a0;
		}
	}
	{
		G_B15_0 = (1.0f);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		goto IL_00af;
	}

IL_00a0:
	{
		float L_15 = UITweener_get_amountPerDelta_m1343789098(__this, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = __this->get_timeScale_15();
		G_B15_0 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_15, (float)L_16)), (float)L_17));
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
	}

IL_00af:
	{
		NullCheck(G_B15_2);
		G_B15_2->set_mFactor_20(((float)il2cpp_codegen_add((float)G_B15_1, (float)G_B15_0)));
		int32_t L_18 = __this->get_style_4();
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_00ee;
		}
	}
	{
		float L_19 = __this->get_mFactor_20();
		if ((!(((float)L_19) > ((float)(1.0f)))))
		{
			goto IL_00e9;
		}
	}
	{
		float L_20 = __this->get_mFactor_20();
		float L_21 = __this->get_mFactor_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = floorf(L_21);
		__this->set_mFactor_20(((float)il2cpp_codegen_subtract((float)L_20, (float)L_22)));
	}

IL_00e9:
	{
		goto IL_017c;
	}

IL_00ee:
	{
		int32_t L_23 = __this->get_style_4();
		if ((!(((uint32_t)L_23) == ((uint32_t)2))))
		{
			goto IL_017c;
		}
	}
	{
		float L_24 = __this->get_mFactor_20();
		if ((!(((float)L_24) > ((float)(1.0f)))))
		{
			goto IL_013a;
		}
	}
	{
		float L_25 = __this->get_mFactor_20();
		float L_26 = __this->get_mFactor_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_27 = floorf(L_26);
		__this->set_mFactor_20(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_subtract((float)L_25, (float)L_27)))));
		float L_28 = __this->get_mAmountPerDelta_19();
		__this->set_mAmountPerDelta_19(((-L_28)));
		goto IL_017c;
	}

IL_013a:
	{
		float L_29 = __this->get_mFactor_20();
		if ((!(((float)L_29) < ((float)(0.0f)))))
		{
			goto IL_017c;
		}
	}
	{
		float L_30 = __this->get_mFactor_20();
		__this->set_mFactor_20(((-L_30)));
		float L_31 = __this->get_mFactor_20();
		float L_32 = __this->get_mFactor_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_33 = floorf(L_32);
		__this->set_mFactor_20(((float)il2cpp_codegen_subtract((float)L_31, (float)L_33)));
		float L_34 = __this->get_mAmountPerDelta_19();
		__this->set_mAmountPerDelta_19(((-L_34)));
	}

IL_017c:
	{
		int32_t L_35 = __this->get_style_4();
		if (L_35)
		{
			goto IL_02bc;
		}
	}
	{
		float L_36 = __this->get_duration_8();
		if ((((float)L_36) == ((float)(0.0f))))
		{
			goto IL_01b7;
		}
	}
	{
		float L_37 = __this->get_mFactor_20();
		if ((((float)L_37) > ((float)(1.0f))))
		{
			goto IL_01b7;
		}
	}
	{
		float L_38 = __this->get_mFactor_20();
		if ((!(((float)L_38) < ((float)(0.0f)))))
		{
			goto IL_02bc;
		}
	}

IL_01b7:
	{
		float L_39 = __this->get_mFactor_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_40 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		__this->set_mFactor_20(L_40);
		float L_41 = __this->get_mFactor_20();
		UITweener_Sample_m928485241(__this, L_41, (bool)1, /*hidden argument*/NULL);
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
		UITweener_t260334902 * L_42 = ((UITweener_t260334902_StaticFields*)il2cpp_codegen_static_fields_for(UITweener_t260334902_il2cpp_TypeInfo_var))->get_current_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_42, __this, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_02b7;
		}
	}
	{
		UITweener_t260334902 * L_44 = ((UITweener_t260334902_StaticFields*)il2cpp_codegen_static_fields_for(UITweener_t260334902_il2cpp_TypeInfo_var))->get_current_2();
		V_2 = L_44;
		((UITweener_t260334902_StaticFields*)il2cpp_codegen_static_fields_for(UITweener_t260334902_il2cpp_TypeInfo_var))->set_current_2(__this);
		List_1_t4210400802 * L_45 = __this->get_onFinished_12();
		if (!L_45)
		{
			goto IL_027d;
		}
	}
	{
		List_1_t4210400802 * L_46 = __this->get_onFinished_12();
		__this->set_mTemp_21(L_46);
		List_1_t4210400802 * L_47 = (List_1_t4210400802 *)il2cpp_codegen_object_new(List_1_t4210400802_il2cpp_TypeInfo_var);
		List_1__ctor_m2541037942(L_47, /*hidden argument*/List_1__ctor_m2541037942_RuntimeMethod_var);
		__this->set_onFinished_12(L_47);
		List_1_t4210400802 * L_48 = __this->get_mTemp_21();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Execute_m2610939864(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0265;
	}

IL_022c:
	{
		List_1_t4210400802 * L_49 = __this->get_mTemp_21();
		int32_t L_50 = V_3;
		NullCheck(L_49);
		EventDelegate_t2738326060 * L_51 = List_1_get_Item_m1629109689(L_49, L_50, /*hidden argument*/List_1_get_Item_m1629109689_RuntimeMethod_var);
		V_4 = L_51;
		EventDelegate_t2738326060 * L_52 = V_4;
		if (!L_52)
		{
			goto IL_0261;
		}
	}
	{
		EventDelegate_t2738326060 * L_53 = V_4;
		NullCheck(L_53);
		bool L_54 = L_53->get_oneShot_3();
		if (L_54)
		{
			goto IL_0261;
		}
	}
	{
		List_1_t4210400802 * L_55 = __this->get_onFinished_12();
		EventDelegate_t2738326060 * L_56 = V_4;
		EventDelegate_t2738326060 * L_57 = V_4;
		NullCheck(L_57);
		bool L_58 = L_57->get_oneShot_3();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1750872839(NULL /*static, unused*/, L_55, L_56, L_58, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_59 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_59, (int32_t)1));
	}

IL_0265:
	{
		int32_t L_60 = V_3;
		List_1_t4210400802 * L_61 = __this->get_mTemp_21();
		NullCheck(L_61);
		int32_t L_62 = List_1_get_Count_m1118803847(L_61, /*hidden argument*/List_1_get_Count_m1118803847_RuntimeMethod_var);
		if ((((int32_t)L_60) < ((int32_t)L_62)))
		{
			goto IL_022c;
		}
	}
	{
		__this->set_mTemp_21((List_1_t4210400802 *)NULL);
	}

IL_027d:
	{
		GameObject_t1113636619 * L_63 = __this->get_eventReceiver_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_64 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_63, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_02b1;
		}
	}
	{
		String_t* L_65 = __this->get_callWhenFinished_14();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_66 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		if (L_66)
		{
			goto IL_02b1;
		}
	}
	{
		GameObject_t1113636619 * L_67 = __this->get_eventReceiver_13();
		String_t* L_68 = __this->get_callWhenFinished_14();
		NullCheck(L_67);
		GameObject_SendMessage_m3720186693(L_67, L_68, __this, 1, /*hidden argument*/NULL);
	}

IL_02b1:
	{
		UITweener_t260334902 * L_69 = V_2;
		((UITweener_t260334902_StaticFields*)il2cpp_codegen_static_fields_for(UITweener_t260334902_il2cpp_TypeInfo_var))->set_current_2(L_69);
	}

IL_02b7:
	{
		goto IL_02c9;
	}

IL_02bc:
	{
		float L_70 = __this->get_mFactor_20();
		UITweener_Sample_m928485241(__this, L_70, (bool)0, /*hidden argument*/NULL);
	}

IL_02c9:
	{
		return;
	}
}
// System.Void UITweener::SetOnFinished(EventDelegate/Callback)
extern "C"  void UITweener_SetOnFinished_m2969301297 (UITweener_t260334902 * __this, Callback_t3139336517 * ___del0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_SetOnFinished_m2969301297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4210400802 * L_0 = __this->get_onFinished_12();
		Callback_t3139336517 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Set_m3113464886(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::SetOnFinished(EventDelegate)
extern "C"  void UITweener_SetOnFinished_m1889836421 (UITweener_t260334902 * __this, EventDelegate_t2738326060 * ___del0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_SetOnFinished_m1889836421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4210400802 * L_0 = __this->get_onFinished_12();
		EventDelegate_t2738326060 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Set_m3840208201(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::AddOnFinished(EventDelegate/Callback)
extern "C"  void UITweener_AddOnFinished_m4181903323 (UITweener_t260334902 * __this, Callback_t3139336517 * ___del0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_AddOnFinished_m4181903323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4210400802 * L_0 = __this->get_onFinished_12();
		Callback_t3139336517 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1448281318(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::AddOnFinished(EventDelegate)
extern "C"  void UITweener_AddOnFinished_m3187080535 (UITweener_t260334902 * __this, EventDelegate_t2738326060 * ___del0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_AddOnFinished_m3187080535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4210400802 * L_0 = __this->get_onFinished_12();
		EventDelegate_t2738326060 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t2738326060_il2cpp_TypeInfo_var);
		EventDelegate_Add_m2810218290(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::RemoveOnFinished(EventDelegate)
extern "C"  void UITweener_RemoveOnFinished_m2850141943 (UITweener_t260334902 * __this, EventDelegate_t2738326060 * ___del0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_RemoveOnFinished_m2850141943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4210400802 * L_0 = __this->get_onFinished_12();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		List_1_t4210400802 * L_1 = __this->get_onFinished_12();
		EventDelegate_t2738326060 * L_2 = ___del0;
		NullCheck(L_1);
		List_1_Remove_m1907737694(L_1, L_2, /*hidden argument*/List_1_Remove_m1907737694_RuntimeMethod_var);
	}

IL_0018:
	{
		List_1_t4210400802 * L_3 = __this->get_mTemp_21();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		List_1_t4210400802 * L_4 = __this->get_mTemp_21();
		EventDelegate_t2738326060 * L_5 = ___del0;
		NullCheck(L_4);
		List_1_Remove_m1907737694(L_4, L_5, /*hidden argument*/List_1_Remove_m1907737694_RuntimeMethod_var);
	}

IL_0030:
	{
		return;
	}
}
// System.Void UITweener::OnDisable()
extern "C"  void UITweener_OnDisable_m1451937429 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mStarted_16((bool)0);
		return;
	}
}
// System.Void UITweener::Sample(System.Single,System.Boolean)
extern "C"  void UITweener_Sample_m928485241 (UITweener_t260334902 * __this, float ___factor0, bool ___isFinished1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Sample_m928485241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	UITweener_t260334902 * G_B18_0 = NULL;
	UITweener_t260334902 * G_B17_0 = NULL;
	float G_B19_0 = 0.0f;
	UITweener_t260334902 * G_B19_1 = NULL;
	{
		float L_0 = ___factor0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_method_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = sinf(((float)il2cpp_codegen_multiply((float)(1.57079637f), (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_3)))));
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_4));
		bool L_5 = __this->get_steeperCurves_9();
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		float L_6 = V_0;
		float L_7 = V_0;
		V_0 = ((float)il2cpp_codegen_multiply((float)L_6, (float)L_7));
	}

IL_003b:
	{
		goto IL_0121;
	}

IL_0040:
	{
		int32_t L_8 = __this->get_method_3();
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_007b;
		}
	}
	{
		float L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_10 = sinf(((float)il2cpp_codegen_multiply((float)(1.57079637f), (float)L_9)));
		V_0 = L_10;
		bool L_11 = __this->get_steeperCurves_9();
		if (!L_11)
		{
			goto IL_0076;
		}
	}
	{
		float L_12 = V_0;
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_12));
		float L_13 = V_0;
		float L_14 = V_0;
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_14))));
	}

IL_0076:
	{
		goto IL_0121;
	}

IL_007b:
	{
		int32_t L_15 = __this->get_method_3();
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_00e8;
		}
	}
	{
		float L_16 = V_0;
		float L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_18 = sinf(((float)il2cpp_codegen_multiply((float)L_17, (float)(6.28318548f))));
		V_0 = ((float)il2cpp_codegen_subtract((float)L_16, (float)((float)((float)L_18/(float)(6.28318548f)))));
		bool L_19 = __this->get_steeperCurves_9();
		if (!L_19)
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_0;
		V_0 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_20, (float)(2.0f))), (float)(1.0f)));
		float L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		float L_23 = V_0;
		float L_24 = fabsf(L_23);
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_24));
		float L_25 = V_0;
		float L_26 = V_0;
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)L_25, (float)L_26))));
		float L_27 = V_1;
		float L_28 = V_0;
		V_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_27, (float)L_28)), (float)(0.5f))), (float)(0.5f)));
	}

IL_00e3:
	{
		goto IL_0121;
	}

IL_00e8:
	{
		int32_t L_29 = __this->get_method_3();
		if ((!(((uint32_t)L_29) == ((uint32_t)4))))
		{
			goto IL_0101;
		}
	}
	{
		float L_30 = V_0;
		float L_31 = UITweener_BounceLogic_m1320068922(__this, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		goto IL_0121;
	}

IL_0101:
	{
		int32_t L_32 = __this->get_method_3();
		if ((!(((uint32_t)L_32) == ((uint32_t)5))))
		{
			goto IL_0121;
		}
	}
	{
		float L_33 = V_0;
		float L_34 = UITweener_BounceLogic_m1320068922(__this, ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_33)), /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_34));
	}

IL_0121:
	{
		AnimationCurve_t3046754366 * L_35 = __this->get_animationCurve_5();
		G_B17_0 = __this;
		if (!L_35)
		{
			G_B18_0 = __this;
			goto IL_013e;
		}
	}
	{
		AnimationCurve_t3046754366 * L_36 = __this->get_animationCurve_5();
		float L_37 = V_0;
		NullCheck(L_36);
		float L_38 = AnimationCurve_Evaluate_m2125563588(L_36, L_37, /*hidden argument*/NULL);
		G_B19_0 = L_38;
		G_B19_1 = G_B17_0;
		goto IL_013f;
	}

IL_013e:
	{
		float L_39 = V_0;
		G_B19_0 = L_39;
		G_B19_1 = G_B18_0;
	}

IL_013f:
	{
		bool L_40 = ___isFinished1;
		NullCheck(G_B19_1);
		VirtActionInvoker2< float, bool >::Invoke(6 /* System.Void UITweener::OnUpdate(System.Single,System.Boolean) */, G_B19_1, G_B19_0, L_40);
		return;
	}
}
// System.Single UITweener::BounceLogic(System.Single)
extern "C"  float UITweener_BounceLogic_m1320068922 (UITweener_t260334902 * __this, float ___val0, const RuntimeMethod* method)
{
	{
		float L_0 = ___val0;
		if ((!(((float)L_0) < ((float)(0.363636f)))))
		{
			goto IL_001b;
		}
	}
	{
		float L_1 = ___val0;
		float L_2 = ___val0;
		___val0 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(7.5685f), (float)L_1)), (float)L_2));
		goto IL_0089;
	}

IL_001b:
	{
		float L_3 = ___val0;
		if ((!(((float)L_3) < ((float)(0.727272f)))))
		{
			goto IL_0045;
		}
	}
	{
		float L_4 = ___val0;
		float L_5 = ((float)il2cpp_codegen_subtract((float)L_4, (float)(0.545454f)));
		___val0 = L_5;
		float L_6 = ___val0;
		___val0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(7.5625f), (float)L_5)), (float)L_6)), (float)(0.75f)));
		goto IL_0089;
	}

IL_0045:
	{
		float L_7 = ___val0;
		if ((!(((float)L_7) < ((float)(0.90909f)))))
		{
			goto IL_006f;
		}
	}
	{
		float L_8 = ___val0;
		float L_9 = ((float)il2cpp_codegen_subtract((float)L_8, (float)(0.818181f)));
		___val0 = L_9;
		float L_10 = ___val0;
		___val0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(7.5625f), (float)L_9)), (float)L_10)), (float)(0.9375f)));
		goto IL_0089;
	}

IL_006f:
	{
		float L_11 = ___val0;
		float L_12 = ((float)il2cpp_codegen_subtract((float)L_11, (float)(0.9545454f)));
		___val0 = L_12;
		float L_13 = ___val0;
		___val0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(7.5625f), (float)L_12)), (float)L_13)), (float)(0.984375f)));
	}

IL_0089:
	{
		float L_14 = ___val0;
		return L_14;
	}
}
// System.Void UITweener::Play()
extern "C"  void UITweener_Play_m4120488661 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void UITweener::Play(System.Boolean) */, __this, (bool)1);
		return;
	}
}
// System.Void UITweener::PlayForward()
extern "C"  void UITweener_PlayForward_m2402233290 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void UITweener::Play(System.Boolean) */, __this, (bool)1);
		return;
	}
}
// System.Void UITweener::PlayReverse()
extern "C"  void UITweener_PlayReverse_m1598069729 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void UITweener::Play(System.Boolean) */, __this, (bool)0);
		return;
	}
}
// System.Void UITweener::Play(System.Boolean)
extern "C"  void UITweener_Play_m646427819 (UITweener_t260334902 * __this, bool ___forward0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Play_m646427819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = UITweener_get_amountPerDelta_m1343789098(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		__this->set_mAmountPerDelta_19(L_1);
		bool L_2 = ___forward0;
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		float L_3 = __this->get_mAmountPerDelta_19();
		__this->set_mAmountPerDelta_19(((-L_3)));
	}

IL_0024:
	{
		bool L_4 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003d;
		}
	}
	{
		Behaviour_set_enabled_m20417929(__this, (bool)1, /*hidden argument*/NULL);
		__this->set_mStarted_16((bool)0);
	}

IL_003d:
	{
		UITweener_DoUpdate_m1102478613(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::ResetToBeginning()
extern "C"  void UITweener_ResetToBeginning_m2516976937 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	UITweener_t260334902 * G_B2_0 = NULL;
	UITweener_t260334902 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	UITweener_t260334902 * G_B3_1 = NULL;
	{
		__this->set_mStarted_16((bool)0);
		float L_0 = UITweener_get_amountPerDelta_m1343789098(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0022;
		}
	}
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0022:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mFactor_20(G_B3_0);
		float L_1 = __this->get_mFactor_20();
		UITweener_Sample_m928485241(__this, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Toggle()
extern "C"  void UITweener_Toggle_m2162660133 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Toggle_m2162660133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_mFactor_20();
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = UITweener_get_amountPerDelta_m1343789098(__this, /*hidden argument*/NULL);
		__this->set_mAmountPerDelta_19(((-L_1)));
		goto IL_0033;
	}

IL_0022:
	{
		float L_2 = UITweener_get_amountPerDelta_m1343789098(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		__this->set_mAmountPerDelta_19(L_3);
	}

IL_0033:
	{
		Behaviour_set_enabled_m20417929(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::SetStartToCurrentValue()
extern "C"  void UITweener_SetStartToCurrentValue_m1560789613 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UITweener::SetEndToCurrentValue()
extern "C"  void UITweener_SetEndToCurrentValue_m2906889360 (UITweener_t260334902 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIViewport::.ctor()
extern "C"  void UIViewport__ctor_m3120554563 (UIViewport_t1918760134 * __this, const RuntimeMethod* method)
{
	{
		__this->set_fullSize_5((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIViewport::Start()
extern "C"  void UIViewport_Start_m2127192097 (UIViewport_t1918760134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIViewport_Start_m2127192097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		__this->set_mCam_6(L_0);
		Camera_t4157153871 * L_1 = __this->get_sourceCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Camera_t4157153871 * L_3 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sourceCamera_2(L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UIViewport::LateUpdate()
extern "C"  void UIViewport_LateUpdate_m3432131260 (UIViewport_t1918760134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIViewport_LateUpdate_m3432131260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		Transform_t3600365921 * L_0 = __this->get_topLeft_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t3600365921 * L_2 = __this->get_bottomRight_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t3600365921 * L_4 = __this->get_topLeft_3();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = GameObject_get_activeInHierarchy_m2006396688(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0113;
		}
	}
	{
		Camera_t4157153871 * L_7 = __this->get_sourceCamera_2();
		Transform_t3600365921 * L_8 = __this->get_topLeft_3();
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_10 = Camera_WorldToScreenPoint_m3726311023(L_7, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Camera_t4157153871 * L_11 = __this->get_sourceCamera_2();
		Transform_t3600365921 * L_12 = __this->get_bottomRight_4();
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t3722313464  L_14 = Camera_WorldToScreenPoint_m3726311023(L_11, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_0)->get_x_1();
		int32_t L_16 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = (&V_1)->get_y_2();
		int32_t L_18 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_x_1();
		float L_20 = (&V_0)->get_x_1();
		int32_t L_21 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = (&V_0)->get_y_2();
		float L_23 = (&V_1)->get_y_2();
		int32_t L_24 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((&V_2), ((float)((float)L_15/(float)(((float)((float)L_16))))), ((float)((float)L_17/(float)(((float)((float)L_18))))), ((float)((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_20))/(float)(((float)((float)L_21))))), ((float)((float)((float)il2cpp_codegen_subtract((float)L_22, (float)L_23))/(float)(((float)((float)L_24))))), /*hidden argument*/NULL);
		float L_25 = __this->get_fullSize_5();
		float L_26 = Rect_get_height_m1358425599((&V_2), /*hidden argument*/NULL);
		V_3 = ((float)il2cpp_codegen_multiply((float)L_25, (float)L_26));
		Rect_t2360479859  L_27 = V_2;
		Camera_t4157153871 * L_28 = __this->get_mCam_6();
		NullCheck(L_28);
		Rect_t2360479859  L_29 = Camera_get_rect_m2458154151(L_28, /*hidden argument*/NULL);
		bool L_30 = Rect_op_Inequality_m51778115(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00e5;
		}
	}
	{
		Camera_t4157153871 * L_31 = __this->get_mCam_6();
		Rect_t2360479859  L_32 = V_2;
		NullCheck(L_31);
		Camera_set_rect_m521006799(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		Camera_t4157153871 * L_33 = __this->get_mCam_6();
		NullCheck(L_33);
		float L_34 = Camera_get_orthographicSize_m3903216845(L_33, /*hidden argument*/NULL);
		float L_35 = V_3;
		if ((((float)L_34) == ((float)L_35)))
		{
			goto IL_0102;
		}
	}
	{
		Camera_t4157153871 * L_36 = __this->get_mCam_6();
		float L_37 = V_3;
		NullCheck(L_36);
		Camera_set_orthographicSize_m76971700(L_36, L_37, /*hidden argument*/NULL);
	}

IL_0102:
	{
		Camera_t4157153871 * L_38 = __this->get_mCam_6();
		NullCheck(L_38);
		Behaviour_set_enabled_m20417929(L_38, (bool)1, /*hidden argument*/NULL);
		goto IL_011f;
	}

IL_0113:
	{
		Camera_t4157153871 * L_39 = __this->get_mCam_6();
		NullCheck(L_39);
		Behaviour_set_enabled_m20417929(L_39, (bool)0, /*hidden argument*/NULL);
	}

IL_011f:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIWidget::.ctor()
extern "C"  void UIWidget__ctor_m2452914675 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget__ctor_m2452914675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mColor_22(L_0);
		__this->set_mPivot_23(4);
		__this->set_mWidth_24(((int32_t)100));
		__this->set_mHeight_25(((int32_t)100));
		__this->set_aspectRatio_34((1.0f));
		UIGeometry_t1059483952 * L_1 = (UIGeometry_t1059483952 *)il2cpp_codegen_object_new(UIGeometry_t1059483952_il2cpp_TypeInfo_var);
		UIGeometry__ctor_m3724890878(L_1, /*hidden argument*/NULL);
		__this->set_geometry_37(L_1);
		__this->set_fillGeometry_38((bool)1);
		__this->set_mPlayMode_39((bool)1);
		Vector4_t3319028937  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m2498754347((&L_2), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_mDrawRegion_40(L_2);
		__this->set_mIsVisibleByAlpha_42((bool)1);
		__this->set_mIsVisibleByPanel_43((bool)1);
		__this->set_mIsInFront_44((bool)1);
		__this->set_mCorners_48(((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_mAlphaFrameID_49((-1));
		__this->set_mMatrixFrame_50((-1));
		IL2CPP_RUNTIME_CLASS_INIT(UIRect_t2875960382_il2cpp_TypeInfo_var);
		UIRect__ctor_m1261317954(__this, /*hidden argument*/NULL);
		return;
	}
}
// UIDrawCall/OnRenderCallback UIWidget::get_onRender()
extern "C"  OnRenderCallback_t133425655 * UIWidget_get_onRender_m3838727314 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		OnRenderCallback_t133425655 * L_0 = __this->get_mOnRender_30();
		return L_0;
	}
}
// System.Void UIWidget::set_onRender(UIDrawCall/OnRenderCallback)
extern "C"  void UIWidget_set_onRender_m176877109 (UIWidget_t3538521925 * __this, OnRenderCallback_t133425655 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_onRender_m176877109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnRenderCallback_t133425655 * L_0 = __this->get_mOnRender_30();
		OnRenderCallback_t133425655 * L_1 = ___value0;
		bool L_2 = Delegate_op_Inequality_m1112333615(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t1293405319 * L_3 = __this->get_drawCall_47();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t1293405319 * L_5 = __this->get_drawCall_47();
		NullCheck(L_5);
		OnRenderCallback_t133425655 * L_6 = L_5->get_onRender_35();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		OnRenderCallback_t133425655 * L_7 = __this->get_mOnRender_30();
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t1293405319 * L_8 = __this->get_drawCall_47();
		UIDrawCall_t1293405319 * L_9 = L_8;
		NullCheck(L_9);
		OnRenderCallback_t133425655 * L_10 = L_9->get_onRender_35();
		OnRenderCallback_t133425655 * L_11 = __this->get_mOnRender_30();
		Delegate_t1188392813 * L_12 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_onRender_35(((OnRenderCallback_t133425655 *)CastclassSealed((RuntimeObject*)L_12, OnRenderCallback_t133425655_il2cpp_TypeInfo_var)));
	}

IL_005e:
	{
		OnRenderCallback_t133425655 * L_13 = ___value0;
		__this->set_mOnRender_30(L_13);
		UIDrawCall_t1293405319 * L_14 = __this->get_drawCall_47();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_14, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t1293405319 * L_16 = __this->get_drawCall_47();
		UIDrawCall_t1293405319 * L_17 = L_16;
		NullCheck(L_17);
		OnRenderCallback_t133425655 * L_18 = L_17->get_onRender_35();
		OnRenderCallback_t133425655 * L_19 = ___value0;
		Delegate_t1188392813 * L_20 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_onRender_35(((OnRenderCallback_t133425655 *)CastclassSealed((RuntimeObject*)L_20, OnRenderCallback_t133425655_il2cpp_TypeInfo_var)));
	}

IL_0092:
	{
		return;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawRegion()
extern "C"  Vector4_t3319028937  UIWidget_get_drawRegion_m2411973907 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		Vector4_t3319028937  L_0 = __this->get_mDrawRegion_40();
		return L_0;
	}
}
// System.Void UIWidget::set_drawRegion(UnityEngine.Vector4)
extern "C"  void UIWidget_set_drawRegion_m1955755355 (UIWidget_t3538521925 * __this, Vector4_t3319028937  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_drawRegion_m1955755355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t3319028937  L_0 = __this->get_mDrawRegion_40();
		Vector4_t3319028937  L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		bool L_2 = Vector4_op_Inequality_m3625339929(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector4_t3319028937  L_3 = ___value0;
		__this->set_mDrawRegion_40(L_3);
		bool L_4 = __this->get_autoResizeBoxCollider_31();
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		UIWidget_ResizeCollider_m1303972682(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002f:
	{
		return;
	}
}
// UnityEngine.Vector2 UIWidget::get_pivotOffset()
extern "C"  Vector2_t2156229523  UIWidget_get_pivotOffset_m1997789874 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = UIWidget_get_pivot_m3232390905(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_1 = NGUIMath_GetPivotOffset_m3409460361(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UIWidget::get_width()
extern "C"  int32_t UIWidget_get_width_m2008696000 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mWidth_24();
		return L_0;
	}
}
// System.Void UIWidget::set_width(System.Int32)
extern "C"  void UIWidget_set_width_m181008468 (UIWidget_t3538521925 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_width_m181008468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mWidth_24();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_33();
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UIRect::get_isAnchoredHorizontally() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t1754718329 * L_8 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t1754718329 * L_11 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_11);
		Transform_t3600365921 * L_12 = L_11->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_12, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)3)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if (L_16)
		{
			goto IL_00a6;
		}
	}

IL_0083:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)L_18))))), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a6:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if ((((int32_t)L_19) == ((int32_t)8)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)5)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mWidth_24();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)L_23))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mWidth_24();
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, ((float)il2cpp_codegen_multiply((float)(((float)((float)((-L_29))))), (float)(0.5f))), (0.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_30))), (float)(0.5f))), (0.0f), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t1754718329 * L_31 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_31);
		Transform_t3600365921 * L_32 = L_31->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_32, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_34, (int32_t)L_35))))), (0.0f), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mWidth_24();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_36, (int32_t)L_37))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = ___value0;
		int32_t L_39 = __this->get_mHeight_25();
		UIWidget_SetDimensions_m2627586629(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// System.Int32 UIWidget::get_height()
extern "C"  int32_t UIWidget_get_height_m3299050233 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mHeight_25();
		return L_0;
	}
}
// System.Void UIWidget::set_height(System.Int32)
extern "C"  void UIWidget_set_height_m878974275 (UIWidget_t3538521925 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_height_m878974275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mHeight_25();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_33();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UIRect::get_isAnchoredVertically() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t1754718329 * L_8 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t1754718329 * L_11 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_11);
		Transform_t3600365921 * L_12 = L_11->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_12, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)7)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_16) == ((uint32_t)8))))
		{
			goto IL_00a7;
		}
	}

IL_0084:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)L_18))))), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a7:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if (!L_19)
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)1)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mHeight_25();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)L_23))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mHeight_25();
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (0.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)((-L_29))))), (float)(0.5f))), (0.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_30))), (float)(0.5f))), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t1754718329 * L_31 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_31);
		Transform_t3600365921 * L_32 = L_31->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_32, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_34, (int32_t)L_35))))), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mHeight_25();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m1901830214(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_36, (int32_t)L_37))))), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = __this->get_mWidth_24();
		int32_t L_39 = ___value0;
		UIWidget_SetDimensions_m2627586629(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// UnityEngine.Color UIWidget::get_color()
extern "C"  Color_t2555686324  UIWidget_get_color_m3938892930 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0 = __this->get_mColor_22();
		return L_0;
	}
}
// System.Void UIWidget::set_color(UnityEngine.Color)
extern "C"  void UIWidget_set_color_m2288988844 (UIWidget_t3538521925 * __this, Color_t2555686324  ___value0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Color_t2555686324  L_0 = __this->get_mColor_22();
		Color_t2555686324  L_1 = ___value0;
		bool L_2 = Color_op_Inequality_m3353772181(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Color_t2555686324 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = L_3->get_a_3();
		float L_5 = (&___value0)->get_a_3();
		V_0 = (bool)((((int32_t)((((float)L_4) == ((float)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Color_t2555686324  L_6 = ___value0;
		__this->set_mColor_22(L_6);
		bool L_7 = V_0;
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIRect::Invalidate(System.Boolean) */, __this, L_7);
	}

IL_0037:
	{
		return;
	}
}
// System.Void UIWidget::SetColorNoAlpha(UnityEngine.Color)
extern "C"  void UIWidget_SetColorNoAlpha_m359729975 (UIWidget_t3538521925 * __this, Color_t2555686324  ___c0, const RuntimeMethod* method)
{
	{
		Color_t2555686324 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_r_0();
		float L_2 = (&___c0)->get_r_0();
		if ((!(((float)L_1) == ((float)L_2))))
		{
			goto IL_0045;
		}
	}
	{
		Color_t2555686324 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = L_3->get_g_1();
		float L_5 = (&___c0)->get_g_1();
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_0045;
		}
	}
	{
		Color_t2555686324 * L_6 = __this->get_address_of_mColor_22();
		float L_7 = L_6->get_b_2();
		float L_8 = (&___c0)->get_b_2();
		if ((((float)L_7) == ((float)L_8)))
		{
			goto IL_0082;
		}
	}

IL_0045:
	{
		Color_t2555686324 * L_9 = __this->get_address_of_mColor_22();
		float L_10 = (&___c0)->get_r_0();
		L_9->set_r_0(L_10);
		Color_t2555686324 * L_11 = __this->get_address_of_mColor_22();
		float L_12 = (&___c0)->get_g_1();
		L_11->set_g_1(L_12);
		Color_t2555686324 * L_13 = __this->get_address_of_mColor_22();
		float L_14 = (&___c0)->get_b_2();
		L_13->set_b_2(L_14);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIRect::Invalidate(System.Boolean) */, __this, (bool)0);
	}

IL_0082:
	{
		return;
	}
}
// System.Single UIWidget::get_alpha()
extern "C"  float UIWidget_get_alpha_m3064373203 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		return L_1;
	}
}
// System.Void UIWidget::set_alpha(System.Single)
extern "C"  void UIWidget_set_alpha_m2278043477 (UIWidget_t3538521925 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Color_t2555686324 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		float L_2 = ___value0;
		if ((((float)L_1) == ((float)L_2)))
		{
			goto IL_0024;
		}
	}
	{
		Color_t2555686324 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = ___value0;
		L_3->set_a_3(L_4);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIRect::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean UIWidget::get_isVisible()
extern "C"  bool UIWidget_get_isVisible_m524426372 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_isVisible_m524426372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		bool L_0 = __this->get_mIsVisibleByPanel_43();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		bool L_1 = __this->get_mIsVisibleByAlpha_42();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		bool L_2 = __this->get_mIsInFront_44();
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = ((UIRect_t2875960382 *)__this)->get_finalAlpha_20();
		if ((!(((float)L_3) > ((float)(0.001f)))))
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_4 = NGUITools_GetActive_m3370498562(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_4));
		goto IL_003a;
	}

IL_0039:
	{
		G_B6_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean UIWidget::get_hasVertices()
extern "C"  bool UIWidget_get_hasVertices_m2576649473 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		UIGeometry_t1059483952 * L_0 = __this->get_geometry_37();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UIGeometry_t1059483952 * L_1 = __this->get_geometry_37();
		NullCheck(L_1);
		bool L_2 = UIGeometry_get_hasVertices_m3715418080(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// UIWidget/Pivot UIWidget::get_rawPivot()
extern "C"  int32_t UIWidget_get_rawPivot_m4136022915 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_rawPivot(UIWidget/Pivot)
extern "C"  void UIWidget_set_rawPivot_m2282739067 (UIWidget_t3538521925 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set_mPivot_23(L_2);
		bool L_3 = __this->get_autoResizeBoxCollider_31();
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_ResizeCollider_m1303972682(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002a:
	{
		return;
	}
}
// UIWidget/Pivot UIWidget::get_pivot()
extern "C"  int32_t UIWidget_get_pivot_m3232390905 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_pivot(UIWidget/Pivot)
extern "C"  void UIWidget_set_pivot_m951060879 (UIWidget_t3538521925 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_pivot_m951060879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Transform_t3600365921 * V_2 = NULL;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_00ea;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, __this);
		NullCheck(L_2);
		V_0 = (*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		int32_t L_3 = ___value0;
		__this->set_mPivot_23(L_3);
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)1);
		Vector3U5BU5D_t1718750761* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, __this);
		NullCheck(L_4);
		V_1 = (*(Vector3_t3722313464 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Transform_t3600365921 * L_5 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		Transform_t3600365921 * L_6 = V_2;
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		Transform_t3600365921 * L_8 = V_2;
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_localPosition_m4234289348(L_8, /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = (&V_5)->get_z_3();
		V_4 = L_10;
		Vector3_t3722313464 * L_11 = (&V_3);
		float L_12 = L_11->get_x_1();
		float L_13 = (&V_0)->get_x_1();
		float L_14 = (&V_1)->get_x_1();
		L_11->set_x_1(((float)il2cpp_codegen_add((float)L_12, (float)((float)il2cpp_codegen_subtract((float)L_13, (float)L_14)))));
		Vector3_t3722313464 * L_15 = (&V_3);
		float L_16 = L_15->get_y_2();
		float L_17 = (&V_0)->get_y_2();
		float L_18 = (&V_1)->get_y_2();
		L_15->set_y_2(((float)il2cpp_codegen_add((float)L_16, (float)((float)il2cpp_codegen_subtract((float)L_17, (float)L_18)))));
		Transform_t3600365921 * L_19 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = V_3;
		NullCheck(L_19);
		Transform_set_position_m3387557959(L_19, L_20, /*hidden argument*/NULL);
		Transform_t3600365921 * L_21 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t3722313464  L_22 = Transform_get_localPosition_m4234289348(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_24 = bankers_roundf(L_23);
		(&V_3)->set_x_1(L_24);
		float L_25 = (&V_3)->get_y_2();
		float L_26 = bankers_roundf(L_25);
		(&V_3)->set_y_2(L_26);
		float L_27 = V_4;
		(&V_3)->set_z_3(L_27);
		Transform_t3600365921 * L_28 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_29 = V_3;
		NullCheck(L_28);
		Transform_set_localPosition_m4128471975(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		return;
	}
}
// System.Int32 UIWidget::get_depth()
extern "C"  int32_t UIWidget_get_depth_m431179076 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mDepth_26();
		return L_0;
	}
}
// System.Void UIWidget::set_depth(System.Int32)
extern "C"  void UIWidget_set_depth_m2124062109 (UIWidget_t3538521925 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_depth_m2124062109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_mDepth_26();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t1716472341 * L_2 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		UIPanel_t1716472341 * L_4 = __this->get_panel_36();
		NullCheck(L_4);
		UIPanel_RemoveWidget_m2262424736(L_4, __this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = ___value0;
		__this->set_mDepth_26(L_5);
		UIPanel_t1716472341 * L_6 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t1716472341 * L_8 = __this->get_panel_36();
		NullCheck(L_8);
		UIPanel_AddWidget_m4245952570(L_8, __this, /*hidden argument*/NULL);
		bool L_9 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t1716472341 * L_10 = __this->get_panel_36();
		NullCheck(L_10);
		UIPanel_SortWidgets_m346874278(L_10, /*hidden argument*/NULL);
		UIPanel_t1716472341 * L_11 = __this->get_panel_36();
		NullCheck(L_11);
		UIPanel_RebuildAllDrawCalls_m3198038699(L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Int32 UIWidget::get_raycastDepth()
extern "C"  int32_t UIWidget_get_raycastDepth_m12826239 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_raycastDepth_m12826239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		UIPanel_t1716472341 * L_0 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m976889642(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		UIPanel_t1716472341 * L_2 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_4 = __this->get_mDepth_26();
		UIPanel_t1716472341 * L_5 = __this->get_panel_36();
		NullCheck(L_5);
		int32_t L_6 = UIPanel_get_depth_m1723958116(L_5, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)((int32_t)1000)))));
		goto IL_004c;
	}

IL_0046:
	{
		int32_t L_7 = __this->get_mDepth_26();
		G_B5_0 = L_7;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3[] UIWidget::get_localCorners()
extern "C"  Vector3U5BU5D_t1718750761* UIWidget_get_localCorners_m3881285883 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector2_t2156229523  L_0 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)il2cpp_codegen_multiply((float)((-L_1)), (float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)il2cpp_codegen_multiply((float)((-L_3)), (float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)il2cpp_codegen_add((float)L_5, (float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)il2cpp_codegen_add((float)L_7, (float)(((float)((float)L_8)))));
		Vector3U5BU5D_t1718750761* L_9 = __this->get_mCorners_48();
		NullCheck(L_9);
		float L_10 = V_1;
		float L_11 = V_2;
		Vector3_t3722313464  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m1719387948((&L_12), L_10, L_11, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_12;
		Vector3U5BU5D_t1718750761* L_13 = __this->get_mCorners_48();
		NullCheck(L_13);
		float L_14 = V_1;
		float L_15 = V_4;
		Vector3_t3722313464  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m1719387948((&L_16), L_14, L_15, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_16;
		Vector3U5BU5D_t1718750761* L_17 = __this->get_mCorners_48();
		NullCheck(L_17);
		float L_18 = V_3;
		float L_19 = V_4;
		Vector3_t3722313464  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m1719387948((&L_20), L_18, L_19, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_20;
		Vector3U5BU5D_t1718750761* L_21 = __this->get_mCorners_48();
		NullCheck(L_21);
		float L_22 = V_3;
		float L_23 = V_2;
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1719387948((&L_24), L_22, L_23, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_24;
		Vector3U5BU5D_t1718750761* L_25 = __this->get_mCorners_48();
		return L_25;
	}
}
// UnityEngine.Vector2 UIWidget::get_localSize()
extern "C"  Vector2_t2156229523  UIWidget_get_localSize_m3820549068 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_localSize_m3820549068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	{
		Vector3U5BU5D_t1718750761* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(10 /* UnityEngine.Vector3[] UIRect::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t1718750761* L_1 = V_0;
		NullCheck(L_1);
		Vector3U5BU5D_t1718750761* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, (*(Vector3_t3722313464 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 UIWidget::get_localCenter()
extern "C"  Vector3_t3722313464  UIWidget_get_localCenter_m3035087122 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_localCenter_m3035087122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	{
		Vector3U5BU5D_t1718750761* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(10 /* UnityEngine.Vector3[] UIRect::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t1718750761* L_1 = V_0;
		NullCheck(L_1);
		Vector3U5BU5D_t1718750761* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_Lerp_m407887542(NULL /*static, unused*/, (*(Vector3_t3722313464 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3[] UIWidget::get_worldCorners()
extern "C"  Vector3U5BU5D_t1718750761* UIWidget_get_worldCorners_m4277823239 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Transform_t3600365921 * V_5 = NULL;
	{
		Vector2_t2156229523  L_0 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)il2cpp_codegen_multiply((float)((-L_1)), (float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)il2cpp_codegen_multiply((float)((-L_3)), (float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)il2cpp_codegen_add((float)L_5, (float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)il2cpp_codegen_add((float)L_7, (float)(((float)((float)L_8)))));
		Transform_t3600365921 * L_9 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		V_5 = L_9;
		Vector3U5BU5D_t1718750761* L_10 = __this->get_mCorners_48();
		NullCheck(L_10);
		Transform_t3600365921 * L_11 = V_5;
		float L_12 = V_1;
		float L_13 = V_2;
		NullCheck(L_11);
		Vector3_t3722313464  L_14 = Transform_TransformPoint_m4024714202(L_11, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_14;
		Vector3U5BU5D_t1718750761* L_15 = __this->get_mCorners_48();
		NullCheck(L_15);
		Transform_t3600365921 * L_16 = V_5;
		float L_17 = V_1;
		float L_18 = V_4;
		NullCheck(L_16);
		Vector3_t3722313464  L_19 = Transform_TransformPoint_m4024714202(L_16, L_17, L_18, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_19;
		Vector3U5BU5D_t1718750761* L_20 = __this->get_mCorners_48();
		NullCheck(L_20);
		Transform_t3600365921 * L_21 = V_5;
		float L_22 = V_3;
		float L_23 = V_4;
		NullCheck(L_21);
		Vector3_t3722313464  L_24 = Transform_TransformPoint_m4024714202(L_21, L_22, L_23, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_24;
		Vector3U5BU5D_t1718750761* L_25 = __this->get_mCorners_48();
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = V_5;
		float L_27 = V_3;
		float L_28 = V_2;
		NullCheck(L_26);
		Vector3_t3722313464  L_29 = Transform_TransformPoint_m4024714202(L_26, L_27, L_28, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_29;
		Vector3U5BU5D_t1718750761* L_30 = __this->get_mCorners_48();
		return L_30;
	}
}
// UnityEngine.Vector3 UIWidget::get_worldCenter()
extern "C"  Vector3_t3722313464  UIWidget_get_worldCenter_m482208363 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = UIWidget_get_localCenter_m3035087122(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_2 = Transform_TransformPoint_m226827784(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawingDimensions()
extern "C"  Vector4_t3319028937  UIWidget_get_drawingDimensions_m2099475420 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_drawingDimensions_m2099475420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B9_2 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B11_2 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B10_1 = 0.0f;
	float G_B10_2 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B12_2 = 0.0f;
	float G_B12_3 = 0.0f;
	{
		Vector2_t2156229523  L_0 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)il2cpp_codegen_multiply((float)((-L_1)), (float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)il2cpp_codegen_multiply((float)((-L_3)), (float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)il2cpp_codegen_add((float)L_5, (float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)il2cpp_codegen_add((float)L_7, (float)(((float)((float)L_8)))));
		Vector4_t3319028937 * L_9 = __this->get_address_of_mDrawRegion_40();
		float L_10 = L_9->get_x_1();
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_11 = V_1;
		G_B3_0 = L_11;
		goto IL_006b;
	}

IL_0059:
	{
		float L_12 = V_1;
		float L_13 = V_3;
		Vector4_t3319028937 * L_14 = __this->get_address_of_mDrawRegion_40();
		float L_15 = L_14->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		G_B3_0 = L_16;
	}

IL_006b:
	{
		Vector4_t3319028937 * L_17 = __this->get_address_of_mDrawRegion_40();
		float L_18 = L_17->get_y_2();
		G_B4_0 = G_B3_0;
		if ((!(((float)L_18) == ((float)(0.0f)))))
		{
			G_B5_0 = G_B3_0;
			goto IL_0086;
		}
	}
	{
		float L_19 = V_2;
		G_B6_0 = L_19;
		G_B6_1 = G_B4_0;
		goto IL_0099;
	}

IL_0086:
	{
		float L_20 = V_2;
		float L_21 = V_4;
		Vector4_t3319028937 * L_22 = __this->get_address_of_mDrawRegion_40();
		float L_23 = L_22->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_20, L_21, L_23, /*hidden argument*/NULL);
		G_B6_0 = L_24;
		G_B6_1 = G_B5_0;
	}

IL_0099:
	{
		Vector4_t3319028937 * L_25 = __this->get_address_of_mDrawRegion_40();
		float L_26 = L_25->get_z_3();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		if ((!(((float)L_26) == ((float)(1.0f)))))
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			goto IL_00b4;
		}
	}
	{
		float L_27 = V_3;
		G_B9_0 = L_27;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00c6;
	}

IL_00b4:
	{
		float L_28 = V_1;
		float L_29 = V_3;
		Vector4_t3319028937 * L_30 = __this->get_address_of_mDrawRegion_40();
		float L_31 = L_30->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_32 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_28, L_29, L_31, /*hidden argument*/NULL);
		G_B9_0 = L_32;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00c6:
	{
		Vector4_t3319028937 * L_33 = __this->get_address_of_mDrawRegion_40();
		float L_34 = L_33->get_w_4();
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		if ((!(((float)L_34) == ((float)(1.0f)))))
		{
			G_B11_0 = G_B9_0;
			G_B11_1 = G_B9_1;
			G_B11_2 = G_B9_2;
			goto IL_00e2;
		}
	}
	{
		float L_35 = V_4;
		G_B12_0 = L_35;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_00f5;
	}

IL_00e2:
	{
		float L_36 = V_2;
		float L_37 = V_4;
		Vector4_t3319028937 * L_38 = __this->get_address_of_mDrawRegion_40();
		float L_39 = L_38->get_w_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_40 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_36, L_37, L_39, /*hidden argument*/NULL);
		G_B12_0 = L_40;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_00f5:
	{
		Vector4_t3319028937  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector4__ctor_m2498754347((&L_41), G_B12_3, G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		return L_41;
	}
}
// UnityEngine.Material UIWidget::get_material()
extern "C"  Material_t340375123 * UIWidget_get_material_m3288121629 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		Material_t340375123 * L_0 = __this->get_mMat_27();
		return L_0;
	}
}
// System.Void UIWidget::set_material(UnityEngine.Material)
extern "C"  void UIWidget_set_material_m3876747158 (UIWidget_t3538521925 * __this, Material_t340375123 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_material_m3876747158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t340375123 * L_0 = __this->get_mMat_27();
		Material_t340375123 * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_RemoveFromPanel_m725257632(__this, /*hidden argument*/NULL);
		Material_t340375123 * L_3 = ___value0;
		__this->set_mMat_27(L_3);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_0024:
	{
		return;
	}
}
// UnityEngine.Texture UIWidget::get_mainTexture()
extern "C"  Texture_t3661962703 * UIWidget_get_mainTexture_m3193712698 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_mainTexture_m3193712698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	Texture_t3661962703 * G_B3_0 = NULL;
	{
		Material_t340375123 * L_0 = VirtFuncInvoker0< Material_t340375123 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t340375123 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t340375123 * L_3 = V_0;
		NullCheck(L_3);
		Texture_t3661962703 * L_4 = Material_get_mainTexture_m692510677(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Texture_t3661962703 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_mainTexture(UnityEngine.Texture)
extern "C"  void UIWidget_set_mainTexture_m1065044862 (UIWidget_t3538521925 * __this, Texture_t3661962703 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_mainTexture_m1065044862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m904156431(NULL /*static, unused*/, L_0, _stringLiteral4204125540, /*hidden argument*/NULL);
		NotImplementedException_t3489357830 * L_2 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3095902440(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2,UIWidget_set_mainTexture_m1065044862_RuntimeMethod_var);
	}
}
// UnityEngine.Shader UIWidget::get_shader()
extern "C"  Shader_t4151988712 * UIWidget_get_shader_m4140967340 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_shader_m4140967340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	Shader_t4151988712 * G_B3_0 = NULL;
	{
		Material_t340375123 * L_0 = VirtFuncInvoker0< Material_t340375123 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t340375123 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t340375123 * L_3 = V_0;
		NullCheck(L_3);
		Shader_t4151988712 * L_4 = Material_get_shader_m1331119247(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Shader_t4151988712 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_shader(UnityEngine.Shader)
extern "C"  void UIWidget_set_shader_m1925003059 (UIWidget_t3538521925 * __this, Shader_t4151988712 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_shader_m1925003059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m904156431(NULL /*static, unused*/, L_0, _stringLiteral723286479, /*hidden argument*/NULL);
		NotImplementedException_t3489357830 * L_2 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3095902440(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2,UIWidget_set_shader_m1925003059_RuntimeMethod_var);
	}
}
// UnityEngine.Vector2 UIWidget::get_relativeSize()
extern "C"  Vector2_t2156229523  UIWidget_get_relativeSize_m887221726 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_relativeSize_m887221726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_one_m738793577(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UIWidget::get_hasBoxCollider()
extern "C"  bool UIWidget_get_hasBoxCollider_m3448657381 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_hasBoxCollider_m3448657381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BoxCollider_t1640800422 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = Component_GetComponent_TisCollider_t1773347010_m4226749020(__this, /*hidden argument*/Component_GetComponent_TisCollider_t1773347010_m4226749020_RuntimeMethod_var);
		V_0 = ((BoxCollider_t1640800422 *)IsInstSealed((RuntimeObject*)L_0, BoxCollider_t1640800422_il2cpp_TypeInfo_var));
		BoxCollider_t1640800422 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		BoxCollider2D_t3581341831 * L_3 = Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UIWidget::SetDimensions(System.Int32,System.Int32)
extern "C"  void UIWidget_SetDimensions_m2627586629 (UIWidget_t3538521925 * __this, int32_t ___w0, int32_t ___h1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetDimensions_m2627586629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_mWidth_24();
		int32_t L_1 = ___w0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_mHeight_25();
		int32_t L_3 = ___h1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_00b8;
		}
	}

IL_0018:
	{
		int32_t L_4 = ___w0;
		__this->set_mWidth_24(L_4);
		int32_t L_5 = ___h1;
		__this->set_mHeight_25(L_5);
		int32_t L_6 = __this->get_keepAspectRatio_33();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_7 = __this->get_mWidth_24();
		float L_8 = __this->get_aspectRatio_34();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)(((float)((float)L_7)))/(float)L_8)), /*hidden argument*/NULL);
		__this->set_mHeight_25(L_9);
		goto IL_009a;
	}

IL_0050:
	{
		int32_t L_10 = __this->get_keepAspectRatio_33();
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_11 = __this->get_mHeight_25();
		float L_12 = __this->get_aspectRatio_34();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_13 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_11))), (float)L_12)), /*hidden argument*/NULL);
		__this->set_mWidth_24(L_13);
		goto IL_009a;
	}

IL_007a:
	{
		int32_t L_14 = __this->get_keepAspectRatio_33();
		if (L_14)
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_15 = __this->get_mWidth_24();
		int32_t L_16 = __this->get_mHeight_25();
		__this->set_aspectRatio_34(((float)((float)(((float)((float)L_15)))/(float)(((float)((float)L_16))))));
	}

IL_009a:
	{
		__this->set_mMoved_46((bool)1);
		bool L_17 = __this->get_autoResizeBoxCollider_31();
		if (!L_17)
		{
			goto IL_00b2;
		}
	}
	{
		UIWidget_ResizeCollider_m1303972682(__this, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_00b8:
	{
		return;
	}
}
// UnityEngine.Vector3[] UIWidget::GetSides(UnityEngine.Transform)
extern "C"  Vector3U5BU5D_t1718750761* UIWidget_GetSides_m2621800934 (UIWidget_t3538521925 * __this, Transform_t3600365921 * ___relativeTo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_GetSides_m2621800934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Transform_t3600365921 * V_7 = NULL;
	int32_t V_8 = 0;
	{
		Vector2_t2156229523  L_0 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)il2cpp_codegen_multiply((float)((-L_1)), (float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)il2cpp_codegen_multiply((float)((-L_3)), (float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)il2cpp_codegen_add((float)L_5, (float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)il2cpp_codegen_add((float)L_7, (float)(((float)((float)L_8)))));
		float L_9 = V_1;
		float L_10 = V_3;
		V_5 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_add((float)L_9, (float)L_10)), (float)(0.5f)));
		float L_11 = V_2;
		float L_12 = V_4;
		V_6 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_add((float)L_11, (float)L_12)), (float)(0.5f)));
		Transform_t3600365921 * L_13 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		V_7 = L_13;
		Vector3U5BU5D_t1718750761* L_14 = __this->get_mCorners_48();
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = V_7;
		float L_16 = V_1;
		float L_17 = V_6;
		NullCheck(L_15);
		Vector3_t3722313464  L_18 = Transform_TransformPoint_m4024714202(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_18;
		Vector3U5BU5D_t1718750761* L_19 = __this->get_mCorners_48();
		NullCheck(L_19);
		Transform_t3600365921 * L_20 = V_7;
		float L_21 = V_5;
		float L_22 = V_4;
		NullCheck(L_20);
		Vector3_t3722313464  L_23 = Transform_TransformPoint_m4024714202(L_20, L_21, L_22, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_23;
		Vector3U5BU5D_t1718750761* L_24 = __this->get_mCorners_48();
		NullCheck(L_24);
		Transform_t3600365921 * L_25 = V_7;
		float L_26 = V_3;
		float L_27 = V_6;
		NullCheck(L_25);
		Vector3_t3722313464  L_28 = Transform_TransformPoint_m4024714202(L_25, L_26, L_27, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_28;
		Vector3U5BU5D_t1718750761* L_29 = __this->get_mCorners_48();
		NullCheck(L_29);
		Transform_t3600365921 * L_30 = V_7;
		float L_31 = V_5;
		float L_32 = V_2;
		NullCheck(L_30);
		Vector3_t3722313464  L_33 = Transform_TransformPoint_m4024714202(L_30, L_31, L_32, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_33;
		Transform_t3600365921 * L_34 = ___relativeTo0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_35 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_34, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012a;
		}
	}
	{
		V_8 = 0;
		goto IL_0122;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1718750761* L_36 = __this->get_mCorners_48();
		int32_t L_37 = V_8;
		NullCheck(L_36);
		Transform_t3600365921 * L_38 = ___relativeTo0;
		Vector3U5BU5D_t1718750761* L_39 = __this->get_mCorners_48();
		int32_t L_40 = V_8;
		NullCheck(L_39);
		NullCheck(L_38);
		Vector3_t3722313464  L_41 = Transform_InverseTransformPoint_m1343916000(L_38, (*(Vector3_t3722313464 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37))) = L_41;
		int32_t L_42 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_0122:
	{
		int32_t L_43 = V_8;
		if ((((int32_t)L_43) < ((int32_t)4)))
		{
			goto IL_00f2;
		}
	}

IL_012a:
	{
		Vector3U5BU5D_t1718750761* L_44 = __this->get_mCorners_48();
		return L_44;
	}
}
// System.Single UIWidget::CalculateFinalAlpha(System.Int32)
extern "C"  float UIWidget_CalculateFinalAlpha_m654752104 (UIWidget_t3538521925 * __this, int32_t ___frameID0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mAlphaFrameID_49();
		int32_t L_1 = ___frameID0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = ___frameID0;
		__this->set_mAlphaFrameID_49(L_2);
		int32_t L_3 = ___frameID0;
		UIWidget_UpdateFinalAlpha_m4075768229(__this, L_3, /*hidden argument*/NULL);
	}

IL_001a:
	{
		float L_4 = ((UIRect_t2875960382 *)__this)->get_finalAlpha_20();
		return L_4;
	}
}
// System.Void UIWidget::UpdateFinalAlpha(System.Int32)
extern "C"  void UIWidget_UpdateFinalAlpha_m4075768229 (UIWidget_t3538521925 * __this, int32_t ___frameID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpdateFinalAlpha_m4075768229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIRect_t2875960382 * V_0 = NULL;
	UIWidget_t3538521925 * G_B5_0 = NULL;
	UIWidget_t3538521925 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	UIWidget_t3538521925 * G_B6_1 = NULL;
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_42();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_mIsInFront_44();
		if (L_1)
		{
			goto IL_0026;
		}
	}

IL_0016:
	{
		((UIRect_t2875960382 *)__this)->set_finalAlpha_20((0.0f));
		goto IL_0062;
	}

IL_0026:
	{
		UIRect_t2875960382 * L_2 = UIRect_get_parent_m3902033658(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		UIRect_t2875960382 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if (!L_4)
		{
			G_B5_0 = __this;
			goto IL_0052;
		}
	}
	{
		UIRect_t2875960382 * L_5 = V_0;
		int32_t L_6 = ___frameID0;
		NullCheck(L_5);
		float L_7 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_5, L_6);
		Color_t2555686324 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B6_0 = ((float)il2cpp_codegen_multiply((float)L_7, (float)L_9));
		G_B6_1 = G_B4_0;
		goto IL_005d;
	}

IL_0052:
	{
		Color_t2555686324 * L_10 = __this->get_address_of_mColor_22();
		float L_11 = L_10->get_a_3();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
	}

IL_005d:
	{
		NullCheck(G_B6_1);
		((UIRect_t2875960382 *)G_B6_1)->set_finalAlpha_20(G_B6_0);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UIWidget::Invalidate(System.Boolean)
extern "C"  void UIWidget_Invalidate_m1464153160 (UIWidget_t3538521925 * __this, bool ___includeChildren0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_Invalidate_m1464153160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)1);
		__this->set_mAlphaFrameID_49((-1));
		UIPanel_t1716472341 * L_0 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007f;
		}
	}
	{
		bool L_2 = __this->get_hideIfOffScreen_32();
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		UIPanel_t1716472341 * L_3 = __this->get_panel_36();
		NullCheck(L_3);
		bool L_4 = UIPanel_get_hasCumulativeClipping_m1863233802(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}

IL_003a:
	{
		UIPanel_t1716472341 * L_5 = __this->get_panel_36();
		NullCheck(L_5);
		bool L_6 = UIPanel_IsVisible_m4063952194(L_5, __this, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 1;
	}

IL_004c:
	{
		V_0 = (bool)G_B5_0;
		int32_t L_7 = Time_get_frameCount_m1220035214(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = UIWidget_CalculateCumulativeAlpha_m49421948(__this, L_7, /*hidden argument*/NULL);
		bool L_9 = V_0;
		UIWidget_UpdateVisibility_m2152012452(__this, (bool)((((float)L_8) > ((float)(0.001f)))? 1 : 0), L_9, /*hidden argument*/NULL);
		int32_t L_10 = Time_get_frameCount_m1220035214(NULL /*static, unused*/, /*hidden argument*/NULL);
		UIWidget_UpdateFinalAlpha_m4075768229(__this, L_10, /*hidden argument*/NULL);
		bool L_11 = ___includeChildren0;
		if (!L_11)
		{
			goto IL_007f;
		}
	}
	{
		UIRect_Invalidate_m3183458831(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Single UIWidget::CalculateCumulativeAlpha(System.Int32)
extern "C"  float UIWidget_CalculateCumulativeAlpha_m49421948 (UIWidget_t3538521925 * __this, int32_t ___frameID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CalculateCumulativeAlpha_m49421948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIRect_t2875960382 * V_0 = NULL;
	float G_B3_0 = 0.0f;
	{
		UIRect_t2875960382 * L_0 = UIRect_get_parent_m3902033658(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		UIRect_t2875960382 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		UIRect_t2875960382 * L_3 = V_0;
		int32_t L_4 = ___frameID0;
		NullCheck(L_3);
		float L_5 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_3, L_4);
		Color_t2555686324 * L_6 = __this->get_address_of_mColor_22();
		float L_7 = L_6->get_a_3();
		G_B3_0 = ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7));
		goto IL_0036;
	}

IL_002b:
	{
		Color_t2555686324 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::SetRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  void UIWidget_SetRect_m1100895450 (UIWidget_t3538521925 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetRect_m1100895450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Transform_t3600365921 * V_5 = NULL;
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Vector2_t2156229523  L_0 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___x0;
		float L_2 = ___x0;
		float L_3 = ___width2;
		float L_4 = (&V_0)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_1, ((float)il2cpp_codegen_add((float)L_2, (float)L_3)), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = ___y1;
		float L_7 = ___y1;
		float L_8 = ___height3;
		float L_9 = (&V_0)->get_y_1();
		float L_10 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_6, ((float)il2cpp_codegen_add((float)L_7, (float)L_8)), L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = ___width2;
		int32_t L_12 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_11, (float)(0.5f))), /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = ___height3;
		int32_t L_14 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_13, (float)(0.5f))), /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = (&V_0)->get_x_0();
		if ((!(((float)L_15) == ((float)(0.5f)))))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)L_16>>(int32_t)1))<<(int32_t)1));
	}

IL_005d:
	{
		float L_17 = (&V_0)->get_y_1();
		if ((!(((float)L_17) == ((float)(0.5f)))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18>>(int32_t)1))<<(int32_t)1));
	}

IL_0076:
	{
		Transform_t3600365921 * L_19 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		V_5 = L_19;
		Transform_t3600365921 * L_20 = V_5;
		NullCheck(L_20);
		Vector3_t3722313464  L_21 = Transform_get_localPosition_m4234289348(L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		float L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_23 = floorf(((float)il2cpp_codegen_add((float)L_22, (float)(0.5f))));
		(&V_6)->set_x_1(L_23);
		float L_24 = V_2;
		float L_25 = floorf(((float)il2cpp_codegen_add((float)L_24, (float)(0.5f))));
		(&V_6)->set_y_2(L_25);
		int32_t L_26 = V_3;
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_00c0;
		}
	}
	{
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_3 = L_28;
	}

IL_00c0:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_29) >= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_4 = L_31;
	}

IL_00d5:
	{
		Transform_t3600365921 * L_32 = V_5;
		Vector3_t3722313464  L_33 = V_6;
		NullCheck(L_32);
		Transform_set_localPosition_m4128471975(L_32, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_3;
		UIWidget_set_width_m181008468(__this, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_4;
		UIWidget_set_height_m878974275(__this, L_35, /*hidden argument*/NULL);
		bool L_36 = UIRect_get_isAnchored_m1025914852(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0192;
		}
	}
	{
		Transform_t3600365921 * L_37 = V_5;
		NullCheck(L_37);
		Transform_t3600365921 * L_38 = Transform_get_parent_m835071599(L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		AnchorPoint_t1754718329 * L_39 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_39);
		Transform_t3600365921 * L_40 = L_39->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0124;
		}
	}
	{
		AnchorPoint_t1754718329 * L_42 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		Transform_t3600365921 * L_43 = V_5;
		float L_44 = ___x0;
		NullCheck(L_42);
		AnchorPoint_SetHorizontal_m3420307353(L_42, L_43, L_44, /*hidden argument*/NULL);
	}

IL_0124:
	{
		AnchorPoint_t1754718329 * L_45 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_45);
		Transform_t3600365921 * L_46 = L_45->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0149;
		}
	}
	{
		AnchorPoint_t1754718329 * L_48 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		Transform_t3600365921 * L_49 = V_5;
		float L_50 = ___x0;
		float L_51 = ___width2;
		NullCheck(L_48);
		AnchorPoint_SetHorizontal_m3420307353(L_48, L_49, ((float)il2cpp_codegen_add((float)L_50, (float)L_51)), /*hidden argument*/NULL);
	}

IL_0149:
	{
		AnchorPoint_t1754718329 * L_52 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_52);
		Transform_t3600365921 * L_53 = L_52->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_54 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_016c;
		}
	}
	{
		AnchorPoint_t1754718329 * L_55 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		Transform_t3600365921 * L_56 = V_5;
		float L_57 = ___y1;
		NullCheck(L_55);
		AnchorPoint_SetVertical_m4045229769(L_55, L_56, L_57, /*hidden argument*/NULL);
	}

IL_016c:
	{
		AnchorPoint_t1754718329 * L_58 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_58);
		Transform_t3600365921 * L_59 = L_58->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0192;
		}
	}
	{
		AnchorPoint_t1754718329 * L_61 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		Transform_t3600365921 * L_62 = V_5;
		float L_63 = ___y1;
		float L_64 = ___height3;
		NullCheck(L_61);
		AnchorPoint_SetVertical_m4045229769(L_61, L_62, ((float)il2cpp_codegen_add((float)L_63, (float)L_64)), /*hidden argument*/NULL);
	}

IL_0192:
	{
		return;
	}
}
// System.Void UIWidget::ResizeCollider()
extern "C"  void UIWidget_ResizeCollider_m1303972682 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ResizeCollider_m1303972682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BoxCollider_t1640800422 * V_0 = NULL;
	{
		BoxCollider_t1640800422 * L_0 = Component_GetComponent_TisBoxCollider_t1640800422_m4104100802(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider_t1640800422_m4104100802_RuntimeMethod_var);
		V_0 = L_0;
		BoxCollider_t1640800422 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		BoxCollider_t1640800422 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m3438335504(NULL /*static, unused*/, __this, L_3, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_001f:
	{
		BoxCollider2D_t3581341831 * L_4 = Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m1925672297(NULL /*static, unused*/, __this, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Int32 UIWidget::FullCompareFunc(UIWidget,UIWidget)
extern "C"  int32_t UIWidget_FullCompareFunc_m1006404042 (RuntimeObject * __this /* static, unused */, UIWidget_t3538521925 * ___left0, UIWidget_t3538521925 * ___right1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_FullCompareFunc_m1006404042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		UIWidget_t3538521925 * L_0 = ___left0;
		NullCheck(L_0);
		UIPanel_t1716472341 * L_1 = L_0->get_panel_36();
		UIWidget_t3538521925 * L_2 = ___right1;
		NullCheck(L_2);
		UIPanel_t1716472341 * L_3 = L_2->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t1716472341_il2cpp_TypeInfo_var);
		int32_t L_4 = UIPanel_CompareFunc_m2640156642(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_t3538521925 * L_6 = ___left0;
		UIWidget_t3538521925 * L_7 = ___right1;
		int32_t L_8 = UIWidget_PanelCompareFunc_m3717063172(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		goto IL_0025;
	}

IL_0024:
	{
		int32_t L_9 = V_0;
		G_B3_0 = L_9;
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Int32 UIWidget::PanelCompareFunc(UIWidget,UIWidget)
extern "C"  int32_t UIWidget_PanelCompareFunc_m3717063172 (RuntimeObject * __this /* static, unused */, UIWidget_t3538521925 * ___left0, UIWidget_t3538521925 * ___right1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_PanelCompareFunc_m3717063172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	Material_t340375123 * V_1 = NULL;
	int32_t G_B13_0 = 0;
	{
		UIWidget_t3538521925 * L_0 = ___left0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_mDepth_26();
		UIWidget_t3538521925 * L_2 = ___right1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_mDepth_26();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0013;
		}
	}
	{
		return (-1);
	}

IL_0013:
	{
		UIWidget_t3538521925 * L_4 = ___left0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_mDepth_26();
		UIWidget_t3538521925 * L_6 = ___right1;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_mDepth_26();
		if ((((int32_t)L_5) <= ((int32_t)L_7)))
		{
			goto IL_0026;
		}
	}
	{
		return 1;
	}

IL_0026:
	{
		UIWidget_t3538521925 * L_8 = ___left0;
		NullCheck(L_8);
		Material_t340375123 * L_9 = VirtFuncInvoker0< Material_t340375123 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, L_8);
		V_0 = L_9;
		UIWidget_t3538521925 * L_10 = ___right1;
		NullCheck(L_10);
		Material_t340375123 * L_11 = VirtFuncInvoker0< Material_t340375123 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, L_10);
		V_1 = L_11;
		Material_t340375123 * L_12 = V_0;
		Material_t340375123 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}

IL_0042:
	{
		Material_t340375123 * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_15, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0050;
		}
	}
	{
		return 1;
	}

IL_0050:
	{
		Material_t340375123 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_17, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_005e;
		}
	}
	{
		return (-1);
	}

IL_005e:
	{
		Material_t340375123 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = Object_GetInstanceID_m1255174761(L_19, /*hidden argument*/NULL);
		Material_t340375123 * L_21 = V_1;
		NullCheck(L_21);
		int32_t L_22 = Object_GetInstanceID_m1255174761(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_20) >= ((int32_t)L_22)))
		{
			goto IL_0075;
		}
	}
	{
		G_B13_0 = (-1);
		goto IL_0076;
	}

IL_0075:
	{
		G_B13_0 = 1;
	}

IL_0076:
	{
		return G_B13_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds()
extern "C"  Bounds_t2266837910  UIWidget_CalculateBounds_m602793801 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		Bounds_t2266837910  L_0 = UIWidget_CalculateBounds_m3008782085(__this, (Transform_t3600365921 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds(UnityEngine.Transform)
extern "C"  Bounds_t2266837910  UIWidget_CalculateBounds_m3008782085 (UIWidget_t3538521925 * __this, Transform_t3600365921 * ___relativeParent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CalculateBounds_m3008782085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Matrix4x4_t1817901843  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3U5BU5D_t1718750761* V_4 = NULL;
	Bounds_t2266837910  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	{
		Transform_t3600365921 * L_0 = ___relativeParent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(10 /* UnityEngine.Vector3[] UIRect::get_localCorners() */, __this);
		V_0 = L_2;
		Vector3U5BU5D_t1718750761* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m1937678907((&V_1), (*(Vector3_t3722313464 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), L_4, /*hidden argument*/NULL);
		V_2 = 1;
		goto IL_0049;
	}

IL_0032:
	{
		Vector3U5BU5D_t1718750761* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		Bounds_Encapsulate_m3553480203((&V_1), (*(Vector3_t3722313464 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))), /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0049:
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) < ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		Bounds_t2266837910  L_9 = V_1;
		return L_9;
	}

IL_0052:
	{
		Transform_t3600365921 * L_10 = ___relativeParent0;
		NullCheck(L_10);
		Matrix4x4_t1817901843  L_11 = Transform_get_worldToLocalMatrix_m399704877(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Vector3U5BU5D_t1718750761* L_12 = VirtFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, __this);
		V_4 = L_12;
		Vector3U5BU5D_t1718750761* L_13 = V_4;
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = Matrix4x4_MultiplyPoint3x4_m4145063176((&V_3), (*(Vector3_t3722313464 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_15 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m1937678907((&V_5), L_14, L_15, /*hidden argument*/NULL);
		V_6 = 1;
		goto IL_00ab;
	}

IL_0089:
	{
		Vector3U5BU5D_t1718750761* L_16 = V_4;
		int32_t L_17 = V_6;
		NullCheck(L_16);
		Vector3_t3722313464  L_18 = Matrix4x4_MultiplyPoint3x4_m4145063176((&V_3), (*(Vector3_t3722313464 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))), /*hidden argument*/NULL);
		Bounds_Encapsulate_m3553480203((&V_5), L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_00ab:
	{
		int32_t L_20 = V_6;
		if ((((int32_t)L_20) < ((int32_t)4)))
		{
			goto IL_0089;
		}
	}
	{
		Bounds_t2266837910  L_21 = V_5;
		return L_21;
	}
}
// System.Void UIWidget::SetDirty()
extern "C"  void UIWidget_SetDirty_m2297015651 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetDirty_m2297015651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIDrawCall_t1293405319 * L_0 = __this->get_drawCall_47();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		UIDrawCall_t1293405319 * L_2 = __this->get_drawCall_47();
		NullCheck(L_2);
		L_2->set_isDirty_32((bool)1);
		goto IL_003f;
	}

IL_0022:
	{
		bool L_3 = UIWidget_get_isVisible_m524426372(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		bool L_4 = UIWidget_get_hasVertices_m2576649473(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		UIWidget_CreatePanel_m976889642(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void UIWidget::RemoveFromPanel()
extern "C"  void UIWidget_RemoveFromPanel_m725257632 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_RemoveFromPanel_m725257632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIPanel_t1716472341 * L_0 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		UIPanel_t1716472341 * L_2 = __this->get_panel_36();
		NullCheck(L_2);
		UIPanel_RemoveWidget_m2262424736(L_2, __this, /*hidden argument*/NULL);
		__this->set_panel_36((UIPanel_t1716472341 *)NULL);
	}

IL_0024:
	{
		__this->set_drawCall_47((UIDrawCall_t1293405319 *)NULL);
		return;
	}
}
// System.Void UIWidget::MarkAsChanged()
extern "C"  void UIWidget_MarkAsChanged_m786570369 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MarkAsChanged_m786570369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_0 = NGUITools_GetActive_m3370498562(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0055;
		}
	}
	{
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)1);
		UIPanel_t1716472341 * L_1 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m1538523522(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		bool L_6 = __this->get_mPlayMode_39();
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		UIWidget_SetDirty_m2297015651(__this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m2684013386(__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// UIPanel UIWidget::CreatePanel()
extern "C"  UIPanel_t1716472341 * UIWidget_CreatePanel_m976889642 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CreatePanel_m976889642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((UIRect_t2875960382 *)__this)->get_mStarted_19();
		if (!L_0)
		{
			goto IL_0085;
		}
	}
	{
		UIPanel_t1716472341 * L_1 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0085;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m1538523522(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0085;
		}
	}
	{
		Transform_t3600365921 * L_6 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = UIRect_get_cachedGameObject_m4173353535(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = GameObject_get_layer_m4158800245(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t1716472341_il2cpp_TypeInfo_var);
		UIPanel_t1716472341 * L_9 = UIPanel_Find_m92918396(NULL /*static, unused*/, L_6, (bool)1, L_8, /*hidden argument*/NULL);
		__this->set_panel_36(L_9);
		UIPanel_t1716472341 * L_10 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_10, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0085;
		}
	}
	{
		((UIRect_t2875960382 *)__this)->set_mParentFound_11((bool)0);
		UIPanel_t1716472341 * L_12 = __this->get_panel_36();
		NullCheck(L_12);
		UIPanel_AddWidget_m4245952570(L_12, __this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m2684013386(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIRect::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0085:
	{
		UIPanel_t1716472341 * L_13 = __this->get_panel_36();
		return L_13;
	}
}
// System.Void UIWidget::CheckLayer()
extern "C"  void UIWidget_CheckLayer_m2684013386 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CheckLayer_m2684013386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIPanel_t1716472341 * L_0 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0057;
		}
	}
	{
		UIPanel_t1716472341 * L_2 = __this->get_panel_36();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m4158800245(L_3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = GameObject_get_layer_m4158800245(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_6)))
		{
			goto IL_0057;
		}
	}
	{
		Debug_LogWarning_m2186869729(NULL /*static, unused*/, _stringLiteral2595132038, __this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		UIPanel_t1716472341 * L_8 = __this->get_panel_36();
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = GameObject_get_layer_m4158800245(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_set_layer_m3294992795(L_7, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UIWidget::ParentHasChanged()
extern "C"  void UIWidget_ParentHasChanged_m2262919468 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ParentHasChanged_m2262919468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPanel_t1716472341 * V_0 = NULL;
	{
		UIRect_ParentHasChanged_m4139252803(__this, /*hidden argument*/NULL);
		UIPanel_t1716472341 * L_0 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t3600365921 * L_2 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = UIRect_get_cachedGameObject_m4173353535(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m4158800245(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t1716472341_il2cpp_TypeInfo_var);
		UIPanel_t1716472341 * L_5 = UIPanel_Find_m92918396(NULL /*static, unused*/, L_2, (bool)1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		UIPanel_t1716472341 * L_6 = __this->get_panel_36();
		UIPanel_t1716472341 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		UIWidget_RemoveFromPanel_m725257632(__this, /*hidden argument*/NULL);
		UIWidget_CreatePanel_m976889642(__this, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UIWidget::Awake()
extern "C"  void UIWidget_Awake_m3305545152 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		UIRect_Awake_m1429700095(__this, /*hidden argument*/NULL);
		bool L_0 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_39(L_0);
		return;
	}
}
// System.Void UIWidget::OnInit()
extern "C"  void UIWidget_OnInit_m3633263421 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		UIRect_OnInit_m1393697898(__this, /*hidden argument*/NULL);
		UIWidget_RemoveFromPanel_m725257632(__this, /*hidden argument*/NULL);
		__this->set_mMoved_46((bool)1);
		UIRect_Update_m267147630(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::UpgradeFrom265()
extern "C"  void UIWidget_UpgradeFrom265_m4222449532 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpgradeFrom265_m4222449532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localScale_m129152068(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_4 = Mathf_Abs_m2460432655(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_mWidth_24(L_4);
		float L_5 = (&V_0)->get_y_2();
		int32_t L_6 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_7 = Mathf_Abs_m2460432655(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_mHeight_25(L_7);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m2045900173(NULL /*static, unused*/, L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnStart()
extern "C"  void UIWidget_OnStart_m1214323431 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		UIWidget_CreatePanel_m976889642(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnAnchor()
extern "C"  void UIWidget_OnAnchor_m4074470753 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_OnAnchor_m4074470753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Transform_t3600365921 * V_4 = NULL;
	Transform_t3600365921 * V_5 = NULL;
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3U5BU5D_t1718750761* V_8 = NULL;
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3U5BU5D_t1718750761* V_10 = NULL;
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3U5BU5D_t1718750761* V_12 = NULL;
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3U5BU5D_t1718750761* V_14 = NULL;
	Vector3_t3722313464  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3U5BU5D_t1718750761* V_16 = NULL;
	Vector3_t3722313464  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t3722313464  V_18;
	memset(&V_18, 0, sizeof(V_18));
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	UIWidget_t3538521925 * G_B7_0 = NULL;
	UIWidget_t3538521925 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	UIWidget_t3538521925 * G_B8_1 = NULL;
	{
		Transform_t3600365921 * L_0 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		V_4 = L_0;
		Transform_t3600365921 * L_1 = V_4;
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = Transform_get_parent_m835071599(L_1, /*hidden argument*/NULL);
		V_5 = L_2;
		Transform_t3600365921 * L_3 = V_4;
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_localPosition_m4234289348(L_3, /*hidden argument*/NULL);
		V_6 = L_4;
		Vector2_t2156229523  L_5 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_7 = L_5;
		AnchorPoint_t1754718329 * L_6 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = L_6->get_target_0();
		AnchorPoint_t1754718329 * L_8 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t1754718329 * L_11 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_11);
		Transform_t3600365921 * L_12 = L_11->get_target_0();
		AnchorPoint_t1754718329 * L_13 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_13);
		Transform_t3600365921 * L_14 = L_13->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t1754718329 * L_16 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_16);
		Transform_t3600365921 * L_17 = L_16->get_target_0();
		AnchorPoint_t1754718329 * L_18 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_18);
		Transform_t3600365921 * L_19 = L_18->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t1754718329 * L_21 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		Transform_t3600365921 * L_22 = V_5;
		NullCheck(L_21);
		Vector3U5BU5D_t1718750761* L_23 = AnchorPoint_GetSides_m3751877420(L_21, L_22, /*hidden argument*/NULL);
		V_8 = L_23;
		Vector3U5BU5D_t1718750761* L_24 = V_8;
		if (!L_24)
		{
			goto IL_0184;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_25 = V_8;
		NullCheck(L_25);
		float L_26 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1718750761* L_27 = V_8;
		NullCheck(L_27);
		float L_28 = ((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t1754718329 * L_29 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_29);
		float L_30 = L_29->get_relative_1();
		float L_31 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_26, L_28, L_30, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_32 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_32);
		int32_t L_33 = L_32->get_absolute_2();
		V_0 = ((float)il2cpp_codegen_add((float)L_31, (float)(((float)((float)L_33)))));
		Vector3U5BU5D_t1718750761* L_34 = V_8;
		NullCheck(L_34);
		float L_35 = ((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1718750761* L_36 = V_8;
		NullCheck(L_36);
		float L_37 = ((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t1754718329 * L_38 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_38);
		float L_39 = L_38->get_relative_1();
		float L_40 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_35, L_37, L_39, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_41 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_41);
		int32_t L_42 = L_41->get_absolute_2();
		V_2 = ((float)il2cpp_codegen_add((float)L_40, (float)(((float)((float)L_42)))));
		Vector3U5BU5D_t1718750761* L_43 = V_8;
		NullCheck(L_43);
		float L_44 = ((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1718750761* L_45 = V_8;
		NullCheck(L_45);
		float L_46 = ((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t1754718329 * L_47 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_47);
		float L_48 = L_47->get_relative_1();
		float L_49 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_44, L_46, L_48, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_50 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_50);
		int32_t L_51 = L_50->get_absolute_2();
		V_1 = ((float)il2cpp_codegen_add((float)L_49, (float)(((float)((float)L_51)))));
		Vector3U5BU5D_t1718750761* L_52 = V_8;
		NullCheck(L_52);
		float L_53 = ((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1718750761* L_54 = V_8;
		NullCheck(L_54);
		float L_55 = ((L_54)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t1754718329 * L_56 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_56);
		float L_57 = L_56->get_relative_1();
		float L_58 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_53, L_55, L_57, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_59 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_59);
		int32_t L_60 = L_59->get_absolute_2();
		V_3 = ((float)il2cpp_codegen_add((float)L_58, (float)(((float)((float)L_60)))));
		__this->set_mIsInFront_44((bool)1);
		goto IL_020d;
	}

IL_0184:
	{
		AnchorPoint_t1754718329 * L_61 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		Transform_t3600365921 * L_62 = V_5;
		Vector3_t3722313464  L_63 = UIRect_GetLocalPos_m1776190051(__this, L_61, L_62, /*hidden argument*/NULL);
		V_9 = L_63;
		float L_64 = (&V_9)->get_x_1();
		AnchorPoint_t1754718329 * L_65 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_65);
		int32_t L_66 = L_65->get_absolute_2();
		V_0 = ((float)il2cpp_codegen_add((float)L_64, (float)(((float)((float)L_66)))));
		float L_67 = (&V_9)->get_y_2();
		AnchorPoint_t1754718329 * L_68 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_68);
		int32_t L_69 = L_68->get_absolute_2();
		V_1 = ((float)il2cpp_codegen_add((float)L_67, (float)(((float)((float)L_69)))));
		float L_70 = (&V_9)->get_x_1();
		AnchorPoint_t1754718329 * L_71 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_71);
		int32_t L_72 = L_71->get_absolute_2();
		V_2 = ((float)il2cpp_codegen_add((float)L_70, (float)(((float)((float)L_72)))));
		float L_73 = (&V_9)->get_y_2();
		AnchorPoint_t1754718329 * L_74 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_74);
		int32_t L_75 = L_74->get_absolute_2();
		V_3 = ((float)il2cpp_codegen_add((float)L_73, (float)(((float)((float)L_75)))));
		bool L_76 = __this->get_hideIfOffScreen_32();
		G_B6_0 = __this;
		if (!L_76)
		{
			G_B7_0 = __this;
			goto IL_0207;
		}
	}
	{
		float L_77 = (&V_9)->get_z_3();
		G_B8_0 = ((((int32_t)((!(((float)L_77) >= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B8_1 = G_B6_0;
		goto IL_0208;
	}

IL_0207:
	{
		G_B8_0 = 1;
		G_B8_1 = G_B7_0;
	}

IL_0208:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_mIsInFront_44((bool)G_B8_0);
	}

IL_020d:
	{
		goto IL_04d1;
	}

IL_0212:
	{
		__this->set_mIsInFront_44((bool)1);
		AnchorPoint_t1754718329 * L_78 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_78);
		Transform_t3600365921 * L_79 = L_78->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_80 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_02ab;
		}
	}
	{
		AnchorPoint_t1754718329 * L_81 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		Transform_t3600365921 * L_82 = V_5;
		NullCheck(L_81);
		Vector3U5BU5D_t1718750761* L_83 = AnchorPoint_GetSides_m3751877420(L_81, L_82, /*hidden argument*/NULL);
		V_10 = L_83;
		Vector3U5BU5D_t1718750761* L_84 = V_10;
		if (!L_84)
		{
			goto IL_0281;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_85 = V_10;
		NullCheck(L_85);
		float L_86 = ((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1718750761* L_87 = V_10;
		NullCheck(L_87);
		float L_88 = ((L_87)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t1754718329 * L_89 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_89);
		float L_90 = L_89->get_relative_1();
		float L_91 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_86, L_88, L_90, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_92 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_92);
		int32_t L_93 = L_92->get_absolute_2();
		V_0 = ((float)il2cpp_codegen_add((float)L_91, (float)(((float)((float)L_93)))));
		goto IL_02a6;
	}

IL_0281:
	{
		AnchorPoint_t1754718329 * L_94 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		Transform_t3600365921 * L_95 = V_5;
		Vector3_t3722313464  L_96 = UIRect_GetLocalPos_m1776190051(__this, L_94, L_95, /*hidden argument*/NULL);
		V_11 = L_96;
		float L_97 = (&V_11)->get_x_1();
		AnchorPoint_t1754718329 * L_98 = ((UIRect_t2875960382 *)__this)->get_leftAnchor_2();
		NullCheck(L_98);
		int32_t L_99 = L_98->get_absolute_2();
		V_0 = ((float)il2cpp_codegen_add((float)L_97, (float)(((float)((float)L_99)))));
	}

IL_02a6:
	{
		goto IL_02c3;
	}

IL_02ab:
	{
		float L_100 = (&V_6)->get_x_1();
		float L_101 = (&V_7)->get_x_0();
		int32_t L_102 = __this->get_mWidth_24();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_100, (float)((float)il2cpp_codegen_multiply((float)L_101, (float)(((float)((float)L_102)))))));
	}

IL_02c3:
	{
		AnchorPoint_t1754718329 * L_103 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_103);
		Transform_t3600365921 * L_104 = L_103->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_105 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_0355;
		}
	}
	{
		AnchorPoint_t1754718329 * L_106 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		Transform_t3600365921 * L_107 = V_5;
		NullCheck(L_106);
		Vector3U5BU5D_t1718750761* L_108 = AnchorPoint_GetSides_m3751877420(L_106, L_107, /*hidden argument*/NULL);
		V_12 = L_108;
		Vector3U5BU5D_t1718750761* L_109 = V_12;
		if (!L_109)
		{
			goto IL_032b;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_110 = V_12;
		NullCheck(L_110);
		float L_111 = ((L_110)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1718750761* L_112 = V_12;
		NullCheck(L_112);
		float L_113 = ((L_112)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t1754718329 * L_114 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_114);
		float L_115 = L_114->get_relative_1();
		float L_116 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_111, L_113, L_115, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_117 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_117);
		int32_t L_118 = L_117->get_absolute_2();
		V_2 = ((float)il2cpp_codegen_add((float)L_116, (float)(((float)((float)L_118)))));
		goto IL_0350;
	}

IL_032b:
	{
		AnchorPoint_t1754718329 * L_119 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		Transform_t3600365921 * L_120 = V_5;
		Vector3_t3722313464  L_121 = UIRect_GetLocalPos_m1776190051(__this, L_119, L_120, /*hidden argument*/NULL);
		V_13 = L_121;
		float L_122 = (&V_13)->get_x_1();
		AnchorPoint_t1754718329 * L_123 = ((UIRect_t2875960382 *)__this)->get_rightAnchor_3();
		NullCheck(L_123);
		int32_t L_124 = L_123->get_absolute_2();
		V_2 = ((float)il2cpp_codegen_add((float)L_122, (float)(((float)((float)L_124)))));
	}

IL_0350:
	{
		goto IL_0375;
	}

IL_0355:
	{
		float L_125 = (&V_6)->get_x_1();
		float L_126 = (&V_7)->get_x_0();
		int32_t L_127 = __this->get_mWidth_24();
		int32_t L_128 = __this->get_mWidth_24();
		V_2 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)L_125, (float)((float)il2cpp_codegen_multiply((float)L_126, (float)(((float)((float)L_127))))))), (float)(((float)((float)L_128)))));
	}

IL_0375:
	{
		AnchorPoint_t1754718329 * L_129 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_129);
		Transform_t3600365921 * L_130 = L_129->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_131 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_130, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_0407;
		}
	}
	{
		AnchorPoint_t1754718329 * L_132 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		Transform_t3600365921 * L_133 = V_5;
		NullCheck(L_132);
		Vector3U5BU5D_t1718750761* L_134 = AnchorPoint_GetSides_m3751877420(L_132, L_133, /*hidden argument*/NULL);
		V_14 = L_134;
		Vector3U5BU5D_t1718750761* L_135 = V_14;
		if (!L_135)
		{
			goto IL_03dd;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_136 = V_14;
		NullCheck(L_136);
		float L_137 = ((L_136)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1718750761* L_138 = V_14;
		NullCheck(L_138);
		float L_139 = ((L_138)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t1754718329 * L_140 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_140);
		float L_141 = L_140->get_relative_1();
		float L_142 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_137, L_139, L_141, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_143 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_143);
		int32_t L_144 = L_143->get_absolute_2();
		V_1 = ((float)il2cpp_codegen_add((float)L_142, (float)(((float)((float)L_144)))));
		goto IL_0402;
	}

IL_03dd:
	{
		AnchorPoint_t1754718329 * L_145 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		Transform_t3600365921 * L_146 = V_5;
		Vector3_t3722313464  L_147 = UIRect_GetLocalPos_m1776190051(__this, L_145, L_146, /*hidden argument*/NULL);
		V_15 = L_147;
		float L_148 = (&V_15)->get_y_2();
		AnchorPoint_t1754718329 * L_149 = ((UIRect_t2875960382 *)__this)->get_bottomAnchor_4();
		NullCheck(L_149);
		int32_t L_150 = L_149->get_absolute_2();
		V_1 = ((float)il2cpp_codegen_add((float)L_148, (float)(((float)((float)L_150)))));
	}

IL_0402:
	{
		goto IL_041f;
	}

IL_0407:
	{
		float L_151 = (&V_6)->get_y_2();
		float L_152 = (&V_7)->get_y_1();
		int32_t L_153 = __this->get_mHeight_25();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_151, (float)((float)il2cpp_codegen_multiply((float)L_152, (float)(((float)((float)L_153)))))));
	}

IL_041f:
	{
		AnchorPoint_t1754718329 * L_154 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_154);
		Transform_t3600365921 * L_155 = L_154->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_156 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_155, /*hidden argument*/NULL);
		if (!L_156)
		{
			goto IL_04b1;
		}
	}
	{
		AnchorPoint_t1754718329 * L_157 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		Transform_t3600365921 * L_158 = V_5;
		NullCheck(L_157);
		Vector3U5BU5D_t1718750761* L_159 = AnchorPoint_GetSides_m3751877420(L_157, L_158, /*hidden argument*/NULL);
		V_16 = L_159;
		Vector3U5BU5D_t1718750761* L_160 = V_16;
		if (!L_160)
		{
			goto IL_0487;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_161 = V_16;
		NullCheck(L_161);
		float L_162 = ((L_161)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1718750761* L_163 = V_16;
		NullCheck(L_163);
		float L_164 = ((L_163)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t1754718329 * L_165 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_165);
		float L_166 = L_165->get_relative_1();
		float L_167 = NGUIMath_Lerp_m466078211(NULL /*static, unused*/, L_162, L_164, L_166, /*hidden argument*/NULL);
		AnchorPoint_t1754718329 * L_168 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_168);
		int32_t L_169 = L_168->get_absolute_2();
		V_3 = ((float)il2cpp_codegen_add((float)L_167, (float)(((float)((float)L_169)))));
		goto IL_04ac;
	}

IL_0487:
	{
		AnchorPoint_t1754718329 * L_170 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		Transform_t3600365921 * L_171 = V_5;
		Vector3_t3722313464  L_172 = UIRect_GetLocalPos_m1776190051(__this, L_170, L_171, /*hidden argument*/NULL);
		V_17 = L_172;
		float L_173 = (&V_17)->get_y_2();
		AnchorPoint_t1754718329 * L_174 = ((UIRect_t2875960382 *)__this)->get_topAnchor_5();
		NullCheck(L_174);
		int32_t L_175 = L_174->get_absolute_2();
		V_3 = ((float)il2cpp_codegen_add((float)L_173, (float)(((float)((float)L_175)))));
	}

IL_04ac:
	{
		goto IL_04d1;
	}

IL_04b1:
	{
		float L_176 = (&V_6)->get_y_2();
		float L_177 = (&V_7)->get_y_1();
		int32_t L_178 = __this->get_mHeight_25();
		int32_t L_179 = __this->get_mHeight_25();
		V_3 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)L_176, (float)((float)il2cpp_codegen_multiply((float)L_177, (float)(((float)((float)L_178))))))), (float)(((float)((float)L_179)))));
	}

IL_04d1:
	{
		float L_180 = V_0;
		float L_181 = V_2;
		float L_182 = (&V_7)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_183 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_180, L_181, L_182, /*hidden argument*/NULL);
		float L_184 = V_1;
		float L_185 = V_3;
		float L_186 = (&V_7)->get_y_1();
		float L_187 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_184, L_185, L_186, /*hidden argument*/NULL);
		float L_188 = (&V_6)->get_z_3();
		Vector3__ctor_m3353183577((&V_18), L_183, L_187, L_188, /*hidden argument*/NULL);
		float L_189 = (&V_18)->get_x_1();
		float L_190 = bankers_roundf(L_189);
		(&V_18)->set_x_1(L_190);
		float L_191 = (&V_18)->get_y_2();
		float L_192 = bankers_roundf(L_191);
		(&V_18)->set_y_2(L_192);
		float L_193 = V_2;
		float L_194 = V_0;
		int32_t L_195 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)L_193, (float)L_194)), (float)(0.5f))), /*hidden argument*/NULL);
		V_19 = L_195;
		float L_196 = V_3;
		float L_197 = V_1;
		int32_t L_198 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)L_196, (float)L_197)), (float)(0.5f))), /*hidden argument*/NULL);
		V_20 = L_198;
		int32_t L_199 = __this->get_keepAspectRatio_33();
		if (!L_199)
		{
			goto IL_058f;
		}
	}
	{
		float L_200 = __this->get_aspectRatio_34();
		if ((((float)L_200) == ((float)(0.0f))))
		{
			goto IL_058f;
		}
	}
	{
		int32_t L_201 = __this->get_keepAspectRatio_33();
		if ((!(((uint32_t)L_201) == ((uint32_t)2))))
		{
			goto IL_057e;
		}
	}
	{
		int32_t L_202 = V_20;
		float L_203 = __this->get_aspectRatio_34();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_204 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_202))), (float)L_203)), /*hidden argument*/NULL);
		V_19 = L_204;
		goto IL_058f;
	}

IL_057e:
	{
		int32_t L_205 = V_19;
		float L_206 = __this->get_aspectRatio_34();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_207 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)(((float)((float)L_205)))/(float)L_206)), /*hidden argument*/NULL);
		V_20 = L_207;
	}

IL_058f:
	{
		int32_t L_208 = V_19;
		int32_t L_209 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_208) >= ((int32_t)L_209)))
		{
			goto IL_05a4;
		}
	}
	{
		int32_t L_210 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_19 = L_210;
	}

IL_05a4:
	{
		int32_t L_211 = V_20;
		int32_t L_212 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_211) >= ((int32_t)L_212)))
		{
			goto IL_05b9;
		}
	}
	{
		int32_t L_213 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_20 = L_213;
	}

IL_05b9:
	{
		Vector3_t3722313464  L_214 = V_6;
		Vector3_t3722313464  L_215 = V_18;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_216 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_214, L_215, /*hidden argument*/NULL);
		float L_217 = Vector3_SqrMagnitude_m3025115945(NULL /*static, unused*/, L_216, /*hidden argument*/NULL);
		if ((!(((float)L_217) > ((float)(0.001f)))))
		{
			goto IL_05f0;
		}
	}
	{
		Transform_t3600365921 * L_218 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_219 = V_18;
		NullCheck(L_218);
		Transform_set_localPosition_m4128471975(L_218, L_219, /*hidden argument*/NULL);
		bool L_220 = __this->get_mIsInFront_44();
		if (!L_220)
		{
			goto IL_05f0;
		}
	}
	{
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)1);
	}

IL_05f0:
	{
		int32_t L_221 = __this->get_mWidth_24();
		int32_t L_222 = V_19;
		if ((!(((uint32_t)L_221) == ((uint32_t)L_222))))
		{
			goto IL_060a;
		}
	}
	{
		int32_t L_223 = __this->get_mHeight_25();
		int32_t L_224 = V_20;
		if ((((int32_t)L_223) == ((int32_t)L_224)))
		{
			goto IL_063d;
		}
	}

IL_060a:
	{
		int32_t L_225 = V_19;
		__this->set_mWidth_24(L_225);
		int32_t L_226 = V_20;
		__this->set_mHeight_25(L_226);
		bool L_227 = __this->get_mIsInFront_44();
		if (!L_227)
		{
			goto IL_062c;
		}
	}
	{
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)1);
	}

IL_062c:
	{
		bool L_228 = __this->get_autoResizeBoxCollider_31();
		if (!L_228)
		{
			goto IL_063d;
		}
	}
	{
		UIWidget_ResizeCollider_m1303972682(__this, /*hidden argument*/NULL);
	}

IL_063d:
	{
		return;
	}
}
// System.Void UIWidget::OnUpdate()
extern "C"  void UIWidget_OnUpdate_m3837236151 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_OnUpdate_m3837236151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIPanel_t1716472341 * L_0 = __this->get_panel_36();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m976889642(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UIWidget::OnApplicationPause(System.Boolean)
extern "C"  void UIWidget_OnApplicationPause_m2765049197 (UIWidget_t3538521925 * __this, bool ___paused0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___paused0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_000c:
	{
		return;
	}
}
// System.Void UIWidget::OnDisable()
extern "C"  void UIWidget_OnDisable_m1454496561 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		UIWidget_RemoveFromPanel_m725257632(__this, /*hidden argument*/NULL);
		UIRect_OnDisable_m201565937(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnDestroy()
extern "C"  void UIWidget_OnDestroy_m371483101 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		UIWidget_RemoveFromPanel_m725257632(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIWidget::UpdateVisibility(System.Boolean,System.Boolean)
extern "C"  bool UIWidget_UpdateVisibility_m2152012452 (UIWidget_t3538521925 * __this, bool ___visibleByAlpha0, bool ___visibleByPanel1, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_42();
		bool L_1 = ___visibleByAlpha0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		bool L_2 = __this->get_mIsVisibleByPanel_43();
		bool L_3 = ___visibleByPanel1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002f;
		}
	}

IL_0018:
	{
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)1);
		bool L_4 = ___visibleByAlpha0;
		__this->set_mIsVisibleByAlpha_42(L_4);
		bool L_5 = ___visibleByPanel1;
		__this->set_mIsVisibleByPanel_43(L_5);
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean UIWidget::UpdateTransform(System.Int32)
extern "C"  bool UIWidget_UpdateTransform_m3758311267 (UIWidget_t3538521925 * __this, int32_t ___frame0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpdateTransform_m3758311267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector2_t2156229523  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t3722313464  V_12;
	memset(&V_12, 0, sizeof(V_12));
	int32_t G_B13_0 = 0;
	{
		Transform_t3600365921 * L_0 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_39(L_1);
		bool L_2 = __this->get_mMoved_46();
		if (!L_2)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_mMoved_46((bool)1);
		__this->set_mMatrixFrame_50((-1));
		Transform_t3600365921 * L_3 = V_0;
		NullCheck(L_3);
		Transform_set_hasChanged_m4213723989(L_3, (bool)0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_0();
		int32_t L_6 = __this->get_mWidth_24();
		V_2 = ((float)il2cpp_codegen_multiply((float)((-L_5)), (float)(((float)((float)L_6)))));
		float L_7 = (&V_1)->get_y_1();
		int32_t L_8 = __this->get_mHeight_25();
		V_3 = ((float)il2cpp_codegen_multiply((float)((-L_7)), (float)(((float)((float)L_8)))));
		float L_9 = V_2;
		int32_t L_10 = __this->get_mWidth_24();
		V_4 = ((float)il2cpp_codegen_add((float)L_9, (float)(((float)((float)L_10)))));
		float L_11 = V_3;
		int32_t L_12 = __this->get_mHeight_25();
		V_5 = ((float)il2cpp_codegen_add((float)L_11, (float)(((float)((float)L_12)))));
		UIPanel_t1716472341 * L_13 = __this->get_panel_36();
		NullCheck(L_13);
		Matrix4x4_t1817901843 * L_14 = L_13->get_address_of_worldToLocal_37();
		Transform_t3600365921 * L_15 = V_0;
		float L_16 = V_2;
		float L_17 = V_3;
		NullCheck(L_15);
		Vector3_t3722313464  L_18 = Transform_TransformPoint_m4024714202(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = Matrix4x4_MultiplyPoint3x4_m4145063176(L_14, L_18, /*hidden argument*/NULL);
		__this->set_mOldV0_51(L_19);
		UIPanel_t1716472341 * L_20 = __this->get_panel_36();
		NullCheck(L_20);
		Matrix4x4_t1817901843 * L_21 = L_20->get_address_of_worldToLocal_37();
		Transform_t3600365921 * L_22 = V_0;
		float L_23 = V_4;
		float L_24 = V_5;
		NullCheck(L_22);
		Vector3_t3722313464  L_25 = Transform_TransformPoint_m4024714202(L_22, L_23, L_24, (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_26 = Matrix4x4_MultiplyPoint3x4_m4145063176(L_21, L_25, /*hidden argument*/NULL);
		__this->set_mOldV1_52(L_26);
		goto IL_01bc;
	}

IL_00be:
	{
		UIPanel_t1716472341 * L_27 = __this->get_panel_36();
		NullCheck(L_27);
		bool L_28 = L_27->get_widgetsAreStatic_28();
		if (L_28)
		{
			goto IL_01bc;
		}
	}
	{
		Transform_t3600365921 * L_29 = V_0;
		NullCheck(L_29);
		bool L_30 = Transform_get_hasChanged_m186929804(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_01bc;
		}
	}
	{
		__this->set_mMatrixFrame_50((-1));
		Transform_t3600365921 * L_31 = V_0;
		NullCheck(L_31);
		Transform_set_hasChanged_m4213723989(L_31, (bool)0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_32 = UIWidget_get_pivotOffset_m1997789874(__this, /*hidden argument*/NULL);
		V_6 = L_32;
		float L_33 = (&V_6)->get_x_0();
		int32_t L_34 = __this->get_mWidth_24();
		V_7 = ((float)il2cpp_codegen_multiply((float)((-L_33)), (float)(((float)((float)L_34)))));
		float L_35 = (&V_6)->get_y_1();
		int32_t L_36 = __this->get_mHeight_25();
		V_8 = ((float)il2cpp_codegen_multiply((float)((-L_35)), (float)(((float)((float)L_36)))));
		float L_37 = V_7;
		int32_t L_38 = __this->get_mWidth_24();
		V_9 = ((float)il2cpp_codegen_add((float)L_37, (float)(((float)((float)L_38)))));
		float L_39 = V_8;
		int32_t L_40 = __this->get_mHeight_25();
		V_10 = ((float)il2cpp_codegen_add((float)L_39, (float)(((float)((float)L_40)))));
		UIPanel_t1716472341 * L_41 = __this->get_panel_36();
		NullCheck(L_41);
		Matrix4x4_t1817901843 * L_42 = L_41->get_address_of_worldToLocal_37();
		Transform_t3600365921 * L_43 = V_0;
		float L_44 = V_7;
		float L_45 = V_8;
		NullCheck(L_43);
		Vector3_t3722313464  L_46 = Transform_TransformPoint_m4024714202(L_43, L_44, L_45, (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_47 = Matrix4x4_MultiplyPoint3x4_m4145063176(L_42, L_46, /*hidden argument*/NULL);
		V_11 = L_47;
		UIPanel_t1716472341 * L_48 = __this->get_panel_36();
		NullCheck(L_48);
		Matrix4x4_t1817901843 * L_49 = L_48->get_address_of_worldToLocal_37();
		Transform_t3600365921 * L_50 = V_0;
		float L_51 = V_9;
		float L_52 = V_10;
		NullCheck(L_50);
		Vector3_t3722313464  L_53 = Transform_TransformPoint_m4024714202(L_50, L_51, L_52, (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_54 = Matrix4x4_MultiplyPoint3x4_m4145063176(L_49, L_53, /*hidden argument*/NULL);
		V_12 = L_54;
		Vector3_t3722313464  L_55 = __this->get_mOldV0_51();
		Vector3_t3722313464  L_56 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_57 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		float L_58 = Vector3_SqrMagnitude_m3025115945(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if ((((float)L_58) > ((float)(1.0E-06f))))
		{
			goto IL_01a5;
		}
	}
	{
		Vector3_t3722313464  L_59 = __this->get_mOldV1_52();
		Vector3_t3722313464  L_60 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_61 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		float L_62 = Vector3_SqrMagnitude_m3025115945(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if ((!(((float)L_62) > ((float)(1.0E-06f)))))
		{
			goto IL_01bc;
		}
	}

IL_01a5:
	{
		__this->set_mMoved_46((bool)1);
		Vector3_t3722313464  L_63 = V_11;
		__this->set_mOldV0_51(L_63);
		Vector3_t3722313464  L_64 = V_12;
		__this->set_mOldV1_52(L_64);
	}

IL_01bc:
	{
		bool L_65 = __this->get_mMoved_46();
		if (!L_65)
		{
			goto IL_01dd;
		}
	}
	{
		OnDimensionsChanged_t3101921181 * L_66 = __this->get_onChange_28();
		if (!L_66)
		{
			goto IL_01dd;
		}
	}
	{
		OnDimensionsChanged_t3101921181 * L_67 = __this->get_onChange_28();
		NullCheck(L_67);
		OnDimensionsChanged_Invoke_m1136944693(L_67, /*hidden argument*/NULL);
	}

IL_01dd:
	{
		bool L_68 = __this->get_mMoved_46();
		if (L_68)
		{
			goto IL_01f0;
		}
	}
	{
		bool L_69 = ((UIRect_t2875960382 *)__this)->get_mChanged_10();
		G_B13_0 = ((int32_t)(L_69));
		goto IL_01f1;
	}

IL_01f0:
	{
		G_B13_0 = 1;
	}

IL_01f1:
	{
		return (bool)G_B13_0;
	}
}
// System.Boolean UIWidget::UpdateGeometry(System.Int32)
extern "C"  bool UIWidget_UpdateGeometry_m145703376 (UIWidget_t3538521925 * __this, int32_t ___frame0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpdateGeometry_m145703376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		int32_t L_0 = ___frame0;
		float L_1 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, __this, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_mIsVisibleByAlpha_42();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		float L_3 = __this->get_mLastAlpha_45();
		float L_4 = V_0;
		if ((((float)L_3) == ((float)L_4)))
		{
			goto IL_0026;
		}
	}
	{
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)1);
	}

IL_0026:
	{
		float L_5 = V_0;
		__this->set_mLastAlpha_45(L_5);
		bool L_6 = ((UIRect_t2875960382 *)__this)->get_mChanged_10();
		if (!L_6)
		{
			goto IL_015c;
		}
	}
	{
		bool L_7 = __this->get_mIsVisibleByAlpha_42();
		if (!L_7)
		{
			goto IL_0121;
		}
	}
	{
		float L_8 = V_0;
		if ((!(((float)L_8) > ((float)(0.001f)))))
		{
			goto IL_0121;
		}
	}
	{
		Shader_t4151988712 * L_9 = VirtFuncInvoker0< Shader_t4151988712 * >::Invoke(29 /* UnityEngine.Shader UIWidget::get_shader() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0121;
		}
	}
	{
		UIGeometry_t1059483952 * L_11 = __this->get_geometry_37();
		NullCheck(L_11);
		bool L_12 = UIGeometry_get_hasVertices_m3715418080(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		bool L_13 = __this->get_fillGeometry_38();
		if (!L_13)
		{
			goto IL_00a8;
		}
	}
	{
		UIGeometry_t1059483952 * L_14 = __this->get_geometry_37();
		NullCheck(L_14);
		UIGeometry_Clear_m3150037314(L_14, /*hidden argument*/NULL);
		UIGeometry_t1059483952 * L_15 = __this->get_geometry_37();
		NullCheck(L_15);
		List_1_t899420910 * L_16 = L_15->get_verts_0();
		UIGeometry_t1059483952 * L_17 = __this->get_geometry_37();
		NullCheck(L_17);
		List_1_t3628304265 * L_18 = L_17->get_uvs_1();
		UIGeometry_t1059483952 * L_19 = __this->get_geometry_37();
		NullCheck(L_19);
		List_1_t4027761066 * L_20 = L_19->get_cols_2();
		VirtActionInvoker3< List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(38 /* System.Void UIWidget::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>) */, __this, L_16, L_18, L_20);
	}

IL_00a8:
	{
		UIGeometry_t1059483952 * L_21 = __this->get_geometry_37();
		NullCheck(L_21);
		bool L_22 = UIGeometry_get_hasVertices_m3715418080(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_23 = __this->get_mMatrixFrame_50();
		int32_t L_24 = ___frame0;
		if ((((int32_t)L_23) == ((int32_t)L_24)))
		{
			goto IL_00ec;
		}
	}
	{
		UIPanel_t1716472341 * L_25 = __this->get_panel_36();
		NullCheck(L_25);
		Matrix4x4_t1817901843  L_26 = L_25->get_worldToLocal_37();
		Transform_t3600365921 * L_27 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Matrix4x4_t1817901843  L_28 = Transform_get_localToWorldMatrix_m4155710351(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_29 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_41(L_29);
		int32_t L_30 = ___frame0;
		__this->set_mMatrixFrame_50(L_30);
	}

IL_00ec:
	{
		UIGeometry_t1059483952 * L_31 = __this->get_geometry_37();
		Matrix4x4_t1817901843  L_32 = __this->get_mLocalToPanel_41();
		UIPanel_t1716472341 * L_33 = __this->get_panel_36();
		NullCheck(L_33);
		bool L_34 = L_33->get_generateNormals_25();
		NullCheck(L_31);
		UIGeometry_ApplyTransform_m8241091(L_31, L_32, L_34, /*hidden argument*/NULL);
		__this->set_mMoved_46((bool)0);
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_0118:
	{
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)0);
		bool L_35 = V_1;
		return L_35;
	}

IL_0121:
	{
		UIGeometry_t1059483952 * L_36 = __this->get_geometry_37();
		NullCheck(L_36);
		bool L_37 = UIGeometry_get_hasVertices_m3715418080(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0157;
		}
	}
	{
		bool L_38 = __this->get_fillGeometry_38();
		if (!L_38)
		{
			goto IL_0147;
		}
	}
	{
		UIGeometry_t1059483952 * L_39 = __this->get_geometry_37();
		NullCheck(L_39);
		UIGeometry_Clear_m3150037314(L_39, /*hidden argument*/NULL);
	}

IL_0147:
	{
		__this->set_mMoved_46((bool)0);
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_0157:
	{
		goto IL_01d7;
	}

IL_015c:
	{
		bool L_40 = __this->get_mMoved_46();
		if (!L_40)
		{
			goto IL_01d7;
		}
	}
	{
		UIGeometry_t1059483952 * L_41 = __this->get_geometry_37();
		NullCheck(L_41);
		bool L_42 = UIGeometry_get_hasVertices_m3715418080(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01d7;
		}
	}
	{
		int32_t L_43 = __this->get_mMatrixFrame_50();
		int32_t L_44 = ___frame0;
		if ((((int32_t)L_43) == ((int32_t)L_44)))
		{
			goto IL_01ab;
		}
	}
	{
		UIPanel_t1716472341 * L_45 = __this->get_panel_36();
		NullCheck(L_45);
		Matrix4x4_t1817901843  L_46 = L_45->get_worldToLocal_37();
		Transform_t3600365921 * L_47 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Matrix4x4_t1817901843  L_48 = Transform_get_localToWorldMatrix_m4155710351(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_49 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_41(L_49);
		int32_t L_50 = ___frame0;
		__this->set_mMatrixFrame_50(L_50);
	}

IL_01ab:
	{
		UIGeometry_t1059483952 * L_51 = __this->get_geometry_37();
		Matrix4x4_t1817901843  L_52 = __this->get_mLocalToPanel_41();
		UIPanel_t1716472341 * L_53 = __this->get_panel_36();
		NullCheck(L_53);
		bool L_54 = L_53->get_generateNormals_25();
		NullCheck(L_51);
		UIGeometry_ApplyTransform_m8241091(L_51, L_52, L_54, /*hidden argument*/NULL);
		__this->set_mMoved_46((bool)0);
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_01d7:
	{
		__this->set_mMoved_46((bool)0);
		((UIRect_t2875960382 *)__this)->set_mChanged_10((bool)0);
		return (bool)0;
	}
}
// System.Void UIWidget::WriteToBuffers(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void UIWidget_WriteToBuffers_m4241387002 (UIWidget_t3538521925 * __this, List_1_t899420910 * ___v0, List_1_t3628304265 * ___u1, List_1_t4027761066 * ___c2, List_1_t899420910 * ___n3, List_1_t496136383 * ___t4, List_1_t496136383 * ___u25, const RuntimeMethod* method)
{
	{
		UIGeometry_t1059483952 * L_0 = __this->get_geometry_37();
		List_1_t899420910 * L_1 = ___v0;
		List_1_t3628304265 * L_2 = ___u1;
		List_1_t4027761066 * L_3 = ___c2;
		List_1_t899420910 * L_4 = ___n3;
		List_1_t496136383 * L_5 = ___t4;
		List_1_t496136383 * L_6 = ___u25;
		NullCheck(L_0);
		UIGeometry_WriteToBuffers_m139827744(L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::MakePixelPerfect()
extern "C"  void UIWidget_MakePixelPerfect_m194260931 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MakePixelPerfect_m194260931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3600365921 * L_0 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localPosition_m4234289348(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(L_2);
		(&V_0)->set_z_3(L_3);
		float L_4 = (&V_0)->get_x_1();
		float L_5 = bankers_roundf(L_4);
		(&V_0)->set_x_1(L_5);
		float L_6 = (&V_0)->get_y_2();
		float L_7 = bankers_roundf(L_6);
		(&V_0)->set_y_2(L_7);
		Transform_t3600365921 * L_8 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_localPosition_m4128471975(L_8, L_9, /*hidden argument*/NULL);
		Transform_t3600365921 * L_10 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = Transform_get_localScale_m129152068(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		Transform_t3600365921 * L_12 = UIRect_get_cachedTransform_m3314958558(__this, /*hidden argument*/NULL);
		float L_13 = (&V_1)->get_x_1();
		float L_14 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = (&V_1)->get_y_2();
		float L_16 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m3353183577((&L_17), L_14, L_16, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_m3053443106(L_12, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UIWidget::get_minWidth()
extern "C"  int32_t UIWidget_get_minWidth_m1770693470 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		return 2;
	}
}
// System.Int32 UIWidget::get_minHeight()
extern "C"  int32_t UIWidget_get_minHeight_m379412401 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	{
		return 2;
	}
}
// UnityEngine.Vector4 UIWidget::get_border()
extern "C"  Vector4_t3319028937  UIWidget_get_border_m1908907599 (UIWidget_t3538521925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_border_m1908907599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_0 = Vector4_get_zero_m1422399515(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UIWidget::set_border(UnityEngine.Vector4)
extern "C"  void UIWidget_set_border_m2107491430 (UIWidget_t3538521925 * __this, Vector4_t3319028937  ___value0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UIWidget::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern "C"  void UIWidget_OnFill_m4250209722 (UIWidget_t3538521925 * __this, List_1_t899420910 * ___verts0, List_1_t3628304265 * ___uvs1, List_1_t4027761066 * ___cols2, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  bool DelegatePInvokeWrapper_HitCheck_t2300079615 (HitCheck_t2300079615 * __this, Vector3_t3722313464  ___worldPos0, const RuntimeMethod* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(Vector3_t3722313464 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___worldPos0);

	return static_cast<bool>(returnValue);
}
// System.Void UIWidget/HitCheck::.ctor(System.Object,System.IntPtr)
extern "C"  void HitCheck__ctor_m1061316756 (HitCheck_t2300079615 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UIWidget/HitCheck::Invoke(UnityEngine.Vector3)
extern "C"  bool HitCheck_Invoke_m902107116 (HitCheck_t2300079615 * __this, Vector3_t3722313464  ___worldPos0, const RuntimeMethod* method)
{
	bool result = false;
	if(__this->get_prev_9() != NULL)
	{
		HitCheck_Invoke_m902107116((HitCheck_t2300079615 *)__this->get_prev_9(), ___worldPos0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef bool (*FunctionPointerType) (RuntimeObject *, Vector3_t3722313464 , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(NULL, ___worldPos0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef bool (*FunctionPointerType) (RuntimeObject *, void*, Vector3_t3722313464 , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___worldPos0, targetMethod);
			}
		}
	}
	else
	{
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< bool, Vector3_t3722313464  >::Invoke(targetMethod, targetThis, ___worldPos0);
					else
						result = GenericVirtFuncInvoker1< bool, Vector3_t3722313464  >::Invoke(targetMethod, targetThis, ___worldPos0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< bool, Vector3_t3722313464  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___worldPos0);
					else
						result = VirtFuncInvoker1< bool, Vector3_t3722313464  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___worldPos0);
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (void*, Vector3_t3722313464 , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___worldPos0, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult UIWidget/HitCheck::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* HitCheck_BeginInvoke_m1081756051 (HitCheck_t2300079615 * __this, Vector3_t3722313464  ___worldPos0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitCheck_BeginInvoke_m1081756051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &___worldPos0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean UIWidget/HitCheck::EndInvoke(System.IAsyncResult)
extern "C"  bool HitCheck_EndInvoke_m441931996 (HitCheck_t2300079615 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_OnDimensionsChanged_t3101921181 (OnDimensionsChanged_t3101921181 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UIWidget/OnDimensionsChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDimensionsChanged__ctor_m413197107 (OnDimensionsChanged_t3101921181 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnDimensionsChanged::Invoke()
extern "C"  void OnDimensionsChanged_Invoke_m1136944693 (OnDimensionsChanged_t3101921181 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDimensionsChanged_Invoke_m1136944693((OnDimensionsChanged_t3101921181 *)__this->get_prev_9(), method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
			}
		}
	}
	else
	{
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult UIWidget/OnDimensionsChanged::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnDimensionsChanged_BeginInvoke_m4061805870 (OnDimensionsChanged_t3101921181 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UIWidget/OnDimensionsChanged::EndInvoke(System.IAsyncResult)
extern "C"  void OnDimensionsChanged_EndInvoke_m3682528395 (OnDimensionsChanged_t3101921181 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIWidget/OnPostFillCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPostFillCallback__ctor_m2271775146 (OnPostFillCallback_t2835645043 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnPostFillCallback::Invoke(UIWidget,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern "C"  void OnPostFillCallback_Invoke_m394319234 (OnPostFillCallback_t2835645043 * __this, UIWidget_t3538521925 * ___widget0, int32_t ___bufferOffset1, List_1_t899420910 * ___verts2, List_1_t3628304265 * ___uvs3, List_1_t4027761066 * ___cols4, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnPostFillCallback_Invoke_m394319234((OnPostFillCallback_t2835645043 *)__this->get_prev_9(), ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 5)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 5)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker5< UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(targetMethod, targetThis, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
					else
						GenericVirtActionInvoker5< UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(targetMethod, targetThis, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker5< UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
					else
						VirtActionInvoker5< UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker4< int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(targetMethod, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
					else
						GenericVirtActionInvoker4< int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(targetMethod, ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker4< int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
					else
						VirtActionInvoker4< int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (UIWidget_t3538521925 *, int32_t, List_1_t899420910 *, List_1_t3628304265 *, List_1_t4027761066 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, targetMethod);
			}
		}
	}
}
// System.IAsyncResult UIWidget/OnPostFillCallback::BeginInvoke(UIWidget,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnPostFillCallback_BeginInvoke_m1173000092 (OnPostFillCallback_t2835645043 * __this, UIWidget_t3538521925 * ___widget0, int32_t ___bufferOffset1, List_1_t899420910 * ___verts2, List_1_t3628304265 * ___uvs3, List_1_t4027761066 * ___cols4, AsyncCallback_t3962456242 * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPostFillCallback_BeginInvoke_m1173000092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = ___widget0;
	__d_args[1] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___bufferOffset1);
	__d_args[2] = ___verts2;
	__d_args[3] = ___uvs3;
	__d_args[4] = ___cols4;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// System.Void UIWidget/OnPostFillCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnPostFillCallback_EndInvoke_m2346222365 (OnPostFillCallback_t2835645043 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIWidgetContainer::.ctor()
extern "C"  void UIWidgetContainer__ctor_m155952688 (UIWidgetContainer_t30162560 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIWrapContent::.ctor()
extern "C"  void UIWrapContent__ctor_m3120265429 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent__ctor_m3120265429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_itemSize_2(((int32_t)100));
		__this->set_cullContent_3((bool)1);
		__this->set_mFirstTime_12((bool)1);
		List_1_t777473367 * L_0 = (List_1_t777473367 *)il2cpp_codegen_object_new(List_1_t777473367_il2cpp_TypeInfo_var);
		List_1__ctor_m2885667311(L_0, /*hidden argument*/List_1__ctor_m2885667311_RuntimeMethod_var);
		__this->set_mChildren_13(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::Start()
extern "C"  void UIWrapContent_Start_m878423027 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_Start_m878423027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void UIWrapContent::SortBasedOnScrollMovement() */, __this);
		VirtActionInvoker0::Invoke(9 /* System.Void UIWrapContent::WrapContent() */, __this);
		UIScrollView_t1973404950 * L_0 = __this->get_mScroll_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		UIScrollView_t1973404950 * L_2 = __this->get_mScroll_10();
		NullCheck(L_2);
		UIPanel_t1716472341 * L_3 = Component_GetComponent_TisUIPanel_t1716472341_m3980802358(L_2, /*hidden argument*/Component_GetComponent_TisUIPanel_t1716472341_m3980802358_RuntimeMethod_var);
		intptr_t L_4 = (intptr_t)GetVirtualMethodInfo(__this, 5);
		OnClippingMoved_t476625095 * L_5 = (OnClippingMoved_t476625095 *)il2cpp_codegen_object_new(OnClippingMoved_t476625095_il2cpp_TypeInfo_var);
		OnClippingMoved__ctor_m1106797729(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_onClipMove_39(L_5);
	}

IL_003a:
	{
		__this->set_mFirstTime_12((bool)0);
		return;
	}
}
// System.Void UIWrapContent::OnMove(UIPanel)
extern "C"  void UIWrapContent_OnMove_m1208103735 (UIWrapContent_t1188558554 * __this, UIPanel_t1716472341 * ___panel0, const RuntimeMethod* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void UIWrapContent::WrapContent() */, __this);
		return;
	}
}
// System.Void UIWrapContent::SortBasedOnScrollMovement()
extern "C"  void UIWrapContent_SortBasedOnScrollMovement_m1287634130 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortBasedOnScrollMovement_m1287634130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3600365921 * V_1 = NULL;
	List_1_t777473367 * G_B12_0 = NULL;
	List_1_t777473367 * G_B11_0 = NULL;
	List_1_t777473367 * G_B15_0 = NULL;
	List_1_t777473367 * G_B14_0 = NULL;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m1573533213(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t777473367 * L_1 = __this->get_mChildren_13();
		NullCheck(L_1);
		List_1_Clear_m3082658015(L_1, /*hidden argument*/List_1_Clear_m3082658015_RuntimeMethod_var);
		V_0 = 0;
		goto IL_005b;
	}

IL_001e:
	{
		Transform_t3600365921 * L_2 = __this->get_mTrans_8();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t3600365921 * L_4 = Transform_GetChild_m1092972975(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = __this->get_hideInactive_6();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Transform_t3600365921 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = GameObject_get_activeInHierarchy_m2006396688(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		goto IL_0057;
	}

IL_004b:
	{
		List_1_t777473367 * L_9 = __this->get_mChildren_13();
		Transform_t3600365921 * L_10 = V_1;
		NullCheck(L_9);
		List_1_Add_m4073477735(L_9, L_10, /*hidden argument*/List_1_Add_m4073477735_RuntimeMethod_var);
	}

IL_0057:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005b:
	{
		int32_t L_12 = V_0;
		Transform_t3600365921 * L_13 = __this->get_mTrans_8();
		NullCheck(L_13);
		int32_t L_14 = Transform_get_childCount_m3145433196(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_001e;
		}
	}
	{
		bool L_15 = __this->get_mHorizontal_11();
		if (!L_15)
		{
			goto IL_00a4;
		}
	}
	{
		List_1_t777473367 * L_16 = __this->get_mChildren_13();
		Comparison_1_t3375297100 * L_17 = ((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_14();
		G_B11_0 = L_16;
		if (L_17)
		{
			G_B12_0 = L_16;
			goto IL_0095;
		}
	}
	{
		intptr_t L_18 = (intptr_t)UIGrid_SortHorizontal_m510606556_RuntimeMethod_var;
		Comparison_1_t3375297100 * L_19 = (Comparison_1_t3375297100 *)il2cpp_codegen_object_new(Comparison_1_t3375297100_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m2800470641(L_19, NULL, L_18, /*hidden argument*/Comparison_1__ctor_m2800470641_RuntimeMethod_var);
		((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_14(L_19);
		G_B12_0 = G_B11_0;
	}

IL_0095:
	{
		Comparison_1_t3375297100 * L_20 = ((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_14();
		NullCheck(G_B12_0);
		List_1_Sort_m620079343(G_B12_0, L_20, /*hidden argument*/List_1_Sort_m620079343_RuntimeMethod_var);
		goto IL_00cc;
	}

IL_00a4:
	{
		List_1_t777473367 * L_21 = __this->get_mChildren_13();
		Comparison_1_t3375297100 * L_22 = ((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_15();
		G_B14_0 = L_21;
		if (L_22)
		{
			G_B15_0 = L_21;
			goto IL_00c2;
		}
	}
	{
		intptr_t L_23 = (intptr_t)UIGrid_SortVertical_m1233841256_RuntimeMethod_var;
		Comparison_1_t3375297100 * L_24 = (Comparison_1_t3375297100 *)il2cpp_codegen_object_new(Comparison_1_t3375297100_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m2800470641(L_24, NULL, L_23, /*hidden argument*/Comparison_1__ctor_m2800470641_RuntimeMethod_var);
		((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_15(L_24);
		G_B15_0 = G_B14_0;
	}

IL_00c2:
	{
		Comparison_1_t3375297100 * L_25 = ((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_15();
		NullCheck(G_B15_0);
		List_1_Sort_m620079343(G_B15_0, L_25, /*hidden argument*/List_1_Sort_m620079343_RuntimeMethod_var);
	}

IL_00cc:
	{
		VirtActionInvoker0::Invoke(8 /* System.Void UIWrapContent::ResetChildPositions() */, __this);
		return;
	}
}
// System.Void UIWrapContent::SortAlphabetically()
extern "C"  void UIWrapContent_SortAlphabetically_m2142146348 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortAlphabetically_m2142146348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3600365921 * V_1 = NULL;
	List_1_t777473367 * G_B11_0 = NULL;
	List_1_t777473367 * G_B10_0 = NULL;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m1573533213(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t777473367 * L_1 = __this->get_mChildren_13();
		NullCheck(L_1);
		List_1_Clear_m3082658015(L_1, /*hidden argument*/List_1_Clear_m3082658015_RuntimeMethod_var);
		V_0 = 0;
		goto IL_005b;
	}

IL_001e:
	{
		Transform_t3600365921 * L_2 = __this->get_mTrans_8();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t3600365921 * L_4 = Transform_GetChild_m1092972975(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = __this->get_hideInactive_6();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Transform_t3600365921 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = GameObject_get_activeInHierarchy_m2006396688(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		goto IL_0057;
	}

IL_004b:
	{
		List_1_t777473367 * L_9 = __this->get_mChildren_13();
		Transform_t3600365921 * L_10 = V_1;
		NullCheck(L_9);
		List_1_Add_m4073477735(L_9, L_10, /*hidden argument*/List_1_Add_m4073477735_RuntimeMethod_var);
	}

IL_0057:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_005b:
	{
		int32_t L_12 = V_0;
		Transform_t3600365921 * L_13 = __this->get_mTrans_8();
		NullCheck(L_13);
		int32_t L_14 = Transform_get_childCount_m3145433196(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t777473367 * L_15 = __this->get_mChildren_13();
		Comparison_1_t3375297100 * L_16 = ((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_16();
		G_B10_0 = L_15;
		if (L_16)
		{
			G_B11_0 = L_15;
			goto IL_008a;
		}
	}
	{
		intptr_t L_17 = (intptr_t)UIGrid_SortByName_m2291677436_RuntimeMethod_var;
		Comparison_1_t3375297100 * L_18 = (Comparison_1_t3375297100 *)il2cpp_codegen_object_new(Comparison_1_t3375297100_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m2800470641(L_18, NULL, L_17, /*hidden argument*/Comparison_1__ctor_m2800470641_RuntimeMethod_var);
		((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache2_16(L_18);
		G_B11_0 = G_B10_0;
	}

IL_008a:
	{
		Comparison_1_t3375297100 * L_19 = ((UIWrapContent_t1188558554_StaticFields*)il2cpp_codegen_static_fields_for(UIWrapContent_t1188558554_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_16();
		NullCheck(G_B11_0);
		List_1_Sort_m620079343(G_B11_0, L_19, /*hidden argument*/List_1_Sort_m620079343_RuntimeMethod_var);
		VirtActionInvoker0::Invoke(8 /* System.Void UIWrapContent::ResetChildPositions() */, __this);
		return;
	}
}
// System.Boolean UIWrapContent::CacheScrollView()
extern "C"  bool UIWrapContent_CacheScrollView_m1573533213 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_CacheScrollView_m1573533213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_8(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		UIPanel_t1716472341 * L_2 = NGUITools_FindInParents_TisUIPanel_t1716472341_m1961265283(NULL /*static, unused*/, L_1, /*hidden argument*/NGUITools_FindInParents_TisUIPanel_t1716472341_m1961265283_RuntimeMethod_var);
		__this->set_mPanel_9(L_2);
		UIPanel_t1716472341 * L_3 = __this->get_mPanel_9();
		NullCheck(L_3);
		UIScrollView_t1973404950 * L_4 = Component_GetComponent_TisUIScrollView_t1973404950_m746780642(L_3, /*hidden argument*/Component_GetComponent_TisUIScrollView_t1973404950_m746780642_RuntimeMethod_var);
		__this->set_mScroll_10(L_4);
		UIScrollView_t1973404950 * L_5 = __this->get_mScroll_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		return (bool)0;
	}

IL_0041:
	{
		UIScrollView_t1973404950 * L_7 = __this->get_mScroll_10();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_movement_3();
		if (L_8)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_mHorizontal_11((bool)1);
		goto IL_007c;
	}

IL_005d:
	{
		UIScrollView_t1973404950 * L_9 = __this->get_mScroll_10();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_movement_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		__this->set_mHorizontal_11((bool)0);
		goto IL_007c;
	}

IL_007a:
	{
		return (bool)0;
	}

IL_007c:
	{
		return (bool)1;
	}
}
// System.Void UIWrapContent::ResetChildPositions()
extern "C"  void UIWrapContent_ResetChildPositions_m832715332 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_ResetChildPositions_m832715332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Transform_t3600365921 * V_2 = NULL;
	Transform_t3600365921 * G_B3_0 = NULL;
	Transform_t3600365921 * G_B2_0 = NULL;
	Vector3_t3722313464  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	Transform_t3600365921 * G_B4_1 = NULL;
	{
		V_0 = 0;
		List_1_t777473367 * L_0 = __this->get_mChildren_13();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3787308655(L_0, /*hidden argument*/List_1_get_Count_m3787308655_RuntimeMethod_var);
		V_1 = L_1;
		goto IL_0073;
	}

IL_0013:
	{
		List_1_t777473367 * L_2 = __this->get_mChildren_13();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t3600365921 * L_4 = List_1_get_Item_m3022113929(L_2, L_3, /*hidden argument*/List_1_get_Item_m3022113929_RuntimeMethod_var);
		V_2 = L_4;
		Transform_t3600365921 * L_5 = V_2;
		bool L_6 = __this->get_mHorizontal_11();
		G_B2_0 = L_5;
		if (!L_6)
		{
			G_B3_0 = L_5;
			goto IL_0049;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_itemSize_2();
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3353183577((&L_9), (((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)L_7, (int32_t)L_8))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_9;
		G_B4_1 = G_B2_0;
		goto IL_0062;
	}

IL_0049:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = __this->get_itemSize_2();
		Vector3_t3722313464  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m3353183577((&L_12), (0.0f), (((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)((-L_10)), (int32_t)L_11))))), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0062:
	{
		NullCheck(G_B4_1);
		Transform_set_localPosition_m4128471975(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = V_2;
		int32_t L_14 = V_0;
		VirtActionInvoker2< Transform_t3600365921 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_13, L_14);
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0073:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UIWrapContent::WrapContent()
extern "C"  void UIWrapContent_WrapContent_m967226172 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_WrapContent_m967226172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3U5BU5D_t1718750761* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	bool V_5 = false;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Transform_t3600365921 * V_11 = NULL;
	float V_12 = 0.0f;
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t3722313464  V_14;
	memset(&V_14, 0, sizeof(V_14));
	int32_t V_15 = 0;
	Vector3_t3722313464  V_16;
	memset(&V_16, 0, sizeof(V_16));
	int32_t V_17 = 0;
	Vector2_t2156229523  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t3722313464  V_19;
	memset(&V_19, 0, sizeof(V_19));
	float V_20 = 0.0f;
	float V_21 = 0.0f;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	Transform_t3600365921 * V_24 = NULL;
	float V_25 = 0.0f;
	Vector3_t3722313464  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t3722313464  V_27;
	memset(&V_27, 0, sizeof(V_27));
	int32_t V_28 = 0;
	Vector3_t3722313464  V_29;
	memset(&V_29, 0, sizeof(V_29));
	int32_t V_30 = 0;
	Vector2_t2156229523  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t3722313464  V_32;
	memset(&V_32, 0, sizeof(V_32));
	GameObject_t1113636619 * G_B25_0 = NULL;
	GameObject_t1113636619 * G_B24_0 = NULL;
	int32_t G_B26_0 = 0;
	GameObject_t1113636619 * G_B26_1 = NULL;
	GameObject_t1113636619 * G_B51_0 = NULL;
	GameObject_t1113636619 * G_B50_0 = NULL;
	int32_t G_B52_0 = 0;
	GameObject_t1113636619 * G_B52_1 = NULL;
	{
		int32_t L_0 = __this->get_itemSize_2();
		List_1_t777473367 * L_1 = __this->get_mChildren_13();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m3787308655(L_1, /*hidden argument*/List_1_get_Count_m3787308655_RuntimeMethod_var);
		V_0 = ((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)L_2))))), (float)(0.5f)));
		UIPanel_t1716472341 * L_3 = __this->get_mPanel_9();
		NullCheck(L_3);
		Vector3U5BU5D_t1718750761* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, L_3);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0058;
	}

IL_002d:
	{
		Vector3U5BU5D_t1718750761* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		V_3 = (*(Vector3_t3722313464 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		Transform_t3600365921 * L_7 = __this->get_mTrans_8();
		Vector3_t3722313464  L_8 = V_3;
		NullCheck(L_7);
		Vector3_t3722313464  L_9 = Transform_InverseTransformPoint_m1343916000(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3U5BU5D_t1718750761* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		Vector3_t3722313464  L_12 = V_3;
		*(Vector3_t3722313464 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11))) = L_12;
		int32_t L_13 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0058:
	{
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) < ((int32_t)4)))
		{
			goto IL_002d;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_15 = V_1;
		NullCheck(L_15);
		Vector3U5BU5D_t1718750761* L_16 = V_1;
		NullCheck(L_16);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_17 = Vector3_Lerp_m407887542(NULL /*static, unused*/, (*(Vector3_t3722313464 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t3722313464 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		V_4 = L_17;
		V_5 = (bool)1;
		float L_18 = V_0;
		V_6 = ((float)il2cpp_codegen_multiply((float)L_18, (float)(2.0f)));
		bool L_19 = __this->get_mHorizontal_11();
		if (!L_19)
		{
			goto IL_02bd;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_20 = V_1;
		NullCheck(L_20);
		float L_21 = ((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		int32_t L_22 = __this->get_itemSize_2();
		V_7 = ((float)il2cpp_codegen_subtract((float)L_21, (float)(((float)((float)L_22)))));
		Vector3U5BU5D_t1718750761* L_23 = V_1;
		NullCheck(L_23);
		float L_24 = ((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		int32_t L_25 = __this->get_itemSize_2();
		V_8 = ((float)il2cpp_codegen_add((float)L_24, (float)(((float)((float)L_25)))));
		V_9 = 0;
		List_1_t777473367 * L_26 = __this->get_mChildren_13();
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m3787308655(L_26, /*hidden argument*/List_1_get_Count_m3787308655_RuntimeMethod_var);
		V_10 = L_27;
		goto IL_02af;
	}

IL_00db:
	{
		List_1_t777473367 * L_28 = __this->get_mChildren_13();
		int32_t L_29 = V_9;
		NullCheck(L_28);
		Transform_t3600365921 * L_30 = List_1_get_Item_m3022113929(L_28, L_29, /*hidden argument*/List_1_get_Item_m3022113929_RuntimeMethod_var);
		V_11 = L_30;
		Transform_t3600365921 * L_31 = V_11;
		NullCheck(L_31);
		Vector3_t3722313464  L_32 = Transform_get_localPosition_m4234289348(L_31, /*hidden argument*/NULL);
		V_13 = L_32;
		float L_33 = (&V_13)->get_x_1();
		float L_34 = (&V_4)->get_x_1();
		V_12 = ((float)il2cpp_codegen_subtract((float)L_33, (float)L_34));
		float L_35 = V_12;
		float L_36 = V_0;
		if ((!(((float)L_35) < ((float)((-L_36))))))
		{
			goto IL_0198;
		}
	}
	{
		Transform_t3600365921 * L_37 = V_11;
		NullCheck(L_37);
		Vector3_t3722313464  L_38 = Transform_get_localPosition_m4234289348(L_37, /*hidden argument*/NULL);
		V_14 = L_38;
		Vector3_t3722313464 * L_39 = (&V_14);
		float L_40 = L_39->get_x_1();
		float L_41 = V_6;
		L_39->set_x_1(((float)il2cpp_codegen_add((float)L_40, (float)L_41)));
		float L_42 = (&V_14)->get_x_1();
		float L_43 = (&V_4)->get_x_1();
		V_12 = ((float)il2cpp_codegen_subtract((float)L_42, (float)L_43));
		float L_44 = (&V_14)->get_x_1();
		int32_t L_45 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_46 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_44/(float)(((float)((float)L_45))))), /*hidden argument*/NULL);
		V_15 = L_46;
		int32_t L_47 = __this->get_minIndex_4();
		int32_t L_48 = __this->get_maxIndex_5();
		if ((((int32_t)L_47) == ((int32_t)L_48)))
		{
			goto IL_0178;
		}
	}
	{
		int32_t L_49 = __this->get_minIndex_4();
		int32_t L_50 = V_15;
		if ((((int32_t)L_49) > ((int32_t)L_50)))
		{
			goto IL_0190;
		}
	}
	{
		int32_t L_51 = V_15;
		int32_t L_52 = __this->get_maxIndex_5();
		if ((((int32_t)L_51) > ((int32_t)L_52)))
		{
			goto IL_0190;
		}
	}

IL_0178:
	{
		Transform_t3600365921 * L_53 = V_11;
		Vector3_t3722313464  L_54 = V_14;
		NullCheck(L_53);
		Transform_set_localPosition_m4128471975(L_53, L_54, /*hidden argument*/NULL);
		Transform_t3600365921 * L_55 = V_11;
		int32_t L_56 = V_9;
		VirtActionInvoker2< Transform_t3600365921 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_55, L_56);
		goto IL_0193;
	}

IL_0190:
	{
		V_5 = (bool)0;
	}

IL_0193:
	{
		goto IL_0240;
	}

IL_0198:
	{
		float L_57 = V_12;
		float L_58 = V_0;
		if ((!(((float)L_57) > ((float)L_58))))
		{
			goto IL_022b;
		}
	}
	{
		Transform_t3600365921 * L_59 = V_11;
		NullCheck(L_59);
		Vector3_t3722313464  L_60 = Transform_get_localPosition_m4234289348(L_59, /*hidden argument*/NULL);
		V_16 = L_60;
		Vector3_t3722313464 * L_61 = (&V_16);
		float L_62 = L_61->get_x_1();
		float L_63 = V_6;
		L_61->set_x_1(((float)il2cpp_codegen_subtract((float)L_62, (float)L_63)));
		float L_64 = (&V_16)->get_x_1();
		float L_65 = (&V_4)->get_x_1();
		V_12 = ((float)il2cpp_codegen_subtract((float)L_64, (float)L_65));
		float L_66 = (&V_16)->get_x_1();
		int32_t L_67 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_68 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_66/(float)(((float)((float)L_67))))), /*hidden argument*/NULL);
		V_17 = L_68;
		int32_t L_69 = __this->get_minIndex_4();
		int32_t L_70 = __this->get_maxIndex_5();
		if ((((int32_t)L_69) == ((int32_t)L_70)))
		{
			goto IL_020b;
		}
	}
	{
		int32_t L_71 = __this->get_minIndex_4();
		int32_t L_72 = V_17;
		if ((((int32_t)L_71) > ((int32_t)L_72)))
		{
			goto IL_0223;
		}
	}
	{
		int32_t L_73 = V_17;
		int32_t L_74 = __this->get_maxIndex_5();
		if ((((int32_t)L_73) > ((int32_t)L_74)))
		{
			goto IL_0223;
		}
	}

IL_020b:
	{
		Transform_t3600365921 * L_75 = V_11;
		Vector3_t3722313464  L_76 = V_16;
		NullCheck(L_75);
		Transform_set_localPosition_m4128471975(L_75, L_76, /*hidden argument*/NULL);
		Transform_t3600365921 * L_77 = V_11;
		int32_t L_78 = V_9;
		VirtActionInvoker2< Transform_t3600365921 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_77, L_78);
		goto IL_0226;
	}

IL_0223:
	{
		V_5 = (bool)0;
	}

IL_0226:
	{
		goto IL_0240;
	}

IL_022b:
	{
		bool L_79 = __this->get_mFirstTime_12();
		if (!L_79)
		{
			goto IL_0240;
		}
	}
	{
		Transform_t3600365921 * L_80 = V_11;
		int32_t L_81 = V_9;
		VirtActionInvoker2< Transform_t3600365921 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_80, L_81);
	}

IL_0240:
	{
		bool L_82 = __this->get_cullContent_3();
		if (!L_82)
		{
			goto IL_02a9;
		}
	}
	{
		float L_83 = V_12;
		UIPanel_t1716472341 * L_84 = __this->get_mPanel_9();
		NullCheck(L_84);
		Vector2_t2156229523  L_85 = UIPanel_get_clipOffset_m110674167(L_84, /*hidden argument*/NULL);
		V_18 = L_85;
		float L_86 = (&V_18)->get_x_0();
		Transform_t3600365921 * L_87 = __this->get_mTrans_8();
		NullCheck(L_87);
		Vector3_t3722313464  L_88 = Transform_get_localPosition_m4234289348(L_87, /*hidden argument*/NULL);
		V_19 = L_88;
		float L_89 = (&V_19)->get_x_1();
		V_12 = ((float)il2cpp_codegen_add((float)L_83, (float)((float)il2cpp_codegen_subtract((float)L_86, (float)L_89))));
		Transform_t3600365921 * L_90 = V_11;
		NullCheck(L_90);
		GameObject_t1113636619 * L_91 = Component_get_gameObject_m442555142(L_90, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t1356438871_il2cpp_TypeInfo_var);
		bool L_92 = UICamera_IsPressed_m2472947029(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		if (L_92)
		{
			goto IL_02a9;
		}
	}
	{
		Transform_t3600365921 * L_93 = V_11;
		NullCheck(L_93);
		GameObject_t1113636619 * L_94 = Component_get_gameObject_m442555142(L_93, /*hidden argument*/NULL);
		float L_95 = V_12;
		float L_96 = V_7;
		G_B24_0 = L_94;
		if ((!(((float)L_95) > ((float)L_96))))
		{
			G_B25_0 = L_94;
			goto IL_02a2;
		}
	}
	{
		float L_97 = V_12;
		float L_98 = V_8;
		G_B26_0 = ((((float)L_97) < ((float)L_98))? 1 : 0);
		G_B26_1 = G_B24_0;
		goto IL_02a3;
	}

IL_02a2:
	{
		G_B26_0 = 0;
		G_B26_1 = G_B25_0;
	}

IL_02a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m1909334044(NULL /*static, unused*/, G_B26_1, (bool)G_B26_0, (bool)0, /*hidden argument*/NULL);
	}

IL_02a9:
	{
		int32_t L_99 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_99, (int32_t)1));
	}

IL_02af:
	{
		int32_t L_100 = V_9;
		int32_t L_101 = V_10;
		if ((((int32_t)L_100) < ((int32_t)L_101)))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_04db;
	}

IL_02bd:
	{
		Vector3U5BU5D_t1718750761* L_102 = V_1;
		NullCheck(L_102);
		float L_103 = ((L_102)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_y_2();
		int32_t L_104 = __this->get_itemSize_2();
		V_20 = ((float)il2cpp_codegen_subtract((float)L_103, (float)(((float)((float)L_104)))));
		Vector3U5BU5D_t1718750761* L_105 = V_1;
		NullCheck(L_105);
		float L_106 = ((L_105)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_y_2();
		int32_t L_107 = __this->get_itemSize_2();
		V_21 = ((float)il2cpp_codegen_add((float)L_106, (float)(((float)((float)L_107)))));
		V_22 = 0;
		List_1_t777473367 * L_108 = __this->get_mChildren_13();
		NullCheck(L_108);
		int32_t L_109 = List_1_get_Count_m3787308655(L_108, /*hidden argument*/List_1_get_Count_m3787308655_RuntimeMethod_var);
		V_23 = L_109;
		goto IL_04d2;
	}

IL_02fe:
	{
		List_1_t777473367 * L_110 = __this->get_mChildren_13();
		int32_t L_111 = V_22;
		NullCheck(L_110);
		Transform_t3600365921 * L_112 = List_1_get_Item_m3022113929(L_110, L_111, /*hidden argument*/List_1_get_Item_m3022113929_RuntimeMethod_var);
		V_24 = L_112;
		Transform_t3600365921 * L_113 = V_24;
		NullCheck(L_113);
		Vector3_t3722313464  L_114 = Transform_get_localPosition_m4234289348(L_113, /*hidden argument*/NULL);
		V_26 = L_114;
		float L_115 = (&V_26)->get_y_2();
		float L_116 = (&V_4)->get_y_2();
		V_25 = ((float)il2cpp_codegen_subtract((float)L_115, (float)L_116));
		float L_117 = V_25;
		float L_118 = V_0;
		if ((!(((float)L_117) < ((float)((-L_118))))))
		{
			goto IL_03bb;
		}
	}
	{
		Transform_t3600365921 * L_119 = V_24;
		NullCheck(L_119);
		Vector3_t3722313464  L_120 = Transform_get_localPosition_m4234289348(L_119, /*hidden argument*/NULL);
		V_27 = L_120;
		Vector3_t3722313464 * L_121 = (&V_27);
		float L_122 = L_121->get_y_2();
		float L_123 = V_6;
		L_121->set_y_2(((float)il2cpp_codegen_add((float)L_122, (float)L_123)));
		float L_124 = (&V_27)->get_y_2();
		float L_125 = (&V_4)->get_y_2();
		V_25 = ((float)il2cpp_codegen_subtract((float)L_124, (float)L_125));
		float L_126 = (&V_27)->get_y_2();
		int32_t L_127 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_128 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_126/(float)(((float)((float)L_127))))), /*hidden argument*/NULL);
		V_28 = L_128;
		int32_t L_129 = __this->get_minIndex_4();
		int32_t L_130 = __this->get_maxIndex_5();
		if ((((int32_t)L_129) == ((int32_t)L_130)))
		{
			goto IL_039b;
		}
	}
	{
		int32_t L_131 = __this->get_minIndex_4();
		int32_t L_132 = V_28;
		if ((((int32_t)L_131) > ((int32_t)L_132)))
		{
			goto IL_03b3;
		}
	}
	{
		int32_t L_133 = V_28;
		int32_t L_134 = __this->get_maxIndex_5();
		if ((((int32_t)L_133) > ((int32_t)L_134)))
		{
			goto IL_03b3;
		}
	}

IL_039b:
	{
		Transform_t3600365921 * L_135 = V_24;
		Vector3_t3722313464  L_136 = V_27;
		NullCheck(L_135);
		Transform_set_localPosition_m4128471975(L_135, L_136, /*hidden argument*/NULL);
		Transform_t3600365921 * L_137 = V_24;
		int32_t L_138 = V_22;
		VirtActionInvoker2< Transform_t3600365921 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_137, L_138);
		goto IL_03b6;
	}

IL_03b3:
	{
		V_5 = (bool)0;
	}

IL_03b6:
	{
		goto IL_0463;
	}

IL_03bb:
	{
		float L_139 = V_25;
		float L_140 = V_0;
		if ((!(((float)L_139) > ((float)L_140))))
		{
			goto IL_044e;
		}
	}
	{
		Transform_t3600365921 * L_141 = V_24;
		NullCheck(L_141);
		Vector3_t3722313464  L_142 = Transform_get_localPosition_m4234289348(L_141, /*hidden argument*/NULL);
		V_29 = L_142;
		Vector3_t3722313464 * L_143 = (&V_29);
		float L_144 = L_143->get_y_2();
		float L_145 = V_6;
		L_143->set_y_2(((float)il2cpp_codegen_subtract((float)L_144, (float)L_145)));
		float L_146 = (&V_29)->get_y_2();
		float L_147 = (&V_4)->get_y_2();
		V_25 = ((float)il2cpp_codegen_subtract((float)L_146, (float)L_147));
		float L_148 = (&V_29)->get_y_2();
		int32_t L_149 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_150 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_148/(float)(((float)((float)L_149))))), /*hidden argument*/NULL);
		V_30 = L_150;
		int32_t L_151 = __this->get_minIndex_4();
		int32_t L_152 = __this->get_maxIndex_5();
		if ((((int32_t)L_151) == ((int32_t)L_152)))
		{
			goto IL_042e;
		}
	}
	{
		int32_t L_153 = __this->get_minIndex_4();
		int32_t L_154 = V_30;
		if ((((int32_t)L_153) > ((int32_t)L_154)))
		{
			goto IL_0446;
		}
	}
	{
		int32_t L_155 = V_30;
		int32_t L_156 = __this->get_maxIndex_5();
		if ((((int32_t)L_155) > ((int32_t)L_156)))
		{
			goto IL_0446;
		}
	}

IL_042e:
	{
		Transform_t3600365921 * L_157 = V_24;
		Vector3_t3722313464  L_158 = V_29;
		NullCheck(L_157);
		Transform_set_localPosition_m4128471975(L_157, L_158, /*hidden argument*/NULL);
		Transform_t3600365921 * L_159 = V_24;
		int32_t L_160 = V_22;
		VirtActionInvoker2< Transform_t3600365921 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_159, L_160);
		goto IL_0449;
	}

IL_0446:
	{
		V_5 = (bool)0;
	}

IL_0449:
	{
		goto IL_0463;
	}

IL_044e:
	{
		bool L_161 = __this->get_mFirstTime_12();
		if (!L_161)
		{
			goto IL_0463;
		}
	}
	{
		Transform_t3600365921 * L_162 = V_24;
		int32_t L_163 = V_22;
		VirtActionInvoker2< Transform_t3600365921 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_162, L_163);
	}

IL_0463:
	{
		bool L_164 = __this->get_cullContent_3();
		if (!L_164)
		{
			goto IL_04cc;
		}
	}
	{
		float L_165 = V_25;
		UIPanel_t1716472341 * L_166 = __this->get_mPanel_9();
		NullCheck(L_166);
		Vector2_t2156229523  L_167 = UIPanel_get_clipOffset_m110674167(L_166, /*hidden argument*/NULL);
		V_31 = L_167;
		float L_168 = (&V_31)->get_y_1();
		Transform_t3600365921 * L_169 = __this->get_mTrans_8();
		NullCheck(L_169);
		Vector3_t3722313464  L_170 = Transform_get_localPosition_m4234289348(L_169, /*hidden argument*/NULL);
		V_32 = L_170;
		float L_171 = (&V_32)->get_y_2();
		V_25 = ((float)il2cpp_codegen_add((float)L_165, (float)((float)il2cpp_codegen_subtract((float)L_168, (float)L_171))));
		Transform_t3600365921 * L_172 = V_24;
		NullCheck(L_172);
		GameObject_t1113636619 * L_173 = Component_get_gameObject_m442555142(L_172, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t1356438871_il2cpp_TypeInfo_var);
		bool L_174 = UICamera_IsPressed_m2472947029(NULL /*static, unused*/, L_173, /*hidden argument*/NULL);
		if (L_174)
		{
			goto IL_04cc;
		}
	}
	{
		Transform_t3600365921 * L_175 = V_24;
		NullCheck(L_175);
		GameObject_t1113636619 * L_176 = Component_get_gameObject_m442555142(L_175, /*hidden argument*/NULL);
		float L_177 = V_25;
		float L_178 = V_20;
		G_B50_0 = L_176;
		if ((!(((float)L_177) > ((float)L_178))))
		{
			G_B51_0 = L_176;
			goto IL_04c5;
		}
	}
	{
		float L_179 = V_25;
		float L_180 = V_21;
		G_B52_0 = ((((float)L_179) < ((float)L_180))? 1 : 0);
		G_B52_1 = G_B50_0;
		goto IL_04c6;
	}

IL_04c5:
	{
		G_B52_0 = 0;
		G_B52_1 = G_B51_0;
	}

IL_04c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m1909334044(NULL /*static, unused*/, G_B52_1, (bool)G_B52_0, (bool)0, /*hidden argument*/NULL);
	}

IL_04cc:
	{
		int32_t L_181 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add((int32_t)L_181, (int32_t)1));
	}

IL_04d2:
	{
		int32_t L_182 = V_22;
		int32_t L_183 = V_23;
		if ((((int32_t)L_182) < ((int32_t)L_183)))
		{
			goto IL_02fe;
		}
	}

IL_04db:
	{
		UIScrollView_t1973404950 * L_184 = __this->get_mScroll_10();
		bool L_185 = V_5;
		NullCheck(L_184);
		L_184->set_restrictWithinPanel_5((bool)((((int32_t)L_185) == ((int32_t)0))? 1 : 0));
		UIScrollView_t1973404950 * L_186 = __this->get_mScroll_10();
		NullCheck(L_186);
		UIScrollView_InvalidateBounds_m1519753606(L_186, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::OnValidate()
extern "C"  void UIWrapContent_OnValidate_m3758291390 (UIWrapContent_t1188558554 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_maxIndex_5();
		int32_t L_1 = __this->get_minIndex_4();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_2);
	}

IL_001d:
	{
		int32_t L_3 = __this->get_minIndex_4();
		int32_t L_4 = __this->get_maxIndex_5();
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_5);
	}

IL_003a:
	{
		return;
	}
}
// System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32)
extern "C"  void UIWrapContent_UpdateItem_m2557257775 (UIWrapContent_t1188558554 * __this, Transform_t3600365921 * ___item0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_UpdateItem_m2557257775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B4_0 = 0;
	{
		OnInitializeItem_t992046894 * L_0 = __this->get_onInitializeItem_7();
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		UIScrollView_t1973404950 * L_1 = __this->get_mScroll_10();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_movement_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_003c;
		}
	}
	{
		Transform_t3600365921 * L_3 = ___item0;
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_localPosition_m4234289348(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		int32_t L_6 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_5/(float)(((float)((float)L_6))))), /*hidden argument*/NULL);
		G_B4_0 = L_7;
		goto IL_0057;
	}

IL_003c:
	{
		Transform_t3600365921 * L_8 = ___item0;
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_localPosition_m4234289348(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_x_1();
		int32_t L_11 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_12 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_10/(float)(((float)((float)L_11))))), /*hidden argument*/NULL);
		G_B4_0 = L_12;
	}

IL_0057:
	{
		V_0 = G_B4_0;
		OnInitializeItem_t992046894 * L_13 = __this->get_onInitializeItem_7();
		Transform_t3600365921 * L_14 = ___item0;
		NullCheck(L_14);
		GameObject_t1113636619 * L_15 = Component_get_gameObject_m442555142(L_14, /*hidden argument*/NULL);
		int32_t L_16 = ___index1;
		int32_t L_17 = V_0;
		NullCheck(L_13);
		OnInitializeItem_Invoke_m4231678144(L_13, L_15, L_16, L_17, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIWrapContent/OnInitializeItem::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInitializeItem__ctor_m691092399 (OnInitializeItem_t992046894 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWrapContent/OnInitializeItem::Invoke(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void OnInitializeItem_Invoke_m4231678144 (OnInitializeItem_t992046894 * __this, GameObject_t1113636619 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnInitializeItem_Invoke_m4231678144((OnInitializeItem_t992046894 *)__this->get_prev_9(), ___go0, ___wrapIndex1, ___realIndex2, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 3)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, GameObject_t1113636619 *, int32_t, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___go0, ___wrapIndex1, ___realIndex2, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, GameObject_t1113636619 *, int32_t, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___go0, ___wrapIndex1, ___realIndex2, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 3)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker3< GameObject_t1113636619 *, int32_t, int32_t >::Invoke(targetMethod, targetThis, ___go0, ___wrapIndex1, ___realIndex2);
					else
						GenericVirtActionInvoker3< GameObject_t1113636619 *, int32_t, int32_t >::Invoke(targetMethod, targetThis, ___go0, ___wrapIndex1, ___realIndex2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker3< GameObject_t1113636619 *, int32_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___go0, ___wrapIndex1, ___realIndex2);
					else
						VirtActionInvoker3< GameObject_t1113636619 *, int32_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___go0, ___wrapIndex1, ___realIndex2);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, GameObject_t1113636619 *, int32_t, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___go0, ___wrapIndex1, ___realIndex2, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< int32_t, int32_t >::Invoke(targetMethod, ___go0, ___wrapIndex1, ___realIndex2);
					else
						GenericVirtActionInvoker2< int32_t, int32_t >::Invoke(targetMethod, ___go0, ___wrapIndex1, ___realIndex2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< int32_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___go0, ___wrapIndex1, ___realIndex2);
					else
						VirtActionInvoker2< int32_t, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___go0, ___wrapIndex1, ___realIndex2);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (GameObject_t1113636619 *, int32_t, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___go0, ___wrapIndex1, ___realIndex2, targetMethod);
			}
		}
	}
}
// System.IAsyncResult UIWrapContent/OnInitializeItem::BeginInvoke(UnityEngine.GameObject,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnInitializeItem_BeginInvoke_m3043049668 (OnInitializeItem_t992046894 * __this, GameObject_t1113636619 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, AsyncCallback_t3962456242 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnInitializeItem_BeginInvoke_m3043049668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___go0;
	__d_args[1] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___wrapIndex1);
	__d_args[2] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___realIndex2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void UIWrapContent/OnInitializeItem::EndInvoke(System.IAsyncResult)
extern "C"  void OnInitializeItem_EndInvoke_m2629410527 (OnInitializeItem_t992046894 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityProjectSetting::.ctor()
extern "C"  void UnityProjectSetting__ctor_m3803001152 (UnityProjectSetting_t2579824760 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityProjectSetting__ctor_m3803001152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__gameType_4(1);
		__this->set_StageNum_5(1);
		__this->set__loadStageNo_6(6);
		__this->set__shareFormatInTitle_7(_stringLiteral1845752016);
		__this->set__shareFormatInClear_8(_stringLiteral850945763);
		__this->set__shareFormatInMiss_9(_stringLiteral2735947596);
		__this->set__shareFormatInTitleEnglish_11(_stringLiteral1845752016);
		__this->set__shareFormatInClearEnglish_12(_stringLiteral850945763);
		__this->set__shareFormatInMissEnglish_13(_stringLiteral2735947596);
		__this->set__tag_15(_stringLiteral2653172448);
		__this->set__iosStoreURL_16(_stringLiteral1403141053);
		__this->set__androidStoreURL_17(_stringLiteral1465845074);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityProjectSetting UnityProjectSetting::get_Entity()
extern "C"  UnityProjectSetting_t2579824760 * UnityProjectSetting_get_Entity_m2785360446 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityProjectSetting_get_Entity_m2785360446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityProjectSetting_t2579824760 * L_0 = ((UnityProjectSetting_t2579824760_StaticFields*)il2cpp_codegen_static_fields_for(UnityProjectSetting_t2579824760_il2cpp_TypeInfo_var))->get__entity_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		UnityProjectSetting_t2579824760 * L_2 = Resources_Load_TisUnityProjectSetting_t2579824760_m3201061709(NULL /*static, unused*/, _stringLiteral1472809214, /*hidden argument*/Resources_Load_TisUnityProjectSetting_t2579824760_m3201061709_RuntimeMethod_var);
		((UnityProjectSetting_t2579824760_StaticFields*)il2cpp_codegen_static_fields_for(UnityProjectSetting_t2579824760_il2cpp_TypeInfo_var))->set__entity_3(L_2);
		UnityProjectSetting_t2579824760 * L_3 = ((UnityProjectSetting_t2579824760_StaticFields*)il2cpp_codegen_static_fields_for(UnityProjectSetting_t2579824760_il2cpp_TypeInfo_var))->get__entity_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		Debug_LogError_m1051815641(NULL /*static, unused*/, _stringLiteral544349615, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
	}

IL_003a:
	{
		UnityProjectSetting_t2579824760 * L_5 = ((UnityProjectSetting_t2579824760_StaticFields*)il2cpp_codegen_static_fields_for(UnityProjectSetting_t2579824760_il2cpp_TypeInfo_var))->get__entity_3();
		return L_5;
	}
}
// GameType UnityProjectSetting::get_GameType()
extern "C"  int32_t UnityProjectSetting_get_GameType_m3090335053 (UnityProjectSetting_t2579824760 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__gameType_4();
		return L_0;
	}
}
// System.Int32 UnityProjectSetting::get_LoadStageNo()
extern "C"  int32_t UnityProjectSetting_get_LoadStageNo_m3563312919 (UnityProjectSetting_t2579824760 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__loadStageNo_6();
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<ShareLocation,System.String> UnityProjectSetting::get_ShareFormatDict()
extern "C"  Dictionary_2_t3173579796 * UnityProjectSetting_get_ShareFormatDict_m3368659902 (UnityProjectSetting_t2579824760 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityProjectSetting_get_ShareFormatDict_m3368659902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3173579796 * V_0 = NULL;
	{
		Dictionary_2_t3173579796 * L_0 = __this->get__shareFormatDict_10();
		if (L_0)
		{
			goto IL_004c;
		}
	}
	{
		Dictionary_2_t3173579796 * L_1 = (Dictionary_2_t3173579796 *)il2cpp_codegen_object_new(Dictionary_2_t3173579796_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3989434104(L_1, /*hidden argument*/Dictionary_2__ctor_m3989434104_RuntimeMethod_var);
		V_0 = L_1;
		Dictionary_2_t3173579796 * L_2 = V_0;
		String_t* L_3 = __this->get__shareFormatInTitle_7();
		NullCheck(L_2);
		Dictionary_2_Add_m3554053801(L_2, 0, L_3, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_4 = V_0;
		String_t* L_5 = __this->get__shareFormatInClear_8();
		NullCheck(L_4);
		Dictionary_2_Add_m3554053801(L_4, 1, L_5, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_6 = V_0;
		String_t* L_7 = __this->get__shareFormatInClear_8();
		NullCheck(L_6);
		Dictionary_2_Add_m3554053801(L_6, 2, L_7, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_8 = V_0;
		String_t* L_9 = __this->get__shareFormatInMiss_9();
		NullCheck(L_8);
		Dictionary_2_Add_m3554053801(L_8, 3, L_9, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_10 = V_0;
		__this->set__shareFormatDict_10(L_10);
	}

IL_004c:
	{
		Dictionary_2_t3173579796 * L_11 = __this->get__shareFormatDict_10();
		return L_11;
	}
}
// System.Collections.Generic.Dictionary`2<ShareLocation,System.String> UnityProjectSetting::get_ShareFormatDictEnglish()
extern "C"  Dictionary_2_t3173579796 * UnityProjectSetting_get_ShareFormatDictEnglish_m1615427313 (UnityProjectSetting_t2579824760 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityProjectSetting_get_ShareFormatDictEnglish_m1615427313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3173579796 * V_0 = NULL;
	{
		Dictionary_2_t3173579796 * L_0 = __this->get__shareFormatDictEnglish_14();
		if (L_0)
		{
			goto IL_004c;
		}
	}
	{
		Dictionary_2_t3173579796 * L_1 = (Dictionary_2_t3173579796 *)il2cpp_codegen_object_new(Dictionary_2_t3173579796_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3989434104(L_1, /*hidden argument*/Dictionary_2__ctor_m3989434104_RuntimeMethod_var);
		V_0 = L_1;
		Dictionary_2_t3173579796 * L_2 = V_0;
		String_t* L_3 = __this->get__shareFormatInTitleEnglish_11();
		NullCheck(L_2);
		Dictionary_2_Add_m3554053801(L_2, 0, L_3, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_4 = V_0;
		String_t* L_5 = __this->get__shareFormatInClearEnglish_12();
		NullCheck(L_4);
		Dictionary_2_Add_m3554053801(L_4, 1, L_5, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_6 = V_0;
		String_t* L_7 = __this->get__shareFormatInClearEnglish_12();
		NullCheck(L_6);
		Dictionary_2_Add_m3554053801(L_6, 2, L_7, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_8 = V_0;
		String_t* L_9 = __this->get__shareFormatInMissEnglish_13();
		NullCheck(L_8);
		Dictionary_2_Add_m3554053801(L_8, 3, L_9, /*hidden argument*/Dictionary_2_Add_m3554053801_RuntimeMethod_var);
		Dictionary_2_t3173579796 * L_10 = V_0;
		__this->set__shareFormatDictEnglish_14(L_10);
	}

IL_004c:
	{
		Dictionary_2_t3173579796 * L_11 = __this->get__shareFormatDictEnglish_14();
		return L_11;
	}
}
// System.String UnityProjectSetting::get_Tag()
extern "C"  String_t* UnityProjectSetting_get_Tag_m877662104 (UnityProjectSetting_t2579824760 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__tag_15();
		return L_0;
	}
}
// System.String UnityProjectSetting::get_StoreURL()
extern "C"  String_t* UnityProjectSetting_get_StoreURL_m758555589 (UnityProjectSetting_t2579824760 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_1 = __this->get__androidStoreURL_17();
		return L_1;
	}

IL_0013:
	{
		String_t* L_2 = __this->get__iosStoreURL_16();
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UserData::Save()
extern "C"  void UserData_Save_m1334545219 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_Save_m1334545219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		int32_t L_0 = UserData_get_PlayCount_m3442301827(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerPrefsUtility_Save_m1276614609(NULL /*static, unused*/, _stringLiteral1838617347, L_0, /*hidden argument*/NULL);
		bool L_1 = UserData_get_IsMute_m2788164951(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerPrefsUtility_Save_m3487088120(NULL /*static, unused*/, _stringLiteral1453243796, L_1, /*hidden argument*/NULL);
		bool L_2 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isFirstTimeOpenApp_12();
		PlayerPrefsUtility_Save_m3487088120(NULL /*static, unused*/, _stringLiteral3021547732, L_2, /*hidden argument*/NULL);
		DateTime_t3738529785  L_3 = UserData_get_TimeOfPushedHouseAdButton_m195643557(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerPrefsUtility_Save_m2171612309(NULL /*static, unused*/, _stringLiteral3474414062, L_3, /*hidden argument*/NULL);
		Dictionary_2_t2736202052 * L_4 = UserData_get_HighScoreDict_m1377977320(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerPrefsUtility_SaveDict_TisString_t_TisInt32_t2950945753_m1477132359(NULL /*static, unused*/, _stringLiteral311406783, L_4, /*hidden argument*/PlayerPrefsUtility_SaveDict_TisString_t_TisInt32_t2950945753_m1477132359_RuntimeMethod_var);
		PlayerPrefs_Save_m2701929462(NULL /*static, unused*/, /*hidden argument*/NULL);
		Debug_Log_m3276510664(NULL /*static, unused*/, _stringLiteral3236165112, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UserData::get_PlayCount()
extern "C"  int32_t UserData_get_PlayCount_m3442301827 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_get_PlayCount_m3442301827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isInitializedPlayCount_1();
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		int32_t L_1 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__playCount_2();
		int32_t L_2 = PlayerPrefsUtility_Load_m3492684851(NULL /*static, unused*/, _stringLiteral1838617347, L_1, /*hidden argument*/NULL);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__playCount_2(L_2);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedPlayCount_1((bool)1);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		int32_t L_3 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__playCount_2();
		return L_3;
	}
}
// System.Void UserData::set_PlayCount(System.Int32)
extern "C"  void UserData_set_PlayCount_m3841829882 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_set_PlayCount_m3841829882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__playCount_2(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UserData::get_HighScoreDict()
extern "C"  Dictionary_2_t2736202052 * UserData_get_HighScoreDict_m1377977320 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_get_HighScoreDict_m1377977320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isInitializedHighScoreDict_4();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Dictionary_2_t2736202052 * L_1 = PlayerPrefsUtility_LoadDict_TisString_t_TisInt32_t2950945753_m1536884824(NULL /*static, unused*/, _stringLiteral311406783, /*hidden argument*/PlayerPrefsUtility_LoadDict_TisString_t_TisInt32_t2950945753_m1536884824_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__highScoreDict_5(L_1);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedHighScoreDict_4((bool)1);
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Dictionary_2_t2736202052 * L_2 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__highScoreDict_5();
		return L_2;
	}
}
// System.Void UserData::set_HighScoreDict(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void UserData_set_HighScoreDict_m2375964171 (RuntimeObject * __this /* static, unused */, Dictionary_2_t2736202052 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_set_HighScoreDict_m2375964171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2736202052 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__highScoreDict_5(L_0);
		return;
	}
}
// System.Int32 UserData::get_HighScore()
extern "C"  int32_t UserData_get_HighScore_m2475012517 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_get_HighScore_m2475012517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Dictionary_2_t2736202052 * L_0 = UserData_get_HighScoreDict_m1377977320(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = DictionaryExtensions_GetValueOrDefault_TisString_t_TisInt32_t2950945753_m3168452452(NULL /*static, unused*/, L_0, _stringLiteral3452614543, 0, /*hidden argument*/DictionaryExtensions_GetValueOrDefault_TisString_t_TisInt32_t2950945753_m3168452452_RuntimeMethod_var);
		return L_1;
	}
}
// System.Void UserData::set_HighScore(System.Int32)
extern "C"  void UserData_set_HighScore_m700482015 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_set_HighScore_m700482015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Dictionary_2_t2736202052 * L_0 = UserData_get_HighScoreDict_m1377977320(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___value0;
		NullCheck(L_0);
		Dictionary_2_set_Item_m3800595820(L_0, _stringLiteral3452614543, L_1, /*hidden argument*/Dictionary_2_set_Item_m3800595820_RuntimeMethod_var);
		return;
	}
}
// System.Void UserData::add_OnChangeMuteSetting(System.Action`1<System.Boolean>)
extern "C"  void UserData_add_OnChangeMuteSetting_m564281592 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_add_OnChangeMuteSetting_m564281592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t269755560 * V_0 = NULL;
	Action_1_t269755560 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_OnChangeMuteSetting_9();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t269755560 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_2 = V_1;
		Action_1_t269755560 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_1_t269755560 * L_5 = V_0;
		Action_1_t269755560 * L_6 = InterlockedCompareExchangeImpl<Action_1_t269755560 *>((((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_address_of_OnChangeMuteSetting_9()), ((Action_1_t269755560 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t269755560_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_1_t269755560 * L_7 = V_0;
		Action_1_t269755560 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t269755560 *)L_7) == ((RuntimeObject*)(Action_1_t269755560 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UserData::remove_OnChangeMuteSetting(System.Action`1<System.Boolean>)
extern "C"  void UserData_remove_OnChangeMuteSetting_m3825023625 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_remove_OnChangeMuteSetting_m3825023625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t269755560 * V_0 = NULL;
	Action_1_t269755560 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_OnChangeMuteSetting_9();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t269755560 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_2 = V_1;
		Action_1_t269755560 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_1_t269755560 * L_5 = V_0;
		Action_1_t269755560 * L_6 = InterlockedCompareExchangeImpl<Action_1_t269755560 *>((((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_address_of_OnChangeMuteSetting_9()), ((Action_1_t269755560 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t269755560_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_1_t269755560 * L_7 = V_0;
		Action_1_t269755560 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t269755560 *)L_7) == ((RuntimeObject*)(Action_1_t269755560 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Boolean UserData::get_IsMute()
extern "C"  bool UserData_get_IsMute_m2788164951 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_get_IsMute_m2788164951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isInitializedMuteSetting_7();
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_1 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isMute_8();
		bool L_2 = PlayerPrefsUtility_Load_m3631663835(NULL /*static, unused*/, _stringLiteral1453243796, L_1, /*hidden argument*/NULL);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isMute_8(L_2);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedMuteSetting_7((bool)1);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_3 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isMute_8();
		return L_3;
	}
}
// System.Void UserData::set_IsMute(System.Boolean)
extern "C"  void UserData_set_IsMute_m3373741355 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_set_IsMute_m3373741355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isMute_8(L_0);
		Action_1_t269755560 * L_1 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_OnChangeMuteSetting_9();
		bool L_2 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isMute_8();
		NullCheck(L_1);
		Action_1_Invoke_m1933767679(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1933767679_RuntimeMethod_var);
		return;
	}
}
// System.Void UserData::add_OnChangeFirstTime(System.Action`1<System.Boolean>)
extern "C"  void UserData_add_OnChangeFirstTime_m2478173862 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_add_OnChangeFirstTime_m2478173862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t269755560 * V_0 = NULL;
	Action_1_t269755560 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_OnChangeFirstTime_13();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t269755560 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_2 = V_1;
		Action_1_t269755560 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_1_t269755560 * L_5 = V_0;
		Action_1_t269755560 * L_6 = InterlockedCompareExchangeImpl<Action_1_t269755560 *>((((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_address_of_OnChangeFirstTime_13()), ((Action_1_t269755560 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t269755560_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_1_t269755560 * L_7 = V_0;
		Action_1_t269755560 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t269755560 *)L_7) == ((RuntimeObject*)(Action_1_t269755560 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UserData::remove_OnChangeFirstTime(System.Action`1<System.Boolean>)
extern "C"  void UserData_remove_OnChangeFirstTime_m1822527583 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_remove_OnChangeFirstTime_m1822527583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t269755560 * V_0 = NULL;
	Action_1_t269755560 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_OnChangeFirstTime_13();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t269755560 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_2 = V_1;
		Action_1_t269755560 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_1_t269755560 * L_5 = V_0;
		Action_1_t269755560 * L_6 = InterlockedCompareExchangeImpl<Action_1_t269755560 *>((((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_address_of_OnChangeFirstTime_13()), ((Action_1_t269755560 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t269755560_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_1_t269755560 * L_7 = V_0;
		Action_1_t269755560 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t269755560 *)L_7) == ((RuntimeObject*)(Action_1_t269755560 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Boolean UserData::get_IsFisrtTime()
extern "C"  bool UserData_get_IsFisrtTime_m903176408 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_get_IsFisrtTime_m903176408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isInitFirstTimeOpenApp_11();
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_1 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isMute_8();
		bool L_2 = PlayerPrefsUtility_Load_m3631663835(NULL /*static, unused*/, _stringLiteral1453243796, L_1, /*hidden argument*/NULL);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isFirstTimeOpenApp_12(L_2);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitFirstTimeOpenApp_11((bool)1);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_3 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isMute_8();
		return L_3;
	}
}
// System.Void UserData::set_IsFisrtTime(System.Boolean)
extern "C"  void UserData_set_IsFisrtTime_m19282104 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_set_IsFisrtTime_m19282104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isFirstTimeOpenApp_12(L_0);
		Action_1_t269755560 * L_1 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get_OnChangeFirstTime_13();
		bool L_2 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isMute_8();
		NullCheck(L_1);
		Action_1_Invoke_m1933767679(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1933767679_RuntimeMethod_var);
		return;
	}
}
// System.DateTime UserData::get_TimeOfPushedHouseAdButton()
extern "C"  DateTime_t3738529785  UserData_get_TimeOfPushedHouseAdButton_m195643557 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_get_TimeOfPushedHouseAdButton_m195643557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		bool L_0 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__isInitializedTimeOfPushedHouseAdButton_15();
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_1 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__timeOfPushedHouseAdButton_16();
		DateTime_t3738529785  L_2 = PlayerPrefsUtility_Load_m3694695221(NULL /*static, unused*/, _stringLiteral3474414062, L_1, /*hidden argument*/NULL);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__timeOfPushedHouseAdButton_16(L_2);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedTimeOfPushedHouseAdButton_15((bool)1);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_3 = ((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->get__timeOfPushedHouseAdButton_16();
		return L_3;
	}
}
// System.Void UserData::set_TimeOfPushedHouseAdButton(System.DateTime)
extern "C"  void UserData_set_TimeOfPushedHouseAdButton_m977932569 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData_set_TimeOfPushedHouseAdButton_m977932569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t3738529785  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(UserData_t2822117362_il2cpp_TypeInfo_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__timeOfPushedHouseAdButton_16(L_0);
		return;
	}
}
// System.Void UserData::.cctor()
extern "C"  void UserData__cctor_m656705242 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserData__cctor_m656705242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedPlayCount_1((bool)0);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__playCount_2(0);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedHighScoreDict_4((bool)0);
		Dictionary_2_t2736202052 * L_0 = (Dictionary_2_t2736202052 *)il2cpp_codegen_object_new(Dictionary_2_t2736202052_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3200964102(L_0, /*hidden argument*/Dictionary_2__ctor_m3200964102_RuntimeMethod_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__highScoreDict_5(L_0);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedMuteSetting_7((bool)0);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isMute_8((bool)0);
		intptr_t L_1 = (intptr_t)UserData_U3COnChangeMuteSettingU3Em__0_m2939914662_RuntimeMethod_var;
		Action_1_t269755560 * L_2 = (Action_1_t269755560 *)il2cpp_codegen_object_new(Action_1_t269755560_il2cpp_TypeInfo_var);
		Action_1__ctor_m981112613(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m981112613_RuntimeMethod_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set_OnChangeMuteSetting_9(L_2);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitFirstTimeOpenApp_11((bool)0);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isFirstTimeOpenApp_12((bool)0);
		intptr_t L_3 = (intptr_t)UserData_U3COnChangeFirstTimeU3Em__1_m4114381407_RuntimeMethod_var;
		Action_1_t269755560 * L_4 = (Action_1_t269755560 *)il2cpp_codegen_object_new(Action_1_t269755560_il2cpp_TypeInfo_var);
		Action_1__ctor_m981112613(L_4, NULL, L_3, /*hidden argument*/Action_1__ctor_m981112613_RuntimeMethod_var);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set_OnChangeFirstTime_13(L_4);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__isInitializedTimeOfPushedHouseAdButton_15((bool)0);
		DateTime_t3738529785  L_5;
		memset(&L_5, 0, sizeof(L_5));
		DateTime__ctor_m2956360140((&L_5), ((int32_t)2000), 1, 1, ((int32_t)16), ((int32_t)32), 0, 2, /*hidden argument*/NULL);
		((UserData_t2822117362_StaticFields*)il2cpp_codegen_static_fields_for(UserData_t2822117362_il2cpp_TypeInfo_var))->set__timeOfPushedHouseAdButton_16(L_5);
		return;
	}
}
// System.Void UserData::<OnChangeMuteSetting>m__0(System.Boolean)
extern "C"  void UserData_U3COnChangeMuteSettingU3Em__0_m2939914662 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UserData::<OnChangeFirstTime>m__1(System.Boolean)
extern "C"  void UserData_U3COnChangeFirstTimeU3Em__1_m4114381407 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single VectorCalculator::GetAim(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float VectorCalculator_GetAim_m745570127 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___p10, Vector2_t2156229523  ___p21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorCalculator_GetAim_m745570127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = (&___p21)->get_x_0();
		float L_1 = (&___p10)->get_x_0();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_0, (float)L_1));
		float L_2 = (&___p21)->get_y_1();
		float L_3 = (&___p10)->get_y_1();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_2, (float)L_3));
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_6 = atan2f(L_4, L_5);
		V_2 = L_6;
		float L_7 = V_2;
		return ((float)il2cpp_codegen_multiply((float)L_7, (float)(57.29578f)));
	}
}
// UnityEngine.Vector3 VectorCalculator::GetDirection(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  VectorCalculator_GetDirection_m207383374 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___start0, Vector3_t3722313464  ___goal1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorCalculator_GetDirection_m207383374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3_t3722313464  L_0 = ___goal1;
		Vector3_t3722313464  L_1 = ___start0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_m27958459((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t3722313464  L_4 = V_0;
		float L_5 = V_1;
		Vector3_t3722313464  L_6 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Vector3_t3722313464  L_7 = V_2;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Velocity2DTmp::.ctor()
extern "C"  void Velocity2DTmp__ctor_m3231794684 (Velocity2DTmp_t2283591314 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Velocity2DTmp::get_AngularVelocity()
extern "C"  float Velocity2DTmp_get_AngularVelocity_m1585536783 (Velocity2DTmp_t2283591314 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__angularVelocity_2();
		return L_0;
	}
}
// UnityEngine.Vector2 Velocity2DTmp::get_Velocity()
extern "C"  Vector2_t2156229523  Velocity2DTmp_get_Velocity_m1575695724 (Velocity2DTmp_t2283591314 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0 = __this->get__velocity_3();
		return L_0;
	}
}
// System.Void Velocity2DTmp::Set(UnityEngine.Rigidbody2D)
extern "C"  void Velocity2DTmp_Set_m2080557956 (Velocity2DTmp_t2283591314 * __this, Rigidbody2D_t939494601 * ___rigidbody2D0, const RuntimeMethod* method)
{
	{
		Rigidbody2D_t939494601 * L_0 = ___rigidbody2D0;
		NullCheck(L_0);
		float L_1 = Rigidbody2D_get_angularVelocity_m1959705066(L_0, /*hidden argument*/NULL);
		__this->set__angularVelocity_2(L_1);
		Rigidbody2D_t939494601 * L_2 = ___rigidbody2D0;
		NullCheck(L_2);
		Vector2_t2156229523  L_3 = Rigidbody2D_get_velocity_m366589732(L_2, /*hidden argument*/NULL);
		__this->set__velocity_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VelocityTmp::.ctor()
extern "C"  void VelocityTmp__ctor_m2935043370 (VelocityTmp_t3274308841 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 VelocityTmp::get_AngularVelocity()
extern "C"  Vector3_t3722313464  VelocityTmp_get_AngularVelocity_m3263973382 (VelocityTmp_t3274308841 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = __this->get__angularVelocity_2();
		return L_0;
	}
}
// UnityEngine.Vector3 VelocityTmp::get_Velocity()
extern "C"  Vector3_t3722313464  VelocityTmp_get_Velocity_m4075903438 (VelocityTmp_t3274308841 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = __this->get__velocity_3();
		return L_0;
	}
}
// System.Void VelocityTmp::Set(UnityEngine.Rigidbody)
extern "C"  void VelocityTmp_Set_m3515497691 (VelocityTmp_t3274308841 * __this, Rigidbody_t3916780224 * ___rigidbody0, const RuntimeMethod* method)
{
	{
		Rigidbody_t3916780224 * L_0 = ___rigidbody0;
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Rigidbody_get_angularVelocity_m191123884(L_0, /*hidden argument*/NULL);
		__this->set__angularVelocity_2(L_1);
		Rigidbody_t3916780224 * L_2 = ___rigidbody0;
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Rigidbody_get_velocity_m2993632669(L_2, /*hidden argument*/NULL);
		__this->set__velocity_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
