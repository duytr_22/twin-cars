﻿//  UserData.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.28.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 永続的に保存するデータを管理するクラス
/// </summary>
public static class UserData
{

	//=================================================================================
	//外部からの保存用
	//=================================================================================

	/// <summary>
	/// 諸々のデータを保存
	/// </summary>
	public static void Save ()
	{
		PlayerPrefsUtility.Save (PLAY_COUNT_KEY, PlayCount);
		PlayerPrefsUtility.Save (IS_MUTE_KEY, IsMute);
		PlayerPrefsUtility.Save (FIRT_TIME_OPEN_APP, _isFirstTimeOpenApp);

		PlayerPrefsUtility.Save (TIME_OF_PUSHED_HOUSEAD_BUTTON_KEY, TimeOfPushedHouseAdButton);

		PlayerPrefsUtility.SaveDict<string, int> (HIGH_SCORE_KEY, HighScoreDict);

		//全てのデータを同期
		PlayerPrefs.Save ();
		Debug.Log ("Save PlayerPrefs");
	}

	//=================================================================================
	//プレイカウント
	//=================================================================================
	private const string PLAY_COUNT_KEY = "PLAY_COUNT_KEY";

	private static bool _isInitializedPlayCount = false;
	private static int _playCount = 0;

	/// <summary>
	/// ゲームをプレイした回数
	/// </summary>
	public static int PlayCount {
		get {
			if (!_isInitializedPlayCount) {
				_playCount = PlayerPrefsUtility.Load (PLAY_COUNT_KEY, _playCount);
				_isInitializedPlayCount = true;
			}
			return _playCount;
		}
		set{ _playCount = value; }
	}

	//=================================================================================
	//ハイスコア
	//=================================================================================
	private const string HIGH_SCORE_KEY = "HIGH_SCORE_KEY";

	private static bool _isInitializedHighScoreDict = false;
	private static Dictionary<string, int> _highScoreDict = new Dictionary<string, int> ();

	/// <summary>
	/// ハイスコア(複)
	/// </summary>
	public static Dictionary<string, int> HighScoreDict {
		get {
			if (!_isInitializedHighScoreDict) {
				_highScoreDict = PlayerPrefsUtility.LoadDict<string, int> (HIGH_SCORE_KEY);
				_isInitializedHighScoreDict = true;
			}
			return _highScoreDict;
		}
		set{ _highScoreDict = value; }
	}

	/// <summary>
	/// ハイスコア(単)
	/// </summary>
	public static int HighScore {
		get{ return HighScoreDict.GetValueOrDefault ("1", 0); }
		set{ HighScoreDict ["1"] = value; }
	}

	//=================================================================================
	//ミュートかどうか
	//=================================================================================
	private const string IS_MUTE_KEY = "IS_MUTE_KEY";

	private static bool _isInitializedMuteSetting = false;
	private static bool _isMute = false;

	//ミュートの状態が変わった時のイベント
	public static event Action<bool> OnChangeMuteSetting = delegate {};

	/// <summary>
	/// ミュートに設定されてるか否か
	/// </summary>
	public static bool IsMute {
		get {
			if (!_isInitializedMuteSetting) {
				_isMute = PlayerPrefsUtility.Load (IS_MUTE_KEY, _isMute);
				_isInitializedMuteSetting = true;
			}
			return _isMute;
		}
		set {
			_isMute = value;
			OnChangeMuteSetting (_isMute);
		}
	}

	//=================================================================================
	private const string FIRT_TIME_OPEN_APP = "IS_FIRST_TIME_OPEN_APP";

	private static bool _isInitFirstTimeOpenApp = false;
	private static bool _isFirstTimeOpenApp = false;

	//ミュートの状態が変わった時のイベント
	public static event Action<bool> OnChangeFirstTime = delegate {};

	/// <summary>
	/// ミュートに設定されてるか否か
	/// </summary>
	public static bool IsFisrtTime {
		get {
			if (!_isInitFirstTimeOpenApp) {
				_isFirstTimeOpenApp = PlayerPrefsUtility.Load (IS_MUTE_KEY, _isMute);
				_isInitFirstTimeOpenApp = true;
			}
			return _isMute;
		}
		set {
			_isFirstTimeOpenApp = value;
			OnChangeFirstTime (_isMute);
		}
	}
	//自社広告ボタンを押した最後の時間
	//=================================================================================
	private const string TIME_OF_PUSHED_HOUSEAD_BUTTON_KEY = "TIME_OF_PUSHED_HOUSEAD_BUTTON_KEY";

	private static bool _isInitializedTimeOfPushedHouseAdButton = false;
	private static DateTime _timeOfPushedHouseAdButton = new DateTime (2000, 1, 1, 16, 32, 0, DateTimeKind.Local);

	/// <summary>
	/// 自社広告ボタンを押した最後の時間
	/// </summary>
	public static DateTime TimeOfPushedHouseAdButton {
		get {
			if (!_isInitializedTimeOfPushedHouseAdButton) {
				_timeOfPushedHouseAdButton = PlayerPrefsUtility.Load (TIME_OF_PUSHED_HOUSEAD_BUTTON_KEY, _timeOfPushedHouseAdButton);
				_isInitializedTimeOfPushedHouseAdButton = true;
			}
			return _timeOfPushedHouseAdButton;
		}
		set {
			_timeOfPushedHouseAdButton = value;
		}
	}

}
