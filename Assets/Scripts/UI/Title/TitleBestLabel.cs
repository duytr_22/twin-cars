﻿//  TitleBestLabel.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.27.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// タイトルでベストスコアを表示するラベル
/// </summary>
[ExecuteInEditMode]
public class TitleBestLabel : BestLabel {
	
	//=================================================================================
	//更新
	//=================================================================================

	#if UNITY_EDITOR
	private void Update () {
		if(Application.isPlaying){
			return;
		}

		//Mode時は表示しないように
		GetComponent<Text> ().enabled = UnityProjectSetting.Entity.GameType != GameType.ModeSelect;
		UpdateText ();
	}
	#endif
}