//
//  GameCenter.m
//  TemplateProject
//
//
//

#import "GameCenter.h"

@interface GameCenter () <GKLeaderboardViewControllerDelegate>
+ (GameCenter *) sharedGameCenter;
@end


@implementation GameCenter

#pragma mark - Singleton
static GameCenter* _sharedGameCenter = nil;

+ (GameCenter *)sharedGameCenter
{
    @synchronized(self) {
        if (!_sharedGameCenter) {
            [[self alloc] init];
        }
    }
    return _sharedGameCenter;
}

+ (id)allocWithZone:(NSZone*)zone
{
    @synchronized(self) {
        if (!_sharedGameCenter) {
            _sharedGameCenter = [super allocWithZone:zone];
            
            return _sharedGameCenter;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone
{
    return self;
}



- (BOOL) startGameCenter
{
    if( [self isGameCenterAPIAvailable] == NO )
    {
        NSLog(@"startGameCenter Faile");
        return NO;
    }
    [self authenticateLocalPlayer];
    
    NSLog(@"startGameCenter OK");
    return YES;
}

- (BOOL) isGameCenterAPIAvailable
{
    // Check for presence of GKLocalPlayer class.
    BOOL localPlayerClassAvailable = (NSClassFromString(@"GKLocalPlayer")) != nil;
    
    // The device must be running iOS 4.1 or later.
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
    
    return (localPlayerClassAvailable && osVersionSupported);
}

- (void) authenticateLocalPlayer
{
    GKLocalPlayer* localPlayer = [GKLocalPlayer localPlayer];
    [localPlayer setAuthenticateHandler:^(UIViewController *, NSError *) {
        if( localPlayer.isAuthenticated )
        {
            /*
             ///< NSStringをchar*に変換...
             CCLOG("Alias : %s", localPlayer.alias);
             CCLOG("Player ID : %s", localPlayer.playerID);
             */
            NSLog(@"Alias : %@", localPlayer.alias);
            NSLog(@"Player ID : %@", localPlayer.playerID);
        }
    }];
}

- (void) reportScore:(int64_t)score Category:(NSString *)category
{
    GKScore * scoreReporter = [[GKScore alloc] initWithCategory:category];
    scoreReporter.value = score;
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error)
     {
         // スコア送信失敗時
         if( error != nil )
         {
//             CCMessageBox("reportScore Error", "Game Center");
         }
     }];
}

- (void) showLeaderboard
{
    GKLeaderboardViewController* pController = [[GKLeaderboardViewController alloc] init];
    [pController setViewState:GKGameCenterViewControllerStateLeaderboards];
	
    if( pController != nil )
    {
        pController.leaderboardDelegate = self;
		pController.category = nil;
		
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [rootViewController presentViewController:pController animated:YES  completion:nil];
    }
}

- (void) getUserRanking
{
    GKLeaderboard *pbBoard = [[GKLeaderboard alloc] init];
    pbBoard.timeScope = GKLeaderboardTimeScopeAllTime;
    pbBoard.range = NSMakeRange(1, 1);
    pbBoard.identifier = @"bestscore_twins1";
    
    [pbBoard loadScoresWithCompletionHandler: ^(NSArray *scores, NSError *error) {
        
        if (error != nil) {
            // handle the error.
        }
        if (scores != nil) {
            
            GKScore* score = [pbBoard localPlayerScore];
            long rank = score.rank;
            
            UnitySendMessage("NativeManager", "UpdateRanking", [[NSString stringWithFormat:@"%ld", rank] UTF8String ]);
        }
    }];
}

#pragma mark GKLeaderboardViewControllerDelegate
- (void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark 以下Deprecated
+ (BOOL) startGameCenter
{
    return [[self sharedGameCenter] startGameCenter];
}

+ (BOOL) isGameCenterAPIAvailable
{
    return [[self sharedGameCenter] isGameCenterAPIAvailable];
}

+ (void) authenticateLocalPlayer
{
    [[self sharedGameCenter] authenticateLocalPlayer];
}

+ (void) reportScore:(int64_t)score Category:(NSString *)category
{
    [[self sharedGameCenter] reportScore:score Category:category];
}

+ (void) showLeaderboard
{
    // Singletonのインスタンスに投げる
    [[self sharedGameCenter] showLeaderboard];
}

@end
