﻿//  CacheData.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.05.09.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[CreateAssetMenu()]
/// <summary>
/// キャッシュ用のデータ
/// </summary>
public class CacheData : ScriptableObject {

	//プロジェクトの設定ファイル
	private static CacheData _entity;
	public  static CacheData  Entity{
		get{
			if(_entity == null){
				_entity = Resources.Load<CacheData>(ResourcesFilePath.ASSET_CACHE_DATA);

				if(_entity == null){
					Debug.LogError(ResourcesFilePath.ASSET_CACHE_DATA + " not found");
				}

				//実機で実行する時は初期化する
				#if !UNITY_EDITOR
				_entity.Refresh ();
				#endif
			}
			return _entity;
		}
	}

	//ステージをクリアしたか、ハイスコアを更新したか
	public bool IsClear = false, IsNewRecored = false;

	//クリアしたステージNo
	public int StageNo = 1;

	//スコア
	public float CurrentScore = 0;

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// キャッシュを全て初期化
	/// </summary>
	public void Refresh(){
		IsClear      = false;
		IsNewRecored = false;
		CurrentScore = 0;
		StageNo      = 1;
	}

}