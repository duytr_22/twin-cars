//
//  ShareController.m
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  シェア関係の処理を行うクラス

#import "ShareController.h"

@implementation ShareController

//=================================================================================
//初期化
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName{
    self = [super init];
    return self;
}

//=================================================================================
//内部
//=================================================================================


//=================================================================================
//外部
//=================================================================================

//シェア
- (void)tweet:(const char *)text{
    NSString* shareMessage = [NSString stringWithUTF8String:text];
    NSLog(@"tweet %@", shareMessage);
    
    UIDevice *device = [UIDevice currentDevice];
    float version_ = [device.systemVersion floatValue];
    if (version_ < 6.0)
    {
        return;
    }
    
    __block UIActivityIndicatorView *spiner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    [UnityGetGLViewController().view addSubview:spiner];
    CGRect mainFrame = UnityGetGLViewController().view.frame;
    CGSize spinerSize = spiner.frame.size;
    float padingX = mainFrame.origin.x + mainFrame.size.width*0.5 - spinerSize.width*0.5;
    float padingY = mainFrame.origin.y + mainFrame.size.height*0.5 - spinerSize.height*0.5;
    [spiner setFrame:CGRectMake(padingX , padingY, spinerSize.width, spinerSize.height)];
    [spiner startAnimating];
    
    
    NSArray* items = [[NSArray alloc] initWithObjects:shareMessage, nil];
    UIActivityViewController* viewController = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
    
    if ( [viewController respondsToSelector:@selector(popoverPresentationController)] )
    {
        //iOS8以降
        viewController.popoverPresentationController.sourceView = UnityGetGLViewController().view;
    }
    
    [UnityGetGLViewController() presentViewController:viewController animated:YES completion:^{
        [spiner removeFromSuperview];
        spiner = nil;
    }];
}


@end
