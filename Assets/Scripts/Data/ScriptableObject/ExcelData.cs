﻿//  ExcelData.cs
//  ProductName Template
//
//  Created by kan.kikuchi

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// エクセルから変換して使う汎用データクラス
/// </summary>
public class ExcelData : ScriptableObject{

	//各行のデータをまとめたリスト
	public List<RowData> RowDataList = new List<RowData> ();

	//各行のデータ
	[System.SerializableAttribute]
	public class RowData{
		public List<string> DataList  = new List<string> ();
	}

	//=================================================================================
	//外部メソッド
	//=================================================================================

	/// <summary>
	/// 指定した行の全データを取得
	/// </summary>
	public List<string> GetColumnDataList(int columnNo){
		List<string> list = new List<string>();

		foreach (RowData detail in RowDataList) {
			list.Add(detail.DataList[columnNo]);
		}

		return list;
	}

}