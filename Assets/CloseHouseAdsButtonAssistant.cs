﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseHouseAdsButtonAssistant : ButtonAssistant
{
	[SerializeField]
	GameObject dialog;

	[SerializeField]
	bool showAds = false;

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		if (showAds) {
		
			#if UNITY_IPHONE
			Application.OpenURL ("https://itunes.apple.com/app/id1337487175?mt=8");
			#else
			Application.OpenURL ("https://play.google.com/store/apps/details?id=jp.keitachibana.Pinball");
			#endif
		}
		
		dialog.SetActive (false);
	}

}
