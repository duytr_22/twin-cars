//
//  GAEConfig.h
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  GAEの設定を読み込み、保持するクラス

#import "UserInfoObject.h"

//各プロバイダーのkey
#define GAE_ADSTIR_KEY       @"S"
#define GAE_ADSTIR_FULL_KEY  @"SF"

#define GAE_IMOBILE_KEY      @"I"
#define GAE_IMOBILE_FULL_KEY @"IF"
#define GAE_IMOBILE_ADEX_KEY @"IX"
#define GAE_ADGENERATION_KEY @"AG"

#define GAE_NEND_KEY         @"N"
#define GAE_NEND_FULL_KEY    @"NF"
#define GAE_NEND_NATIVE_KEY  @"NN"

#define GAE_ADCOLONY_KEY     @"AC"
#define GAE_UNITYAD_KEY      @"UA"
#define GAE_MAIO_KEY         @"MO"
#define GAE_APPLOVIN_KEY     @"L"
#define GAE_FLUCT_KEY        @"FL"
#define GAE_FIVE_KEY        @"F"
#define GAE_ADMOB            @"AM"

#define GAE_STARTUP_NOAD_KEY    @"0"
#define GAE_STARTUP_HALF_KEY    @"-1"

#define GAE_BROWSER_ON_KEY  @"1"
#define GAE_BROWSER_OFF_KEY @"0"

@interface GAEConfig  : NSObject


//読み込んだGAEの設定を取得するメソッド
+(NSString*)getNewsType;
+(NSString*)getSplashType;
+(NSString*)getSplashTypeRank;
+(int64_t)getSplashFrequency;
+(NSString*)getBannerType;
+(NSString*)getRectangleType;
+(NSString*)getRectangleEndType;
+(NSString*)getIsSWAT;

+(BOOL)isEnabledBrowser;

+(int64_t)getIncentiveRateAdColony;
+(int64_t)getIncentiveRateUnityAds;
+(int64_t)getIncentiveRateMaio;
+(int64_t)getVideoFrequency;
+(NSString*)getVideoType;

//GAEを読み込み、設定を確認する
+ (void) confirmGAE:(id)callbackInstance selector:(SEL)callback;


@end
