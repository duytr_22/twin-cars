//
//  StartupPopAdHelper.m
//  Unity-iPhone
//
//  Created on 2015/10/29.
//
//  起動時のポップアップ広告を管理する

#import "StartupPopAdHelper.h"
#import "AdManager.h"
#import "CommonConfig.h"

@implementation StartupPopAdHelper

//初期化
-(id)init{
	self = [super init];
	return self;
}

//起動時のポップアップ広告
-(void)showStartupPopAd{
	
	//初回は表示しない
	NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
	int showStartupPopAd = (int)[userDefault integerForKey:@"ShowStartupPopAd"];
    
	[userDefault setInteger:showStartupPopAd + 1 forKey:@"ShowStartupPopAd"];
	[userDefault synchronize];

	NSLog(@"showStartupPopAd:%d ", showStartupPopAd);

	if (showStartupPopAd < 3) {
		return;
    } else {
                
        if ([[GAEConfig getNewsType] isEqualToString:GAE_STARTUP_HALF_KEY]){ //Nend or HOUSEAD
            
            int probabirity = arc4random_uniform(101);
            
            if (0 < probabirity && probabirity <= 50) {
                
                
                
            } else {
                
            }
        }
    }
}


@end
