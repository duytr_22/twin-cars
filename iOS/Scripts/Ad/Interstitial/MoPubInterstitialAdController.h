//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  Nendのスプラッシュ(インターステイシャル)広告コントローラー

#import "InterstitialAdController.h"
#import "MoPub.h"

@interface MoPubInterstitialAdController : InterstitialAdController<MPInterstitialAdControllerDelegate>{

}
@property (nonatomic, retain) MPInterstitialAdController * interstitial;
@property(nonatomic, strong) NSTimer* timer;
-(id)init;
-(void)showInterstitialAd:(int)playCount;

@end
