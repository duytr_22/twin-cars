//
//  AdstirMovieAdController.h
//  Unity-iPhone
//
//  Created  on 2015/06/30.
//
//  Adstirの動画広告のコントローラー


#import "MovieAdController.h"
#import <IronSource/IronSource.h>

@interface IronSourceMovieAdController : MovieAdController<ISRewardedVideoDelegate> {
    
    //リワードを受け取ったか、ビューが閉じられたか
    BOOL _isReceivedReward;
}



-(id)init;
-(void)show;
-(bool)isCanShow;
@end
