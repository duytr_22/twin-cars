﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class CubeScript : MonoBehaviour
{

    // public float maxFadeDistance;
    // public float minFadeDistance;

    [SerializeField] CarCrashingPhysiscsBehavior _carCrashedPhysics;
    Rigidbody rigid;
    // private float alpha;
    // private bool isFade;
    // private Renderer _renderer;
    bool _isStopMoving;
    float _randomSpeed;
    float _moveSpeed;

    void OnEnable()
    {
        _carCrashedPhysics.onCollisionEnterEvent += OnCarCrashed;
    }
    
    void OnDisable()
    {
        _carCrashedPhysics.onCollisionEnterEvent -= OnCarCrashed;
    }

    private void OnCarCrashed()
    {
        _isStopMoving = true;
    }

    // Use this for initialization
    void Start()
    {
        rigid = this.GetComponent<Rigidbody>();

        // if (!_renderer)
        //     _renderer = this.GetComponent<Renderer>();

        // isFade = true;
        // alpha = 0;
    }


    public void RandomizeSpeed(int direction)
    {
        if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial)
            _randomSpeed = 0;
        else
        {
            _randomSpeed = GamePlayScript.Instance.scrollSpeed * Random.Range(0.04f, 0.15f);
            _randomSpeed *= direction;
        }
        _moveSpeed = GamePlayScript.Instance.scrollSpeed + _randomSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (GamePlayScript.Instance.isOver || _isStopMoving)
        {
            return;
        }

        //move the cube
        Vector3 local = rigid.transform.localPosition;
        local.z -= _moveSpeed * Time.deltaTime;
        rigid.transform.localPosition = local;

        //calculate transparent
        // if (isFade)
        // {
        //     float distance = this.transform.localPosition.z;
        //     alpha = Mathf.Abs(distance - maxFadeDistance) / (maxFadeDistance - minFadeDistance);

        //     if (alpha >= 1)
        //     {
        //         alpha = 1;
        //         isFade = false;
        //     }

        //     // Material mat = _renderer.material;

        //     // Color color = mat.GetColor("_Color");
        //     // color.a = alpha;
        //     // mat.SetColor("_Color", color);
        // }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "EndPoint")
        {

            GamePlayScript.Instance.spawnPointScript.CubeOnInActive(this.gameObject);

            this.gameObject.SetActive(false);
        }
    }

    //init the cube position
    public void InitCubeWithLocalPosition(Vector3 localposition)
    {
        // isFade = true;
        // alpha = 0;

        // if (!_renderer)
        //     _renderer = this.GetComponent<Renderer>();

        // Material mat = _renderer.material;
        // Color color = mat.GetColor("_Color");
        // color.a = alpha;
        // mat.SetColor("_Color", color);

        this.gameObject.name = Define.OBSTACLE_NAME;
        this.gameObject.SetActive(true);
        this.transform.localPosition = localposition;
    }

    public float GetMoveSpeed()
    {
        return _moveSpeed;
    }

    internal void SetMoveSpeed(float spd)
    {
        _moveSpeed = spd;
    }
}
