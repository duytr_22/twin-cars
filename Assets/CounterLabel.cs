﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterLabel : MonoBehaviour
{
	[SerializeField] 
	UISprite _counterImage;

	[SerializeField]
	string[] _spriteName;

	int _countDown = 5;

	void Start ()
	{
		StartCoroutine (CountDown ());
	}

	IEnumerator CountDown ()
	{
		TweenScale.Begin (gameObject, 0.15f, new Vector3 (1.1f, 1.1f, 1.1f));
		yield return new WaitForSeconds (0.15f);

		TweenScale.Begin (gameObject, 0.15f, new Vector3 (1.0f, 1.0f, 1.0f));
		yield return new WaitForSeconds (0.85f);

		TweenScale.Begin (gameObject, 0.15f, new Vector3 (0.8f, 0.8f, 0.8f));
		yield return new WaitForSeconds (0.15f);

		_countDown--;

		if (_countDown >= 0)
			_counterImage.spriteName = _spriteName [_countDown];
		 
		if (_countDown >= 0) {
			
			StartCoroutine (CountDown ());

		} else {

			StopAllCoroutines ();

			AdManager.Instance.HideGameRect ();

			GameManager.Instance.End (false);
			transform.parent.gameObject.SetActive (false);
		}

	}
}
