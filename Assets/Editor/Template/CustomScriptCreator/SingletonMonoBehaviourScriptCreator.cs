﻿using UnityEditor;

/// <summary>
/// SingletonMonoBehaviourのテンプレートから新しくスクリプトを作るクラス
/// </summary>
public class SingletonMonoBehaviourScriptCreator : CustomScriptCreator {

	private const string TEMPLATE_SCRIPT_NAME  = "SingletonMonoBehaviour";

	[MenuItem(MENU_PATH + TEMPLATE_SCRIPT_NAME)]
	private static void CreateScript(){
		ShowWindow (TEMPLATE_SCRIPT_NAME);
	}

}
