package jp.oliviaashley.Advertising.Providers;

import com.socdm.d.adgeneration.ADG;
import com.socdm.d.adgeneration.ADG.AdFrameSize;
import com.socdm.d.adgeneration.ADGListener;
import com.socdm.d.adgeneration.ADGConsts.ADGErrorCode;

import jp.oliviaashley.Twins.R;
import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;

/**
 * Library dependencies:
 * 		ADG-2.0.0.jar
 * 		google-play-services
 *
 * AndroidManifest.xml:
 * 		<uses-permission android:name="android.permission.INTERNET"/>
 * 		<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 * 		<application>
 * 			<meta-data android:name="com.google.android.gms.version"
 * 				android:value="@integer/google_play_services_version" />
 *          <activity android:name="com.socdm.d.adgeneration.ADGClickWebView"
 *        		android:configChanges="orientation|keyboardHidden" />
 *		 </application>
 *
 * @author Aaron Santin - 2014
 * @version 1.0
 */
public class AdGenerationHouseAdsHelper {

    // XXX AdGeneration also has banners, implement it

    private static final String TAG = "AdGenerationHelper";

    // ADGRectangle
    private static ADG adgBanner  = null;
    /**
     * Put this on your activity's onCreate method to preload intertitials
     *
     * @param activity
     * 				the main activity
     */
    public static void doOnCreate(Activity activity) {
    }

    public static void doOnStop(){
    }

    public static void doOnResume() {
        // 広告表示/ローテーション再開
        if (adgBanner!= null) {
            adgBanner.start();
        }
    }

    public static void doOnPause() {
        // ローテーション停止
        if (adgBanner!= null) {
            adgBanner.pause();
        }
    }

    public static LinearLayout getBannerView(Activity activity){

        Log.v(TAG, "Showing AdGeneration Banner ...");

        LinearLayout layout = new LinearLayout(activity);

        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        adgBanner = new ADG(activity);

        if(!isJapaneseLanguage(activity))
            adgBanner.setLocationId(activity.getResources().getString(R.string.AdGene_HouseAds_BannerID));
        else
            adgBanner.setLocationId(activity.getResources().getString(R.string.AdGene_HouseAds_BannerID_JP));

        adgBanner.setAdFrameSize(AdFrameSize.LARGE);
        adgBanner.start();
        adgBanner.setAdListener(
                new ADGListener() {
                    @Override
                    public void onReceiveAd() {
                        Log.v(TAG, "On ReceiveAD ...");
                    }

                    @Override
                    public void onFailedToReceiveAd(ADGErrorCode code) {
                        // 不通とエラー過多のとき以外はリトライ
                        Log.v(TAG, "Error " + code);
                        switch (code) {
                            case EXCEED_LIMIT:
                            case NEED_CONNECTION:
                                break;
                            default:
                                if (adgBanner != null) {
                                    adgBanner.start();
                                }
                                break;
                        }
                    }
                });

        layout.setGravity(Gravity.CENTER | Gravity.TOP);
        layout.addView(adgBanner, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layout;
    }

    public static boolean isJapaneseLanguage(Activity activity) {
        String lang = activity.getResources().getConfiguration().locale.getLanguage();
        if (lang.equals("ja")) {
            Log.d(TAG, "日本語");
            return true;
        }
        return false;
    }
}
