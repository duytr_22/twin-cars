﻿//  GameSample.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.21.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲーム用サンプル
/// </summary>
public class GameSample : MonoBehaviour {

	private int _cubeNum = 0;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start(){
		//全キューブを取得、クリックされた時にイベントを登録。個数も数える。
		foreach (SampleCube sampleCube in transform.GetComponentsInChildren<SampleCube>()) {
			sampleCube.OnClick += OnClickCube;
			_cubeNum++;
		}

		//イベント登録
		TouchEventHandler.Instance.onDragIn3D += OnDrag;
		TouchEventHandler.Instance.onPinch    += OnPinch;
	}

	//=================================================================================
	//内部
	//=================================================================================

	//キューブをクリックした時に呼ばれる
	private void OnClickCube(){
		_cubeNum--;
		if(_cubeNum == 0){
			GameManager.Instance.End (isClear:true);
		}
	}

	//ドラックされた時
	private void OnDrag(Vector3 delta){
		transform.position += delta;
	}

	//ピンチされた時
	private void OnPinch(float delta){
		float scale = Mathf.Clamp(transform.localScale.x + delta * 0.01f, 0, 10);
		transform.localScale = new Vector3 (scale, scale, scale);
	}

}