﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingScaleIpad : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if (SystemInfo.deviceModel.Contains("iPad"))
        {
            Debug.Log("Enter ending cale");
            gameObject.transform.SetLocalScaleY(1.35f);
            gameObject.transform.SetLocalScaleX(1.35f);
        }
    }
}