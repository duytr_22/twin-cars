﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeButtonAssisstant : ButtonAssistant
{

	[SerializeField]
	GameObject _canvas;

	private bool pauseStatus = true;

	protected override void OnClickButton ()
	{
		pauseStatus = !pauseStatus;

		ChangeState (pauseStatus);
	}

	private void ChangeState (bool isPause)
	{
		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial) {

			TutorialManager.Instance.isPause = isPause;

			if (!isPause) {
				Time.timeScale = TutorialManager.Instance.lastTimeScale;
			} else {
				TutorialManager.Instance.lastTimeScale = Time.timeScale;
				Time.timeScale = 0;
			}
		} else {
			Time.timeScale = isPause ? 0 : 1;
		}

		_canvas.SetActive (isPause);

		AdManager.Instance.UpdateAdWithPause (isPause);

		if (isPause) {
			BGMManager.Instance.Pause ();
		} else {
			BGMManager.Instance.Resume ();
		}
	}
}
