package jp.oliviaashley.Advertising;

import java.util.EventListener;

import jp.oliviaashley.Advertising.Providers.IronSourceHelper;

import android.app.Activity;
import android.util.Log;


public class SplashAdManager {

	private static final String TAG = "SplashAdManager";

	private static int AdsCount = 0;

	//インタースティシャル広告 結果通知用リスナー
	public abstract interface SplashAdListener extends EventListener {

			//閉じた時
		public abstract void onClose();
	}

	public static void doOnCreate(Activity activity) {
		CheckSplSpfJson.doOnCreate(activity);
	}

	public static void showOptionalAd(Activity activity, int adCount) {
		SplashAdManager.showOptionalAd(activity, adCount, null);
	}

	public static void showOptionalAd(Activity activity, int adCount, SplashAdListener listener) {
		Log.d(TAG, "showOptionalAd:" + adCount);
		Log.d(TAG, "adSplashFrequency:" + CheckSplSpfJson.getAdSplashFrequency());

//		if (adCount > 3) {

			showIronSouceOptionalAd(activity, listener);
//		}
	}

	public static void showOptionalAdRank(Activity activity, SplashAdListener listener) {
		Log.d(TAG, "showOptionalAdRank");
		Log.d(TAG, "splashTypeRankStr:" + CheckSplSpfJson.getSplashTypeRank());

		int frequencyCount = Integer.parseInt(CheckSplSpfJson.getAdSplashFrequency());

		// 0の時は表示しない
		if (frequencyCount != 0) {

			showIronSouceOptionalAd(activity, listener);
		}
	}

	public static void showIronSouceOptionalAd(Activity activity, SplashAdListener listener) {

		IronSourceHelper.showAd(activity, listener);
	}

	public static boolean isJapaneseLanguage(Activity activity) {
		String lang = activity.getResources().getConfiguration().locale.getLanguage();
		if (lang.equals("ja")) {
			Log.d(TAG, "日本語");
			return true;
		}
		return false;
	}
}
