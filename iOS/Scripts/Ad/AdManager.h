//
//  AdManager.h
//  Unity-iPhone
//
//  Created by kikuchikan on 2015/10/27.
//
//  広告関係の処理を行うクラス

#import "CommonConfig.h"

#import "InterstitialAdController.h"
#import "BannerAdController.h"
#import "RectangleAdController.h"
#import "MovieAdController.h"

//Unity側のオブジェクト名
#define UNITY_OBJECT_NAME "NativeManager"

//Unity側のメソッド名
#define UNITY_ON_INIT_METHOD_NAME "OnInitNative"

@interface AdManager : UIViewController {
    
    //バナー広告管理クラス
    BannerAdController* _bannerAdController;
    BannerAdController* _resultBigBannerAdController;
    
    //インステ広告管理クラス
    InterstitialAdController* _baseInterstitialAdController;
    InterstitialAdController* _rankInterstitialAdController;
    
    RectangleAdController *_pauseRectangleAdController;
    RectangleAdController *_endRectangleAdController;
    RectangleAdController *_gameRectangleAdController;
    RectangleAdController *_popupRectangleAdController;
    
    //動画広告管理クラス
    MovieAdController *_movieAdController;
    
    //GAEの確認が済んだか
    BOOL _isConfirmedGAE;
    
    //自社広告の準備が済んだか
    BOOL _isPreparedHouseAd;
    
    BOOL _isRemoveAds;
    
    BOOL _isFirstTimes;
}

//=================================================================================
//メソッド宣言
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName;


//スプラッシュ広告を表示
-(void)showInterstitialAd:(int)scene playCount:(int)playCount;

//シーンに合わせて広告の更新
-(void) updateAdByScene:(int)scene;

-(void) showGameRect:(BOOL) enable;
-(void) showPopupRect:(BOOL) enable;

-(void) showMovieAd;

-(void) showIndicator:(BOOL) enable;

-(BOOL) canShowMovieAd;

-(void) hideAllAds;

@end
