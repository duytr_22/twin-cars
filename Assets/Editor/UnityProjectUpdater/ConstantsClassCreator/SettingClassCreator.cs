﻿//  SettingClassCreator.cs
//   
//
//  Created by kan.kikuchi on 2015

using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

/// <summary>
/// タグ、レイヤー、シーン名を定数で管理するクラスを自動で作成するスクリプト
/// </summary>
public class SettingClassCreator : AssetPostprocessorEx{

	//returnの値が小さいほど優先される
	public override int GetPostprocessOrder(){
		return 10;
	}

	//=================================================================================
	//検知
	//=================================================================================

	//ProjectSettings以下の設定が編集されたら自動で各スクリプトを作成
	private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths){
		//最初の起動時に実行しないように
		if(!UnityProjectSetting.IsLaunchedProject){
			return;
		}

		List<string[]> assetsList = new List<string[]> (){
			importedAssets
		};

		List<string> targetDirectoryNameList = new List<string> (){
			"ProjectSettings"
		};

		if(ExistsDirectoryInAssets(assetsList, targetDirectoryNameList)){
			Create ();
		}
	}

	//=================================================================================
	//作成
	//=================================================================================

	//スクリプトを作成します
	[MenuItem("Tools/Create/Setting Class")]
	private static void Create(){

		//タグ
		Dictionary<string, string> tagDic = InternalEditorUtility.tags.ToDictionary(value => value);
		ConstantsClassCreator.Create ("TagName", "タグ名を定数で管理するクラス", tagDic);


		//シーン
		Dictionary<string, string> scenesNameDic = new Dictionary<string, string>();
		Dictionary<string, int>    scenesNoDic   = new Dictionary<string, int>();

		//ビルドに含まれているやつのシーンNoを設定
		for(int i = 0; i < EditorBuildSettings.scenes.Count(); i++){
			string sceneName = Path.GetFileNameWithoutExtension (EditorBuildSettings.scenes [i].path);
			scenesNoDic[sceneName] = i;
		}

		//.unityファイルだけ取得し、登録(ビルドに含まれていないのも含む)
		foreach (var guid in AssetDatabase.FindAssets("t:Scene")) {
			var path = AssetDatabase.GUIDToAssetPath (guid);
			string sceneName = System.IO.Path.GetFileNameWithoutExtension (path);

			scenesNameDic[sceneName]  = sceneName;

			//ビルドに含まれていないやつだけ新しく登録
			if(!scenesNoDic.ContainsKey(sceneName)){
				scenesNoDic  [sceneName] = scenesNoDic.Count;
			}
		}

		ConstantsClassCreator.Create ("SceneName", "シーン名を定数で管理するクラス",    scenesNameDic);
		ConstantsClassCreator.Create ("SceneNo"  , "シーン番号を定数で管理するクラス",  scenesNoDic);


		//レイヤーとレイヤーマスク
		Dictionary<string, int> layerNoDic     = InternalEditorUtility.layers.ToDictionary(layer => layer, layer => LayerMask.NameToLayer (layer));
		Dictionary<string, int> layerMaskNoDic = InternalEditorUtility.layers.ToDictionary(layer => layer, layer => 1 <<  LayerMask.NameToLayer (layer));

		ConstantsClassCreator.Create ("LayerNo",     "レイヤー番号を定数で管理するクラス",      layerNoDic);
		ConstantsClassCreator.Create ("LayerMaskNo", "レイヤーマスク番号を定数で管理するクラス", layerMaskNoDic);


		//ソーティングレイヤー
		Dictionary<string, string> sortingLayerDic = GetSortingLayerNames ().ToDictionary(value => value);
		ConstantsClassCreator.Create ("SortingLayerName", "ソーティングレイヤー名を定数で管理するクラス", sortingLayerDic);


		//PlayerSettings
		Dictionary<string, string> playerSettingsDic = new Dictionary<string, string>(){
			{"ProductName"     , PlayerSettings.productName},
			{"BundleVersion"   , PlayerSettings.bundleVersion},
			{"BundleIdentifier", PlayerSettings.applicationIdentifier}
		};
		ConstantsClassCreator.Create ("PlayerSettingsValue", "PlayerSettingsの設定を定数で管理するクラス", playerSettingsDic);


	}

	//sortinglayerの名前一覧を取得
	private static string[] GetSortingLayerNames() {

		Type internalEditorUtilityType = typeof(InternalEditorUtility);
		PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
		return (string[])sortingLayersProperty.GetValue(null, new object[0]);

	}

}