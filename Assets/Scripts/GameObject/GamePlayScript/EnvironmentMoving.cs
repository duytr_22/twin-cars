﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentMoving : MonoBehaviour
{
    [SerializeField] float _partDistance;
    [SerializeField] float _movingSpeed;
    [SerializeField] float _offsetDistanceHelper = 1f;
    // [SerializeField] TimelineSystem _startingTimeline;

    List<GameObject> _partList;
    Vector3 _startPosition;
    int _frontIndex, _backIndex;
    float _distanceMoved;
    int _listCount;
    bool _isStarted;

    // void OnEnable()
    // {
    //     if (_startingTimeline != null)
    //         _startingTimeline.onTimelineBeforeStop += OnPlay;
    // }
    // void OnDisable()
    // {
    //     if (_startingTimeline != null)
    //         _startingTimeline.onTimelineBeforeStop -= OnPlay;
    // }

    public void OnPlay()
    {
        _isStarted = true;
    }

    void Awake()
    {
        _partList = new List<GameObject>();
    }

    void Start()
    {
        Initialize();
    }

    void Initialize()
    {
        foreach (Transform item in transform)
        {
            _partList.Add(item.gameObject);
        }
        // for (int i = 0; i < tf.transform.childCount; i++)
        // {
        //     _partList.Add(tf.transform.GetChild(i).gameObject);
        // }

        _startPosition = transform.position;
        _listCount = _partList.Count;
        _frontIndex = 0;
        _backIndex = 1;

        ActivePart(_frontIndex, true);
        ActivePart(_backIndex, true);
        ActivePart(_backIndex + 1, true);
    }

    void Update()
    {
        if (_isStarted == false || GamePlayScript.Instance.isOver == true)
            return;

        transform.position += Vector3.back * _movingSpeed * Time.deltaTime;

        _distanceMoved = Mathf.Abs(transform.position.z - _startPosition.z);

        var movedIndex = (int)(_distanceMoved / (_partDistance));
        if (movedIndex % _listCount == _backIndex)
            ReArrangePart(_frontIndex);

        _frontIndex = (int)((_distanceMoved / (_partDistance)) % _listCount);
        if (_frontIndex < _listCount - 1)
            _backIndex = _frontIndex + 1;
        else
            _backIndex = 0;

        for (int i = 0; i < _listCount; i++)
        {
            if (i == _backIndex || i == _frontIndex || i == _backIndex + 1)
                ActivePart(i, true);
        }
    }

    private void ReArrangePart(int id)
    {
        Debug.Log("call rearrange");
        var z = _partList[id].transform.localPosition.z + _partDistance * _listCount;
        var pos = _partList[id].transform.localPosition;
        pos.z = z;
        _partList[id].transform.localPosition = pos;
        ActivePart(id, true);
    }

    private void ActivePart(int id, bool active)
    {
        _partList[id].SetActive(active);
    }

    public float GetMoveSpeed()
    {
        return _movingSpeed;
    }

    internal void StopMovingGradually()
    {
        StartCoroutine(DecreaseSpeed());
    }

    private IEnumerator DecreaseSpeed()
    {
        while (_movingSpeed > 0)
        {
            _movingSpeed -= Time.deltaTime * 15f;
            yield return new WaitForEndOfFrame();
        }
        _movingSpeed = 0;
    }

    [ContextMenu("Config Child Position")]
    public void ConfigChildPosition()
    {
        var startPos = new Vector3(0, 0, 0);
        // for (int i = 0; i < transform.childCount; i++)
        // {

        // }
        int i = 0;
        foreach (Transform child in transform)
        {
            child.localPosition = new Vector3(0, 0, startPos.z + i * _partDistance);
            i++;
        }
    }
}
