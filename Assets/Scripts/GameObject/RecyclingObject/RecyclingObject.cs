﻿//  RecyclingObject.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.05.10.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 再利用するオブジェクト
/// </summary>
public class RecyclingObject : MonoBehaviour {

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// 初期化
	/// </summary>
	public virtual void Init(){

	}

	/// <summary>
	/// 解放
	/// </summary>
	public virtual void Release(){

	}

}