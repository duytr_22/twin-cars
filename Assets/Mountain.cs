﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mountain : MonoBehaviour
{

	UISprite _sprite;

	string _name = "main_back_mountain_stage0{0}";

	// Use this for initialization
	void Start ()
	{
		_sprite = GetComponent<UISprite> ();

		int idx = PlayerPrefs.GetInt (Define.STAGE_SELECTED, 1);

		if (idx != 1) {

			_sprite.spriteName = string.Format (_name, idx);
		}
	}
}
