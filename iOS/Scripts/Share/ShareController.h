//
//  ShareController.h
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  シェア関係の処理を行うクラス

#import <Twitter/Twitter.h>

@interface ShareController : UIViewController {
    
}

//=================================================================================
//メソッド宣言
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName;

//Tweet
- (void)tweet:(const char *)text;


@end