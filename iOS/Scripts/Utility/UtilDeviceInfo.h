//
//  UtilDeviceInfo.h
//
//  Copyright © 2016  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilDeviceInfo : NSObject

+(BOOL) isJapaneseLanguage;
+(void) confirmDeviceLanguage;

@end
