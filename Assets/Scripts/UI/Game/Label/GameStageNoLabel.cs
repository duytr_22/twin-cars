﻿//  GameStageNoLabel.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.27.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲームシーンでステージ数を表示するラベル
/// </summary>
[ExecuteInEditMode]
public class GameStageNoLabel : MonoBehaviour {

	//テキストのフォーマット
	[SerializeField]
	private string _textFormat = "{0}";

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start(){
		UpdateText ();
	}

	//=================================================================================
	//更新
	//=================================================================================

	private void UpdateText(){
		GetComponent<Text> ().text = string.Format(_textFormat, CacheData.Entity.StageNo.ToString("D2"));
	}

	#if UNITY_EDITOR
	private void Update () {
		if(Application.isPlaying){
			return;
		}

		//Mode時は表示しないように
		GetComponent<Text> ().enabled = UnityProjectSetting.Entity.GameType == GameType.LevelSelect;
		UpdateText ();
	}
	#endif

}