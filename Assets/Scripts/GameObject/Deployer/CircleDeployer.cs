﻿//  CircleDeployer.cs
//  ProductName うどん
//
//  Created by kan.kikuchi on 2016.01.12.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 子にあるオブジェクトを円状に配置するクラス
/// </summary>
public class CircleDeployer : MonoBehaviour {

	//半径
	[SerializeField]
	private float _radius;

	//=================================================================================
	//初期化
	//=================================================================================

	private void OnValidate (){
		Deploy ();
	}

	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// 子を円状に配置する
	/// </summary>
	[ContextMenu("Deploy")]
	public void Deploy(){

		//子を取得
		List<GameObject> _childList = new List<GameObject> ();
		foreach(Transform child in transform) {
			_childList.Add (child.gameObject);
		}

		//数値、アルファベット順にソート
		_childList.Sort (
			(a, b) => {
				return string.Compare(a.name, b.name);
			}
		);

		//オブジェクト間の角度差
		float angleDiff = 360f / (float)transform.childCount;

		//各オブジェクトを円状に配置
		for (int i = 0; i < _childList.Count; i++)  {
			Vector3 childPostion = transform.position;

			float angle = (90 - angleDiff * i) * Mathf.Deg2Rad;
			childPostion.x += _radius * Mathf.Cos (angle);
			childPostion.y += _radius * Mathf.Sin (angle);

			_childList[i].transform.position = childPostion;
		}

	}

}