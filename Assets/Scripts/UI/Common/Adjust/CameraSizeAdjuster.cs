﻿//  CameraSizeAdjuster.cs
//  ProductName ピンボール
//
//  Created by kan kikuchi on 2015.11.27.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// カメラのサイズをアス比に合わせて調整する
/// </summary>
[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class CameraSizeAdjuster : UIAdjuster {

	//基準のカメラサイズ比
	[SerializeField]
	private float _basisCameraSize = 6;

	//基準のアス比タイプ
	private enum AspectRateType{
		iPhone4, iPhone5
	}
	[SerializeField]
	private AspectRateType _basisAspectRateType = AspectRateType.iPhone5;

	//各タイプ毎のアス比
	private readonly static Dictionary<AspectRateType, float> ASPECT_RATE_DICT = new Dictionary<AspectRateType, float>(){
		{AspectRateType.iPhone4, 640f / 960f}, {AspectRateType.iPhone5, 640f / 1136f}
	};

	//=================================================================================
	//更新
	//=================================================================================

	//カメラのサイズを調整
	protected override void AdjustUI(){
		base.AdjustUI();

		float basisAspectRate = ASPECT_RATE_DICT [_basisAspectRateType];
		GetComponent<Camera> ().orthographicSize = _basisCameraSize * basisAspectRate / CurrentAspectRate;
	}
}