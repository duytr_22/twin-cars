﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugController : MonoBehaviour
{
	[SerializeField] UILabel _environmentText, _playerSkinText, _enemySkinText;

	void OnEnable()
	{
#if !SHOW_DEBUG
        gameObject.SetActive(false);
#endif
	}

	void Start()
	{
		// if (_environmentText != null && EnvironmentController.Instance != null)
		//     _environmentText.text = $"BG { EnvironmentController.Instance.GetBackgroundName() }";
		// if (_playerSkinText != null)
		//     _playerSkinText.text = $"P. { Define.DEBUG_CAR_PATTERN_PLAYER }";
		// if (_enemySkinText != null)
		//     _enemySkinText.text = $"E. { Define.DEBUG_CAR_PATTERN_ENEMY }";
	}

	void Update()
	{
		if (_environmentText != null && EnvironmentController.Instance != null)
			_environmentText.text = $"BG {EnvironmentController.Instance.GetBackgroundName()}";
		if (_playerSkinText != null)
			_playerSkinText.text = $"P. {Define.DEBUG_CAR_PATTERN_PLAYER}";
		if (_enemySkinText != null)
			_enemySkinText.text = $"E. {Define.DEBUG_CAR_PATTERN_ENEMY}";
	}

	public void OnDebugNextBG()
	{
		EnvironmentController.Instance.OnDebugNextBG();
	}

	public void OnDebugPrevBG()
	{
		EnvironmentController.Instance.OnDebugPrevBG();
	}

	public void OnDebugNextPlayerSkin()
	{
		Define.NextPlayerSkin();
		SceneNavigator.Instance.MoveScene(SceneName.TITLE);
	}

	public void OnDebugPrevPlayerSkin()
	{
		Define.PrevPlayerSkin();
		SceneNavigator.Instance.MoveScene(SceneName.TITLE);
	}

	public void OnDebugNextEnemySkin()
	{
		Define.NextEnemySkin();
		// SceneNavigator.Instance.MoveScene(SceneName.TITLE);
	}

	public void OnDebugPrevEnemySkin()
	{
		Define.PrevEnemySkin();
		// SceneNavigator.Instance.MoveScene(SceneName.TITLE);
	}
}