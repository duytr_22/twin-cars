﻿using UnityEngine;
using System.Collections;

public class BackCameraScript : MonoBehaviour {

	private bool reverseSetting;

	// Use this for initialization
	void Start () {
	
	}

	void OnPreRender() {
		reverseSetting = RenderSettings.fog;
		RenderSettings.fog = true;
	}

	void OnPostRender() {
		RenderSettings.fog = reverseSetting;
	}
}
