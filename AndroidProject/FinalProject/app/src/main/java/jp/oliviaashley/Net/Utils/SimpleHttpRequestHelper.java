package jp.oliviaashley.Net.Utils;

import android.os.StrictMode;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by aaron on 16/5/16.
 */
public class SimpleHttpRequestHelper {

    private static final String TAG = "SimpleHttpHelper";


    public static JSONObject getJSONObject(String url) {
        String response = getString(url);
        JSONObject rootObj = null;
        if (response != null) {
            try {
                rootObj = new JSONObject(response);
            } catch (JSONException e) {
                Log.e(TAG, "JSONException:" + e.toString());
            }
        }

        return rootObj;
    }


    public static String getString(String url) {
        // Android3.0からの端末に必須になる、StrictModeの設定(StrictModeクラスは、Android2.3以降に存在)
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .permitAll().build());

        String result = null;
        URL u;
        try {
            u = new URL(url);
            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) u.openConnection();

                int responseCode = httpURLConnection.getResponseCode();

                switch (responseCode) {
                    case HttpURLConnection.HTTP_OK:
                        BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        String inputLine;
                        StringBuilder response = new StringBuilder();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        result = response.toString();
                        break;
                    default:
                        Log.d(TAG, "httpURLConnection.getResponseCode() > " + responseCode);
                }
            } catch (ProtocolException e) {
                Log.e(TAG, "ProtocolException:" + e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, "IOException:" + e.toString());
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null)
                    httpURLConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException:" + e.toString());
            e.printStackTrace();
        }

        return  result;
    }
}
