//
//  FiveRectangleAdController.m
//  Unity-iPhone
//
//
//

#import "AdGenerationRectangleAdController.h"
#import "CommonConfig.h"
#import <ADG/ADGSettings.h>
#import <UIKit/UIKit.h>

@implementation AdGenerationRectangleAdController

//レクタングルアドを初期化
-(UIView *)prepareRectangleAd:(CGRect)frame {
    
    //手順1
    NSDictionary *adgparam = @{
                               @"locationid" : ADG_ID_RECTANGLE, //管理画面から払い出された広告枠ID
                               @"adtype" : @(kADG_AdType_Rect), //枠サイズ(kADG_AdType_Sp：320x50, kADG_AdType_Large:320x100, kADG_AdType_Rect:300x250, kADG_AdType_Tablet:728x90, kADG_AdType_Free:自由設定)
                               @"originx" : @(0), //広告枠設置起点のx座標
                               @"originy" : @(0), //広告枠設置起点のy座標
                               @"w" : @(0), //広告枠横幅（kADG_AdType_Freeのとき有効）
                               @"h" : @(0)  //広告枠高さ（kADG_AdType_Freeのとき有効）
                               };
    
    if (isEndRect) {
    
        adgparam = @{
                     @"locationid" : ADG_ID_RECTANGLE_END, //管理画面から払い出された広告枠ID
                     @"adtype" : @(kADG_AdType_Rect), //枠サイズ(kADG_AdType_Sp：320x50, kADG_AdType_Large:320x100, kADG_AdType_Rect:300x250, kADG_AdType_Tablet:728x90, kADG_AdType_Free:自由設定)
                     @"originx" : @(0), //広告枠設置起点のx座標
                     @"originy" : @(0), //広告枠設置起点のy座標
                     @"w" : @(0), //広告枠横幅（kADG_AdType_Freeのとき有効）
                     @"h" : @(0)  //広告枠高さ（kADG_AdType_Freeのとき有効）
                     };
    }
    
    if(isAdmob) {
        
        adgparam = @{
                     @"locationid" : ADG_ID_RECTANGLE_GAME, //管理画面から払い出された広告枠ID
                     @"adtype" : @(kADG_AdType_Rect), //枠サイズ(kADG_AdType_Sp：320x50, kADG_AdType_Large:320x100, kADG_AdType_Rect:300x250, kADG_AdType_Tablet:728x90, kADG_AdType_Free:自由設定)
                     @"originx" : @(0), //広告枠設置起点のx座標
                     @"originy" : @(0), //広告枠設置起点のy座標
                     @"w" : @(0), //広告枠横幅（kADG_AdType_Freeのとき有効）
                     @"h" : @(0)  //広告枠高さ（kADG_AdType_Freeのとき有効）
                     };
    }
    
    if(isPopup) {
        
        adgparam = @{
                     @"locationid" : ADG_ID_RECTANGLE_POPUP, //管理画面から払い出された広告枠ID
                     @"adtype" : @(kADG_AdType_Rect), //枠サイズ(kADG_AdType_Sp：320x50, kADG_AdType_Large:320x100, kADG_AdType_Rect:300x250, kADG_AdType_Tablet:728x90, kADG_AdType_Free:自由設定)
                     @"originx" : @(0), //広告枠設置起点のx座標
                     @"originy" : @(0), //広告枠設置起点のy座標
                     @"w" : @(0), //広告枠横幅（kADG_AdType_Freeのとき有効）
                     @"h" : @(0)  //広告枠高さ（kADG_AdType_Freeのとき有効）
                     };
    }
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(UnityGetGLViewController().view.frame.size.width/2-150, 0, 300, 250)];
    view.backgroundColor = [UIColor clearColor];
    
    [ADGSettings setGeolocationEnabled:false];
    
    ADGManagerViewController *adgvc = [[ADGManagerViewController alloc] initWithAdParams:adgparam adView:view];// adViewには広告を表示する画面のUIViewインスタンスをセットする。
    self.banner = adgvc;
    self.banner.rootViewController = UnityGetGLViewController();
    self.banner.delegate = self;
    [self.banner setFillerRetry:NO];
    [self.banner loadRequest];
    
    return view;
}

-(void) reloadRectangleAd {
    [self.banner updateView];
}

-(void) removeRectangleAd {
    
}

- (void)ADGManagerViewControllerReceiveAd:(ADGManagerViewController *)adgManagerViewController
{
    NSLog(@"%@", @"ADGManagerViewControllerReceiveAd");
}

// エラー時のリトライは特段の理由がない限り必ず記述するようにしてください。
- (void)ADGManagerViewControllerFailedToReceiveAd:(ADGManagerViewController *)adgManagerViewController code:(kADGErrorCode)code {
    
    NSLog(@"%@", @"ADGManagerViewControllerFailedToReceiveAd");
    
    switch (code) {
        case kADGErrorCodeExceedLimit:
        case kADGErrorCodeNeedConnection:
        case kADGErrorCodeNoAd:
            break;
        default:
            [adgManagerViewController loadRequest];
            break;
    }
}

- (void)ADGManagerViewControllerDidTapAd:(ADGManagerViewController *)adgManagerViewController{
    NSLog(@"%@", @"ADGManagerViewControllerDidTapAd");
}

@end
