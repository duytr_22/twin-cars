﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// SE関連の管理をするクラス
/// </summary>
public class SEManager : AudioManager<SEManager>
{

	//=================================================================================
	//初期化
	//=================================================================================
	protected override void Init ()
	{
		_audioDirectoryPath = ResourcesDirectoryPath.AUDIO_SE;
		_audioSourceNum = 30;
		_volumeDefult = 1.0f;
		_isLoop = false;

		base.Init ();
	}

	//=================================================================================
	//SE再生
	//=================================================================================

	/// <summary>
	/// 指定したファイル名のSEを流す。第二引数のdelayに指定した時間だけ再生までの間隔を空ける
	/// </summary>
	public void PlaySE (string seName, float delay = 0.0f, float pitch = 1.0f, float volume = 1.0f)
	{
		return;

		if (string.IsNullOrEmpty (seName)) {
			Debug.Log ("SE名空です。");
			return;
		}
		if (!_audoClipDic.ContainsKey (seName)) {
			Debug.Log (seName + "という名前のSEがありません");
			return;
		}

		//ディレイが0の時に遅延実行すると同時に複数のSEを流せなくなるためInvokeを使わ無いように
		if (delay == 0) {
			PlayDelaySE (seName, pitch, volume);
		} else {
			StartCoroutine (this.DelayMethod (delay, PlayDelaySE, seName, pitch, volume));
		}

	}

	//SEを遅延実行
	private void PlayDelaySE (string seName, float pitch, float volume)
	{
		if (_isMute) {
			return;
		}

		foreach (AudioSource seSource in _audioSourceList) {
			if (seSource.isPlaying) {
				continue;
			}
			Play (seSource, pitch, volume, seName);
			return;
		}

	}

	//SEを鳴らす
	private void Play (AudioSource seSource, float pitch, float volume, string seName)
	{
		return;
		seSource.pitch = pitch;
		seSource.clip = _audoClipDic [seName] as AudioClip;
		seSource.volume = volume;
		seSource.Play ();
	}

	//=================================================================================
	//SE停止
	//=================================================================================

	/// <summary>
	/// 指定した名前のSEを止める
	/// </summary>
	public void StopSE (string seName)
	{
		foreach (AudioSource seSource in _audioSourceList) {
			if (!seSource.clip) {
				continue;
			}

			if (seSource.clip.name == seName) {
				seSource.Stop ();
			}
		}
	}


	//=================================================================================
	//継承先で実装
	//=================================================================================
	protected override bool IsBGM ()
	{
		return false;
	}

	public void muteAll ()
	{
		base.muteAll ();
	}

	public void resumeAll ()
	{
		base.resumeAll ();
	}
}