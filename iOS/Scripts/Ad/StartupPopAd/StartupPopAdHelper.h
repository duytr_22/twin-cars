//
//  StartupPopAdHelper.h
//  Unity-iPhone
//
//  Created by kikuchikan on 2015/10/29.
//
//  起動時のポップアップ広告を管理する


@interface StartupPopAdHelper : NSObject;

-(id)init;
-(void)showStartupPopAd;

@end
