﻿//  HelpButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class HelpButtonAssistant : ButtonAssistant
{

	protected override void OnClickButton ()
	{
		SceneNavigator.Instance.MoveScene (SceneName.TUTORIAL);
	}

}