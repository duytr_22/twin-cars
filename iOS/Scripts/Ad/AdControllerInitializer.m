//
//  AdControllerInitializer.m
//  Unity-iPhone
//
//  Created by kikuchikan on 2015/10/29.
//
//  各広告のコントローラーを初期化するクラス

#import "AdControllerInitializer.h"
#import "AdManager.h"

#import "IronSourceBannerHelper.h"
#import "AdGenerationBannerHelper.h"
#import "AdGenerationBigBannerHelper.h"

#import "FiveRectangleHelper.h"
#import "AdmobRectangleHelper.h"
#import "AdGenerationRectangleAdController.h"

#import "IronSouceInterstitialAdController.h"

#import "IronSourceMovieAdController.h"

#import "MoPubBannerHelper.h"
#import "MoPubInterstitialAdController.h"

#import "MoPubMovieAdController.h"

#import "UtilDeviceInfo.h"

@implementation AdControllerInitializer

//=================================================================================
//インステ広告コントローラー初期化
//=================================================================================

+(BannerAdController*)initBannerAdController{
    
    BannerAdController *bannerAdController = bannerAdController = [[MoPubBannerHelper alloc] init];
    
    return bannerAdController;
}

+(BannerAdController*)initResultBigBannerAdController{
    
    BannerAdController *bannerAdController = nil;
    
    bannerAdController = [[AdGenerationBigBannerHelper alloc] init];
    
    return bannerAdController;
}

//=================================================================================
//インステ広告コントローラー初期化
//=================================================================================

+(InterstitialAdController*)initInterstitialAdController:(NSString*)interstitialType{
    InterstitialAdController* interstitialAdController = [[MoPubInterstitialAdController alloc] init];
    return interstitialAdController;
}

//エンディングで表示するインステ用コントローラー初期化
+(InterstitialAdController*)initBaseInterstitialAdController{
    InterstitialAdController* baseInterstitialAdController = [self initInterstitialAdController:[GAEConfig getSplashType]];
    [baseInterstitialAdController setRankType:NO];
    return baseInterstitialAdController;
}

//タイトルとランキングボタンを押した時に表示するインステ用コントローラー初期化
+(InterstitialAdController*)initRankInterstitialAdController{
    InterstitialAdController* rankInterstitialAdController = [self initInterstitialAdController:[GAEConfig getSplashTypeRank]];
    [rankInterstitialAdController setRankType:YES];
    return rankInterstitialAdController;
}

+(RectangleAdController*)initRectangleAdController:(NSString*)rectangleType isBottom:(BOOL)isBottom{
    RectangleAdController* rectangleAdController = nil;
    
    if ([UtilDeviceInfo isJapaneseLanguage]) {
        
        rectangleAdController = [[AdGenerationRectangleAdController alloc] init:isBottom isNative:NO isAdmob:NO isPopup:false];
        
        NSLog(@"rectangleType %@, rectangleAdController %@", rectangleType, rectangleAdController);
    } else {
        
        // Admob
        rectangleAdController = [[AdmobRectangleHelper alloc] init:isBottom isNative:NO isAdmob:NO isPopup:false];
    }
    
    return rectangleAdController;
}

//ロードで表示するレクタングル用コントローラー初期化
+(RectangleAdController*)initPauseRectangleAdController{
    return [self initRectangleAdController:[GAEConfig getRectangleType] isBottom:NO];
}

//エンディングで表示するレクタングル用コントローラー初期化
+(RectangleAdController*)initEndRectangleAdController{
    
    RectangleAdController* rectangleAdController = nil;
    
    if ([UtilDeviceInfo isJapaneseLanguage]) {
        
        rectangleAdController = [[AdGenerationRectangleAdController alloc] init:YES isNative:NO isAdmob: YES isPopup:false];
        
    } else {
        
        rectangleAdController = [[AdmobRectangleHelper alloc] init:YES isNative:NO isAdmob: YES isPopup:false];
    }
    
    return rectangleAdController;
}

+(RectangleAdController*)initGameRectangleAdController{
    
    RectangleAdController* rectangleAdController = nil;
    
    if ([UtilDeviceInfo isJapaneseLanguage]) {
        
        rectangleAdController = [[FiveRectangleHelper alloc] init:NO isNative:NO isAdmob: YES isPopup:false];
        
    } else {
        
        rectangleAdController = [[AdmobRectangleHelper alloc] init:NO isNative:NO isAdmob: YES isPopup:false];
    }
    
    return rectangleAdController;
}

+(RectangleAdController*)initPopupRectangleAdController{
    
    RectangleAdController* rectangleAdController = nil;
    
    if ([UtilDeviceInfo isJapaneseLanguage]) {
        
        rectangleAdController = [[FiveRectangleHelper alloc] init:NO isNative:NO isAdmob: YES isPopup:true];
        
    } else {
        
        rectangleAdController = [[AdmobRectangleHelper alloc] init:NO isNative:NO isAdmob: YES isPopup:true];
    }
    
    return rectangleAdController;
}

//=================================================================================
//動画広告コントローラー初期化
//=================================================================================

+(MovieAdController*)initMovieAdController;{
        
    MovieAdController *movieAdController = [[MoPubMovieAdController alloc] init];

    return movieAdController;
}

@end
