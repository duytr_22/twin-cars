﻿//  SoundButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ミュート切り替えボタンのアシスタントクラス
/// </summary>
public class SoundButtonAssistant : ButtonAssistant
{
	void Start ()
	{
		UpdateUI ();
	}

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{
		//先にミュート設定
		UserData.IsMute = !UserData.IsMute;

		UpdateUI ();
	}

	void UpdateUI ()
	{
		if (!UserData.IsMute) {

			_image.spriteName = "title_btn_sound_on";

		} else {

			_image.spriteName = "title_btn_sound_off";
		}

		_button.normalSprite = _image.spriteName;
		_button.hoverSprite = _image.spriteName;
		_button.pressedSprite = _image.spriteName;
		_button.disabledSprite = _image.spriteName;
	}
}