//
//  MessageBox.m
//  Unity-iPhone
//
//  Created  on 2015/11/02.
//
//  メッセージを表示する便利クラス

#import "MessageBox.h"

@implementation MessageBox

//メッセージを表示
+ (void)Show:(NSString *)title message:(NSString*)message buttonTitle:(NSString*)buttonTitle{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:buttonTitle
                                          otherButtonTitles:nil];
    [alert show];
}


@end