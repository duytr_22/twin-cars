﻿//  DeveloperInfoSetter.cs
//  ProductName BreakBall
//
//  Created by kikuchikan on 2015.08.31.

using UnityEngine;
using UnityEditor;

/// <summary>
/// 開発者情報クラス
/// </summary>
public static class DeveloperInfo{

	private static string _developerName;

	//保存key
	private const string DEVELOPER_NAME_KEY = "DeveloperName";

	//=================================================================================
	//表示するGUIの設定
	//=================================================================================

	[PreferenceItem("Developer Info")]
	private static void OnPreferenceGUI (){

		//内容の変更チェック開始
		EditorGUI.BeginChangeCheck ();

		//開発者名
		GUILayout.Label("Name", EditorStyles.boldLabel);

		_developerName = GetDeveloperName ();
		_developerName = GUILayout.TextField(_developerName);

		//内容が変更されていれば保存
		if (EditorGUI.EndChangeCheck ()){
			SetDeveloperName (_developerName);
		}

	}

	//=================================================================================
	//情報設定
	//=================================================================================

	/// <summary>
	/// 開発者を設定
	/// </summary>
	public static void SetDeveloperName(string developerName){
		EditorPrefs.SetString (DEVELOPER_NAME_KEY, developerName);
	}

	//=================================================================================
	//情報取得
	//=================================================================================

	/// <summary>
	/// 開発者名を取得
	/// </summary>
	public static string GetDeveloperName(){
		return EditorPrefs.GetString (DEVELOPER_NAME_KEY, "");
	}

}
