//
//  AdPlugin.mm
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  Unity側とのラ広告関連で連携を行うプラグイン

#import "AdManager.h"
#import "UnityAppController.h"
#import "RankingController.h"

//=================================================================================
//Unity側との連携
//=================================================================================


extern "C" {
    
    //初期化、ネイティブ側のプラグインのインスタンスを作成し、Unity側へ渡す
    void *_AdPlugin_Init(const char *gameObjectName){
        id instance = [[AdManager alloc]initWithGameObjectName:gameObjectName];
        
        CFRetain((CFTypeRef)instance);
        return (__bridge void *)instance;
    }
    
    //シーンに合わせて広告の更新
    void _UpdateAdByScene(void *instance, int scene){
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager updateAdByScene:scene];
    }
    
    //スプラッシュ広告を表示
    void _ShowSplashAd(void *instance, int scene, int playCount){
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showInterstitialAd:scene playCount:playCount];
    }
    
    //リーダーボードを表示した時に広告を重ねる
    void _AddAdToLeaderBoard(void *instance){
        
    }
    
    void _PurchaseNoAds(void *instance){
        
    }
    
    void _ShowRewardVideo(void *instance){
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showMovieAd];
    }
    
    bool _CanShowRewardVideo (void *instance){
     
        AdManager *adManager = (__bridge AdManager *)instance;
        return [adManager canShowMovieAd];
    }
    
    void _HideGameRect(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showGameRect:false];
    }
    
    void _ShowGameRect(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showGameRect:true];
    }
    
    void _HideIndicator(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showIndicator:false];
    }
    
    void _ShowIndicator(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showIndicator:true];
    }
    
    void _HideAllAds(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager hideAllAds];
    }
    
    void _HidePopupRect(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showPopupRect:false];
        
        [adManager updateAdByScene:SCENE_RESULT];
    }
    
    void _ShowPopupRect(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showPopupRect:true];
        
        [adManager updateAdByScene:-1];
    }
}
