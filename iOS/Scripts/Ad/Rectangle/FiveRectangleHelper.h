//
//  ImobileRectangleHelper.h
//
//  Created on 14/10/2014.
//
//

#import "RectangleAdController.h"
#import <FiveAd/FiveAd.h>

@interface FiveRectangleHelper : RectangleAdController <FADDelegate>{
    
}

+ (void) startFive;

-(UIView *)prepareRectangleAd:(CGRect)frame;

@property(nonatomic, strong) FADAdViewW320H180 * fiveViewTop;
@property(nonatomic, strong) FADAdViewW320H180 * fiveViewBottom;
@property(nonatomic, assign) CGRect frameTop;
@property(nonatomic, assign) CGRect frameBottom;
@property(nonatomic, strong) NSTimer* time1;
@property(nonatomic, strong) NSTimer* time2;
@property(nonatomic, assign) int count1;
@property(nonatomic, assign) int count2;

@end
