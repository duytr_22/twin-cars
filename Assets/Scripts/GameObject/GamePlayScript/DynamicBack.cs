﻿//  DynamicBack.cs
//  ProductName ゴール神3D
//
//  Created by kan.kikuchi on 2016.02.10.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 動的に動く背景
/// </summary>
public class DynamicBack : MonoBehaviour
{

	private const float ANIMATION_TIME = 2.50f;

	//子
	private GameObject _childObject;

	//スプライト
	private UISprite _sprite;

	public  UISprite  Sprite {
		get{ return _sprite; }
	}

	//アニメーション
	private TweenPosition _tweenPosition;
	private TweenAlpha _tweenAlpha;
	private TweenScale _tweenScale;
	private TweenColor _tweenColor;

	[HideInInspector] public bool _isLeft;
	[HideInInspector] public float _delay;

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init (float factor, bool isLeft, float delay, bool isContinue = false)
	{
		_isLeft = isLeft;
		_delay = delay;

		this.transform.localScale = new Vector3 (1, 1, 1);

		_childObject = transform.GetChild (0).gameObject;

		_sprite = _childObject.GetComponent<UISprite> ();
		_sprite.MakePixelPerfect ();
		_sprite.depth = Random.Range (100, 120);

		_tweenPosition = GetComponent<TweenPosition> ();
		_tweenAlpha = _childObject.GetComponent<TweenAlpha> ();
		_tweenScale = _childObject.GetComponent<TweenScale> ();
		_tweenColor = _childObject.GetComponent<TweenColor> ();

		int index = BackgroundScript.Instance.currentColorIndex;

		if (isContinue && PlayerPrefs.GetInt (Define.CONTINUE_GAME, 0) == 1 && SceneNavigator.Instance.CurrentSceneName == SceneName.GAME) {
			
			index = PlayerPrefs.GetInt (Define.CURRENT_COLOR_INDEX, 0);
		}

		_tweenColor.from = BackgroundScript.Instance.mStartColors [index];
		_tweenColor.to = BackgroundScript.Instance.mEndColors [index];

		_tweenColor.duration = _tweenScale.duration;
		_tweenColor.animationCurve = _tweenScale.animationCurve;

		_sprite.alpha -= _tweenAlpha.from;

		_childObject.transform.localScale = _tweenScale.from;

		_sprite.color = _tweenColor.from;

		//ポジション設定
		SetTweenPosition (_childObject, isLeft);

		//遅延時間の設定
		SetDelay (gameObject, delay);

		//Set Factor
		_childObject.GetComponent<TweenPosition> ().tweenFactor = factor;
		_tweenPosition.tweenFactor = factor;
		_tweenAlpha.tweenFactor = factor;
		_tweenScale.tweenFactor = factor;
		_tweenColor.tweenFactor = factor;

//		BackgroundScript.Instance.OnMountainChangeColor += OnChangeColor;
	}

	public void ChangeTweenColorInMiddle ()
	{
		int index = BackgroundScript.Instance.currentColorIndex;

		_tweenColor.animationCurve = _tweenAlpha.animationCurve;
		_tweenColor.duration = _tweenColor.duration * (1f - _tweenColor.tweenFactor) * 1.2f;
		_tweenColor.from = _sprite.color;
		_tweenColor.to = BackgroundScript.Instance.mEndColors [index];
		_tweenColor.ResetToBeginning ();
	}

	public void OnTweenFinished ()
	{
		Destroy (this.gameObject);
	}

	//移動アニメーションの設定
	private void SetTweenPosition (GameObject target, bool isLeft)
	{
		TweenPosition tweenPosition = target.GetComponent<TweenPosition> ();
		float adjustY = -170 + Random.Range (0, 10);

		//アニメーション開始地点の決定
		Vector3 startPositon = tweenPosition.from;
		if (!isLeft) {
			startPositon.x *= -1;
		}
		startPositon.x += 0;
		startPositon.y += adjustY;

		//アニメーションの終了地点の決定
		Vector3 moveDistance = tweenPosition.to - tweenPosition.from;
		Vector3 endPostion = startPositon;

		if (isLeft) {
			endPostion += moveDistance;
		} else {
			endPostion -= moveDistance;
		}
		endPostion.x += Random.Range (40, 100) * Random.Range (0, 2) == 0 ? -1 : 1;
		endPostion.y -= adjustY * 4f;

		//オブジェクトの位置とアニメーションの位置を設定
		target.transform.localPosition = startPositon;
		tweenPosition.from = startPositon;
		tweenPosition.to = endPostion;
	}

	//遅延時間を設定する
	private void SetDelay (GameObject target, float delay)
	{
		foreach (UITweener tween in target.GetComponents<UITweener>()) {
			tween.delay = delay;
			tween.duration = ANIMATION_TIME;
			tween.ResetToBeginning ();
			tween.PlayForward ();
		}

		foreach (Transform child in target.transform) {
			SetDelay (child.gameObject, delay);
		}
	}

	//=================================================================================
	//更新
	//=================================================================================

	private void Update ()
	{
		//深度を変える
		_sprite.depth = (int)(_tweenPosition.tweenFactor * 1000);
		if (GameManager.Instance.playMode != GameManager.PlayMode.PlayModeTitle) {
			if (GameManager.Instance.IsPlayingGame) {
				this.StartTween ();
			} else {
				this.StopTween ();
			}
		}
	}

	//	private void OnChangeColor (Color color) {
	//		Color c = color;
	//		c.a = _sprite.alpha;
	//		_sprite.color = c;
	//	}

	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// スプライトを更新する
	/// </summary>
	public void UpdateUISprite (int spriteTypeNo)
	{
		//動的な背景の画像更新
		string dynamicBackSpriteName = string.Format ("main_back_{0}_fore", spriteTypeNo);
		_sprite.spriteName = dynamicBackSpriteName;
		_sprite.MakePixelPerfect ();
	}

	/// <summary>
	/// 動的背景を止める
	/// </summary>
	public void StopTween ()
	{
		foreach (UITweener tween in gameObject.GetComponents<UITweener>()) {
			tween.enabled = false;
		}
		foreach (UITweener tween in _childObject.GetComponents<UITweener>()) {
			tween.enabled = false;
		}
	}

	public void StartTween ()
	{
		foreach (UITweener tween in gameObject.GetComponents<UITweener>()) {
			tween.enabled = true;
		}
		foreach (UITweener tween in _childObject.GetComponents<UITweener>()) {
			tween.enabled = true;
		}
	}
}