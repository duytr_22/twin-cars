﻿//  ResultTweetButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// リザルトシーンでのツイートボタンのアシスタントクラス
/// </summary>
public class TweetButtonAssistant : ButtonAssistant
{

	[SerializeField]
	private ShareLocation _location;

	bool _canTap = false;

	//=================================================================================
	//内部
	//=================================================================================

	IEnumerator Start ()
	{
		if (UserData.PlayCount % 3 == 0)
			yield return new WaitForSeconds (0.75f);
		else
			yield return new WaitForSeconds (0.0f);

		_canTap = true;
	}

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{
		if (_canTap) {

			ShareManager.Instance.Tweet (_location);
		}
	}

}