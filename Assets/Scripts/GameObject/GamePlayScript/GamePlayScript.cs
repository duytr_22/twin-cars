﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public struct SpeedBalance
{
	public int score;
	public float speed;
	public float duration;
}

[System.Serializable]
public struct SpawnDistance
{
	public int score;
	public float min;
	public float max;
}

public class GamePlayScript : MonoBehaviour
{
	private static GamePlayScript instance;

	public static GamePlayScript Instance
	{
		get { return instance; }
	}

	private void Awake()
	{
		if (instance == null)
			instance = this;
	}

	//spawn point
	public GameObject spawnPointCube;
	[HideInInspector] public SpawnPointScript spawnPointScript;

	// spawn distance
	private float spawnDistance;
	private float distanceCount;
	private bool isRandomSpawn;

	//speed up
	private bool isSpeedUp;
	private float nextScrollSpeed;
	private float nextScrollSpeedDuration;

	//over
	public float maxDieCountDown;
	[HideInInspector] public bool isOver;
	[HideInInspector] public bool isDelayOver;

	//camera transform
	private Transform camTransform;
	private Vector3 camLocalPos;


	// balance
	public float scrollSpeed;

	[Space(10)] [Header("====Spawn Distance ====")] [Space(0)]
	public List<SpeedBalance> speedBalance;

	[Space(10)] [Header("====Spawn Distance ====")] [Space(0)]
	public List<SpawnDistance> spawnDistanceData;

	public float totalPlayTime;
	public bool isStartedGame;

	[SerializeField] TimelineSystem _startingTimeline;
	[SerializeField] ParticleSystem _vfxSpeed;
	float _startGameTime;

	// Use this for initialization
	void Start()
	{
		spawnPointScript = spawnPointCube.GetComponent<SpawnPointScript>();

		if (GameManager.Instance.playMode != GameManager.PlayMode.PlayModeTitle)
		{
			TimelineSystem.onTimelineCompleted += () =>
			{
				camLocalPos = Camera.main.transform.position;
				if (Define.IsShowTutorial())
					UIGamePlay.Instance.ShowTutorial(null);

				_startGameTime = Time.time;
				isStartedGame = true;
			};
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (isOver || Time.timeScale == 0 || isStartedGame == false)
		{
			return;
		}

		if (!isDelayOver)
		{
			totalPlayTime = (Time.time - _startGameTime) * 10;
			UIGamePlay.Instance.UpdateTimeText(totalPlayTime);
			this.UpdateSpeed();
			this.UpdateSpawnCube();
		}
	}

	void UpdateSpawnCube()
	{
		// spawn calculation
		if (GameManager.Instance.playMode != GameManager.PlayMode.PlayModeTutorial)
		{
			if (isRandomSpawn)
			{
				// couting the distance and check if it is up then spawn a cubes of line
				distanceCount += scrollSpeed * Time.deltaTime;
				if (distanceCount >= spawnDistance)
				{
					SpawnPointScript script = this.GetComponentInChildren<SpawnPointScript>();
					if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeGame)
					{
						script.SpawnLineOfCube();
					}
					else
					{
						script.TitleSpawnLineOfCube();
					}

					isRandomSpawn = false;
				}
			}
			else
			{
				isRandomSpawn = true;
				distanceCount = 0;
				int score = GameManager.Instance._score;
				SpawnDistance sd = new SpawnDistance();

				// pick up the right data with score
				if (score < spawnDistanceData[spawnDistanceData.Count - 1].score)
				{
					for (int i = spawnDistanceData.Count - 1; i >= 0; i--)
					{
						sd = spawnDistanceData[i];
						if (score >= sd.score)
						{
							break;
						}
					}
				}
				else
				{
					sd = spawnDistanceData[spawnDistanceData.Count - 1];
				}

				//random the next distance to spawn another cubes of line
				float min = sd.min;
				float max = sd.max;
				spawnDistance = UnityEngine.Random.Range(min, max);
			}
		}
		else
		{
			// couting the distance and check if it is up then spawn a cubes of line
			distanceCount += scrollSpeed * Time.deltaTime;
			if (distanceCount >= 10)
			{
				distanceCount = 0;
				SpawnPointScript script = this.GetComponentInChildren<SpawnPointScript>();
				script.TutorialSpawnLineOfCube();
			}
		}
	}

	void UpdateSpeed()
	{
		if (isSpeedUp)
		{
			return;
		}

		if (GameManager.Instance._score < speedBalance[speedBalance.Count - 1].score)
		{
			for (int i = speedBalance.Count - 1; i >= 0; i--)
			{
				SpeedBalance sb = speedBalance[i];
				if (GameManager.Instance._score >= sb.score)
				{
					if (scrollSpeed < sb.speed)
					{
						nextScrollSpeed = sb.speed;
						nextScrollSpeedDuration = sb.duration;
						isSpeedUp = true;
						this.StartCoroutine(this.InscreasingSpeed());
					}

					break;
				}
			}
		}
		else
		{
			nextScrollSpeed = speedBalance[speedBalance.Count - 1].speed;
			nextScrollSpeedDuration = speedBalance[speedBalance.Count - 1].duration;
		}
	}

	IEnumerator InscreasingSpeed()
	{
		float t = 0;
		float startSpeed = scrollSpeed;
		float speed = nextScrollSpeed - scrollSpeed;

		while (scrollSpeed < nextScrollSpeed)
		{
			t += Time.deltaTime;

			if (t >= nextScrollSpeedDuration)
			{
				scrollSpeed = nextScrollSpeed;
				isSpeedUp = false;
				yield break;
			}
			else
			{
				float ratio = t / nextScrollSpeedDuration;
				scrollSpeed = startSpeed + speed * ratio;
			}

			yield return null;
		}

		yield return null;
	}

	IEnumerator DecreasingSpeedToZero(float duration)
	{
		float d = duration;
		float t = 0;
		float ratio = 0;
		float startSpeed = scrollSpeed;
		while (scrollSpeed > 0)
		{
			t += Time.deltaTime;

			ratio = t / d;

			scrollSpeed = startSpeed - (startSpeed * ratio * 0.75f);

			if (scrollSpeed < 0)
			{
				scrollSpeed = 0;

				yield break;
			}

			yield return null;
		}

		yield break;
	}

	IEnumerator WaitToStopMoveBackground(float duration)
	{
		bool canContinue = false;

		PlayerPrefs.SetInt(Define.CONTINUE_GAME, 0);

		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeGame
		    && !GameManager.Instance._alreadyContinue)
		{
#if !CHEAT_ADS
			if (AdManager.Instance.CanShowRewardVideo())
			{
				yield return new WaitForSeconds(0.25f);

				GameManager.Instance.ShowContinue();

				canContinue = true;
			}
#endif
		}

		if (!canContinue && GameManager.Instance.playMode == GameManager.PlayMode.PlayModeGame)
		{
			yield return new WaitForSeconds(duration);

			GameManager.Instance.End(false);
		}

		GamePlayScript.Instance.isOver = true;

		// UIBackScript uibs = BackgroundScript.Instance.GetComponent<UIBackScript>();
		// uibs.StopUIBack();

		yield break;
	}

	IEnumerator CameraShake()
	{
		float t = 0;
		while (t <= 0.5f)
		{
			while (Time.timeScale == 0)
			{
				yield return null;
			}

			t += Time.deltaTime;
			Camera.main.transform.localPosition = camLocalPos + UnityEngine.Random.insideUnitSphere * 0.1f;


			yield return null;
		}

		Camera.main.transform.localPosition = camLocalPos;

		yield break;
	}

	internal void StopVFX()
	{
		_vfxSpeed.Stop();
	}

	public void ScrollDelayEndAnimation()
	{
		if (!isDelayOver)
		{
			BGMManager.Instance.StopBGM();
			//			BGMManager.Instance.Pause();

			isDelayOver = true;
			this.StartCoroutine(this.DecreasingSpeedToZero(maxDieCountDown));
			this.StartCoroutine(this.WaitToStopMoveBackground(maxDieCountDown));
		}
	}

	public void ShakingCamera()
	{
		this.StartCoroutine(this.CameraShake());
	}

	public bool PauseCheck(Vector2 touchPoint)
	{
		Rect rect = new Rect();
		rect.x = 16;
		rect.y = Screen.height - (16 + 88);
		rect.width = 88;
		rect.height = 88;

		return rect.Contains(new Vector3(touchPoint.x, touchPoint.y, 0));
	}
}