//
//  PurchaseController.h
//  Unity-iPhone
//
//  Created  on 2015/09/10.
//
//  課金関係の処理を行うクラス

#import <StoreKit/StoreKit.h>

//delegateにSKProductsRequestDelegateとSKPaymentTransactionObserverを追加
@interface PurchaseController : UIViewController <SKProductsRequestDelegate,SKPaymentTransactionObserver> {
    
    
}

//=================================================================================
//定数
//=================================================================================

//Unity側のオブジェクト名
#define UNITY_OBJECT_NAME "NativeManager"

//Unity側のメソッド名
#define UNITY_SET_ITEM_INFO_METHOD_NAME "SetItemInfo"
#define UNITY_PURCHASING_METHOD_NAME    "Purchasing"
#define UNITY_FAILED_METHOD_NAME        "Failed"
#define UNITY_SUCCESS_METHOD_NAME       "Success"

//=================================================================================
//メソッド宣言
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName;


//課金アイテム情報のリクエストを送る
- (void)sendProductsRequest:(const char *)productID isPurchasing:(BOOL)isPurchasing;


@end