﻿//  CommonNativeManager.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.27.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ネイティブ関連の汎用的なマネージャ
/// </summary>
public class CommonNativeManager : SingletonMonoBehaviour<CommonNativeManager>
{

	//ネイティブ連携用プラグイン
	private CommonNativePlugin _commonNativePlugin;
	#if UNITY_ANDROID
	public  AndroidJavaObject  CommonAndroidPlugin{
		get{return _commonNativePlugin.AndroidPlugin;}
	}
	#endif

	//FlurryのKey
	private const string FLURRY_GAME_START_KEY = "T_GAME_START";
	private const string FLURRY_PLAY_COUNT_KEY_FORMAT = "PLAY_COUNT:{0}";
	private const string FLURRY_STAGE_CLEAR_KEY_FORMAT = "STAGE_{0}_CLEAR";

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Init ()
	{
		base.Init ();

		//ネイティブ連携用プラグイン初期化
		_commonNativePlugin = gameObject.AddComponent<CommonNativePlugin> ();

		#if !NO_NAITIVE
		_commonNativePlugin.Init ();
		#endif

		//シーン移動後にBGMを変えるように
		SceneNavigator.Instance.OnLoadedScene += OnLoadedScene;
	}

	//シーン移動後
	private void OnLoadedScene (int level)
	{
		#if !NO_NAITIVE
		Debug.Log ("OnLoadedScene + " + level);

		//ゲームを開始した事をFlurryに伝える
		if (level == SceneNo.GAME) {
			_commonNativePlugin.ReportToFlurry (FLURRY_GAME_START_KEY);
		}
		//ゲームのプレイ回数を増加させFlurryに伝える
		else if (level == SceneNo.RESULT && SceneNavigator.Instance.BeforeSceneName != SceneName.BALL) {			
			UserData.PlayCount++;
			ReportPlayCountToFlurryIfneeded ();

			#if !NO_AD
			//ハイスコア時にはレビュー催促を行う
			if (CacheData.Entity.IsNewRecored) {
				ShowReviewPopUp ();
			}
			#endif
		}

		#if !NO_AD
		//シーンに合わせて広告設定(初回タイトルでは行わない)
		AdManager.Instance.UpdateAdWithScene (SceneNavigator.Instance.CurrentSceneName, true);

		#endif

		#endif
	}

	//=================================================================================
	//内部
	//=================================================================================

	//必要に応じてプレイ回数をFlurryに送信
	private void ReportPlayCountToFlurryIfneeded ()
	{
		int playCount = UserData.PlayCount;

		if (playCount <= 10) {
			_commonNativePlugin.ReportToFlurry (string.Format (FLURRY_PLAY_COUNT_KEY_FORMAT, playCount));
		} else if (playCount <= 100 && playCount % 10 == 0) {
			_commonNativePlugin.ReportToFlurry (string.Format (FLURRY_PLAY_COUNT_KEY_FORMAT, playCount));
		} else if (playCount <= 1000 && playCount % 100 == 0) {
			_commonNativePlugin.ReportToFlurry (string.Format (FLURRY_PLAY_COUNT_KEY_FORMAT, playCount));
		} else if (playCount <= 10000 && playCount % 1000 == 0) {
			_commonNativePlugin.ReportToFlurry (string.Format (FLURRY_PLAY_COUNT_KEY_FORMAT, playCount));
		} else if (playCount <= 100000 && playCount % 10000 == 0) {
			_commonNativePlugin.ReportToFlurry (string.Format (FLURRY_PLAY_COUNT_KEY_FORMAT, playCount));
		}
	}

	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// クリアしたステージのNoを送信
	/// </summary>
	public void ReportClearStageNo (int stageNo)
	{
		#if !NO_NAITIVE
		_commonNativePlugin.ReportToFlurry (string.Format (FLURRY_STAGE_CLEAR_KEY_FORMAT, stageNo));
		#endif
	}

	/// <summary>
	/// アプリ内でAppStoreを開く
	/// </summary>
	public void ShowAppStoreInApp (int id)
	{
		#if !NO_NAITIVE
		_commonNativePlugin.ShowAppStoreInApp (id);
		#endif
	}

	/// <summary>
	/// ウェブページ表示
	/// </summary>
	public void ShowCommonWeb ()
	{
		#if !NO_NAITIVE
		_commonNativePlugin.ShowCommonWeb ();
		#endif
	}

	/// <summary>
	/// レビュー催促を表示する
	/// </summary>
	public void ShowReviewPopUp ()
	{
		#if !NO_NAITIVE
		_commonNativePlugin.ShowReviewPopUp ();
		#endif
	}

	/// <summary>
	/// レビューページを表示する
	/// </summary>
	public void ShowReviewPage ()
	{
		#if !NO_NAITIVE
		_commonNativePlugin.ShowReviewPage ();
		#endif
	}
}