﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// TweenPosition
struct TweenPosition_t1378762002;
// UIPopupList
struct UIPopupList_t4167399471;
// CounterLabel
struct CounterLabel_t1397518286;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t631007953;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>
struct List_1_t1139589975;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UIWrapContent/OnInitializeItem
struct OnInitializeItem_t992046894;
// UnityEngine.Transform
struct Transform_t3600365921;
// UIPanel
struct UIPanel_t1716472341;
// UIScrollView
struct UIScrollView_t1973404950;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t3375297100;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t4210400802;
// UILabel
struct UILabel_t3248798549;
// BetterList`1<TypewriterEffect/FadeEntry>
struct BetterList_1_t4089408747;
// UnityEngine.Collider
struct Collider_t1773347010;
// UIButton
struct UIButton_t1100396938;
// UISprite
struct UISprite_t194114938;
// EventDelegate/Callback
struct Callback_t3139336517;
// System.String[]
struct StringU5BU5D_t1281789340;
// UIWidget
struct UIWidget_t3538521925;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Animation
struct Animation_t3648466861;
// UnityEngine.Animator
struct Animator_t434523843;
// UITweener[]
struct UITweenerU5BU5D_t3440034099;
// BetterList`1<UIKeyNavigation>
struct BetterList_1_t3399976721;
// UIDraggableCamera
struct UIDraggableCamera_t1644204495;
// UnityEngine.Camera
struct Camera_t4157153871;
// UIRoot
struct UIRoot_t4022971450;
// SpringPanel/OnFinished
struct OnFinished_t3778785451;
// UICenterOnChild/OnCenterCallback
struct OnCenterCallback_t2077531662;
// System.Collections.Generic.List`1<UIKeyBinding>
struct List_1_t4170520585;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UIGrid
struct UIGrid_t1536638187;
// UITable
struct UITable_t3168834800;
// UIDragScrollView
struct UIDragScrollView_t2492060641;
// UICamera/MouseOrTouch
struct MouseOrTouch_t3052596533;
// System.Collections.Generic.List`1<UIDragDropItem>
struct List_1_t1099956827;
// UIToggle
struct UIToggle_t4192126258;
// UIProgressBar
struct UIProgressBar_t1222110469;
// UIRect
struct UIRect_t2875960382;
// BetterList`1<UIScrollView>
struct BetterList_1_t1128425268;
// UIScrollView/OnDragNotification
struct OnDragNotification_t1437737811;
// UICenterOnChild
struct UICenterOnChild_t253063637;
// UIGrid/OnReposition
struct OnReposition_t1372889220;
// UITable/OnReposition
struct OnReposition_t3913508630;
// UIAtlas
struct UIAtlas_t3195533529;
// UIFont
struct UIFont_t2766063701;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// UIBasicSprite
struct UIBasicSprite_t1521297657;
// System.Collections.Generic.List`1<UILabel>
struct List_1_t425905995;
// UIPopupList/LegacyEvent
struct LegacyEvent_t2749056879;
// IAPController
struct IAPController_t1855063710;
// BallController
struct BallController_t2992829471;
// UIProgressBar/OnDragFinished
struct OnDragFinished_t3715779777;
// BetterList`1<UIToggle>
struct BetterList_1_t3347146576;
// UITweener
struct UITweener_t260334902;
// UIToggle/Validate
struct Validate_t3702293971;
// System.Collections.Generic.List`1<BallSelectButtonAssistant>
struct List_1_t4288528635;
// System.Collections.Generic.List`1<StageSelectButtonAssistant>
struct List_1_t2037122148;
// TabBallButtonAssistant
struct TabBallButtonAssistant_t3092544413;
// TabStageButtonAssistant
struct TabStageButtonAssistant_t2589366364;
// UI2DSprite
struct UI2DSprite_t1366157572;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t2579314702;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t3180538779;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;




#ifndef U3CMODULEU3E_T692745563_H
#define U3CMODULEU3E_T692745563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745563 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745563_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CUPDATETWEENPOSITIONU3EC__ITERATOR0_T2265653740_H
#define U3CUPDATETWEENPOSITIONU3EC__ITERATOR0_T2265653740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/<UpdateTweenPosition>c__Iterator0
struct  U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740  : public RuntimeObject
{
public:
	// TweenPosition UIPopupList/<UpdateTweenPosition>c__Iterator0::<tp>__1
	TweenPosition_t1378762002 * ___U3CtpU3E__1_0;
	// UIPopupList UIPopupList/<UpdateTweenPosition>c__Iterator0::$this
	UIPopupList_t4167399471 * ___U24this_1;
	// System.Object UIPopupList/<UpdateTweenPosition>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UIPopupList/<UpdateTweenPosition>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UIPopupList/<UpdateTweenPosition>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtpU3E__1_0() { return static_cast<int32_t>(offsetof(U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740, ___U3CtpU3E__1_0)); }
	inline TweenPosition_t1378762002 * get_U3CtpU3E__1_0() const { return ___U3CtpU3E__1_0; }
	inline TweenPosition_t1378762002 ** get_address_of_U3CtpU3E__1_0() { return &___U3CtpU3E__1_0; }
	inline void set_U3CtpU3E__1_0(TweenPosition_t1378762002 * value)
	{
		___U3CtpU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtpU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740, ___U24this_1)); }
	inline UIPopupList_t4167399471 * get_U24this_1() const { return ___U24this_1; }
	inline UIPopupList_t4167399471 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UIPopupList_t4167399471 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATETWEENPOSITIONU3EC__ITERATOR0_T2265653740_H
#ifndef U3CCOUNTDOWNU3EC__ITERATOR0_T63069431_H
#define U3CCOUNTDOWNU3EC__ITERATOR0_T63069431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterLabel/<CountDown>c__Iterator0
struct  U3CCountDownU3Ec__Iterator0_t63069431  : public RuntimeObject
{
public:
	// CounterLabel CounterLabel/<CountDown>c__Iterator0::$this
	CounterLabel_t1397518286 * ___U24this_0;
	// System.Object CounterLabel/<CountDown>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CounterLabel/<CountDown>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CounterLabel/<CountDown>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCountDownU3Ec__Iterator0_t63069431, ___U24this_0)); }
	inline CounterLabel_t1397518286 * get_U24this_0() const { return ___U24this_0; }
	inline CounterLabel_t1397518286 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CounterLabel_t1397518286 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCountDownU3Ec__Iterator0_t63069431, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCountDownU3Ec__Iterator0_t63069431, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCountDownU3Ec__Iterator0_t63069431, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOUNTDOWNU3EC__ITERATOR0_T63069431_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CCLOSEIFUNSELECTEDU3EC__ITERATOR1_T2833751522_H
#define U3CCLOSEIFUNSELECTEDU3EC__ITERATOR1_T2833751522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/<CloseIfUnselected>c__Iterator1
struct  U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UIPopupList/<CloseIfUnselected>c__Iterator1::<sel>__1
	GameObject_t1113636619 * ___U3CselU3E__1_0;
	// UIPopupList UIPopupList/<CloseIfUnselected>c__Iterator1::$this
	UIPopupList_t4167399471 * ___U24this_1;
	// System.Object UIPopupList/<CloseIfUnselected>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UIPopupList/<CloseIfUnselected>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UIPopupList/<CloseIfUnselected>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CselU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522, ___U3CselU3E__1_0)); }
	inline GameObject_t1113636619 * get_U3CselU3E__1_0() const { return ___U3CselU3E__1_0; }
	inline GameObject_t1113636619 ** get_address_of_U3CselU3E__1_0() { return &___U3CselU3E__1_0; }
	inline void set_U3CselU3E__1_0(GameObject_t1113636619 * value)
	{
		___U3CselU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CselU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522, ___U24this_1)); }
	inline UIPopupList_t4167399471 * get_U24this_1() const { return ___U24this_1; }
	inline UIPopupList_t4167399471 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UIPopupList_t4167399471 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLOSEIFUNSELECTEDU3EC__ITERATOR1_T2833751522_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef FADEENTRY_T639421133_H
#define FADEENTRY_T639421133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TypewriterEffect/FadeEntry
struct  FadeEntry_t639421133 
{
public:
	// System.Int32 TypewriterEffect/FadeEntry::index
	int32_t ___index_0;
	// System.String TypewriterEffect/FadeEntry::text
	String_t* ___text_1;
	// System.Single TypewriterEffect/FadeEntry::alpha
	float ___alpha_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(FadeEntry_t639421133, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(FadeEntry_t639421133, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_alpha_2() { return static_cast<int32_t>(offsetof(FadeEntry_t639421133, ___alpha_2)); }
	inline float get_alpha_2() const { return ___alpha_2; }
	inline float* get_address_of_alpha_2() { return &___alpha_2; }
	inline void set_alpha_2(float value)
	{
		___alpha_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TypewriterEffect/FadeEntry
struct FadeEntry_t639421133_marshaled_pinvoke
{
	int32_t ___index_0;
	char* ___text_1;
	float ___alpha_2;
};
// Native definition for COM marshalling of TypewriterEffect/FadeEntry
struct FadeEntry_t639421133_marshaled_com
{
	int32_t ___index_0;
	Il2CppChar* ___text_1;
	float ___alpha_2;
};
#endif // FADEENTRY_T639421133_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FILLDIRECTION_T551265397_H
#define FILLDIRECTION_T551265397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIProgressBar/FillDirection
struct  FillDirection_t551265397 
{
public:
	// System.Int32 UIProgressBar/FillDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillDirection_t551265397, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLDIRECTION_T551265397_H
#ifndef DIRECTION_T340832489_H
#define DIRECTION_T340832489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollBar/Direction
struct  Direction_t340832489 
{
public:
	// System.Int32 UIScrollBar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t340832489, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T340832489_H
#ifndef MOVEMENT_T247277292_H
#define MOVEMENT_T247277292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView/Movement
struct  Movement_t247277292 
{
public:
	// System.Int32 UIScrollView/Movement::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Movement_t247277292, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENT_T247277292_H
#ifndef OPENON_T1997085761_H
#define OPENON_T1997085761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/OpenOn
struct  OpenOn_t1997085761 
{
public:
	// System.Int32 UIPopupList/OpenOn::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpenOn_t1997085761, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENON_T1997085761_H
#ifndef SELECTION_T3039885439_H
#define SELECTION_T3039885439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/Selection
struct  Selection_t3039885439 
{
public:
	// System.Int32 UIPopupList/Selection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Selection_t3039885439, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTION_T3039885439_H
#ifndef POSITION_T1583461796_H
#define POSITION_T1583461796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/Position
struct  Position_t1583461796 
{
public:
	// System.Int32 UIPopupList/Position::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Position_t1583461796, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T1583461796_H
#ifndef DRAGEFFECT_T491601944_H
#define DRAGEFFECT_T491601944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView/DragEffect
struct  DragEffect_t491601944 
{
public:
	// System.Int32 UIScrollView/DragEffect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DragEffect_t491601944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEFFECT_T491601944_H
#ifndef TRIGGER_T2172727401_H
#define TRIGGER_T2172727401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPlaySound/Trigger
struct  Trigger_t2172727401 
{
public:
	// System.Int32 UIPlaySound/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t2172727401, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T2172727401_H
#ifndef CONSTRAINT_T2778583555_H
#define CONSTRAINT_T2778583555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIKeyNavigation/Constraint
struct  Constraint_t2778583555 
{
public:
	// System.Int32 UIKeyNavigation/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t2778583555, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T2778583555_H
#ifndef MODIFIER_T1697911081_H
#define MODIFIER_T1697911081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIKeyBinding/Modifier
struct  Modifier_t1697911081 
{
public:
	// System.Int32 UIKeyBinding/Modifier::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Modifier_t1697911081, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIER_T1697911081_H
#ifndef ACTION_T4142137194_H
#define ACTION_T4142137194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIKeyBinding/Action
struct  Action_t4142137194 
{
public:
	// System.Int32 UIKeyBinding/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t4142137194, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T4142137194_H
#ifndef SHOWCONDITION_T1535012424_H
#define SHOWCONDITION_T1535012424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView/ShowCondition
struct  ShowCondition_t1535012424 
{
public:
	// System.Int32 UIScrollView/ShowCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShowCondition_t1535012424, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWCONDITION_T1535012424_H
#ifndef DIRECTION_T3048248572_H
#define DIRECTION_T3048248572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISlider/Direction
struct  Direction_t3048248572 
{
public:
	// System.Int32 UISlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t3048248572, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T3048248572_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef SORTING_T2823944879_H
#define SORTING_T2823944879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITable/Sorting
struct  Sorting_t2823944879 
{
public:
	// System.Int32 UITable/Sorting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sorting_t2823944879, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTING_T2823944879_H
#ifndef SORTING_T533260699_H
#define SORTING_T533260699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid/Sorting
struct  Sorting_t533260699 
{
public:
	// System.Int32 UIGrid/Sorting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sorting_t533260699, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTING_T533260699_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef PLANE_T1000493321_H
#define PLANE_T1000493321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1000493321 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3722313464  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Normal_0)); }
	inline Vector3_t3722313464  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3722313464  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1000493321_H
#ifndef PIVOT_T1798046373_H
#define PIVOT_T1798046373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/Pivot
struct  Pivot_t1798046373 
{
public:
	// System.Int32 UIWidget/Pivot::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pivot_t1798046373, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOT_T1798046373_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef TRIGGER_T3745258312_H
#define TRIGGER_T3745258312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Trigger
struct  Trigger_t3745258312 
{
public:
	// System.Int32 AnimationOrTween.Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t3745258312, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T3745258312_H
#ifndef DIRECTION_T2061188385_H
#define DIRECTION_T2061188385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.Direction
struct  Direction_t2061188385 
{
public:
	// System.Int32 AnimationOrTween.Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t2061188385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T2061188385_H
#ifndef ENABLECONDITION_T1125033030_H
#define ENABLECONDITION_T1125033030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.EnableCondition
struct  EnableCondition_t1125033030 
{
public:
	// System.Int32 AnimationOrTween.EnableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EnableCondition_t1125033030, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLECONDITION_T1125033030_H
#ifndef DISABLECONDITION_T2151257518_H
#define DISABLECONDITION_T2151257518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationOrTween.DisableCondition
struct  DisableCondition_t2151257518 
{
public:
	// System.Int32 AnimationOrTween.DisableCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisableCondition_t2151257518, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECONDITION_T2151257518_H
#ifndef FONTSTYLE_T82229486_H
#define FONTSTYLE_T82229486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t82229486 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyle_t82229486, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T82229486_H
#ifndef ALIGNMENT_T3228070485_H
#define ALIGNMENT_T3228070485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/Alignment
struct  Alignment_t3228070485 
{
public:
	// System.Int32 NGUIText/Alignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Alignment_t3228070485, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T3228070485_H
#ifndef MODIFIER_T3840764400_H
#define MODIFIER_T3840764400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Modifier
struct  Modifier_t3840764400 
{
public:
	// System.Int32 UILabel/Modifier::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Modifier_t3840764400, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIER_T3840764400_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef DIRECTION_T2487117792_H
#define DIRECTION_T2487117792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITable/Direction
struct  Direction_t2487117792 
{
public:
	// System.Int32 UITable/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t2487117792, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T2487117792_H
#ifndef ARRANGEMENT_T1850956547_H
#define ARRANGEMENT_T1850956547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid/Arrangement
struct  Arrangement_t1850956547 
{
public:
	// System.Int32 UIGrid/Arrangement::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Arrangement_t1850956547, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRANGEMENT_T1850956547_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef STATE_T3991372483_H
#define STATE_T3991372483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonColor/State
struct  State_t3991372483 
{
public:
	// System.Int32 UIButtonColor/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t3991372483, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T3991372483_H
#ifndef DRAGEFFECT_T2686339116_H
#define DRAGEFFECT_T2686339116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragObject/DragEffect
struct  DragEffect_t2686339116 
{
public:
	// System.Int32 UIDragObject/DragEffect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DragEffect_t2686339116, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEFFECT_T2686339116_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef RESTRICTION_T4221874387_H
#define RESTRICTION_T4221874387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragDropItem/Restriction
struct  Restriction_t4221874387 
{
public:
	// System.Int32 UIDragDropItem/Restriction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Restriction_t4221874387, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRICTION_T4221874387_H
#ifndef TRIGGER_T148524997_H
#define TRIGGER_T148524997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonMessage/Trigger
struct  Trigger_t148524997 
{
public:
	// System.Int32 UIButtonMessage/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t148524997, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T148524997_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ONDRAGNOTIFICATION_T1437737811_H
#define ONDRAGNOTIFICATION_T1437737811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView/OnDragNotification
struct  OnDragNotification_t1437737811  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAGNOTIFICATION_T1437737811_H
#ifndef ONCENTERCALLBACK_T2077531662_H
#define ONCENTERCALLBACK_T2077531662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICenterOnChild/OnCenterCallback
struct  OnCenterCallback_t2077531662  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCENTERCALLBACK_T2077531662_H
#ifndef LEGACYEVENT_T2749056879_H
#define LEGACYEVENT_T2749056879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/LegacyEvent
struct  LegacyEvent_t2749056879  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYEVENT_T2749056879_H
#ifndef ONREPOSITION_T3913508630_H
#define ONREPOSITION_T3913508630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITable/OnReposition
struct  OnReposition_t3913508630  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREPOSITION_T3913508630_H
#ifndef ONDRAGFINISHED_T3715779777_H
#define ONDRAGFINISHED_T3715779777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIProgressBar/OnDragFinished
struct  OnDragFinished_t3715779777  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAGFINISHED_T3715779777_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef ONREPOSITION_T1372889220_H
#define ONREPOSITION_T1372889220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid/OnReposition
struct  OnReposition_t1372889220  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREPOSITION_T1372889220_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef VALIDATE_T3702293971_H
#define VALIDATE_T3702293971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggle/Validate
struct  Validate_t3702293971  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATE_T3702293971_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UITOGGLEDCOMPONENTS_T772955118_H
#define UITOGGLEDCOMPONENTS_T772955118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggledComponents
struct  UIToggledComponents_t772955118  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour> UIToggledComponents::activate
	List_1_t1139589975 * ___activate_2;
	// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour> UIToggledComponents::deactivate
	List_1_t1139589975 * ___deactivate_3;
	// UnityEngine.MonoBehaviour UIToggledComponents::target
	MonoBehaviour_t3962482529 * ___target_4;
	// System.Boolean UIToggledComponents::inverse
	bool ___inverse_5;

public:
	inline static int32_t get_offset_of_activate_2() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___activate_2)); }
	inline List_1_t1139589975 * get_activate_2() const { return ___activate_2; }
	inline List_1_t1139589975 ** get_address_of_activate_2() { return &___activate_2; }
	inline void set_activate_2(List_1_t1139589975 * value)
	{
		___activate_2 = value;
		Il2CppCodeGenWriteBarrier((&___activate_2), value);
	}

	inline static int32_t get_offset_of_deactivate_3() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___deactivate_3)); }
	inline List_1_t1139589975 * get_deactivate_3() const { return ___deactivate_3; }
	inline List_1_t1139589975 ** get_address_of_deactivate_3() { return &___deactivate_3; }
	inline void set_deactivate_3(List_1_t1139589975 * value)
	{
		___deactivate_3 = value;
		Il2CppCodeGenWriteBarrier((&___deactivate_3), value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___target_4)); }
	inline MonoBehaviour_t3962482529 * get_target_4() const { return ___target_4; }
	inline MonoBehaviour_t3962482529 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(MonoBehaviour_t3962482529 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_inverse_5() { return static_cast<int32_t>(offsetof(UIToggledComponents_t772955118, ___inverse_5)); }
	inline bool get_inverse_5() const { return ___inverse_5; }
	inline bool* get_address_of_inverse_5() { return &___inverse_5; }
	inline void set_inverse_5(bool value)
	{
		___inverse_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEDCOMPONENTS_T772955118_H
#ifndef UITOGGLEDOBJECTS_T3502557910_H
#define UITOGGLEDOBJECTS_T3502557910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggledObjects
struct  UIToggledObjects_t3502557910  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UIToggledObjects::activate
	List_1_t2585711361 * ___activate_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UIToggledObjects::deactivate
	List_1_t2585711361 * ___deactivate_3;
	// UnityEngine.GameObject UIToggledObjects::target
	GameObject_t1113636619 * ___target_4;
	// System.Boolean UIToggledObjects::inverse
	bool ___inverse_5;

public:
	inline static int32_t get_offset_of_activate_2() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___activate_2)); }
	inline List_1_t2585711361 * get_activate_2() const { return ___activate_2; }
	inline List_1_t2585711361 ** get_address_of_activate_2() { return &___activate_2; }
	inline void set_activate_2(List_1_t2585711361 * value)
	{
		___activate_2 = value;
		Il2CppCodeGenWriteBarrier((&___activate_2), value);
	}

	inline static int32_t get_offset_of_deactivate_3() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___deactivate_3)); }
	inline List_1_t2585711361 * get_deactivate_3() const { return ___deactivate_3; }
	inline List_1_t2585711361 ** get_address_of_deactivate_3() { return &___deactivate_3; }
	inline void set_deactivate_3(List_1_t2585711361 * value)
	{
		___deactivate_3 = value;
		Il2CppCodeGenWriteBarrier((&___deactivate_3), value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___target_4)); }
	inline GameObject_t1113636619 * get_target_4() const { return ___target_4; }
	inline GameObject_t1113636619 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_t1113636619 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_inverse_5() { return static_cast<int32_t>(offsetof(UIToggledObjects_t3502557910, ___inverse_5)); }
	inline bool get_inverse_5() const { return ___inverse_5; }
	inline bool* get_address_of_inverse_5() { return &___inverse_5; }
	inline void set_inverse_5(bool value)
	{
		___inverse_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEDOBJECTS_T3502557910_H
#ifndef UIWIDGETCONTAINER_T30162560_H
#define UIWIDGETCONTAINER_T30162560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidgetContainer
struct  UIWidgetContainer_t30162560  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWIDGETCONTAINER_T30162560_H
#ifndef UIWRAPCONTENT_T1188558554_H
#define UIWRAPCONTENT_T1188558554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWrapContent
struct  UIWrapContent_t1188558554  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UIWrapContent::itemSize
	int32_t ___itemSize_2;
	// System.Boolean UIWrapContent::cullContent
	bool ___cullContent_3;
	// System.Int32 UIWrapContent::minIndex
	int32_t ___minIndex_4;
	// System.Int32 UIWrapContent::maxIndex
	int32_t ___maxIndex_5;
	// System.Boolean UIWrapContent::hideInactive
	bool ___hideInactive_6;
	// UIWrapContent/OnInitializeItem UIWrapContent::onInitializeItem
	OnInitializeItem_t992046894 * ___onInitializeItem_7;
	// UnityEngine.Transform UIWrapContent::mTrans
	Transform_t3600365921 * ___mTrans_8;
	// UIPanel UIWrapContent::mPanel
	UIPanel_t1716472341 * ___mPanel_9;
	// UIScrollView UIWrapContent::mScroll
	UIScrollView_t1973404950 * ___mScroll_10;
	// System.Boolean UIWrapContent::mHorizontal
	bool ___mHorizontal_11;
	// System.Boolean UIWrapContent::mFirstTime
	bool ___mFirstTime_12;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UIWrapContent::mChildren
	List_1_t777473367 * ___mChildren_13;

public:
	inline static int32_t get_offset_of_itemSize_2() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___itemSize_2)); }
	inline int32_t get_itemSize_2() const { return ___itemSize_2; }
	inline int32_t* get_address_of_itemSize_2() { return &___itemSize_2; }
	inline void set_itemSize_2(int32_t value)
	{
		___itemSize_2 = value;
	}

	inline static int32_t get_offset_of_cullContent_3() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___cullContent_3)); }
	inline bool get_cullContent_3() const { return ___cullContent_3; }
	inline bool* get_address_of_cullContent_3() { return &___cullContent_3; }
	inline void set_cullContent_3(bool value)
	{
		___cullContent_3 = value;
	}

	inline static int32_t get_offset_of_minIndex_4() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___minIndex_4)); }
	inline int32_t get_minIndex_4() const { return ___minIndex_4; }
	inline int32_t* get_address_of_minIndex_4() { return &___minIndex_4; }
	inline void set_minIndex_4(int32_t value)
	{
		___minIndex_4 = value;
	}

	inline static int32_t get_offset_of_maxIndex_5() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___maxIndex_5)); }
	inline int32_t get_maxIndex_5() const { return ___maxIndex_5; }
	inline int32_t* get_address_of_maxIndex_5() { return &___maxIndex_5; }
	inline void set_maxIndex_5(int32_t value)
	{
		___maxIndex_5 = value;
	}

	inline static int32_t get_offset_of_hideInactive_6() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___hideInactive_6)); }
	inline bool get_hideInactive_6() const { return ___hideInactive_6; }
	inline bool* get_address_of_hideInactive_6() { return &___hideInactive_6; }
	inline void set_hideInactive_6(bool value)
	{
		___hideInactive_6 = value;
	}

	inline static int32_t get_offset_of_onInitializeItem_7() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___onInitializeItem_7)); }
	inline OnInitializeItem_t992046894 * get_onInitializeItem_7() const { return ___onInitializeItem_7; }
	inline OnInitializeItem_t992046894 ** get_address_of_onInitializeItem_7() { return &___onInitializeItem_7; }
	inline void set_onInitializeItem_7(OnInitializeItem_t992046894 * value)
	{
		___onInitializeItem_7 = value;
		Il2CppCodeGenWriteBarrier((&___onInitializeItem_7), value);
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mTrans_8)); }
	inline Transform_t3600365921 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t3600365921 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t3600365921 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mPanel_9() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mPanel_9)); }
	inline UIPanel_t1716472341 * get_mPanel_9() const { return ___mPanel_9; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_9() { return &___mPanel_9; }
	inline void set_mPanel_9(UIPanel_t1716472341 * value)
	{
		___mPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_9), value);
	}

	inline static int32_t get_offset_of_mScroll_10() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mScroll_10)); }
	inline UIScrollView_t1973404950 * get_mScroll_10() const { return ___mScroll_10; }
	inline UIScrollView_t1973404950 ** get_address_of_mScroll_10() { return &___mScroll_10; }
	inline void set_mScroll_10(UIScrollView_t1973404950 * value)
	{
		___mScroll_10 = value;
		Il2CppCodeGenWriteBarrier((&___mScroll_10), value);
	}

	inline static int32_t get_offset_of_mHorizontal_11() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mHorizontal_11)); }
	inline bool get_mHorizontal_11() const { return ___mHorizontal_11; }
	inline bool* get_address_of_mHorizontal_11() { return &___mHorizontal_11; }
	inline void set_mHorizontal_11(bool value)
	{
		___mHorizontal_11 = value;
	}

	inline static int32_t get_offset_of_mFirstTime_12() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mFirstTime_12)); }
	inline bool get_mFirstTime_12() const { return ___mFirstTime_12; }
	inline bool* get_address_of_mFirstTime_12() { return &___mFirstTime_12; }
	inline void set_mFirstTime_12(bool value)
	{
		___mFirstTime_12 = value;
	}

	inline static int32_t get_offset_of_mChildren_13() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554, ___mChildren_13)); }
	inline List_1_t777473367 * get_mChildren_13() const { return ___mChildren_13; }
	inline List_1_t777473367 ** get_address_of_mChildren_13() { return &___mChildren_13; }
	inline void set_mChildren_13(List_1_t777473367 * value)
	{
		___mChildren_13 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_13), value);
	}
};

struct UIWrapContent_t1188558554_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.Transform> UIWrapContent::<>f__mg$cache0
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache0_14;
	// System.Comparison`1<UnityEngine.Transform> UIWrapContent::<>f__mg$cache1
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache1_15;
	// System.Comparison`1<UnityEngine.Transform> UIWrapContent::<>f__mg$cache2
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache2_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_14() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554_StaticFields, ___U3CU3Ef__mgU24cache0_14)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache0_14() const { return ___U3CU3Ef__mgU24cache0_14; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache0_14() { return &___U3CU3Ef__mgU24cache0_14; }
	inline void set_U3CU3Ef__mgU24cache0_14(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_15() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554_StaticFields, ___U3CU3Ef__mgU24cache1_15)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache1_15() const { return ___U3CU3Ef__mgU24cache1_15; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache1_15() { return &___U3CU3Ef__mgU24cache1_15; }
	inline void set_U3CU3Ef__mgU24cache1_15(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_16() { return static_cast<int32_t>(offsetof(UIWrapContent_t1188558554_StaticFields, ___U3CU3Ef__mgU24cache2_16)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache2_16() const { return ___U3CU3Ef__mgU24cache2_16; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache2_16() { return &___U3CU3Ef__mgU24cache2_16; }
	inline void set_U3CU3Ef__mgU24cache2_16(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWRAPCONTENT_T1188558554_H
#ifndef UIDRAGSCROLLVIEW_T2492060641_H
#define UIDRAGSCROLLVIEW_T2492060641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragScrollView
struct  UIDragScrollView_t2492060641  : public MonoBehaviour_t3962482529
{
public:
	// UIScrollView UIDragScrollView::scrollView
	UIScrollView_t1973404950 * ___scrollView_2;
	// UIScrollView UIDragScrollView::draggablePanel
	UIScrollView_t1973404950 * ___draggablePanel_3;
	// UnityEngine.Transform UIDragScrollView::mTrans
	Transform_t3600365921 * ___mTrans_4;
	// UIScrollView UIDragScrollView::mScroll
	UIScrollView_t1973404950 * ___mScroll_5;
	// System.Boolean UIDragScrollView::mAutoFind
	bool ___mAutoFind_6;
	// System.Boolean UIDragScrollView::mStarted
	bool ___mStarted_7;
	// System.Boolean UIDragScrollView::mPressed
	bool ___mPressed_8;

public:
	inline static int32_t get_offset_of_scrollView_2() { return static_cast<int32_t>(offsetof(UIDragScrollView_t2492060641, ___scrollView_2)); }
	inline UIScrollView_t1973404950 * get_scrollView_2() const { return ___scrollView_2; }
	inline UIScrollView_t1973404950 ** get_address_of_scrollView_2() { return &___scrollView_2; }
	inline void set_scrollView_2(UIScrollView_t1973404950 * value)
	{
		___scrollView_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrollView_2), value);
	}

	inline static int32_t get_offset_of_draggablePanel_3() { return static_cast<int32_t>(offsetof(UIDragScrollView_t2492060641, ___draggablePanel_3)); }
	inline UIScrollView_t1973404950 * get_draggablePanel_3() const { return ___draggablePanel_3; }
	inline UIScrollView_t1973404950 ** get_address_of_draggablePanel_3() { return &___draggablePanel_3; }
	inline void set_draggablePanel_3(UIScrollView_t1973404950 * value)
	{
		___draggablePanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___draggablePanel_3), value);
	}

	inline static int32_t get_offset_of_mTrans_4() { return static_cast<int32_t>(offsetof(UIDragScrollView_t2492060641, ___mTrans_4)); }
	inline Transform_t3600365921 * get_mTrans_4() const { return ___mTrans_4; }
	inline Transform_t3600365921 ** get_address_of_mTrans_4() { return &___mTrans_4; }
	inline void set_mTrans_4(Transform_t3600365921 * value)
	{
		___mTrans_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_4), value);
	}

	inline static int32_t get_offset_of_mScroll_5() { return static_cast<int32_t>(offsetof(UIDragScrollView_t2492060641, ___mScroll_5)); }
	inline UIScrollView_t1973404950 * get_mScroll_5() const { return ___mScroll_5; }
	inline UIScrollView_t1973404950 ** get_address_of_mScroll_5() { return &___mScroll_5; }
	inline void set_mScroll_5(UIScrollView_t1973404950 * value)
	{
		___mScroll_5 = value;
		Il2CppCodeGenWriteBarrier((&___mScroll_5), value);
	}

	inline static int32_t get_offset_of_mAutoFind_6() { return static_cast<int32_t>(offsetof(UIDragScrollView_t2492060641, ___mAutoFind_6)); }
	inline bool get_mAutoFind_6() const { return ___mAutoFind_6; }
	inline bool* get_address_of_mAutoFind_6() { return &___mAutoFind_6; }
	inline void set_mAutoFind_6(bool value)
	{
		___mAutoFind_6 = value;
	}

	inline static int32_t get_offset_of_mStarted_7() { return static_cast<int32_t>(offsetof(UIDragScrollView_t2492060641, ___mStarted_7)); }
	inline bool get_mStarted_7() const { return ___mStarted_7; }
	inline bool* get_address_of_mStarted_7() { return &___mStarted_7; }
	inline void set_mStarted_7(bool value)
	{
		___mStarted_7 = value;
	}

	inline static int32_t get_offset_of_mPressed_8() { return static_cast<int32_t>(offsetof(UIDragScrollView_t2492060641, ___mPressed_8)); }
	inline bool get_mPressed_8() const { return ___mPressed_8; }
	inline bool* get_address_of_mPressed_8() { return &___mPressed_8; }
	inline void set_mPressed_8(bool value)
	{
		___mPressed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGSCROLLVIEW_T2492060641_H
#ifndef ENVELOPCONTENT_T3305418577_H
#define ENVELOPCONTENT_T3305418577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvelopContent
struct  EnvelopContent_t3305418577  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform EnvelopContent::targetRoot
	Transform_t3600365921 * ___targetRoot_2;
	// System.Int32 EnvelopContent::padLeft
	int32_t ___padLeft_3;
	// System.Int32 EnvelopContent::padRight
	int32_t ___padRight_4;
	// System.Int32 EnvelopContent::padBottom
	int32_t ___padBottom_5;
	// System.Int32 EnvelopContent::padTop
	int32_t ___padTop_6;
	// System.Boolean EnvelopContent::ignoreDisabled
	bool ___ignoreDisabled_7;
	// System.Boolean EnvelopContent::mStarted
	bool ___mStarted_8;

public:
	inline static int32_t get_offset_of_targetRoot_2() { return static_cast<int32_t>(offsetof(EnvelopContent_t3305418577, ___targetRoot_2)); }
	inline Transform_t3600365921 * get_targetRoot_2() const { return ___targetRoot_2; }
	inline Transform_t3600365921 ** get_address_of_targetRoot_2() { return &___targetRoot_2; }
	inline void set_targetRoot_2(Transform_t3600365921 * value)
	{
		___targetRoot_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetRoot_2), value);
	}

	inline static int32_t get_offset_of_padLeft_3() { return static_cast<int32_t>(offsetof(EnvelopContent_t3305418577, ___padLeft_3)); }
	inline int32_t get_padLeft_3() const { return ___padLeft_3; }
	inline int32_t* get_address_of_padLeft_3() { return &___padLeft_3; }
	inline void set_padLeft_3(int32_t value)
	{
		___padLeft_3 = value;
	}

	inline static int32_t get_offset_of_padRight_4() { return static_cast<int32_t>(offsetof(EnvelopContent_t3305418577, ___padRight_4)); }
	inline int32_t get_padRight_4() const { return ___padRight_4; }
	inline int32_t* get_address_of_padRight_4() { return &___padRight_4; }
	inline void set_padRight_4(int32_t value)
	{
		___padRight_4 = value;
	}

	inline static int32_t get_offset_of_padBottom_5() { return static_cast<int32_t>(offsetof(EnvelopContent_t3305418577, ___padBottom_5)); }
	inline int32_t get_padBottom_5() const { return ___padBottom_5; }
	inline int32_t* get_address_of_padBottom_5() { return &___padBottom_5; }
	inline void set_padBottom_5(int32_t value)
	{
		___padBottom_5 = value;
	}

	inline static int32_t get_offset_of_padTop_6() { return static_cast<int32_t>(offsetof(EnvelopContent_t3305418577, ___padTop_6)); }
	inline int32_t get_padTop_6() const { return ___padTop_6; }
	inline int32_t* get_address_of_padTop_6() { return &___padTop_6; }
	inline void set_padTop_6(int32_t value)
	{
		___padTop_6 = value;
	}

	inline static int32_t get_offset_of_ignoreDisabled_7() { return static_cast<int32_t>(offsetof(EnvelopContent_t3305418577, ___ignoreDisabled_7)); }
	inline bool get_ignoreDisabled_7() const { return ___ignoreDisabled_7; }
	inline bool* get_address_of_ignoreDisabled_7() { return &___ignoreDisabled_7; }
	inline void set_ignoreDisabled_7(bool value)
	{
		___ignoreDisabled_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(EnvelopContent_t3305418577, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVELOPCONTENT_T3305418577_H
#ifndef TYPEWRITEREFFECT_T1340895546_H
#define TYPEWRITEREFFECT_T1340895546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TypewriterEffect
struct  TypewriterEffect_t1340895546  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TypewriterEffect::charsPerSecond
	int32_t ___charsPerSecond_3;
	// System.Single TypewriterEffect::fadeInTime
	float ___fadeInTime_4;
	// System.Single TypewriterEffect::delayOnPeriod
	float ___delayOnPeriod_5;
	// System.Single TypewriterEffect::delayOnNewLine
	float ___delayOnNewLine_6;
	// UIScrollView TypewriterEffect::scrollView
	UIScrollView_t1973404950 * ___scrollView_7;
	// System.Boolean TypewriterEffect::keepFullDimensions
	bool ___keepFullDimensions_8;
	// System.Collections.Generic.List`1<EventDelegate> TypewriterEffect::onFinished
	List_1_t4210400802 * ___onFinished_9;
	// UILabel TypewriterEffect::mLabel
	UILabel_t3248798549 * ___mLabel_10;
	// System.String TypewriterEffect::mFullText
	String_t* ___mFullText_11;
	// System.Int32 TypewriterEffect::mCurrentOffset
	int32_t ___mCurrentOffset_12;
	// System.Single TypewriterEffect::mNextChar
	float ___mNextChar_13;
	// System.Boolean TypewriterEffect::mReset
	bool ___mReset_14;
	// System.Boolean TypewriterEffect::mActive
	bool ___mActive_15;
	// BetterList`1<TypewriterEffect/FadeEntry> TypewriterEffect::mFade
	BetterList_1_t4089408747 * ___mFade_16;

public:
	inline static int32_t get_offset_of_charsPerSecond_3() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___charsPerSecond_3)); }
	inline int32_t get_charsPerSecond_3() const { return ___charsPerSecond_3; }
	inline int32_t* get_address_of_charsPerSecond_3() { return &___charsPerSecond_3; }
	inline void set_charsPerSecond_3(int32_t value)
	{
		___charsPerSecond_3 = value;
	}

	inline static int32_t get_offset_of_fadeInTime_4() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___fadeInTime_4)); }
	inline float get_fadeInTime_4() const { return ___fadeInTime_4; }
	inline float* get_address_of_fadeInTime_4() { return &___fadeInTime_4; }
	inline void set_fadeInTime_4(float value)
	{
		___fadeInTime_4 = value;
	}

	inline static int32_t get_offset_of_delayOnPeriod_5() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___delayOnPeriod_5)); }
	inline float get_delayOnPeriod_5() const { return ___delayOnPeriod_5; }
	inline float* get_address_of_delayOnPeriod_5() { return &___delayOnPeriod_5; }
	inline void set_delayOnPeriod_5(float value)
	{
		___delayOnPeriod_5 = value;
	}

	inline static int32_t get_offset_of_delayOnNewLine_6() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___delayOnNewLine_6)); }
	inline float get_delayOnNewLine_6() const { return ___delayOnNewLine_6; }
	inline float* get_address_of_delayOnNewLine_6() { return &___delayOnNewLine_6; }
	inline void set_delayOnNewLine_6(float value)
	{
		___delayOnNewLine_6 = value;
	}

	inline static int32_t get_offset_of_scrollView_7() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___scrollView_7)); }
	inline UIScrollView_t1973404950 * get_scrollView_7() const { return ___scrollView_7; }
	inline UIScrollView_t1973404950 ** get_address_of_scrollView_7() { return &___scrollView_7; }
	inline void set_scrollView_7(UIScrollView_t1973404950 * value)
	{
		___scrollView_7 = value;
		Il2CppCodeGenWriteBarrier((&___scrollView_7), value);
	}

	inline static int32_t get_offset_of_keepFullDimensions_8() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___keepFullDimensions_8)); }
	inline bool get_keepFullDimensions_8() const { return ___keepFullDimensions_8; }
	inline bool* get_address_of_keepFullDimensions_8() { return &___keepFullDimensions_8; }
	inline void set_keepFullDimensions_8(bool value)
	{
		___keepFullDimensions_8 = value;
	}

	inline static int32_t get_offset_of_onFinished_9() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___onFinished_9)); }
	inline List_1_t4210400802 * get_onFinished_9() const { return ___onFinished_9; }
	inline List_1_t4210400802 ** get_address_of_onFinished_9() { return &___onFinished_9; }
	inline void set_onFinished_9(List_1_t4210400802 * value)
	{
		___onFinished_9 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_9), value);
	}

	inline static int32_t get_offset_of_mLabel_10() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___mLabel_10)); }
	inline UILabel_t3248798549 * get_mLabel_10() const { return ___mLabel_10; }
	inline UILabel_t3248798549 ** get_address_of_mLabel_10() { return &___mLabel_10; }
	inline void set_mLabel_10(UILabel_t3248798549 * value)
	{
		___mLabel_10 = value;
		Il2CppCodeGenWriteBarrier((&___mLabel_10), value);
	}

	inline static int32_t get_offset_of_mFullText_11() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___mFullText_11)); }
	inline String_t* get_mFullText_11() const { return ___mFullText_11; }
	inline String_t** get_address_of_mFullText_11() { return &___mFullText_11; }
	inline void set_mFullText_11(String_t* value)
	{
		___mFullText_11 = value;
		Il2CppCodeGenWriteBarrier((&___mFullText_11), value);
	}

	inline static int32_t get_offset_of_mCurrentOffset_12() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___mCurrentOffset_12)); }
	inline int32_t get_mCurrentOffset_12() const { return ___mCurrentOffset_12; }
	inline int32_t* get_address_of_mCurrentOffset_12() { return &___mCurrentOffset_12; }
	inline void set_mCurrentOffset_12(int32_t value)
	{
		___mCurrentOffset_12 = value;
	}

	inline static int32_t get_offset_of_mNextChar_13() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___mNextChar_13)); }
	inline float get_mNextChar_13() const { return ___mNextChar_13; }
	inline float* get_address_of_mNextChar_13() { return &___mNextChar_13; }
	inline void set_mNextChar_13(float value)
	{
		___mNextChar_13 = value;
	}

	inline static int32_t get_offset_of_mReset_14() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___mReset_14)); }
	inline bool get_mReset_14() const { return ___mReset_14; }
	inline bool* get_address_of_mReset_14() { return &___mReset_14; }
	inline void set_mReset_14(bool value)
	{
		___mReset_14 = value;
	}

	inline static int32_t get_offset_of_mActive_15() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___mActive_15)); }
	inline bool get_mActive_15() const { return ___mActive_15; }
	inline bool* get_address_of_mActive_15() { return &___mActive_15; }
	inline void set_mActive_15(bool value)
	{
		___mActive_15 = value;
	}

	inline static int32_t get_offset_of_mFade_16() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546, ___mFade_16)); }
	inline BetterList_1_t4089408747 * get_mFade_16() const { return ___mFade_16; }
	inline BetterList_1_t4089408747 ** get_address_of_mFade_16() { return &___mFade_16; }
	inline void set_mFade_16(BetterList_1_t4089408747 * value)
	{
		___mFade_16 = value;
		Il2CppCodeGenWriteBarrier((&___mFade_16), value);
	}
};

struct TypewriterEffect_t1340895546_StaticFields
{
public:
	// TypewriterEffect TypewriterEffect::current
	TypewriterEffect_t1340895546 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(TypewriterEffect_t1340895546_StaticFields, ___current_2)); }
	inline TypewriterEffect_t1340895546 * get_current_2() const { return ___current_2; }
	inline TypewriterEffect_t1340895546 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(TypewriterEffect_t1340895546 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEWRITEREFFECT_T1340895546_H
#ifndef BUTTONASSISTANT_T3922690862_H
#define BUTTONASSISTANT_T3922690862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonAssistant
struct  ButtonAssistant_t3922690862  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Collider ButtonAssistant::_collider
	Collider_t1773347010 * ____collider_2;
	// UIButton ButtonAssistant::_button
	UIButton_t1100396938 * ____button_3;
	// UISprite ButtonAssistant::_image
	UISprite_t194114938 * ____image_4;

public:
	inline static int32_t get_offset_of__collider_2() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____collider_2)); }
	inline Collider_t1773347010 * get__collider_2() const { return ____collider_2; }
	inline Collider_t1773347010 ** get_address_of__collider_2() { return &____collider_2; }
	inline void set__collider_2(Collider_t1773347010 * value)
	{
		____collider_2 = value;
		Il2CppCodeGenWriteBarrier((&____collider_2), value);
	}

	inline static int32_t get_offset_of__button_3() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____button_3)); }
	inline UIButton_t1100396938 * get__button_3() const { return ____button_3; }
	inline UIButton_t1100396938 ** get_address_of__button_3() { return &____button_3; }
	inline void set__button_3(UIButton_t1100396938 * value)
	{
		____button_3 = value;
		Il2CppCodeGenWriteBarrier((&____button_3), value);
	}

	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____image_4)); }
	inline UISprite_t194114938 * get__image_4() const { return ____image_4; }
	inline UISprite_t194114938 ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(UISprite_t194114938 * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONASSISTANT_T3922690862_H
#ifndef LANGUAGESELECTION_T1927862481_H
#define LANGUAGESELECTION_T1927862481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageSelection
struct  LanguageSelection_t1927862481  : public MonoBehaviour_t3962482529
{
public:
	// UIPopupList LanguageSelection::mList
	UIPopupList_t4167399471 * ___mList_2;
	// System.Boolean LanguageSelection::mStarted
	bool ___mStarted_3;

public:
	inline static int32_t get_offset_of_mList_2() { return static_cast<int32_t>(offsetof(LanguageSelection_t1927862481, ___mList_2)); }
	inline UIPopupList_t4167399471 * get_mList_2() const { return ___mList_2; }
	inline UIPopupList_t4167399471 ** get_address_of_mList_2() { return &___mList_2; }
	inline void set_mList_2(UIPopupList_t4167399471 * value)
	{
		___mList_2 = value;
		Il2CppCodeGenWriteBarrier((&___mList_2), value);
	}

	inline static int32_t get_offset_of_mStarted_3() { return static_cast<int32_t>(offsetof(LanguageSelection_t1927862481, ___mStarted_3)); }
	inline bool get_mStarted_3() const { return ___mStarted_3; }
	inline bool* get_address_of_mStarted_3() { return &___mStarted_3; }
	inline void set_mStarted_3(bool value)
	{
		___mStarted_3 = value;
	}
};

struct LanguageSelection_t1927862481_StaticFields
{
public:
	// EventDelegate/Callback LanguageSelection::<>f__am$cache0
	Callback_t3139336517 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LanguageSelection_t1927862481_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Callback_t3139336517 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Callback_t3139336517 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Callback_t3139336517 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGESELECTION_T1927862481_H
#ifndef HOUSEADSCONTROLLER_T2212916833_H
#define HOUSEADSCONTROLLER_T2212916833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HouseAdsController
struct  HouseAdsController_t2212916833  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject HouseAdsController::dialog
	GameObject_t1113636619 * ___dialog_2;

public:
	inline static int32_t get_offset_of_dialog_2() { return static_cast<int32_t>(offsetof(HouseAdsController_t2212916833, ___dialog_2)); }
	inline GameObject_t1113636619 * get_dialog_2() const { return ___dialog_2; }
	inline GameObject_t1113636619 ** get_address_of_dialog_2() { return &___dialog_2; }
	inline void set_dialog_2(GameObject_t1113636619 * value)
	{
		___dialog_2 = value;
		Il2CppCodeGenWriteBarrier((&___dialog_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOUSEADSCONTROLLER_T2212916833_H
#ifndef COUNTERLABEL_T1397518286_H
#define COUNTERLABEL_T1397518286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterLabel
struct  CounterLabel_t1397518286  : public MonoBehaviour_t3962482529
{
public:
	// UISprite CounterLabel::_counterImage
	UISprite_t194114938 * ____counterImage_2;
	// System.String[] CounterLabel::_spriteName
	StringU5BU5D_t1281789340* ____spriteName_3;
	// System.Int32 CounterLabel::_countDown
	int32_t ____countDown_4;

public:
	inline static int32_t get_offset_of__counterImage_2() { return static_cast<int32_t>(offsetof(CounterLabel_t1397518286, ____counterImage_2)); }
	inline UISprite_t194114938 * get__counterImage_2() const { return ____counterImage_2; }
	inline UISprite_t194114938 ** get_address_of__counterImage_2() { return &____counterImage_2; }
	inline void set__counterImage_2(UISprite_t194114938 * value)
	{
		____counterImage_2 = value;
		Il2CppCodeGenWriteBarrier((&____counterImage_2), value);
	}

	inline static int32_t get_offset_of__spriteName_3() { return static_cast<int32_t>(offsetof(CounterLabel_t1397518286, ____spriteName_3)); }
	inline StringU5BU5D_t1281789340* get__spriteName_3() const { return ____spriteName_3; }
	inline StringU5BU5D_t1281789340** get_address_of__spriteName_3() { return &____spriteName_3; }
	inline void set__spriteName_3(StringU5BU5D_t1281789340* value)
	{
		____spriteName_3 = value;
		Il2CppCodeGenWriteBarrier((&____spriteName_3), value);
	}

	inline static int32_t get_offset_of__countDown_4() { return static_cast<int32_t>(offsetof(CounterLabel_t1397518286, ____countDown_4)); }
	inline int32_t get__countDown_4() const { return ____countDown_4; }
	inline int32_t* get_address_of__countDown_4() { return &____countDown_4; }
	inline void set__countDown_4(int32_t value)
	{
		____countDown_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTERLABEL_T1397518286_H
#ifndef BALLSELECTBUTTONASSISTANT_T2816453893_H
#define BALLSELECTBUTTONASSISTANT_T2816453893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallSelectButtonAssistant
struct  BallSelectButtonAssistant_t2816453893  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BallSelectButtonAssistant::_check
	GameObject_t1113636619 * ____check_2;
	// UnityEngine.GameObject BallSelectButtonAssistant::_icon
	GameObject_t1113636619 * ____icon_3;
	// UISprite BallSelectButtonAssistant::_sprite
	UISprite_t194114938 * ____sprite_4;
	// UIButton BallSelectButtonAssistant::_button
	UIButton_t1100396938 * ____button_5;
	// System.Int32 BallSelectButtonAssistant::idx
	int32_t ___idx_6;
	// System.String BallSelectButtonAssistant::name
	String_t* ___name_7;
	// System.String BallSelectButtonAssistant::nameLock
	String_t* ___nameLock_8;

public:
	inline static int32_t get_offset_of__check_2() { return static_cast<int32_t>(offsetof(BallSelectButtonAssistant_t2816453893, ____check_2)); }
	inline GameObject_t1113636619 * get__check_2() const { return ____check_2; }
	inline GameObject_t1113636619 ** get_address_of__check_2() { return &____check_2; }
	inline void set__check_2(GameObject_t1113636619 * value)
	{
		____check_2 = value;
		Il2CppCodeGenWriteBarrier((&____check_2), value);
	}

	inline static int32_t get_offset_of__icon_3() { return static_cast<int32_t>(offsetof(BallSelectButtonAssistant_t2816453893, ____icon_3)); }
	inline GameObject_t1113636619 * get__icon_3() const { return ____icon_3; }
	inline GameObject_t1113636619 ** get_address_of__icon_3() { return &____icon_3; }
	inline void set__icon_3(GameObject_t1113636619 * value)
	{
		____icon_3 = value;
		Il2CppCodeGenWriteBarrier((&____icon_3), value);
	}

	inline static int32_t get_offset_of__sprite_4() { return static_cast<int32_t>(offsetof(BallSelectButtonAssistant_t2816453893, ____sprite_4)); }
	inline UISprite_t194114938 * get__sprite_4() const { return ____sprite_4; }
	inline UISprite_t194114938 ** get_address_of__sprite_4() { return &____sprite_4; }
	inline void set__sprite_4(UISprite_t194114938 * value)
	{
		____sprite_4 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_4), value);
	}

	inline static int32_t get_offset_of__button_5() { return static_cast<int32_t>(offsetof(BallSelectButtonAssistant_t2816453893, ____button_5)); }
	inline UIButton_t1100396938 * get__button_5() const { return ____button_5; }
	inline UIButton_t1100396938 ** get_address_of__button_5() { return &____button_5; }
	inline void set__button_5(UIButton_t1100396938 * value)
	{
		____button_5 = value;
		Il2CppCodeGenWriteBarrier((&____button_5), value);
	}

	inline static int32_t get_offset_of_idx_6() { return static_cast<int32_t>(offsetof(BallSelectButtonAssistant_t2816453893, ___idx_6)); }
	inline int32_t get_idx_6() const { return ___idx_6; }
	inline int32_t* get_address_of_idx_6() { return &___idx_6; }
	inline void set_idx_6(int32_t value)
	{
		___idx_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(BallSelectButtonAssistant_t2816453893, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_nameLock_8() { return static_cast<int32_t>(offsetof(BallSelectButtonAssistant_t2816453893, ___nameLock_8)); }
	inline String_t* get_nameLock_8() const { return ___nameLock_8; }
	inline String_t** get_address_of_nameLock_8() { return &___nameLock_8; }
	inline void set_nameLock_8(String_t* value)
	{
		___nameLock_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameLock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLSELECTBUTTONASSISTANT_T2816453893_H
#ifndef BALLBACKBUTTONASSISSTANT_T2493153731_H
#define BALLBACKBUTTONASSISSTANT_T2493153731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallBackButtonAssisstant
struct  BallBackButtonAssisstant_t2493153731  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLBACKBUTTONASSISSTANT_T2493153731_H
#ifndef MONOBEHAVIOURWITHINIT_T1117120792_H
#define MONOBEHAVIOURWITHINIT_T1117120792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoBehaviourWithInit
struct  MonoBehaviourWithInit_t1117120792  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MonoBehaviourWithInit::_isInitialized
	bool ____isInitialized_2;

public:
	inline static int32_t get_offset_of__isInitialized_2() { return static_cast<int32_t>(offsetof(MonoBehaviourWithInit_t1117120792, ____isInitialized_2)); }
	inline bool get__isInitialized_2() const { return ____isInitialized_2; }
	inline bool* get_address_of__isInitialized_2() { return &____isInitialized_2; }
	inline void set__isInitialized_2(bool value)
	{
		____isInitialized_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURWITHINIT_T1117120792_H
#ifndef UIBUTTONACTIVATE_T1184781629_H
#define UIBUTTONACTIVATE_T1184781629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonActivate
struct  UIButtonActivate_t1184781629  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIButtonActivate::target
	GameObject_t1113636619 * ___target_2;
	// System.Boolean UIButtonActivate::state
	bool ___state_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIButtonActivate_t1184781629, ___target_2)); }
	inline GameObject_t1113636619 * get_target_2() const { return ___target_2; }
	inline GameObject_t1113636619 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t1113636619 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(UIButtonActivate_t1184781629, ___state_3)); }
	inline bool get_state_3() const { return ___state_3; }
	inline bool* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(bool value)
	{
		___state_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONACTIVATE_T1184781629_H
#ifndef UIDRAGRESIZE_T3151593629_H
#define UIDRAGRESIZE_T3151593629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragResize
struct  UIDragResize_t3151593629  : public MonoBehaviour_t3962482529
{
public:
	// UIWidget UIDragResize::target
	UIWidget_t3538521925 * ___target_2;
	// UIWidget/Pivot UIDragResize::pivot
	int32_t ___pivot_3;
	// System.Int32 UIDragResize::minWidth
	int32_t ___minWidth_4;
	// System.Int32 UIDragResize::minHeight
	int32_t ___minHeight_5;
	// System.Int32 UIDragResize::maxWidth
	int32_t ___maxWidth_6;
	// System.Int32 UIDragResize::maxHeight
	int32_t ___maxHeight_7;
	// System.Boolean UIDragResize::updateAnchors
	bool ___updateAnchors_8;
	// UnityEngine.Plane UIDragResize::mPlane
	Plane_t1000493321  ___mPlane_9;
	// UnityEngine.Vector3 UIDragResize::mRayPos
	Vector3_t3722313464  ___mRayPos_10;
	// UnityEngine.Vector3 UIDragResize::mLocalPos
	Vector3_t3722313464  ___mLocalPos_11;
	// System.Int32 UIDragResize::mWidth
	int32_t ___mWidth_12;
	// System.Int32 UIDragResize::mHeight
	int32_t ___mHeight_13;
	// System.Boolean UIDragResize::mDragging
	bool ___mDragging_14;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___target_2)); }
	inline UIWidget_t3538521925 * get_target_2() const { return ___target_2; }
	inline UIWidget_t3538521925 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(UIWidget_t3538521925 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_pivot_3() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___pivot_3)); }
	inline int32_t get_pivot_3() const { return ___pivot_3; }
	inline int32_t* get_address_of_pivot_3() { return &___pivot_3; }
	inline void set_pivot_3(int32_t value)
	{
		___pivot_3 = value;
	}

	inline static int32_t get_offset_of_minWidth_4() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___minWidth_4)); }
	inline int32_t get_minWidth_4() const { return ___minWidth_4; }
	inline int32_t* get_address_of_minWidth_4() { return &___minWidth_4; }
	inline void set_minWidth_4(int32_t value)
	{
		___minWidth_4 = value;
	}

	inline static int32_t get_offset_of_minHeight_5() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___minHeight_5)); }
	inline int32_t get_minHeight_5() const { return ___minHeight_5; }
	inline int32_t* get_address_of_minHeight_5() { return &___minHeight_5; }
	inline void set_minHeight_5(int32_t value)
	{
		___minHeight_5 = value;
	}

	inline static int32_t get_offset_of_maxWidth_6() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___maxWidth_6)); }
	inline int32_t get_maxWidth_6() const { return ___maxWidth_6; }
	inline int32_t* get_address_of_maxWidth_6() { return &___maxWidth_6; }
	inline void set_maxWidth_6(int32_t value)
	{
		___maxWidth_6 = value;
	}

	inline static int32_t get_offset_of_maxHeight_7() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___maxHeight_7)); }
	inline int32_t get_maxHeight_7() const { return ___maxHeight_7; }
	inline int32_t* get_address_of_maxHeight_7() { return &___maxHeight_7; }
	inline void set_maxHeight_7(int32_t value)
	{
		___maxHeight_7 = value;
	}

	inline static int32_t get_offset_of_updateAnchors_8() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___updateAnchors_8)); }
	inline bool get_updateAnchors_8() const { return ___updateAnchors_8; }
	inline bool* get_address_of_updateAnchors_8() { return &___updateAnchors_8; }
	inline void set_updateAnchors_8(bool value)
	{
		___updateAnchors_8 = value;
	}

	inline static int32_t get_offset_of_mPlane_9() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___mPlane_9)); }
	inline Plane_t1000493321  get_mPlane_9() const { return ___mPlane_9; }
	inline Plane_t1000493321 * get_address_of_mPlane_9() { return &___mPlane_9; }
	inline void set_mPlane_9(Plane_t1000493321  value)
	{
		___mPlane_9 = value;
	}

	inline static int32_t get_offset_of_mRayPos_10() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___mRayPos_10)); }
	inline Vector3_t3722313464  get_mRayPos_10() const { return ___mRayPos_10; }
	inline Vector3_t3722313464 * get_address_of_mRayPos_10() { return &___mRayPos_10; }
	inline void set_mRayPos_10(Vector3_t3722313464  value)
	{
		___mRayPos_10 = value;
	}

	inline static int32_t get_offset_of_mLocalPos_11() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___mLocalPos_11)); }
	inline Vector3_t3722313464  get_mLocalPos_11() const { return ___mLocalPos_11; }
	inline Vector3_t3722313464 * get_address_of_mLocalPos_11() { return &___mLocalPos_11; }
	inline void set_mLocalPos_11(Vector3_t3722313464  value)
	{
		___mLocalPos_11 = value;
	}

	inline static int32_t get_offset_of_mWidth_12() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___mWidth_12)); }
	inline int32_t get_mWidth_12() const { return ___mWidth_12; }
	inline int32_t* get_address_of_mWidth_12() { return &___mWidth_12; }
	inline void set_mWidth_12(int32_t value)
	{
		___mWidth_12 = value;
	}

	inline static int32_t get_offset_of_mHeight_13() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___mHeight_13)); }
	inline int32_t get_mHeight_13() const { return ___mHeight_13; }
	inline int32_t* get_address_of_mHeight_13() { return &___mHeight_13; }
	inline void set_mHeight_13(int32_t value)
	{
		___mHeight_13 = value;
	}

	inline static int32_t get_offset_of_mDragging_14() { return static_cast<int32_t>(offsetof(UIDragResize_t3151593629, ___mDragging_14)); }
	inline bool get_mDragging_14() const { return ___mDragging_14; }
	inline bool* get_address_of_mDragging_14() { return &___mDragging_14; }
	inline void set_mDragging_14(bool value)
	{
		___mDragging_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGRESIZE_T3151593629_H
#ifndef UIPLAYSOUND_T1382070071_H
#define UIPLAYSOUND_T1382070071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPlaySound
struct  UIPlaySound_t1382070071  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip UIPlaySound::audioClip
	AudioClip_t3680889665 * ___audioClip_2;
	// UIPlaySound/Trigger UIPlaySound::trigger
	int32_t ___trigger_3;
	// System.Single UIPlaySound::volume
	float ___volume_4;
	// System.Single UIPlaySound::pitch
	float ___pitch_5;
	// System.Boolean UIPlaySound::mIsOver
	bool ___mIsOver_6;

public:
	inline static int32_t get_offset_of_audioClip_2() { return static_cast<int32_t>(offsetof(UIPlaySound_t1382070071, ___audioClip_2)); }
	inline AudioClip_t3680889665 * get_audioClip_2() const { return ___audioClip_2; }
	inline AudioClip_t3680889665 ** get_address_of_audioClip_2() { return &___audioClip_2; }
	inline void set_audioClip_2(AudioClip_t3680889665 * value)
	{
		___audioClip_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioClip_2), value);
	}

	inline static int32_t get_offset_of_trigger_3() { return static_cast<int32_t>(offsetof(UIPlaySound_t1382070071, ___trigger_3)); }
	inline int32_t get_trigger_3() const { return ___trigger_3; }
	inline int32_t* get_address_of_trigger_3() { return &___trigger_3; }
	inline void set_trigger_3(int32_t value)
	{
		___trigger_3 = value;
	}

	inline static int32_t get_offset_of_volume_4() { return static_cast<int32_t>(offsetof(UIPlaySound_t1382070071, ___volume_4)); }
	inline float get_volume_4() const { return ___volume_4; }
	inline float* get_address_of_volume_4() { return &___volume_4; }
	inline void set_volume_4(float value)
	{
		___volume_4 = value;
	}

	inline static int32_t get_offset_of_pitch_5() { return static_cast<int32_t>(offsetof(UIPlaySound_t1382070071, ___pitch_5)); }
	inline float get_pitch_5() const { return ___pitch_5; }
	inline float* get_address_of_pitch_5() { return &___pitch_5; }
	inline void set_pitch_5(float value)
	{
		___pitch_5 = value;
	}

	inline static int32_t get_offset_of_mIsOver_6() { return static_cast<int32_t>(offsetof(UIPlaySound_t1382070071, ___mIsOver_6)); }
	inline bool get_mIsOver_6() const { return ___mIsOver_6; }
	inline bool* get_address_of_mIsOver_6() { return &___mIsOver_6; }
	inline void set_mIsOver_6(bool value)
	{
		___mIsOver_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPLAYSOUND_T1382070071_H
#ifndef UIBUTTONSCALE_T3973287916_H
#define UIBUTTONSCALE_T3973287916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonScale
struct  UIButtonScale_t3973287916  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UIButtonScale::tweenTarget
	Transform_t3600365921 * ___tweenTarget_2;
	// UnityEngine.Vector3 UIButtonScale::hover
	Vector3_t3722313464  ___hover_3;
	// UnityEngine.Vector3 UIButtonScale::pressed
	Vector3_t3722313464  ___pressed_4;
	// System.Single UIButtonScale::duration
	float ___duration_5;
	// UnityEngine.Vector3 UIButtonScale::mScale
	Vector3_t3722313464  ___mScale_6;
	// System.Boolean UIButtonScale::mStarted
	bool ___mStarted_7;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonScale_t3973287916, ___tweenTarget_2)); }
	inline Transform_t3600365921 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline Transform_t3600365921 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(Transform_t3600365921 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonScale_t3973287916, ___hover_3)); }
	inline Vector3_t3722313464  get_hover_3() const { return ___hover_3; }
	inline Vector3_t3722313464 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Vector3_t3722313464  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonScale_t3973287916, ___pressed_4)); }
	inline Vector3_t3722313464  get_pressed_4() const { return ___pressed_4; }
	inline Vector3_t3722313464 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Vector3_t3722313464  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(UIButtonScale_t3973287916, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mScale_6() { return static_cast<int32_t>(offsetof(UIButtonScale_t3973287916, ___mScale_6)); }
	inline Vector3_t3722313464  get_mScale_6() const { return ___mScale_6; }
	inline Vector3_t3722313464 * get_address_of_mScale_6() { return &___mScale_6; }
	inline void set_mScale_6(Vector3_t3722313464  value)
	{
		___mScale_6 = value;
	}

	inline static int32_t get_offset_of_mStarted_7() { return static_cast<int32_t>(offsetof(UIButtonScale_t3973287916, ___mStarted_7)); }
	inline bool get_mStarted_7() const { return ___mStarted_7; }
	inline bool* get_address_of_mStarted_7() { return &___mStarted_7; }
	inline void set_mStarted_7(bool value)
	{
		___mStarted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONSCALE_T3973287916_H
#ifndef UIPLAYANIMATION_T1976799768_H
#define UIPLAYANIMATION_T1976799768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPlayAnimation
struct  UIPlayAnimation_t1976799768  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animation UIPlayAnimation::target
	Animation_t3648466861 * ___target_3;
	// UnityEngine.Animator UIPlayAnimation::animator
	Animator_t434523843 * ___animator_4;
	// System.String UIPlayAnimation::clipName
	String_t* ___clipName_5;
	// AnimationOrTween.Trigger UIPlayAnimation::trigger
	int32_t ___trigger_6;
	// AnimationOrTween.Direction UIPlayAnimation::playDirection
	int32_t ___playDirection_7;
	// System.Boolean UIPlayAnimation::resetOnPlay
	bool ___resetOnPlay_8;
	// System.Boolean UIPlayAnimation::clearSelection
	bool ___clearSelection_9;
	// AnimationOrTween.EnableCondition UIPlayAnimation::ifDisabledOnPlay
	int32_t ___ifDisabledOnPlay_10;
	// AnimationOrTween.DisableCondition UIPlayAnimation::disableWhenFinished
	int32_t ___disableWhenFinished_11;
	// System.Collections.Generic.List`1<EventDelegate> UIPlayAnimation::onFinished
	List_1_t4210400802 * ___onFinished_12;
	// UnityEngine.GameObject UIPlayAnimation::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_13;
	// System.String UIPlayAnimation::callWhenFinished
	String_t* ___callWhenFinished_14;
	// System.Boolean UIPlayAnimation::mStarted
	bool ___mStarted_15;
	// System.Boolean UIPlayAnimation::mActivated
	bool ___mActivated_16;
	// System.Boolean UIPlayAnimation::dragHighlight
	bool ___dragHighlight_17;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___target_3)); }
	inline Animation_t3648466861 * get_target_3() const { return ___target_3; }
	inline Animation_t3648466861 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Animation_t3648466861 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_clipName_5() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___clipName_5)); }
	inline String_t* get_clipName_5() const { return ___clipName_5; }
	inline String_t** get_address_of_clipName_5() { return &___clipName_5; }
	inline void set_clipName_5(String_t* value)
	{
		___clipName_5 = value;
		Il2CppCodeGenWriteBarrier((&___clipName_5), value);
	}

	inline static int32_t get_offset_of_trigger_6() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___trigger_6)); }
	inline int32_t get_trigger_6() const { return ___trigger_6; }
	inline int32_t* get_address_of_trigger_6() { return &___trigger_6; }
	inline void set_trigger_6(int32_t value)
	{
		___trigger_6 = value;
	}

	inline static int32_t get_offset_of_playDirection_7() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___playDirection_7)); }
	inline int32_t get_playDirection_7() const { return ___playDirection_7; }
	inline int32_t* get_address_of_playDirection_7() { return &___playDirection_7; }
	inline void set_playDirection_7(int32_t value)
	{
		___playDirection_7 = value;
	}

	inline static int32_t get_offset_of_resetOnPlay_8() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___resetOnPlay_8)); }
	inline bool get_resetOnPlay_8() const { return ___resetOnPlay_8; }
	inline bool* get_address_of_resetOnPlay_8() { return &___resetOnPlay_8; }
	inline void set_resetOnPlay_8(bool value)
	{
		___resetOnPlay_8 = value;
	}

	inline static int32_t get_offset_of_clearSelection_9() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___clearSelection_9)); }
	inline bool get_clearSelection_9() const { return ___clearSelection_9; }
	inline bool* get_address_of_clearSelection_9() { return &___clearSelection_9; }
	inline void set_clearSelection_9(bool value)
	{
		___clearSelection_9 = value;
	}

	inline static int32_t get_offset_of_ifDisabledOnPlay_10() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___ifDisabledOnPlay_10)); }
	inline int32_t get_ifDisabledOnPlay_10() const { return ___ifDisabledOnPlay_10; }
	inline int32_t* get_address_of_ifDisabledOnPlay_10() { return &___ifDisabledOnPlay_10; }
	inline void set_ifDisabledOnPlay_10(int32_t value)
	{
		___ifDisabledOnPlay_10 = value;
	}

	inline static int32_t get_offset_of_disableWhenFinished_11() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___disableWhenFinished_11)); }
	inline int32_t get_disableWhenFinished_11() const { return ___disableWhenFinished_11; }
	inline int32_t* get_address_of_disableWhenFinished_11() { return &___disableWhenFinished_11; }
	inline void set_disableWhenFinished_11(int32_t value)
	{
		___disableWhenFinished_11 = value;
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___onFinished_12)); }
	inline List_1_t4210400802 * get_onFinished_12() const { return ___onFinished_12; }
	inline List_1_t4210400802 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(List_1_t4210400802 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_eventReceiver_13() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___eventReceiver_13)); }
	inline GameObject_t1113636619 * get_eventReceiver_13() const { return ___eventReceiver_13; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_13() { return &___eventReceiver_13; }
	inline void set_eventReceiver_13(GameObject_t1113636619 * value)
	{
		___eventReceiver_13 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_13), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_14() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___callWhenFinished_14)); }
	inline String_t* get_callWhenFinished_14() const { return ___callWhenFinished_14; }
	inline String_t** get_address_of_callWhenFinished_14() { return &___callWhenFinished_14; }
	inline void set_callWhenFinished_14(String_t* value)
	{
		___callWhenFinished_14 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_14), value);
	}

	inline static int32_t get_offset_of_mStarted_15() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___mStarted_15)); }
	inline bool get_mStarted_15() const { return ___mStarted_15; }
	inline bool* get_address_of_mStarted_15() { return &___mStarted_15; }
	inline void set_mStarted_15(bool value)
	{
		___mStarted_15 = value;
	}

	inline static int32_t get_offset_of_mActivated_16() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___mActivated_16)); }
	inline bool get_mActivated_16() const { return ___mActivated_16; }
	inline bool* get_address_of_mActivated_16() { return &___mActivated_16; }
	inline void set_mActivated_16(bool value)
	{
		___mActivated_16 = value;
	}

	inline static int32_t get_offset_of_dragHighlight_17() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768, ___dragHighlight_17)); }
	inline bool get_dragHighlight_17() const { return ___dragHighlight_17; }
	inline bool* get_address_of_dragHighlight_17() { return &___dragHighlight_17; }
	inline void set_dragHighlight_17(bool value)
	{
		___dragHighlight_17 = value;
	}
};

struct UIPlayAnimation_t1976799768_StaticFields
{
public:
	// UIPlayAnimation UIPlayAnimation::current
	UIPlayAnimation_t1976799768 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIPlayAnimation_t1976799768_StaticFields, ___current_2)); }
	inline UIPlayAnimation_t1976799768 * get_current_2() const { return ___current_2; }
	inline UIPlayAnimation_t1976799768 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIPlayAnimation_t1976799768 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPLAYANIMATION_T1976799768_H
#ifndef UIPLAYTWEEN_T2213781924_H
#define UIPLAYTWEEN_T2213781924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPlayTween
struct  UIPlayTween_t2213781924  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIPlayTween::tweenTarget
	GameObject_t1113636619 * ___tweenTarget_3;
	// System.Int32 UIPlayTween::tweenGroup
	int32_t ___tweenGroup_4;
	// AnimationOrTween.Trigger UIPlayTween::trigger
	int32_t ___trigger_5;
	// AnimationOrTween.Direction UIPlayTween::playDirection
	int32_t ___playDirection_6;
	// System.Boolean UIPlayTween::resetOnPlay
	bool ___resetOnPlay_7;
	// System.Boolean UIPlayTween::resetIfDisabled
	bool ___resetIfDisabled_8;
	// AnimationOrTween.EnableCondition UIPlayTween::ifDisabledOnPlay
	int32_t ___ifDisabledOnPlay_9;
	// AnimationOrTween.DisableCondition UIPlayTween::disableWhenFinished
	int32_t ___disableWhenFinished_10;
	// System.Boolean UIPlayTween::includeChildren
	bool ___includeChildren_11;
	// System.Collections.Generic.List`1<EventDelegate> UIPlayTween::onFinished
	List_1_t4210400802 * ___onFinished_12;
	// UnityEngine.GameObject UIPlayTween::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_13;
	// System.String UIPlayTween::callWhenFinished
	String_t* ___callWhenFinished_14;
	// UITweener[] UIPlayTween::mTweens
	UITweenerU5BU5D_t3440034099* ___mTweens_15;
	// System.Boolean UIPlayTween::mStarted
	bool ___mStarted_16;
	// System.Int32 UIPlayTween::mActive
	int32_t ___mActive_17;
	// System.Boolean UIPlayTween::mActivated
	bool ___mActivated_18;

public:
	inline static int32_t get_offset_of_tweenTarget_3() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___tweenTarget_3)); }
	inline GameObject_t1113636619 * get_tweenTarget_3() const { return ___tweenTarget_3; }
	inline GameObject_t1113636619 ** get_address_of_tweenTarget_3() { return &___tweenTarget_3; }
	inline void set_tweenTarget_3(GameObject_t1113636619 * value)
	{
		___tweenTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_3), value);
	}

	inline static int32_t get_offset_of_tweenGroup_4() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___tweenGroup_4)); }
	inline int32_t get_tweenGroup_4() const { return ___tweenGroup_4; }
	inline int32_t* get_address_of_tweenGroup_4() { return &___tweenGroup_4; }
	inline void set_tweenGroup_4(int32_t value)
	{
		___tweenGroup_4 = value;
	}

	inline static int32_t get_offset_of_trigger_5() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___trigger_5)); }
	inline int32_t get_trigger_5() const { return ___trigger_5; }
	inline int32_t* get_address_of_trigger_5() { return &___trigger_5; }
	inline void set_trigger_5(int32_t value)
	{
		___trigger_5 = value;
	}

	inline static int32_t get_offset_of_playDirection_6() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___playDirection_6)); }
	inline int32_t get_playDirection_6() const { return ___playDirection_6; }
	inline int32_t* get_address_of_playDirection_6() { return &___playDirection_6; }
	inline void set_playDirection_6(int32_t value)
	{
		___playDirection_6 = value;
	}

	inline static int32_t get_offset_of_resetOnPlay_7() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___resetOnPlay_7)); }
	inline bool get_resetOnPlay_7() const { return ___resetOnPlay_7; }
	inline bool* get_address_of_resetOnPlay_7() { return &___resetOnPlay_7; }
	inline void set_resetOnPlay_7(bool value)
	{
		___resetOnPlay_7 = value;
	}

	inline static int32_t get_offset_of_resetIfDisabled_8() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___resetIfDisabled_8)); }
	inline bool get_resetIfDisabled_8() const { return ___resetIfDisabled_8; }
	inline bool* get_address_of_resetIfDisabled_8() { return &___resetIfDisabled_8; }
	inline void set_resetIfDisabled_8(bool value)
	{
		___resetIfDisabled_8 = value;
	}

	inline static int32_t get_offset_of_ifDisabledOnPlay_9() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___ifDisabledOnPlay_9)); }
	inline int32_t get_ifDisabledOnPlay_9() const { return ___ifDisabledOnPlay_9; }
	inline int32_t* get_address_of_ifDisabledOnPlay_9() { return &___ifDisabledOnPlay_9; }
	inline void set_ifDisabledOnPlay_9(int32_t value)
	{
		___ifDisabledOnPlay_9 = value;
	}

	inline static int32_t get_offset_of_disableWhenFinished_10() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___disableWhenFinished_10)); }
	inline int32_t get_disableWhenFinished_10() const { return ___disableWhenFinished_10; }
	inline int32_t* get_address_of_disableWhenFinished_10() { return &___disableWhenFinished_10; }
	inline void set_disableWhenFinished_10(int32_t value)
	{
		___disableWhenFinished_10 = value;
	}

	inline static int32_t get_offset_of_includeChildren_11() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___includeChildren_11)); }
	inline bool get_includeChildren_11() const { return ___includeChildren_11; }
	inline bool* get_address_of_includeChildren_11() { return &___includeChildren_11; }
	inline void set_includeChildren_11(bool value)
	{
		___includeChildren_11 = value;
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___onFinished_12)); }
	inline List_1_t4210400802 * get_onFinished_12() const { return ___onFinished_12; }
	inline List_1_t4210400802 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(List_1_t4210400802 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_eventReceiver_13() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___eventReceiver_13)); }
	inline GameObject_t1113636619 * get_eventReceiver_13() const { return ___eventReceiver_13; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_13() { return &___eventReceiver_13; }
	inline void set_eventReceiver_13(GameObject_t1113636619 * value)
	{
		___eventReceiver_13 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_13), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_14() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___callWhenFinished_14)); }
	inline String_t* get_callWhenFinished_14() const { return ___callWhenFinished_14; }
	inline String_t** get_address_of_callWhenFinished_14() { return &___callWhenFinished_14; }
	inline void set_callWhenFinished_14(String_t* value)
	{
		___callWhenFinished_14 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_14), value);
	}

	inline static int32_t get_offset_of_mTweens_15() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___mTweens_15)); }
	inline UITweenerU5BU5D_t3440034099* get_mTweens_15() const { return ___mTweens_15; }
	inline UITweenerU5BU5D_t3440034099** get_address_of_mTweens_15() { return &___mTweens_15; }
	inline void set_mTweens_15(UITweenerU5BU5D_t3440034099* value)
	{
		___mTweens_15 = value;
		Il2CppCodeGenWriteBarrier((&___mTweens_15), value);
	}

	inline static int32_t get_offset_of_mStarted_16() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___mStarted_16)); }
	inline bool get_mStarted_16() const { return ___mStarted_16; }
	inline bool* get_address_of_mStarted_16() { return &___mStarted_16; }
	inline void set_mStarted_16(bool value)
	{
		___mStarted_16 = value;
	}

	inline static int32_t get_offset_of_mActive_17() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___mActive_17)); }
	inline int32_t get_mActive_17() const { return ___mActive_17; }
	inline int32_t* get_address_of_mActive_17() { return &___mActive_17; }
	inline void set_mActive_17(int32_t value)
	{
		___mActive_17 = value;
	}

	inline static int32_t get_offset_of_mActivated_18() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924, ___mActivated_18)); }
	inline bool get_mActivated_18() const { return ___mActivated_18; }
	inline bool* get_address_of_mActivated_18() { return &___mActivated_18; }
	inline void set_mActivated_18(bool value)
	{
		___mActivated_18 = value;
	}
};

struct UIPlayTween_t2213781924_StaticFields
{
public:
	// UIPlayTween UIPlayTween::current
	UIPlayTween_t2213781924 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIPlayTween_t2213781924_StaticFields, ___current_2)); }
	inline UIPlayTween_t2213781924 * get_current_2() const { return ___current_2; }
	inline UIPlayTween_t2213781924 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIPlayTween_t2213781924 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPLAYTWEEN_T2213781924_H
#ifndef UIKEYNAVIGATION_T4244956403_H
#define UIKEYNAVIGATION_T4244956403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIKeyNavigation
struct  UIKeyNavigation_t4244956403  : public MonoBehaviour_t3962482529
{
public:
	// UIKeyNavigation/Constraint UIKeyNavigation::constraint
	int32_t ___constraint_3;
	// UnityEngine.GameObject UIKeyNavigation::onUp
	GameObject_t1113636619 * ___onUp_4;
	// UnityEngine.GameObject UIKeyNavigation::onDown
	GameObject_t1113636619 * ___onDown_5;
	// UnityEngine.GameObject UIKeyNavigation::onLeft
	GameObject_t1113636619 * ___onLeft_6;
	// UnityEngine.GameObject UIKeyNavigation::onRight
	GameObject_t1113636619 * ___onRight_7;
	// UnityEngine.GameObject UIKeyNavigation::onClick
	GameObject_t1113636619 * ___onClick_8;
	// UnityEngine.GameObject UIKeyNavigation::onTab
	GameObject_t1113636619 * ___onTab_9;
	// System.Boolean UIKeyNavigation::startsSelected
	bool ___startsSelected_10;
	// System.Boolean UIKeyNavigation::mStarted
	bool ___mStarted_11;

public:
	inline static int32_t get_offset_of_constraint_3() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___constraint_3)); }
	inline int32_t get_constraint_3() const { return ___constraint_3; }
	inline int32_t* get_address_of_constraint_3() { return &___constraint_3; }
	inline void set_constraint_3(int32_t value)
	{
		___constraint_3 = value;
	}

	inline static int32_t get_offset_of_onUp_4() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___onUp_4)); }
	inline GameObject_t1113636619 * get_onUp_4() const { return ___onUp_4; }
	inline GameObject_t1113636619 ** get_address_of_onUp_4() { return &___onUp_4; }
	inline void set_onUp_4(GameObject_t1113636619 * value)
	{
		___onUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___onUp_4), value);
	}

	inline static int32_t get_offset_of_onDown_5() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___onDown_5)); }
	inline GameObject_t1113636619 * get_onDown_5() const { return ___onDown_5; }
	inline GameObject_t1113636619 ** get_address_of_onDown_5() { return &___onDown_5; }
	inline void set_onDown_5(GameObject_t1113636619 * value)
	{
		___onDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___onDown_5), value);
	}

	inline static int32_t get_offset_of_onLeft_6() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___onLeft_6)); }
	inline GameObject_t1113636619 * get_onLeft_6() const { return ___onLeft_6; }
	inline GameObject_t1113636619 ** get_address_of_onLeft_6() { return &___onLeft_6; }
	inline void set_onLeft_6(GameObject_t1113636619 * value)
	{
		___onLeft_6 = value;
		Il2CppCodeGenWriteBarrier((&___onLeft_6), value);
	}

	inline static int32_t get_offset_of_onRight_7() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___onRight_7)); }
	inline GameObject_t1113636619 * get_onRight_7() const { return ___onRight_7; }
	inline GameObject_t1113636619 ** get_address_of_onRight_7() { return &___onRight_7; }
	inline void set_onRight_7(GameObject_t1113636619 * value)
	{
		___onRight_7 = value;
		Il2CppCodeGenWriteBarrier((&___onRight_7), value);
	}

	inline static int32_t get_offset_of_onClick_8() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___onClick_8)); }
	inline GameObject_t1113636619 * get_onClick_8() const { return ___onClick_8; }
	inline GameObject_t1113636619 ** get_address_of_onClick_8() { return &___onClick_8; }
	inline void set_onClick_8(GameObject_t1113636619 * value)
	{
		___onClick_8 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_8), value);
	}

	inline static int32_t get_offset_of_onTab_9() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___onTab_9)); }
	inline GameObject_t1113636619 * get_onTab_9() const { return ___onTab_9; }
	inline GameObject_t1113636619 ** get_address_of_onTab_9() { return &___onTab_9; }
	inline void set_onTab_9(GameObject_t1113636619 * value)
	{
		___onTab_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTab_9), value);
	}

	inline static int32_t get_offset_of_startsSelected_10() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___startsSelected_10)); }
	inline bool get_startsSelected_10() const { return ___startsSelected_10; }
	inline bool* get_address_of_startsSelected_10() { return &___startsSelected_10; }
	inline void set_startsSelected_10(bool value)
	{
		___startsSelected_10 = value;
	}

	inline static int32_t get_offset_of_mStarted_11() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403, ___mStarted_11)); }
	inline bool get_mStarted_11() const { return ___mStarted_11; }
	inline bool* get_address_of_mStarted_11() { return &___mStarted_11; }
	inline void set_mStarted_11(bool value)
	{
		___mStarted_11 = value;
	}
};

struct UIKeyNavigation_t4244956403_StaticFields
{
public:
	// BetterList`1<UIKeyNavigation> UIKeyNavigation::list
	BetterList_1_t3399976721 * ___list_2;
	// System.Int32 UIKeyNavigation::mLastFrame
	int32_t ___mLastFrame_12;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403_StaticFields, ___list_2)); }
	inline BetterList_1_t3399976721 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t3399976721 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t3399976721 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of_mLastFrame_12() { return static_cast<int32_t>(offsetof(UIKeyNavigation_t4244956403_StaticFields, ___mLastFrame_12)); }
	inline int32_t get_mLastFrame_12() const { return ___mLastFrame_12; }
	inline int32_t* get_address_of_mLastFrame_12() { return &___mLastFrame_12; }
	inline void set_mLastFrame_12(int32_t value)
	{
		___mLastFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIKEYNAVIGATION_T4244956403_H
#ifndef UISOUNDVOLUME_T313711476_H
#define UISOUNDVOLUME_T313711476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISoundVolume
struct  UISoundVolume_t313711476  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISOUNDVOLUME_T313711476_H
#ifndef UIDRAGDROPCONTAINER_T2199185397_H
#define UIDRAGDROPCONTAINER_T2199185397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragDropContainer
struct  UIDragDropContainer_t2199185397  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UIDragDropContainer::reparentTarget
	Transform_t3600365921 * ___reparentTarget_2;

public:
	inline static int32_t get_offset_of_reparentTarget_2() { return static_cast<int32_t>(offsetof(UIDragDropContainer_t2199185397, ___reparentTarget_2)); }
	inline Transform_t3600365921 * get_reparentTarget_2() const { return ___reparentTarget_2; }
	inline Transform_t3600365921 ** get_address_of_reparentTarget_2() { return &___reparentTarget_2; }
	inline void set_reparentTarget_2(Transform_t3600365921 * value)
	{
		___reparentTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___reparentTarget_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGDROPCONTAINER_T2199185397_H
#ifndef UIDRAGDROPROOT_T3716445314_H
#define UIDRAGDROPROOT_T3716445314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragDropRoot
struct  UIDragDropRoot_t3716445314  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct UIDragDropRoot_t3716445314_StaticFields
{
public:
	// UnityEngine.Transform UIDragDropRoot::root
	Transform_t3600365921 * ___root_2;

public:
	inline static int32_t get_offset_of_root_2() { return static_cast<int32_t>(offsetof(UIDragDropRoot_t3716445314_StaticFields, ___root_2)); }
	inline Transform_t3600365921 * get_root_2() const { return ___root_2; }
	inline Transform_t3600365921 ** get_address_of_root_2() { return &___root_2; }
	inline void set_root_2(Transform_t3600365921 * value)
	{
		___root_2 = value;
		Il2CppCodeGenWriteBarrier((&___root_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGDROPROOT_T3716445314_H
#ifndef UIDRAGCAMERA_T3716731078_H
#define UIDRAGCAMERA_T3716731078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragCamera
struct  UIDragCamera_t3716731078  : public MonoBehaviour_t3962482529
{
public:
	// UIDraggableCamera UIDragCamera::draggableCamera
	UIDraggableCamera_t1644204495 * ___draggableCamera_2;

public:
	inline static int32_t get_offset_of_draggableCamera_2() { return static_cast<int32_t>(offsetof(UIDragCamera_t3716731078, ___draggableCamera_2)); }
	inline UIDraggableCamera_t1644204495 * get_draggableCamera_2() const { return ___draggableCamera_2; }
	inline UIDraggableCamera_t1644204495 ** get_address_of_draggableCamera_2() { return &___draggableCamera_2; }
	inline void set_draggableCamera_2(UIDraggableCamera_t1644204495 * value)
	{
		___draggableCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___draggableCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGCAMERA_T3716731078_H
#ifndef UIDRAGGABLECAMERA_T1644204495_H
#define UIDRAGGABLECAMERA_T1644204495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDraggableCamera
struct  UIDraggableCamera_t1644204495  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UIDraggableCamera::rootForBounds
	Transform_t3600365921 * ___rootForBounds_2;
	// UnityEngine.Vector2 UIDraggableCamera::scale
	Vector2_t2156229523  ___scale_3;
	// System.Single UIDraggableCamera::scrollWheelFactor
	float ___scrollWheelFactor_4;
	// UIDragObject/DragEffect UIDraggableCamera::dragEffect
	int32_t ___dragEffect_5;
	// System.Boolean UIDraggableCamera::smoothDragStart
	bool ___smoothDragStart_6;
	// System.Single UIDraggableCamera::momentumAmount
	float ___momentumAmount_7;
	// UnityEngine.Camera UIDraggableCamera::mCam
	Camera_t4157153871 * ___mCam_8;
	// UnityEngine.Transform UIDraggableCamera::mTrans
	Transform_t3600365921 * ___mTrans_9;
	// System.Boolean UIDraggableCamera::mPressed
	bool ___mPressed_10;
	// UnityEngine.Vector2 UIDraggableCamera::mMomentum
	Vector2_t2156229523  ___mMomentum_11;
	// UnityEngine.Bounds UIDraggableCamera::mBounds
	Bounds_t2266837910  ___mBounds_12;
	// System.Single UIDraggableCamera::mScroll
	float ___mScroll_13;
	// UIRoot UIDraggableCamera::mRoot
	UIRoot_t4022971450 * ___mRoot_14;
	// System.Boolean UIDraggableCamera::mDragStarted
	bool ___mDragStarted_15;

public:
	inline static int32_t get_offset_of_rootForBounds_2() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___rootForBounds_2)); }
	inline Transform_t3600365921 * get_rootForBounds_2() const { return ___rootForBounds_2; }
	inline Transform_t3600365921 ** get_address_of_rootForBounds_2() { return &___rootForBounds_2; }
	inline void set_rootForBounds_2(Transform_t3600365921 * value)
	{
		___rootForBounds_2 = value;
		Il2CppCodeGenWriteBarrier((&___rootForBounds_2), value);
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___scale_3)); }
	inline Vector2_t2156229523  get_scale_3() const { return ___scale_3; }
	inline Vector2_t2156229523 * get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(Vector2_t2156229523  value)
	{
		___scale_3 = value;
	}

	inline static int32_t get_offset_of_scrollWheelFactor_4() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___scrollWheelFactor_4)); }
	inline float get_scrollWheelFactor_4() const { return ___scrollWheelFactor_4; }
	inline float* get_address_of_scrollWheelFactor_4() { return &___scrollWheelFactor_4; }
	inline void set_scrollWheelFactor_4(float value)
	{
		___scrollWheelFactor_4 = value;
	}

	inline static int32_t get_offset_of_dragEffect_5() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___dragEffect_5)); }
	inline int32_t get_dragEffect_5() const { return ___dragEffect_5; }
	inline int32_t* get_address_of_dragEffect_5() { return &___dragEffect_5; }
	inline void set_dragEffect_5(int32_t value)
	{
		___dragEffect_5 = value;
	}

	inline static int32_t get_offset_of_smoothDragStart_6() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___smoothDragStart_6)); }
	inline bool get_smoothDragStart_6() const { return ___smoothDragStart_6; }
	inline bool* get_address_of_smoothDragStart_6() { return &___smoothDragStart_6; }
	inline void set_smoothDragStart_6(bool value)
	{
		___smoothDragStart_6 = value;
	}

	inline static int32_t get_offset_of_momentumAmount_7() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___momentumAmount_7)); }
	inline float get_momentumAmount_7() const { return ___momentumAmount_7; }
	inline float* get_address_of_momentumAmount_7() { return &___momentumAmount_7; }
	inline void set_momentumAmount_7(float value)
	{
		___momentumAmount_7 = value;
	}

	inline static int32_t get_offset_of_mCam_8() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mCam_8)); }
	inline Camera_t4157153871 * get_mCam_8() const { return ___mCam_8; }
	inline Camera_t4157153871 ** get_address_of_mCam_8() { return &___mCam_8; }
	inline void set_mCam_8(Camera_t4157153871 * value)
	{
		___mCam_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_8), value);
	}

	inline static int32_t get_offset_of_mTrans_9() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mTrans_9)); }
	inline Transform_t3600365921 * get_mTrans_9() const { return ___mTrans_9; }
	inline Transform_t3600365921 ** get_address_of_mTrans_9() { return &___mTrans_9; }
	inline void set_mTrans_9(Transform_t3600365921 * value)
	{
		___mTrans_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_9), value);
	}

	inline static int32_t get_offset_of_mPressed_10() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mPressed_10)); }
	inline bool get_mPressed_10() const { return ___mPressed_10; }
	inline bool* get_address_of_mPressed_10() { return &___mPressed_10; }
	inline void set_mPressed_10(bool value)
	{
		___mPressed_10 = value;
	}

	inline static int32_t get_offset_of_mMomentum_11() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mMomentum_11)); }
	inline Vector2_t2156229523  get_mMomentum_11() const { return ___mMomentum_11; }
	inline Vector2_t2156229523 * get_address_of_mMomentum_11() { return &___mMomentum_11; }
	inline void set_mMomentum_11(Vector2_t2156229523  value)
	{
		___mMomentum_11 = value;
	}

	inline static int32_t get_offset_of_mBounds_12() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mBounds_12)); }
	inline Bounds_t2266837910  get_mBounds_12() const { return ___mBounds_12; }
	inline Bounds_t2266837910 * get_address_of_mBounds_12() { return &___mBounds_12; }
	inline void set_mBounds_12(Bounds_t2266837910  value)
	{
		___mBounds_12 = value;
	}

	inline static int32_t get_offset_of_mScroll_13() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mScroll_13)); }
	inline float get_mScroll_13() const { return ___mScroll_13; }
	inline float* get_address_of_mScroll_13() { return &___mScroll_13; }
	inline void set_mScroll_13(float value)
	{
		___mScroll_13 = value;
	}

	inline static int32_t get_offset_of_mRoot_14() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mRoot_14)); }
	inline UIRoot_t4022971450 * get_mRoot_14() const { return ___mRoot_14; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_14() { return &___mRoot_14; }
	inline void set_mRoot_14(UIRoot_t4022971450 * value)
	{
		___mRoot_14 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_14), value);
	}

	inline static int32_t get_offset_of_mDragStarted_15() { return static_cast<int32_t>(offsetof(UIDraggableCamera_t1644204495, ___mDragStarted_15)); }
	inline bool get_mDragStarted_15() const { return ___mDragStarted_15; }
	inline bool* get_address_of_mDragStarted_15() { return &___mDragStarted_15; }
	inline void set_mDragStarted_15(bool value)
	{
		___mDragStarted_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGGABLECAMERA_T1644204495_H
#ifndef UICENTERONCLICK_T4058804012_H
#define UICENTERONCLICK_T4058804012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICenterOnClick
struct  UICenterOnClick_t4058804012  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICENTERONCLICK_T4058804012_H
#ifndef UICENTERONCHILD_T253063637_H
#define UICENTERONCHILD_T253063637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICenterOnChild
struct  UICenterOnChild_t253063637  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UICenterOnChild::springStrength
	float ___springStrength_2;
	// System.Single UICenterOnChild::nextPageThreshold
	float ___nextPageThreshold_3;
	// SpringPanel/OnFinished UICenterOnChild::onFinished
	OnFinished_t3778785451 * ___onFinished_4;
	// UICenterOnChild/OnCenterCallback UICenterOnChild::onCenter
	OnCenterCallback_t2077531662 * ___onCenter_5;
	// UIScrollView UICenterOnChild::mScrollView
	UIScrollView_t1973404950 * ___mScrollView_6;
	// UnityEngine.GameObject UICenterOnChild::mCenteredObject
	GameObject_t1113636619 * ___mCenteredObject_7;

public:
	inline static int32_t get_offset_of_springStrength_2() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___springStrength_2)); }
	inline float get_springStrength_2() const { return ___springStrength_2; }
	inline float* get_address_of_springStrength_2() { return &___springStrength_2; }
	inline void set_springStrength_2(float value)
	{
		___springStrength_2 = value;
	}

	inline static int32_t get_offset_of_nextPageThreshold_3() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___nextPageThreshold_3)); }
	inline float get_nextPageThreshold_3() const { return ___nextPageThreshold_3; }
	inline float* get_address_of_nextPageThreshold_3() { return &___nextPageThreshold_3; }
	inline void set_nextPageThreshold_3(float value)
	{
		___nextPageThreshold_3 = value;
	}

	inline static int32_t get_offset_of_onFinished_4() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___onFinished_4)); }
	inline OnFinished_t3778785451 * get_onFinished_4() const { return ___onFinished_4; }
	inline OnFinished_t3778785451 ** get_address_of_onFinished_4() { return &___onFinished_4; }
	inline void set_onFinished_4(OnFinished_t3778785451 * value)
	{
		___onFinished_4 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_4), value);
	}

	inline static int32_t get_offset_of_onCenter_5() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___onCenter_5)); }
	inline OnCenterCallback_t2077531662 * get_onCenter_5() const { return ___onCenter_5; }
	inline OnCenterCallback_t2077531662 ** get_address_of_onCenter_5() { return &___onCenter_5; }
	inline void set_onCenter_5(OnCenterCallback_t2077531662 * value)
	{
		___onCenter_5 = value;
		Il2CppCodeGenWriteBarrier((&___onCenter_5), value);
	}

	inline static int32_t get_offset_of_mScrollView_6() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___mScrollView_6)); }
	inline UIScrollView_t1973404950 * get_mScrollView_6() const { return ___mScrollView_6; }
	inline UIScrollView_t1973404950 ** get_address_of_mScrollView_6() { return &___mScrollView_6; }
	inline void set_mScrollView_6(UIScrollView_t1973404950 * value)
	{
		___mScrollView_6 = value;
		Il2CppCodeGenWriteBarrier((&___mScrollView_6), value);
	}

	inline static int32_t get_offset_of_mCenteredObject_7() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___mCenteredObject_7)); }
	inline GameObject_t1113636619 * get_mCenteredObject_7() const { return ___mCenteredObject_7; }
	inline GameObject_t1113636619 ** get_address_of_mCenteredObject_7() { return &___mCenteredObject_7; }
	inline void set_mCenteredObject_7(GameObject_t1113636619 * value)
	{
		___mCenteredObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCenteredObject_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICENTERONCHILD_T253063637_H
#ifndef UIKEYBINDING_T2698445843_H
#define UIKEYBINDING_T2698445843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIKeyBinding
struct  UIKeyBinding_t2698445843  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode UIKeyBinding::keyCode
	int32_t ___keyCode_3;
	// UIKeyBinding/Modifier UIKeyBinding::modifier
	int32_t ___modifier_4;
	// UIKeyBinding/Action UIKeyBinding::action
	int32_t ___action_5;
	// System.Boolean UIKeyBinding::mIgnoreUp
	bool ___mIgnoreUp_6;
	// System.Boolean UIKeyBinding::mIsInput
	bool ___mIsInput_7;
	// System.Boolean UIKeyBinding::mPress
	bool ___mPress_8;

public:
	inline static int32_t get_offset_of_keyCode_3() { return static_cast<int32_t>(offsetof(UIKeyBinding_t2698445843, ___keyCode_3)); }
	inline int32_t get_keyCode_3() const { return ___keyCode_3; }
	inline int32_t* get_address_of_keyCode_3() { return &___keyCode_3; }
	inline void set_keyCode_3(int32_t value)
	{
		___keyCode_3 = value;
	}

	inline static int32_t get_offset_of_modifier_4() { return static_cast<int32_t>(offsetof(UIKeyBinding_t2698445843, ___modifier_4)); }
	inline int32_t get_modifier_4() const { return ___modifier_4; }
	inline int32_t* get_address_of_modifier_4() { return &___modifier_4; }
	inline void set_modifier_4(int32_t value)
	{
		___modifier_4 = value;
	}

	inline static int32_t get_offset_of_action_5() { return static_cast<int32_t>(offsetof(UIKeyBinding_t2698445843, ___action_5)); }
	inline int32_t get_action_5() const { return ___action_5; }
	inline int32_t* get_address_of_action_5() { return &___action_5; }
	inline void set_action_5(int32_t value)
	{
		___action_5 = value;
	}

	inline static int32_t get_offset_of_mIgnoreUp_6() { return static_cast<int32_t>(offsetof(UIKeyBinding_t2698445843, ___mIgnoreUp_6)); }
	inline bool get_mIgnoreUp_6() const { return ___mIgnoreUp_6; }
	inline bool* get_address_of_mIgnoreUp_6() { return &___mIgnoreUp_6; }
	inline void set_mIgnoreUp_6(bool value)
	{
		___mIgnoreUp_6 = value;
	}

	inline static int32_t get_offset_of_mIsInput_7() { return static_cast<int32_t>(offsetof(UIKeyBinding_t2698445843, ___mIsInput_7)); }
	inline bool get_mIsInput_7() const { return ___mIsInput_7; }
	inline bool* get_address_of_mIsInput_7() { return &___mIsInput_7; }
	inline void set_mIsInput_7(bool value)
	{
		___mIsInput_7 = value;
	}

	inline static int32_t get_offset_of_mPress_8() { return static_cast<int32_t>(offsetof(UIKeyBinding_t2698445843, ___mPress_8)); }
	inline bool get_mPress_8() const { return ___mPress_8; }
	inline bool* get_address_of_mPress_8() { return &___mPress_8; }
	inline void set_mPress_8(bool value)
	{
		___mPress_8 = value;
	}
};

struct UIKeyBinding_t2698445843_StaticFields
{
public:
	// System.Collections.Generic.List`1<UIKeyBinding> UIKeyBinding::mList
	List_1_t4170520585 * ___mList_2;

public:
	inline static int32_t get_offset_of_mList_2() { return static_cast<int32_t>(offsetof(UIKeyBinding_t2698445843_StaticFields, ___mList_2)); }
	inline List_1_t4170520585 * get_mList_2() const { return ___mList_2; }
	inline List_1_t4170520585 ** get_address_of_mList_2() { return &___mList_2; }
	inline void set_mList_2(List_1_t4170520585 * value)
	{
		___mList_2 = value;
		Il2CppCodeGenWriteBarrier((&___mList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIKEYBINDING_T2698445843_H
#ifndef UIDRAGDROPITEM_T3922849381_H
#define UIDRAGDROPITEM_T3922849381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragDropItem
struct  UIDragDropItem_t3922849381  : public MonoBehaviour_t3962482529
{
public:
	// UIDragDropItem/Restriction UIDragDropItem::restriction
	int32_t ___restriction_2;
	// System.Boolean UIDragDropItem::cloneOnDrag
	bool ___cloneOnDrag_3;
	// System.Single UIDragDropItem::pressAndHoldDelay
	float ___pressAndHoldDelay_4;
	// System.Boolean UIDragDropItem::interactable
	bool ___interactable_5;
	// UnityEngine.Transform UIDragDropItem::mTrans
	Transform_t3600365921 * ___mTrans_6;
	// UnityEngine.Transform UIDragDropItem::mParent
	Transform_t3600365921 * ___mParent_7;
	// UnityEngine.Collider UIDragDropItem::mCollider
	Collider_t1773347010 * ___mCollider_8;
	// UnityEngine.Collider2D UIDragDropItem::mCollider2D
	Collider2D_t2806799626 * ___mCollider2D_9;
	// UIButton UIDragDropItem::mButton
	UIButton_t1100396938 * ___mButton_10;
	// UIRoot UIDragDropItem::mRoot
	UIRoot_t4022971450 * ___mRoot_11;
	// UIGrid UIDragDropItem::mGrid
	UIGrid_t1536638187 * ___mGrid_12;
	// UITable UIDragDropItem::mTable
	UITable_t3168834800 * ___mTable_13;
	// System.Single UIDragDropItem::mDragStartTime
	float ___mDragStartTime_14;
	// UIDragScrollView UIDragDropItem::mDragScrollView
	UIDragScrollView_t2492060641 * ___mDragScrollView_15;
	// System.Boolean UIDragDropItem::mPressed
	bool ___mPressed_16;
	// System.Boolean UIDragDropItem::mDragging
	bool ___mDragging_17;
	// UICamera/MouseOrTouch UIDragDropItem::mTouch
	MouseOrTouch_t3052596533 * ___mTouch_18;

public:
	inline static int32_t get_offset_of_restriction_2() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___restriction_2)); }
	inline int32_t get_restriction_2() const { return ___restriction_2; }
	inline int32_t* get_address_of_restriction_2() { return &___restriction_2; }
	inline void set_restriction_2(int32_t value)
	{
		___restriction_2 = value;
	}

	inline static int32_t get_offset_of_cloneOnDrag_3() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___cloneOnDrag_3)); }
	inline bool get_cloneOnDrag_3() const { return ___cloneOnDrag_3; }
	inline bool* get_address_of_cloneOnDrag_3() { return &___cloneOnDrag_3; }
	inline void set_cloneOnDrag_3(bool value)
	{
		___cloneOnDrag_3 = value;
	}

	inline static int32_t get_offset_of_pressAndHoldDelay_4() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___pressAndHoldDelay_4)); }
	inline float get_pressAndHoldDelay_4() const { return ___pressAndHoldDelay_4; }
	inline float* get_address_of_pressAndHoldDelay_4() { return &___pressAndHoldDelay_4; }
	inline void set_pressAndHoldDelay_4(float value)
	{
		___pressAndHoldDelay_4 = value;
	}

	inline static int32_t get_offset_of_interactable_5() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___interactable_5)); }
	inline bool get_interactable_5() const { return ___interactable_5; }
	inline bool* get_address_of_interactable_5() { return &___interactable_5; }
	inline void set_interactable_5(bool value)
	{
		___interactable_5 = value;
	}

	inline static int32_t get_offset_of_mTrans_6() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mTrans_6)); }
	inline Transform_t3600365921 * get_mTrans_6() const { return ___mTrans_6; }
	inline Transform_t3600365921 ** get_address_of_mTrans_6() { return &___mTrans_6; }
	inline void set_mTrans_6(Transform_t3600365921 * value)
	{
		___mTrans_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_6), value);
	}

	inline static int32_t get_offset_of_mParent_7() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mParent_7)); }
	inline Transform_t3600365921 * get_mParent_7() const { return ___mParent_7; }
	inline Transform_t3600365921 ** get_address_of_mParent_7() { return &___mParent_7; }
	inline void set_mParent_7(Transform_t3600365921 * value)
	{
		___mParent_7 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_7), value);
	}

	inline static int32_t get_offset_of_mCollider_8() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mCollider_8)); }
	inline Collider_t1773347010 * get_mCollider_8() const { return ___mCollider_8; }
	inline Collider_t1773347010 ** get_address_of_mCollider_8() { return &___mCollider_8; }
	inline void set_mCollider_8(Collider_t1773347010 * value)
	{
		___mCollider_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCollider_8), value);
	}

	inline static int32_t get_offset_of_mCollider2D_9() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mCollider2D_9)); }
	inline Collider2D_t2806799626 * get_mCollider2D_9() const { return ___mCollider2D_9; }
	inline Collider2D_t2806799626 ** get_address_of_mCollider2D_9() { return &___mCollider2D_9; }
	inline void set_mCollider2D_9(Collider2D_t2806799626 * value)
	{
		___mCollider2D_9 = value;
		Il2CppCodeGenWriteBarrier((&___mCollider2D_9), value);
	}

	inline static int32_t get_offset_of_mButton_10() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mButton_10)); }
	inline UIButton_t1100396938 * get_mButton_10() const { return ___mButton_10; }
	inline UIButton_t1100396938 ** get_address_of_mButton_10() { return &___mButton_10; }
	inline void set_mButton_10(UIButton_t1100396938 * value)
	{
		___mButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___mButton_10), value);
	}

	inline static int32_t get_offset_of_mRoot_11() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mRoot_11)); }
	inline UIRoot_t4022971450 * get_mRoot_11() const { return ___mRoot_11; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_11() { return &___mRoot_11; }
	inline void set_mRoot_11(UIRoot_t4022971450 * value)
	{
		___mRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_11), value);
	}

	inline static int32_t get_offset_of_mGrid_12() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mGrid_12)); }
	inline UIGrid_t1536638187 * get_mGrid_12() const { return ___mGrid_12; }
	inline UIGrid_t1536638187 ** get_address_of_mGrid_12() { return &___mGrid_12; }
	inline void set_mGrid_12(UIGrid_t1536638187 * value)
	{
		___mGrid_12 = value;
		Il2CppCodeGenWriteBarrier((&___mGrid_12), value);
	}

	inline static int32_t get_offset_of_mTable_13() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mTable_13)); }
	inline UITable_t3168834800 * get_mTable_13() const { return ___mTable_13; }
	inline UITable_t3168834800 ** get_address_of_mTable_13() { return &___mTable_13; }
	inline void set_mTable_13(UITable_t3168834800 * value)
	{
		___mTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTable_13), value);
	}

	inline static int32_t get_offset_of_mDragStartTime_14() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mDragStartTime_14)); }
	inline float get_mDragStartTime_14() const { return ___mDragStartTime_14; }
	inline float* get_address_of_mDragStartTime_14() { return &___mDragStartTime_14; }
	inline void set_mDragStartTime_14(float value)
	{
		___mDragStartTime_14 = value;
	}

	inline static int32_t get_offset_of_mDragScrollView_15() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mDragScrollView_15)); }
	inline UIDragScrollView_t2492060641 * get_mDragScrollView_15() const { return ___mDragScrollView_15; }
	inline UIDragScrollView_t2492060641 ** get_address_of_mDragScrollView_15() { return &___mDragScrollView_15; }
	inline void set_mDragScrollView_15(UIDragScrollView_t2492060641 * value)
	{
		___mDragScrollView_15 = value;
		Il2CppCodeGenWriteBarrier((&___mDragScrollView_15), value);
	}

	inline static int32_t get_offset_of_mPressed_16() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mPressed_16)); }
	inline bool get_mPressed_16() const { return ___mPressed_16; }
	inline bool* get_address_of_mPressed_16() { return &___mPressed_16; }
	inline void set_mPressed_16(bool value)
	{
		___mPressed_16 = value;
	}

	inline static int32_t get_offset_of_mDragging_17() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mDragging_17)); }
	inline bool get_mDragging_17() const { return ___mDragging_17; }
	inline bool* get_address_of_mDragging_17() { return &___mDragging_17; }
	inline void set_mDragging_17(bool value)
	{
		___mDragging_17 = value;
	}

	inline static int32_t get_offset_of_mTouch_18() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381, ___mTouch_18)); }
	inline MouseOrTouch_t3052596533 * get_mTouch_18() const { return ___mTouch_18; }
	inline MouseOrTouch_t3052596533 ** get_address_of_mTouch_18() { return &___mTouch_18; }
	inline void set_mTouch_18(MouseOrTouch_t3052596533 * value)
	{
		___mTouch_18 = value;
		Il2CppCodeGenWriteBarrier((&___mTouch_18), value);
	}
};

struct UIDragDropItem_t3922849381_StaticFields
{
public:
	// System.Collections.Generic.List`1<UIDragDropItem> UIDragDropItem::draggedItems
	List_1_t1099956827 * ___draggedItems_19;

public:
	inline static int32_t get_offset_of_draggedItems_19() { return static_cast<int32_t>(offsetof(UIDragDropItem_t3922849381_StaticFields, ___draggedItems_19)); }
	inline List_1_t1099956827 * get_draggedItems_19() const { return ___draggedItems_19; }
	inline List_1_t1099956827 ** get_address_of_draggedItems_19() { return &___draggedItems_19; }
	inline void set_draggedItems_19(List_1_t1099956827 * value)
	{
		___draggedItems_19 = value;
		Il2CppCodeGenWriteBarrier((&___draggedItems_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGDROPITEM_T3922849381_H
#ifndef UIIMAGEBUTTON_T3745027676_H
#define UIIMAGEBUTTON_T3745027676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIImageButton
struct  UIImageButton_t3745027676  : public MonoBehaviour_t3962482529
{
public:
	// UISprite UIImageButton::target
	UISprite_t194114938 * ___target_2;
	// System.String UIImageButton::normalSprite
	String_t* ___normalSprite_3;
	// System.String UIImageButton::hoverSprite
	String_t* ___hoverSprite_4;
	// System.String UIImageButton::pressedSprite
	String_t* ___pressedSprite_5;
	// System.String UIImageButton::disabledSprite
	String_t* ___disabledSprite_6;
	// System.Boolean UIImageButton::pixelSnap
	bool ___pixelSnap_7;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIImageButton_t3745027676, ___target_2)); }
	inline UISprite_t194114938 * get_target_2() const { return ___target_2; }
	inline UISprite_t194114938 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(UISprite_t194114938 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_normalSprite_3() { return static_cast<int32_t>(offsetof(UIImageButton_t3745027676, ___normalSprite_3)); }
	inline String_t* get_normalSprite_3() const { return ___normalSprite_3; }
	inline String_t** get_address_of_normalSprite_3() { return &___normalSprite_3; }
	inline void set_normalSprite_3(String_t* value)
	{
		___normalSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___normalSprite_3), value);
	}

	inline static int32_t get_offset_of_hoverSprite_4() { return static_cast<int32_t>(offsetof(UIImageButton_t3745027676, ___hoverSprite_4)); }
	inline String_t* get_hoverSprite_4() const { return ___hoverSprite_4; }
	inline String_t** get_address_of_hoverSprite_4() { return &___hoverSprite_4; }
	inline void set_hoverSprite_4(String_t* value)
	{
		___hoverSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___hoverSprite_4), value);
	}

	inline static int32_t get_offset_of_pressedSprite_5() { return static_cast<int32_t>(offsetof(UIImageButton_t3745027676, ___pressedSprite_5)); }
	inline String_t* get_pressedSprite_5() const { return ___pressedSprite_5; }
	inline String_t** get_address_of_pressedSprite_5() { return &___pressedSprite_5; }
	inline void set_pressedSprite_5(String_t* value)
	{
		___pressedSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite_5), value);
	}

	inline static int32_t get_offset_of_disabledSprite_6() { return static_cast<int32_t>(offsetof(UIImageButton_t3745027676, ___disabledSprite_6)); }
	inline String_t* get_disabledSprite_6() const { return ___disabledSprite_6; }
	inline String_t** get_address_of_disabledSprite_6() { return &___disabledSprite_6; }
	inline void set_disabledSprite_6(String_t* value)
	{
		___disabledSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___disabledSprite_6), value);
	}

	inline static int32_t get_offset_of_pixelSnap_7() { return static_cast<int32_t>(offsetof(UIImageButton_t3745027676, ___pixelSnap_7)); }
	inline bool get_pixelSnap_7() const { return ___pixelSnap_7; }
	inline bool* get_address_of_pixelSnap_7() { return &___pixelSnap_7; }
	inline void set_pixelSnap_7(bool value)
	{
		___pixelSnap_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGEBUTTON_T3745027676_H
#ifndef UISAVEDOPTION_T2504198202_H
#define UISAVEDOPTION_T2504198202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISavedOption
struct  UISavedOption_t2504198202  : public MonoBehaviour_t3962482529
{
public:
	// System.String UISavedOption::keyName
	String_t* ___keyName_2;
	// UIPopupList UISavedOption::mList
	UIPopupList_t4167399471 * ___mList_3;
	// UIToggle UISavedOption::mCheck
	UIToggle_t4192126258 * ___mCheck_4;
	// UIProgressBar UISavedOption::mSlider
	UIProgressBar_t1222110469 * ___mSlider_5;

public:
	inline static int32_t get_offset_of_keyName_2() { return static_cast<int32_t>(offsetof(UISavedOption_t2504198202, ___keyName_2)); }
	inline String_t* get_keyName_2() const { return ___keyName_2; }
	inline String_t** get_address_of_keyName_2() { return &___keyName_2; }
	inline void set_keyName_2(String_t* value)
	{
		___keyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyName_2), value);
	}

	inline static int32_t get_offset_of_mList_3() { return static_cast<int32_t>(offsetof(UISavedOption_t2504198202, ___mList_3)); }
	inline UIPopupList_t4167399471 * get_mList_3() const { return ___mList_3; }
	inline UIPopupList_t4167399471 ** get_address_of_mList_3() { return &___mList_3; }
	inline void set_mList_3(UIPopupList_t4167399471 * value)
	{
		___mList_3 = value;
		Il2CppCodeGenWriteBarrier((&___mList_3), value);
	}

	inline static int32_t get_offset_of_mCheck_4() { return static_cast<int32_t>(offsetof(UISavedOption_t2504198202, ___mCheck_4)); }
	inline UIToggle_t4192126258 * get_mCheck_4() const { return ___mCheck_4; }
	inline UIToggle_t4192126258 ** get_address_of_mCheck_4() { return &___mCheck_4; }
	inline void set_mCheck_4(UIToggle_t4192126258 * value)
	{
		___mCheck_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCheck_4), value);
	}

	inline static int32_t get_offset_of_mSlider_5() { return static_cast<int32_t>(offsetof(UISavedOption_t2504198202, ___mSlider_5)); }
	inline UIProgressBar_t1222110469 * get_mSlider_5() const { return ___mSlider_5; }
	inline UIProgressBar_t1222110469 ** get_address_of_mSlider_5() { return &___mSlider_5; }
	inline void set_mSlider_5(UIProgressBar_t1222110469 * value)
	{
		___mSlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSlider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISAVEDOPTION_T2504198202_H
#ifndef UIDRAGOBJECT_T167167434_H
#define UIDRAGOBJECT_T167167434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDragObject
struct  UIDragObject_t167167434  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UIDragObject::target
	Transform_t3600365921 * ___target_2;
	// UIPanel UIDragObject::panelRegion
	UIPanel_t1716472341 * ___panelRegion_3;
	// UnityEngine.Vector3 UIDragObject::scrollMomentum
	Vector3_t3722313464  ___scrollMomentum_4;
	// System.Boolean UIDragObject::restrictWithinPanel
	bool ___restrictWithinPanel_5;
	// UIRect UIDragObject::contentRect
	UIRect_t2875960382 * ___contentRect_6;
	// UIDragObject/DragEffect UIDragObject::dragEffect
	int32_t ___dragEffect_7;
	// System.Single UIDragObject::momentumAmount
	float ___momentumAmount_8;
	// UnityEngine.Vector3 UIDragObject::scale
	Vector3_t3722313464  ___scale_9;
	// System.Single UIDragObject::scrollWheelFactor
	float ___scrollWheelFactor_10;
	// UnityEngine.Plane UIDragObject::mPlane
	Plane_t1000493321  ___mPlane_11;
	// UnityEngine.Vector3 UIDragObject::mTargetPos
	Vector3_t3722313464  ___mTargetPos_12;
	// UnityEngine.Vector3 UIDragObject::mLastPos
	Vector3_t3722313464  ___mLastPos_13;
	// UnityEngine.Vector3 UIDragObject::mMomentum
	Vector3_t3722313464  ___mMomentum_14;
	// UnityEngine.Vector3 UIDragObject::mScroll
	Vector3_t3722313464  ___mScroll_15;
	// UnityEngine.Bounds UIDragObject::mBounds
	Bounds_t2266837910  ___mBounds_16;
	// System.Int32 UIDragObject::mTouchID
	int32_t ___mTouchID_17;
	// System.Boolean UIDragObject::mStarted
	bool ___mStarted_18;
	// System.Boolean UIDragObject::mPressed
	bool ___mPressed_19;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_panelRegion_3() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___panelRegion_3)); }
	inline UIPanel_t1716472341 * get_panelRegion_3() const { return ___panelRegion_3; }
	inline UIPanel_t1716472341 ** get_address_of_panelRegion_3() { return &___panelRegion_3; }
	inline void set_panelRegion_3(UIPanel_t1716472341 * value)
	{
		___panelRegion_3 = value;
		Il2CppCodeGenWriteBarrier((&___panelRegion_3), value);
	}

	inline static int32_t get_offset_of_scrollMomentum_4() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___scrollMomentum_4)); }
	inline Vector3_t3722313464  get_scrollMomentum_4() const { return ___scrollMomentum_4; }
	inline Vector3_t3722313464 * get_address_of_scrollMomentum_4() { return &___scrollMomentum_4; }
	inline void set_scrollMomentum_4(Vector3_t3722313464  value)
	{
		___scrollMomentum_4 = value;
	}

	inline static int32_t get_offset_of_restrictWithinPanel_5() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___restrictWithinPanel_5)); }
	inline bool get_restrictWithinPanel_5() const { return ___restrictWithinPanel_5; }
	inline bool* get_address_of_restrictWithinPanel_5() { return &___restrictWithinPanel_5; }
	inline void set_restrictWithinPanel_5(bool value)
	{
		___restrictWithinPanel_5 = value;
	}

	inline static int32_t get_offset_of_contentRect_6() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___contentRect_6)); }
	inline UIRect_t2875960382 * get_contentRect_6() const { return ___contentRect_6; }
	inline UIRect_t2875960382 ** get_address_of_contentRect_6() { return &___contentRect_6; }
	inline void set_contentRect_6(UIRect_t2875960382 * value)
	{
		___contentRect_6 = value;
		Il2CppCodeGenWriteBarrier((&___contentRect_6), value);
	}

	inline static int32_t get_offset_of_dragEffect_7() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___dragEffect_7)); }
	inline int32_t get_dragEffect_7() const { return ___dragEffect_7; }
	inline int32_t* get_address_of_dragEffect_7() { return &___dragEffect_7; }
	inline void set_dragEffect_7(int32_t value)
	{
		___dragEffect_7 = value;
	}

	inline static int32_t get_offset_of_momentumAmount_8() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___momentumAmount_8)); }
	inline float get_momentumAmount_8() const { return ___momentumAmount_8; }
	inline float* get_address_of_momentumAmount_8() { return &___momentumAmount_8; }
	inline void set_momentumAmount_8(float value)
	{
		___momentumAmount_8 = value;
	}

	inline static int32_t get_offset_of_scale_9() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___scale_9)); }
	inline Vector3_t3722313464  get_scale_9() const { return ___scale_9; }
	inline Vector3_t3722313464 * get_address_of_scale_9() { return &___scale_9; }
	inline void set_scale_9(Vector3_t3722313464  value)
	{
		___scale_9 = value;
	}

	inline static int32_t get_offset_of_scrollWheelFactor_10() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___scrollWheelFactor_10)); }
	inline float get_scrollWheelFactor_10() const { return ___scrollWheelFactor_10; }
	inline float* get_address_of_scrollWheelFactor_10() { return &___scrollWheelFactor_10; }
	inline void set_scrollWheelFactor_10(float value)
	{
		___scrollWheelFactor_10 = value;
	}

	inline static int32_t get_offset_of_mPlane_11() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mPlane_11)); }
	inline Plane_t1000493321  get_mPlane_11() const { return ___mPlane_11; }
	inline Plane_t1000493321 * get_address_of_mPlane_11() { return &___mPlane_11; }
	inline void set_mPlane_11(Plane_t1000493321  value)
	{
		___mPlane_11 = value;
	}

	inline static int32_t get_offset_of_mTargetPos_12() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mTargetPos_12)); }
	inline Vector3_t3722313464  get_mTargetPos_12() const { return ___mTargetPos_12; }
	inline Vector3_t3722313464 * get_address_of_mTargetPos_12() { return &___mTargetPos_12; }
	inline void set_mTargetPos_12(Vector3_t3722313464  value)
	{
		___mTargetPos_12 = value;
	}

	inline static int32_t get_offset_of_mLastPos_13() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mLastPos_13)); }
	inline Vector3_t3722313464  get_mLastPos_13() const { return ___mLastPos_13; }
	inline Vector3_t3722313464 * get_address_of_mLastPos_13() { return &___mLastPos_13; }
	inline void set_mLastPos_13(Vector3_t3722313464  value)
	{
		___mLastPos_13 = value;
	}

	inline static int32_t get_offset_of_mMomentum_14() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mMomentum_14)); }
	inline Vector3_t3722313464  get_mMomentum_14() const { return ___mMomentum_14; }
	inline Vector3_t3722313464 * get_address_of_mMomentum_14() { return &___mMomentum_14; }
	inline void set_mMomentum_14(Vector3_t3722313464  value)
	{
		___mMomentum_14 = value;
	}

	inline static int32_t get_offset_of_mScroll_15() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mScroll_15)); }
	inline Vector3_t3722313464  get_mScroll_15() const { return ___mScroll_15; }
	inline Vector3_t3722313464 * get_address_of_mScroll_15() { return &___mScroll_15; }
	inline void set_mScroll_15(Vector3_t3722313464  value)
	{
		___mScroll_15 = value;
	}

	inline static int32_t get_offset_of_mBounds_16() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mBounds_16)); }
	inline Bounds_t2266837910  get_mBounds_16() const { return ___mBounds_16; }
	inline Bounds_t2266837910 * get_address_of_mBounds_16() { return &___mBounds_16; }
	inline void set_mBounds_16(Bounds_t2266837910  value)
	{
		___mBounds_16 = value;
	}

	inline static int32_t get_offset_of_mTouchID_17() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mTouchID_17)); }
	inline int32_t get_mTouchID_17() const { return ___mTouchID_17; }
	inline int32_t* get_address_of_mTouchID_17() { return &___mTouchID_17; }
	inline void set_mTouchID_17(int32_t value)
	{
		___mTouchID_17 = value;
	}

	inline static int32_t get_offset_of_mStarted_18() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mStarted_18)); }
	inline bool get_mStarted_18() const { return ___mStarted_18; }
	inline bool* get_address_of_mStarted_18() { return &___mStarted_18; }
	inline void set_mStarted_18(bool value)
	{
		___mStarted_18 = value;
	}

	inline static int32_t get_offset_of_mPressed_19() { return static_cast<int32_t>(offsetof(UIDragObject_t167167434, ___mPressed_19)); }
	inline bool get_mPressed_19() const { return ___mPressed_19; }
	inline bool* get_address_of_mPressed_19() { return &___mPressed_19; }
	inline void set_mPressed_19(bool value)
	{
		___mPressed_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAGOBJECT_T167167434_H
#ifndef UIBUTTONROTATION_T284788413_H
#define UIBUTTONROTATION_T284788413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonRotation
struct  UIButtonRotation_t284788413  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UIButtonRotation::tweenTarget
	Transform_t3600365921 * ___tweenTarget_2;
	// UnityEngine.Vector3 UIButtonRotation::hover
	Vector3_t3722313464  ___hover_3;
	// UnityEngine.Vector3 UIButtonRotation::pressed
	Vector3_t3722313464  ___pressed_4;
	// System.Single UIButtonRotation::duration
	float ___duration_5;
	// UnityEngine.Quaternion UIButtonRotation::mRot
	Quaternion_t2301928331  ___mRot_6;
	// System.Boolean UIButtonRotation::mStarted
	bool ___mStarted_7;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonRotation_t284788413, ___tweenTarget_2)); }
	inline Transform_t3600365921 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline Transform_t3600365921 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(Transform_t3600365921 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonRotation_t284788413, ___hover_3)); }
	inline Vector3_t3722313464  get_hover_3() const { return ___hover_3; }
	inline Vector3_t3722313464 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Vector3_t3722313464  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonRotation_t284788413, ___pressed_4)); }
	inline Vector3_t3722313464  get_pressed_4() const { return ___pressed_4; }
	inline Vector3_t3722313464 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Vector3_t3722313464  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(UIButtonRotation_t284788413, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mRot_6() { return static_cast<int32_t>(offsetof(UIButtonRotation_t284788413, ___mRot_6)); }
	inline Quaternion_t2301928331  get_mRot_6() const { return ___mRot_6; }
	inline Quaternion_t2301928331 * get_address_of_mRot_6() { return &___mRot_6; }
	inline void set_mRot_6(Quaternion_t2301928331  value)
	{
		___mRot_6 = value;
	}

	inline static int32_t get_offset_of_mStarted_7() { return static_cast<int32_t>(offsetof(UIButtonRotation_t284788413, ___mStarted_7)); }
	inline bool get_mStarted_7() const { return ___mStarted_7; }
	inline bool* get_address_of_mStarted_7() { return &___mStarted_7; }
	inline void set_mStarted_7(bool value)
	{
		___mStarted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONROTATION_T284788413_H
#ifndef UISCROLLVIEW_T1973404950_H
#define UISCROLLVIEW_T1973404950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollView
struct  UIScrollView_t1973404950  : public MonoBehaviour_t3962482529
{
public:
	// UIScrollView/Movement UIScrollView::movement
	int32_t ___movement_3;
	// UIScrollView/DragEffect UIScrollView::dragEffect
	int32_t ___dragEffect_4;
	// System.Boolean UIScrollView::restrictWithinPanel
	bool ___restrictWithinPanel_5;
	// System.Boolean UIScrollView::constrainOnDrag
	bool ___constrainOnDrag_6;
	// System.Boolean UIScrollView::disableDragIfFits
	bool ___disableDragIfFits_7;
	// System.Boolean UIScrollView::smoothDragStart
	bool ___smoothDragStart_8;
	// System.Boolean UIScrollView::iOSDragEmulation
	bool ___iOSDragEmulation_9;
	// System.Single UIScrollView::scrollWheelFactor
	float ___scrollWheelFactor_10;
	// System.Single UIScrollView::momentumAmount
	float ___momentumAmount_11;
	// System.Single UIScrollView::dampenStrength
	float ___dampenStrength_12;
	// UIProgressBar UIScrollView::horizontalScrollBar
	UIProgressBar_t1222110469 * ___horizontalScrollBar_13;
	// UIProgressBar UIScrollView::verticalScrollBar
	UIProgressBar_t1222110469 * ___verticalScrollBar_14;
	// UIScrollView/ShowCondition UIScrollView::showScrollBars
	int32_t ___showScrollBars_15;
	// UnityEngine.Vector2 UIScrollView::customMovement
	Vector2_t2156229523  ___customMovement_16;
	// UIWidget/Pivot UIScrollView::contentPivot
	int32_t ___contentPivot_17;
	// UIScrollView/OnDragNotification UIScrollView::onDragStarted
	OnDragNotification_t1437737811 * ___onDragStarted_18;
	// UIScrollView/OnDragNotification UIScrollView::onDragFinished
	OnDragNotification_t1437737811 * ___onDragFinished_19;
	// UIScrollView/OnDragNotification UIScrollView::onMomentumMove
	OnDragNotification_t1437737811 * ___onMomentumMove_20;
	// UIScrollView/OnDragNotification UIScrollView::onStoppedMoving
	OnDragNotification_t1437737811 * ___onStoppedMoving_21;
	// UnityEngine.Vector3 UIScrollView::scale
	Vector3_t3722313464  ___scale_22;
	// UnityEngine.Vector2 UIScrollView::relativePositionOnReset
	Vector2_t2156229523  ___relativePositionOnReset_23;
	// UnityEngine.Transform UIScrollView::mTrans
	Transform_t3600365921 * ___mTrans_24;
	// UIPanel UIScrollView::mPanel
	UIPanel_t1716472341 * ___mPanel_25;
	// UnityEngine.Plane UIScrollView::mPlane
	Plane_t1000493321  ___mPlane_26;
	// UnityEngine.Vector3 UIScrollView::mLastPos
	Vector3_t3722313464  ___mLastPos_27;
	// System.Boolean UIScrollView::mPressed
	bool ___mPressed_28;
	// UnityEngine.Vector3 UIScrollView::mMomentum
	Vector3_t3722313464  ___mMomentum_29;
	// System.Single UIScrollView::mScroll
	float ___mScroll_30;
	// UnityEngine.Bounds UIScrollView::mBounds
	Bounds_t2266837910  ___mBounds_31;
	// System.Boolean UIScrollView::mCalculatedBounds
	bool ___mCalculatedBounds_32;
	// System.Boolean UIScrollView::mShouldMove
	bool ___mShouldMove_33;
	// System.Boolean UIScrollView::mIgnoreCallbacks
	bool ___mIgnoreCallbacks_34;
	// System.Int32 UIScrollView::mDragID
	int32_t ___mDragID_35;
	// UnityEngine.Vector2 UIScrollView::mDragStartOffset
	Vector2_t2156229523  ___mDragStartOffset_36;
	// System.Boolean UIScrollView::mDragStarted
	bool ___mDragStarted_37;
	// System.Boolean UIScrollView::mStarted
	bool ___mStarted_38;
	// UICenterOnChild UIScrollView::centerOnChild
	UICenterOnChild_t253063637 * ___centerOnChild_39;

public:
	inline static int32_t get_offset_of_movement_3() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___movement_3)); }
	inline int32_t get_movement_3() const { return ___movement_3; }
	inline int32_t* get_address_of_movement_3() { return &___movement_3; }
	inline void set_movement_3(int32_t value)
	{
		___movement_3 = value;
	}

	inline static int32_t get_offset_of_dragEffect_4() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___dragEffect_4)); }
	inline int32_t get_dragEffect_4() const { return ___dragEffect_4; }
	inline int32_t* get_address_of_dragEffect_4() { return &___dragEffect_4; }
	inline void set_dragEffect_4(int32_t value)
	{
		___dragEffect_4 = value;
	}

	inline static int32_t get_offset_of_restrictWithinPanel_5() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___restrictWithinPanel_5)); }
	inline bool get_restrictWithinPanel_5() const { return ___restrictWithinPanel_5; }
	inline bool* get_address_of_restrictWithinPanel_5() { return &___restrictWithinPanel_5; }
	inline void set_restrictWithinPanel_5(bool value)
	{
		___restrictWithinPanel_5 = value;
	}

	inline static int32_t get_offset_of_constrainOnDrag_6() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___constrainOnDrag_6)); }
	inline bool get_constrainOnDrag_6() const { return ___constrainOnDrag_6; }
	inline bool* get_address_of_constrainOnDrag_6() { return &___constrainOnDrag_6; }
	inline void set_constrainOnDrag_6(bool value)
	{
		___constrainOnDrag_6 = value;
	}

	inline static int32_t get_offset_of_disableDragIfFits_7() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___disableDragIfFits_7)); }
	inline bool get_disableDragIfFits_7() const { return ___disableDragIfFits_7; }
	inline bool* get_address_of_disableDragIfFits_7() { return &___disableDragIfFits_7; }
	inline void set_disableDragIfFits_7(bool value)
	{
		___disableDragIfFits_7 = value;
	}

	inline static int32_t get_offset_of_smoothDragStart_8() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___smoothDragStart_8)); }
	inline bool get_smoothDragStart_8() const { return ___smoothDragStart_8; }
	inline bool* get_address_of_smoothDragStart_8() { return &___smoothDragStart_8; }
	inline void set_smoothDragStart_8(bool value)
	{
		___smoothDragStart_8 = value;
	}

	inline static int32_t get_offset_of_iOSDragEmulation_9() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___iOSDragEmulation_9)); }
	inline bool get_iOSDragEmulation_9() const { return ___iOSDragEmulation_9; }
	inline bool* get_address_of_iOSDragEmulation_9() { return &___iOSDragEmulation_9; }
	inline void set_iOSDragEmulation_9(bool value)
	{
		___iOSDragEmulation_9 = value;
	}

	inline static int32_t get_offset_of_scrollWheelFactor_10() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___scrollWheelFactor_10)); }
	inline float get_scrollWheelFactor_10() const { return ___scrollWheelFactor_10; }
	inline float* get_address_of_scrollWheelFactor_10() { return &___scrollWheelFactor_10; }
	inline void set_scrollWheelFactor_10(float value)
	{
		___scrollWheelFactor_10 = value;
	}

	inline static int32_t get_offset_of_momentumAmount_11() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___momentumAmount_11)); }
	inline float get_momentumAmount_11() const { return ___momentumAmount_11; }
	inline float* get_address_of_momentumAmount_11() { return &___momentumAmount_11; }
	inline void set_momentumAmount_11(float value)
	{
		___momentumAmount_11 = value;
	}

	inline static int32_t get_offset_of_dampenStrength_12() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___dampenStrength_12)); }
	inline float get_dampenStrength_12() const { return ___dampenStrength_12; }
	inline float* get_address_of_dampenStrength_12() { return &___dampenStrength_12; }
	inline void set_dampenStrength_12(float value)
	{
		___dampenStrength_12 = value;
	}

	inline static int32_t get_offset_of_horizontalScrollBar_13() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___horizontalScrollBar_13)); }
	inline UIProgressBar_t1222110469 * get_horizontalScrollBar_13() const { return ___horizontalScrollBar_13; }
	inline UIProgressBar_t1222110469 ** get_address_of_horizontalScrollBar_13() { return &___horizontalScrollBar_13; }
	inline void set_horizontalScrollBar_13(UIProgressBar_t1222110469 * value)
	{
		___horizontalScrollBar_13 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalScrollBar_13), value);
	}

	inline static int32_t get_offset_of_verticalScrollBar_14() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___verticalScrollBar_14)); }
	inline UIProgressBar_t1222110469 * get_verticalScrollBar_14() const { return ___verticalScrollBar_14; }
	inline UIProgressBar_t1222110469 ** get_address_of_verticalScrollBar_14() { return &___verticalScrollBar_14; }
	inline void set_verticalScrollBar_14(UIProgressBar_t1222110469 * value)
	{
		___verticalScrollBar_14 = value;
		Il2CppCodeGenWriteBarrier((&___verticalScrollBar_14), value);
	}

	inline static int32_t get_offset_of_showScrollBars_15() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___showScrollBars_15)); }
	inline int32_t get_showScrollBars_15() const { return ___showScrollBars_15; }
	inline int32_t* get_address_of_showScrollBars_15() { return &___showScrollBars_15; }
	inline void set_showScrollBars_15(int32_t value)
	{
		___showScrollBars_15 = value;
	}

	inline static int32_t get_offset_of_customMovement_16() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___customMovement_16)); }
	inline Vector2_t2156229523  get_customMovement_16() const { return ___customMovement_16; }
	inline Vector2_t2156229523 * get_address_of_customMovement_16() { return &___customMovement_16; }
	inline void set_customMovement_16(Vector2_t2156229523  value)
	{
		___customMovement_16 = value;
	}

	inline static int32_t get_offset_of_contentPivot_17() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___contentPivot_17)); }
	inline int32_t get_contentPivot_17() const { return ___contentPivot_17; }
	inline int32_t* get_address_of_contentPivot_17() { return &___contentPivot_17; }
	inline void set_contentPivot_17(int32_t value)
	{
		___contentPivot_17 = value;
	}

	inline static int32_t get_offset_of_onDragStarted_18() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onDragStarted_18)); }
	inline OnDragNotification_t1437737811 * get_onDragStarted_18() const { return ___onDragStarted_18; }
	inline OnDragNotification_t1437737811 ** get_address_of_onDragStarted_18() { return &___onDragStarted_18; }
	inline void set_onDragStarted_18(OnDragNotification_t1437737811 * value)
	{
		___onDragStarted_18 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStarted_18), value);
	}

	inline static int32_t get_offset_of_onDragFinished_19() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onDragFinished_19)); }
	inline OnDragNotification_t1437737811 * get_onDragFinished_19() const { return ___onDragFinished_19; }
	inline OnDragNotification_t1437737811 ** get_address_of_onDragFinished_19() { return &___onDragFinished_19; }
	inline void set_onDragFinished_19(OnDragNotification_t1437737811 * value)
	{
		___onDragFinished_19 = value;
		Il2CppCodeGenWriteBarrier((&___onDragFinished_19), value);
	}

	inline static int32_t get_offset_of_onMomentumMove_20() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onMomentumMove_20)); }
	inline OnDragNotification_t1437737811 * get_onMomentumMove_20() const { return ___onMomentumMove_20; }
	inline OnDragNotification_t1437737811 ** get_address_of_onMomentumMove_20() { return &___onMomentumMove_20; }
	inline void set_onMomentumMove_20(OnDragNotification_t1437737811 * value)
	{
		___onMomentumMove_20 = value;
		Il2CppCodeGenWriteBarrier((&___onMomentumMove_20), value);
	}

	inline static int32_t get_offset_of_onStoppedMoving_21() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___onStoppedMoving_21)); }
	inline OnDragNotification_t1437737811 * get_onStoppedMoving_21() const { return ___onStoppedMoving_21; }
	inline OnDragNotification_t1437737811 ** get_address_of_onStoppedMoving_21() { return &___onStoppedMoving_21; }
	inline void set_onStoppedMoving_21(OnDragNotification_t1437737811 * value)
	{
		___onStoppedMoving_21 = value;
		Il2CppCodeGenWriteBarrier((&___onStoppedMoving_21), value);
	}

	inline static int32_t get_offset_of_scale_22() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___scale_22)); }
	inline Vector3_t3722313464  get_scale_22() const { return ___scale_22; }
	inline Vector3_t3722313464 * get_address_of_scale_22() { return &___scale_22; }
	inline void set_scale_22(Vector3_t3722313464  value)
	{
		___scale_22 = value;
	}

	inline static int32_t get_offset_of_relativePositionOnReset_23() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___relativePositionOnReset_23)); }
	inline Vector2_t2156229523  get_relativePositionOnReset_23() const { return ___relativePositionOnReset_23; }
	inline Vector2_t2156229523 * get_address_of_relativePositionOnReset_23() { return &___relativePositionOnReset_23; }
	inline void set_relativePositionOnReset_23(Vector2_t2156229523  value)
	{
		___relativePositionOnReset_23 = value;
	}

	inline static int32_t get_offset_of_mTrans_24() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mTrans_24)); }
	inline Transform_t3600365921 * get_mTrans_24() const { return ___mTrans_24; }
	inline Transform_t3600365921 ** get_address_of_mTrans_24() { return &___mTrans_24; }
	inline void set_mTrans_24(Transform_t3600365921 * value)
	{
		___mTrans_24 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_24), value);
	}

	inline static int32_t get_offset_of_mPanel_25() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mPanel_25)); }
	inline UIPanel_t1716472341 * get_mPanel_25() const { return ___mPanel_25; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_25() { return &___mPanel_25; }
	inline void set_mPanel_25(UIPanel_t1716472341 * value)
	{
		___mPanel_25 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_25), value);
	}

	inline static int32_t get_offset_of_mPlane_26() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mPlane_26)); }
	inline Plane_t1000493321  get_mPlane_26() const { return ___mPlane_26; }
	inline Plane_t1000493321 * get_address_of_mPlane_26() { return &___mPlane_26; }
	inline void set_mPlane_26(Plane_t1000493321  value)
	{
		___mPlane_26 = value;
	}

	inline static int32_t get_offset_of_mLastPos_27() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mLastPos_27)); }
	inline Vector3_t3722313464  get_mLastPos_27() const { return ___mLastPos_27; }
	inline Vector3_t3722313464 * get_address_of_mLastPos_27() { return &___mLastPos_27; }
	inline void set_mLastPos_27(Vector3_t3722313464  value)
	{
		___mLastPos_27 = value;
	}

	inline static int32_t get_offset_of_mPressed_28() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mPressed_28)); }
	inline bool get_mPressed_28() const { return ___mPressed_28; }
	inline bool* get_address_of_mPressed_28() { return &___mPressed_28; }
	inline void set_mPressed_28(bool value)
	{
		___mPressed_28 = value;
	}

	inline static int32_t get_offset_of_mMomentum_29() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mMomentum_29)); }
	inline Vector3_t3722313464  get_mMomentum_29() const { return ___mMomentum_29; }
	inline Vector3_t3722313464 * get_address_of_mMomentum_29() { return &___mMomentum_29; }
	inline void set_mMomentum_29(Vector3_t3722313464  value)
	{
		___mMomentum_29 = value;
	}

	inline static int32_t get_offset_of_mScroll_30() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mScroll_30)); }
	inline float get_mScroll_30() const { return ___mScroll_30; }
	inline float* get_address_of_mScroll_30() { return &___mScroll_30; }
	inline void set_mScroll_30(float value)
	{
		___mScroll_30 = value;
	}

	inline static int32_t get_offset_of_mBounds_31() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mBounds_31)); }
	inline Bounds_t2266837910  get_mBounds_31() const { return ___mBounds_31; }
	inline Bounds_t2266837910 * get_address_of_mBounds_31() { return &___mBounds_31; }
	inline void set_mBounds_31(Bounds_t2266837910  value)
	{
		___mBounds_31 = value;
	}

	inline static int32_t get_offset_of_mCalculatedBounds_32() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mCalculatedBounds_32)); }
	inline bool get_mCalculatedBounds_32() const { return ___mCalculatedBounds_32; }
	inline bool* get_address_of_mCalculatedBounds_32() { return &___mCalculatedBounds_32; }
	inline void set_mCalculatedBounds_32(bool value)
	{
		___mCalculatedBounds_32 = value;
	}

	inline static int32_t get_offset_of_mShouldMove_33() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mShouldMove_33)); }
	inline bool get_mShouldMove_33() const { return ___mShouldMove_33; }
	inline bool* get_address_of_mShouldMove_33() { return &___mShouldMove_33; }
	inline void set_mShouldMove_33(bool value)
	{
		___mShouldMove_33 = value;
	}

	inline static int32_t get_offset_of_mIgnoreCallbacks_34() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mIgnoreCallbacks_34)); }
	inline bool get_mIgnoreCallbacks_34() const { return ___mIgnoreCallbacks_34; }
	inline bool* get_address_of_mIgnoreCallbacks_34() { return &___mIgnoreCallbacks_34; }
	inline void set_mIgnoreCallbacks_34(bool value)
	{
		___mIgnoreCallbacks_34 = value;
	}

	inline static int32_t get_offset_of_mDragID_35() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mDragID_35)); }
	inline int32_t get_mDragID_35() const { return ___mDragID_35; }
	inline int32_t* get_address_of_mDragID_35() { return &___mDragID_35; }
	inline void set_mDragID_35(int32_t value)
	{
		___mDragID_35 = value;
	}

	inline static int32_t get_offset_of_mDragStartOffset_36() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mDragStartOffset_36)); }
	inline Vector2_t2156229523  get_mDragStartOffset_36() const { return ___mDragStartOffset_36; }
	inline Vector2_t2156229523 * get_address_of_mDragStartOffset_36() { return &___mDragStartOffset_36; }
	inline void set_mDragStartOffset_36(Vector2_t2156229523  value)
	{
		___mDragStartOffset_36 = value;
	}

	inline static int32_t get_offset_of_mDragStarted_37() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mDragStarted_37)); }
	inline bool get_mDragStarted_37() const { return ___mDragStarted_37; }
	inline bool* get_address_of_mDragStarted_37() { return &___mDragStarted_37; }
	inline void set_mDragStarted_37(bool value)
	{
		___mDragStarted_37 = value;
	}

	inline static int32_t get_offset_of_mStarted_38() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___mStarted_38)); }
	inline bool get_mStarted_38() const { return ___mStarted_38; }
	inline bool* get_address_of_mStarted_38() { return &___mStarted_38; }
	inline void set_mStarted_38(bool value)
	{
		___mStarted_38 = value;
	}

	inline static int32_t get_offset_of_centerOnChild_39() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950, ___centerOnChild_39)); }
	inline UICenterOnChild_t253063637 * get_centerOnChild_39() const { return ___centerOnChild_39; }
	inline UICenterOnChild_t253063637 ** get_address_of_centerOnChild_39() { return &___centerOnChild_39; }
	inline void set_centerOnChild_39(UICenterOnChild_t253063637 * value)
	{
		___centerOnChild_39 = value;
		Il2CppCodeGenWriteBarrier((&___centerOnChild_39), value);
	}
};

struct UIScrollView_t1973404950_StaticFields
{
public:
	// BetterList`1<UIScrollView> UIScrollView::list
	BetterList_1_t1128425268 * ___list_2;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UIScrollView_t1973404950_StaticFields, ___list_2)); }
	inline BetterList_1_t1128425268 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t1128425268 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t1128425268 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLVIEW_T1973404950_H
#ifndef UIBUTTONOFFSET_T4041087173_H
#define UIBUTTONOFFSET_T4041087173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonOffset
struct  UIButtonOffset_t4041087173  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UIButtonOffset::tweenTarget
	Transform_t3600365921 * ___tweenTarget_2;
	// UnityEngine.Vector3 UIButtonOffset::hover
	Vector3_t3722313464  ___hover_3;
	// UnityEngine.Vector3 UIButtonOffset::pressed
	Vector3_t3722313464  ___pressed_4;
	// System.Single UIButtonOffset::duration
	float ___duration_5;
	// UnityEngine.Vector3 UIButtonOffset::mPos
	Vector3_t3722313464  ___mPos_6;
	// System.Boolean UIButtonOffset::mStarted
	bool ___mStarted_7;
	// System.Boolean UIButtonOffset::mPressed
	bool ___mPressed_8;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonOffset_t4041087173, ___tweenTarget_2)); }
	inline Transform_t3600365921 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline Transform_t3600365921 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(Transform_t3600365921 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonOffset_t4041087173, ___hover_3)); }
	inline Vector3_t3722313464  get_hover_3() const { return ___hover_3; }
	inline Vector3_t3722313464 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Vector3_t3722313464  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonOffset_t4041087173, ___pressed_4)); }
	inline Vector3_t3722313464  get_pressed_4() const { return ___pressed_4; }
	inline Vector3_t3722313464 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Vector3_t3722313464  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(UIButtonOffset_t4041087173, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mPos_6() { return static_cast<int32_t>(offsetof(UIButtonOffset_t4041087173, ___mPos_6)); }
	inline Vector3_t3722313464  get_mPos_6() const { return ___mPos_6; }
	inline Vector3_t3722313464 * get_address_of_mPos_6() { return &___mPos_6; }
	inline void set_mPos_6(Vector3_t3722313464  value)
	{
		___mPos_6 = value;
	}

	inline static int32_t get_offset_of_mStarted_7() { return static_cast<int32_t>(offsetof(UIButtonOffset_t4041087173, ___mStarted_7)); }
	inline bool get_mStarted_7() const { return ___mStarted_7; }
	inline bool* get_address_of_mStarted_7() { return &___mStarted_7; }
	inline void set_mStarted_7(bool value)
	{
		___mStarted_7 = value;
	}

	inline static int32_t get_offset_of_mPressed_8() { return static_cast<int32_t>(offsetof(UIButtonOffset_t4041087173, ___mPressed_8)); }
	inline bool get_mPressed_8() const { return ___mPressed_8; }
	inline bool* get_address_of_mPressed_8() { return &___mPressed_8; }
	inline void set_mPressed_8(bool value)
	{
		___mPressed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONOFFSET_T4041087173_H
#ifndef UIBUTTONMESSAGE_T952534536_H
#define UIBUTTONMESSAGE_T952534536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonMessage
struct  UIButtonMessage_t952534536  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIButtonMessage::target
	GameObject_t1113636619 * ___target_2;
	// System.String UIButtonMessage::functionName
	String_t* ___functionName_3;
	// UIButtonMessage/Trigger UIButtonMessage::trigger
	int32_t ___trigger_4;
	// System.Boolean UIButtonMessage::includeChildren
	bool ___includeChildren_5;
	// System.Boolean UIButtonMessage::mStarted
	bool ___mStarted_6;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIButtonMessage_t952534536, ___target_2)); }
	inline GameObject_t1113636619 * get_target_2() const { return ___target_2; }
	inline GameObject_t1113636619 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t1113636619 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_functionName_3() { return static_cast<int32_t>(offsetof(UIButtonMessage_t952534536, ___functionName_3)); }
	inline String_t* get_functionName_3() const { return ___functionName_3; }
	inline String_t** get_address_of_functionName_3() { return &___functionName_3; }
	inline void set_functionName_3(String_t* value)
	{
		___functionName_3 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_3), value);
	}

	inline static int32_t get_offset_of_trigger_4() { return static_cast<int32_t>(offsetof(UIButtonMessage_t952534536, ___trigger_4)); }
	inline int32_t get_trigger_4() const { return ___trigger_4; }
	inline int32_t* get_address_of_trigger_4() { return &___trigger_4; }
	inline void set_trigger_4(int32_t value)
	{
		___trigger_4 = value;
	}

	inline static int32_t get_offset_of_includeChildren_5() { return static_cast<int32_t>(offsetof(UIButtonMessage_t952534536, ___includeChildren_5)); }
	inline bool get_includeChildren_5() const { return ___includeChildren_5; }
	inline bool* get_address_of_includeChildren_5() { return &___includeChildren_5; }
	inline void set_includeChildren_5(bool value)
	{
		___includeChildren_5 = value;
	}

	inline static int32_t get_offset_of_mStarted_6() { return static_cast<int32_t>(offsetof(UIButtonMessage_t952534536, ___mStarted_6)); }
	inline bool get_mStarted_6() const { return ___mStarted_6; }
	inline bool* get_address_of_mStarted_6() { return &___mStarted_6; }
	inline void set_mStarted_6(bool value)
	{
		___mStarted_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONMESSAGE_T952534536_H
#ifndef UIFORWARDEVENTS_T2244381111_H
#define UIFORWARDEVENTS_T2244381111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIForwardEvents
struct  UIForwardEvents_t2244381111  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIForwardEvents::target
	GameObject_t1113636619 * ___target_2;
	// System.Boolean UIForwardEvents::onHover
	bool ___onHover_3;
	// System.Boolean UIForwardEvents::onPress
	bool ___onPress_4;
	// System.Boolean UIForwardEvents::onClick
	bool ___onClick_5;
	// System.Boolean UIForwardEvents::onDoubleClick
	bool ___onDoubleClick_6;
	// System.Boolean UIForwardEvents::onSelect
	bool ___onSelect_7;
	// System.Boolean UIForwardEvents::onDrag
	bool ___onDrag_8;
	// System.Boolean UIForwardEvents::onDrop
	bool ___onDrop_9;
	// System.Boolean UIForwardEvents::onSubmit
	bool ___onSubmit_10;
	// System.Boolean UIForwardEvents::onScroll
	bool ___onScroll_11;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___target_2)); }
	inline GameObject_t1113636619 * get_target_2() const { return ___target_2; }
	inline GameObject_t1113636619 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t1113636619 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_onHover_3() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onHover_3)); }
	inline bool get_onHover_3() const { return ___onHover_3; }
	inline bool* get_address_of_onHover_3() { return &___onHover_3; }
	inline void set_onHover_3(bool value)
	{
		___onHover_3 = value;
	}

	inline static int32_t get_offset_of_onPress_4() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onPress_4)); }
	inline bool get_onPress_4() const { return ___onPress_4; }
	inline bool* get_address_of_onPress_4() { return &___onPress_4; }
	inline void set_onPress_4(bool value)
	{
		___onPress_4 = value;
	}

	inline static int32_t get_offset_of_onClick_5() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onClick_5)); }
	inline bool get_onClick_5() const { return ___onClick_5; }
	inline bool* get_address_of_onClick_5() { return &___onClick_5; }
	inline void set_onClick_5(bool value)
	{
		___onClick_5 = value;
	}

	inline static int32_t get_offset_of_onDoubleClick_6() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onDoubleClick_6)); }
	inline bool get_onDoubleClick_6() const { return ___onDoubleClick_6; }
	inline bool* get_address_of_onDoubleClick_6() { return &___onDoubleClick_6; }
	inline void set_onDoubleClick_6(bool value)
	{
		___onDoubleClick_6 = value;
	}

	inline static int32_t get_offset_of_onSelect_7() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onSelect_7)); }
	inline bool get_onSelect_7() const { return ___onSelect_7; }
	inline bool* get_address_of_onSelect_7() { return &___onSelect_7; }
	inline void set_onSelect_7(bool value)
	{
		___onSelect_7 = value;
	}

	inline static int32_t get_offset_of_onDrag_8() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onDrag_8)); }
	inline bool get_onDrag_8() const { return ___onDrag_8; }
	inline bool* get_address_of_onDrag_8() { return &___onDrag_8; }
	inline void set_onDrag_8(bool value)
	{
		___onDrag_8 = value;
	}

	inline static int32_t get_offset_of_onDrop_9() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onDrop_9)); }
	inline bool get_onDrop_9() const { return ___onDrop_9; }
	inline bool* get_address_of_onDrop_9() { return &___onDrop_9; }
	inline void set_onDrop_9(bool value)
	{
		___onDrop_9 = value;
	}

	inline static int32_t get_offset_of_onSubmit_10() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onSubmit_10)); }
	inline bool get_onSubmit_10() const { return ___onSubmit_10; }
	inline bool* get_address_of_onSubmit_10() { return &___onSubmit_10; }
	inline void set_onSubmit_10(bool value)
	{
		___onSubmit_10 = value;
	}

	inline static int32_t get_offset_of_onScroll_11() { return static_cast<int32_t>(offsetof(UIForwardEvents_t2244381111, ___onScroll_11)); }
	inline bool get_onScroll_11() const { return ___onScroll_11; }
	inline bool* get_address_of_onScroll_11() { return &___onScroll_11; }
	inline void set_onScroll_11(bool value)
	{
		___onScroll_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFORWARDEVENTS_T2244381111_H
#ifndef UISHOWCONTROLSCHEME_T428458459_H
#define UISHOWCONTROLSCHEME_T428458459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIShowControlScheme
struct  UIShowControlScheme_t428458459  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIShowControlScheme::target
	GameObject_t1113636619 * ___target_2;
	// System.Boolean UIShowControlScheme::mouse
	bool ___mouse_3;
	// System.Boolean UIShowControlScheme::touch
	bool ___touch_4;
	// System.Boolean UIShowControlScheme::controller
	bool ___controller_5;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(UIShowControlScheme_t428458459, ___target_2)); }
	inline GameObject_t1113636619 * get_target_2() const { return ___target_2; }
	inline GameObject_t1113636619 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t1113636619 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_mouse_3() { return static_cast<int32_t>(offsetof(UIShowControlScheme_t428458459, ___mouse_3)); }
	inline bool get_mouse_3() const { return ___mouse_3; }
	inline bool* get_address_of_mouse_3() { return &___mouse_3; }
	inline void set_mouse_3(bool value)
	{
		___mouse_3 = value;
	}

	inline static int32_t get_offset_of_touch_4() { return static_cast<int32_t>(offsetof(UIShowControlScheme_t428458459, ___touch_4)); }
	inline bool get_touch_4() const { return ___touch_4; }
	inline bool* get_address_of_touch_4() { return &___touch_4; }
	inline void set_touch_4(bool value)
	{
		___touch_4 = value;
	}

	inline static int32_t get_offset_of_controller_5() { return static_cast<int32_t>(offsetof(UIShowControlScheme_t428458459, ___controller_5)); }
	inline bool get_controller_5() const { return ___controller_5; }
	inline bool* get_address_of_controller_5() { return &___controller_5; }
	inline void set_controller_5(bool value)
	{
		___controller_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISHOWCONTROLSCHEME_T428458459_H
#ifndef UIEVENTTRIGGER_T4038454782_H
#define UIEVENTTRIGGER_T4038454782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventTrigger
struct  UIEventTrigger_t4038454782  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onHoverOver
	List_1_t4210400802 * ___onHoverOver_3;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onHoverOut
	List_1_t4210400802 * ___onHoverOut_4;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onPress
	List_1_t4210400802 * ___onPress_5;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onRelease
	List_1_t4210400802 * ___onRelease_6;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onSelect
	List_1_t4210400802 * ___onSelect_7;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onDeselect
	List_1_t4210400802 * ___onDeselect_8;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onClick
	List_1_t4210400802 * ___onClick_9;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onDoubleClick
	List_1_t4210400802 * ___onDoubleClick_10;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onDragStart
	List_1_t4210400802 * ___onDragStart_11;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onDragEnd
	List_1_t4210400802 * ___onDragEnd_12;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onDragOver
	List_1_t4210400802 * ___onDragOver_13;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onDragOut
	List_1_t4210400802 * ___onDragOut_14;
	// System.Collections.Generic.List`1<EventDelegate> UIEventTrigger::onDrag
	List_1_t4210400802 * ___onDrag_15;

public:
	inline static int32_t get_offset_of_onHoverOver_3() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onHoverOver_3)); }
	inline List_1_t4210400802 * get_onHoverOver_3() const { return ___onHoverOver_3; }
	inline List_1_t4210400802 ** get_address_of_onHoverOver_3() { return &___onHoverOver_3; }
	inline void set_onHoverOver_3(List_1_t4210400802 * value)
	{
		___onHoverOver_3 = value;
		Il2CppCodeGenWriteBarrier((&___onHoverOver_3), value);
	}

	inline static int32_t get_offset_of_onHoverOut_4() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onHoverOut_4)); }
	inline List_1_t4210400802 * get_onHoverOut_4() const { return ___onHoverOut_4; }
	inline List_1_t4210400802 ** get_address_of_onHoverOut_4() { return &___onHoverOut_4; }
	inline void set_onHoverOut_4(List_1_t4210400802 * value)
	{
		___onHoverOut_4 = value;
		Il2CppCodeGenWriteBarrier((&___onHoverOut_4), value);
	}

	inline static int32_t get_offset_of_onPress_5() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onPress_5)); }
	inline List_1_t4210400802 * get_onPress_5() const { return ___onPress_5; }
	inline List_1_t4210400802 ** get_address_of_onPress_5() { return &___onPress_5; }
	inline void set_onPress_5(List_1_t4210400802 * value)
	{
		___onPress_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPress_5), value);
	}

	inline static int32_t get_offset_of_onRelease_6() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onRelease_6)); }
	inline List_1_t4210400802 * get_onRelease_6() const { return ___onRelease_6; }
	inline List_1_t4210400802 ** get_address_of_onRelease_6() { return &___onRelease_6; }
	inline void set_onRelease_6(List_1_t4210400802 * value)
	{
		___onRelease_6 = value;
		Il2CppCodeGenWriteBarrier((&___onRelease_6), value);
	}

	inline static int32_t get_offset_of_onSelect_7() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onSelect_7)); }
	inline List_1_t4210400802 * get_onSelect_7() const { return ___onSelect_7; }
	inline List_1_t4210400802 ** get_address_of_onSelect_7() { return &___onSelect_7; }
	inline void set_onSelect_7(List_1_t4210400802 * value)
	{
		___onSelect_7 = value;
		Il2CppCodeGenWriteBarrier((&___onSelect_7), value);
	}

	inline static int32_t get_offset_of_onDeselect_8() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onDeselect_8)); }
	inline List_1_t4210400802 * get_onDeselect_8() const { return ___onDeselect_8; }
	inline List_1_t4210400802 ** get_address_of_onDeselect_8() { return &___onDeselect_8; }
	inline void set_onDeselect_8(List_1_t4210400802 * value)
	{
		___onDeselect_8 = value;
		Il2CppCodeGenWriteBarrier((&___onDeselect_8), value);
	}

	inline static int32_t get_offset_of_onClick_9() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onClick_9)); }
	inline List_1_t4210400802 * get_onClick_9() const { return ___onClick_9; }
	inline List_1_t4210400802 ** get_address_of_onClick_9() { return &___onClick_9; }
	inline void set_onClick_9(List_1_t4210400802 * value)
	{
		___onClick_9 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_9), value);
	}

	inline static int32_t get_offset_of_onDoubleClick_10() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onDoubleClick_10)); }
	inline List_1_t4210400802 * get_onDoubleClick_10() const { return ___onDoubleClick_10; }
	inline List_1_t4210400802 ** get_address_of_onDoubleClick_10() { return &___onDoubleClick_10; }
	inline void set_onDoubleClick_10(List_1_t4210400802 * value)
	{
		___onDoubleClick_10 = value;
		Il2CppCodeGenWriteBarrier((&___onDoubleClick_10), value);
	}

	inline static int32_t get_offset_of_onDragStart_11() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onDragStart_11)); }
	inline List_1_t4210400802 * get_onDragStart_11() const { return ___onDragStart_11; }
	inline List_1_t4210400802 ** get_address_of_onDragStart_11() { return &___onDragStart_11; }
	inline void set_onDragStart_11(List_1_t4210400802 * value)
	{
		___onDragStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStart_11), value);
	}

	inline static int32_t get_offset_of_onDragEnd_12() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onDragEnd_12)); }
	inline List_1_t4210400802 * get_onDragEnd_12() const { return ___onDragEnd_12; }
	inline List_1_t4210400802 ** get_address_of_onDragEnd_12() { return &___onDragEnd_12; }
	inline void set_onDragEnd_12(List_1_t4210400802 * value)
	{
		___onDragEnd_12 = value;
		Il2CppCodeGenWriteBarrier((&___onDragEnd_12), value);
	}

	inline static int32_t get_offset_of_onDragOver_13() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onDragOver_13)); }
	inline List_1_t4210400802 * get_onDragOver_13() const { return ___onDragOver_13; }
	inline List_1_t4210400802 ** get_address_of_onDragOver_13() { return &___onDragOver_13; }
	inline void set_onDragOver_13(List_1_t4210400802 * value)
	{
		___onDragOver_13 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOver_13), value);
	}

	inline static int32_t get_offset_of_onDragOut_14() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onDragOut_14)); }
	inline List_1_t4210400802 * get_onDragOut_14() const { return ___onDragOut_14; }
	inline List_1_t4210400802 ** get_address_of_onDragOut_14() { return &___onDragOut_14; }
	inline void set_onDragOut_14(List_1_t4210400802 * value)
	{
		___onDragOut_14 = value;
		Il2CppCodeGenWriteBarrier((&___onDragOut_14), value);
	}

	inline static int32_t get_offset_of_onDrag_15() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782, ___onDrag_15)); }
	inline List_1_t4210400802 * get_onDrag_15() const { return ___onDrag_15; }
	inline List_1_t4210400802 ** get_address_of_onDrag_15() { return &___onDrag_15; }
	inline void set_onDrag_15(List_1_t4210400802 * value)
	{
		___onDrag_15 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_15), value);
	}
};

struct UIEventTrigger_t4038454782_StaticFields
{
public:
	// UIEventTrigger UIEventTrigger::current
	UIEventTrigger_t4038454782 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIEventTrigger_t4038454782_StaticFields, ___current_2)); }
	inline UIEventTrigger_t4038454782 * get_current_2() const { return ___current_2; }
	inline UIEventTrigger_t4038454782 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIEventTrigger_t4038454782 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTTRIGGER_T4038454782_H
#ifndef UIGRID_T1536638187_H
#define UIGRID_T1536638187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIGrid
struct  UIGrid_t1536638187  : public UIWidgetContainer_t30162560
{
public:
	// UIGrid/Arrangement UIGrid::arrangement
	int32_t ___arrangement_2;
	// UIGrid/Sorting UIGrid::sorting
	int32_t ___sorting_3;
	// UIWidget/Pivot UIGrid::pivot
	int32_t ___pivot_4;
	// System.Int32 UIGrid::maxPerLine
	int32_t ___maxPerLine_5;
	// System.Single UIGrid::cellWidth
	float ___cellWidth_6;
	// System.Single UIGrid::cellHeight
	float ___cellHeight_7;
	// System.Boolean UIGrid::animateSmoothly
	bool ___animateSmoothly_8;
	// System.Boolean UIGrid::hideInactive
	bool ___hideInactive_9;
	// System.Boolean UIGrid::keepWithinPanel
	bool ___keepWithinPanel_10;
	// UIGrid/OnReposition UIGrid::onReposition
	OnReposition_t1372889220 * ___onReposition_11;
	// System.Comparison`1<UnityEngine.Transform> UIGrid::onCustomSort
	Comparison_1_t3375297100 * ___onCustomSort_12;
	// System.Boolean UIGrid::sorted
	bool ___sorted_13;
	// System.Boolean UIGrid::mReposition
	bool ___mReposition_14;
	// UIPanel UIGrid::mPanel
	UIPanel_t1716472341 * ___mPanel_15;
	// System.Boolean UIGrid::mInitDone
	bool ___mInitDone_16;

public:
	inline static int32_t get_offset_of_arrangement_2() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___arrangement_2)); }
	inline int32_t get_arrangement_2() const { return ___arrangement_2; }
	inline int32_t* get_address_of_arrangement_2() { return &___arrangement_2; }
	inline void set_arrangement_2(int32_t value)
	{
		___arrangement_2 = value;
	}

	inline static int32_t get_offset_of_sorting_3() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___sorting_3)); }
	inline int32_t get_sorting_3() const { return ___sorting_3; }
	inline int32_t* get_address_of_sorting_3() { return &___sorting_3; }
	inline void set_sorting_3(int32_t value)
	{
		___sorting_3 = value;
	}

	inline static int32_t get_offset_of_pivot_4() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___pivot_4)); }
	inline int32_t get_pivot_4() const { return ___pivot_4; }
	inline int32_t* get_address_of_pivot_4() { return &___pivot_4; }
	inline void set_pivot_4(int32_t value)
	{
		___pivot_4 = value;
	}

	inline static int32_t get_offset_of_maxPerLine_5() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___maxPerLine_5)); }
	inline int32_t get_maxPerLine_5() const { return ___maxPerLine_5; }
	inline int32_t* get_address_of_maxPerLine_5() { return &___maxPerLine_5; }
	inline void set_maxPerLine_5(int32_t value)
	{
		___maxPerLine_5 = value;
	}

	inline static int32_t get_offset_of_cellWidth_6() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___cellWidth_6)); }
	inline float get_cellWidth_6() const { return ___cellWidth_6; }
	inline float* get_address_of_cellWidth_6() { return &___cellWidth_6; }
	inline void set_cellWidth_6(float value)
	{
		___cellWidth_6 = value;
	}

	inline static int32_t get_offset_of_cellHeight_7() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___cellHeight_7)); }
	inline float get_cellHeight_7() const { return ___cellHeight_7; }
	inline float* get_address_of_cellHeight_7() { return &___cellHeight_7; }
	inline void set_cellHeight_7(float value)
	{
		___cellHeight_7 = value;
	}

	inline static int32_t get_offset_of_animateSmoothly_8() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___animateSmoothly_8)); }
	inline bool get_animateSmoothly_8() const { return ___animateSmoothly_8; }
	inline bool* get_address_of_animateSmoothly_8() { return &___animateSmoothly_8; }
	inline void set_animateSmoothly_8(bool value)
	{
		___animateSmoothly_8 = value;
	}

	inline static int32_t get_offset_of_hideInactive_9() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___hideInactive_9)); }
	inline bool get_hideInactive_9() const { return ___hideInactive_9; }
	inline bool* get_address_of_hideInactive_9() { return &___hideInactive_9; }
	inline void set_hideInactive_9(bool value)
	{
		___hideInactive_9 = value;
	}

	inline static int32_t get_offset_of_keepWithinPanel_10() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___keepWithinPanel_10)); }
	inline bool get_keepWithinPanel_10() const { return ___keepWithinPanel_10; }
	inline bool* get_address_of_keepWithinPanel_10() { return &___keepWithinPanel_10; }
	inline void set_keepWithinPanel_10(bool value)
	{
		___keepWithinPanel_10 = value;
	}

	inline static int32_t get_offset_of_onReposition_11() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___onReposition_11)); }
	inline OnReposition_t1372889220 * get_onReposition_11() const { return ___onReposition_11; }
	inline OnReposition_t1372889220 ** get_address_of_onReposition_11() { return &___onReposition_11; }
	inline void set_onReposition_11(OnReposition_t1372889220 * value)
	{
		___onReposition_11 = value;
		Il2CppCodeGenWriteBarrier((&___onReposition_11), value);
	}

	inline static int32_t get_offset_of_onCustomSort_12() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___onCustomSort_12)); }
	inline Comparison_1_t3375297100 * get_onCustomSort_12() const { return ___onCustomSort_12; }
	inline Comparison_1_t3375297100 ** get_address_of_onCustomSort_12() { return &___onCustomSort_12; }
	inline void set_onCustomSort_12(Comparison_1_t3375297100 * value)
	{
		___onCustomSort_12 = value;
		Il2CppCodeGenWriteBarrier((&___onCustomSort_12), value);
	}

	inline static int32_t get_offset_of_sorted_13() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___sorted_13)); }
	inline bool get_sorted_13() const { return ___sorted_13; }
	inline bool* get_address_of_sorted_13() { return &___sorted_13; }
	inline void set_sorted_13(bool value)
	{
		___sorted_13 = value;
	}

	inline static int32_t get_offset_of_mReposition_14() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___mReposition_14)); }
	inline bool get_mReposition_14() const { return ___mReposition_14; }
	inline bool* get_address_of_mReposition_14() { return &___mReposition_14; }
	inline void set_mReposition_14(bool value)
	{
		___mReposition_14 = value;
	}

	inline static int32_t get_offset_of_mPanel_15() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___mPanel_15)); }
	inline UIPanel_t1716472341 * get_mPanel_15() const { return ___mPanel_15; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_15() { return &___mPanel_15; }
	inline void set_mPanel_15(UIPanel_t1716472341 * value)
	{
		___mPanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_15), value);
	}

	inline static int32_t get_offset_of_mInitDone_16() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187, ___mInitDone_16)); }
	inline bool get_mInitDone_16() const { return ___mInitDone_16; }
	inline bool* get_address_of_mInitDone_16() { return &___mInitDone_16; }
	inline void set_mInitDone_16(bool value)
	{
		___mInitDone_16 = value;
	}
};

struct UIGrid_t1536638187_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.Transform> UIGrid::<>f__mg$cache0
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache0_17;
	// System.Comparison`1<UnityEngine.Transform> UIGrid::<>f__mg$cache1
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache1_18;
	// System.Comparison`1<UnityEngine.Transform> UIGrid::<>f__mg$cache2
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache2_19;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_17() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187_StaticFields, ___U3CU3Ef__mgU24cache0_17)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache0_17() const { return ___U3CU3Ef__mgU24cache0_17; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache0_17() { return &___U3CU3Ef__mgU24cache0_17; }
	inline void set_U3CU3Ef__mgU24cache0_17(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_18() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187_StaticFields, ___U3CU3Ef__mgU24cache1_18)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache1_18() const { return ___U3CU3Ef__mgU24cache1_18; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache1_18() { return &___U3CU3Ef__mgU24cache1_18; }
	inline void set_U3CU3Ef__mgU24cache1_18(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_19() { return static_cast<int32_t>(offsetof(UIGrid_t1536638187_StaticFields, ___U3CU3Ef__mgU24cache2_19)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache2_19() const { return ___U3CU3Ef__mgU24cache2_19; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache2_19() { return &___U3CU3Ef__mgU24cache2_19; }
	inline void set_U3CU3Ef__mgU24cache2_19(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache2_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRID_T1536638187_H
#ifndef BALLBUTTONASSISSTANT_T1104794610_H
#define BALLBUTTONASSISSTANT_T1104794610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallButtonAssisstant
struct  BallButtonAssisstant_t1104794610  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject BallButtonAssisstant::_icon
	GameObject_t1113636619 * ____icon_5;

public:
	inline static int32_t get_offset_of__icon_5() { return static_cast<int32_t>(offsetof(BallButtonAssisstant_t1104794610, ____icon_5)); }
	inline GameObject_t1113636619 * get__icon_5() const { return ____icon_5; }
	inline GameObject_t1113636619 ** get_address_of__icon_5() { return &____icon_5; }
	inline void set__icon_5(GameObject_t1113636619 * value)
	{
		____icon_5 = value;
		Il2CppCodeGenWriteBarrier((&____icon_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLBUTTONASSISSTANT_T1104794610_H
#ifndef UIBUTTONKEYS_T486517330_H
#define UIBUTTONKEYS_T486517330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonKeys
struct  UIButtonKeys_t486517330  : public UIKeyNavigation_t4244956403
{
public:
	// UIButtonKeys UIButtonKeys::selectOnClick
	UIButtonKeys_t486517330 * ___selectOnClick_13;
	// UIButtonKeys UIButtonKeys::selectOnUp
	UIButtonKeys_t486517330 * ___selectOnUp_14;
	// UIButtonKeys UIButtonKeys::selectOnDown
	UIButtonKeys_t486517330 * ___selectOnDown_15;
	// UIButtonKeys UIButtonKeys::selectOnLeft
	UIButtonKeys_t486517330 * ___selectOnLeft_16;
	// UIButtonKeys UIButtonKeys::selectOnRight
	UIButtonKeys_t486517330 * ___selectOnRight_17;

public:
	inline static int32_t get_offset_of_selectOnClick_13() { return static_cast<int32_t>(offsetof(UIButtonKeys_t486517330, ___selectOnClick_13)); }
	inline UIButtonKeys_t486517330 * get_selectOnClick_13() const { return ___selectOnClick_13; }
	inline UIButtonKeys_t486517330 ** get_address_of_selectOnClick_13() { return &___selectOnClick_13; }
	inline void set_selectOnClick_13(UIButtonKeys_t486517330 * value)
	{
		___selectOnClick_13 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnClick_13), value);
	}

	inline static int32_t get_offset_of_selectOnUp_14() { return static_cast<int32_t>(offsetof(UIButtonKeys_t486517330, ___selectOnUp_14)); }
	inline UIButtonKeys_t486517330 * get_selectOnUp_14() const { return ___selectOnUp_14; }
	inline UIButtonKeys_t486517330 ** get_address_of_selectOnUp_14() { return &___selectOnUp_14; }
	inline void set_selectOnUp_14(UIButtonKeys_t486517330 * value)
	{
		___selectOnUp_14 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnUp_14), value);
	}

	inline static int32_t get_offset_of_selectOnDown_15() { return static_cast<int32_t>(offsetof(UIButtonKeys_t486517330, ___selectOnDown_15)); }
	inline UIButtonKeys_t486517330 * get_selectOnDown_15() const { return ___selectOnDown_15; }
	inline UIButtonKeys_t486517330 ** get_address_of_selectOnDown_15() { return &___selectOnDown_15; }
	inline void set_selectOnDown_15(UIButtonKeys_t486517330 * value)
	{
		___selectOnDown_15 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnDown_15), value);
	}

	inline static int32_t get_offset_of_selectOnLeft_16() { return static_cast<int32_t>(offsetof(UIButtonKeys_t486517330, ___selectOnLeft_16)); }
	inline UIButtonKeys_t486517330 * get_selectOnLeft_16() const { return ___selectOnLeft_16; }
	inline UIButtonKeys_t486517330 ** get_address_of_selectOnLeft_16() { return &___selectOnLeft_16; }
	inline void set_selectOnLeft_16(UIButtonKeys_t486517330 * value)
	{
		___selectOnLeft_16 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnLeft_16), value);
	}

	inline static int32_t get_offset_of_selectOnRight_17() { return static_cast<int32_t>(offsetof(UIButtonKeys_t486517330, ___selectOnRight_17)); }
	inline UIButtonKeys_t486517330 * get_selectOnRight_17() const { return ___selectOnRight_17; }
	inline UIButtonKeys_t486517330 ** get_address_of_selectOnRight_17() { return &___selectOnRight_17; }
	inline void set_selectOnRight_17(UIButtonKeys_t486517330 * value)
	{
		___selectOnRight_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnRight_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONKEYS_T486517330_H
#ifndef UITABLE_T3168834800_H
#define UITABLE_T3168834800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITable
struct  UITable_t3168834800  : public UIWidgetContainer_t30162560
{
public:
	// System.Int32 UITable::columns
	int32_t ___columns_2;
	// UITable/Direction UITable::direction
	int32_t ___direction_3;
	// UITable/Sorting UITable::sorting
	int32_t ___sorting_4;
	// UIWidget/Pivot UITable::pivot
	int32_t ___pivot_5;
	// UIWidget/Pivot UITable::cellAlignment
	int32_t ___cellAlignment_6;
	// System.Boolean UITable::hideInactive
	bool ___hideInactive_7;
	// System.Boolean UITable::keepWithinPanel
	bool ___keepWithinPanel_8;
	// UnityEngine.Vector2 UITable::padding
	Vector2_t2156229523  ___padding_9;
	// UITable/OnReposition UITable::onReposition
	OnReposition_t3913508630 * ___onReposition_10;
	// System.Comparison`1<UnityEngine.Transform> UITable::onCustomSort
	Comparison_1_t3375297100 * ___onCustomSort_11;
	// UIPanel UITable::mPanel
	UIPanel_t1716472341 * ___mPanel_12;
	// System.Boolean UITable::mInitDone
	bool ___mInitDone_13;
	// System.Boolean UITable::mReposition
	bool ___mReposition_14;

public:
	inline static int32_t get_offset_of_columns_2() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___columns_2)); }
	inline int32_t get_columns_2() const { return ___columns_2; }
	inline int32_t* get_address_of_columns_2() { return &___columns_2; }
	inline void set_columns_2(int32_t value)
	{
		___columns_2 = value;
	}

	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___direction_3)); }
	inline int32_t get_direction_3() const { return ___direction_3; }
	inline int32_t* get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(int32_t value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_sorting_4() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___sorting_4)); }
	inline int32_t get_sorting_4() const { return ___sorting_4; }
	inline int32_t* get_address_of_sorting_4() { return &___sorting_4; }
	inline void set_sorting_4(int32_t value)
	{
		___sorting_4 = value;
	}

	inline static int32_t get_offset_of_pivot_5() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___pivot_5)); }
	inline int32_t get_pivot_5() const { return ___pivot_5; }
	inline int32_t* get_address_of_pivot_5() { return &___pivot_5; }
	inline void set_pivot_5(int32_t value)
	{
		___pivot_5 = value;
	}

	inline static int32_t get_offset_of_cellAlignment_6() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___cellAlignment_6)); }
	inline int32_t get_cellAlignment_6() const { return ___cellAlignment_6; }
	inline int32_t* get_address_of_cellAlignment_6() { return &___cellAlignment_6; }
	inline void set_cellAlignment_6(int32_t value)
	{
		___cellAlignment_6 = value;
	}

	inline static int32_t get_offset_of_hideInactive_7() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___hideInactive_7)); }
	inline bool get_hideInactive_7() const { return ___hideInactive_7; }
	inline bool* get_address_of_hideInactive_7() { return &___hideInactive_7; }
	inline void set_hideInactive_7(bool value)
	{
		___hideInactive_7 = value;
	}

	inline static int32_t get_offset_of_keepWithinPanel_8() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___keepWithinPanel_8)); }
	inline bool get_keepWithinPanel_8() const { return ___keepWithinPanel_8; }
	inline bool* get_address_of_keepWithinPanel_8() { return &___keepWithinPanel_8; }
	inline void set_keepWithinPanel_8(bool value)
	{
		___keepWithinPanel_8 = value;
	}

	inline static int32_t get_offset_of_padding_9() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___padding_9)); }
	inline Vector2_t2156229523  get_padding_9() const { return ___padding_9; }
	inline Vector2_t2156229523 * get_address_of_padding_9() { return &___padding_9; }
	inline void set_padding_9(Vector2_t2156229523  value)
	{
		___padding_9 = value;
	}

	inline static int32_t get_offset_of_onReposition_10() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___onReposition_10)); }
	inline OnReposition_t3913508630 * get_onReposition_10() const { return ___onReposition_10; }
	inline OnReposition_t3913508630 ** get_address_of_onReposition_10() { return &___onReposition_10; }
	inline void set_onReposition_10(OnReposition_t3913508630 * value)
	{
		___onReposition_10 = value;
		Il2CppCodeGenWriteBarrier((&___onReposition_10), value);
	}

	inline static int32_t get_offset_of_onCustomSort_11() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___onCustomSort_11)); }
	inline Comparison_1_t3375297100 * get_onCustomSort_11() const { return ___onCustomSort_11; }
	inline Comparison_1_t3375297100 ** get_address_of_onCustomSort_11() { return &___onCustomSort_11; }
	inline void set_onCustomSort_11(Comparison_1_t3375297100 * value)
	{
		___onCustomSort_11 = value;
		Il2CppCodeGenWriteBarrier((&___onCustomSort_11), value);
	}

	inline static int32_t get_offset_of_mPanel_12() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___mPanel_12)); }
	inline UIPanel_t1716472341 * get_mPanel_12() const { return ___mPanel_12; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_12() { return &___mPanel_12; }
	inline void set_mPanel_12(UIPanel_t1716472341 * value)
	{
		___mPanel_12 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_12), value);
	}

	inline static int32_t get_offset_of_mInitDone_13() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___mInitDone_13)); }
	inline bool get_mInitDone_13() const { return ___mInitDone_13; }
	inline bool* get_address_of_mInitDone_13() { return &___mInitDone_13; }
	inline void set_mInitDone_13(bool value)
	{
		___mInitDone_13 = value;
	}

	inline static int32_t get_offset_of_mReposition_14() { return static_cast<int32_t>(offsetof(UITable_t3168834800, ___mReposition_14)); }
	inline bool get_mReposition_14() const { return ___mReposition_14; }
	inline bool* get_address_of_mReposition_14() { return &___mReposition_14; }
	inline void set_mReposition_14(bool value)
	{
		___mReposition_14 = value;
	}
};

struct UITable_t3168834800_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.Transform> UITable::<>f__mg$cache0
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache0_15;
	// System.Comparison`1<UnityEngine.Transform> UITable::<>f__mg$cache1
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache1_16;
	// System.Comparison`1<UnityEngine.Transform> UITable::<>f__mg$cache2
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache2_17;
	// System.Comparison`1<UnityEngine.Transform> UITable::<>f__mg$cache3
	Comparison_1_t3375297100 * ___U3CU3Ef__mgU24cache3_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_15() { return static_cast<int32_t>(offsetof(UITable_t3168834800_StaticFields, ___U3CU3Ef__mgU24cache0_15)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache0_15() const { return ___U3CU3Ef__mgU24cache0_15; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache0_15() { return &___U3CU3Ef__mgU24cache0_15; }
	inline void set_U3CU3Ef__mgU24cache0_15(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_16() { return static_cast<int32_t>(offsetof(UITable_t3168834800_StaticFields, ___U3CU3Ef__mgU24cache1_16)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache1_16() const { return ___U3CU3Ef__mgU24cache1_16; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache1_16() { return &___U3CU3Ef__mgU24cache1_16; }
	inline void set_U3CU3Ef__mgU24cache1_16(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_17() { return static_cast<int32_t>(offsetof(UITable_t3168834800_StaticFields, ___U3CU3Ef__mgU24cache2_17)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache2_17() const { return ___U3CU3Ef__mgU24cache2_17; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache2_17() { return &___U3CU3Ef__mgU24cache2_17; }
	inline void set_U3CU3Ef__mgU24cache2_17(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache2_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_18() { return static_cast<int32_t>(offsetof(UITable_t3168834800_StaticFields, ___U3CU3Ef__mgU24cache3_18)); }
	inline Comparison_1_t3375297100 * get_U3CU3Ef__mgU24cache3_18() const { return ___U3CU3Ef__mgU24cache3_18; }
	inline Comparison_1_t3375297100 ** get_address_of_U3CU3Ef__mgU24cache3_18() { return &___U3CU3Ef__mgU24cache3_18; }
	inline void set_U3CU3Ef__mgU24cache3_18(Comparison_1_t3375297100 * value)
	{
		___U3CU3Ef__mgU24cache3_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITABLE_T3168834800_H
#ifndef CLOSERECTBUTTONASSISSTANT_T998390754_H
#define CLOSERECTBUTTONASSISSTANT_T998390754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseRectButtonAssisstant
struct  CloseRectButtonAssisstant_t998390754  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject CloseRectButtonAssisstant::_dialogRect
	GameObject_t1113636619 * ____dialogRect_5;

public:
	inline static int32_t get_offset_of__dialogRect_5() { return static_cast<int32_t>(offsetof(CloseRectButtonAssisstant_t998390754, ____dialogRect_5)); }
	inline GameObject_t1113636619 * get__dialogRect_5() const { return ____dialogRect_5; }
	inline GameObject_t1113636619 ** get_address_of__dialogRect_5() { return &____dialogRect_5; }
	inline void set__dialogRect_5(GameObject_t1113636619 * value)
	{
		____dialogRect_5 = value;
		Il2CppCodeGenWriteBarrier((&____dialogRect_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSERECTBUTTONASSISSTANT_T998390754_H
#ifndef CONTINUEGAMEBUTTONASSISTANT_T4254328874_H
#define CONTINUEGAMEBUTTONASSISTANT_T4254328874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContinueGameButtonAssistant
struct  ContinueGameButtonAssistant_t4254328874  : public ButtonAssistant_t3922690862
{
public:
	// System.Boolean ContinueGameButtonAssistant::showAds
	bool ___showAds_5;
	// UnityEngine.GameObject ContinueGameButtonAssistant::dialog
	GameObject_t1113636619 * ___dialog_6;

public:
	inline static int32_t get_offset_of_showAds_5() { return static_cast<int32_t>(offsetof(ContinueGameButtonAssistant_t4254328874, ___showAds_5)); }
	inline bool get_showAds_5() const { return ___showAds_5; }
	inline bool* get_address_of_showAds_5() { return &___showAds_5; }
	inline void set_showAds_5(bool value)
	{
		___showAds_5 = value;
	}

	inline static int32_t get_offset_of_dialog_6() { return static_cast<int32_t>(offsetof(ContinueGameButtonAssistant_t4254328874, ___dialog_6)); }
	inline GameObject_t1113636619 * get_dialog_6() const { return ___dialog_6; }
	inline GameObject_t1113636619 ** get_address_of_dialog_6() { return &___dialog_6; }
	inline void set_dialog_6(GameObject_t1113636619 * value)
	{
		___dialog_6 = value;
		Il2CppCodeGenWriteBarrier((&___dialog_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTINUEGAMEBUTTONASSISTANT_T4254328874_H
#ifndef UIPOPUPLIST_T4167399471_H
#define UIPOPUPLIST_T4167399471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList
struct  UIPopupList_t4167399471  : public UIWidgetContainer_t30162560
{
public:
	// UIAtlas UIPopupList::atlas
	UIAtlas_t3195533529 * ___atlas_6;
	// UIFont UIPopupList::bitmapFont
	UIFont_t2766063701 * ___bitmapFont_7;
	// UnityEngine.Font UIPopupList::trueTypeFont
	Font_t1956802104 * ___trueTypeFont_8;
	// System.Int32 UIPopupList::fontSize
	int32_t ___fontSize_9;
	// UnityEngine.FontStyle UIPopupList::fontStyle
	int32_t ___fontStyle_10;
	// System.String UIPopupList::backgroundSprite
	String_t* ___backgroundSprite_11;
	// System.String UIPopupList::highlightSprite
	String_t* ___highlightSprite_12;
	// UnityEngine.Sprite UIPopupList::background2DSprite
	Sprite_t280657092 * ___background2DSprite_13;
	// UnityEngine.Sprite UIPopupList::highlight2DSprite
	Sprite_t280657092 * ___highlight2DSprite_14;
	// UIPopupList/Position UIPopupList::position
	int32_t ___position_15;
	// UIPopupList/Selection UIPopupList::selection
	int32_t ___selection_16;
	// NGUIText/Alignment UIPopupList::alignment
	int32_t ___alignment_17;
	// System.Collections.Generic.List`1<System.String> UIPopupList::items
	List_1_t3319525431 * ___items_18;
	// System.Collections.Generic.List`1<System.Object> UIPopupList::itemData
	List_1_t257213610 * ___itemData_19;
	// System.Collections.Generic.List`1<System.Action> UIPopupList::itemCallbacks
	List_1_t2736452219 * ___itemCallbacks_20;
	// UnityEngine.Vector2 UIPopupList::padding
	Vector2_t2156229523  ___padding_21;
	// UnityEngine.Color UIPopupList::textColor
	Color_t2555686324  ___textColor_22;
	// UnityEngine.Color UIPopupList::backgroundColor
	Color_t2555686324  ___backgroundColor_23;
	// UnityEngine.Color UIPopupList::highlightColor
	Color_t2555686324  ___highlightColor_24;
	// System.Boolean UIPopupList::isAnimated
	bool ___isAnimated_25;
	// System.Boolean UIPopupList::isLocalized
	bool ___isLocalized_26;
	// UILabel/Modifier UIPopupList::textModifier
	int32_t ___textModifier_27;
	// System.Boolean UIPopupList::separatePanel
	bool ___separatePanel_28;
	// System.Int32 UIPopupList::overlap
	int32_t ___overlap_29;
	// UIPopupList/OpenOn UIPopupList::openOn
	int32_t ___openOn_30;
	// System.Collections.Generic.List`1<EventDelegate> UIPopupList::onChange
	List_1_t4210400802 * ___onChange_31;
	// System.String UIPopupList::mSelectedItem
	String_t* ___mSelectedItem_32;
	// UIPanel UIPopupList::mPanel
	UIPanel_t1716472341 * ___mPanel_33;
	// UIBasicSprite UIPopupList::mBackground
	UIBasicSprite_t1521297657 * ___mBackground_34;
	// UIBasicSprite UIPopupList::mHighlight
	UIBasicSprite_t1521297657 * ___mHighlight_35;
	// UILabel UIPopupList::mHighlightedLabel
	UILabel_t3248798549 * ___mHighlightedLabel_36;
	// System.Collections.Generic.List`1<UILabel> UIPopupList::mLabelList
	List_1_t425905995 * ___mLabelList_37;
	// System.Single UIPopupList::mBgBorder
	float ___mBgBorder_38;
	// System.Boolean UIPopupList::keepValue
	bool ___keepValue_39;
	// UnityEngine.GameObject UIPopupList::mSelection
	GameObject_t1113636619 * ___mSelection_40;
	// System.Int32 UIPopupList::mOpenFrame
	int32_t ___mOpenFrame_41;
	// UnityEngine.GameObject UIPopupList::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_42;
	// System.String UIPopupList::functionName
	String_t* ___functionName_43;
	// System.Single UIPopupList::textScale
	float ___textScale_44;
	// UIFont UIPopupList::font
	UIFont_t2766063701 * ___font_45;
	// UILabel UIPopupList::textLabel
	UILabel_t3248798549 * ___textLabel_46;
	// UnityEngine.Vector3 UIPopupList::startingPosition
	Vector3_t3722313464  ___startingPosition_47;
	// UIPopupList/LegacyEvent UIPopupList::mLegacyEvent
	LegacyEvent_t2749056879 * ___mLegacyEvent_48;
	// System.Boolean UIPopupList::mExecuting
	bool ___mExecuting_49;
	// System.Boolean UIPopupList::mUseDynamicFont
	bool ___mUseDynamicFont_50;
	// System.Boolean UIPopupList::mStarted
	bool ___mStarted_51;
	// System.Boolean UIPopupList::mTweening
	bool ___mTweening_52;
	// UnityEngine.GameObject UIPopupList::source
	GameObject_t1113636619 * ___source_53;

public:
	inline static int32_t get_offset_of_atlas_6() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___atlas_6)); }
	inline UIAtlas_t3195533529 * get_atlas_6() const { return ___atlas_6; }
	inline UIAtlas_t3195533529 ** get_address_of_atlas_6() { return &___atlas_6; }
	inline void set_atlas_6(UIAtlas_t3195533529 * value)
	{
		___atlas_6 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_6), value);
	}

	inline static int32_t get_offset_of_bitmapFont_7() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___bitmapFont_7)); }
	inline UIFont_t2766063701 * get_bitmapFont_7() const { return ___bitmapFont_7; }
	inline UIFont_t2766063701 ** get_address_of_bitmapFont_7() { return &___bitmapFont_7; }
	inline void set_bitmapFont_7(UIFont_t2766063701 * value)
	{
		___bitmapFont_7 = value;
		Il2CppCodeGenWriteBarrier((&___bitmapFont_7), value);
	}

	inline static int32_t get_offset_of_trueTypeFont_8() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___trueTypeFont_8)); }
	inline Font_t1956802104 * get_trueTypeFont_8() const { return ___trueTypeFont_8; }
	inline Font_t1956802104 ** get_address_of_trueTypeFont_8() { return &___trueTypeFont_8; }
	inline void set_trueTypeFont_8(Font_t1956802104 * value)
	{
		___trueTypeFont_8 = value;
		Il2CppCodeGenWriteBarrier((&___trueTypeFont_8), value);
	}

	inline static int32_t get_offset_of_fontSize_9() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___fontSize_9)); }
	inline int32_t get_fontSize_9() const { return ___fontSize_9; }
	inline int32_t* get_address_of_fontSize_9() { return &___fontSize_9; }
	inline void set_fontSize_9(int32_t value)
	{
		___fontSize_9 = value;
	}

	inline static int32_t get_offset_of_fontStyle_10() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___fontStyle_10)); }
	inline int32_t get_fontStyle_10() const { return ___fontStyle_10; }
	inline int32_t* get_address_of_fontStyle_10() { return &___fontStyle_10; }
	inline void set_fontStyle_10(int32_t value)
	{
		___fontStyle_10 = value;
	}

	inline static int32_t get_offset_of_backgroundSprite_11() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___backgroundSprite_11)); }
	inline String_t* get_backgroundSprite_11() const { return ___backgroundSprite_11; }
	inline String_t** get_address_of_backgroundSprite_11() { return &___backgroundSprite_11; }
	inline void set_backgroundSprite_11(String_t* value)
	{
		___backgroundSprite_11 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSprite_11), value);
	}

	inline static int32_t get_offset_of_highlightSprite_12() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___highlightSprite_12)); }
	inline String_t* get_highlightSprite_12() const { return ___highlightSprite_12; }
	inline String_t** get_address_of_highlightSprite_12() { return &___highlightSprite_12; }
	inline void set_highlightSprite_12(String_t* value)
	{
		___highlightSprite_12 = value;
		Il2CppCodeGenWriteBarrier((&___highlightSprite_12), value);
	}

	inline static int32_t get_offset_of_background2DSprite_13() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___background2DSprite_13)); }
	inline Sprite_t280657092 * get_background2DSprite_13() const { return ___background2DSprite_13; }
	inline Sprite_t280657092 ** get_address_of_background2DSprite_13() { return &___background2DSprite_13; }
	inline void set_background2DSprite_13(Sprite_t280657092 * value)
	{
		___background2DSprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___background2DSprite_13), value);
	}

	inline static int32_t get_offset_of_highlight2DSprite_14() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___highlight2DSprite_14)); }
	inline Sprite_t280657092 * get_highlight2DSprite_14() const { return ___highlight2DSprite_14; }
	inline Sprite_t280657092 ** get_address_of_highlight2DSprite_14() { return &___highlight2DSprite_14; }
	inline void set_highlight2DSprite_14(Sprite_t280657092 * value)
	{
		___highlight2DSprite_14 = value;
		Il2CppCodeGenWriteBarrier((&___highlight2DSprite_14), value);
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___position_15)); }
	inline int32_t get_position_15() const { return ___position_15; }
	inline int32_t* get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(int32_t value)
	{
		___position_15 = value;
	}

	inline static int32_t get_offset_of_selection_16() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___selection_16)); }
	inline int32_t get_selection_16() const { return ___selection_16; }
	inline int32_t* get_address_of_selection_16() { return &___selection_16; }
	inline void set_selection_16(int32_t value)
	{
		___selection_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_items_18() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___items_18)); }
	inline List_1_t3319525431 * get_items_18() const { return ___items_18; }
	inline List_1_t3319525431 ** get_address_of_items_18() { return &___items_18; }
	inline void set_items_18(List_1_t3319525431 * value)
	{
		___items_18 = value;
		Il2CppCodeGenWriteBarrier((&___items_18), value);
	}

	inline static int32_t get_offset_of_itemData_19() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___itemData_19)); }
	inline List_1_t257213610 * get_itemData_19() const { return ___itemData_19; }
	inline List_1_t257213610 ** get_address_of_itemData_19() { return &___itemData_19; }
	inline void set_itemData_19(List_1_t257213610 * value)
	{
		___itemData_19 = value;
		Il2CppCodeGenWriteBarrier((&___itemData_19), value);
	}

	inline static int32_t get_offset_of_itemCallbacks_20() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___itemCallbacks_20)); }
	inline List_1_t2736452219 * get_itemCallbacks_20() const { return ___itemCallbacks_20; }
	inline List_1_t2736452219 ** get_address_of_itemCallbacks_20() { return &___itemCallbacks_20; }
	inline void set_itemCallbacks_20(List_1_t2736452219 * value)
	{
		___itemCallbacks_20 = value;
		Il2CppCodeGenWriteBarrier((&___itemCallbacks_20), value);
	}

	inline static int32_t get_offset_of_padding_21() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___padding_21)); }
	inline Vector2_t2156229523  get_padding_21() const { return ___padding_21; }
	inline Vector2_t2156229523 * get_address_of_padding_21() { return &___padding_21; }
	inline void set_padding_21(Vector2_t2156229523  value)
	{
		___padding_21 = value;
	}

	inline static int32_t get_offset_of_textColor_22() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___textColor_22)); }
	inline Color_t2555686324  get_textColor_22() const { return ___textColor_22; }
	inline Color_t2555686324 * get_address_of_textColor_22() { return &___textColor_22; }
	inline void set_textColor_22(Color_t2555686324  value)
	{
		___textColor_22 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_23() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___backgroundColor_23)); }
	inline Color_t2555686324  get_backgroundColor_23() const { return ___backgroundColor_23; }
	inline Color_t2555686324 * get_address_of_backgroundColor_23() { return &___backgroundColor_23; }
	inline void set_backgroundColor_23(Color_t2555686324  value)
	{
		___backgroundColor_23 = value;
	}

	inline static int32_t get_offset_of_highlightColor_24() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___highlightColor_24)); }
	inline Color_t2555686324  get_highlightColor_24() const { return ___highlightColor_24; }
	inline Color_t2555686324 * get_address_of_highlightColor_24() { return &___highlightColor_24; }
	inline void set_highlightColor_24(Color_t2555686324  value)
	{
		___highlightColor_24 = value;
	}

	inline static int32_t get_offset_of_isAnimated_25() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___isAnimated_25)); }
	inline bool get_isAnimated_25() const { return ___isAnimated_25; }
	inline bool* get_address_of_isAnimated_25() { return &___isAnimated_25; }
	inline void set_isAnimated_25(bool value)
	{
		___isAnimated_25 = value;
	}

	inline static int32_t get_offset_of_isLocalized_26() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___isLocalized_26)); }
	inline bool get_isLocalized_26() const { return ___isLocalized_26; }
	inline bool* get_address_of_isLocalized_26() { return &___isLocalized_26; }
	inline void set_isLocalized_26(bool value)
	{
		___isLocalized_26 = value;
	}

	inline static int32_t get_offset_of_textModifier_27() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___textModifier_27)); }
	inline int32_t get_textModifier_27() const { return ___textModifier_27; }
	inline int32_t* get_address_of_textModifier_27() { return &___textModifier_27; }
	inline void set_textModifier_27(int32_t value)
	{
		___textModifier_27 = value;
	}

	inline static int32_t get_offset_of_separatePanel_28() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___separatePanel_28)); }
	inline bool get_separatePanel_28() const { return ___separatePanel_28; }
	inline bool* get_address_of_separatePanel_28() { return &___separatePanel_28; }
	inline void set_separatePanel_28(bool value)
	{
		___separatePanel_28 = value;
	}

	inline static int32_t get_offset_of_overlap_29() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___overlap_29)); }
	inline int32_t get_overlap_29() const { return ___overlap_29; }
	inline int32_t* get_address_of_overlap_29() { return &___overlap_29; }
	inline void set_overlap_29(int32_t value)
	{
		___overlap_29 = value;
	}

	inline static int32_t get_offset_of_openOn_30() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___openOn_30)); }
	inline int32_t get_openOn_30() const { return ___openOn_30; }
	inline int32_t* get_address_of_openOn_30() { return &___openOn_30; }
	inline void set_openOn_30(int32_t value)
	{
		___openOn_30 = value;
	}

	inline static int32_t get_offset_of_onChange_31() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___onChange_31)); }
	inline List_1_t4210400802 * get_onChange_31() const { return ___onChange_31; }
	inline List_1_t4210400802 ** get_address_of_onChange_31() { return &___onChange_31; }
	inline void set_onChange_31(List_1_t4210400802 * value)
	{
		___onChange_31 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_31), value);
	}

	inline static int32_t get_offset_of_mSelectedItem_32() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mSelectedItem_32)); }
	inline String_t* get_mSelectedItem_32() const { return ___mSelectedItem_32; }
	inline String_t** get_address_of_mSelectedItem_32() { return &___mSelectedItem_32; }
	inline void set_mSelectedItem_32(String_t* value)
	{
		___mSelectedItem_32 = value;
		Il2CppCodeGenWriteBarrier((&___mSelectedItem_32), value);
	}

	inline static int32_t get_offset_of_mPanel_33() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mPanel_33)); }
	inline UIPanel_t1716472341 * get_mPanel_33() const { return ___mPanel_33; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_33() { return &___mPanel_33; }
	inline void set_mPanel_33(UIPanel_t1716472341 * value)
	{
		___mPanel_33 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_33), value);
	}

	inline static int32_t get_offset_of_mBackground_34() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mBackground_34)); }
	inline UIBasicSprite_t1521297657 * get_mBackground_34() const { return ___mBackground_34; }
	inline UIBasicSprite_t1521297657 ** get_address_of_mBackground_34() { return &___mBackground_34; }
	inline void set_mBackground_34(UIBasicSprite_t1521297657 * value)
	{
		___mBackground_34 = value;
		Il2CppCodeGenWriteBarrier((&___mBackground_34), value);
	}

	inline static int32_t get_offset_of_mHighlight_35() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mHighlight_35)); }
	inline UIBasicSprite_t1521297657 * get_mHighlight_35() const { return ___mHighlight_35; }
	inline UIBasicSprite_t1521297657 ** get_address_of_mHighlight_35() { return &___mHighlight_35; }
	inline void set_mHighlight_35(UIBasicSprite_t1521297657 * value)
	{
		___mHighlight_35 = value;
		Il2CppCodeGenWriteBarrier((&___mHighlight_35), value);
	}

	inline static int32_t get_offset_of_mHighlightedLabel_36() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mHighlightedLabel_36)); }
	inline UILabel_t3248798549 * get_mHighlightedLabel_36() const { return ___mHighlightedLabel_36; }
	inline UILabel_t3248798549 ** get_address_of_mHighlightedLabel_36() { return &___mHighlightedLabel_36; }
	inline void set_mHighlightedLabel_36(UILabel_t3248798549 * value)
	{
		___mHighlightedLabel_36 = value;
		Il2CppCodeGenWriteBarrier((&___mHighlightedLabel_36), value);
	}

	inline static int32_t get_offset_of_mLabelList_37() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mLabelList_37)); }
	inline List_1_t425905995 * get_mLabelList_37() const { return ___mLabelList_37; }
	inline List_1_t425905995 ** get_address_of_mLabelList_37() { return &___mLabelList_37; }
	inline void set_mLabelList_37(List_1_t425905995 * value)
	{
		___mLabelList_37 = value;
		Il2CppCodeGenWriteBarrier((&___mLabelList_37), value);
	}

	inline static int32_t get_offset_of_mBgBorder_38() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mBgBorder_38)); }
	inline float get_mBgBorder_38() const { return ___mBgBorder_38; }
	inline float* get_address_of_mBgBorder_38() { return &___mBgBorder_38; }
	inline void set_mBgBorder_38(float value)
	{
		___mBgBorder_38 = value;
	}

	inline static int32_t get_offset_of_keepValue_39() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___keepValue_39)); }
	inline bool get_keepValue_39() const { return ___keepValue_39; }
	inline bool* get_address_of_keepValue_39() { return &___keepValue_39; }
	inline void set_keepValue_39(bool value)
	{
		___keepValue_39 = value;
	}

	inline static int32_t get_offset_of_mSelection_40() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mSelection_40)); }
	inline GameObject_t1113636619 * get_mSelection_40() const { return ___mSelection_40; }
	inline GameObject_t1113636619 ** get_address_of_mSelection_40() { return &___mSelection_40; }
	inline void set_mSelection_40(GameObject_t1113636619 * value)
	{
		___mSelection_40 = value;
		Il2CppCodeGenWriteBarrier((&___mSelection_40), value);
	}

	inline static int32_t get_offset_of_mOpenFrame_41() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mOpenFrame_41)); }
	inline int32_t get_mOpenFrame_41() const { return ___mOpenFrame_41; }
	inline int32_t* get_address_of_mOpenFrame_41() { return &___mOpenFrame_41; }
	inline void set_mOpenFrame_41(int32_t value)
	{
		___mOpenFrame_41 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_42() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___eventReceiver_42)); }
	inline GameObject_t1113636619 * get_eventReceiver_42() const { return ___eventReceiver_42; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_42() { return &___eventReceiver_42; }
	inline void set_eventReceiver_42(GameObject_t1113636619 * value)
	{
		___eventReceiver_42 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_42), value);
	}

	inline static int32_t get_offset_of_functionName_43() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___functionName_43)); }
	inline String_t* get_functionName_43() const { return ___functionName_43; }
	inline String_t** get_address_of_functionName_43() { return &___functionName_43; }
	inline void set_functionName_43(String_t* value)
	{
		___functionName_43 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_43), value);
	}

	inline static int32_t get_offset_of_textScale_44() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___textScale_44)); }
	inline float get_textScale_44() const { return ___textScale_44; }
	inline float* get_address_of_textScale_44() { return &___textScale_44; }
	inline void set_textScale_44(float value)
	{
		___textScale_44 = value;
	}

	inline static int32_t get_offset_of_font_45() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___font_45)); }
	inline UIFont_t2766063701 * get_font_45() const { return ___font_45; }
	inline UIFont_t2766063701 ** get_address_of_font_45() { return &___font_45; }
	inline void set_font_45(UIFont_t2766063701 * value)
	{
		___font_45 = value;
		Il2CppCodeGenWriteBarrier((&___font_45), value);
	}

	inline static int32_t get_offset_of_textLabel_46() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___textLabel_46)); }
	inline UILabel_t3248798549 * get_textLabel_46() const { return ___textLabel_46; }
	inline UILabel_t3248798549 ** get_address_of_textLabel_46() { return &___textLabel_46; }
	inline void set_textLabel_46(UILabel_t3248798549 * value)
	{
		___textLabel_46 = value;
		Il2CppCodeGenWriteBarrier((&___textLabel_46), value);
	}

	inline static int32_t get_offset_of_startingPosition_47() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___startingPosition_47)); }
	inline Vector3_t3722313464  get_startingPosition_47() const { return ___startingPosition_47; }
	inline Vector3_t3722313464 * get_address_of_startingPosition_47() { return &___startingPosition_47; }
	inline void set_startingPosition_47(Vector3_t3722313464  value)
	{
		___startingPosition_47 = value;
	}

	inline static int32_t get_offset_of_mLegacyEvent_48() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mLegacyEvent_48)); }
	inline LegacyEvent_t2749056879 * get_mLegacyEvent_48() const { return ___mLegacyEvent_48; }
	inline LegacyEvent_t2749056879 ** get_address_of_mLegacyEvent_48() { return &___mLegacyEvent_48; }
	inline void set_mLegacyEvent_48(LegacyEvent_t2749056879 * value)
	{
		___mLegacyEvent_48 = value;
		Il2CppCodeGenWriteBarrier((&___mLegacyEvent_48), value);
	}

	inline static int32_t get_offset_of_mExecuting_49() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mExecuting_49)); }
	inline bool get_mExecuting_49() const { return ___mExecuting_49; }
	inline bool* get_address_of_mExecuting_49() { return &___mExecuting_49; }
	inline void set_mExecuting_49(bool value)
	{
		___mExecuting_49 = value;
	}

	inline static int32_t get_offset_of_mUseDynamicFont_50() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mUseDynamicFont_50)); }
	inline bool get_mUseDynamicFont_50() const { return ___mUseDynamicFont_50; }
	inline bool* get_address_of_mUseDynamicFont_50() { return &___mUseDynamicFont_50; }
	inline void set_mUseDynamicFont_50(bool value)
	{
		___mUseDynamicFont_50 = value;
	}

	inline static int32_t get_offset_of_mStarted_51() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mStarted_51)); }
	inline bool get_mStarted_51() const { return ___mStarted_51; }
	inline bool* get_address_of_mStarted_51() { return &___mStarted_51; }
	inline void set_mStarted_51(bool value)
	{
		___mStarted_51 = value;
	}

	inline static int32_t get_offset_of_mTweening_52() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mTweening_52)); }
	inline bool get_mTweening_52() const { return ___mTweening_52; }
	inline bool* get_address_of_mTweening_52() { return &___mTweening_52; }
	inline void set_mTweening_52(bool value)
	{
		___mTweening_52 = value;
	}

	inline static int32_t get_offset_of_source_53() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___source_53)); }
	inline GameObject_t1113636619 * get_source_53() const { return ___source_53; }
	inline GameObject_t1113636619 ** get_address_of_source_53() { return &___source_53; }
	inline void set_source_53(GameObject_t1113636619 * value)
	{
		___source_53 = value;
		Il2CppCodeGenWriteBarrier((&___source_53), value);
	}
};

struct UIPopupList_t4167399471_StaticFields
{
public:
	// UIPopupList UIPopupList::current
	UIPopupList_t4167399471 * ___current_2;
	// UnityEngine.GameObject UIPopupList::mChild
	GameObject_t1113636619 * ___mChild_3;
	// System.Single UIPopupList::mFadeOutComplete
	float ___mFadeOutComplete_4;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471_StaticFields, ___current_2)); }
	inline UIPopupList_t4167399471 * get_current_2() const { return ___current_2; }
	inline UIPopupList_t4167399471 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIPopupList_t4167399471 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}

	inline static int32_t get_offset_of_mChild_3() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471_StaticFields, ___mChild_3)); }
	inline GameObject_t1113636619 * get_mChild_3() const { return ___mChild_3; }
	inline GameObject_t1113636619 ** get_address_of_mChild_3() { return &___mChild_3; }
	inline void set_mChild_3(GameObject_t1113636619 * value)
	{
		___mChild_3 = value;
		Il2CppCodeGenWriteBarrier((&___mChild_3), value);
	}

	inline static int32_t get_offset_of_mFadeOutComplete_4() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471_StaticFields, ___mFadeOutComplete_4)); }
	inline float get_mFadeOutComplete_4() const { return ___mFadeOutComplete_4; }
	inline float* get_address_of_mFadeOutComplete_4() { return &___mFadeOutComplete_4; }
	inline void set_mFadeOutComplete_4(float value)
	{
		___mFadeOutComplete_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPLIST_T4167399471_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T4124865168_H
#define SINGLETONMONOBEHAVIOUR_1_T4124865168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<IAPController>
struct  SingletonMonoBehaviour_1_t4124865168  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t4124865168_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	IAPController_t1855063710 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t4124865168_StaticFields, ____instance_3)); }
	inline IAPController_t1855063710 * get__instance_3() const { return ____instance_3; }
	inline IAPController_t1855063710 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(IAPController_t1855063710 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T4124865168_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T967663633_H
#define SINGLETONMONOBEHAVIOUR_1_T967663633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<BallController>
struct  SingletonMonoBehaviour_1_t967663633  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t967663633_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	BallController_t2992829471 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t967663633_StaticFields, ____instance_3)); }
	inline BallController_t2992829471 * get__instance_3() const { return ____instance_3; }
	inline BallController_t2992829471 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(BallController_t2992829471 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T967663633_H
#ifndef UIPROGRESSBAR_T1222110469_H
#define UIPROGRESSBAR_T1222110469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIProgressBar
struct  UIProgressBar_t1222110469  : public UIWidgetContainer_t30162560
{
public:
	// UIProgressBar/OnDragFinished UIProgressBar::onDragFinished
	OnDragFinished_t3715779777 * ___onDragFinished_3;
	// UnityEngine.Transform UIProgressBar::thumb
	Transform_t3600365921 * ___thumb_4;
	// UIWidget UIProgressBar::mBG
	UIWidget_t3538521925 * ___mBG_5;
	// UIWidget UIProgressBar::mFG
	UIWidget_t3538521925 * ___mFG_6;
	// System.Single UIProgressBar::mValue
	float ___mValue_7;
	// UIProgressBar/FillDirection UIProgressBar::mFill
	int32_t ___mFill_8;
	// System.Boolean UIProgressBar::mStarted
	bool ___mStarted_9;
	// UnityEngine.Transform UIProgressBar::mTrans
	Transform_t3600365921 * ___mTrans_10;
	// System.Boolean UIProgressBar::mIsDirty
	bool ___mIsDirty_11;
	// UnityEngine.Camera UIProgressBar::mCam
	Camera_t4157153871 * ___mCam_12;
	// System.Single UIProgressBar::mOffset
	float ___mOffset_13;
	// System.Int32 UIProgressBar::numberOfSteps
	int32_t ___numberOfSteps_14;
	// System.Collections.Generic.List`1<EventDelegate> UIProgressBar::onChange
	List_1_t4210400802 * ___onChange_15;

public:
	inline static int32_t get_offset_of_onDragFinished_3() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___onDragFinished_3)); }
	inline OnDragFinished_t3715779777 * get_onDragFinished_3() const { return ___onDragFinished_3; }
	inline OnDragFinished_t3715779777 ** get_address_of_onDragFinished_3() { return &___onDragFinished_3; }
	inline void set_onDragFinished_3(OnDragFinished_t3715779777 * value)
	{
		___onDragFinished_3 = value;
		Il2CppCodeGenWriteBarrier((&___onDragFinished_3), value);
	}

	inline static int32_t get_offset_of_thumb_4() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___thumb_4)); }
	inline Transform_t3600365921 * get_thumb_4() const { return ___thumb_4; }
	inline Transform_t3600365921 ** get_address_of_thumb_4() { return &___thumb_4; }
	inline void set_thumb_4(Transform_t3600365921 * value)
	{
		___thumb_4 = value;
		Il2CppCodeGenWriteBarrier((&___thumb_4), value);
	}

	inline static int32_t get_offset_of_mBG_5() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mBG_5)); }
	inline UIWidget_t3538521925 * get_mBG_5() const { return ___mBG_5; }
	inline UIWidget_t3538521925 ** get_address_of_mBG_5() { return &___mBG_5; }
	inline void set_mBG_5(UIWidget_t3538521925 * value)
	{
		___mBG_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBG_5), value);
	}

	inline static int32_t get_offset_of_mFG_6() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mFG_6)); }
	inline UIWidget_t3538521925 * get_mFG_6() const { return ___mFG_6; }
	inline UIWidget_t3538521925 ** get_address_of_mFG_6() { return &___mFG_6; }
	inline void set_mFG_6(UIWidget_t3538521925 * value)
	{
		___mFG_6 = value;
		Il2CppCodeGenWriteBarrier((&___mFG_6), value);
	}

	inline static int32_t get_offset_of_mValue_7() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mValue_7)); }
	inline float get_mValue_7() const { return ___mValue_7; }
	inline float* get_address_of_mValue_7() { return &___mValue_7; }
	inline void set_mValue_7(float value)
	{
		___mValue_7 = value;
	}

	inline static int32_t get_offset_of_mFill_8() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mFill_8)); }
	inline int32_t get_mFill_8() const { return ___mFill_8; }
	inline int32_t* get_address_of_mFill_8() { return &___mFill_8; }
	inline void set_mFill_8(int32_t value)
	{
		___mFill_8 = value;
	}

	inline static int32_t get_offset_of_mStarted_9() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mStarted_9)); }
	inline bool get_mStarted_9() const { return ___mStarted_9; }
	inline bool* get_address_of_mStarted_9() { return &___mStarted_9; }
	inline void set_mStarted_9(bool value)
	{
		___mStarted_9 = value;
	}

	inline static int32_t get_offset_of_mTrans_10() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mTrans_10)); }
	inline Transform_t3600365921 * get_mTrans_10() const { return ___mTrans_10; }
	inline Transform_t3600365921 ** get_address_of_mTrans_10() { return &___mTrans_10; }
	inline void set_mTrans_10(Transform_t3600365921 * value)
	{
		___mTrans_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_10), value);
	}

	inline static int32_t get_offset_of_mIsDirty_11() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mIsDirty_11)); }
	inline bool get_mIsDirty_11() const { return ___mIsDirty_11; }
	inline bool* get_address_of_mIsDirty_11() { return &___mIsDirty_11; }
	inline void set_mIsDirty_11(bool value)
	{
		___mIsDirty_11 = value;
	}

	inline static int32_t get_offset_of_mCam_12() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mCam_12)); }
	inline Camera_t4157153871 * get_mCam_12() const { return ___mCam_12; }
	inline Camera_t4157153871 ** get_address_of_mCam_12() { return &___mCam_12; }
	inline void set_mCam_12(Camera_t4157153871 * value)
	{
		___mCam_12 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_12), value);
	}

	inline static int32_t get_offset_of_mOffset_13() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___mOffset_13)); }
	inline float get_mOffset_13() const { return ___mOffset_13; }
	inline float* get_address_of_mOffset_13() { return &___mOffset_13; }
	inline void set_mOffset_13(float value)
	{
		___mOffset_13 = value;
	}

	inline static int32_t get_offset_of_numberOfSteps_14() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___numberOfSteps_14)); }
	inline int32_t get_numberOfSteps_14() const { return ___numberOfSteps_14; }
	inline int32_t* get_address_of_numberOfSteps_14() { return &___numberOfSteps_14; }
	inline void set_numberOfSteps_14(int32_t value)
	{
		___numberOfSteps_14 = value;
	}

	inline static int32_t get_offset_of_onChange_15() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469, ___onChange_15)); }
	inline List_1_t4210400802 * get_onChange_15() const { return ___onChange_15; }
	inline List_1_t4210400802 ** get_address_of_onChange_15() { return &___onChange_15; }
	inline void set_onChange_15(List_1_t4210400802 * value)
	{
		___onChange_15 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_15), value);
	}
};

struct UIProgressBar_t1222110469_StaticFields
{
public:
	// UIProgressBar UIProgressBar::current
	UIProgressBar_t1222110469 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIProgressBar_t1222110469_StaticFields, ___current_2)); }
	inline UIProgressBar_t1222110469 * get_current_2() const { return ___current_2; }
	inline UIProgressBar_t1222110469 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIProgressBar_t1222110469 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPROGRESSBAR_T1222110469_H
#ifndef UITOGGLE_T4192126258_H
#define UITOGGLE_T4192126258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggle
struct  UIToggle_t4192126258  : public UIWidgetContainer_t30162560
{
public:
	// System.Int32 UIToggle::group
	int32_t ___group_4;
	// UIWidget UIToggle::activeSprite
	UIWidget_t3538521925 * ___activeSprite_5;
	// System.Boolean UIToggle::invertSpriteState
	bool ___invertSpriteState_6;
	// UnityEngine.Animation UIToggle::activeAnimation
	Animation_t3648466861 * ___activeAnimation_7;
	// UnityEngine.Animator UIToggle::animator
	Animator_t434523843 * ___animator_8;
	// UITweener UIToggle::tween
	UITweener_t260334902 * ___tween_9;
	// System.Boolean UIToggle::startsActive
	bool ___startsActive_10;
	// System.Boolean UIToggle::instantTween
	bool ___instantTween_11;
	// System.Boolean UIToggle::optionCanBeNone
	bool ___optionCanBeNone_12;
	// System.Collections.Generic.List`1<EventDelegate> UIToggle::onChange
	List_1_t4210400802 * ___onChange_13;
	// UIToggle/Validate UIToggle::validator
	Validate_t3702293971 * ___validator_14;
	// UISprite UIToggle::checkSprite
	UISprite_t194114938 * ___checkSprite_15;
	// UnityEngine.Animation UIToggle::checkAnimation
	Animation_t3648466861 * ___checkAnimation_16;
	// UnityEngine.GameObject UIToggle::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_17;
	// System.String UIToggle::functionName
	String_t* ___functionName_18;
	// System.Boolean UIToggle::startsChecked
	bool ___startsChecked_19;
	// System.Boolean UIToggle::mIsActive
	bool ___mIsActive_20;
	// System.Boolean UIToggle::mStarted
	bool ___mStarted_21;

public:
	inline static int32_t get_offset_of_group_4() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___group_4)); }
	inline int32_t get_group_4() const { return ___group_4; }
	inline int32_t* get_address_of_group_4() { return &___group_4; }
	inline void set_group_4(int32_t value)
	{
		___group_4 = value;
	}

	inline static int32_t get_offset_of_activeSprite_5() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___activeSprite_5)); }
	inline UIWidget_t3538521925 * get_activeSprite_5() const { return ___activeSprite_5; }
	inline UIWidget_t3538521925 ** get_address_of_activeSprite_5() { return &___activeSprite_5; }
	inline void set_activeSprite_5(UIWidget_t3538521925 * value)
	{
		___activeSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___activeSprite_5), value);
	}

	inline static int32_t get_offset_of_invertSpriteState_6() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___invertSpriteState_6)); }
	inline bool get_invertSpriteState_6() const { return ___invertSpriteState_6; }
	inline bool* get_address_of_invertSpriteState_6() { return &___invertSpriteState_6; }
	inline void set_invertSpriteState_6(bool value)
	{
		___invertSpriteState_6 = value;
	}

	inline static int32_t get_offset_of_activeAnimation_7() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___activeAnimation_7)); }
	inline Animation_t3648466861 * get_activeAnimation_7() const { return ___activeAnimation_7; }
	inline Animation_t3648466861 ** get_address_of_activeAnimation_7() { return &___activeAnimation_7; }
	inline void set_activeAnimation_7(Animation_t3648466861 * value)
	{
		___activeAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___activeAnimation_7), value);
	}

	inline static int32_t get_offset_of_animator_8() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___animator_8)); }
	inline Animator_t434523843 * get_animator_8() const { return ___animator_8; }
	inline Animator_t434523843 ** get_address_of_animator_8() { return &___animator_8; }
	inline void set_animator_8(Animator_t434523843 * value)
	{
		___animator_8 = value;
		Il2CppCodeGenWriteBarrier((&___animator_8), value);
	}

	inline static int32_t get_offset_of_tween_9() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___tween_9)); }
	inline UITweener_t260334902 * get_tween_9() const { return ___tween_9; }
	inline UITweener_t260334902 ** get_address_of_tween_9() { return &___tween_9; }
	inline void set_tween_9(UITweener_t260334902 * value)
	{
		___tween_9 = value;
		Il2CppCodeGenWriteBarrier((&___tween_9), value);
	}

	inline static int32_t get_offset_of_startsActive_10() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___startsActive_10)); }
	inline bool get_startsActive_10() const { return ___startsActive_10; }
	inline bool* get_address_of_startsActive_10() { return &___startsActive_10; }
	inline void set_startsActive_10(bool value)
	{
		___startsActive_10 = value;
	}

	inline static int32_t get_offset_of_instantTween_11() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___instantTween_11)); }
	inline bool get_instantTween_11() const { return ___instantTween_11; }
	inline bool* get_address_of_instantTween_11() { return &___instantTween_11; }
	inline void set_instantTween_11(bool value)
	{
		___instantTween_11 = value;
	}

	inline static int32_t get_offset_of_optionCanBeNone_12() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___optionCanBeNone_12)); }
	inline bool get_optionCanBeNone_12() const { return ___optionCanBeNone_12; }
	inline bool* get_address_of_optionCanBeNone_12() { return &___optionCanBeNone_12; }
	inline void set_optionCanBeNone_12(bool value)
	{
		___optionCanBeNone_12 = value;
	}

	inline static int32_t get_offset_of_onChange_13() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___onChange_13)); }
	inline List_1_t4210400802 * get_onChange_13() const { return ___onChange_13; }
	inline List_1_t4210400802 ** get_address_of_onChange_13() { return &___onChange_13; }
	inline void set_onChange_13(List_1_t4210400802 * value)
	{
		___onChange_13 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_13), value);
	}

	inline static int32_t get_offset_of_validator_14() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___validator_14)); }
	inline Validate_t3702293971 * get_validator_14() const { return ___validator_14; }
	inline Validate_t3702293971 ** get_address_of_validator_14() { return &___validator_14; }
	inline void set_validator_14(Validate_t3702293971 * value)
	{
		___validator_14 = value;
		Il2CppCodeGenWriteBarrier((&___validator_14), value);
	}

	inline static int32_t get_offset_of_checkSprite_15() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___checkSprite_15)); }
	inline UISprite_t194114938 * get_checkSprite_15() const { return ___checkSprite_15; }
	inline UISprite_t194114938 ** get_address_of_checkSprite_15() { return &___checkSprite_15; }
	inline void set_checkSprite_15(UISprite_t194114938 * value)
	{
		___checkSprite_15 = value;
		Il2CppCodeGenWriteBarrier((&___checkSprite_15), value);
	}

	inline static int32_t get_offset_of_checkAnimation_16() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___checkAnimation_16)); }
	inline Animation_t3648466861 * get_checkAnimation_16() const { return ___checkAnimation_16; }
	inline Animation_t3648466861 ** get_address_of_checkAnimation_16() { return &___checkAnimation_16; }
	inline void set_checkAnimation_16(Animation_t3648466861 * value)
	{
		___checkAnimation_16 = value;
		Il2CppCodeGenWriteBarrier((&___checkAnimation_16), value);
	}

	inline static int32_t get_offset_of_eventReceiver_17() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___eventReceiver_17)); }
	inline GameObject_t1113636619 * get_eventReceiver_17() const { return ___eventReceiver_17; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_17() { return &___eventReceiver_17; }
	inline void set_eventReceiver_17(GameObject_t1113636619 * value)
	{
		___eventReceiver_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_17), value);
	}

	inline static int32_t get_offset_of_functionName_18() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___functionName_18)); }
	inline String_t* get_functionName_18() const { return ___functionName_18; }
	inline String_t** get_address_of_functionName_18() { return &___functionName_18; }
	inline void set_functionName_18(String_t* value)
	{
		___functionName_18 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_18), value);
	}

	inline static int32_t get_offset_of_startsChecked_19() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___startsChecked_19)); }
	inline bool get_startsChecked_19() const { return ___startsChecked_19; }
	inline bool* get_address_of_startsChecked_19() { return &___startsChecked_19; }
	inline void set_startsChecked_19(bool value)
	{
		___startsChecked_19 = value;
	}

	inline static int32_t get_offset_of_mIsActive_20() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___mIsActive_20)); }
	inline bool get_mIsActive_20() const { return ___mIsActive_20; }
	inline bool* get_address_of_mIsActive_20() { return &___mIsActive_20; }
	inline void set_mIsActive_20(bool value)
	{
		___mIsActive_20 = value;
	}

	inline static int32_t get_offset_of_mStarted_21() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___mStarted_21)); }
	inline bool get_mStarted_21() const { return ___mStarted_21; }
	inline bool* get_address_of_mStarted_21() { return &___mStarted_21; }
	inline void set_mStarted_21(bool value)
	{
		___mStarted_21 = value;
	}
};

struct UIToggle_t4192126258_StaticFields
{
public:
	// BetterList`1<UIToggle> UIToggle::list
	BetterList_1_t3347146576 * ___list_2;
	// UIToggle UIToggle::current
	UIToggle_t4192126258 * ___current_3;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258_StaticFields, ___list_2)); }
	inline BetterList_1_t3347146576 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t3347146576 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t3347146576 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258_StaticFields, ___current_3)); }
	inline UIToggle_t4192126258 * get_current_3() const { return ___current_3; }
	inline UIToggle_t4192126258 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UIToggle_t4192126258 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLE_T4192126258_H
#ifndef UIBUTTONCOLOR_T2038074180_H
#define UIBUTTONCOLOR_T2038074180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonColor
struct  UIButtonColor_t2038074180  : public UIWidgetContainer_t30162560
{
public:
	// UnityEngine.GameObject UIButtonColor::tweenTarget
	GameObject_t1113636619 * ___tweenTarget_2;
	// UnityEngine.Color UIButtonColor::hover
	Color_t2555686324  ___hover_3;
	// UnityEngine.Color UIButtonColor::pressed
	Color_t2555686324  ___pressed_4;
	// UnityEngine.Color UIButtonColor::disabledColor
	Color_t2555686324  ___disabledColor_5;
	// System.Single UIButtonColor::duration
	float ___duration_6;
	// UnityEngine.Color UIButtonColor::mStartingColor
	Color_t2555686324  ___mStartingColor_7;
	// UnityEngine.Color UIButtonColor::mDefaultColor
	Color_t2555686324  ___mDefaultColor_8;
	// System.Boolean UIButtonColor::mInitDone
	bool ___mInitDone_9;
	// UIWidget UIButtonColor::mWidget
	UIWidget_t3538521925 * ___mWidget_10;
	// UIButtonColor/State UIButtonColor::mState
	int32_t ___mState_11;

public:
	inline static int32_t get_offset_of_tweenTarget_2() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___tweenTarget_2)); }
	inline GameObject_t1113636619 * get_tweenTarget_2() const { return ___tweenTarget_2; }
	inline GameObject_t1113636619 ** get_address_of_tweenTarget_2() { return &___tweenTarget_2; }
	inline void set_tweenTarget_2(GameObject_t1113636619 * value)
	{
		___tweenTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweenTarget_2), value);
	}

	inline static int32_t get_offset_of_hover_3() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___hover_3)); }
	inline Color_t2555686324  get_hover_3() const { return ___hover_3; }
	inline Color_t2555686324 * get_address_of_hover_3() { return &___hover_3; }
	inline void set_hover_3(Color_t2555686324  value)
	{
		___hover_3 = value;
	}

	inline static int32_t get_offset_of_pressed_4() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___pressed_4)); }
	inline Color_t2555686324  get_pressed_4() const { return ___pressed_4; }
	inline Color_t2555686324 * get_address_of_pressed_4() { return &___pressed_4; }
	inline void set_pressed_4(Color_t2555686324  value)
	{
		___pressed_4 = value;
	}

	inline static int32_t get_offset_of_disabledColor_5() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___disabledColor_5)); }
	inline Color_t2555686324  get_disabledColor_5() const { return ___disabledColor_5; }
	inline Color_t2555686324 * get_address_of_disabledColor_5() { return &___disabledColor_5; }
	inline void set_disabledColor_5(Color_t2555686324  value)
	{
		___disabledColor_5 = value;
	}

	inline static int32_t get_offset_of_duration_6() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___duration_6)); }
	inline float get_duration_6() const { return ___duration_6; }
	inline float* get_address_of_duration_6() { return &___duration_6; }
	inline void set_duration_6(float value)
	{
		___duration_6 = value;
	}

	inline static int32_t get_offset_of_mStartingColor_7() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___mStartingColor_7)); }
	inline Color_t2555686324  get_mStartingColor_7() const { return ___mStartingColor_7; }
	inline Color_t2555686324 * get_address_of_mStartingColor_7() { return &___mStartingColor_7; }
	inline void set_mStartingColor_7(Color_t2555686324  value)
	{
		___mStartingColor_7 = value;
	}

	inline static int32_t get_offset_of_mDefaultColor_8() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___mDefaultColor_8)); }
	inline Color_t2555686324  get_mDefaultColor_8() const { return ___mDefaultColor_8; }
	inline Color_t2555686324 * get_address_of_mDefaultColor_8() { return &___mDefaultColor_8; }
	inline void set_mDefaultColor_8(Color_t2555686324  value)
	{
		___mDefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_mInitDone_9() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___mInitDone_9)); }
	inline bool get_mInitDone_9() const { return ___mInitDone_9; }
	inline bool* get_address_of_mInitDone_9() { return &___mInitDone_9; }
	inline void set_mInitDone_9(bool value)
	{
		___mInitDone_9 = value;
	}

	inline static int32_t get_offset_of_mWidget_10() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___mWidget_10)); }
	inline UIWidget_t3538521925 * get_mWidget_10() const { return ___mWidget_10; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_10() { return &___mWidget_10; }
	inline void set_mWidget_10(UIWidget_t3538521925 * value)
	{
		___mWidget_10 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_10), value);
	}

	inline static int32_t get_offset_of_mState_11() { return static_cast<int32_t>(offsetof(UIButtonColor_t2038074180, ___mState_11)); }
	inline int32_t get_mState_11() const { return ___mState_11; }
	inline int32_t* get_address_of_mState_11() { return &___mState_11; }
	inline void set_mState_11(int32_t value)
	{
		___mState_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONCOLOR_T2038074180_H
#ifndef CLOSEHOUSEADSBUTTONASSISTANT_T3993876536_H
#define CLOSEHOUSEADSBUTTONASSISTANT_T3993876536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseHouseAdsButtonAssistant
struct  CloseHouseAdsButtonAssistant_t3993876536  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject CloseHouseAdsButtonAssistant::dialog
	GameObject_t1113636619 * ___dialog_5;
	// System.Boolean CloseHouseAdsButtonAssistant::showAds
	bool ___showAds_6;

public:
	inline static int32_t get_offset_of_dialog_5() { return static_cast<int32_t>(offsetof(CloseHouseAdsButtonAssistant_t3993876536, ___dialog_5)); }
	inline GameObject_t1113636619 * get_dialog_5() const { return ___dialog_5; }
	inline GameObject_t1113636619 ** get_address_of_dialog_5() { return &___dialog_5; }
	inline void set_dialog_5(GameObject_t1113636619 * value)
	{
		___dialog_5 = value;
		Il2CppCodeGenWriteBarrier((&___dialog_5), value);
	}

	inline static int32_t get_offset_of_showAds_6() { return static_cast<int32_t>(offsetof(CloseHouseAdsButtonAssistant_t3993876536, ___showAds_6)); }
	inline bool get_showAds_6() const { return ___showAds_6; }
	inline bool* get_address_of_showAds_6() { return &___showAds_6; }
	inline void set_showAds_6(bool value)
	{
		___showAds_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEHOUSEADSBUTTONASSISTANT_T3993876536_H
#ifndef BALLCONTROLLER_T2992829471_H
#define BALLCONTROLLER_T2992829471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallController
struct  BallController_t2992829471  : public SingletonMonoBehaviour_1_t967663633
{
public:
	// UILabel BallController::_numberLbl
	UILabel_t3248798549 * ____numberLbl_4;
	// UILabel BallController::_stageNumberLbl
	UILabel_t3248798549 * ____stageNumberLbl_5;
	// System.Collections.Generic.List`1<BallSelectButtonAssistant> BallController::_list
	List_1_t4288528635 * ____list_6;
	// System.Collections.Generic.List`1<StageSelectButtonAssistant> BallController::_stageList
	List_1_t2037122148 * ____stageList_7;
	// TweenPosition BallController::_ballGroup
	TweenPosition_t1378762002 * ____ballGroup_8;
	// TweenPosition BallController::_stageGroup
	TweenPosition_t1378762002 * ____stageGroup_9;
	// TabBallButtonAssistant BallController::_tabBall
	TabBallButtonAssistant_t3092544413 * ____tabBall_10;
	// TabStageButtonAssistant BallController::_tabStage
	TabStageButtonAssistant_t2589366364 * ____tabStage_11;
	// System.Int32 BallController::_currentIdx
	int32_t ____currentIdx_12;
	// UnityEngine.Vector3 BallController::start
	Vector3_t3722313464  ___start_13;
	// UnityEngine.Vector3 BallController::end
	Vector3_t3722313464  ___end_14;
	// System.Boolean BallController::isMoving
	bool ___isMoving_15;

public:
	inline static int32_t get_offset_of__numberLbl_4() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____numberLbl_4)); }
	inline UILabel_t3248798549 * get__numberLbl_4() const { return ____numberLbl_4; }
	inline UILabel_t3248798549 ** get_address_of__numberLbl_4() { return &____numberLbl_4; }
	inline void set__numberLbl_4(UILabel_t3248798549 * value)
	{
		____numberLbl_4 = value;
		Il2CppCodeGenWriteBarrier((&____numberLbl_4), value);
	}

	inline static int32_t get_offset_of__stageNumberLbl_5() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____stageNumberLbl_5)); }
	inline UILabel_t3248798549 * get__stageNumberLbl_5() const { return ____stageNumberLbl_5; }
	inline UILabel_t3248798549 ** get_address_of__stageNumberLbl_5() { return &____stageNumberLbl_5; }
	inline void set__stageNumberLbl_5(UILabel_t3248798549 * value)
	{
		____stageNumberLbl_5 = value;
		Il2CppCodeGenWriteBarrier((&____stageNumberLbl_5), value);
	}

	inline static int32_t get_offset_of__list_6() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____list_6)); }
	inline List_1_t4288528635 * get__list_6() const { return ____list_6; }
	inline List_1_t4288528635 ** get_address_of__list_6() { return &____list_6; }
	inline void set__list_6(List_1_t4288528635 * value)
	{
		____list_6 = value;
		Il2CppCodeGenWriteBarrier((&____list_6), value);
	}

	inline static int32_t get_offset_of__stageList_7() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____stageList_7)); }
	inline List_1_t2037122148 * get__stageList_7() const { return ____stageList_7; }
	inline List_1_t2037122148 ** get_address_of__stageList_7() { return &____stageList_7; }
	inline void set__stageList_7(List_1_t2037122148 * value)
	{
		____stageList_7 = value;
		Il2CppCodeGenWriteBarrier((&____stageList_7), value);
	}

	inline static int32_t get_offset_of__ballGroup_8() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____ballGroup_8)); }
	inline TweenPosition_t1378762002 * get__ballGroup_8() const { return ____ballGroup_8; }
	inline TweenPosition_t1378762002 ** get_address_of__ballGroup_8() { return &____ballGroup_8; }
	inline void set__ballGroup_8(TweenPosition_t1378762002 * value)
	{
		____ballGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&____ballGroup_8), value);
	}

	inline static int32_t get_offset_of__stageGroup_9() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____stageGroup_9)); }
	inline TweenPosition_t1378762002 * get__stageGroup_9() const { return ____stageGroup_9; }
	inline TweenPosition_t1378762002 ** get_address_of__stageGroup_9() { return &____stageGroup_9; }
	inline void set__stageGroup_9(TweenPosition_t1378762002 * value)
	{
		____stageGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&____stageGroup_9), value);
	}

	inline static int32_t get_offset_of__tabBall_10() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____tabBall_10)); }
	inline TabBallButtonAssistant_t3092544413 * get__tabBall_10() const { return ____tabBall_10; }
	inline TabBallButtonAssistant_t3092544413 ** get_address_of__tabBall_10() { return &____tabBall_10; }
	inline void set__tabBall_10(TabBallButtonAssistant_t3092544413 * value)
	{
		____tabBall_10 = value;
		Il2CppCodeGenWriteBarrier((&____tabBall_10), value);
	}

	inline static int32_t get_offset_of__tabStage_11() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____tabStage_11)); }
	inline TabStageButtonAssistant_t2589366364 * get__tabStage_11() const { return ____tabStage_11; }
	inline TabStageButtonAssistant_t2589366364 ** get_address_of__tabStage_11() { return &____tabStage_11; }
	inline void set__tabStage_11(TabStageButtonAssistant_t2589366364 * value)
	{
		____tabStage_11 = value;
		Il2CppCodeGenWriteBarrier((&____tabStage_11), value);
	}

	inline static int32_t get_offset_of__currentIdx_12() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ____currentIdx_12)); }
	inline int32_t get__currentIdx_12() const { return ____currentIdx_12; }
	inline int32_t* get_address_of__currentIdx_12() { return &____currentIdx_12; }
	inline void set__currentIdx_12(int32_t value)
	{
		____currentIdx_12 = value;
	}

	inline static int32_t get_offset_of_start_13() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___start_13)); }
	inline Vector3_t3722313464  get_start_13() const { return ___start_13; }
	inline Vector3_t3722313464 * get_address_of_start_13() { return &___start_13; }
	inline void set_start_13(Vector3_t3722313464  value)
	{
		___start_13 = value;
	}

	inline static int32_t get_offset_of_end_14() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___end_14)); }
	inline Vector3_t3722313464  get_end_14() const { return ___end_14; }
	inline Vector3_t3722313464 * get_address_of_end_14() { return &___end_14; }
	inline void set_end_14(Vector3_t3722313464  value)
	{
		___end_14 = value;
	}

	inline static int32_t get_offset_of_isMoving_15() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___isMoving_15)); }
	inline bool get_isMoving_15() const { return ___isMoving_15; }
	inline bool* get_address_of_isMoving_15() { return &___isMoving_15; }
	inline void set_isMoving_15(bool value)
	{
		___isMoving_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLCONTROLLER_T2992829471_H
#ifndef UIBUTTON_T1100396938_H
#define UIBUTTON_T1100396938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButton
struct  UIButton_t1100396938  : public UIButtonColor_t2038074180
{
public:
	// System.Boolean UIButton::dragHighlight
	bool ___dragHighlight_13;
	// System.String UIButton::hoverSprite
	String_t* ___hoverSprite_14;
	// System.String UIButton::pressedSprite
	String_t* ___pressedSprite_15;
	// System.String UIButton::disabledSprite
	String_t* ___disabledSprite_16;
	// UnityEngine.Sprite UIButton::hoverSprite2D
	Sprite_t280657092 * ___hoverSprite2D_17;
	// UnityEngine.Sprite UIButton::pressedSprite2D
	Sprite_t280657092 * ___pressedSprite2D_18;
	// UnityEngine.Sprite UIButton::disabledSprite2D
	Sprite_t280657092 * ___disabledSprite2D_19;
	// System.Boolean UIButton::pixelSnap
	bool ___pixelSnap_20;
	// System.Collections.Generic.List`1<EventDelegate> UIButton::onClick
	List_1_t4210400802 * ___onClick_21;
	// UISprite UIButton::mSprite
	UISprite_t194114938 * ___mSprite_22;
	// UI2DSprite UIButton::mSprite2D
	UI2DSprite_t1366157572 * ___mSprite2D_23;
	// System.String UIButton::mNormalSprite
	String_t* ___mNormalSprite_24;
	// UnityEngine.Sprite UIButton::mNormalSprite2D
	Sprite_t280657092 * ___mNormalSprite2D_25;

public:
	inline static int32_t get_offset_of_dragHighlight_13() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___dragHighlight_13)); }
	inline bool get_dragHighlight_13() const { return ___dragHighlight_13; }
	inline bool* get_address_of_dragHighlight_13() { return &___dragHighlight_13; }
	inline void set_dragHighlight_13(bool value)
	{
		___dragHighlight_13 = value;
	}

	inline static int32_t get_offset_of_hoverSprite_14() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___hoverSprite_14)); }
	inline String_t* get_hoverSprite_14() const { return ___hoverSprite_14; }
	inline String_t** get_address_of_hoverSprite_14() { return &___hoverSprite_14; }
	inline void set_hoverSprite_14(String_t* value)
	{
		___hoverSprite_14 = value;
		Il2CppCodeGenWriteBarrier((&___hoverSprite_14), value);
	}

	inline static int32_t get_offset_of_pressedSprite_15() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___pressedSprite_15)); }
	inline String_t* get_pressedSprite_15() const { return ___pressedSprite_15; }
	inline String_t** get_address_of_pressedSprite_15() { return &___pressedSprite_15; }
	inline void set_pressedSprite_15(String_t* value)
	{
		___pressedSprite_15 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite_15), value);
	}

	inline static int32_t get_offset_of_disabledSprite_16() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___disabledSprite_16)); }
	inline String_t* get_disabledSprite_16() const { return ___disabledSprite_16; }
	inline String_t** get_address_of_disabledSprite_16() { return &___disabledSprite_16; }
	inline void set_disabledSprite_16(String_t* value)
	{
		___disabledSprite_16 = value;
		Il2CppCodeGenWriteBarrier((&___disabledSprite_16), value);
	}

	inline static int32_t get_offset_of_hoverSprite2D_17() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___hoverSprite2D_17)); }
	inline Sprite_t280657092 * get_hoverSprite2D_17() const { return ___hoverSprite2D_17; }
	inline Sprite_t280657092 ** get_address_of_hoverSprite2D_17() { return &___hoverSprite2D_17; }
	inline void set_hoverSprite2D_17(Sprite_t280657092 * value)
	{
		___hoverSprite2D_17 = value;
		Il2CppCodeGenWriteBarrier((&___hoverSprite2D_17), value);
	}

	inline static int32_t get_offset_of_pressedSprite2D_18() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___pressedSprite2D_18)); }
	inline Sprite_t280657092 * get_pressedSprite2D_18() const { return ___pressedSprite2D_18; }
	inline Sprite_t280657092 ** get_address_of_pressedSprite2D_18() { return &___pressedSprite2D_18; }
	inline void set_pressedSprite2D_18(Sprite_t280657092 * value)
	{
		___pressedSprite2D_18 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite2D_18), value);
	}

	inline static int32_t get_offset_of_disabledSprite2D_19() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___disabledSprite2D_19)); }
	inline Sprite_t280657092 * get_disabledSprite2D_19() const { return ___disabledSprite2D_19; }
	inline Sprite_t280657092 ** get_address_of_disabledSprite2D_19() { return &___disabledSprite2D_19; }
	inline void set_disabledSprite2D_19(Sprite_t280657092 * value)
	{
		___disabledSprite2D_19 = value;
		Il2CppCodeGenWriteBarrier((&___disabledSprite2D_19), value);
	}

	inline static int32_t get_offset_of_pixelSnap_20() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___pixelSnap_20)); }
	inline bool get_pixelSnap_20() const { return ___pixelSnap_20; }
	inline bool* get_address_of_pixelSnap_20() { return &___pixelSnap_20; }
	inline void set_pixelSnap_20(bool value)
	{
		___pixelSnap_20 = value;
	}

	inline static int32_t get_offset_of_onClick_21() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___onClick_21)); }
	inline List_1_t4210400802 * get_onClick_21() const { return ___onClick_21; }
	inline List_1_t4210400802 ** get_address_of_onClick_21() { return &___onClick_21; }
	inline void set_onClick_21(List_1_t4210400802 * value)
	{
		___onClick_21 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_21), value);
	}

	inline static int32_t get_offset_of_mSprite_22() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___mSprite_22)); }
	inline UISprite_t194114938 * get_mSprite_22() const { return ___mSprite_22; }
	inline UISprite_t194114938 ** get_address_of_mSprite_22() { return &___mSprite_22; }
	inline void set_mSprite_22(UISprite_t194114938 * value)
	{
		___mSprite_22 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_22), value);
	}

	inline static int32_t get_offset_of_mSprite2D_23() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___mSprite2D_23)); }
	inline UI2DSprite_t1366157572 * get_mSprite2D_23() const { return ___mSprite2D_23; }
	inline UI2DSprite_t1366157572 ** get_address_of_mSprite2D_23() { return &___mSprite2D_23; }
	inline void set_mSprite2D_23(UI2DSprite_t1366157572 * value)
	{
		___mSprite2D_23 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite2D_23), value);
	}

	inline static int32_t get_offset_of_mNormalSprite_24() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___mNormalSprite_24)); }
	inline String_t* get_mNormalSprite_24() const { return ___mNormalSprite_24; }
	inline String_t** get_address_of_mNormalSprite_24() { return &___mNormalSprite_24; }
	inline void set_mNormalSprite_24(String_t* value)
	{
		___mNormalSprite_24 = value;
		Il2CppCodeGenWriteBarrier((&___mNormalSprite_24), value);
	}

	inline static int32_t get_offset_of_mNormalSprite2D_25() { return static_cast<int32_t>(offsetof(UIButton_t1100396938, ___mNormalSprite2D_25)); }
	inline Sprite_t280657092 * get_mNormalSprite2D_25() const { return ___mNormalSprite2D_25; }
	inline Sprite_t280657092 ** get_address_of_mNormalSprite2D_25() { return &___mNormalSprite2D_25; }
	inline void set_mNormalSprite2D_25(Sprite_t280657092 * value)
	{
		___mNormalSprite2D_25 = value;
		Il2CppCodeGenWriteBarrier((&___mNormalSprite2D_25), value);
	}
};

struct UIButton_t1100396938_StaticFields
{
public:
	// UIButton UIButton::current
	UIButton_t1100396938 * ___current_12;

public:
	inline static int32_t get_offset_of_current_12() { return static_cast<int32_t>(offsetof(UIButton_t1100396938_StaticFields, ___current_12)); }
	inline UIButton_t1100396938 * get_current_12() const { return ___current_12; }
	inline UIButton_t1100396938 ** get_address_of_current_12() { return &___current_12; }
	inline void set_current_12(UIButton_t1100396938 * value)
	{
		___current_12 = value;
		Il2CppCodeGenWriteBarrier((&___current_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTON_T1100396938_H
#ifndef UISLIDER_T886033014_H
#define UISLIDER_T886033014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISlider
struct  UISlider_t886033014  : public UIProgressBar_t1222110469
{
public:
	// UnityEngine.Transform UISlider::foreground
	Transform_t3600365921 * ___foreground_16;
	// System.Single UISlider::rawValue
	float ___rawValue_17;
	// UISlider/Direction UISlider::direction
	int32_t ___direction_18;
	// System.Boolean UISlider::mInverted
	bool ___mInverted_19;

public:
	inline static int32_t get_offset_of_foreground_16() { return static_cast<int32_t>(offsetof(UISlider_t886033014, ___foreground_16)); }
	inline Transform_t3600365921 * get_foreground_16() const { return ___foreground_16; }
	inline Transform_t3600365921 ** get_address_of_foreground_16() { return &___foreground_16; }
	inline void set_foreground_16(Transform_t3600365921 * value)
	{
		___foreground_16 = value;
		Il2CppCodeGenWriteBarrier((&___foreground_16), value);
	}

	inline static int32_t get_offset_of_rawValue_17() { return static_cast<int32_t>(offsetof(UISlider_t886033014, ___rawValue_17)); }
	inline float get_rawValue_17() const { return ___rawValue_17; }
	inline float* get_address_of_rawValue_17() { return &___rawValue_17; }
	inline void set_rawValue_17(float value)
	{
		___rawValue_17 = value;
	}

	inline static int32_t get_offset_of_direction_18() { return static_cast<int32_t>(offsetof(UISlider_t886033014, ___direction_18)); }
	inline int32_t get_direction_18() const { return ___direction_18; }
	inline int32_t* get_address_of_direction_18() { return &___direction_18; }
	inline void set_direction_18(int32_t value)
	{
		___direction_18 = value;
	}

	inline static int32_t get_offset_of_mInverted_19() { return static_cast<int32_t>(offsetof(UISlider_t886033014, ___mInverted_19)); }
	inline bool get_mInverted_19() const { return ___mInverted_19; }
	inline bool* get_address_of_mInverted_19() { return &___mInverted_19; }
	inline void set_mInverted_19(bool value)
	{
		___mInverted_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDER_T886033014_H
#ifndef IAPCONTROLLER_T1855063710_H
#define IAPCONTROLLER_T1855063710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPController
struct  IAPController_t1855063710  : public SingletonMonoBehaviour_1_t4124865168
{
public:

public:
};

struct IAPController_t1855063710_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController IAPController::m_StoreController
	RuntimeObject* ___m_StoreController_4;
	// UnityEngine.Purchasing.IExtensionProvider IAPController::m_StoreExtensionProvider
	RuntimeObject* ___m_StoreExtensionProvider_5;
	// System.String IAPController::kProductIDNoAds
	String_t* ___kProductIDNoAds_6;
	// System.Action`1<System.Boolean> IAPController::<>f__am$cache0
	Action_1_t269755560 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_m_StoreController_4() { return static_cast<int32_t>(offsetof(IAPController_t1855063710_StaticFields, ___m_StoreController_4)); }
	inline RuntimeObject* get_m_StoreController_4() const { return ___m_StoreController_4; }
	inline RuntimeObject** get_address_of_m_StoreController_4() { return &___m_StoreController_4; }
	inline void set_m_StoreController_4(RuntimeObject* value)
	{
		___m_StoreController_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreController_4), value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_5() { return static_cast<int32_t>(offsetof(IAPController_t1855063710_StaticFields, ___m_StoreExtensionProvider_5)); }
	inline RuntimeObject* get_m_StoreExtensionProvider_5() const { return ___m_StoreExtensionProvider_5; }
	inline RuntimeObject** get_address_of_m_StoreExtensionProvider_5() { return &___m_StoreExtensionProvider_5; }
	inline void set_m_StoreExtensionProvider_5(RuntimeObject* value)
	{
		___m_StoreExtensionProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreExtensionProvider_5), value);
	}

	inline static int32_t get_offset_of_kProductIDNoAds_6() { return static_cast<int32_t>(offsetof(IAPController_t1855063710_StaticFields, ___kProductIDNoAds_6)); }
	inline String_t* get_kProductIDNoAds_6() const { return ___kProductIDNoAds_6; }
	inline String_t** get_address_of_kProductIDNoAds_6() { return &___kProductIDNoAds_6; }
	inline void set_kProductIDNoAds_6(String_t* value)
	{
		___kProductIDNoAds_6 = value;
		Il2CppCodeGenWriteBarrier((&___kProductIDNoAds_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(IAPController_t1855063710_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPCONTROLLER_T1855063710_H
#ifndef UISCROLLBAR_T1285772729_H
#define UISCROLLBAR_T1285772729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScrollBar
struct  UIScrollBar_t1285772729  : public UISlider_t886033014
{
public:
	// System.Single UIScrollBar::mSize
	float ___mSize_20;
	// System.Single UIScrollBar::mScroll
	float ___mScroll_21;
	// UIScrollBar/Direction UIScrollBar::mDir
	int32_t ___mDir_22;

public:
	inline static int32_t get_offset_of_mSize_20() { return static_cast<int32_t>(offsetof(UIScrollBar_t1285772729, ___mSize_20)); }
	inline float get_mSize_20() const { return ___mSize_20; }
	inline float* get_address_of_mSize_20() { return &___mSize_20; }
	inline void set_mSize_20(float value)
	{
		___mSize_20 = value;
	}

	inline static int32_t get_offset_of_mScroll_21() { return static_cast<int32_t>(offsetof(UIScrollBar_t1285772729, ___mScroll_21)); }
	inline float get_mScroll_21() const { return ___mScroll_21; }
	inline float* get_address_of_mScroll_21() { return &___mScroll_21; }
	inline void set_mScroll_21(float value)
	{
		___mScroll_21 = value;
	}

	inline static int32_t get_offset_of_mDir_22() { return static_cast<int32_t>(offsetof(UIScrollBar_t1285772729, ___mDir_22)); }
	inline int32_t get_mDir_22() const { return ___mDir_22; }
	inline int32_t* get_address_of_mDir_22() { return &___mDir_22; }
	inline void set_mDir_22(int32_t value)
	{
		___mDir_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBAR_T1285772729_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2403[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2404[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2405[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2406[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (U3CModuleU3E_t692745563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (BallBackButtonAssisstant_t2493153731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (BallButtonAssisstant_t1104794610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[1] = 
{
	BallButtonAssisstant_t1104794610::get_offset_of__icon_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (BallController_t2992829471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[12] = 
{
	BallController_t2992829471::get_offset_of__numberLbl_4(),
	BallController_t2992829471::get_offset_of__stageNumberLbl_5(),
	BallController_t2992829471::get_offset_of__list_6(),
	BallController_t2992829471::get_offset_of__stageList_7(),
	BallController_t2992829471::get_offset_of__ballGroup_8(),
	BallController_t2992829471::get_offset_of__stageGroup_9(),
	BallController_t2992829471::get_offset_of__tabBall_10(),
	BallController_t2992829471::get_offset_of__tabStage_11(),
	BallController_t2992829471::get_offset_of__currentIdx_12(),
	BallController_t2992829471::get_offset_of_start_13(),
	BallController_t2992829471::get_offset_of_end_14(),
	BallController_t2992829471::get_offset_of_isMoving_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (BallSelectButtonAssistant_t2816453893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[7] = 
{
	BallSelectButtonAssistant_t2816453893::get_offset_of__check_2(),
	BallSelectButtonAssistant_t2816453893::get_offset_of__icon_3(),
	BallSelectButtonAssistant_t2816453893::get_offset_of__sprite_4(),
	BallSelectButtonAssistant_t2816453893::get_offset_of__button_5(),
	BallSelectButtonAssistant_t2816453893::get_offset_of_idx_6(),
	BallSelectButtonAssistant_t2816453893::get_offset_of_name_7(),
	BallSelectButtonAssistant_t2816453893::get_offset_of_nameLock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (CloseHouseAdsButtonAssistant_t3993876536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[2] = 
{
	CloseHouseAdsButtonAssistant_t3993876536::get_offset_of_dialog_5(),
	CloseHouseAdsButtonAssistant_t3993876536::get_offset_of_showAds_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (CloseRectButtonAssisstant_t998390754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[1] = 
{
	CloseRectButtonAssisstant_t998390754::get_offset_of__dialogRect_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (ContinueGameButtonAssistant_t4254328874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[2] = 
{
	ContinueGameButtonAssistant_t4254328874::get_offset_of_showAds_5(),
	ContinueGameButtonAssistant_t4254328874::get_offset_of_dialog_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (CounterLabel_t1397518286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[3] = 
{
	CounterLabel_t1397518286::get_offset_of__counterImage_2(),
	CounterLabel_t1397518286::get_offset_of__spriteName_3(),
	CounterLabel_t1397518286::get_offset_of__countDown_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (U3CCountDownU3Ec__Iterator0_t63069431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[4] = 
{
	U3CCountDownU3Ec__Iterator0_t63069431::get_offset_of_U24this_0(),
	U3CCountDownU3Ec__Iterator0_t63069431::get_offset_of_U24current_1(),
	U3CCountDownU3Ec__Iterator0_t63069431::get_offset_of_U24disposing_2(),
	U3CCountDownU3Ec__Iterator0_t63069431::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (HouseAdsController_t2212916833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[1] = 
{
	HouseAdsController_t2212916833::get_offset_of_dialog_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (IAPController_t1855063710), -1, sizeof(IAPController_t1855063710_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2424[4] = 
{
	IAPController_t1855063710_StaticFields::get_offset_of_m_StoreController_4(),
	IAPController_t1855063710_StaticFields::get_offset_of_m_StoreExtensionProvider_5(),
	IAPController_t1855063710_StaticFields::get_offset_of_kProductIDNoAds_6(),
	IAPController_t1855063710_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (EnvelopContent_t3305418577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[7] = 
{
	EnvelopContent_t3305418577::get_offset_of_targetRoot_2(),
	EnvelopContent_t3305418577::get_offset_of_padLeft_3(),
	EnvelopContent_t3305418577::get_offset_of_padRight_4(),
	EnvelopContent_t3305418577::get_offset_of_padBottom_5(),
	EnvelopContent_t3305418577::get_offset_of_padTop_6(),
	EnvelopContent_t3305418577::get_offset_of_ignoreDisabled_7(),
	EnvelopContent_t3305418577::get_offset_of_mStarted_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (LanguageSelection_t1927862481), -1, sizeof(LanguageSelection_t1927862481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2426[3] = 
{
	LanguageSelection_t1927862481::get_offset_of_mList_2(),
	LanguageSelection_t1927862481::get_offset_of_mStarted_3(),
	LanguageSelection_t1927862481_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (TypewriterEffect_t1340895546), -1, sizeof(TypewriterEffect_t1340895546_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2427[15] = 
{
	TypewriterEffect_t1340895546_StaticFields::get_offset_of_current_2(),
	TypewriterEffect_t1340895546::get_offset_of_charsPerSecond_3(),
	TypewriterEffect_t1340895546::get_offset_of_fadeInTime_4(),
	TypewriterEffect_t1340895546::get_offset_of_delayOnPeriod_5(),
	TypewriterEffect_t1340895546::get_offset_of_delayOnNewLine_6(),
	TypewriterEffect_t1340895546::get_offset_of_scrollView_7(),
	TypewriterEffect_t1340895546::get_offset_of_keepFullDimensions_8(),
	TypewriterEffect_t1340895546::get_offset_of_onFinished_9(),
	TypewriterEffect_t1340895546::get_offset_of_mLabel_10(),
	TypewriterEffect_t1340895546::get_offset_of_mFullText_11(),
	TypewriterEffect_t1340895546::get_offset_of_mCurrentOffset_12(),
	TypewriterEffect_t1340895546::get_offset_of_mNextChar_13(),
	TypewriterEffect_t1340895546::get_offset_of_mReset_14(),
	TypewriterEffect_t1340895546::get_offset_of_mActive_15(),
	TypewriterEffect_t1340895546::get_offset_of_mFade_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (FadeEntry_t639421133)+ sizeof (RuntimeObject), sizeof(FadeEntry_t639421133_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2428[3] = 
{
	FadeEntry_t639421133::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FadeEntry_t639421133::get_offset_of_text_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FadeEntry_t639421133::get_offset_of_alpha_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (UIButton_t1100396938), -1, sizeof(UIButton_t1100396938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2429[14] = 
{
	UIButton_t1100396938_StaticFields::get_offset_of_current_12(),
	UIButton_t1100396938::get_offset_of_dragHighlight_13(),
	UIButton_t1100396938::get_offset_of_hoverSprite_14(),
	UIButton_t1100396938::get_offset_of_pressedSprite_15(),
	UIButton_t1100396938::get_offset_of_disabledSprite_16(),
	UIButton_t1100396938::get_offset_of_hoverSprite2D_17(),
	UIButton_t1100396938::get_offset_of_pressedSprite2D_18(),
	UIButton_t1100396938::get_offset_of_disabledSprite2D_19(),
	UIButton_t1100396938::get_offset_of_pixelSnap_20(),
	UIButton_t1100396938::get_offset_of_onClick_21(),
	UIButton_t1100396938::get_offset_of_mSprite_22(),
	UIButton_t1100396938::get_offset_of_mSprite2D_23(),
	UIButton_t1100396938::get_offset_of_mNormalSprite_24(),
	UIButton_t1100396938::get_offset_of_mNormalSprite2D_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (UIButtonActivate_t1184781629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[2] = 
{
	UIButtonActivate_t1184781629::get_offset_of_target_2(),
	UIButtonActivate_t1184781629::get_offset_of_state_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (UIButtonColor_t2038074180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[10] = 
{
	UIButtonColor_t2038074180::get_offset_of_tweenTarget_2(),
	UIButtonColor_t2038074180::get_offset_of_hover_3(),
	UIButtonColor_t2038074180::get_offset_of_pressed_4(),
	UIButtonColor_t2038074180::get_offset_of_disabledColor_5(),
	UIButtonColor_t2038074180::get_offset_of_duration_6(),
	UIButtonColor_t2038074180::get_offset_of_mStartingColor_7(),
	UIButtonColor_t2038074180::get_offset_of_mDefaultColor_8(),
	UIButtonColor_t2038074180::get_offset_of_mInitDone_9(),
	UIButtonColor_t2038074180::get_offset_of_mWidget_10(),
	UIButtonColor_t2038074180::get_offset_of_mState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (State_t3991372483)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2432[5] = 
{
	State_t3991372483::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (UIButtonKeys_t486517330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[5] = 
{
	UIButtonKeys_t486517330::get_offset_of_selectOnClick_13(),
	UIButtonKeys_t486517330::get_offset_of_selectOnUp_14(),
	UIButtonKeys_t486517330::get_offset_of_selectOnDown_15(),
	UIButtonKeys_t486517330::get_offset_of_selectOnLeft_16(),
	UIButtonKeys_t486517330::get_offset_of_selectOnRight_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (UIButtonMessage_t952534536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[5] = 
{
	UIButtonMessage_t952534536::get_offset_of_target_2(),
	UIButtonMessage_t952534536::get_offset_of_functionName_3(),
	UIButtonMessage_t952534536::get_offset_of_trigger_4(),
	UIButtonMessage_t952534536::get_offset_of_includeChildren_5(),
	UIButtonMessage_t952534536::get_offset_of_mStarted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (Trigger_t148524997)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2435[7] = 
{
	Trigger_t148524997::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (UIButtonOffset_t4041087173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[7] = 
{
	UIButtonOffset_t4041087173::get_offset_of_tweenTarget_2(),
	UIButtonOffset_t4041087173::get_offset_of_hover_3(),
	UIButtonOffset_t4041087173::get_offset_of_pressed_4(),
	UIButtonOffset_t4041087173::get_offset_of_duration_5(),
	UIButtonOffset_t4041087173::get_offset_of_mPos_6(),
	UIButtonOffset_t4041087173::get_offset_of_mStarted_7(),
	UIButtonOffset_t4041087173::get_offset_of_mPressed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (UIButtonRotation_t284788413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[6] = 
{
	UIButtonRotation_t284788413::get_offset_of_tweenTarget_2(),
	UIButtonRotation_t284788413::get_offset_of_hover_3(),
	UIButtonRotation_t284788413::get_offset_of_pressed_4(),
	UIButtonRotation_t284788413::get_offset_of_duration_5(),
	UIButtonRotation_t284788413::get_offset_of_mRot_6(),
	UIButtonRotation_t284788413::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (UIButtonScale_t3973287916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[6] = 
{
	UIButtonScale_t3973287916::get_offset_of_tweenTarget_2(),
	UIButtonScale_t3973287916::get_offset_of_hover_3(),
	UIButtonScale_t3973287916::get_offset_of_pressed_4(),
	UIButtonScale_t3973287916::get_offset_of_duration_5(),
	UIButtonScale_t3973287916::get_offset_of_mScale_6(),
	UIButtonScale_t3973287916::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (UICenterOnChild_t253063637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[6] = 
{
	UICenterOnChild_t253063637::get_offset_of_springStrength_2(),
	UICenterOnChild_t253063637::get_offset_of_nextPageThreshold_3(),
	UICenterOnChild_t253063637::get_offset_of_onFinished_4(),
	UICenterOnChild_t253063637::get_offset_of_onCenter_5(),
	UICenterOnChild_t253063637::get_offset_of_mScrollView_6(),
	UICenterOnChild_t253063637::get_offset_of_mCenteredObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (OnCenterCallback_t2077531662), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (UICenterOnClick_t4058804012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (UIDragCamera_t3716731078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[1] = 
{
	UIDragCamera_t3716731078::get_offset_of_draggableCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (UIDragDropContainer_t2199185397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[1] = 
{
	UIDragDropContainer_t2199185397::get_offset_of_reparentTarget_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (UIDragDropItem_t3922849381), -1, sizeof(UIDragDropItem_t3922849381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[18] = 
{
	UIDragDropItem_t3922849381::get_offset_of_restriction_2(),
	UIDragDropItem_t3922849381::get_offset_of_cloneOnDrag_3(),
	UIDragDropItem_t3922849381::get_offset_of_pressAndHoldDelay_4(),
	UIDragDropItem_t3922849381::get_offset_of_interactable_5(),
	UIDragDropItem_t3922849381::get_offset_of_mTrans_6(),
	UIDragDropItem_t3922849381::get_offset_of_mParent_7(),
	UIDragDropItem_t3922849381::get_offset_of_mCollider_8(),
	UIDragDropItem_t3922849381::get_offset_of_mCollider2D_9(),
	UIDragDropItem_t3922849381::get_offset_of_mButton_10(),
	UIDragDropItem_t3922849381::get_offset_of_mRoot_11(),
	UIDragDropItem_t3922849381::get_offset_of_mGrid_12(),
	UIDragDropItem_t3922849381::get_offset_of_mTable_13(),
	UIDragDropItem_t3922849381::get_offset_of_mDragStartTime_14(),
	UIDragDropItem_t3922849381::get_offset_of_mDragScrollView_15(),
	UIDragDropItem_t3922849381::get_offset_of_mPressed_16(),
	UIDragDropItem_t3922849381::get_offset_of_mDragging_17(),
	UIDragDropItem_t3922849381::get_offset_of_mTouch_18(),
	UIDragDropItem_t3922849381_StaticFields::get_offset_of_draggedItems_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (Restriction_t4221874387)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2445[5] = 
{
	Restriction_t4221874387::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (UIDragDropRoot_t3716445314), -1, sizeof(UIDragDropRoot_t3716445314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2446[1] = 
{
	UIDragDropRoot_t3716445314_StaticFields::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (UIDraggableCamera_t1644204495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[14] = 
{
	UIDraggableCamera_t1644204495::get_offset_of_rootForBounds_2(),
	UIDraggableCamera_t1644204495::get_offset_of_scale_3(),
	UIDraggableCamera_t1644204495::get_offset_of_scrollWheelFactor_4(),
	UIDraggableCamera_t1644204495::get_offset_of_dragEffect_5(),
	UIDraggableCamera_t1644204495::get_offset_of_smoothDragStart_6(),
	UIDraggableCamera_t1644204495::get_offset_of_momentumAmount_7(),
	UIDraggableCamera_t1644204495::get_offset_of_mCam_8(),
	UIDraggableCamera_t1644204495::get_offset_of_mTrans_9(),
	UIDraggableCamera_t1644204495::get_offset_of_mPressed_10(),
	UIDraggableCamera_t1644204495::get_offset_of_mMomentum_11(),
	UIDraggableCamera_t1644204495::get_offset_of_mBounds_12(),
	UIDraggableCamera_t1644204495::get_offset_of_mScroll_13(),
	UIDraggableCamera_t1644204495::get_offset_of_mRoot_14(),
	UIDraggableCamera_t1644204495::get_offset_of_mDragStarted_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (UIDragObject_t167167434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[18] = 
{
	UIDragObject_t167167434::get_offset_of_target_2(),
	UIDragObject_t167167434::get_offset_of_panelRegion_3(),
	UIDragObject_t167167434::get_offset_of_scrollMomentum_4(),
	UIDragObject_t167167434::get_offset_of_restrictWithinPanel_5(),
	UIDragObject_t167167434::get_offset_of_contentRect_6(),
	UIDragObject_t167167434::get_offset_of_dragEffect_7(),
	UIDragObject_t167167434::get_offset_of_momentumAmount_8(),
	UIDragObject_t167167434::get_offset_of_scale_9(),
	UIDragObject_t167167434::get_offset_of_scrollWheelFactor_10(),
	UIDragObject_t167167434::get_offset_of_mPlane_11(),
	UIDragObject_t167167434::get_offset_of_mTargetPos_12(),
	UIDragObject_t167167434::get_offset_of_mLastPos_13(),
	UIDragObject_t167167434::get_offset_of_mMomentum_14(),
	UIDragObject_t167167434::get_offset_of_mScroll_15(),
	UIDragObject_t167167434::get_offset_of_mBounds_16(),
	UIDragObject_t167167434::get_offset_of_mTouchID_17(),
	UIDragObject_t167167434::get_offset_of_mStarted_18(),
	UIDragObject_t167167434::get_offset_of_mPressed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (DragEffect_t2686339116)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2449[4] = 
{
	DragEffect_t2686339116::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (UIDragResize_t3151593629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[13] = 
{
	UIDragResize_t3151593629::get_offset_of_target_2(),
	UIDragResize_t3151593629::get_offset_of_pivot_3(),
	UIDragResize_t3151593629::get_offset_of_minWidth_4(),
	UIDragResize_t3151593629::get_offset_of_minHeight_5(),
	UIDragResize_t3151593629::get_offset_of_maxWidth_6(),
	UIDragResize_t3151593629::get_offset_of_maxHeight_7(),
	UIDragResize_t3151593629::get_offset_of_updateAnchors_8(),
	UIDragResize_t3151593629::get_offset_of_mPlane_9(),
	UIDragResize_t3151593629::get_offset_of_mRayPos_10(),
	UIDragResize_t3151593629::get_offset_of_mLocalPos_11(),
	UIDragResize_t3151593629::get_offset_of_mWidth_12(),
	UIDragResize_t3151593629::get_offset_of_mHeight_13(),
	UIDragResize_t3151593629::get_offset_of_mDragging_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (UIDragScrollView_t2492060641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[7] = 
{
	UIDragScrollView_t2492060641::get_offset_of_scrollView_2(),
	UIDragScrollView_t2492060641::get_offset_of_draggablePanel_3(),
	UIDragScrollView_t2492060641::get_offset_of_mTrans_4(),
	UIDragScrollView_t2492060641::get_offset_of_mScroll_5(),
	UIDragScrollView_t2492060641::get_offset_of_mAutoFind_6(),
	UIDragScrollView_t2492060641::get_offset_of_mStarted_7(),
	UIDragScrollView_t2492060641::get_offset_of_mPressed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (UIEventTrigger_t4038454782), -1, sizeof(UIEventTrigger_t4038454782_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2452[14] = 
{
	UIEventTrigger_t4038454782_StaticFields::get_offset_of_current_2(),
	UIEventTrigger_t4038454782::get_offset_of_onHoverOver_3(),
	UIEventTrigger_t4038454782::get_offset_of_onHoverOut_4(),
	UIEventTrigger_t4038454782::get_offset_of_onPress_5(),
	UIEventTrigger_t4038454782::get_offset_of_onRelease_6(),
	UIEventTrigger_t4038454782::get_offset_of_onSelect_7(),
	UIEventTrigger_t4038454782::get_offset_of_onDeselect_8(),
	UIEventTrigger_t4038454782::get_offset_of_onClick_9(),
	UIEventTrigger_t4038454782::get_offset_of_onDoubleClick_10(),
	UIEventTrigger_t4038454782::get_offset_of_onDragStart_11(),
	UIEventTrigger_t4038454782::get_offset_of_onDragEnd_12(),
	UIEventTrigger_t4038454782::get_offset_of_onDragOver_13(),
	UIEventTrigger_t4038454782::get_offset_of_onDragOut_14(),
	UIEventTrigger_t4038454782::get_offset_of_onDrag_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (UIForwardEvents_t2244381111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[10] = 
{
	UIForwardEvents_t2244381111::get_offset_of_target_2(),
	UIForwardEvents_t2244381111::get_offset_of_onHover_3(),
	UIForwardEvents_t2244381111::get_offset_of_onPress_4(),
	UIForwardEvents_t2244381111::get_offset_of_onClick_5(),
	UIForwardEvents_t2244381111::get_offset_of_onDoubleClick_6(),
	UIForwardEvents_t2244381111::get_offset_of_onSelect_7(),
	UIForwardEvents_t2244381111::get_offset_of_onDrag_8(),
	UIForwardEvents_t2244381111::get_offset_of_onDrop_9(),
	UIForwardEvents_t2244381111::get_offset_of_onSubmit_10(),
	UIForwardEvents_t2244381111::get_offset_of_onScroll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (UIGrid_t1536638187), -1, sizeof(UIGrid_t1536638187_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[18] = 
{
	UIGrid_t1536638187::get_offset_of_arrangement_2(),
	UIGrid_t1536638187::get_offset_of_sorting_3(),
	UIGrid_t1536638187::get_offset_of_pivot_4(),
	UIGrid_t1536638187::get_offset_of_maxPerLine_5(),
	UIGrid_t1536638187::get_offset_of_cellWidth_6(),
	UIGrid_t1536638187::get_offset_of_cellHeight_7(),
	UIGrid_t1536638187::get_offset_of_animateSmoothly_8(),
	UIGrid_t1536638187::get_offset_of_hideInactive_9(),
	UIGrid_t1536638187::get_offset_of_keepWithinPanel_10(),
	UIGrid_t1536638187::get_offset_of_onReposition_11(),
	UIGrid_t1536638187::get_offset_of_onCustomSort_12(),
	UIGrid_t1536638187::get_offset_of_sorted_13(),
	UIGrid_t1536638187::get_offset_of_mReposition_14(),
	UIGrid_t1536638187::get_offset_of_mPanel_15(),
	UIGrid_t1536638187::get_offset_of_mInitDone_16(),
	UIGrid_t1536638187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_17(),
	UIGrid_t1536638187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_18(),
	UIGrid_t1536638187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (OnReposition_t1372889220), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (Arrangement_t1850956547)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2456[4] = 
{
	Arrangement_t1850956547::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (Sorting_t533260699)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2457[6] = 
{
	Sorting_t533260699::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (UIImageButton_t3745027676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[6] = 
{
	UIImageButton_t3745027676::get_offset_of_target_2(),
	UIImageButton_t3745027676::get_offset_of_normalSprite_3(),
	UIImageButton_t3745027676::get_offset_of_hoverSprite_4(),
	UIImageButton_t3745027676::get_offset_of_pressedSprite_5(),
	UIImageButton_t3745027676::get_offset_of_disabledSprite_6(),
	UIImageButton_t3745027676::get_offset_of_pixelSnap_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (UIKeyBinding_t2698445843), -1, sizeof(UIKeyBinding_t2698445843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2459[7] = 
{
	UIKeyBinding_t2698445843_StaticFields::get_offset_of_mList_2(),
	UIKeyBinding_t2698445843::get_offset_of_keyCode_3(),
	UIKeyBinding_t2698445843::get_offset_of_modifier_4(),
	UIKeyBinding_t2698445843::get_offset_of_action_5(),
	UIKeyBinding_t2698445843::get_offset_of_mIgnoreUp_6(),
	UIKeyBinding_t2698445843::get_offset_of_mIsInput_7(),
	UIKeyBinding_t2698445843::get_offset_of_mPress_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (Action_t4142137194)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2460[4] = 
{
	Action_t4142137194::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (Modifier_t1697911081)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2461[6] = 
{
	Modifier_t1697911081::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (UIKeyNavigation_t4244956403), -1, sizeof(UIKeyNavigation_t4244956403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[11] = 
{
	UIKeyNavigation_t4244956403_StaticFields::get_offset_of_list_2(),
	UIKeyNavigation_t4244956403::get_offset_of_constraint_3(),
	UIKeyNavigation_t4244956403::get_offset_of_onUp_4(),
	UIKeyNavigation_t4244956403::get_offset_of_onDown_5(),
	UIKeyNavigation_t4244956403::get_offset_of_onLeft_6(),
	UIKeyNavigation_t4244956403::get_offset_of_onRight_7(),
	UIKeyNavigation_t4244956403::get_offset_of_onClick_8(),
	UIKeyNavigation_t4244956403::get_offset_of_onTab_9(),
	UIKeyNavigation_t4244956403::get_offset_of_startsSelected_10(),
	UIKeyNavigation_t4244956403::get_offset_of_mStarted_11(),
	UIKeyNavigation_t4244956403_StaticFields::get_offset_of_mLastFrame_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (Constraint_t2778583555)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2463[5] = 
{
	Constraint_t2778583555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (UIPlayAnimation_t1976799768), -1, sizeof(UIPlayAnimation_t1976799768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2464[16] = 
{
	UIPlayAnimation_t1976799768_StaticFields::get_offset_of_current_2(),
	UIPlayAnimation_t1976799768::get_offset_of_target_3(),
	UIPlayAnimation_t1976799768::get_offset_of_animator_4(),
	UIPlayAnimation_t1976799768::get_offset_of_clipName_5(),
	UIPlayAnimation_t1976799768::get_offset_of_trigger_6(),
	UIPlayAnimation_t1976799768::get_offset_of_playDirection_7(),
	UIPlayAnimation_t1976799768::get_offset_of_resetOnPlay_8(),
	UIPlayAnimation_t1976799768::get_offset_of_clearSelection_9(),
	UIPlayAnimation_t1976799768::get_offset_of_ifDisabledOnPlay_10(),
	UIPlayAnimation_t1976799768::get_offset_of_disableWhenFinished_11(),
	UIPlayAnimation_t1976799768::get_offset_of_onFinished_12(),
	UIPlayAnimation_t1976799768::get_offset_of_eventReceiver_13(),
	UIPlayAnimation_t1976799768::get_offset_of_callWhenFinished_14(),
	UIPlayAnimation_t1976799768::get_offset_of_mStarted_15(),
	UIPlayAnimation_t1976799768::get_offset_of_mActivated_16(),
	UIPlayAnimation_t1976799768::get_offset_of_dragHighlight_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (UIPlaySound_t1382070071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[5] = 
{
	UIPlaySound_t1382070071::get_offset_of_audioClip_2(),
	UIPlaySound_t1382070071::get_offset_of_trigger_3(),
	UIPlaySound_t1382070071::get_offset_of_volume_4(),
	UIPlaySound_t1382070071::get_offset_of_pitch_5(),
	UIPlaySound_t1382070071::get_offset_of_mIsOver_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (Trigger_t2172727401)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2466[9] = 
{
	Trigger_t2172727401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (UIPlayTween_t2213781924), -1, sizeof(UIPlayTween_t2213781924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2467[17] = 
{
	UIPlayTween_t2213781924_StaticFields::get_offset_of_current_2(),
	UIPlayTween_t2213781924::get_offset_of_tweenTarget_3(),
	UIPlayTween_t2213781924::get_offset_of_tweenGroup_4(),
	UIPlayTween_t2213781924::get_offset_of_trigger_5(),
	UIPlayTween_t2213781924::get_offset_of_playDirection_6(),
	UIPlayTween_t2213781924::get_offset_of_resetOnPlay_7(),
	UIPlayTween_t2213781924::get_offset_of_resetIfDisabled_8(),
	UIPlayTween_t2213781924::get_offset_of_ifDisabledOnPlay_9(),
	UIPlayTween_t2213781924::get_offset_of_disableWhenFinished_10(),
	UIPlayTween_t2213781924::get_offset_of_includeChildren_11(),
	UIPlayTween_t2213781924::get_offset_of_onFinished_12(),
	UIPlayTween_t2213781924::get_offset_of_eventReceiver_13(),
	UIPlayTween_t2213781924::get_offset_of_callWhenFinished_14(),
	UIPlayTween_t2213781924::get_offset_of_mTweens_15(),
	UIPlayTween_t2213781924::get_offset_of_mStarted_16(),
	UIPlayTween_t2213781924::get_offset_of_mActive_17(),
	UIPlayTween_t2213781924::get_offset_of_mActivated_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (UIPopupList_t4167399471), -1, sizeof(UIPopupList_t4167399471_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2468[52] = 
{
	UIPopupList_t4167399471_StaticFields::get_offset_of_current_2(),
	UIPopupList_t4167399471_StaticFields::get_offset_of_mChild_3(),
	UIPopupList_t4167399471_StaticFields::get_offset_of_mFadeOutComplete_4(),
	0,
	UIPopupList_t4167399471::get_offset_of_atlas_6(),
	UIPopupList_t4167399471::get_offset_of_bitmapFont_7(),
	UIPopupList_t4167399471::get_offset_of_trueTypeFont_8(),
	UIPopupList_t4167399471::get_offset_of_fontSize_9(),
	UIPopupList_t4167399471::get_offset_of_fontStyle_10(),
	UIPopupList_t4167399471::get_offset_of_backgroundSprite_11(),
	UIPopupList_t4167399471::get_offset_of_highlightSprite_12(),
	UIPopupList_t4167399471::get_offset_of_background2DSprite_13(),
	UIPopupList_t4167399471::get_offset_of_highlight2DSprite_14(),
	UIPopupList_t4167399471::get_offset_of_position_15(),
	UIPopupList_t4167399471::get_offset_of_selection_16(),
	UIPopupList_t4167399471::get_offset_of_alignment_17(),
	UIPopupList_t4167399471::get_offset_of_items_18(),
	UIPopupList_t4167399471::get_offset_of_itemData_19(),
	UIPopupList_t4167399471::get_offset_of_itemCallbacks_20(),
	UIPopupList_t4167399471::get_offset_of_padding_21(),
	UIPopupList_t4167399471::get_offset_of_textColor_22(),
	UIPopupList_t4167399471::get_offset_of_backgroundColor_23(),
	UIPopupList_t4167399471::get_offset_of_highlightColor_24(),
	UIPopupList_t4167399471::get_offset_of_isAnimated_25(),
	UIPopupList_t4167399471::get_offset_of_isLocalized_26(),
	UIPopupList_t4167399471::get_offset_of_textModifier_27(),
	UIPopupList_t4167399471::get_offset_of_separatePanel_28(),
	UIPopupList_t4167399471::get_offset_of_overlap_29(),
	UIPopupList_t4167399471::get_offset_of_openOn_30(),
	UIPopupList_t4167399471::get_offset_of_onChange_31(),
	UIPopupList_t4167399471::get_offset_of_mSelectedItem_32(),
	UIPopupList_t4167399471::get_offset_of_mPanel_33(),
	UIPopupList_t4167399471::get_offset_of_mBackground_34(),
	UIPopupList_t4167399471::get_offset_of_mHighlight_35(),
	UIPopupList_t4167399471::get_offset_of_mHighlightedLabel_36(),
	UIPopupList_t4167399471::get_offset_of_mLabelList_37(),
	UIPopupList_t4167399471::get_offset_of_mBgBorder_38(),
	UIPopupList_t4167399471::get_offset_of_keepValue_39(),
	UIPopupList_t4167399471::get_offset_of_mSelection_40(),
	UIPopupList_t4167399471::get_offset_of_mOpenFrame_41(),
	UIPopupList_t4167399471::get_offset_of_eventReceiver_42(),
	UIPopupList_t4167399471::get_offset_of_functionName_43(),
	UIPopupList_t4167399471::get_offset_of_textScale_44(),
	UIPopupList_t4167399471::get_offset_of_font_45(),
	UIPopupList_t4167399471::get_offset_of_textLabel_46(),
	UIPopupList_t4167399471::get_offset_of_startingPosition_47(),
	UIPopupList_t4167399471::get_offset_of_mLegacyEvent_48(),
	UIPopupList_t4167399471::get_offset_of_mExecuting_49(),
	UIPopupList_t4167399471::get_offset_of_mUseDynamicFont_50(),
	UIPopupList_t4167399471::get_offset_of_mStarted_51(),
	UIPopupList_t4167399471::get_offset_of_mTweening_52(),
	UIPopupList_t4167399471::get_offset_of_source_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (Position_t1583461796)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2469[4] = 
{
	Position_t1583461796::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (Selection_t3039885439)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2470[3] = 
{
	Selection_t3039885439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (OpenOn_t1997085761)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[5] = 
{
	OpenOn_t1997085761::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (LegacyEvent_t2749056879), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[5] = 
{
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U3CtpU3E__1_0(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24this_1(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24current_2(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24disposing_3(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[5] = 
{
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U3CselU3E__1_0(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24this_1(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24current_2(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24disposing_3(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (UIProgressBar_t1222110469), -1, sizeof(UIProgressBar_t1222110469_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2475[14] = 
{
	UIProgressBar_t1222110469_StaticFields::get_offset_of_current_2(),
	UIProgressBar_t1222110469::get_offset_of_onDragFinished_3(),
	UIProgressBar_t1222110469::get_offset_of_thumb_4(),
	UIProgressBar_t1222110469::get_offset_of_mBG_5(),
	UIProgressBar_t1222110469::get_offset_of_mFG_6(),
	UIProgressBar_t1222110469::get_offset_of_mValue_7(),
	UIProgressBar_t1222110469::get_offset_of_mFill_8(),
	UIProgressBar_t1222110469::get_offset_of_mStarted_9(),
	UIProgressBar_t1222110469::get_offset_of_mTrans_10(),
	UIProgressBar_t1222110469::get_offset_of_mIsDirty_11(),
	UIProgressBar_t1222110469::get_offset_of_mCam_12(),
	UIProgressBar_t1222110469::get_offset_of_mOffset_13(),
	UIProgressBar_t1222110469::get_offset_of_numberOfSteps_14(),
	UIProgressBar_t1222110469::get_offset_of_onChange_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (FillDirection_t551265397)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2476[5] = 
{
	FillDirection_t551265397::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (OnDragFinished_t3715779777), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (UISavedOption_t2504198202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[4] = 
{
	UISavedOption_t2504198202::get_offset_of_keyName_2(),
	UISavedOption_t2504198202::get_offset_of_mList_3(),
	UISavedOption_t2504198202::get_offset_of_mCheck_4(),
	UISavedOption_t2504198202::get_offset_of_mSlider_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (UIScrollBar_t1285772729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[3] = 
{
	UIScrollBar_t1285772729::get_offset_of_mSize_20(),
	UIScrollBar_t1285772729::get_offset_of_mScroll_21(),
	UIScrollBar_t1285772729::get_offset_of_mDir_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (Direction_t340832489)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2480[4] = 
{
	Direction_t340832489::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (UIScrollView_t1973404950), -1, sizeof(UIScrollView_t1973404950_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2481[38] = 
{
	UIScrollView_t1973404950_StaticFields::get_offset_of_list_2(),
	UIScrollView_t1973404950::get_offset_of_movement_3(),
	UIScrollView_t1973404950::get_offset_of_dragEffect_4(),
	UIScrollView_t1973404950::get_offset_of_restrictWithinPanel_5(),
	UIScrollView_t1973404950::get_offset_of_constrainOnDrag_6(),
	UIScrollView_t1973404950::get_offset_of_disableDragIfFits_7(),
	UIScrollView_t1973404950::get_offset_of_smoothDragStart_8(),
	UIScrollView_t1973404950::get_offset_of_iOSDragEmulation_9(),
	UIScrollView_t1973404950::get_offset_of_scrollWheelFactor_10(),
	UIScrollView_t1973404950::get_offset_of_momentumAmount_11(),
	UIScrollView_t1973404950::get_offset_of_dampenStrength_12(),
	UIScrollView_t1973404950::get_offset_of_horizontalScrollBar_13(),
	UIScrollView_t1973404950::get_offset_of_verticalScrollBar_14(),
	UIScrollView_t1973404950::get_offset_of_showScrollBars_15(),
	UIScrollView_t1973404950::get_offset_of_customMovement_16(),
	UIScrollView_t1973404950::get_offset_of_contentPivot_17(),
	UIScrollView_t1973404950::get_offset_of_onDragStarted_18(),
	UIScrollView_t1973404950::get_offset_of_onDragFinished_19(),
	UIScrollView_t1973404950::get_offset_of_onMomentumMove_20(),
	UIScrollView_t1973404950::get_offset_of_onStoppedMoving_21(),
	UIScrollView_t1973404950::get_offset_of_scale_22(),
	UIScrollView_t1973404950::get_offset_of_relativePositionOnReset_23(),
	UIScrollView_t1973404950::get_offset_of_mTrans_24(),
	UIScrollView_t1973404950::get_offset_of_mPanel_25(),
	UIScrollView_t1973404950::get_offset_of_mPlane_26(),
	UIScrollView_t1973404950::get_offset_of_mLastPos_27(),
	UIScrollView_t1973404950::get_offset_of_mPressed_28(),
	UIScrollView_t1973404950::get_offset_of_mMomentum_29(),
	UIScrollView_t1973404950::get_offset_of_mScroll_30(),
	UIScrollView_t1973404950::get_offset_of_mBounds_31(),
	UIScrollView_t1973404950::get_offset_of_mCalculatedBounds_32(),
	UIScrollView_t1973404950::get_offset_of_mShouldMove_33(),
	UIScrollView_t1973404950::get_offset_of_mIgnoreCallbacks_34(),
	UIScrollView_t1973404950::get_offset_of_mDragID_35(),
	UIScrollView_t1973404950::get_offset_of_mDragStartOffset_36(),
	UIScrollView_t1973404950::get_offset_of_mDragStarted_37(),
	UIScrollView_t1973404950::get_offset_of_mStarted_38(),
	UIScrollView_t1973404950::get_offset_of_centerOnChild_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (Movement_t247277292)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2482[5] = 
{
	Movement_t247277292::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (DragEffect_t491601944)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2483[4] = 
{
	DragEffect_t491601944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (ShowCondition_t1535012424)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2484[4] = 
{
	ShowCondition_t1535012424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (OnDragNotification_t1437737811), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (UIShowControlScheme_t428458459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[4] = 
{
	UIShowControlScheme_t428458459::get_offset_of_target_2(),
	UIShowControlScheme_t428458459::get_offset_of_mouse_3(),
	UIShowControlScheme_t428458459::get_offset_of_touch_4(),
	UIShowControlScheme_t428458459::get_offset_of_controller_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (UISlider_t886033014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[4] = 
{
	UISlider_t886033014::get_offset_of_foreground_16(),
	UISlider_t886033014::get_offset_of_rawValue_17(),
	UISlider_t886033014::get_offset_of_direction_18(),
	UISlider_t886033014::get_offset_of_mInverted_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (Direction_t3048248572)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2488[4] = 
{
	Direction_t3048248572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (UISoundVolume_t313711476), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (UITable_t3168834800), -1, sizeof(UITable_t3168834800_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2490[17] = 
{
	UITable_t3168834800::get_offset_of_columns_2(),
	UITable_t3168834800::get_offset_of_direction_3(),
	UITable_t3168834800::get_offset_of_sorting_4(),
	UITable_t3168834800::get_offset_of_pivot_5(),
	UITable_t3168834800::get_offset_of_cellAlignment_6(),
	UITable_t3168834800::get_offset_of_hideInactive_7(),
	UITable_t3168834800::get_offset_of_keepWithinPanel_8(),
	UITable_t3168834800::get_offset_of_padding_9(),
	UITable_t3168834800::get_offset_of_onReposition_10(),
	UITable_t3168834800::get_offset_of_onCustomSort_11(),
	UITable_t3168834800::get_offset_of_mPanel_12(),
	UITable_t3168834800::get_offset_of_mInitDone_13(),
	UITable_t3168834800::get_offset_of_mReposition_14(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_16(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_17(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (OnReposition_t3913508630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (Direction_t2487117792)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2492[3] = 
{
	Direction_t2487117792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (Sorting_t2823944879)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2493[6] = 
{
	Sorting_t2823944879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (UIToggle_t4192126258), -1, sizeof(UIToggle_t4192126258_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2494[20] = 
{
	UIToggle_t4192126258_StaticFields::get_offset_of_list_2(),
	UIToggle_t4192126258_StaticFields::get_offset_of_current_3(),
	UIToggle_t4192126258::get_offset_of_group_4(),
	UIToggle_t4192126258::get_offset_of_activeSprite_5(),
	UIToggle_t4192126258::get_offset_of_invertSpriteState_6(),
	UIToggle_t4192126258::get_offset_of_activeAnimation_7(),
	UIToggle_t4192126258::get_offset_of_animator_8(),
	UIToggle_t4192126258::get_offset_of_tween_9(),
	UIToggle_t4192126258::get_offset_of_startsActive_10(),
	UIToggle_t4192126258::get_offset_of_instantTween_11(),
	UIToggle_t4192126258::get_offset_of_optionCanBeNone_12(),
	UIToggle_t4192126258::get_offset_of_onChange_13(),
	UIToggle_t4192126258::get_offset_of_validator_14(),
	UIToggle_t4192126258::get_offset_of_checkSprite_15(),
	UIToggle_t4192126258::get_offset_of_checkAnimation_16(),
	UIToggle_t4192126258::get_offset_of_eventReceiver_17(),
	UIToggle_t4192126258::get_offset_of_functionName_18(),
	UIToggle_t4192126258::get_offset_of_startsChecked_19(),
	UIToggle_t4192126258::get_offset_of_mIsActive_20(),
	UIToggle_t4192126258::get_offset_of_mStarted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (Validate_t3702293971), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (UIToggledComponents_t772955118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[4] = 
{
	UIToggledComponents_t772955118::get_offset_of_activate_2(),
	UIToggledComponents_t772955118::get_offset_of_deactivate_3(),
	UIToggledComponents_t772955118::get_offset_of_target_4(),
	UIToggledComponents_t772955118::get_offset_of_inverse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (UIToggledObjects_t3502557910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[4] = 
{
	UIToggledObjects_t3502557910::get_offset_of_activate_2(),
	UIToggledObjects_t3502557910::get_offset_of_deactivate_3(),
	UIToggledObjects_t3502557910::get_offset_of_target_4(),
	UIToggledObjects_t3502557910::get_offset_of_inverse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (UIWidgetContainer_t30162560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (UIWrapContent_t1188558554), -1, sizeof(UIWrapContent_t1188558554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2499[15] = 
{
	UIWrapContent_t1188558554::get_offset_of_itemSize_2(),
	UIWrapContent_t1188558554::get_offset_of_cullContent_3(),
	UIWrapContent_t1188558554::get_offset_of_minIndex_4(),
	UIWrapContent_t1188558554::get_offset_of_maxIndex_5(),
	UIWrapContent_t1188558554::get_offset_of_hideInactive_6(),
	UIWrapContent_t1188558554::get_offset_of_onInitializeItem_7(),
	UIWrapContent_t1188558554::get_offset_of_mTrans_8(),
	UIWrapContent_t1188558554::get_offset_of_mPanel_9(),
	UIWrapContent_t1188558554::get_offset_of_mScroll_10(),
	UIWrapContent_t1188558554::get_offset_of_mHorizontal_11(),
	UIWrapContent_t1188558554::get_offset_of_mFirstTime_12(),
	UIWrapContent_t1188558554::get_offset_of_mChildren_13(),
	UIWrapContent_t1188558554_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
	UIWrapContent_t1188558554_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_15(),
	UIWrapContent_t1188558554_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
