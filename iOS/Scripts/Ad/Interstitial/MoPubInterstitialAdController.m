//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  Nendのスプラッシュ(インターステイシャル)広告コントローラー

#import "MoPubInterstitialAdController.h"
#import "MovieAdController.h"

@implementation MoPubInterstitialAdController
{
    BOOL canShow;
}
//=================================================================================
//外部
//=================================================================================

//初期化
- (id)init{
    self = [super init];
    [self initInterstitial];
    return self;
}

//表示
-(void)showInterstitialAd:(int)playCount{
//    if (![super canShow:playCount]) {
//        return;
//    }
    
    if (self.interstitial.ready && canShow)
        [self.interstitial showFromViewController:UnityGetGLViewController()];
}

//=================================================================================
//内部
//=================================================================================


#pragma mark - Nend Interstitial
- (void)initInterstitial{
    
    canShow = true;
    self.interstitial = [MPInterstitialAdController
                         interstitialAdControllerForAdUnitId:MOPUB_INTERSTITIAL_ID];
    self.interstitial.delegate = self;
    [self.interstitial loadAd];
}


#pragma -delegate

- (void)interstitialDidLoadAd:(MPInterstitialAdController *)interstitial
{
    NSLog(@"MoPub Interstitial: did load");
}

- (void)interstitialDidFailToLoadAd:(MPInterstitialAdController *)interstitial
{
    NSLog(@"MoPub Interstitial: load failed");
    [self finishInterstitial];
}

- (void)interstitialDidFailToLoadAd:(MPInterstitialAdController *)interstitial
                          withError:(NSError *)error
{
    NSLog(@"MoPub Interstitial: load failed");
}

- (void)interstitialWillAppear:(MPInterstitialAdController *)interstitial
{
    UnitySendMessage(UNITY_OBJECT_NAME, "PauseGame", "");
    [MovieAdController startInterstial];
}

- (void)interstitialDidAppear:(MPInterstitialAdController *)interstitial
{
    
}

- (void)interstitialWillDisappear:(MPInterstitialAdController *)interstitial
{
    
}

- (void)interstitialDidDisappear:(MPInterstitialAdController *)interstitial
{
    [self finishInterstitial];
    
}

- (void)interstitialDidExpire:(MPInterstitialAdController *)interstitial
{
    [self finishInterstitial];
}

- (void)interstitialDidReceiveTapEvent:(MPInterstitialAdController *)interstitial
{
    
}

-(void)finishInterstitial
{
    [self.interstitial loadAd];
    [MovieAdController finishInterstial];
    canShow = false;
    _timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(enableCanShow) userInfo:nil repeats:NO];
}

-(void)enableCanShow
{
    [_timer invalidate];
    _timer = nil;
    canShow = true;
}
@end
