﻿//  ResultUISwitcher.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.26.

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// リザルトのUIを表示、表示切り替えるクラス
/// </summary>
[ExecuteInEditMode]
public class ResultUISwitcher : MonoBehaviour {

	//リザルトの状態
	[Flags]
	private enum ResultType{
		GameOver = 1 << 0, 
		AllClear = 1 << 1, 
		Clear    = 1 << 2, 
	}

	[SerializeField][EnumFlags]
	private ResultType _targetResultType;

	[SerializeField][EnumFlags]
	private GameType _targetGameType;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start(){
		UpdateState ();
	}

	//状態を更新する
	private void UpdateState(){
		//状態を判定
		ResultType resultType = ResultType.GameOver;

		if(CacheData.Entity.IsClear){
			if(CacheData.Entity.StageNo >= UnityProjectSetting.Entity.StageNum || UnityProjectSetting.Entity.GameType == GameType.Mini){
				resultType = ResultType.AllClear;
			}
			else{
				resultType = ResultType.Clear;
			}
		}

		//表示するか
		bool isEnabled = _targetResultType.HasFlag (resultType) && _targetGameType.HasFlag(UnityProjectSetting.Entity.GameType);

		//全コンポーネントを表示切り替え
		foreach (MonoBehaviour mono in GetComponentsInChildren<MonoBehaviour>()) {
			if(mono != this){
				mono.enabled = isEnabled;
			}
		}
	}

	//=================================================================================
	//更新
	//=================================================================================

	#if UNITY_EDITOR
	private void Update () {
		if(Application.isPlaying){
			return;
		}
		UpdateState ();
	}
	#endif

}