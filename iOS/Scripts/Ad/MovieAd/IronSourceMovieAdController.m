//
//  AdstirMovieAdController.m
//  Unity-iPhone
//
//  Created  on 2015/06/30.
//
//  Adstirの動画広告のコントローラー

#import "IronSourceMovieAdController.h"

@implementation IronSourceMovieAdController


//=================================================================================
//共通処理
//=================================================================================

//初期化
- (id)init{
    self = [super init];
    
    _isReceivedReward = NO;
    
    [IronSource setRewardedVideoDelegate:self];
    [IronSource shouldTrackReachability:YES];
    
    //動画を読み込む
    [self loadVideoRandomly];
    
    return self;
}

-(BOOL)canShow
{
    return  [IronSource hasRewardedVideo];
}
//表示
-(void)show{
    _isReceivedReward = NO;
    
    // 広告の表示
    if([IronSource hasRewardedVideo])
    {
        
        [IronSource showRewardedVideoWithViewController:UnityGetGLViewController() placement:IRONSOURCE_REWARDVIDEO];
    }else {
        [self loadVideoRandomly];
    }
}

//=================================================================================
//内部処理
//=================================================================================

/** ランダムな間隔で動画を読み込むメソッド */
- (void)loadVideoRandomly{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((arc4random() % 10) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       [IronSource rewardedVideoPlacementInfo:IRONSOURCE_REWARDVIDEO];
    });
}

#pragma mark - Delegate
- (void)rewardedVideoHasChangedAvailability:(BOOL)available {
    
    NSLog(@"Ironsource: has changed available %d", available);
}
- (void)didReceiveRewardForPlacement:(ISPlacementInfo *)placementInfo {
    
    _isReceivedReward = YES;
    
    NSLog(@"Ironsource: receive reward %@ - %@ ", placementInfo.placementName, placementInfo.rewardName);
}

- (void)rewardedVideoDidFailToShowWithError:(NSError *)error {
    
    NSLog(@"Ironsource: did load with error %@ ", error.description);
}

- (void)rewardedVideoDidOpen {
    
    NSLog(@"Ironsource: did video open");
}

- (void)rewardedVideoDidClose {
    
    [MovieAdController sendFinishedPlayingMessage:_isReceivedReward];
    NSLog(@"Ironsource: did video close");
}

- (void)rewardedVideoDidStart {
    
    NSLog(@"Ironsource: did video start");
}

- (void)rewardedVideoDidEnd {
    
    _isReceivedReward = YES;
    NSLog(@"Ironsource: did video end");
}

- (void)didClickRewardedVideo:(ISPlacementInfo *)placementInfo {
    
    NSLog(@"Ironsource: did video click");
}

@end
