﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingLabel : MonoBehaviour
{

	[SerializeField]
	GameObject frame;

	[SerializeField]
	UILabel lbl;

	int lastRank = -1;

	void Start ()
	{
		RankingManager.Instance.GetRankingValue ();
	}

	void LateUpdate ()
	{
		if (AdManager.Instance.Ranking > 0) {

			if (lastRank < 0) {

				frame.SetActive (true);
				lbl.text = AdManager.Instance.Ranking.ToString ();

				lastRank = AdManager.Instance.Ranking;

				RankingManager.Instance.GetRankingValue ();

			} else if (AdManager.Instance.Ranking < lastRank) {

				lbl.text = AdManager.Instance.Ranking.ToString ();

				lastRank = AdManager.Instance.Ranking;

			}

			RankingManager.Instance.GetRankingValue ();
		}
	}
}
