﻿//  TitleRecordLabel.cs
//  ProductName 胴上げ
//
//  Created by kan kikuchi on 2015.11.19.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// タイトルのハイスコアを表示するラベル
/// </summary>
public class TitleRecordLabel : MonoBehaviour {

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start(){
		GetComponent<UILabel> ().text = "Best " + UserData.HighScore.ToString () + Define.SCORE_UNIT;
	}

}
