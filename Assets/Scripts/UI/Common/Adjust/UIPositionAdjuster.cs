﻿//  TitleBottomButtons.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.19.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// UIの位置を調整するクラス
/// </summary>
public class UIPositionAdjuster : UIAdjuster {

	private RectTransform _rectTransform;

	//ベースにするアス比
	[SerializeField]
	private AspectRateType _baseAspectRateType = AspectRateType.iPhone4;

	//基準位置とアス比に合わせて移動する距離
	[SerializeField]
	private Vector2 _baisisPos = Vector2.zero, _moveDistance = Vector2.zero, _iPadBaisisPos = Vector2.zero;

	//=================================================================================
	//更新
	//=================================================================================

	protected override void AdjustUI(){
		base.AdjustUI();

		if(_rectTransform == null){
			_rectTransform = GetComponent<RectTransform> ();
		}

		//アス比に合わせて下部ボタンを移動する
		Vector2 baseAspectRate = SCREEN_SIZE_640_960;
		if(_baseAspectRateType == AspectRateType.iPhone5){
			baseAspectRate = SCREEN_SIZE_640_1136;
		}

		float aspectRate = 1.0f / GetScreenSizeRate (baseAspectRate, false) - 1.0f;
		aspectRate = Mathf.Max (aspectRate, 0);

		Vector2 pos = _rectTransform.anchoredPosition, basisPos = _baisisPos, moveDistance = _moveDistance;

		//iPadの場合は基準位置と移動距離を変える
		if(IsIpad()){
			basisPos     = _iPadBaisisPos;
			moveDistance = Vector3.zero;
		}

		pos.x = basisPos.x + moveDistance.x * aspectRate;
		pos.y = basisPos.y + moveDistance.y * aspectRate;

		_rectTransform.anchoredPosition = pos;
	}

}