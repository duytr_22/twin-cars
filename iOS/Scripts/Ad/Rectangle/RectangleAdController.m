//
//  RectangleAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/30.
//
//  レクタングル広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "RectangleAdController.h"


@implementation RectangleAdController


//=================================================================================
//共通処理
//=================================================================================

//初期化
- (id)init:(BOOL)isBottom isNative:(BOOL)isNative  isAdmob:(BOOL)isAm isPopup:(BOOL)isPop {
    self = [super init];
    
    isEndRect = isBottom;
    isFiveAds = isNative;
    isAdmob = isAm;
    isPopup = isPop;
    
    _rectangleAdView = [[UIView alloc] init];
    _rectangleAdView.backgroundColor = [UIColor clearColor];
    
    [_rectangleAdView setFrame:[self GetRectangleAdFrame:isBottom]];
    [_rectangleAdView addSubview: [self prepareRectangleAd:[self GetFrame]]];
    
    [_rectangleAdView setHidden:true];
    
    [UnityGetGLViewController().view addSubview:_rectangleAdView];
    
    return self;
}


//レクタングルのフレームを取得
-(CGRect)GetRectangleAdFrame:(BOOL)isBottom{
    
    //下部に表示
    if (isBottom) {
        
        return CGRectMake(0,
                          [[UIScreen mainScreen] bounds].size.height - RECT_AD_HEIGHT -10,
                          [[UIScreen mainScreen] bounds].size.width,
                          RECT_AD_HEIGHT);
        
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    
        if(isAdmob) {
         
            if(isPopup)
                return CGRectMake(0,
                                  450,
                                  [[UIScreen mainScreen] bounds].size.width,
                                  RECT_AD_HEIGHT);
            else
                return CGRectMake(0,
                                  550,
                                  [[UIScreen mainScreen] bounds].size.width,
                                  RECT_AD_HEIGHT);
        } else
            return CGRectMake(0,
                              350,
                              [[UIScreen mainScreen] bounds].size.width,
                              RECT_AD_HEIGHT);
    } else {
        
        CGRect frame = [[UIScreen mainScreen] applicationFrame];
        
        if (frame.size.height > 480.0)
        {
            if(isAdmob) {
                
                if(isPopup)
                    return CGRectMake(0,
                                      150,
                                      [[UIScreen mainScreen] bounds].size.width,
                                      RECT_AD_HEIGHT);
                else
                    return CGRectMake(0,
                                      [[UIScreen mainScreen] bounds].size.height - RECT_AD_HEIGHT -10,
                                      [[UIScreen mainScreen] bounds].size.width,
                                      RECT_AD_HEIGHT);
                
            } else
                return CGRectMake(0,
                                  100,
                                  [[UIScreen mainScreen] bounds].size.width,
                                  RECT_AD_HEIGHT);
        } else {
            
            return CGRectMake(0,
                              50,
                              [[UIScreen mainScreen] bounds].size.width,
                              RECT_AD_HEIGHT);
        }
    }
    
    
    
}

//フレームを取得
-(CGRect)GetFrame{
    return  CGRectMake(([_rectangleAdView frame].size.width - RECT_AD_WIDTH)*0.5,
                       0,
                       RECT_AD_WIDTH,
                       RECT_AD_HEIGHT);
}

//非表示切り替え
-(void)updateRectangleAd:(BOOL)enable{
    [_rectangleAdView setHidden:!enable];
    
    if(!enable)
        [self removeRectangleAd];
    else
        [self reloadRectangleAd];
}

-(void) removeRectangleAd
{
    
}

-(void) reloadRectangleAd
{
    
}

//=================================================================================
//継承先で実装
//=================================================================================

//レクタングルアドを初期化
-(UIView *)prepareRectangleAd:(CGRect)frame{
    return nil;
}


-(UIView *)prepareRectangleAdNative:(CGRect)frame{
    return nil;
}

-(void)dealloc{
}

@end
