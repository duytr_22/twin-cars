﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" void DEFAULT_CALL ReversePInvokeWrapper_AppleStoreImpl_MessageCallback_m846550198(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FacebookStoreImpl_MessageCallback_m2188567040(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TizenStoreImpl_MessageCallback_m187990427(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosTenjin_DeepLinkHandler_m676897158(intptr_t ___deepLinkDataPairArray0, int32_t ___deepLinkDataPairCount1);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[4] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AppleStoreImpl_MessageCallback_m846550198),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_FacebookStoreImpl_MessageCallback_m2188567040),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_TizenStoreImpl_MessageCallback_m187990427),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosTenjin_DeepLinkHandler_m676897158),
};
