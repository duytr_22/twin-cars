package jp.oliviaashley.Advertising.Providers;

import com.socdm.d.adgeneration.ADG;
import com.socdm.d.adgeneration.ADG.AdFrameSize;
import com.socdm.d.adgeneration.ADGListener;
import com.socdm.d.adgeneration.ADGConsts.ADGErrorCode;

import jp.oliviaashley.Twins.R;
import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;

/**
 * Library dependencies:
 * 		ADG-2.0.0.jar
 * 		google-play-services
 *
 * AndroidManifest.xml:
 * 		<uses-permission android:name="android.permission.INTERNET"/>
 * 		<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 * 		<application>
 * 			<meta-data android:name="com.google.android.gms.version"
 * 				android:value="@integer/google_play_services_version" />
 *          <activity android:name="com.socdm.d.adgeneration.ADGClickWebView"
 *        		android:configChanges="orientation|keyboardHidden" />
 *		 </application>
 *
 * @author Aaron Santin - 2014
 * @version 1.0
 */
public class AdGenerationHelper {

    // XXX AdGeneration also has banners, implement it

    private static final String TAG = "AdGenerationHelper";

    // ADGRectangle
    private static ADG adgBanner  = null;

    private static ADG adgRect  = null;
    private static ADG adgRectEnd  = null;
    private static ADG adgRectGame  = null;

    private static LinearLayout layout;
    private static LinearLayout layoutEnd;
    private static LinearLayout layoutGame;

    /**
     * Put this on your activity's onCreate method to preload intertitials
     *
     * @param activity
     * 				the main activity
     */
    public static void doOnCreate(Activity activity) {
    }

    public static void doOnStop(){
    }

    public static void doOnResume() {
        // 広告表示/ローテーション再開
        if (adgBanner!= null) {
            adgBanner.start();
        }
        if (adgRectEnd!= null) {
            adgRectEnd.start();
        }
        if (adgRect!= null) {
            adgRect.start();
        }
    }

    public static void doOnPause() {
        // ローテーション停止
        if (adgBanner!= null) {
            adgBanner.pause();
        }
        if (adgRect!= null) {
            adgRect.pause();
        }
        if (adgRectEnd!= null) {
            adgRectEnd.pause();
        }
    }

    public static LinearLayout getBannerView(Activity activity){

        Log.v(TAG, "Showing AdGeneration Banner ...");

        LinearLayout layout = new LinearLayout(activity);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        adgBanner = new ADG(activity);
        adgBanner.setLocationId(activity.getResources().getString(R.string.AdGene_BannerID));
        adgBanner.setAdFrameSize(AdFrameSize.SP);
        adgBanner.start();
        adgBanner.setAdListener(
                new ADGListener() {
                    @Override
                    public void onReceiveAd() {
                        Log.v(TAG, "On ReceiveAD ...");
                    }

                    @Override
                    public void onFailedToReceiveAd(ADGErrorCode code) {
                        // 不通とエラー過多のとき以外はリトライ
                        Log.v(TAG, "Error " + code);
                        switch (code) {
                            case EXCEED_LIMIT:
                            case NEED_CONNECTION:
                                break;
                            default:
                                if (adgBanner != null) {
                                    adgBanner.start();
                                }
                                break;
                        }
                    }
                });

        layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layout.addView(adgBanner, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layout;
    }

    public static LinearLayout getAdRectangleEndView(Activity activity){

        layoutEnd = new LinearLayout(activity);
        layoutEnd.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        adgRectEnd = new ADG(activity);
        adgRectEnd.setLocationId(activity.getResources().getString(R.string.AdGene_Rect_End_ID));
        adgRectEnd.setAdFrameSize(AdFrameSize.RECT);
        adgRectEnd.start();
        adgRectEnd.setAdListener(
                new ADGListener() {
                    @Override
                    public void onReceiveAd() {
                        Log.v(TAG, "On ReceiveAD ...");
                    }

                    @Override
                    public void onFailedToReceiveAd(ADGErrorCode code) {
                        // 不通とエラー過多のとき以外はリトライ
                        Log.v(TAG, "Error " + code);
                        switch (code) {
                            case EXCEED_LIMIT:
                            case NEED_CONNECTION:
                                break;
                            default:
                                if (adgRectEnd != null) {
                                    adgRectEnd.start();
                                }
                                break;
                        }
                    }
                });

        layoutEnd.addView(adgRectEnd, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layoutEnd;
    }

    public static LinearLayout getAdRectangleView(Activity activity){

        layout = new LinearLayout(activity);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        adgRect = new ADG(activity);
        adgRect.setLocationId(activity.getResources().getString(R.string.AdGene_Rect_ID));
        adgRect.setAdFrameSize(AdFrameSize.RECT);
        adgRect.start();
        adgRect.setAdListener(
                new ADGListener() {
                    @Override
                    public void onReceiveAd() {
                        Log.v(TAG, "On ReceiveAD ...");
                    }

                    @Override
                    public void onFailedToReceiveAd(ADGErrorCode code) {
                        // 不通とエラー過多のとき以外はリトライ
                        Log.v(TAG, "Error " + code);
                        switch (code) {
                            case EXCEED_LIMIT:
                            case NEED_CONNECTION:
                                break;
                            default:
                                if (adgRect != null) {
                                    adgRect.start();
                                }
                                break;
                        }
                    }
                });

        layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layout.addView(adgRect, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layout;
    }

    public static LinearLayout getAdRectangleGameView(Activity activity){

        layoutGame = new LinearLayout(activity);
        layoutGame.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        adgRectGame = new ADG(activity);
        adgRectGame.setLocationId(activity.getResources().getString(R.string.AdGene_Rect_ID));
        adgRectGame.setAdFrameSize(AdFrameSize.RECT);
        adgRectGame.start();
        adgRectGame.setAdListener(
                new ADGListener() {
                    @Override
                    public void onReceiveAd() {
                        Log.v(TAG, "On ReceiveAD ...");
                    }

                    @Override
                    public void onFailedToReceiveAd(ADGErrorCode code) {
                        // 不通とエラー過多のとき以外はリトライ
                        Log.v(TAG, "Error " + code);
                        switch (code) {
                            case EXCEED_LIMIT:
                            case NEED_CONNECTION:
                                break;
                            default:
                                if (adgRectGame != null) {
                                    adgRectGame.start();
                                }
                                break;
                        }
                    }
                });

        layoutGame.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layoutGame.addView(adgRectGame, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layoutGame;
    }

    public static LinearLayout getAdRectanglePopupView(Activity activity){

        layoutGame = new LinearLayout(activity);
        layoutGame.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        adgRectGame = new ADG(activity);
        adgRectGame.setLocationId(activity.getResources().getString(R.string.AdGene_Rect_POPUP_ID));
        adgRectGame.setAdFrameSize(AdFrameSize.RECT);
        adgRectGame.start();
        adgRectGame.setAdListener(
                new ADGListener() {
                    @Override
                    public void onReceiveAd() {
                        Log.v(TAG, "On ReceiveAD ...");
                    }

                    @Override
                    public void onFailedToReceiveAd(ADGErrorCode code) {
                        // 不通とエラー過多のとき以外はリトライ
                        Log.v(TAG, "Error " + code);
                        switch (code) {
                            case EXCEED_LIMIT:
                            case NEED_CONNECTION:
                                break;
                            default:
                                if (adgRectGame != null) {
                                    adgRectGame.start();
                                }
                                break;
                        }
                    }
                });

        layoutGame.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        layoutGame.addView(adgRectGame, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return layoutGame;
    }

    public static boolean isJapaneseLanguage(Activity activity) {
        String lang = activity.getResources().getConfiguration().locale.getLanguage();
        if (lang.equals("ja")) {
            Log.d(TAG, "日本語");
            return true;
        }
        return false;
    }
}
