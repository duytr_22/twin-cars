//
//  GAEConfig.m
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  GAEの設定を読み込み、保持するクラス

#import "GAEConfig.h"

@implementation GAEConfig

//=================================================================================
//読み込んだGAEの設定とそれを取得するメソッド
//=================================================================================
static NSString *newsType = NULL;
+(NSString*)getNewsType{return newsType;}

static NSString *splashType = NULL;
+(NSString*)getSplashType{return splashType;}

static NSString *splashTypeRank = NULL;
+(NSString*)getSplashTypeRank{return splashTypeRank;}

static int64_t splashFrequency = 4;
+(int64_t)getSplashFrequency{return splashFrequency;};

static NSString *bannerType = NULL;
+(NSString*)getBannerType{return bannerType;};

static NSString *rectangleType = NULL;
+(NSString*)getRectangleType{return rectangleType;};

static NSString *rectangleEndType = NULL;
+(NSString*)getRectangleEndType{return rectangleEndType;};

static NSString * isSWAT = NULL;
+(NSString*)getIsSWAT{return isSWAT;};

static NSString * browser = NULL;
+(BOOL)isEnabledBrowser{
    return [browser isEqualToString:GAE_BROWSER_ON_KEY];
}

static NSString *videoType = NULL;
+(NSString*)getVideoType{return videoType;};

static int64_t incentiveRateAdColony = 1;
+(int64_t)getIncentiveRateAdColony{return incentiveRateAdColony;};

static int64_t incentiveRateUnityAds = 1;
+(int64_t)getIncentiveRateUnityAds{return incentiveRateUnityAds;};

static int64_t incentiveRateMaio = 1;
+(int64_t)getIncentiveRateMaio{return incentiveRateMaio;};

static int64_t videoFrequency   = 4;
+(int64_t)getVideoFrequency{return videoFrequency;};

//=================================================================================
//読み込み、設定
//=================================================================================
//GAEを読み込み、設定を確認する
+ (void)confirmGAE:(id)callbackInstance selector:(SEL)callback{
    
    UserInfoObject *userInfo = [UserInfoObject userInfo];
    [userInfo confirmWhichAdSplashWithHandler:^(NSDictionary *dict, NSError *error) {
        
        //読み込み失敗時は移行の処理は実行しない
        if (error){
            NSLog(@"confirmGAE 失敗");
            //読み込み失敗時にコールバックメソッドを実行
            [callbackInstance performSelector:callback];
            return;
        }
        NSLog(@"confirmGAE 成功");
        
        //各項目を読み込み
        newsType = [NSString stringWithFormat:@"%@", [dict objectForKey:@"newsType"]];
        
        splashType      = [NSString stringWithFormat:@"%@", [dict objectForKey:@"splashType"]];
        splashTypeRank  = [NSString stringWithFormat:@"%@", [dict objectForKey:@"splashTypeRank"]];
        splashFrequency = [[dict objectForKey:@"splashFrequency"] intValue];
        
        bannerType = [NSString stringWithFormat:@"%@", [dict objectForKey:@"bannerType"]];
        
        rectangleType    = [NSString stringWithFormat:@"%@", [dict objectForKey:@"rectangleType"]];
        rectangleEndType = [NSString stringWithFormat:@"%@", [dict objectForKey:@"rectangleTypeEnd"]];
        
        isSWAT  = [NSString stringWithFormat:@"%@",[dict objectForKey:@"SWAT"]];
        browser = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Browser"]];
        
        videoType              = [NSString stringWithFormat:@"%@", [dict objectForKey:@"videoType"]];
        
        //デバッグで強制的に設定を変更
#ifdef DEBUG_GAE_STARTUP_POP
        NSLog(@"GAEのnewsTypeをデバッグで%@から%@に変更", newsType, DEBUG_GAE_STARTUP_POP);
        newsType = DEBUG_GAE_STARTUP_POP;
#endif
#ifdef DEBUG_GAE_BANNER
        NSLog(@"GAEのbannerTypeをデバッグで%@から%@に変更", bannerType, DEBUG_GAE_BANNER);
        bannerType = DEBUG_GAE_BANNER;
#endif
#ifdef DEBUG_GAE_SPLASH
        NSLog(@"GAEのsplashTypeをデバッグで%@から%@に変更", splashType, DEBUG_GAE_SPLASH);
        splashType = DEBUG_GAE_SPLASH;
#endif
#ifdef DEBUG_GAE_SPLASH_RANK
        NSLog(@"GAEのsplashTypeRankをデバッグで%@から%@に変更", splashTypeRank, DEBUG_GAE_SPLASH_RANK);
        splashTypeRank = DEBUG_GAE_SPLASH_RANK;
#endif
#ifdef DEBUG_GAE_RECTANGLE
        NSLog(@"GAEのrectangleTypeをデバッグで%@から%@に変更", rectangleType, DEBUG_GAE_RECTANGLE);
        rectangleType = DEBUG_GAE_RECTANGLE;
#endif
#ifdef DEBUG_GAE_RECTANGLE_END
        NSLog(@"GAEのrectangleTypeEndをデバッグで%@から%@に変更", rectangleEndType, DEBUG_GAE_RECTANGLE_END);
        rectangleEndType = DEBUG_GAE_RECTANGLE_END;
#endif
#ifdef DEBUG_GAE_BROWSER_KEY
        NSLog(@"GAEのBrowserをデバッグで%@から%@に変更", browser, DEBUG_GAE_BROWSER_KEY);
        browser = DEBUG_GAE_BROWSER_KEY;
#endif
        
        
        NSLog(@"newsType:%@, splashType:%@ splashTypeRank:%@, splashFrequency:%lld, bannerType:%@, rectangleType:%@, rectangleEndType:%@, browser:%@", newsType, splashType, splashTypeRank, splashFrequency, bannerType, rectangleType, rectangleEndType, browser);
        
        
        //インセンティブ付き動画表示比率
        incentiveRateAdColony = [[dict objectForKey:@"incentiveRate_AC"] intValue];
        incentiveRateUnityAds = [[dict objectForKey:@"incentiveRate_UA"] intValue];
        incentiveRateMaio     = [[dict objectForKey:@"incentiveRate_MA"] intValue];
        videoFrequency   = [[dict objectForKey:@"videoFrequency"] intValue];
        
        //デバッグで強制的に設定を変更
#ifdef DEBUG_INCENTVIE_RATE_ADCOLONY
        NSLog(@"GAEのincentiveRateAdColonyCをデバッグで%lldから%dに変更", incentiveRateAdColony, DEBUG_INCENTVIE_RATE_ADCOLONY);
        incentiveRateAdColony = DEBUG_INCENTVIE_RATE_ADCOLONY;
#endif
#ifdef DEBUG_INCENTVIE_RATE_UNITYADS
        NSLog(@"GAEのincentiveRateUnityAdsをデバッグで%lldから%dに変更", incentiveRateUnityAds, DEBUG_INCENTVIE_RATE_UNITYADS);
        incentiveRateUnityAds = DEBUG_INCENTVIE_RATE_UNITYADS;
#endif
#ifdef DEBUG_INCENTVIE_RATE_MAIO
        NSLog(@"GAEのincentiveRateMaioをデバッグで%lldから%dに変更", incentiveRateMaio, DEBUG_INCENTVIE_RATE_MAIO);
        incentiveRateMaio = DEBUG_INCENTVIE_RATE_MAIO;
#endif
#ifdef DEBUG_VIDEO_FREQUENCY
        NSLog(@"GAEのvideoFrequencyをデバッグで%lldから%dに変更", videoFrequency, DEBUG_VIDEO_FREQUENCY);
        videoFrequency = DEBUG_VIDEO_FREQUENCY;
#endif
        
        NSLog(@"videoFrequency:%lld\nAdColony:UnityAds:Maio\n%lld:%lld:%lld", videoFrequency, incentiveRateAdColony, incentiveRateUnityAds, incentiveRateMaio);
        
        
        //読み込み成功時にコールバックメソッドを実行
        [callbackInstance performSelector:callback];
        
    }];
    
}

@end
