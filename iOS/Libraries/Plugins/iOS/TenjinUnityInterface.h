//
//  TenjinUnityInterface.h
//  Unity-iOS bridge
//
//  Created by Christopher Farm.
//
//

#ifndef __Unity_iPhone__TenjinUnityInterface__
#define __Unity_iPhone__TenjinUnityInterface__

#include "TenjinSDK.h"

extern "C" {
typedef struct TenjinStringStringKeyValuePair {
    const char* key;
    const char* value;
} TenjinStringStringKeyValuePair;

typedef void (*TenjinDeeplinkHandlerFunc)(TenjinStringStringKeyValuePair* deepLinkDataPairArray, int32_t deepLinkDataPairCount);

void iosTenjinConnect(const char* apiKey);
void iosTenjinConnectWithDeferredDeeplink(const char* apiKey, const char* deferredDeeplink);
void iosTenjinSendEvent(const char* eventName);
void iosTenjinSendEventWithValue(const char* eventName, const char* eventValue);
void iosTenjinTransaction(const char* productId, const char* currencyCode, int quantity, double price);
void iosTenjinTransactionWithReceiptData(const char* productId, const char* currencyCode, int quantity, double price, const char* transactionId, const char* receipt);
void iosTenjinRegisterDeepLinkHandler(TenjinDeeplinkHandlerFunc deeplinkHandlerFunc);
}

#endif
