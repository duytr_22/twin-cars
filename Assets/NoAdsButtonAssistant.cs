﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoAdsButtonAssistant : ButtonAssistant
{

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		#if UNITY_ANDROID
		AdManager.Instance.Buy ();
		#else
		IAPController.Instance.Buy ();
		#endif
	}

}
