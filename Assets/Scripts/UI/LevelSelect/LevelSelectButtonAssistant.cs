﻿//  LevelSelectButtonAssistant.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.25.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// レベル選択をするラベル付きボタンのアシスタントクラス
/// </summary>
public class LevelSelectButtonAssistant : ButtonAssistant
{

	//このボタンのステージNo
	[SerializeField]
	private int _stageNo = 1;

	//解放していない時の画像
	[SerializeField]
	private Sprite _lockSprite;

	//ステージ番号を表記するテキスト
	[SerializeField]
	private Text _stageNoText;

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Awake ()
	{
		base.Awake ();
		UpdateUI ();
	}

	/// <summary>
	/// ステージNoを登録し、ボタン画像を変更
	/// </summary>
	public virtual void SetStageNo (int stageNo)
	{
		//ステージNo登録、ソート用にオブジェクト名をステージ番号に
		_stageNo = stageNo;
		gameObject.name = _stageNo.ToString ("D5");

		//UI
		UpdateUI ();
	}

	//UIを更新
	private void UpdateUI ()
	{
		//テキスト設定
		_stageNoText.text = _stageNo.ToString ();

		//選べるステージか確認
		bool canPlay = UserData.HighScore >= _stageNo - 1;

		//常に全ステージ選べるようにするデバッグコード
		#if CAN_PLAY_ALL_STAGE
		canPlay = true;
		#endif

		//選べない時はボタン画像変更、ボタン無効、ラベル表示
		if (!canPlay) {
			GetComponent<Image> ().sprite = _lockSprite;
			GetComponent<Image> ().SetNativeSize ();
			GetComponent<Button> ().enabled = false;
			_stageNoText.gameObject.SetActive (false);
		}
	}

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		CacheData.Entity.StageNo = _stageNo;
		SceneNavigator.Instance.MoveScene (SceneName.GAME);
	}

}