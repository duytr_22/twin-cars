//
//  RankingPlugin.mm
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  Unity側とのランキング関連で連携を行うプラグイン

#import "RankingController.h"

//=================================================================================
//Unity側との連携
//=================================================================================


extern "C" {
    
    //初期化、ネイティブ側のプラグインのインスタンスを作成し、Unity側へ渡す
    void *_RankingPlugin_Init(const char *gameObjectName){
        id instance = [[RankingController alloc]initWithGameObjectName:gameObjectName];
        CFRetain((CFTypeRef)instance);
        return (__bridge void *)instance;
    }
    
    //リーダーボード表示
    void _ShowLeaderBoard(void *instance) {
        RankingController *rankingController = (__bridge RankingController*)instance;
        [rankingController showLeaderBoard];
    }
    
    //スコアをリーダーボードに送信
    void _RankingReportIntScore(void *instance, int rankingNo, int score){
        RankingController *rankingController = (__bridge RankingController*)instance;
        [rankingController reportScore:rankingNo intScore:score];
    }
    
    //スコアをリーダーボードに送信
    void _RankingReportFloatScore(void *instance, int rankingNo, float score){
        RankingController *rankingController = (__bridge RankingController*)instance;
        [rankingController reportScore:rankingNo floatScore:score];
    }
    
    void _GetRankingValue(void *instance)
    {
        RankingController *rankingController = (__bridge RankingController*)instance;
        [rankingController getUserRanking];
    }
}


