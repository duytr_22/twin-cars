﻿using UnityEngine;
using System.Collections;

public class TutorialPointScript : MonoBehaviour {
	
	//for tutorial
	public delegate void TriggerTutorial ();
	public static event TriggerTutorial OnTriggerTutorialPoint;
	public static event TriggerTutorial OnTriggerTutorialPassPoint;

	private Rigidbody rigibody;
	private int collisionCount;

	// Use this for initialization
	void Start () {
		rigibody = this.GetComponent <Rigidbody> ();
		if (rigibody.IsSleeping ()) {
			rigibody.WakeUp ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (rigibody.IsSleeping ()) {
			rigibody.WakeUp ();
		}
	}

	void OnTriggerEnter(Collider col) {
		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial) {
			//because there have two cube on a line
			//we dont want call this twice
			if (collisionCount >= 1) {
				collisionCount = 0;
				if (this.gameObject.tag == "TutorialPoint") {
					OnTriggerTutorialPoint ();
				} else {
					OnTriggerTutorialPassPoint ();
				}
			} else {
				collisionCount++;
			}
		}
	}
}
