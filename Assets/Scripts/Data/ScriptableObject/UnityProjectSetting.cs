﻿//  UnityProjectSetting .cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.22.

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

//ゲームの種類
[Flags]
public enum GameType
{
	Mini = 1 << 0,
	ModeSelect = 1 << 1,
	LevelSelect = 1 << 2,
}

//[CreateAssetMenu()]
/// <summary>
/// プロジェクトの各設定値
/// </summary>
public class UnityProjectSetting : ScriptableObject
{

	//初回起動した事のフラグを保存する時のKey
	public const string IS_LAUNCHED_PROJECT_FLAG_KEY = "IS_LAUNCHED_PROJECT_FLAG";

	//設定ファイルの実体
	private static UnityProjectSetting _entity;

	public  static UnityProjectSetting  Entity {
		get {
			if (_entity == null) {
				_entity = Resources.Load<UnityProjectSetting> (ResourcesFilePath.ASSET_UNITY_PROJECT_SETTING);

				//エディター上で初回アクセスがあった時、起動した事おフラグを保存
				#if UNITY_EDITOR
				if (!IsLaunchedProject) {
					EditorUserSettings.SetConfigValue (IS_LAUNCHED_PROJECT_FLAG_KEY, IS_LAUNCHED_PROJECT_FLAG_KEY);
					Debug.Log ("Save IsLaunchedProjectFlag");
				}
				#endif

				if (_entity == null) {
					Debug.LogError (ResourcesFilePath.ASSET_UNITY_PROJECT_SETTING + " not found");
				}
			}

			return _entity;
		}
	}

	#if UNITY_EDITOR
	//このプロジェクトを一度でも起動したことがあるか
	public static bool _isLaunchedProject = false;

	public static bool IsLaunchedProject {
		get {
			//EditorUserSettingsにフラグが設定されているか否かで判定する
			if (!_isLaunchedProject) {
				_isLaunchedProject = !string.IsNullOrEmpty (EditorUserSettings.GetConfigValue (IS_LAUNCHED_PROJECT_FLAG_KEY));
			}
			return _isLaunchedProject;
		}
	}
	#endif

	//=================================================================================
	//ゲームの設定
	//=================================================================================

	[Header ("======Game Type Setting======")]
	[Space (5)]

	[SerializeField]
	private GameType _gameType = GameType.Mini;

	public  GameType  GameType {
		get{ return _gameType; }
	}

	//ステージ数
	[HideInInspector]
	public int StageNum = 1;

	#if UNITY_EDITOR
	[MenuItem ("Tools/Info/StageNum")]
	private static void ShowStageNum ()
	{
		Debug.Log ("StageNum : " + UnityProjectSetting.Entity.StageNum);
	}
	#endif

	//=================================================================================
	//ロード
	//=================================================================================

	[Header ("======Load Setting======")]

	//ロードするステージの倍数
	[SerializeField]
	private int _loadStageNo = 6;

	public  int  LoadStageNo {
		get{ return _loadStageNo; }
	}

	//=================================================================================
	//シェア
	//=================================================================================

	[Header ("======Share Setting======")]

	//シェア文
	[SerializeField, TextAreaAttribute]
	private string
		_shareFormatInTitle = "おじさんの頭にカツラを乗せろ！？\nツルツルしすぎて難しい！ww";
	[SerializeField, TextAreaAttribute, Space (10)]
	private string
		_shareFormatInClear = "【ステージ{0}】\nカツラのセッティング完了！！\nおじさんの頭ツルツルしすぎ！ww",
		_shareFormatInMiss = "【ステージ{0}】\nカツラのセッティング失敗！！\nおじさんの頭ツルツルしすぎ！ww";

	public Dictionary<ShareLocation, string> _shareFormatDict;

	public Dictionary<ShareLocation, string> ShareFormatDict {
		get {
			if (_shareFormatDict == null) {
				_shareFormatDict = new Dictionary<ShareLocation, string> () {
					{ ShareLocation.Title,  _shareFormatInTitle },
					{ ShareLocation.Result, _shareFormatInClear },
					{ ShareLocation.Clear,  _shareFormatInClear },
					{ ShareLocation.Miss,   _shareFormatInMiss  },
				};
			}
			return _shareFormatDict;
		}
	}

	[Header ("======Share Setting English ======")]

	//シェア文
	[SerializeField, TextAreaAttribute]
	private string
		_shareFormatInTitleEnglish = "おじさんの頭にカツラを乗せろ！？\nツルツルしすぎて難しい！ww";
	[SerializeField, TextAreaAttribute, Space (10)]
	private string
		_shareFormatInClearEnglish = "【ステージ{0}】\nカツラのセッティング完了！！\nおじさんの頭ツルツルしすぎ！ww",
		_shareFormatInMissEnglish = "【ステージ{0}】\nカツラのセッティング失敗！！\nおじさんの頭ツルツルしすぎ！ww";

	public Dictionary<ShareLocation, string> _shareFormatDictEnglish;

	public Dictionary<ShareLocation, string> ShareFormatDictEnglish {
		get {
			if (_shareFormatDictEnglish == null) {
				_shareFormatDictEnglish = new Dictionary<ShareLocation, string> () {
					{ ShareLocation.Title,  _shareFormatInTitleEnglish },
					{ ShareLocation.Result, _shareFormatInClearEnglish },
					{ ShareLocation.Clear,  _shareFormatInClearEnglish },
					{ ShareLocation.Miss,   _shareFormatInMissEnglish  },
				};
			}
			return _shareFormatDictEnglish;
		}
	}

	[Header ("====== Tags ======")]

	//タグ
	[SerializeField, TextAreaAttribute, Space (10)]
	private string _tag = "#Goodia #グッディア #うどんちゅるん";

	public  string  Tag {
		get{ return _tag; }
	}

	[Header ("====== Store Url ======")]

	//ストアURL
	[SerializeField, TextAreaAttribute, Space (10)]
	private string _iosStoreURL = "https://goo.gl/KsuszL", _androidStoreURL = "https://goo.gl/mm443X";

	public string StoreURL {
		get {
			if (Application.platform == RuntimePlatform.Android) {
				return _androidStoreURL;
			}
			return _iosStoreURL;
		}
	}

}
