﻿using UnityEngine;
using System.Collections;

public class CloudScript : MonoBehaviour
{
	UISprite sprite;

	// Use this for initialization
	void Start ()
	{
		sprite = this.GetComponent<UISprite> ();

		gameObject.SetActive (true);

		sprite.color = BackgroundScript.Instance.cloudColors [0];

		BackgroundScript.Instance.OnCloudChangeColor += OnCloudChangeColor;
	}

	void OnCloudChangeColor (Color color)
	{
		sprite.color = color;
	}
}
