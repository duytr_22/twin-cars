﻿//  SampleCube.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.21.

using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲームのサンプルで使ってるキューブ
/// </summary>
public class SampleCube : MonoBehaviour, IPointerClickHandler{

	//クリックされた時のイベント
	public event Action OnClick = delegate{};

	//=================================================================================
	//タッチ処理
	//=================================================================================

	/// <summary>
	/// クリック(タップ)時
	/// </summary>
	public void OnPointerClick(PointerEventData eventData){
		GameManager.Instance.AddScore (1);
		Destroy (gameObject);
		OnClick ();
	}

}