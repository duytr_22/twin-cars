﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITitle : MonoBehaviour
{
    public void OnTap()
    {
        SceneNavigator.Instance.MoveScene(SceneName.GAME);
    }
}
