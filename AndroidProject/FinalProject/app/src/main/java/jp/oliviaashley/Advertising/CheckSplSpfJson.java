package jp.oliviaashley.Advertising;

import android.app.Activity;
import android.util.Log;

import org.json.JSONObject;

import jp.oliviaashley.Net.Utils.SimpleHttpRequestHelper;
import jp.oliviaashley.Twins.R;

public class CheckSplSpfJson {

    private static JSONObject rootObjSpl = null;

    private static final String TAG = "CheckSplSpfJson";

    public static final String NO_VALUE = "0";

    public static void doOnCreate(Activity activity) {

        if (rootObjSpl == null) {
            String url = activity.getResources().getString(R.string.Fms_url)
                + activity.getResources().getString(R.string.SplSpf_APP_ID)
                + "/"
                + activity.getResources().getString(R.string.SplSpf_SPL_ID);
            rootObjSpl = SimpleHttpRequestHelper.getJSONObject(url);
        } else {
            Log.i(TAG, "Already did");
        }
    }


    public static String getValueSpl(String field) {
        return getValue(rootObjSpl, field);
    }

    public static String getValue(JSONObject rootObj, String field) {
        String value = NO_VALUE;
        if (rootObj == null) {
            return value;
        }
        try {
            value = rootObj.getString(field);
            Log.d(TAG, field + ":" + value);
        } catch (Exception e) {
            value = NO_VALUE;
            Log.e(TAG, "JSONException:" + e.toString());
        }
        return value;
    }

    public static String getNewsType() {
        return getValue(rootObjSpl, "newsType");
    }

    public static String getSplashType() {
        return getValue(rootObjSpl, "splashType");
    }

    public static String getSplashTypeWall() {
        return getValue(rootObjSpl, "splashTypeWall");
    }

    public static String getSplashTypeRank() {
        return getValue(rootObjSpl, "splashTypeRank");
    }

    public static String getSplashTypeExit() {
        return getValue(rootObjSpl, "splashTypeExit");
    }

    public static String getBannerType() {
        return getValue(rootObjSpl, "bannerType");
    }

    public static String getAdSplashFrequency() {
        return getValue(rootObjSpl, "splashFrequency");
    }

    public static String getTitleIconType() {
        return getValue(rootObjSpl, "titleIconType");
    }

    public static String getEndingIconType() {
        return getValue(rootObjSpl, "endingIconType");
    }

    public static String getEndingIconType2() {
        return getValue(rootObjSpl, "endingIconType2");
    }

    public static String getSelectIconType() {
        return getValue(rootObjSpl, "selectIconType");
    }

    public static String getIngameIconType() {
        return getValue(rootObjSpl, "ingameIconType");
    }

    public static String getRectangleType() {
        return getValue(rootObjSpl, "rectangleType");
    }

    public static String getRectangleEndType() {
        return getValue(rootObjSpl, "rectangleTypeEnd");
    }

    public static String getWallType() {
        return getValue(rootObjSpl, "wallType");
    }

    public static String getVideoType() {
        return getValue(rootObjSpl, "videoType");
    }
}