//
//  RankingController.h
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  ランキング関係の処理を行うクラス

#import "CommonConfig.h"
#import "GameCenter.h"

@interface RankingController : UIViewController<GKGameCenterControllerDelegate> {
    
}

//=================================================================================
//メソッド宣言
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName;

//スコアをリーダーボードに送信
-(void)reportScore:(int)rankingNo intScore:(int)score;
-(void)reportScore:(int)rankingNo floatScore:(float)score;

//リーダーボード表示
-(void)showLeaderBoard;

-(void) getUserRanking;

@end
