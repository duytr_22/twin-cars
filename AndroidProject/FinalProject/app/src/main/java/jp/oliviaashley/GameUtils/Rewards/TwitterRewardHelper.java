package jp.oliviaashley.GameUtils.Rewards;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import java.util.EventListener;

import jp.oliviaashley.Social.ShareUtils;
import jp.oliviaashley.Social.TwitterOAuthHelper;
import jp.oliviaashley.Social.TwitterOAuthLogInActivity;


public class TwitterRewardHelper {
    private static final String TAG = "TwitterRewardHelper";

    private static Activity mActivity = null;
    private static String mFilePath = null;

    //Twitter
    public static final int REQUEST_CODE_TWITTER = 1001;
    public static TwitterOAuthHelper tweet_hlp;
    public static String tweet = "";


    //ツイート結果 通知用リスナー
    public interface TwitterRewardListener extends EventListener {
        //処理結果 [true]ツイートした [false]それ以外
        void onTweet(boolean result);
    }


    //リスナー(ダイアログのボタンを押したときに通知)
    private static TwitterRewardListener listener = null;

    private static void setTwitterRewardListener(TwitterRewardListener listener) {
        TwitterRewardHelper.listener = listener;
    }

    public static TwitterRewardListener getTwitterRewardListener() {
        return TwitterRewardHelper.listener;
    }


    /**
     * (Java -> JNI -> C++)ツイートしたかの結果
     *
     * @param result ... true:ツイート完了, false:ツイート未完了(キャンセル・中断 etc)
     */
    private static native void tweetCompleted(final boolean result);


    protected static void didTweet(final boolean result) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);

        // ダイアログの設定
        {
            // result ... ツイート結果
            final String titleText = (result) ? "Tweet完了" : "Tweet失敗";
            final String msgText = (result) ? "Tweetしました！" : "Tweetに失敗しました...";
            final String btnText = (result) ? "OK" : "OK";

            // アラートダイアログのタイトルを設定
            alertDialogBuilder.setTitle(titleText);

            // アラートダイアログのメッセージを設定
            alertDialogBuilder.setMessage(msgText);

            // キャンセル無し
            alertDialogBuilder.setCancelable(false);

            // ボタンを押したとき
            alertDialogBuilder.setPositiveButton(btnText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // ツイート結果を通知
                            TwitterRewardListener listener = TwitterRewardHelper.getTwitterRewardListener();
                            if (listener != null) {
                                listener.onTweet(result);
                            }
                        }
                    });
        }

        // ダイアログの表示
        alertDialogBuilder.show();
    }


    //テキストのみツイート (リスナー指定無し)
    public static void launchTwitter(final Activity activity, final String msg) {
        TwitterRewardListener listener = new TwitterRewardListener() {
            @Override
            public void onTweet(boolean result) {
                //従来の処理 (JNI処理へ)
                TwitterRewardHelper.tweetCompleted(result);
            }
        };

        TwitterRewardHelper.launchTwitter(activity, msg, null, listener);
    }

    //テキストのみツイート (リスナー指定有り)
    public static void launchTwitter(final Activity activity, final String msg, final TwitterRewardListener listener) {
        TwitterRewardHelper.launchTwitter(activity, msg, null, listener);
    }

    //画像付きテキストをツイート (リスナー指定無し)
    public static void launchTwitter(final Activity activity, final String msg, final String filePath) {
        TwitterRewardListener listener = new TwitterRewardListener() {
            @Override
            public void onTweet(boolean result) {
                //従来の処理 (JNI処理へ)
                TwitterRewardHelper.tweetCompleted(result);
            }
        };

        TwitterRewardHelper.launchTwitter(activity, msg, filePath, listener);
    }

    //画像付きテキストをツイート (リスナー指定有り)
    public static void launchTwitter(
            final Activity activity,
            final String msg,
            final String filePath,
            final TwitterRewardListener listener) {
        Log.d(TAG, "launchTwitter() ... ツイート");

        TwitterRewardHelper.mActivity = activity;
        TwitterRewardHelper.mFilePath = filePath;
        TwitterRewardHelper.tweet = ShareUtils.getTweetMessage(activity, msg);
        TwitterRewardHelper.listener = listener;

        TwitterRewardHelper.launchTwitter();
    }


    private static void launchTwitter() {
        Log.d(TAG, "launchTwitter()");
        if (tweet_hlp == null) {
            tweet_hlp = new TwitterOAuthHelper(mActivity);
        }

        //MARK: 現時点は定型文をそのまま送信
        //iOSのように送信するテキストと画像を確認して送信できたら一番いいかなと思う...

        Activity activity = mActivity;
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    try {
                        //MARK: Tweet文章は showHintAlert で最初に生成

                        if (!tweet_hlp.verify_logindata()) {
                            //ログイン処理へ
                            Intent i = new Intent(mActivity, TwitterOAuthLogInActivity.class);
                            i.putExtra("URL", tweet_hlp.get_AuthenticationURL());

                            mActivity.startActivityForResult(i, REQUEST_CODE_TWITTER);
                        } else {
                            //Twitter投稿
                            final boolean result = tweet_hlp.Send_Tweet(tweet, mFilePath);

                            //結果をゲーム側に返す
                            TwitterRewardHelper.didTweet(result);
                        }
                    } catch (Exception e) {
                        //MARK: 覚え書き
                        //ココに処理が飛ばされる場合...
                        //まず AndroidManifest.xml に TwitterOAuthLogInActivity の Activity が宣言されているかどうかを確認

                        //問題が起きた
                        tweet_hlp = null;

                        //結果をゲーム側に返す
                        TwitterRewardHelper.didTweet(false);

                        e.printStackTrace();
                        Log.d(TAG, "Twitter起動失敗 Error");
                    }
                }
            });
        }
    }


    public static void onActivityResult(int request, int response, Intent data) {
        if ((tweet_hlp != null) && (!tweet_hlp.verify_logindata())) {
            final boolean result = tweet_hlp.onActivityResult(request, response, data, tweet, mFilePath);
            TwitterRewardHelper.didTweet(result);

            tweet_hlp = null;
        }
    }
}
