//
//  BannerAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Adstirのバナー広告のコントローラー

#import "IronSourceBannerHelper.h"
#import "CommonConfig.h"
#import <ADG/ADGSettings.h>
#import <UIKit/UIKit.h>

@implementation IronSourceBannerHelper

//バナーアドを初期化
-(UIView*)prepareBannerAd{
    
    [IronSource loadBannerWithViewController:UnityGetGLViewController() size:IS_AD_SIZE_BANNER];
    [IronSource setBannerDelegate:self];
    
    self.view = [[UIView alloc] initWithFrame:CGRectMake(UnityGetGLViewController().view.frame.size.width/2-160, UnityGetGLViewController().view.frame.size.height - 50.0f, 320, 50)];
    self.view.backgroundColor = [UIColor clearColor];
    
    return self.view;
}

#pragma mark - delegate
- (void)bannerDidLoad:(ISBannerView *)bannerView
{
    CGFloat width = CGRectGetWidth(bannerView.bounds);
    CGRect frame = CGRectMake(UnityGetGLViewController().view.frame.size.width/2-width/2, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = frame;
    
    [self.view addSubview:bannerView];
    NSLog(@"Ironsource banner: did load");
}

- (void)bannerDidFailToLoadWithError:(NSError *)error
{
    NSLog(@"Ironsource banner: error %@", error.description);
}

- (void)didClickBanner {
    
    NSLog(@"Ironsource banner: did click");
}

- (void)bannerWillPresentScreen
{
    
    NSLog(@"Ironsource banner: will present screen");
}

- (void)bannerDidDismissScreen
{
    
    NSLog(@"Ironsource banner: did dismiss screen");
}

- (void)bannerWillLeaveApplication
{
    
    NSLog(@"Ironsource banner: will leave application");
}
@end
