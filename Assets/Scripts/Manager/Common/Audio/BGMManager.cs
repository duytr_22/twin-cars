﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// BGM関連の管理をするクラス
/// </summary>
public class BGMManager : AudioManager<BGMManager>
{

	//最後に流したBGM or 流そうとしたBGM
	private string _lastBGMName = "";

	//BGMをフェードアウト中か
	private bool _isFadeOut = false;

	//=================================================================================
	//初期化
	//=================================================================================
	protected override void Init ()
	{
		_audioDirectoryPath = ResourcesDirectoryPath.AUDIO_BGM;
		_audioSourceNum = 1;
		_volumeDefult = 1.0f;
		_isLoop = true;

		base.Init ();

		//BGMの切り替え器を設定
		gameObject.AddComponent<AutoBGMSwitcher> ();
	}

	//=================================================================================
	//更新、変更
	//=================================================================================

	private void Update ()
	{
		if (!_isFadeOut) {
			return;
		}

		//徐々にボリュームを下げていき、ボリュームが0になったらボリュームを戻、あれば次の曲を流す
		_audioSourceList [0].volume -= Time.deltaTime * 0.9f;
		if (_audioSourceList [0].volume <= 0) {
			StopBGM ();
			if (!string.IsNullOrEmpty (_nextAudioName)) {
				PlayBGM (_nextAudioName);
			}
		}

	}

	/// <summary>
	/// ループ設定を変更
	/// </summary>
	public void ChangeLoop (bool isLoop)
	{
		_audioSourceList [0].loop = isLoop;
	}

	//=================================================================================
	//BGM再生
	//=================================================================================

	/// <summary>
	/// 指定したファイル名のBGMを流す
	/// </summary>
	public void PlayBGM (string bgmName)
	{
		return;
		
		if (string.IsNullOrEmpty (bgmName)) {
			Debug.LogWarning ("BGM名空です。");
			return;
		}
		if (!_audoClipDic.ContainsKey (bgmName)) {
			Debug.LogWarning (bgmName + "という名前のBGMがありません");
			return;
		}

		_lastBGMName = bgmName;

		if (_isMute) {
			return;
		}
		//既に指定のBGMが流れている時はスルー
		if (_audioSourceList [0].isPlaying && _audioSourceList [0].clip.name == bgmName) {
			return;
		}

		_isFadeOut = false;
		_audioSourceList [0].volume = _volumeDefult;

		_nextAudioName = "";
		_audioSourceList [0].clip = _audoClipDic [bgmName] as AudioClip;
		_audioSourceList [0].Play ();
	}

	//=================================================================================
	//停止
	//=================================================================================

	/// <summary>
	/// BGMを止める
	/// </summary>
	public void StopBGM ()
	{
		_audioSourceList [0].Stop ();
		_audioSourceList [0].volume = _volumeDefult;
		_isFadeOut = false;
	}

	/// <summary>
	/// 現在流れている曲をフェードアウトさせる
	/// </summary>
	public void FadeOutBGM (string nextBGMName = "")
	{
		_isFadeOut = true;
		_nextAudioName = nextBGMName;
	}

	/// <summary>
	/// 一時停止
	/// </summary>
	public void Pause ()
	{
		_audioSourceList [0].Pause ();
		_audioSourceList [0].volume = _volumeDefult;
		_isFadeOut = false;
	}

	/// <summary>
	/// 再開
	/// </summary>
	public void Resume ()
	{
		_audioSourceList [0].UnPause ();
	}

	//=================================================================================
	//ミュート関係
	//=================================================================================

	//ミュートのON、OFFを設定する
	protected override void SetIsMute (bool isMute)
	{
		base.SetIsMute (isMute);

		if (isMute) {
			StopBGM ();
		} else {
			PlayBGM (_lastBGMName);
		}
	}

	//=================================================================================
	//継承先で実装
	//=================================================================================
	protected override bool IsBGM ()
	{
		return true;
	}

	public void muteAll ()
	{
		base.muteAll ();
	}

	public void resumeAll ()
	{
		base.resumeAll ();
	}
}