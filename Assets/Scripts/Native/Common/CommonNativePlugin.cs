﻿//  CommonNativePlugin.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.27.

//モバイルの時だけMOBILE_DEVICEを宣言する
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL || UNITY_WEBPLAYER
#undef MOBILE_DEVICE
#else
#define MOBILE_DEVICE
#endif

// #undef MOBILE_DEVICE//lazycat cheat


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// 汎用的なネイティブプラグイン
/// </summary>
public class CommonNativePlugin : NativePlugin {

	//対応するAndroidのクラス名
	private const string ANDROID_CLASS_NAME = "CommonPlugin";

	#if UNITY_ANDROID
	public AndroidJavaObject AndroidPlugin{
		get{return _androidPlugin;}
	}
	#endif

	//=================================================================================
	//初期化
	//=================================================================================

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init(){
		#if UNITY_IOS && MOBILE_DEVICE
		base.Init (_CommonNativePlugin_Init, "");
		#elif UNITY_ANDROID && MOBILE_DEVICE
		base.Init (null, ANDROID_CLASS_NAME);
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern IntPtr _CommonNativePlugin_Init(string gameObjectName);
	#endif

	//=================================================================================
	// アプリ内でAppStoreを開く
	//=================================================================================

	/// <summary>
	/// アプリ内でAppStoreを開く
	/// </summary>
	public void ShowAppStoreInApp(int id){
		Debug.Log ("ShowAppStoreInApp");

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
		return;
		}
		else{
		_ShowAppStoreInApp(_nativeInstance, id);
		}
		#endif
	}

		#if UNITY_IOS
		[DllImport("__Internal")]
		private static extern void _ShowAppStoreInApp (IntPtr instance, int id);
		#endif

	//=================================================================================
	// Flurryにイベント送信
	//=================================================================================

	/// <summary>
	/// Flurryにイベント送信
	/// </summary>
	public void ReportToFlurry (string key){
		Debug.Log ("ReportToFlurry : " + key);

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
		return;
		}
		else{
		_ReportToFlurry(_nativeInstance, key);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("reportToFlurry", key);
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void _ReportToFlurry (IntPtr instance, string key);
	#endif

	//=================================================================================
	// ウェブページ表示
	//=================================================================================

	/// <summary>
	///  ウェブページ表示
	/// </summary>
	public void ShowCommonWeb(){
		Debug.Log ("ShowCommonWeb");

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
		return;
		}
		else{
		_ShowCommonWeb(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		if(Application.systemLanguage == SystemLanguage.Japanese){
			CommonNativeManager.Instance.CommonAndroidPlugin.Call("showCommonWebJPN");
		}
		else{
			CommonNativeManager.Instance.CommonAndroidPlugin.Call("showCommonWebUSA");
		}
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void _ShowCommonWeb (IntPtr instance);
	#endif

	//=================================================================================
	// レビュー催促を表示する
	//=================================================================================

	/// <summary>
	/// レビュー催促を表示する
	/// </summary>
	public void ShowReviewPopUp(){
		Debug.Log ("ShowReviewPopUp");

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
		return;
		}
		else{
		_ShowReviewPopUp(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("showReviewPopup");
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void _ShowReviewPopUp (IntPtr instance);
	#endif

	/// <summary>
	/// レビューページを表示する
	/// </summary>
	public void ShowReviewPage(){
		Debug.Log ("ShowReviewPage");

		#if UNITY_IOS && MOBILE_DEVICE
		if (_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowReviewPage(_nativeInstance);
		}
		#elif UNITY_ANDROID && MOBILE_DEVICE
			CommonNativeManager.Instance.CommonAndroidPlugin.Call("showReviewPage");
		#endif
	}

	#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void _ShowReviewPage (IntPtr instance);
	#endif
}
