﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class PlayerScript : MonoBehaviour
{
    public enum PlayerType
    {
        PlayerTypeLeft = -1,
        PlayerTypeRight = 1,
    }

    public enum PlayerState
    {
        PlayerStateStandA,
        PlayerStateForward,
        PlayerStateStandB,
        PlayerStateBackward,
    }

    public bool debugNoCollision;
    public delegate void PlayerDie(PlayerScript script);
    public static event PlayerDie OnPlayerDie;
    public PlayerType type;
    public float moveDistance;
    public float moveDuration;
    public PlayerState state;
    public Vector3 positionA;
    public Vector3 positionB;
    public float moveTime;
    public bool isDead;

    // [SerializeField] GameObject extraShader;
    Rigidbody _rigid;
    [SerializeField] TimelineSystem _startingTimline;
    // [SerializeField] ParticleSystem _smokeFX;
    int _touchId;
    // MeshRenderer _renderer;

    void Awake()
    {
        if (debugNoCollision)
        {
            GetComponent<Collider>().enabled = false;
            GetComponent<Rigidbody>().useGravity = false;
        }

        name = $"{Define.PLAYER_NAME}_{type}";
    }

    // Use this for initialization
    void Start()
    {
        _rigid = GetComponent<Rigidbody>();
        // _renderer = GetComponent<MeshRenderer>();

        if (GameManager.Instance.playMode != GameManager.PlayMode.PlayModeTitle)
        {
            if (_startingTimline != null)
                TimelineSystem.onTimelineCompleted += () =>
                {
                    positionA = this.transform.localPosition;
                    Transform parent = this.transform.parent;
                    positionB = parent.InverseTransformPoint(this.transform.position + (parent.right.normalized * moveDistance * (int)type));
                };
        }
        //for tutorial
        //		GameManager.Instance.OnChangeScore += checkTutorialEnd;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead || Time.timeScale == 0)
        {
            return;
        }

        if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeGame)
        {

            if (GameManager.Instance.IsPlayingGame)
            {

                //keep the player rigidbody awake for checking collision
                if (_rigid.IsSleeping())
                {
                    _rigid.WakeUp();
                }

                this.UpdateInput();
            }
        }
        else if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial)
        {

            if (GameManager.Instance.IsPlayingGame && GameManager.Instance._score >= 3)
            {

                if (_rigid.IsSleeping())
                {
                    _rigid.WakeUp();
                }

                this.UpdateInput();
            }
        }
    }

    void UpdateInput()
    {
        if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeTutorial)
            return;

        if (Input.touchCount > 0 /* && (UICamera.hoveredObject == null || (UICamera.hoveredObject != null && UICamera.hoveredObject.name != "ButtonPause")) */)
        {
            foreach (Touch touch in Input.touches)
            {
                // if (GamePlayScript.Instance.PauseCheck(touch.position))
                // {
                //     continue;
                // }
                this.CheckTouchPhase(touch);
                /*
				if (type == PlayerType.PlayerTypeLeft && touch.position.x < Screen.width * 0.5f) 
				{
					this.CheckTouchPhase (touch);
					break;
				} else if (type == PlayerType.PlayerTypeRight && touch.position.x > Screen.width * 0.5f) {
					this.CheckTouchPhase (touch);
					break;
				}*/
            }
        }

        if (Input.GetKeyDown(KeyCode.A) && type == PlayerType.PlayerTypeLeft)
        {
            state = PlayerState.PlayerStateForward;
            //sound
            SEManager.Instance.PlaySE(AudioName.SE_MOVE, 0, 1, 1);
        }
        else if (Input.GetKeyDown(KeyCode.D) && type == PlayerType.PlayerTypeRight)
        {
            state = PlayerState.PlayerStateForward;
            //sound
            SEManager.Instance.PlaySE(AudioName.SE_MOVE, 0, 1, 1);
        }

        if (Input.GetKeyUp(KeyCode.A) && type == PlayerType.PlayerTypeLeft)
        {
            state = PlayerState.PlayerStateBackward;
            //sound
            SEManager.Instance.PlaySE(AudioName.SE_MOVE, 0, 1, 1);
        }
        else if (Input.GetKeyUp(KeyCode.D) && type == PlayerType.PlayerTypeRight)
        {
            state = PlayerState.PlayerStateBackward;
            //sound
            SEManager.Instance.PlaySE(AudioName.SE_MOVE, 0, 1, 1);
        }

        this.MoveForward();
        this.MoveBackward();
    }

    void CheckTouchPhase(Touch touch)
    {
        if (touch.phase == TouchPhase.Began)
        {
            if (type == PlayerType.PlayerTypeLeft && touch.position.x < Screen.width * 0.5f)
                this.TouchBegan(touch);
            else if (type == PlayerType.PlayerTypeRight && touch.position.x > Screen.width * 0.5f)
                this.TouchBegan(touch);
        }
        else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
        {
            if (touch.fingerId == _touchId)
                this.TouchEnded(touch);
        }
    }

    void TouchBegan(Touch touch)
    {
        _touchId = touch.fingerId;
        state = PlayerState.PlayerStateForward;
        //sound
        SEManager.Instance.PlaySE(AudioName.SE_MOVE, 0, 1, 1);
    }

    void TouchEnded(Touch touch)
    {
        state = PlayerState.PlayerStateBackward;
        //sound
        SEManager.Instance.PlaySE(AudioName.SE_MOVE, 0, 1, 1);
    }

    public void MoveForward()
    {
        if (state == PlayerState.PlayerStateForward)
        {

            moveTime += Time.deltaTime;
            //check if move time larger move duration
            //set position to destination
            //else calculate the next position
            if (moveTime >= moveDuration)
            {
                state = PlayerState.PlayerStateStandB;
                moveTime = moveDuration;
                this.transform.localPosition = positionB;

                transform.forward = Vector3.forward;
            }
            else
            {
                //todo: test rotation
                transform.forward = Vector3.Lerp(transform.forward,
                    positionB + Vector3.forward * 1.2f - transform.localPosition, Time.deltaTime * 10f);

                this.CalculateMoveBetweenAB();
            }
        }
    }

    public void MoveBackward()
    {
        if (state == PlayerState.PlayerStateBackward)
        {

            moveTime -= Time.deltaTime;
            //check if move time larger move duration
            //set position to destination
            //else calculate the next position
            if (moveTime <= 0)
            {
                state = PlayerState.PlayerStateStandA;
                moveTime = 0;
                this.transform.localPosition = positionA;

                transform.forward = Vector3.forward;
            }
            else
            {
                transform.forward = Vector3.Lerp(transform.forward,
                   positionA + Vector3.forward * 1.2f - transform.localPosition, Time.deltaTime * 10f);

                this.CalculateMoveBetweenAB();
            }
        }
    }

    void CalculateMoveBetweenAB()
    {
        float posX = positionA.x + (Mathf.Abs(positionB.x - positionA.x) * (moveTime / moveDuration) * (int)type);
        this.transform.localPosition = new Vector3(posX, positionA.y, positionA.z);
    }

    void OnCollisionEnter(Collision col)
    {
        if (!isDead)
        {
            if (col.gameObject.name.Contains(Define.OBSTACLE_NAME))
            {
                //decrease environment moving speed
                EnvironmentController.Instance.StopMoving();

                //sound effect
                // SEManager.Instance.PlaySE(AudioName.SE_CRASH_2, 0, 1, 1);

                //play all die animation for 2 player
                PlayerScript[] players = this.transform.parent.GetComponentsInChildren<PlayerScript>();
                foreach (PlayerScript script in players)
                {
                    script.PlayerDieAnimation();
                }
                //decrease speed
                GamePlayScript.Instance.ScrollDelayEndAnimation();

                //shake camera
                GamePlayScript.Instance.ShakingCamera();

                GamePlayScript.Instance.StopVFX();

                if (Application.loadedLevelName.Contains("Tutorial"))
                {
                    //event player die
                    OnPlayerDie(this);
                }
            }
        }
    }



    public void PlayerDieAnimation()
    {
        isDead = true;

        //fx smoke
        // _smokeFX.Stop();

        //fx accident
        var fx = VFXSystem.Instance.SpawnFXAtPosition(VFXSystem.VFXCode.CarAccident, transform.position);
        fx.transform.SetParent(transform);

        // extraShader.gameObject.SetActive(false);

        // //show particles
        // ParticleSystem ps = this.transform.GetChild(0).GetComponent<ParticleSystem>();
        // ps.Play();

        // //hide the player
        // _renderer.enabled = false;

        // //hide shadow
        // this.transform.GetChild(1).gameObject.SetActive(false);
        // this.transform.GetChild(2).gameObject.SetActive(false);
    }
}
