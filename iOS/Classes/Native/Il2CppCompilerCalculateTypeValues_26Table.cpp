﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// ResultController
struct ResultController_t2106065388;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Collider
struct Collider_t1773347010;
// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>
struct List_1_t1313621403;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Collections.Generic.Dictionary`2<ShareLocation,System.String>
struct Dictionary_2_t3173579796;
// System.Collections.Generic.List`1<ExcelData/RowData>
struct List_1_t4163071842;
// UICamera/MouseOrTouch
struct MouseOrTouch_t3052596533;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Material
struct Material_t340375123;
// UIWidget
struct UIWidget_t3538521925;
// UICamera/Touch
struct Touch_t3749196807;
// UIPanel
struct UIPanel_t1716472341;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t4210400802;
// UILabel
struct UILabel_t3248798549;
// UIInput/OnValidate
struct OnValidate_t1246632601;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// UITexture
struct UITexture_t3471168817;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UICamera
struct UICamera_t1356438871;
// System.Action
struct Action_t1264377477;
// BMFont
struct BMFont_t2757936676;
// UIAtlas
struct UIAtlas_t3195533529;
// System.Collections.Generic.List`1<BMSymbol>
struct List_1_t3058133583;
// UnityEngine.Font
struct Font_t1956802104;
// UISpriteData
struct UISpriteData_t900308526;
// UIButton
struct UIButton_t1100396938;
// UISprite
struct UISprite_t194114938;
// Lean.Touch.LeanFingerSwipe/FingerEvent
struct FingerEvent_t2652301007;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image
struct Image_t2670269651;
// System.Collections.Generic.List`1<Lean.Touch.LeanTouch>
struct List_1_t128967781;
// System.Collections.Generic.List`1<Lean.Touch.LeanFinger>
struct List_1_t683400304;
// System.Action`1<Lean.Touch.LeanFinger>
struct Action_1_t3678760453;
// System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>
struct Action_1_t855867899;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>
struct List_1_t3650925511;
// Lean.Touch.LeanFinger
struct LeanFinger_t3506292858;
// Lean.Touch.LeanSelectable/LeanFingerEvent
struct LeanFingerEvent_t821904700;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UIRect/AnchorPoint
struct AnchorPoint_t1754718329;
// BetterList`1<UIRect>
struct BetterList_1_t2030980700;
// UIRoot
struct UIRoot_t4022971450;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UIWidget[]
struct UIWidgetU5BU5D_t2950248968;
// UIProgressBar
struct UIProgressBar_t1222110469;
// BetterList`1<UITextList/Paragraph>
struct BetterList_1_t266084037;
// System.Collections.Generic.Dictionary`2<System.String,BetterList`1<UITextList/Paragraph>>
struct Dictionary_2_t51340336;
// UnityEngine.Animation
struct Animation_t3648466861;
// System.Collections.Generic.List`1<UIRoot>
struct List_1_t1200078896;
// TweenLetters/AnimationProperties
struct AnimationProperties_t1280364982;
// TweenLetters/LetterProperties[]
struct LetterPropertiesU5BU5D_t3676037960;
// UIBasicSprite
struct UIBasicSprite_t1521297657;
// UIWidget/OnDimensionsChanged
struct OnDimensionsChanged_t3101921181;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t2835645043;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t133425655;
// UIWidget/HitCheck
struct HitCheck_t2300079615;
// UIGeometry
struct UIGeometry_t1059483952;
// UIDrawCall
struct UIDrawCall_t1293405319;
// System.Collections.Generic.List`1<UIPanel>
struct List_1_t3188547083;
// UIPanel/OnGeometryUpdated
struct OnGeometryUpdated_t2462438111;
// System.Collections.Generic.List`1<UIWidget>
struct List_1_t715629371;
// System.Collections.Generic.List`1<UIDrawCall>
struct List_1_t2765480061;
// UIPanel/OnClippingMoved
struct OnClippingMoved_t476625095;
// UIPanel/OnCreateMaterial
struct OnCreateMaterial_t2961246930;
// UIDrawCall/OnCreateDrawCall
struct OnCreateDrawCall_t609469653;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Comparison`1<UIPanel>
struct Comparison_1_t1491403520;
// System.Comparison`1<UIWidget>
struct Comparison_1_t3313453104;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UIFont
struct UIFont_t2766063701;
// UILabel/ModifierFunc
struct ModifierFunc_t2998065802;
// BetterList`1<UILabel>
struct BetterList_1_t2403818867;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Int32>
struct Dictionary_2_t2853457297;
// BetterList`1<UIDrawCall>
struct BetterList_1_t448425637;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Action`1<UnityEngine.Font>
struct Action_1_t2129269699;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Shader
struct Shader_t4151988712;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LAYERNO_T1223498628_H
#define LAYERNO_T1223498628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayerNo
struct  LayerNo_t1223498628  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERNO_T1223498628_H
#ifndef DIRECTORYPATH_T2513158077_H
#define DIRECTORYPATH_T2513158077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DirectoryPath
struct  DirectoryPath_t2513158077  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORYPATH_T2513158077_H
#ifndef UISPRITEDATA_T900308526_H
#define UISPRITEDATA_T900308526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISpriteData
struct  UISpriteData_t900308526  : public RuntimeObject
{
public:
	// System.String UISpriteData::name
	String_t* ___name_0;
	// System.Int32 UISpriteData::x
	int32_t ___x_1;
	// System.Int32 UISpriteData::y
	int32_t ___y_2;
	// System.Int32 UISpriteData::width
	int32_t ___width_3;
	// System.Int32 UISpriteData::height
	int32_t ___height_4;
	// System.Int32 UISpriteData::borderLeft
	int32_t ___borderLeft_5;
	// System.Int32 UISpriteData::borderRight
	int32_t ___borderRight_6;
	// System.Int32 UISpriteData::borderTop
	int32_t ___borderTop_7;
	// System.Int32 UISpriteData::borderBottom
	int32_t ___borderBottom_8;
	// System.Int32 UISpriteData::paddingLeft
	int32_t ___paddingLeft_9;
	// System.Int32 UISpriteData::paddingRight
	int32_t ___paddingRight_10;
	// System.Int32 UISpriteData::paddingTop
	int32_t ___paddingTop_11;
	// System.Int32 UISpriteData::paddingBottom
	int32_t ___paddingBottom_12;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___x_1)); }
	inline int32_t get_x_1() const { return ___x_1; }
	inline int32_t* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(int32_t value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___y_2)); }
	inline int32_t get_y_2() const { return ___y_2; }
	inline int32_t* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(int32_t value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_borderLeft_5() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___borderLeft_5)); }
	inline int32_t get_borderLeft_5() const { return ___borderLeft_5; }
	inline int32_t* get_address_of_borderLeft_5() { return &___borderLeft_5; }
	inline void set_borderLeft_5(int32_t value)
	{
		___borderLeft_5 = value;
	}

	inline static int32_t get_offset_of_borderRight_6() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___borderRight_6)); }
	inline int32_t get_borderRight_6() const { return ___borderRight_6; }
	inline int32_t* get_address_of_borderRight_6() { return &___borderRight_6; }
	inline void set_borderRight_6(int32_t value)
	{
		___borderRight_6 = value;
	}

	inline static int32_t get_offset_of_borderTop_7() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___borderTop_7)); }
	inline int32_t get_borderTop_7() const { return ___borderTop_7; }
	inline int32_t* get_address_of_borderTop_7() { return &___borderTop_7; }
	inline void set_borderTop_7(int32_t value)
	{
		___borderTop_7 = value;
	}

	inline static int32_t get_offset_of_borderBottom_8() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___borderBottom_8)); }
	inline int32_t get_borderBottom_8() const { return ___borderBottom_8; }
	inline int32_t* get_address_of_borderBottom_8() { return &___borderBottom_8; }
	inline void set_borderBottom_8(int32_t value)
	{
		___borderBottom_8 = value;
	}

	inline static int32_t get_offset_of_paddingLeft_9() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___paddingLeft_9)); }
	inline int32_t get_paddingLeft_9() const { return ___paddingLeft_9; }
	inline int32_t* get_address_of_paddingLeft_9() { return &___paddingLeft_9; }
	inline void set_paddingLeft_9(int32_t value)
	{
		___paddingLeft_9 = value;
	}

	inline static int32_t get_offset_of_paddingRight_10() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___paddingRight_10)); }
	inline int32_t get_paddingRight_10() const { return ___paddingRight_10; }
	inline int32_t* get_address_of_paddingRight_10() { return &___paddingRight_10; }
	inline void set_paddingRight_10(int32_t value)
	{
		___paddingRight_10 = value;
	}

	inline static int32_t get_offset_of_paddingTop_11() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___paddingTop_11)); }
	inline int32_t get_paddingTop_11() const { return ___paddingTop_11; }
	inline int32_t* get_address_of_paddingTop_11() { return &___paddingTop_11; }
	inline void set_paddingTop_11(int32_t value)
	{
		___paddingTop_11 = value;
	}

	inline static int32_t get_offset_of_paddingBottom_12() { return static_cast<int32_t>(offsetof(UISpriteData_t900308526, ___paddingBottom_12)); }
	inline int32_t get_paddingBottom_12() const { return ___paddingBottom_12; }
	inline int32_t* get_address_of_paddingBottom_12() { return &___paddingBottom_12; }
	inline void set_paddingBottom_12(int32_t value)
	{
		___paddingBottom_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISPRITEDATA_T900308526_H
#ifndef LAYERMASKNO_T2613727478_H
#define LAYERMASKNO_T2613727478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayerMaskNo
struct  LayerMaskNo_t2613727478  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASKNO_T2613727478_H
#ifndef LEANGESTURE_T1335127695_H
#define LEANGESTURE_T1335127695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanGesture
struct  LeanGesture_t1335127695  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANGESTURE_T1335127695_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SCENENAME_T250522944_H
#define SCENENAME_T250522944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneName
struct  SceneName_t250522944  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENENAME_T250522944_H
#ifndef SCENENO_T932058348_H
#define SCENENO_T932058348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneNo
struct  SceneNo_t932058348  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENENO_T932058348_H
#ifndef RESOURCESFILEPATH_T4283178319_H
#define RESOURCESFILEPATH_T4283178319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourcesFilePath
struct  ResourcesFilePath_t4283178319  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCESFILEPATH_T4283178319_H
#ifndef SORTINGLAYERNAME_T3039061313_H
#define SORTINGLAYERNAME_T3039061313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortingLayerName
struct  SortingLayerName_t3039061313  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYERNAME_T3039061313_H
#ifndef TAGNAME_T432652465_H
#define TAGNAME_T432652465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TagName
struct  TagName_t432652465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGNAME_T432652465_H
#ifndef DEFINE_T955668934_H
#define DEFINE_T955668934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Define
struct  Define_t955668934  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINE_T955668934_H
#ifndef PARAGRAPH_T1111063719_H
#define PARAGRAPH_T1111063719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITextList/Paragraph
struct  Paragraph_t1111063719  : public RuntimeObject
{
public:
	// System.String UITextList/Paragraph::text
	String_t* ___text_0;
	// System.String[] UITextList/Paragraph::lines
	StringU5BU5D_t1281789340* ___lines_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(Paragraph_t1111063719, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_lines_1() { return static_cast<int32_t>(offsetof(Paragraph_t1111063719, ___lines_1)); }
	inline StringU5BU5D_t1281789340* get_lines_1() const { return ___lines_1; }
	inline StringU5BU5D_t1281789340** get_address_of_lines_1() { return &___lines_1; }
	inline void set_lines_1(StringU5BU5D_t1281789340* value)
	{
		___lines_1 = value;
		Il2CppCodeGenWriteBarrier((&___lines_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAGRAPH_T1111063719_H
#ifndef RESOURCESDIRECTORYPATH_T218739485_H
#define RESOURCESDIRECTORYPATH_T218739485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourcesDirectoryPath
struct  ResourcesDirectoryPath_t218739485  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCESDIRECTORYPATH_T218739485_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3890121266_H
#define U3CSTARTU3EC__ITERATOR0_T3890121266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultController/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3890121266  : public RuntimeObject
{
public:
	// ResultController ResultController/<Start>c__Iterator0::$this
	ResultController_t2106065388 * ___U24this_0;
	// System.Object ResultController/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ResultController/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ResultController/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3890121266, ___U24this_0)); }
	inline ResultController_t2106065388 * get_U24this_0() const { return ___U24this_0; }
	inline ResultController_t2106065388 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ResultController_t2106065388 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3890121266, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3890121266, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3890121266, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3890121266_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef AUDIONAME_T948499878_H
#define AUDIONAME_T948499878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioName
struct  AudioName_t948499878  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIONAME_T948499878_H
#ifndef DEFINEVALUE_T901717641_H
#define DEFINEVALUE_T901717641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefineValue
struct  DefineValue_t901717641  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINEVALUE_T901717641_H
#ifndef ROWDATA_T2690997100_H
#define ROWDATA_T2690997100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExcelData/RowData
struct  RowData_t2690997100  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> ExcelData/RowData::DataList
	List_1_t3319525431 * ___DataList_0;

public:
	inline static int32_t get_offset_of_DataList_0() { return static_cast<int32_t>(offsetof(RowData_t2690997100, ___DataList_0)); }
	inline List_1_t3319525431 * get_DataList_0() const { return ___DataList_0; }
	inline List_1_t3319525431 ** get_address_of_DataList_0() { return &___DataList_0; }
	inline void set_DataList_0(List_1_t3319525431 * value)
	{
		___DataList_0 = value;
		Il2CppCodeGenWriteBarrier((&___DataList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROWDATA_T2690997100_H
#ifndef PLAYERSETTINGSVALUE_T3578457295_H
#define PLAYERSETTINGSVALUE_T3578457295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerSettingsValue
struct  PlayerSettingsValue_t3578457295  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSETTINGSVALUE_T3578457295_H
#ifndef RECYCLINGOBJECTNAME_T2466862849_H
#define RECYCLINGOBJECTNAME_T2466862849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecyclingObjectName
struct  RecyclingObjectName_t2466862849  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECYCLINGOBJECTNAME_T2466862849_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef UNITYEVENT_1_T92985066_H
#define UNITYEVENT_1_T92985066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>
struct  UnityEvent_1_t92985066  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t92985066, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T92985066_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef LETTERPROPERTIES_T3768168197_H
#define LETTERPROPERTIES_T3768168197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenLetters/LetterProperties
struct  LetterProperties_t3768168197  : public RuntimeObject
{
public:
	// System.Single TweenLetters/LetterProperties::start
	float ___start_0;
	// System.Single TweenLetters/LetterProperties::duration
	float ___duration_1;
	// UnityEngine.Vector2 TweenLetters/LetterProperties::offset
	Vector2_t2156229523  ___offset_2;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(LetterProperties_t3768168197, ___start_0)); }
	inline float get_start_0() const { return ___start_0; }
	inline float* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(float value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(LetterProperties_t3768168197, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(LetterProperties_t3768168197, ___offset_2)); }
	inline Vector2_t2156229523  get_offset_2() const { return ___offset_2; }
	inline Vector2_t2156229523 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Vector2_t2156229523  value)
	{
		___offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERPROPERTIES_T3768168197_H
#ifndef ANIMATIONLETTERORDER_T1458767740_H
#define ANIMATIONLETTERORDER_T1458767740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenLetters/AnimationLetterOrder
struct  AnimationLetterOrder_t1458767740 
{
public:
	// System.Int32 TweenLetters/AnimationLetterOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimationLetterOrder_t1458767740, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONLETTERORDER_T1458767740_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef LEANFINGEREVENT_T821904700_H
#define LEANFINGEREVENT_T821904700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectable/LeanFingerEvent
struct  LeanFingerEvent_t821904700  : public UnityEvent_1_t92985066
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGEREVENT_T821904700_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef GAMETYPE_T3504513020_H
#define GAMETYPE_T3504513020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameType
struct  GameType_t3504513020 
{
public:
	// System.Int32 GameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameType_t3504513020, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMETYPE_T3504513020_H
#ifndef FINGEREVENT_T2652301007_H
#define FINGEREVENT_T2652301007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerSwipe/FingerEvent
struct  FingerEvent_t2652301007  : public UnityEvent_1_t92985066
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGEREVENT_T2652301007_H
#ifndef LEANFINGER_T3506292858_H
#define LEANFINGER_T3506292858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFinger
struct  LeanFinger_t3506292858  : public RuntimeObject
{
public:
	// System.Int32 Lean.Touch.LeanFinger::Index
	int32_t ___Index_0;
	// System.Single Lean.Touch.LeanFinger::Age
	float ___Age_1;
	// System.Boolean Lean.Touch.LeanFinger::Set
	bool ___Set_2;
	// System.Boolean Lean.Touch.LeanFinger::LastSet
	bool ___LastSet_3;
	// System.Boolean Lean.Touch.LeanFinger::Tap
	bool ___Tap_4;
	// System.Int32 Lean.Touch.LeanFinger::TapCount
	int32_t ___TapCount_5;
	// System.Boolean Lean.Touch.LeanFinger::Swipe
	bool ___Swipe_6;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::StartScreenPosition
	Vector2_t2156229523  ___StartScreenPosition_7;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::LastScreenPosition
	Vector2_t2156229523  ___LastScreenPosition_8;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::ScreenPosition
	Vector2_t2156229523  ___ScreenPosition_9;
	// System.Boolean Lean.Touch.LeanFinger::StartedOverGui
	bool ___StartedOverGui_10;
	// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot> Lean.Touch.LeanFinger::Snapshots
	List_1_t1313621403 * ___Snapshots_11;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___Index_0)); }
	inline int32_t get_Index_0() const { return ___Index_0; }
	inline int32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(int32_t value)
	{
		___Index_0 = value;
	}

	inline static int32_t get_offset_of_Age_1() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___Age_1)); }
	inline float get_Age_1() const { return ___Age_1; }
	inline float* get_address_of_Age_1() { return &___Age_1; }
	inline void set_Age_1(float value)
	{
		___Age_1 = value;
	}

	inline static int32_t get_offset_of_Set_2() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___Set_2)); }
	inline bool get_Set_2() const { return ___Set_2; }
	inline bool* get_address_of_Set_2() { return &___Set_2; }
	inline void set_Set_2(bool value)
	{
		___Set_2 = value;
	}

	inline static int32_t get_offset_of_LastSet_3() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___LastSet_3)); }
	inline bool get_LastSet_3() const { return ___LastSet_3; }
	inline bool* get_address_of_LastSet_3() { return &___LastSet_3; }
	inline void set_LastSet_3(bool value)
	{
		___LastSet_3 = value;
	}

	inline static int32_t get_offset_of_Tap_4() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___Tap_4)); }
	inline bool get_Tap_4() const { return ___Tap_4; }
	inline bool* get_address_of_Tap_4() { return &___Tap_4; }
	inline void set_Tap_4(bool value)
	{
		___Tap_4 = value;
	}

	inline static int32_t get_offset_of_TapCount_5() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___TapCount_5)); }
	inline int32_t get_TapCount_5() const { return ___TapCount_5; }
	inline int32_t* get_address_of_TapCount_5() { return &___TapCount_5; }
	inline void set_TapCount_5(int32_t value)
	{
		___TapCount_5 = value;
	}

	inline static int32_t get_offset_of_Swipe_6() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___Swipe_6)); }
	inline bool get_Swipe_6() const { return ___Swipe_6; }
	inline bool* get_address_of_Swipe_6() { return &___Swipe_6; }
	inline void set_Swipe_6(bool value)
	{
		___Swipe_6 = value;
	}

	inline static int32_t get_offset_of_StartScreenPosition_7() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___StartScreenPosition_7)); }
	inline Vector2_t2156229523  get_StartScreenPosition_7() const { return ___StartScreenPosition_7; }
	inline Vector2_t2156229523 * get_address_of_StartScreenPosition_7() { return &___StartScreenPosition_7; }
	inline void set_StartScreenPosition_7(Vector2_t2156229523  value)
	{
		___StartScreenPosition_7 = value;
	}

	inline static int32_t get_offset_of_LastScreenPosition_8() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___LastScreenPosition_8)); }
	inline Vector2_t2156229523  get_LastScreenPosition_8() const { return ___LastScreenPosition_8; }
	inline Vector2_t2156229523 * get_address_of_LastScreenPosition_8() { return &___LastScreenPosition_8; }
	inline void set_LastScreenPosition_8(Vector2_t2156229523  value)
	{
		___LastScreenPosition_8 = value;
	}

	inline static int32_t get_offset_of_ScreenPosition_9() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___ScreenPosition_9)); }
	inline Vector2_t2156229523  get_ScreenPosition_9() const { return ___ScreenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_ScreenPosition_9() { return &___ScreenPosition_9; }
	inline void set_ScreenPosition_9(Vector2_t2156229523  value)
	{
		___ScreenPosition_9 = value;
	}

	inline static int32_t get_offset_of_StartedOverGui_10() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___StartedOverGui_10)); }
	inline bool get_StartedOverGui_10() const { return ___StartedOverGui_10; }
	inline bool* get_address_of_StartedOverGui_10() { return &___StartedOverGui_10; }
	inline void set_StartedOverGui_10(bool value)
	{
		___StartedOverGui_10 = value;
	}

	inline static int32_t get_offset_of_Snapshots_11() { return static_cast<int32_t>(offsetof(LeanFinger_t3506292858, ___Snapshots_11)); }
	inline List_1_t1313621403 * get_Snapshots_11() const { return ___Snapshots_11; }
	inline List_1_t1313621403 ** get_address_of_Snapshots_11() { return &___Snapshots_11; }
	inline void set_Snapshots_11(List_1_t1313621403 * value)
	{
		___Snapshots_11 = value;
		Il2CppCodeGenWriteBarrier((&___Snapshots_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGER_T3506292858_H
#ifndef LEANSNAPSHOT_T4136513957_H
#define LEANSNAPSHOT_T4136513957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSnapshot
struct  LeanSnapshot_t4136513957  : public RuntimeObject
{
public:
	// System.Single Lean.Touch.LeanSnapshot::Age
	float ___Age_0;
	// UnityEngine.Vector2 Lean.Touch.LeanSnapshot::ScreenPosition
	Vector2_t2156229523  ___ScreenPosition_1;

public:
	inline static int32_t get_offset_of_Age_0() { return static_cast<int32_t>(offsetof(LeanSnapshot_t4136513957, ___Age_0)); }
	inline float get_Age_0() const { return ___Age_0; }
	inline float* get_address_of_Age_0() { return &___Age_0; }
	inline void set_Age_0(float value)
	{
		___Age_0 = value;
	}

	inline static int32_t get_offset_of_ScreenPosition_1() { return static_cast<int32_t>(offsetof(LeanSnapshot_t4136513957, ___ScreenPosition_1)); }
	inline Vector2_t2156229523  get_ScreenPosition_1() const { return ___ScreenPosition_1; }
	inline Vector2_t2156229523 * get_address_of_ScreenPosition_1() { return &___ScreenPosition_1; }
	inline void set_ScreenPosition_1(Vector2_t2156229523  value)
	{
		___ScreenPosition_1 = value;
	}
};

struct LeanSnapshot_t4136513957_StaticFields
{
public:
	// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot> Lean.Touch.LeanSnapshot::InactiveSnapshots
	List_1_t1313621403 * ___InactiveSnapshots_2;

public:
	inline static int32_t get_offset_of_InactiveSnapshots_2() { return static_cast<int32_t>(offsetof(LeanSnapshot_t4136513957_StaticFields, ___InactiveSnapshots_2)); }
	inline List_1_t1313621403 * get_InactiveSnapshots_2() const { return ___InactiveSnapshots_2; }
	inline List_1_t1313621403 ** get_address_of_InactiveSnapshots_2() { return &___InactiveSnapshots_2; }
	inline void set_InactiveSnapshots_2(List_1_t1313621403 * value)
	{
		___InactiveSnapshots_2 = value;
		Il2CppCodeGenWriteBarrier((&___InactiveSnapshots_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSNAPSHOT_T4136513957_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef SCALING_T3068739704_H
#define SCALING_T3068739704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRoot/Scaling
struct  Scaling_t3068739704 
{
public:
	// System.Int32 UIRoot/Scaling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Scaling_t3068739704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALING_T3068739704_H
#ifndef FONTSTYLE_T82229486_H
#define FONTSTYLE_T82229486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t82229486 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyle_t82229486, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T82229486_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PROCESSEVENTSIN_T702674362_H
#define PROCESSEVENTSIN_T702674362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ProcessEventsIn
struct  ProcessEventsIn_t702674362 
{
public:
	// System.Int32 UICamera/ProcessEventsIn::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProcessEventsIn_t702674362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSEVENTSIN_T702674362_H
#ifndef STYLE_T3120619385_H
#define STYLE_T3120619385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Style
struct  Style_t3120619385 
{
public:
	// System.Int32 UITweener/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t3120619385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T3120619385_H
#ifndef METHOD_T1730494418_H
#define METHOD_T1730494418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener/Method
struct  Method_t1730494418 
{
public:
	// System.Int32 UITweener/Method::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Method_t1730494418, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T1730494418_H
#ifndef ADVANCEDTYPE_T3940519926_H
#define ADVANCEDTYPE_T3940519926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/AdvancedType
struct  AdvancedType_t3940519926 
{
public:
	// System.Int32 UIBasicSprite/AdvancedType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdvancedType_t3940519926, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDTYPE_T3940519926_H
#ifndef FLIP_T2552321477_H
#define FLIP_T2552321477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/Flip
struct  Flip_t2552321477 
{
public:
	// System.Int32 UIBasicSprite/Flip::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Flip_t2552321477, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLIP_T2552321477_H
#ifndef FILLDIRECTION_T904769527_H
#define FILLDIRECTION_T904769527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/FillDirection
struct  FillDirection_t904769527 
{
public:
	// System.Int32 UIBasicSprite/FillDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillDirection_t904769527, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLDIRECTION_T904769527_H
#ifndef TYPE_T4088629396_H
#define TYPE_T4088629396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite/Type
struct  Type_t4088629396 
{
public:
	// System.Int32 UIBasicSprite/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t4088629396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T4088629396_H
#ifndef ANCHORUPDATE_T1570184075_H
#define ANCHORUPDATE_T1570184075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect/AnchorUpdate
struct  AnchorUpdate_t1570184075 
{
public:
	// System.Int32 UIRect/AnchorUpdate::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnchorUpdate_t1570184075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORUPDATE_T1570184075_H
#ifndef ASPECTRATIOSOURCE_T168813522_H
#define ASPECTRATIOSOURCE_T168813522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/AspectRatioSource
struct  AspectRatioSource_t168813522 
{
public:
	// System.Int32 UIWidget/AspectRatioSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectRatioSource_t168813522, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOSOURCE_T168813522_H
#ifndef PIVOT_T1798046373_H
#define PIVOT_T1798046373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/Pivot
struct  Pivot_t1798046373 
{
public:
	// System.Int32 UIWidget/Pivot::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pivot_t1798046373, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOT_T1798046373_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ICONTYPE_T1006196312_H
#define ICONTYPE_T1006196312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdRegionViewer/IconType
struct  IconType_t1006196312 
{
public:
	// System.Int32 AdRegionViewer/IconType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IconType_t1006196312, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONTYPE_T1006196312_H
#ifndef RECTANGLETYPE_T3748261860_H
#define RECTANGLETYPE_T3748261860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdRegionViewer/RectangleType
struct  RectangleType_t3748261860 
{
public:
	// System.Int32 AdRegionViewer/RectangleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RectangleType_t3748261860, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLETYPE_T3748261860_H
#ifndef INPUTTYPE_T3084801673_H
#define INPUTTYPE_T3084801673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/InputType
struct  InputType_t3084801673 
{
public:
	// System.Int32 UIInput/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t3084801673, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T3084801673_H
#ifndef VALIDATION_T3618213463_H
#define VALIDATION_T3618213463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/Validation
struct  Validation_t3618213463 
{
public:
	// System.Int32 UIInput/Validation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Validation_t3618213463, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATION_T3618213463_H
#ifndef KEYBOARDTYPE_T3374647619_H
#define KEYBOARDTYPE_T3374647619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/KeyboardType
struct  KeyboardType_t3374647619 
{
public:
	// System.Int32 UIInput/KeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyboardType_t3374647619, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDTYPE_T3374647619_H
#ifndef ONRETURNKEY_T3803455137_H
#define ONRETURNKEY_T3803455137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/OnReturnKey
struct  OnReturnKey_t3803455137 
{
public:
	// System.Int32 UIInput/OnReturnKey::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnReturnKey_t3803455137, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRETURNKEY_T3803455137_H
#ifndef BANNERTYPE_T3341813445_H
#define BANNERTYPE_T3341813445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdRegionViewer/BannerType
struct  BannerType_t3341813445 
{
public:
	// System.Int32 AdRegionViewer/BannerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BannerType_t3341813445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERTYPE_T3341813445_H
#ifndef EFFECT_T2533209744_H
#define EFFECT_T2533209744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Effect
struct  Effect_t2533209744 
{
public:
	// System.Int32 UILabel/Effect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Effect_t2533209744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECT_T2533209744_H
#ifndef OVERFLOW_T884389639_H
#define OVERFLOW_T884389639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Overflow
struct  Overflow_t884389639 
{
public:
	// System.Int32 UILabel/Overflow::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Overflow_t884389639, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERFLOW_T884389639_H
#ifndef CRISPNESS_T2029465680_H
#define CRISPNESS_T2029465680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Crispness
struct  Crispness_t2029465680 
{
public:
	// System.Int32 UILabel/Crispness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Crispness_t2029465680, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRISPNESS_T2029465680_H
#ifndef MODIFIER_T3840764400_H
#define MODIFIER_T3840764400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/Modifier
struct  Modifier_t3840764400 
{
public:
	// System.Int32 UILabel/Modifier::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Modifier_t3840764400, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIER_T3840764400_H
#ifndef CLIPPING_T1109313910_H
#define CLIPPING_T1109313910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/Clipping
struct  Clipping_t1109313910 
{
public:
	// System.Int32 UIDrawCall/Clipping::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Clipping_t1109313910, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_T1109313910_H
#ifndef RENDERQUEUE_T2721716586_H
#define RENDERQUEUE_T2721716586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel/RenderQueue
struct  RenderQueue_t2721716586 
{
public:
	// System.Int32 UIPanel/RenderQueue::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderQueue_t2721716586, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERQUEUE_T2721716586_H
#ifndef SHADOWMODE_T1455171354_H
#define SHADOWMODE_T1455171354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall/ShadowMode
struct  ShadowMode_t1455171354 
{
public:
	// System.Int32 UIDrawCall/ShadowMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShadowMode_t1455171354, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWMODE_T1455171354_H
#ifndef SYMBOLSTYLE_T3792107337_H
#define SYMBOLSTYLE_T3792107337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/SymbolStyle
struct  SymbolStyle_t3792107337 
{
public:
	// System.Int32 NGUIText/SymbolStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SymbolStyle_t3792107337, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSTYLE_T3792107337_H
#ifndef CONSTRAINT_T400694485_H
#define CONSTRAINT_T400694485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRoot/Constraint
struct  Constraint_t400694485 
{
public:
	// System.Int32 UIRoot/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t400694485, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T400694485_H
#ifndef ALIGNMENT_T3228070485_H
#define ALIGNMENT_T3228070485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIText/Alignment
struct  Alignment_t3228070485 
{
public:
	// System.Int32 NGUIText/Alignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Alignment_t3228070485, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T3228070485_H
#ifndef STYLE_T3106380496_H
#define STYLE_T3106380496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITextList/Style
struct  Style_t3106380496 
{
public:
	// System.Int32 UITextList/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t3106380496, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T3106380496_H
#ifndef STYLE_T3184300279_H
#define STYLE_T3184300279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIStretch/Style
struct  Style_t3184300279 
{
public:
	// System.Int32 UIStretch/Style::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Style_t3184300279, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T3184300279_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ANIMATIONPROPERTIES_T1280364982_H
#define ANIMATIONPROPERTIES_T1280364982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenLetters/AnimationProperties
struct  AnimationProperties_t1280364982  : public RuntimeObject
{
public:
	// TweenLetters/AnimationLetterOrder TweenLetters/AnimationProperties::animationOrder
	int32_t ___animationOrder_0;
	// System.Single TweenLetters/AnimationProperties::overlap
	float ___overlap_1;
	// System.Boolean TweenLetters/AnimationProperties::randomDurations
	bool ___randomDurations_2;
	// UnityEngine.Vector2 TweenLetters/AnimationProperties::randomness
	Vector2_t2156229523  ___randomness_3;
	// UnityEngine.Vector2 TweenLetters/AnimationProperties::offsetRange
	Vector2_t2156229523  ___offsetRange_4;
	// UnityEngine.Vector3 TweenLetters/AnimationProperties::pos
	Vector3_t3722313464  ___pos_5;
	// UnityEngine.Vector3 TweenLetters/AnimationProperties::rot
	Vector3_t3722313464  ___rot_6;
	// UnityEngine.Vector3 TweenLetters/AnimationProperties::scale
	Vector3_t3722313464  ___scale_7;
	// System.Single TweenLetters/AnimationProperties::alpha
	float ___alpha_8;

public:
	inline static int32_t get_offset_of_animationOrder_0() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___animationOrder_0)); }
	inline int32_t get_animationOrder_0() const { return ___animationOrder_0; }
	inline int32_t* get_address_of_animationOrder_0() { return &___animationOrder_0; }
	inline void set_animationOrder_0(int32_t value)
	{
		___animationOrder_0 = value;
	}

	inline static int32_t get_offset_of_overlap_1() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___overlap_1)); }
	inline float get_overlap_1() const { return ___overlap_1; }
	inline float* get_address_of_overlap_1() { return &___overlap_1; }
	inline void set_overlap_1(float value)
	{
		___overlap_1 = value;
	}

	inline static int32_t get_offset_of_randomDurations_2() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___randomDurations_2)); }
	inline bool get_randomDurations_2() const { return ___randomDurations_2; }
	inline bool* get_address_of_randomDurations_2() { return &___randomDurations_2; }
	inline void set_randomDurations_2(bool value)
	{
		___randomDurations_2 = value;
	}

	inline static int32_t get_offset_of_randomness_3() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___randomness_3)); }
	inline Vector2_t2156229523  get_randomness_3() const { return ___randomness_3; }
	inline Vector2_t2156229523 * get_address_of_randomness_3() { return &___randomness_3; }
	inline void set_randomness_3(Vector2_t2156229523  value)
	{
		___randomness_3 = value;
	}

	inline static int32_t get_offset_of_offsetRange_4() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___offsetRange_4)); }
	inline Vector2_t2156229523  get_offsetRange_4() const { return ___offsetRange_4; }
	inline Vector2_t2156229523 * get_address_of_offsetRange_4() { return &___offsetRange_4; }
	inline void set_offsetRange_4(Vector2_t2156229523  value)
	{
		___offsetRange_4 = value;
	}

	inline static int32_t get_offset_of_pos_5() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___pos_5)); }
	inline Vector3_t3722313464  get_pos_5() const { return ___pos_5; }
	inline Vector3_t3722313464 * get_address_of_pos_5() { return &___pos_5; }
	inline void set_pos_5(Vector3_t3722313464  value)
	{
		___pos_5 = value;
	}

	inline static int32_t get_offset_of_rot_6() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___rot_6)); }
	inline Vector3_t3722313464  get_rot_6() const { return ___rot_6; }
	inline Vector3_t3722313464 * get_address_of_rot_6() { return &___rot_6; }
	inline void set_rot_6(Vector3_t3722313464  value)
	{
		___rot_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___scale_7)); }
	inline Vector3_t3722313464  get_scale_7() const { return ___scale_7; }
	inline Vector3_t3722313464 * get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(Vector3_t3722313464  value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_alpha_8() { return static_cast<int32_t>(offsetof(AnimationProperties_t1280364982, ___alpha_8)); }
	inline float get_alpha_8() const { return ___alpha_8; }
	inline float* get_address_of_alpha_8() { return &___alpha_8; }
	inline void set_alpha_8(float value)
	{
		___alpha_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPROPERTIES_T1280364982_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef TOUCH_T3749196807_H
#define TOUCH_T3749196807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/Touch
struct  Touch_t3749196807  : public RuntimeObject
{
public:
	// System.Int32 UICamera/Touch::fingerId
	int32_t ___fingerId_0;
	// UnityEngine.TouchPhase UICamera/Touch::phase
	int32_t ___phase_1;
	// UnityEngine.Vector2 UICamera/Touch::position
	Vector2_t2156229523  ___position_2;
	// System.Int32 UICamera/Touch::tapCount
	int32_t ___tapCount_3;

public:
	inline static int32_t get_offset_of_fingerId_0() { return static_cast<int32_t>(offsetof(Touch_t3749196807, ___fingerId_0)); }
	inline int32_t get_fingerId_0() const { return ___fingerId_0; }
	inline int32_t* get_address_of_fingerId_0() { return &___fingerId_0; }
	inline void set_fingerId_0(int32_t value)
	{
		___fingerId_0 = value;
	}

	inline static int32_t get_offset_of_phase_1() { return static_cast<int32_t>(offsetof(Touch_t3749196807, ___phase_1)); }
	inline int32_t get_phase_1() const { return ___phase_1; }
	inline int32_t* get_address_of_phase_1() { return &___phase_1; }
	inline void set_phase_1(int32_t value)
	{
		___phase_1 = value;
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(Touch_t3749196807, ___position_2)); }
	inline Vector2_t2156229523  get_position_2() const { return ___position_2; }
	inline Vector2_t2156229523 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector2_t2156229523  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_tapCount_3() { return static_cast<int32_t>(offsetof(Touch_t3749196807, ___tapCount_3)); }
	inline int32_t get_tapCount_3() const { return ___tapCount_3; }
	inline int32_t* get_address_of_tapCount_3() { return &___tapCount_3; }
	inline void set_tapCount_3(int32_t value)
	{
		___tapCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T3749196807_H
#ifndef DEPTHENTRY_T628749918_H
#define DEPTHENTRY_T628749918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/DepthEntry
struct  DepthEntry_t628749918 
{
public:
	// System.Int32 UICamera/DepthEntry::depth
	int32_t ___depth_0;
	// UnityEngine.RaycastHit UICamera/DepthEntry::hit
	RaycastHit_t1056001966  ___hit_1;
	// UnityEngine.Vector3 UICamera/DepthEntry::point
	Vector3_t3722313464  ___point_2;
	// UnityEngine.GameObject UICamera/DepthEntry::go
	GameObject_t1113636619 * ___go_3;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_hit_1() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___hit_1)); }
	inline RaycastHit_t1056001966  get_hit_1() const { return ___hit_1; }
	inline RaycastHit_t1056001966 * get_address_of_hit_1() { return &___hit_1; }
	inline void set_hit_1(RaycastHit_t1056001966  value)
	{
		___hit_1 = value;
	}

	inline static int32_t get_offset_of_point_2() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___point_2)); }
	inline Vector3_t3722313464  get_point_2() const { return ___point_2; }
	inline Vector3_t3722313464 * get_address_of_point_2() { return &___point_2; }
	inline void set_point_2(Vector3_t3722313464  value)
	{
		___point_2 = value;
	}

	inline static int32_t get_offset_of_go_3() { return static_cast<int32_t>(offsetof(DepthEntry_t628749918, ___go_3)); }
	inline GameObject_t1113636619 * get_go_3() const { return ___go_3; }
	inline GameObject_t1113636619 ** get_address_of_go_3() { return &___go_3; }
	inline void set_go_3(GameObject_t1113636619 * value)
	{
		___go_3 = value;
		Il2CppCodeGenWriteBarrier((&___go_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UICamera/DepthEntry
struct DepthEntry_t628749918_marshaled_pinvoke
{
	int32_t ___depth_0;
	RaycastHit_t1056001966_marshaled_pinvoke ___hit_1;
	Vector3_t3722313464  ___point_2;
	GameObject_t1113636619 * ___go_3;
};
// Native definition for COM marshalling of UICamera/DepthEntry
struct DepthEntry_t628749918_marshaled_com
{
	int32_t ___depth_0;
	RaycastHit_t1056001966_marshaled_com ___hit_1;
	Vector3_t3722313464  ___point_2;
	GameObject_t1113636619 * ___go_3;
};
#endif // DEPTHENTRY_T628749918_H
#ifndef USERDATA_T2822117362_H
#define USERDATA_T2822117362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserData
struct  UserData_t2822117362  : public RuntimeObject
{
public:

public:
};

struct UserData_t2822117362_StaticFields
{
public:
	// System.Boolean UserData::_isInitializedPlayCount
	bool ____isInitializedPlayCount_1;
	// System.Int32 UserData::_playCount
	int32_t ____playCount_2;
	// System.Boolean UserData::_isInitializedHighScoreDict
	bool ____isInitializedHighScoreDict_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UserData::_highScoreDict
	Dictionary_2_t2736202052 * ____highScoreDict_5;
	// System.Boolean UserData::_isInitializedMuteSetting
	bool ____isInitializedMuteSetting_7;
	// System.Boolean UserData::_isMute
	bool ____isMute_8;
	// System.Action`1<System.Boolean> UserData::OnChangeMuteSetting
	Action_1_t269755560 * ___OnChangeMuteSetting_9;
	// System.Boolean UserData::_isInitFirstTimeOpenApp
	bool ____isInitFirstTimeOpenApp_11;
	// System.Boolean UserData::_isFirstTimeOpenApp
	bool ____isFirstTimeOpenApp_12;
	// System.Action`1<System.Boolean> UserData::OnChangeFirstTime
	Action_1_t269755560 * ___OnChangeFirstTime_13;
	// System.Boolean UserData::_isInitializedTimeOfPushedHouseAdButton
	bool ____isInitializedTimeOfPushedHouseAdButton_15;
	// System.DateTime UserData::_timeOfPushedHouseAdButton
	DateTime_t3738529785  ____timeOfPushedHouseAdButton_16;

public:
	inline static int32_t get_offset_of__isInitializedPlayCount_1() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedPlayCount_1)); }
	inline bool get__isInitializedPlayCount_1() const { return ____isInitializedPlayCount_1; }
	inline bool* get_address_of__isInitializedPlayCount_1() { return &____isInitializedPlayCount_1; }
	inline void set__isInitializedPlayCount_1(bool value)
	{
		____isInitializedPlayCount_1 = value;
	}

	inline static int32_t get_offset_of__playCount_2() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____playCount_2)); }
	inline int32_t get__playCount_2() const { return ____playCount_2; }
	inline int32_t* get_address_of__playCount_2() { return &____playCount_2; }
	inline void set__playCount_2(int32_t value)
	{
		____playCount_2 = value;
	}

	inline static int32_t get_offset_of__isInitializedHighScoreDict_4() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedHighScoreDict_4)); }
	inline bool get__isInitializedHighScoreDict_4() const { return ____isInitializedHighScoreDict_4; }
	inline bool* get_address_of__isInitializedHighScoreDict_4() { return &____isInitializedHighScoreDict_4; }
	inline void set__isInitializedHighScoreDict_4(bool value)
	{
		____isInitializedHighScoreDict_4 = value;
	}

	inline static int32_t get_offset_of__highScoreDict_5() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____highScoreDict_5)); }
	inline Dictionary_2_t2736202052 * get__highScoreDict_5() const { return ____highScoreDict_5; }
	inline Dictionary_2_t2736202052 ** get_address_of__highScoreDict_5() { return &____highScoreDict_5; }
	inline void set__highScoreDict_5(Dictionary_2_t2736202052 * value)
	{
		____highScoreDict_5 = value;
		Il2CppCodeGenWriteBarrier((&____highScoreDict_5), value);
	}

	inline static int32_t get_offset_of__isInitializedMuteSetting_7() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedMuteSetting_7)); }
	inline bool get__isInitializedMuteSetting_7() const { return ____isInitializedMuteSetting_7; }
	inline bool* get_address_of__isInitializedMuteSetting_7() { return &____isInitializedMuteSetting_7; }
	inline void set__isInitializedMuteSetting_7(bool value)
	{
		____isInitializedMuteSetting_7 = value;
	}

	inline static int32_t get_offset_of__isMute_8() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isMute_8)); }
	inline bool get__isMute_8() const { return ____isMute_8; }
	inline bool* get_address_of__isMute_8() { return &____isMute_8; }
	inline void set__isMute_8(bool value)
	{
		____isMute_8 = value;
	}

	inline static int32_t get_offset_of_OnChangeMuteSetting_9() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ___OnChangeMuteSetting_9)); }
	inline Action_1_t269755560 * get_OnChangeMuteSetting_9() const { return ___OnChangeMuteSetting_9; }
	inline Action_1_t269755560 ** get_address_of_OnChangeMuteSetting_9() { return &___OnChangeMuteSetting_9; }
	inline void set_OnChangeMuteSetting_9(Action_1_t269755560 * value)
	{
		___OnChangeMuteSetting_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnChangeMuteSetting_9), value);
	}

	inline static int32_t get_offset_of__isInitFirstTimeOpenApp_11() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitFirstTimeOpenApp_11)); }
	inline bool get__isInitFirstTimeOpenApp_11() const { return ____isInitFirstTimeOpenApp_11; }
	inline bool* get_address_of__isInitFirstTimeOpenApp_11() { return &____isInitFirstTimeOpenApp_11; }
	inline void set__isInitFirstTimeOpenApp_11(bool value)
	{
		____isInitFirstTimeOpenApp_11 = value;
	}

	inline static int32_t get_offset_of__isFirstTimeOpenApp_12() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isFirstTimeOpenApp_12)); }
	inline bool get__isFirstTimeOpenApp_12() const { return ____isFirstTimeOpenApp_12; }
	inline bool* get_address_of__isFirstTimeOpenApp_12() { return &____isFirstTimeOpenApp_12; }
	inline void set__isFirstTimeOpenApp_12(bool value)
	{
		____isFirstTimeOpenApp_12 = value;
	}

	inline static int32_t get_offset_of_OnChangeFirstTime_13() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ___OnChangeFirstTime_13)); }
	inline Action_1_t269755560 * get_OnChangeFirstTime_13() const { return ___OnChangeFirstTime_13; }
	inline Action_1_t269755560 ** get_address_of_OnChangeFirstTime_13() { return &___OnChangeFirstTime_13; }
	inline void set_OnChangeFirstTime_13(Action_1_t269755560 * value)
	{
		___OnChangeFirstTime_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnChangeFirstTime_13), value);
	}

	inline static int32_t get_offset_of__isInitializedTimeOfPushedHouseAdButton_15() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____isInitializedTimeOfPushedHouseAdButton_15)); }
	inline bool get__isInitializedTimeOfPushedHouseAdButton_15() const { return ____isInitializedTimeOfPushedHouseAdButton_15; }
	inline bool* get_address_of__isInitializedTimeOfPushedHouseAdButton_15() { return &____isInitializedTimeOfPushedHouseAdButton_15; }
	inline void set__isInitializedTimeOfPushedHouseAdButton_15(bool value)
	{
		____isInitializedTimeOfPushedHouseAdButton_15 = value;
	}

	inline static int32_t get_offset_of__timeOfPushedHouseAdButton_16() { return static_cast<int32_t>(offsetof(UserData_t2822117362_StaticFields, ____timeOfPushedHouseAdButton_16)); }
	inline DateTime_t3738529785  get__timeOfPushedHouseAdButton_16() const { return ____timeOfPushedHouseAdButton_16; }
	inline DateTime_t3738529785 * get_address_of__timeOfPushedHouseAdButton_16() { return &____timeOfPushedHouseAdButton_16; }
	inline void set__timeOfPushedHouseAdButton_16(DateTime_t3738529785  value)
	{
		____timeOfPushedHouseAdButton_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDATA_T2822117362_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UNITYPROJECTSETTING_T2579824760_H
#define UNITYPROJECTSETTING_T2579824760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityProjectSetting
struct  UnityProjectSetting_t2579824760  : public ScriptableObject_t2528358522
{
public:
	// GameType UnityProjectSetting::_gameType
	int32_t ____gameType_4;
	// System.Int32 UnityProjectSetting::StageNum
	int32_t ___StageNum_5;
	// System.Int32 UnityProjectSetting::_loadStageNo
	int32_t ____loadStageNo_6;
	// System.String UnityProjectSetting::_shareFormatInTitle
	String_t* ____shareFormatInTitle_7;
	// System.String UnityProjectSetting::_shareFormatInClear
	String_t* ____shareFormatInClear_8;
	// System.String UnityProjectSetting::_shareFormatInMiss
	String_t* ____shareFormatInMiss_9;
	// System.Collections.Generic.Dictionary`2<ShareLocation,System.String> UnityProjectSetting::_shareFormatDict
	Dictionary_2_t3173579796 * ____shareFormatDict_10;
	// System.String UnityProjectSetting::_shareFormatInTitleEnglish
	String_t* ____shareFormatInTitleEnglish_11;
	// System.String UnityProjectSetting::_shareFormatInClearEnglish
	String_t* ____shareFormatInClearEnglish_12;
	// System.String UnityProjectSetting::_shareFormatInMissEnglish
	String_t* ____shareFormatInMissEnglish_13;
	// System.Collections.Generic.Dictionary`2<ShareLocation,System.String> UnityProjectSetting::_shareFormatDictEnglish
	Dictionary_2_t3173579796 * ____shareFormatDictEnglish_14;
	// System.String UnityProjectSetting::_tag
	String_t* ____tag_15;
	// System.String UnityProjectSetting::_iosStoreURL
	String_t* ____iosStoreURL_16;
	// System.String UnityProjectSetting::_androidStoreURL
	String_t* ____androidStoreURL_17;

public:
	inline static int32_t get_offset_of__gameType_4() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____gameType_4)); }
	inline int32_t get__gameType_4() const { return ____gameType_4; }
	inline int32_t* get_address_of__gameType_4() { return &____gameType_4; }
	inline void set__gameType_4(int32_t value)
	{
		____gameType_4 = value;
	}

	inline static int32_t get_offset_of_StageNum_5() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ___StageNum_5)); }
	inline int32_t get_StageNum_5() const { return ___StageNum_5; }
	inline int32_t* get_address_of_StageNum_5() { return &___StageNum_5; }
	inline void set_StageNum_5(int32_t value)
	{
		___StageNum_5 = value;
	}

	inline static int32_t get_offset_of__loadStageNo_6() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____loadStageNo_6)); }
	inline int32_t get__loadStageNo_6() const { return ____loadStageNo_6; }
	inline int32_t* get_address_of__loadStageNo_6() { return &____loadStageNo_6; }
	inline void set__loadStageNo_6(int32_t value)
	{
		____loadStageNo_6 = value;
	}

	inline static int32_t get_offset_of__shareFormatInTitle_7() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInTitle_7)); }
	inline String_t* get__shareFormatInTitle_7() const { return ____shareFormatInTitle_7; }
	inline String_t** get_address_of__shareFormatInTitle_7() { return &____shareFormatInTitle_7; }
	inline void set__shareFormatInTitle_7(String_t* value)
	{
		____shareFormatInTitle_7 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInTitle_7), value);
	}

	inline static int32_t get_offset_of__shareFormatInClear_8() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInClear_8)); }
	inline String_t* get__shareFormatInClear_8() const { return ____shareFormatInClear_8; }
	inline String_t** get_address_of__shareFormatInClear_8() { return &____shareFormatInClear_8; }
	inline void set__shareFormatInClear_8(String_t* value)
	{
		____shareFormatInClear_8 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInClear_8), value);
	}

	inline static int32_t get_offset_of__shareFormatInMiss_9() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInMiss_9)); }
	inline String_t* get__shareFormatInMiss_9() const { return ____shareFormatInMiss_9; }
	inline String_t** get_address_of__shareFormatInMiss_9() { return &____shareFormatInMiss_9; }
	inline void set__shareFormatInMiss_9(String_t* value)
	{
		____shareFormatInMiss_9 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInMiss_9), value);
	}

	inline static int32_t get_offset_of__shareFormatDict_10() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatDict_10)); }
	inline Dictionary_2_t3173579796 * get__shareFormatDict_10() const { return ____shareFormatDict_10; }
	inline Dictionary_2_t3173579796 ** get_address_of__shareFormatDict_10() { return &____shareFormatDict_10; }
	inline void set__shareFormatDict_10(Dictionary_2_t3173579796 * value)
	{
		____shareFormatDict_10 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatDict_10), value);
	}

	inline static int32_t get_offset_of__shareFormatInTitleEnglish_11() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInTitleEnglish_11)); }
	inline String_t* get__shareFormatInTitleEnglish_11() const { return ____shareFormatInTitleEnglish_11; }
	inline String_t** get_address_of__shareFormatInTitleEnglish_11() { return &____shareFormatInTitleEnglish_11; }
	inline void set__shareFormatInTitleEnglish_11(String_t* value)
	{
		____shareFormatInTitleEnglish_11 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInTitleEnglish_11), value);
	}

	inline static int32_t get_offset_of__shareFormatInClearEnglish_12() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInClearEnglish_12)); }
	inline String_t* get__shareFormatInClearEnglish_12() const { return ____shareFormatInClearEnglish_12; }
	inline String_t** get_address_of__shareFormatInClearEnglish_12() { return &____shareFormatInClearEnglish_12; }
	inline void set__shareFormatInClearEnglish_12(String_t* value)
	{
		____shareFormatInClearEnglish_12 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInClearEnglish_12), value);
	}

	inline static int32_t get_offset_of__shareFormatInMissEnglish_13() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatInMissEnglish_13)); }
	inline String_t* get__shareFormatInMissEnglish_13() const { return ____shareFormatInMissEnglish_13; }
	inline String_t** get_address_of__shareFormatInMissEnglish_13() { return &____shareFormatInMissEnglish_13; }
	inline void set__shareFormatInMissEnglish_13(String_t* value)
	{
		____shareFormatInMissEnglish_13 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatInMissEnglish_13), value);
	}

	inline static int32_t get_offset_of__shareFormatDictEnglish_14() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____shareFormatDictEnglish_14)); }
	inline Dictionary_2_t3173579796 * get__shareFormatDictEnglish_14() const { return ____shareFormatDictEnglish_14; }
	inline Dictionary_2_t3173579796 ** get_address_of__shareFormatDictEnglish_14() { return &____shareFormatDictEnglish_14; }
	inline void set__shareFormatDictEnglish_14(Dictionary_2_t3173579796 * value)
	{
		____shareFormatDictEnglish_14 = value;
		Il2CppCodeGenWriteBarrier((&____shareFormatDictEnglish_14), value);
	}

	inline static int32_t get_offset_of__tag_15() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____tag_15)); }
	inline String_t* get__tag_15() const { return ____tag_15; }
	inline String_t** get_address_of__tag_15() { return &____tag_15; }
	inline void set__tag_15(String_t* value)
	{
		____tag_15 = value;
		Il2CppCodeGenWriteBarrier((&____tag_15), value);
	}

	inline static int32_t get_offset_of__iosStoreURL_16() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____iosStoreURL_16)); }
	inline String_t* get__iosStoreURL_16() const { return ____iosStoreURL_16; }
	inline String_t** get_address_of__iosStoreURL_16() { return &____iosStoreURL_16; }
	inline void set__iosStoreURL_16(String_t* value)
	{
		____iosStoreURL_16 = value;
		Il2CppCodeGenWriteBarrier((&____iosStoreURL_16), value);
	}

	inline static int32_t get_offset_of__androidStoreURL_17() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760, ____androidStoreURL_17)); }
	inline String_t* get__androidStoreURL_17() const { return ____androidStoreURL_17; }
	inline String_t** get_address_of__androidStoreURL_17() { return &____androidStoreURL_17; }
	inline void set__androidStoreURL_17(String_t* value)
	{
		____androidStoreURL_17 = value;
		Il2CppCodeGenWriteBarrier((&____androidStoreURL_17), value);
	}
};

struct UnityProjectSetting_t2579824760_StaticFields
{
public:
	// UnityProjectSetting UnityProjectSetting::_entity
	UnityProjectSetting_t2579824760 * ____entity_3;

public:
	inline static int32_t get_offset_of__entity_3() { return static_cast<int32_t>(offsetof(UnityProjectSetting_t2579824760_StaticFields, ____entity_3)); }
	inline UnityProjectSetting_t2579824760 * get__entity_3() const { return ____entity_3; }
	inline UnityProjectSetting_t2579824760 ** get_address_of__entity_3() { return &____entity_3; }
	inline void set__entity_3(UnityProjectSetting_t2579824760 * value)
	{
		____entity_3 = value;
		Il2CppCodeGenWriteBarrier((&____entity_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPROJECTSETTING_T2579824760_H
#ifndef EXCELDATA_T3659864980_H
#define EXCELDATA_T3659864980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExcelData
struct  ExcelData_t3659864980  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<ExcelData/RowData> ExcelData::RowDataList
	List_1_t4163071842 * ___RowDataList_2;

public:
	inline static int32_t get_offset_of_RowDataList_2() { return static_cast<int32_t>(offsetof(ExcelData_t3659864980, ___RowDataList_2)); }
	inline List_1_t4163071842 * get_RowDataList_2() const { return ___RowDataList_2; }
	inline List_1_t4163071842 ** get_address_of_RowDataList_2() { return &___RowDataList_2; }
	inline void set_RowDataList_2(List_1_t4163071842 * value)
	{
		___RowDataList_2 = value;
		Il2CppCodeGenWriteBarrier((&___RowDataList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCELDATA_T3659864980_H
#ifndef GETTOUCHDELEGATE_T4218246285_H
#define GETTOUCHDELEGATE_T4218246285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/GetTouchDelegate
struct  GetTouchDelegate_t4218246285  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTOUCHDELEGATE_T4218246285_H
#ifndef ONGEOMETRYUPDATED_T2462438111_H
#define ONGEOMETRYUPDATED_T2462438111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel/OnGeometryUpdated
struct  OnGeometryUpdated_t2462438111  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGEOMETRYUPDATED_T2462438111_H
#ifndef CACHEDATA_T2005210238_H
#define CACHEDATA_T2005210238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CacheData
struct  CacheData_t2005210238  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean CacheData::IsClear
	bool ___IsClear_3;
	// System.Boolean CacheData::IsNewRecored
	bool ___IsNewRecored_4;
	// System.Int32 CacheData::StageNo
	int32_t ___StageNo_5;
	// System.Single CacheData::CurrentScore
	float ___CurrentScore_6;

public:
	inline static int32_t get_offset_of_IsClear_3() { return static_cast<int32_t>(offsetof(CacheData_t2005210238, ___IsClear_3)); }
	inline bool get_IsClear_3() const { return ___IsClear_3; }
	inline bool* get_address_of_IsClear_3() { return &___IsClear_3; }
	inline void set_IsClear_3(bool value)
	{
		___IsClear_3 = value;
	}

	inline static int32_t get_offset_of_IsNewRecored_4() { return static_cast<int32_t>(offsetof(CacheData_t2005210238, ___IsNewRecored_4)); }
	inline bool get_IsNewRecored_4() const { return ___IsNewRecored_4; }
	inline bool* get_address_of_IsNewRecored_4() { return &___IsNewRecored_4; }
	inline void set_IsNewRecored_4(bool value)
	{
		___IsNewRecored_4 = value;
	}

	inline static int32_t get_offset_of_StageNo_5() { return static_cast<int32_t>(offsetof(CacheData_t2005210238, ___StageNo_5)); }
	inline int32_t get_StageNo_5() const { return ___StageNo_5; }
	inline int32_t* get_address_of_StageNo_5() { return &___StageNo_5; }
	inline void set_StageNo_5(int32_t value)
	{
		___StageNo_5 = value;
	}

	inline static int32_t get_offset_of_CurrentScore_6() { return static_cast<int32_t>(offsetof(CacheData_t2005210238, ___CurrentScore_6)); }
	inline float get_CurrentScore_6() const { return ___CurrentScore_6; }
	inline float* get_address_of_CurrentScore_6() { return &___CurrentScore_6; }
	inline void set_CurrentScore_6(float value)
	{
		___CurrentScore_6 = value;
	}
};

struct CacheData_t2005210238_StaticFields
{
public:
	// CacheData CacheData::_entity
	CacheData_t2005210238 * ____entity_2;

public:
	inline static int32_t get_offset_of__entity_2() { return static_cast<int32_t>(offsetof(CacheData_t2005210238_StaticFields, ____entity_2)); }
	inline CacheData_t2005210238 * get__entity_2() const { return ____entity_2; }
	inline CacheData_t2005210238 ** get_address_of__entity_2() { return &____entity_2; }
	inline void set__entity_2(CacheData_t2005210238 * value)
	{
		____entity_2 = value;
		Il2CppCodeGenWriteBarrier((&____entity_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDATA_T2005210238_H
#ifndef MODIFIERFUNC_T2998065802_H
#define MODIFIERFUNC_T2998065802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel/ModifierFunc
struct  ModifierFunc_t2998065802  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERFUNC_T2998065802_H
#ifndef ONVALIDATE_T1246632601_H
#define ONVALIDATE_T1246632601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput/OnValidate
struct  OnValidate_t1246632601  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVALIDATE_T1246632601_H
#ifndef ONCREATEMATERIAL_T2961246930_H
#define ONCREATEMATERIAL_T2961246930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel/OnCreateMaterial
struct  OnCreateMaterial_t2961246930  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCREATEMATERIAL_T2961246930_H
#ifndef GETTOUCHCALLBACK_T97678626_H
#define GETTOUCHCALLBACK_T97678626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/GetTouchCallback
struct  GetTouchCallback_t97678626  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTOUCHCALLBACK_T97678626_H
#ifndef GETTOUCHCOUNTCALLBACK_T3185863032_H
#define GETTOUCHCOUNTCALLBACK_T3185863032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/GetTouchCountCallback
struct  GetTouchCountCallback_t3185863032  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTOUCHCOUNTCALLBACK_T3185863032_H
#ifndef KEYCODEDELEGATE_T3064672302_H
#define KEYCODEDELEGATE_T3064672302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/KeyCodeDelegate
struct  KeyCodeDelegate_t3064672302  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODEDELEGATE_T3064672302_H
#ifndef OBJECTDELEGATE_T2041570719_H
#define OBJECTDELEGATE_T2041570719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/ObjectDelegate
struct  ObjectDelegate_t2041570719  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDELEGATE_T2041570719_H
#ifndef ONCLIPPINGMOVED_T476625095_H
#define ONCLIPPINGMOVED_T476625095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel/OnClippingMoved
struct  OnClippingMoved_t476625095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLIPPINGMOVED_T476625095_H
#ifndef FLOATDELEGATE_T906524069_H
#define FLOATDELEGATE_T906524069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/FloatDelegate
struct  FloatDelegate_t906524069  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATDELEGATE_T906524069_H
#ifndef BOOLDELEGATE_T3825226153_H
#define BOOLDELEGATE_T3825226153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/BoolDelegate
struct  BoolDelegate_t3825226153  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLDELEGATE_T3825226153_H
#ifndef VOIDDELEGATE_T3100799918_H
#define VOIDDELEGATE_T3100799918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/VoidDelegate
struct  VoidDelegate_t3100799918  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOIDDELEGATE_T3100799918_H
#ifndef MOVEDELEGATE_T16019400_H
#define MOVEDELEGATE_T16019400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/MoveDelegate
struct  MoveDelegate_t16019400  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDELEGATE_T16019400_H
#ifndef ONSCHEMECHANGE_T1701155603_H
#define ONSCHEMECHANGE_T1701155603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/OnSchemeChange
struct  OnSchemeChange_t1701155603  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCHEMECHANGE_T1701155603_H
#ifndef REMOVETOUCHDELEGATE_T2508278027_H
#define REMOVETOUCHDELEGATE_T2508278027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/RemoveTouchDelegate
struct  RemoveTouchDelegate_t2508278027  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVETOUCHDELEGATE_T2508278027_H
#ifndef ONCUSTOMINPUT_T3508588789_H
#define ONCUSTOMINPUT_T3508588789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/OnCustomInput
struct  OnCustomInput_t3508588789  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCUSTOMINPUT_T3508588789_H
#ifndef VECTORDELEGATE_T435795517_H
#define VECTORDELEGATE_T435795517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/VectorDelegate
struct  VectorDelegate_t435795517  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORDELEGATE_T435795517_H
#ifndef ONSCREENRESIZE_T2279991692_H
#define ONSCREENRESIZE_T2279991692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera/OnScreenResize
struct  OnScreenResize_t2279991692  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCREENRESIZE_T2279991692_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIORTHOCAMERA_T1944225589_H
#define UIORTHOCAMERA_T1944225589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIOrthoCamera
struct  UIOrthoCamera_t1944225589  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UIOrthoCamera::mCam
	Camera_t4157153871 * ___mCam_2;
	// UnityEngine.Transform UIOrthoCamera::mTrans
	Transform_t3600365921 * ___mTrans_3;

public:
	inline static int32_t get_offset_of_mCam_2() { return static_cast<int32_t>(offsetof(UIOrthoCamera_t1944225589, ___mCam_2)); }
	inline Camera_t4157153871 * get_mCam_2() const { return ___mCam_2; }
	inline Camera_t4157153871 ** get_address_of_mCam_2() { return &___mCam_2; }
	inline void set_mCam_2(Camera_t4157153871 * value)
	{
		___mCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_2), value);
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(UIOrthoCamera_t1944225589, ___mTrans_3)); }
	inline Transform_t3600365921 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3600365921 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3600365921 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIORTHOCAMERA_T1944225589_H
#ifndef UILOCALIZE_T3543745742_H
#define UILOCALIZE_T3543745742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILocalize
struct  UILocalize_t3543745742  : public MonoBehaviour_t3962482529
{
public:
	// System.String UILocalize::key
	String_t* ___key_2;
	// System.Boolean UILocalize::mStarted
	bool ___mStarted_3;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(UILocalize_t3543745742, ___key_2)); }
	inline String_t* get_key_2() const { return ___key_2; }
	inline String_t** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(String_t* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_mStarted_3() { return static_cast<int32_t>(offsetof(UILocalize_t3543745742, ___mStarted_3)); }
	inline bool get_mStarted_3() const { return ___mStarted_3; }
	inline bool* get_address_of_mStarted_3() { return &___mStarted_3; }
	inline void set_mStarted_3(bool value)
	{
		___mStarted_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILOCALIZE_T3543745742_H
#ifndef UITWEENER_T260334902_H
#define UITWEENER_T260334902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITweener
struct  UITweener_t260334902  : public MonoBehaviour_t3962482529
{
public:
	// UITweener/Method UITweener::method
	int32_t ___method_3;
	// UITweener/Style UITweener::style
	int32_t ___style_4;
	// UnityEngine.AnimationCurve UITweener::animationCurve
	AnimationCurve_t3046754366 * ___animationCurve_5;
	// System.Boolean UITweener::ignoreTimeScale
	bool ___ignoreTimeScale_6;
	// System.Single UITweener::delay
	float ___delay_7;
	// System.Single UITweener::duration
	float ___duration_8;
	// System.Boolean UITweener::steeperCurves
	bool ___steeperCurves_9;
	// System.Int32 UITweener::tweenGroup
	int32_t ___tweenGroup_10;
	// System.Boolean UITweener::useFixedUpdate
	bool ___useFixedUpdate_11;
	// System.Collections.Generic.List`1<EventDelegate> UITweener::onFinished
	List_1_t4210400802 * ___onFinished_12;
	// UnityEngine.GameObject UITweener::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_13;
	// System.String UITweener::callWhenFinished
	String_t* ___callWhenFinished_14;
	// System.Single UITweener::timeScale
	float ___timeScale_15;
	// System.Boolean UITweener::mStarted
	bool ___mStarted_16;
	// System.Single UITweener::mStartTime
	float ___mStartTime_17;
	// System.Single UITweener::mDuration
	float ___mDuration_18;
	// System.Single UITweener::mAmountPerDelta
	float ___mAmountPerDelta_19;
	// System.Single UITweener::mFactor
	float ___mFactor_20;
	// System.Collections.Generic.List`1<EventDelegate> UITweener::mTemp
	List_1_t4210400802 * ___mTemp_21;

public:
	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___method_3)); }
	inline int32_t get_method_3() const { return ___method_3; }
	inline int32_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(int32_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_style_4() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___style_4)); }
	inline int32_t get_style_4() const { return ___style_4; }
	inline int32_t* get_address_of_style_4() { return &___style_4; }
	inline void set_style_4(int32_t value)
	{
		___style_4 = value;
	}

	inline static int32_t get_offset_of_animationCurve_5() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___animationCurve_5)); }
	inline AnimationCurve_t3046754366 * get_animationCurve_5() const { return ___animationCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_animationCurve_5() { return &___animationCurve_5; }
	inline void set_animationCurve_5(AnimationCurve_t3046754366 * value)
	{
		___animationCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___animationCurve_5), value);
	}

	inline static int32_t get_offset_of_ignoreTimeScale_6() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___ignoreTimeScale_6)); }
	inline bool get_ignoreTimeScale_6() const { return ___ignoreTimeScale_6; }
	inline bool* get_address_of_ignoreTimeScale_6() { return &___ignoreTimeScale_6; }
	inline void set_ignoreTimeScale_6(bool value)
	{
		___ignoreTimeScale_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_duration_8() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___duration_8)); }
	inline float get_duration_8() const { return ___duration_8; }
	inline float* get_address_of_duration_8() { return &___duration_8; }
	inline void set_duration_8(float value)
	{
		___duration_8 = value;
	}

	inline static int32_t get_offset_of_steeperCurves_9() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___steeperCurves_9)); }
	inline bool get_steeperCurves_9() const { return ___steeperCurves_9; }
	inline bool* get_address_of_steeperCurves_9() { return &___steeperCurves_9; }
	inline void set_steeperCurves_9(bool value)
	{
		___steeperCurves_9 = value;
	}

	inline static int32_t get_offset_of_tweenGroup_10() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___tweenGroup_10)); }
	inline int32_t get_tweenGroup_10() const { return ___tweenGroup_10; }
	inline int32_t* get_address_of_tweenGroup_10() { return &___tweenGroup_10; }
	inline void set_tweenGroup_10(int32_t value)
	{
		___tweenGroup_10 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_11() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___useFixedUpdate_11)); }
	inline bool get_useFixedUpdate_11() const { return ___useFixedUpdate_11; }
	inline bool* get_address_of_useFixedUpdate_11() { return &___useFixedUpdate_11; }
	inline void set_useFixedUpdate_11(bool value)
	{
		___useFixedUpdate_11 = value;
	}

	inline static int32_t get_offset_of_onFinished_12() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___onFinished_12)); }
	inline List_1_t4210400802 * get_onFinished_12() const { return ___onFinished_12; }
	inline List_1_t4210400802 ** get_address_of_onFinished_12() { return &___onFinished_12; }
	inline void set_onFinished_12(List_1_t4210400802 * value)
	{
		___onFinished_12 = value;
		Il2CppCodeGenWriteBarrier((&___onFinished_12), value);
	}

	inline static int32_t get_offset_of_eventReceiver_13() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___eventReceiver_13)); }
	inline GameObject_t1113636619 * get_eventReceiver_13() const { return ___eventReceiver_13; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_13() { return &___eventReceiver_13; }
	inline void set_eventReceiver_13(GameObject_t1113636619 * value)
	{
		___eventReceiver_13 = value;
		Il2CppCodeGenWriteBarrier((&___eventReceiver_13), value);
	}

	inline static int32_t get_offset_of_callWhenFinished_14() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___callWhenFinished_14)); }
	inline String_t* get_callWhenFinished_14() const { return ___callWhenFinished_14; }
	inline String_t** get_address_of_callWhenFinished_14() { return &___callWhenFinished_14; }
	inline void set_callWhenFinished_14(String_t* value)
	{
		___callWhenFinished_14 = value;
		Il2CppCodeGenWriteBarrier((&___callWhenFinished_14), value);
	}

	inline static int32_t get_offset_of_timeScale_15() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___timeScale_15)); }
	inline float get_timeScale_15() const { return ___timeScale_15; }
	inline float* get_address_of_timeScale_15() { return &___timeScale_15; }
	inline void set_timeScale_15(float value)
	{
		___timeScale_15 = value;
	}

	inline static int32_t get_offset_of_mStarted_16() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mStarted_16)); }
	inline bool get_mStarted_16() const { return ___mStarted_16; }
	inline bool* get_address_of_mStarted_16() { return &___mStarted_16; }
	inline void set_mStarted_16(bool value)
	{
		___mStarted_16 = value;
	}

	inline static int32_t get_offset_of_mStartTime_17() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mStartTime_17)); }
	inline float get_mStartTime_17() const { return ___mStartTime_17; }
	inline float* get_address_of_mStartTime_17() { return &___mStartTime_17; }
	inline void set_mStartTime_17(float value)
	{
		___mStartTime_17 = value;
	}

	inline static int32_t get_offset_of_mDuration_18() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mDuration_18)); }
	inline float get_mDuration_18() const { return ___mDuration_18; }
	inline float* get_address_of_mDuration_18() { return &___mDuration_18; }
	inline void set_mDuration_18(float value)
	{
		___mDuration_18 = value;
	}

	inline static int32_t get_offset_of_mAmountPerDelta_19() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mAmountPerDelta_19)); }
	inline float get_mAmountPerDelta_19() const { return ___mAmountPerDelta_19; }
	inline float* get_address_of_mAmountPerDelta_19() { return &___mAmountPerDelta_19; }
	inline void set_mAmountPerDelta_19(float value)
	{
		___mAmountPerDelta_19 = value;
	}

	inline static int32_t get_offset_of_mFactor_20() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mFactor_20)); }
	inline float get_mFactor_20() const { return ___mFactor_20; }
	inline float* get_address_of_mFactor_20() { return &___mFactor_20; }
	inline void set_mFactor_20(float value)
	{
		___mFactor_20 = value;
	}

	inline static int32_t get_offset_of_mTemp_21() { return static_cast<int32_t>(offsetof(UITweener_t260334902, ___mTemp_21)); }
	inline List_1_t4210400802 * get_mTemp_21() const { return ___mTemp_21; }
	inline List_1_t4210400802 ** get_address_of_mTemp_21() { return &___mTemp_21; }
	inline void set_mTemp_21(List_1_t4210400802 * value)
	{
		___mTemp_21 = value;
		Il2CppCodeGenWriteBarrier((&___mTemp_21), value);
	}
};

struct UITweener_t260334902_StaticFields
{
public:
	// UITweener UITweener::current
	UITweener_t260334902 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UITweener_t260334902_StaticFields, ___current_2)); }
	inline UITweener_t260334902 * get_current_2() const { return ___current_2; }
	inline UITweener_t260334902 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UITweener_t260334902 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITWEENER_T260334902_H
#ifndef UIADJUSTER_T2553577866_H
#define UIADJUSTER_T2553577866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdjuster
struct  UIAdjuster_t2553577866  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UIAdjuster::_currentAspectRate
	float ____currentAspectRate_2;

public:
	inline static int32_t get_offset_of__currentAspectRate_2() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866, ____currentAspectRate_2)); }
	inline float get__currentAspectRate_2() const { return ____currentAspectRate_2; }
	inline float* get_address_of__currentAspectRate_2() { return &____currentAspectRate_2; }
	inline void set__currentAspectRate_2(float value)
	{
		____currentAspectRate_2 = value;
	}
};

struct UIAdjuster_t2553577866_StaticFields
{
public:
	// UnityEngine.Vector2 UIAdjuster::SCREEN_SIZE_DEFULT
	Vector2_t2156229523  ___SCREEN_SIZE_DEFULT_5;
	// UnityEngine.Vector2 UIAdjuster::SCREEN_SIZE_640_960
	Vector2_t2156229523  ___SCREEN_SIZE_640_960_6;
	// UnityEngine.Vector2 UIAdjuster::SCREEN_SIZE_640_1136
	Vector2_t2156229523  ___SCREEN_SIZE_640_1136_7;

public:
	inline static int32_t get_offset_of_SCREEN_SIZE_DEFULT_5() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866_StaticFields, ___SCREEN_SIZE_DEFULT_5)); }
	inline Vector2_t2156229523  get_SCREEN_SIZE_DEFULT_5() const { return ___SCREEN_SIZE_DEFULT_5; }
	inline Vector2_t2156229523 * get_address_of_SCREEN_SIZE_DEFULT_5() { return &___SCREEN_SIZE_DEFULT_5; }
	inline void set_SCREEN_SIZE_DEFULT_5(Vector2_t2156229523  value)
	{
		___SCREEN_SIZE_DEFULT_5 = value;
	}

	inline static int32_t get_offset_of_SCREEN_SIZE_640_960_6() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866_StaticFields, ___SCREEN_SIZE_640_960_6)); }
	inline Vector2_t2156229523  get_SCREEN_SIZE_640_960_6() const { return ___SCREEN_SIZE_640_960_6; }
	inline Vector2_t2156229523 * get_address_of_SCREEN_SIZE_640_960_6() { return &___SCREEN_SIZE_640_960_6; }
	inline void set_SCREEN_SIZE_640_960_6(Vector2_t2156229523  value)
	{
		___SCREEN_SIZE_640_960_6 = value;
	}

	inline static int32_t get_offset_of_SCREEN_SIZE_640_1136_7() { return static_cast<int32_t>(offsetof(UIAdjuster_t2553577866_StaticFields, ___SCREEN_SIZE_640_1136_7)); }
	inline Vector2_t2156229523  get_SCREEN_SIZE_640_1136_7() const { return ___SCREEN_SIZE_640_1136_7; }
	inline Vector2_t2156229523 * get_address_of_SCREEN_SIZE_640_1136_7() { return &___SCREEN_SIZE_640_1136_7; }
	inline void set_SCREEN_SIZE_640_1136_7(Vector2_t2156229523  value)
	{
		___SCREEN_SIZE_640_1136_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADJUSTER_T2553577866_H
#ifndef UIINPUTONGUI_T4239979770_H
#define UIINPUTONGUI_T4239979770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInputOnGUI
struct  UIInputOnGUI_t4239979770  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTONGUI_T4239979770_H
#ifndef UIINPUT_T421821618_H
#define UIINPUT_T421821618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput
struct  UIInput_t421821618  : public MonoBehaviour_t3962482529
{
public:
	// UILabel UIInput::label
	UILabel_t3248798549 * ___label_4;
	// UIInput/InputType UIInput::inputType
	int32_t ___inputType_5;
	// UIInput/OnReturnKey UIInput::onReturnKey
	int32_t ___onReturnKey_6;
	// UIInput/KeyboardType UIInput::keyboardType
	int32_t ___keyboardType_7;
	// System.Boolean UIInput::hideInput
	bool ___hideInput_8;
	// System.Boolean UIInput::selectAllTextOnFocus
	bool ___selectAllTextOnFocus_9;
	// System.Boolean UIInput::submitOnUnselect
	bool ___submitOnUnselect_10;
	// UIInput/Validation UIInput::validation
	int32_t ___validation_11;
	// System.Int32 UIInput::characterLimit
	int32_t ___characterLimit_12;
	// System.String UIInput::savedAs
	String_t* ___savedAs_13;
	// UnityEngine.GameObject UIInput::selectOnTab
	GameObject_t1113636619 * ___selectOnTab_14;
	// UnityEngine.Color UIInput::activeTextColor
	Color_t2555686324  ___activeTextColor_15;
	// UnityEngine.Color UIInput::caretColor
	Color_t2555686324  ___caretColor_16;
	// UnityEngine.Color UIInput::selectionColor
	Color_t2555686324  ___selectionColor_17;
	// System.Collections.Generic.List`1<EventDelegate> UIInput::onSubmit
	List_1_t4210400802 * ___onSubmit_18;
	// System.Collections.Generic.List`1<EventDelegate> UIInput::onChange
	List_1_t4210400802 * ___onChange_19;
	// UIInput/OnValidate UIInput::onValidate
	OnValidate_t1246632601 * ___onValidate_20;
	// System.String UIInput::mValue
	String_t* ___mValue_21;
	// System.String UIInput::mDefaultText
	String_t* ___mDefaultText_22;
	// UnityEngine.Color UIInput::mDefaultColor
	Color_t2555686324  ___mDefaultColor_23;
	// System.Single UIInput::mPosition
	float ___mPosition_24;
	// System.Boolean UIInput::mDoInit
	bool ___mDoInit_25;
	// NGUIText/Alignment UIInput::mAlignment
	int32_t ___mAlignment_26;
	// System.Boolean UIInput::mLoadSavedValue
	bool ___mLoadSavedValue_27;
	// System.Int32 UIInput::mSelectionStart
	int32_t ___mSelectionStart_32;
	// System.Int32 UIInput::mSelectionEnd
	int32_t ___mSelectionEnd_33;
	// UITexture UIInput::mHighlight
	UITexture_t3471168817 * ___mHighlight_34;
	// UITexture UIInput::mCaret
	UITexture_t3471168817 * ___mCaret_35;
	// UnityEngine.Texture2D UIInput::mBlankTex
	Texture2D_t3840446185 * ___mBlankTex_36;
	// System.Single UIInput::mNextBlink
	float ___mNextBlink_37;
	// System.Single UIInput::mLastAlpha
	float ___mLastAlpha_38;
	// System.String UIInput::mCached
	String_t* ___mCached_39;
	// System.Int32 UIInput::mSelectMe
	int32_t ___mSelectMe_40;
	// System.Int32 UIInput::mSelectTime
	int32_t ___mSelectTime_41;
	// System.Boolean UIInput::mStarted
	bool ___mStarted_42;
	// UICamera UIInput::mCam
	UICamera_t1356438871 * ___mCam_43;
	// System.Boolean UIInput::mEllipsis
	bool ___mEllipsis_44;
	// System.Action UIInput::onUpArrow
	Action_t1264377477 * ___onUpArrow_46;
	// System.Action UIInput::onDownArrow
	Action_t1264377477 * ___onDownArrow_47;

public:
	inline static int32_t get_offset_of_label_4() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___label_4)); }
	inline UILabel_t3248798549 * get_label_4() const { return ___label_4; }
	inline UILabel_t3248798549 ** get_address_of_label_4() { return &___label_4; }
	inline void set_label_4(UILabel_t3248798549 * value)
	{
		___label_4 = value;
		Il2CppCodeGenWriteBarrier((&___label_4), value);
	}

	inline static int32_t get_offset_of_inputType_5() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___inputType_5)); }
	inline int32_t get_inputType_5() const { return ___inputType_5; }
	inline int32_t* get_address_of_inputType_5() { return &___inputType_5; }
	inline void set_inputType_5(int32_t value)
	{
		___inputType_5 = value;
	}

	inline static int32_t get_offset_of_onReturnKey_6() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___onReturnKey_6)); }
	inline int32_t get_onReturnKey_6() const { return ___onReturnKey_6; }
	inline int32_t* get_address_of_onReturnKey_6() { return &___onReturnKey_6; }
	inline void set_onReturnKey_6(int32_t value)
	{
		___onReturnKey_6 = value;
	}

	inline static int32_t get_offset_of_keyboardType_7() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___keyboardType_7)); }
	inline int32_t get_keyboardType_7() const { return ___keyboardType_7; }
	inline int32_t* get_address_of_keyboardType_7() { return &___keyboardType_7; }
	inline void set_keyboardType_7(int32_t value)
	{
		___keyboardType_7 = value;
	}

	inline static int32_t get_offset_of_hideInput_8() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___hideInput_8)); }
	inline bool get_hideInput_8() const { return ___hideInput_8; }
	inline bool* get_address_of_hideInput_8() { return &___hideInput_8; }
	inline void set_hideInput_8(bool value)
	{
		___hideInput_8 = value;
	}

	inline static int32_t get_offset_of_selectAllTextOnFocus_9() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___selectAllTextOnFocus_9)); }
	inline bool get_selectAllTextOnFocus_9() const { return ___selectAllTextOnFocus_9; }
	inline bool* get_address_of_selectAllTextOnFocus_9() { return &___selectAllTextOnFocus_9; }
	inline void set_selectAllTextOnFocus_9(bool value)
	{
		___selectAllTextOnFocus_9 = value;
	}

	inline static int32_t get_offset_of_submitOnUnselect_10() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___submitOnUnselect_10)); }
	inline bool get_submitOnUnselect_10() const { return ___submitOnUnselect_10; }
	inline bool* get_address_of_submitOnUnselect_10() { return &___submitOnUnselect_10; }
	inline void set_submitOnUnselect_10(bool value)
	{
		___submitOnUnselect_10 = value;
	}

	inline static int32_t get_offset_of_validation_11() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___validation_11)); }
	inline int32_t get_validation_11() const { return ___validation_11; }
	inline int32_t* get_address_of_validation_11() { return &___validation_11; }
	inline void set_validation_11(int32_t value)
	{
		___validation_11 = value;
	}

	inline static int32_t get_offset_of_characterLimit_12() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___characterLimit_12)); }
	inline int32_t get_characterLimit_12() const { return ___characterLimit_12; }
	inline int32_t* get_address_of_characterLimit_12() { return &___characterLimit_12; }
	inline void set_characterLimit_12(int32_t value)
	{
		___characterLimit_12 = value;
	}

	inline static int32_t get_offset_of_savedAs_13() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___savedAs_13)); }
	inline String_t* get_savedAs_13() const { return ___savedAs_13; }
	inline String_t** get_address_of_savedAs_13() { return &___savedAs_13; }
	inline void set_savedAs_13(String_t* value)
	{
		___savedAs_13 = value;
		Il2CppCodeGenWriteBarrier((&___savedAs_13), value);
	}

	inline static int32_t get_offset_of_selectOnTab_14() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___selectOnTab_14)); }
	inline GameObject_t1113636619 * get_selectOnTab_14() const { return ___selectOnTab_14; }
	inline GameObject_t1113636619 ** get_address_of_selectOnTab_14() { return &___selectOnTab_14; }
	inline void set_selectOnTab_14(GameObject_t1113636619 * value)
	{
		___selectOnTab_14 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnTab_14), value);
	}

	inline static int32_t get_offset_of_activeTextColor_15() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___activeTextColor_15)); }
	inline Color_t2555686324  get_activeTextColor_15() const { return ___activeTextColor_15; }
	inline Color_t2555686324 * get_address_of_activeTextColor_15() { return &___activeTextColor_15; }
	inline void set_activeTextColor_15(Color_t2555686324  value)
	{
		___activeTextColor_15 = value;
	}

	inline static int32_t get_offset_of_caretColor_16() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___caretColor_16)); }
	inline Color_t2555686324  get_caretColor_16() const { return ___caretColor_16; }
	inline Color_t2555686324 * get_address_of_caretColor_16() { return &___caretColor_16; }
	inline void set_caretColor_16(Color_t2555686324  value)
	{
		___caretColor_16 = value;
	}

	inline static int32_t get_offset_of_selectionColor_17() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___selectionColor_17)); }
	inline Color_t2555686324  get_selectionColor_17() const { return ___selectionColor_17; }
	inline Color_t2555686324 * get_address_of_selectionColor_17() { return &___selectionColor_17; }
	inline void set_selectionColor_17(Color_t2555686324  value)
	{
		___selectionColor_17 = value;
	}

	inline static int32_t get_offset_of_onSubmit_18() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___onSubmit_18)); }
	inline List_1_t4210400802 * get_onSubmit_18() const { return ___onSubmit_18; }
	inline List_1_t4210400802 ** get_address_of_onSubmit_18() { return &___onSubmit_18; }
	inline void set_onSubmit_18(List_1_t4210400802 * value)
	{
		___onSubmit_18 = value;
		Il2CppCodeGenWriteBarrier((&___onSubmit_18), value);
	}

	inline static int32_t get_offset_of_onChange_19() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___onChange_19)); }
	inline List_1_t4210400802 * get_onChange_19() const { return ___onChange_19; }
	inline List_1_t4210400802 ** get_address_of_onChange_19() { return &___onChange_19; }
	inline void set_onChange_19(List_1_t4210400802 * value)
	{
		___onChange_19 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_19), value);
	}

	inline static int32_t get_offset_of_onValidate_20() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___onValidate_20)); }
	inline OnValidate_t1246632601 * get_onValidate_20() const { return ___onValidate_20; }
	inline OnValidate_t1246632601 ** get_address_of_onValidate_20() { return &___onValidate_20; }
	inline void set_onValidate_20(OnValidate_t1246632601 * value)
	{
		___onValidate_20 = value;
		Il2CppCodeGenWriteBarrier((&___onValidate_20), value);
	}

	inline static int32_t get_offset_of_mValue_21() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mValue_21)); }
	inline String_t* get_mValue_21() const { return ___mValue_21; }
	inline String_t** get_address_of_mValue_21() { return &___mValue_21; }
	inline void set_mValue_21(String_t* value)
	{
		___mValue_21 = value;
		Il2CppCodeGenWriteBarrier((&___mValue_21), value);
	}

	inline static int32_t get_offset_of_mDefaultText_22() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mDefaultText_22)); }
	inline String_t* get_mDefaultText_22() const { return ___mDefaultText_22; }
	inline String_t** get_address_of_mDefaultText_22() { return &___mDefaultText_22; }
	inline void set_mDefaultText_22(String_t* value)
	{
		___mDefaultText_22 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultText_22), value);
	}

	inline static int32_t get_offset_of_mDefaultColor_23() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mDefaultColor_23)); }
	inline Color_t2555686324  get_mDefaultColor_23() const { return ___mDefaultColor_23; }
	inline Color_t2555686324 * get_address_of_mDefaultColor_23() { return &___mDefaultColor_23; }
	inline void set_mDefaultColor_23(Color_t2555686324  value)
	{
		___mDefaultColor_23 = value;
	}

	inline static int32_t get_offset_of_mPosition_24() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mPosition_24)); }
	inline float get_mPosition_24() const { return ___mPosition_24; }
	inline float* get_address_of_mPosition_24() { return &___mPosition_24; }
	inline void set_mPosition_24(float value)
	{
		___mPosition_24 = value;
	}

	inline static int32_t get_offset_of_mDoInit_25() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mDoInit_25)); }
	inline bool get_mDoInit_25() const { return ___mDoInit_25; }
	inline bool* get_address_of_mDoInit_25() { return &___mDoInit_25; }
	inline void set_mDoInit_25(bool value)
	{
		___mDoInit_25 = value;
	}

	inline static int32_t get_offset_of_mAlignment_26() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mAlignment_26)); }
	inline int32_t get_mAlignment_26() const { return ___mAlignment_26; }
	inline int32_t* get_address_of_mAlignment_26() { return &___mAlignment_26; }
	inline void set_mAlignment_26(int32_t value)
	{
		___mAlignment_26 = value;
	}

	inline static int32_t get_offset_of_mLoadSavedValue_27() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mLoadSavedValue_27)); }
	inline bool get_mLoadSavedValue_27() const { return ___mLoadSavedValue_27; }
	inline bool* get_address_of_mLoadSavedValue_27() { return &___mLoadSavedValue_27; }
	inline void set_mLoadSavedValue_27(bool value)
	{
		___mLoadSavedValue_27 = value;
	}

	inline static int32_t get_offset_of_mSelectionStart_32() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mSelectionStart_32)); }
	inline int32_t get_mSelectionStart_32() const { return ___mSelectionStart_32; }
	inline int32_t* get_address_of_mSelectionStart_32() { return &___mSelectionStart_32; }
	inline void set_mSelectionStart_32(int32_t value)
	{
		___mSelectionStart_32 = value;
	}

	inline static int32_t get_offset_of_mSelectionEnd_33() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mSelectionEnd_33)); }
	inline int32_t get_mSelectionEnd_33() const { return ___mSelectionEnd_33; }
	inline int32_t* get_address_of_mSelectionEnd_33() { return &___mSelectionEnd_33; }
	inline void set_mSelectionEnd_33(int32_t value)
	{
		___mSelectionEnd_33 = value;
	}

	inline static int32_t get_offset_of_mHighlight_34() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mHighlight_34)); }
	inline UITexture_t3471168817 * get_mHighlight_34() const { return ___mHighlight_34; }
	inline UITexture_t3471168817 ** get_address_of_mHighlight_34() { return &___mHighlight_34; }
	inline void set_mHighlight_34(UITexture_t3471168817 * value)
	{
		___mHighlight_34 = value;
		Il2CppCodeGenWriteBarrier((&___mHighlight_34), value);
	}

	inline static int32_t get_offset_of_mCaret_35() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mCaret_35)); }
	inline UITexture_t3471168817 * get_mCaret_35() const { return ___mCaret_35; }
	inline UITexture_t3471168817 ** get_address_of_mCaret_35() { return &___mCaret_35; }
	inline void set_mCaret_35(UITexture_t3471168817 * value)
	{
		___mCaret_35 = value;
		Il2CppCodeGenWriteBarrier((&___mCaret_35), value);
	}

	inline static int32_t get_offset_of_mBlankTex_36() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mBlankTex_36)); }
	inline Texture2D_t3840446185 * get_mBlankTex_36() const { return ___mBlankTex_36; }
	inline Texture2D_t3840446185 ** get_address_of_mBlankTex_36() { return &___mBlankTex_36; }
	inline void set_mBlankTex_36(Texture2D_t3840446185 * value)
	{
		___mBlankTex_36 = value;
		Il2CppCodeGenWriteBarrier((&___mBlankTex_36), value);
	}

	inline static int32_t get_offset_of_mNextBlink_37() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mNextBlink_37)); }
	inline float get_mNextBlink_37() const { return ___mNextBlink_37; }
	inline float* get_address_of_mNextBlink_37() { return &___mNextBlink_37; }
	inline void set_mNextBlink_37(float value)
	{
		___mNextBlink_37 = value;
	}

	inline static int32_t get_offset_of_mLastAlpha_38() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mLastAlpha_38)); }
	inline float get_mLastAlpha_38() const { return ___mLastAlpha_38; }
	inline float* get_address_of_mLastAlpha_38() { return &___mLastAlpha_38; }
	inline void set_mLastAlpha_38(float value)
	{
		___mLastAlpha_38 = value;
	}

	inline static int32_t get_offset_of_mCached_39() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mCached_39)); }
	inline String_t* get_mCached_39() const { return ___mCached_39; }
	inline String_t** get_address_of_mCached_39() { return &___mCached_39; }
	inline void set_mCached_39(String_t* value)
	{
		___mCached_39 = value;
		Il2CppCodeGenWriteBarrier((&___mCached_39), value);
	}

	inline static int32_t get_offset_of_mSelectMe_40() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mSelectMe_40)); }
	inline int32_t get_mSelectMe_40() const { return ___mSelectMe_40; }
	inline int32_t* get_address_of_mSelectMe_40() { return &___mSelectMe_40; }
	inline void set_mSelectMe_40(int32_t value)
	{
		___mSelectMe_40 = value;
	}

	inline static int32_t get_offset_of_mSelectTime_41() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mSelectTime_41)); }
	inline int32_t get_mSelectTime_41() const { return ___mSelectTime_41; }
	inline int32_t* get_address_of_mSelectTime_41() { return &___mSelectTime_41; }
	inline void set_mSelectTime_41(int32_t value)
	{
		___mSelectTime_41 = value;
	}

	inline static int32_t get_offset_of_mStarted_42() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mStarted_42)); }
	inline bool get_mStarted_42() const { return ___mStarted_42; }
	inline bool* get_address_of_mStarted_42() { return &___mStarted_42; }
	inline void set_mStarted_42(bool value)
	{
		___mStarted_42 = value;
	}

	inline static int32_t get_offset_of_mCam_43() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mCam_43)); }
	inline UICamera_t1356438871 * get_mCam_43() const { return ___mCam_43; }
	inline UICamera_t1356438871 ** get_address_of_mCam_43() { return &___mCam_43; }
	inline void set_mCam_43(UICamera_t1356438871 * value)
	{
		___mCam_43 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_43), value);
	}

	inline static int32_t get_offset_of_mEllipsis_44() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___mEllipsis_44)); }
	inline bool get_mEllipsis_44() const { return ___mEllipsis_44; }
	inline bool* get_address_of_mEllipsis_44() { return &___mEllipsis_44; }
	inline void set_mEllipsis_44(bool value)
	{
		___mEllipsis_44 = value;
	}

	inline static int32_t get_offset_of_onUpArrow_46() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___onUpArrow_46)); }
	inline Action_t1264377477 * get_onUpArrow_46() const { return ___onUpArrow_46; }
	inline Action_t1264377477 ** get_address_of_onUpArrow_46() { return &___onUpArrow_46; }
	inline void set_onUpArrow_46(Action_t1264377477 * value)
	{
		___onUpArrow_46 = value;
		Il2CppCodeGenWriteBarrier((&___onUpArrow_46), value);
	}

	inline static int32_t get_offset_of_onDownArrow_47() { return static_cast<int32_t>(offsetof(UIInput_t421821618, ___onDownArrow_47)); }
	inline Action_t1264377477 * get_onDownArrow_47() const { return ___onDownArrow_47; }
	inline Action_t1264377477 ** get_address_of_onDownArrow_47() { return &___onDownArrow_47; }
	inline void set_onDownArrow_47(Action_t1264377477 * value)
	{
		___onDownArrow_47 = value;
		Il2CppCodeGenWriteBarrier((&___onDownArrow_47), value);
	}
};

struct UIInput_t421821618_StaticFields
{
public:
	// UIInput UIInput::current
	UIInput_t421821618 * ___current_2;
	// UIInput UIInput::selection
	UIInput_t421821618 * ___selection_3;
	// System.Int32 UIInput::mDrawStart
	int32_t ___mDrawStart_28;
	// System.String UIInput::mLastIME
	String_t* ___mLastIME_29;
	// UnityEngine.TouchScreenKeyboard UIInput::mKeyboard
	TouchScreenKeyboard_t731888065 * ___mKeyboard_30;
	// System.Boolean UIInput::mWaitForKeyboard
	bool ___mWaitForKeyboard_31;
	// System.Int32 UIInput::mIgnoreKey
	int32_t ___mIgnoreKey_45;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIInput_t421821618_StaticFields, ___current_2)); }
	inline UIInput_t421821618 * get_current_2() const { return ___current_2; }
	inline UIInput_t421821618 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIInput_t421821618 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}

	inline static int32_t get_offset_of_selection_3() { return static_cast<int32_t>(offsetof(UIInput_t421821618_StaticFields, ___selection_3)); }
	inline UIInput_t421821618 * get_selection_3() const { return ___selection_3; }
	inline UIInput_t421821618 ** get_address_of_selection_3() { return &___selection_3; }
	inline void set_selection_3(UIInput_t421821618 * value)
	{
		___selection_3 = value;
		Il2CppCodeGenWriteBarrier((&___selection_3), value);
	}

	inline static int32_t get_offset_of_mDrawStart_28() { return static_cast<int32_t>(offsetof(UIInput_t421821618_StaticFields, ___mDrawStart_28)); }
	inline int32_t get_mDrawStart_28() const { return ___mDrawStart_28; }
	inline int32_t* get_address_of_mDrawStart_28() { return &___mDrawStart_28; }
	inline void set_mDrawStart_28(int32_t value)
	{
		___mDrawStart_28 = value;
	}

	inline static int32_t get_offset_of_mLastIME_29() { return static_cast<int32_t>(offsetof(UIInput_t421821618_StaticFields, ___mLastIME_29)); }
	inline String_t* get_mLastIME_29() const { return ___mLastIME_29; }
	inline String_t** get_address_of_mLastIME_29() { return &___mLastIME_29; }
	inline void set_mLastIME_29(String_t* value)
	{
		___mLastIME_29 = value;
		Il2CppCodeGenWriteBarrier((&___mLastIME_29), value);
	}

	inline static int32_t get_offset_of_mKeyboard_30() { return static_cast<int32_t>(offsetof(UIInput_t421821618_StaticFields, ___mKeyboard_30)); }
	inline TouchScreenKeyboard_t731888065 * get_mKeyboard_30() const { return ___mKeyboard_30; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_mKeyboard_30() { return &___mKeyboard_30; }
	inline void set_mKeyboard_30(TouchScreenKeyboard_t731888065 * value)
	{
		___mKeyboard_30 = value;
		Il2CppCodeGenWriteBarrier((&___mKeyboard_30), value);
	}

	inline static int32_t get_offset_of_mWaitForKeyboard_31() { return static_cast<int32_t>(offsetof(UIInput_t421821618_StaticFields, ___mWaitForKeyboard_31)); }
	inline bool get_mWaitForKeyboard_31() const { return ___mWaitForKeyboard_31; }
	inline bool* get_address_of_mWaitForKeyboard_31() { return &___mWaitForKeyboard_31; }
	inline void set_mWaitForKeyboard_31(bool value)
	{
		___mWaitForKeyboard_31 = value;
	}

	inline static int32_t get_offset_of_mIgnoreKey_45() { return static_cast<int32_t>(offsetof(UIInput_t421821618_StaticFields, ___mIgnoreKey_45)); }
	inline int32_t get_mIgnoreKey_45() const { return ___mIgnoreKey_45; }
	inline int32_t* get_address_of_mIgnoreKey_45() { return &___mIgnoreKey_45; }
	inline void set_mIgnoreKey_45(int32_t value)
	{
		___mIgnoreKey_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUT_T421821618_H
#ifndef UIFONT_T2766063701_H
#define UIFONT_T2766063701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFont
struct  UIFont_t2766063701  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material UIFont::mMat
	Material_t340375123 * ___mMat_2;
	// UnityEngine.Rect UIFont::mUVRect
	Rect_t2360479859  ___mUVRect_3;
	// BMFont UIFont::mFont
	BMFont_t2757936676 * ___mFont_4;
	// UIAtlas UIFont::mAtlas
	UIAtlas_t3195533529 * ___mAtlas_5;
	// UIFont UIFont::mReplacement
	UIFont_t2766063701 * ___mReplacement_6;
	// System.Collections.Generic.List`1<BMSymbol> UIFont::mSymbols
	List_1_t3058133583 * ___mSymbols_7;
	// UnityEngine.Font UIFont::mDynamicFont
	Font_t1956802104 * ___mDynamicFont_8;
	// System.Int32 UIFont::mDynamicFontSize
	int32_t ___mDynamicFontSize_9;
	// UnityEngine.FontStyle UIFont::mDynamicFontStyle
	int32_t ___mDynamicFontStyle_10;
	// UISpriteData UIFont::mSprite
	UISpriteData_t900308526 * ___mSprite_11;
	// System.Int32 UIFont::mPMA
	int32_t ___mPMA_12;
	// System.Int32 UIFont::mPacked
	int32_t ___mPacked_13;

public:
	inline static int32_t get_offset_of_mMat_2() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mMat_2)); }
	inline Material_t340375123 * get_mMat_2() const { return ___mMat_2; }
	inline Material_t340375123 ** get_address_of_mMat_2() { return &___mMat_2; }
	inline void set_mMat_2(Material_t340375123 * value)
	{
		___mMat_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_2), value);
	}

	inline static int32_t get_offset_of_mUVRect_3() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mUVRect_3)); }
	inline Rect_t2360479859  get_mUVRect_3() const { return ___mUVRect_3; }
	inline Rect_t2360479859 * get_address_of_mUVRect_3() { return &___mUVRect_3; }
	inline void set_mUVRect_3(Rect_t2360479859  value)
	{
		___mUVRect_3 = value;
	}

	inline static int32_t get_offset_of_mFont_4() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mFont_4)); }
	inline BMFont_t2757936676 * get_mFont_4() const { return ___mFont_4; }
	inline BMFont_t2757936676 ** get_address_of_mFont_4() { return &___mFont_4; }
	inline void set_mFont_4(BMFont_t2757936676 * value)
	{
		___mFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___mFont_4), value);
	}

	inline static int32_t get_offset_of_mAtlas_5() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mAtlas_5)); }
	inline UIAtlas_t3195533529 * get_mAtlas_5() const { return ___mAtlas_5; }
	inline UIAtlas_t3195533529 ** get_address_of_mAtlas_5() { return &___mAtlas_5; }
	inline void set_mAtlas_5(UIAtlas_t3195533529 * value)
	{
		___mAtlas_5 = value;
		Il2CppCodeGenWriteBarrier((&___mAtlas_5), value);
	}

	inline static int32_t get_offset_of_mReplacement_6() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mReplacement_6)); }
	inline UIFont_t2766063701 * get_mReplacement_6() const { return ___mReplacement_6; }
	inline UIFont_t2766063701 ** get_address_of_mReplacement_6() { return &___mReplacement_6; }
	inline void set_mReplacement_6(UIFont_t2766063701 * value)
	{
		___mReplacement_6 = value;
		Il2CppCodeGenWriteBarrier((&___mReplacement_6), value);
	}

	inline static int32_t get_offset_of_mSymbols_7() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mSymbols_7)); }
	inline List_1_t3058133583 * get_mSymbols_7() const { return ___mSymbols_7; }
	inline List_1_t3058133583 ** get_address_of_mSymbols_7() { return &___mSymbols_7; }
	inline void set_mSymbols_7(List_1_t3058133583 * value)
	{
		___mSymbols_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSymbols_7), value);
	}

	inline static int32_t get_offset_of_mDynamicFont_8() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mDynamicFont_8)); }
	inline Font_t1956802104 * get_mDynamicFont_8() const { return ___mDynamicFont_8; }
	inline Font_t1956802104 ** get_address_of_mDynamicFont_8() { return &___mDynamicFont_8; }
	inline void set_mDynamicFont_8(Font_t1956802104 * value)
	{
		___mDynamicFont_8 = value;
		Il2CppCodeGenWriteBarrier((&___mDynamicFont_8), value);
	}

	inline static int32_t get_offset_of_mDynamicFontSize_9() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mDynamicFontSize_9)); }
	inline int32_t get_mDynamicFontSize_9() const { return ___mDynamicFontSize_9; }
	inline int32_t* get_address_of_mDynamicFontSize_9() { return &___mDynamicFontSize_9; }
	inline void set_mDynamicFontSize_9(int32_t value)
	{
		___mDynamicFontSize_9 = value;
	}

	inline static int32_t get_offset_of_mDynamicFontStyle_10() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mDynamicFontStyle_10)); }
	inline int32_t get_mDynamicFontStyle_10() const { return ___mDynamicFontStyle_10; }
	inline int32_t* get_address_of_mDynamicFontStyle_10() { return &___mDynamicFontStyle_10; }
	inline void set_mDynamicFontStyle_10(int32_t value)
	{
		___mDynamicFontStyle_10 = value;
	}

	inline static int32_t get_offset_of_mSprite_11() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mSprite_11)); }
	inline UISpriteData_t900308526 * get_mSprite_11() const { return ___mSprite_11; }
	inline UISpriteData_t900308526 ** get_address_of_mSprite_11() { return &___mSprite_11; }
	inline void set_mSprite_11(UISpriteData_t900308526 * value)
	{
		___mSprite_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_11), value);
	}

	inline static int32_t get_offset_of_mPMA_12() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mPMA_12)); }
	inline int32_t get_mPMA_12() const { return ___mPMA_12; }
	inline int32_t* get_address_of_mPMA_12() { return &___mPMA_12; }
	inline void set_mPMA_12(int32_t value)
	{
		___mPMA_12 = value;
	}

	inline static int32_t get_offset_of_mPacked_13() { return static_cast<int32_t>(offsetof(UIFont_t2766063701, ___mPacked_13)); }
	inline int32_t get_mPacked_13() const { return ___mPacked_13; }
	inline int32_t* get_address_of_mPacked_13() { return &___mPacked_13; }
	inline void set_mPacked_13(int32_t value)
	{
		___mPacked_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFONT_T2766063701_H
#ifndef BUTTONASSISTANT_T3922690862_H
#define BUTTONASSISTANT_T3922690862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonAssistant
struct  ButtonAssistant_t3922690862  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Collider ButtonAssistant::_collider
	Collider_t1773347010 * ____collider_2;
	// UIButton ButtonAssistant::_button
	UIButton_t1100396938 * ____button_3;
	// UISprite ButtonAssistant::_image
	UISprite_t194114938 * ____image_4;

public:
	inline static int32_t get_offset_of__collider_2() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____collider_2)); }
	inline Collider_t1773347010 * get__collider_2() const { return ____collider_2; }
	inline Collider_t1773347010 ** get_address_of__collider_2() { return &____collider_2; }
	inline void set__collider_2(Collider_t1773347010 * value)
	{
		____collider_2 = value;
		Il2CppCodeGenWriteBarrier((&____collider_2), value);
	}

	inline static int32_t get_offset_of__button_3() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____button_3)); }
	inline UIButton_t1100396938 * get__button_3() const { return ____button_3; }
	inline UIButton_t1100396938 ** get_address_of__button_3() { return &____button_3; }
	inline void set__button_3(UIButton_t1100396938 * value)
	{
		____button_3 = value;
		Il2CppCodeGenWriteBarrier((&____button_3), value);
	}

	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(ButtonAssistant_t3922690862, ____image_4)); }
	inline UISprite_t194114938 * get__image_4() const { return ____image_4; }
	inline UISprite_t194114938 ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(UISprite_t194114938 * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONASSISTANT_T3922690862_H
#ifndef UICOLORPICKER_T463494602_H
#define UICOLORPICKER_T463494602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIColorPicker
struct  UIColorPicker_t463494602  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UIColorPicker::value
	Color_t2555686324  ___value_3;
	// UIWidget UIColorPicker::selectionWidget
	UIWidget_t3538521925 * ___selectionWidget_4;
	// System.Collections.Generic.List`1<EventDelegate> UIColorPicker::onChange
	List_1_t4210400802 * ___onChange_5;
	// UnityEngine.Transform UIColorPicker::mTrans
	Transform_t3600365921 * ___mTrans_6;
	// UITexture UIColorPicker::mUITex
	UITexture_t3471168817 * ___mUITex_7;
	// UnityEngine.Texture2D UIColorPicker::mTex
	Texture2D_t3840446185 * ___mTex_8;
	// UICamera UIColorPicker::mCam
	UICamera_t1356438871 * ___mCam_9;
	// UnityEngine.Vector2 UIColorPicker::mPos
	Vector2_t2156229523  ___mPos_10;
	// System.Int32 UIColorPicker::mWidth
	int32_t ___mWidth_11;
	// System.Int32 UIColorPicker::mHeight
	int32_t ___mHeight_12;

public:
	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___value_3)); }
	inline Color_t2555686324  get_value_3() const { return ___value_3; }
	inline Color_t2555686324 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Color_t2555686324  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_selectionWidget_4() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___selectionWidget_4)); }
	inline UIWidget_t3538521925 * get_selectionWidget_4() const { return ___selectionWidget_4; }
	inline UIWidget_t3538521925 ** get_address_of_selectionWidget_4() { return &___selectionWidget_4; }
	inline void set_selectionWidget_4(UIWidget_t3538521925 * value)
	{
		___selectionWidget_4 = value;
		Il2CppCodeGenWriteBarrier((&___selectionWidget_4), value);
	}

	inline static int32_t get_offset_of_onChange_5() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___onChange_5)); }
	inline List_1_t4210400802 * get_onChange_5() const { return ___onChange_5; }
	inline List_1_t4210400802 ** get_address_of_onChange_5() { return &___onChange_5; }
	inline void set_onChange_5(List_1_t4210400802 * value)
	{
		___onChange_5 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_5), value);
	}

	inline static int32_t get_offset_of_mTrans_6() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___mTrans_6)); }
	inline Transform_t3600365921 * get_mTrans_6() const { return ___mTrans_6; }
	inline Transform_t3600365921 ** get_address_of_mTrans_6() { return &___mTrans_6; }
	inline void set_mTrans_6(Transform_t3600365921 * value)
	{
		___mTrans_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_6), value);
	}

	inline static int32_t get_offset_of_mUITex_7() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___mUITex_7)); }
	inline UITexture_t3471168817 * get_mUITex_7() const { return ___mUITex_7; }
	inline UITexture_t3471168817 ** get_address_of_mUITex_7() { return &___mUITex_7; }
	inline void set_mUITex_7(UITexture_t3471168817 * value)
	{
		___mUITex_7 = value;
		Il2CppCodeGenWriteBarrier((&___mUITex_7), value);
	}

	inline static int32_t get_offset_of_mTex_8() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___mTex_8)); }
	inline Texture2D_t3840446185 * get_mTex_8() const { return ___mTex_8; }
	inline Texture2D_t3840446185 ** get_address_of_mTex_8() { return &___mTex_8; }
	inline void set_mTex_8(Texture2D_t3840446185 * value)
	{
		___mTex_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTex_8), value);
	}

	inline static int32_t get_offset_of_mCam_9() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___mCam_9)); }
	inline UICamera_t1356438871 * get_mCam_9() const { return ___mCam_9; }
	inline UICamera_t1356438871 ** get_address_of_mCam_9() { return &___mCam_9; }
	inline void set_mCam_9(UICamera_t1356438871 * value)
	{
		___mCam_9 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_9), value);
	}

	inline static int32_t get_offset_of_mPos_10() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___mPos_10)); }
	inline Vector2_t2156229523  get_mPos_10() const { return ___mPos_10; }
	inline Vector2_t2156229523 * get_address_of_mPos_10() { return &___mPos_10; }
	inline void set_mPos_10(Vector2_t2156229523  value)
	{
		___mPos_10 = value;
	}

	inline static int32_t get_offset_of_mWidth_11() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___mWidth_11)); }
	inline int32_t get_mWidth_11() const { return ___mWidth_11; }
	inline int32_t* get_address_of_mWidth_11() { return &___mWidth_11; }
	inline void set_mWidth_11(int32_t value)
	{
		___mWidth_11 = value;
	}

	inline static int32_t get_offset_of_mHeight_12() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602, ___mHeight_12)); }
	inline int32_t get_mHeight_12() const { return ___mHeight_12; }
	inline int32_t* get_address_of_mHeight_12() { return &___mHeight_12; }
	inline void set_mHeight_12(int32_t value)
	{
		___mHeight_12 = value;
	}
};

struct UIColorPicker_t463494602_StaticFields
{
public:
	// UIColorPicker UIColorPicker::current
	UIColorPicker_t463494602 * ___current_2;
	// UnityEngine.AnimationCurve UIColorPicker::mRed
	AnimationCurve_t3046754366 * ___mRed_13;
	// UnityEngine.AnimationCurve UIColorPicker::mGreen
	AnimationCurve_t3046754366 * ___mGreen_14;
	// UnityEngine.AnimationCurve UIColorPicker::mBlue
	AnimationCurve_t3046754366 * ___mBlue_15;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602_StaticFields, ___current_2)); }
	inline UIColorPicker_t463494602 * get_current_2() const { return ___current_2; }
	inline UIColorPicker_t463494602 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIColorPicker_t463494602 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}

	inline static int32_t get_offset_of_mRed_13() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602_StaticFields, ___mRed_13)); }
	inline AnimationCurve_t3046754366 * get_mRed_13() const { return ___mRed_13; }
	inline AnimationCurve_t3046754366 ** get_address_of_mRed_13() { return &___mRed_13; }
	inline void set_mRed_13(AnimationCurve_t3046754366 * value)
	{
		___mRed_13 = value;
		Il2CppCodeGenWriteBarrier((&___mRed_13), value);
	}

	inline static int32_t get_offset_of_mGreen_14() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602_StaticFields, ___mGreen_14)); }
	inline AnimationCurve_t3046754366 * get_mGreen_14() const { return ___mGreen_14; }
	inline AnimationCurve_t3046754366 ** get_address_of_mGreen_14() { return &___mGreen_14; }
	inline void set_mGreen_14(AnimationCurve_t3046754366 * value)
	{
		___mGreen_14 = value;
		Il2CppCodeGenWriteBarrier((&___mGreen_14), value);
	}

	inline static int32_t get_offset_of_mBlue_15() { return static_cast<int32_t>(offsetof(UIColorPicker_t463494602_StaticFields, ___mBlue_15)); }
	inline AnimationCurve_t3046754366 * get_mBlue_15() const { return ___mBlue_15; }
	inline AnimationCurve_t3046754366 ** get_address_of_mBlue_15() { return &___mBlue_15; }
	inline void set_mBlue_15(AnimationCurve_t3046754366 * value)
	{
		___mBlue_15 = value;
		Il2CppCodeGenWriteBarrier((&___mBlue_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOLORPICKER_T463494602_H
#ifndef MONOBEHAVIOURWITHINIT_T1117120792_H
#define MONOBEHAVIOURWITHINIT_T1117120792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoBehaviourWithInit
struct  MonoBehaviourWithInit_t1117120792  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MonoBehaviourWithInit::_isInitialized
	bool ____isInitialized_2;

public:
	inline static int32_t get_offset_of__isInitialized_2() { return static_cast<int32_t>(offsetof(MonoBehaviourWithInit_t1117120792, ____isInitialized_2)); }
	inline bool get__isInitialized_2() const { return ____isInitialized_2; }
	inline bool* get_address_of__isInitialized_2() { return &____isInitialized_2; }
	inline void set__isInitialized_2(bool value)
	{
		____isInitialized_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURWITHINIT_T1117120792_H
#ifndef LEANFINGERSWIPE_T3060022393_H
#define LEANFINGERSWIPE_T3060022393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerSwipe
struct  LeanFingerSwipe_t3060022393  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Lean.Touch.LeanFingerSwipe::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_2;
	// System.Boolean Lean.Touch.LeanFingerSwipe::CheckAngle
	bool ___CheckAngle_3;
	// System.Single Lean.Touch.LeanFingerSwipe::Angle
	float ___Angle_4;
	// System.Single Lean.Touch.LeanFingerSwipe::AngleThreshold
	float ___AngleThreshold_5;
	// Lean.Touch.LeanFingerSwipe/FingerEvent Lean.Touch.LeanFingerSwipe::OnFingerSwipe
	FingerEvent_t2652301007 * ___OnFingerSwipe_6;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_2() { return static_cast<int32_t>(offsetof(LeanFingerSwipe_t3060022393, ___IgnoreGuiFingers_2)); }
	inline bool get_IgnoreGuiFingers_2() const { return ___IgnoreGuiFingers_2; }
	inline bool* get_address_of_IgnoreGuiFingers_2() { return &___IgnoreGuiFingers_2; }
	inline void set_IgnoreGuiFingers_2(bool value)
	{
		___IgnoreGuiFingers_2 = value;
	}

	inline static int32_t get_offset_of_CheckAngle_3() { return static_cast<int32_t>(offsetof(LeanFingerSwipe_t3060022393, ___CheckAngle_3)); }
	inline bool get_CheckAngle_3() const { return ___CheckAngle_3; }
	inline bool* get_address_of_CheckAngle_3() { return &___CheckAngle_3; }
	inline void set_CheckAngle_3(bool value)
	{
		___CheckAngle_3 = value;
	}

	inline static int32_t get_offset_of_Angle_4() { return static_cast<int32_t>(offsetof(LeanFingerSwipe_t3060022393, ___Angle_4)); }
	inline float get_Angle_4() const { return ___Angle_4; }
	inline float* get_address_of_Angle_4() { return &___Angle_4; }
	inline void set_Angle_4(float value)
	{
		___Angle_4 = value;
	}

	inline static int32_t get_offset_of_AngleThreshold_5() { return static_cast<int32_t>(offsetof(LeanFingerSwipe_t3060022393, ___AngleThreshold_5)); }
	inline float get_AngleThreshold_5() const { return ___AngleThreshold_5; }
	inline float* get_address_of_AngleThreshold_5() { return &___AngleThreshold_5; }
	inline void set_AngleThreshold_5(float value)
	{
		___AngleThreshold_5 = value;
	}

	inline static int32_t get_offset_of_OnFingerSwipe_6() { return static_cast<int32_t>(offsetof(LeanFingerSwipe_t3060022393, ___OnFingerSwipe_6)); }
	inline FingerEvent_t2652301007 * get_OnFingerSwipe_6() const { return ___OnFingerSwipe_6; }
	inline FingerEvent_t2652301007 ** get_address_of_OnFingerSwipe_6() { return &___OnFingerSwipe_6; }
	inline void set_OnFingerSwipe_6(FingerEvent_t2652301007 * value)
	{
		___OnFingerSwipe_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerSwipe_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGERSWIPE_T3060022393_H
#ifndef RANKINGLABEL_T3462582456_H
#define RANKINGLABEL_T3462582456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RankingLabel
struct  RankingLabel_t3462582456  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RankingLabel::frame
	GameObject_t1113636619 * ___frame_2;
	// UILabel RankingLabel::lbl
	UILabel_t3248798549 * ___lbl_3;
	// System.Int32 RankingLabel::lastRank
	int32_t ___lastRank_4;

public:
	inline static int32_t get_offset_of_frame_2() { return static_cast<int32_t>(offsetof(RankingLabel_t3462582456, ___frame_2)); }
	inline GameObject_t1113636619 * get_frame_2() const { return ___frame_2; }
	inline GameObject_t1113636619 ** get_address_of_frame_2() { return &___frame_2; }
	inline void set_frame_2(GameObject_t1113636619 * value)
	{
		___frame_2 = value;
		Il2CppCodeGenWriteBarrier((&___frame_2), value);
	}

	inline static int32_t get_offset_of_lbl_3() { return static_cast<int32_t>(offsetof(RankingLabel_t3462582456, ___lbl_3)); }
	inline UILabel_t3248798549 * get_lbl_3() const { return ___lbl_3; }
	inline UILabel_t3248798549 ** get_address_of_lbl_3() { return &___lbl_3; }
	inline void set_lbl_3(UILabel_t3248798549 * value)
	{
		___lbl_3 = value;
		Il2CppCodeGenWriteBarrier((&___lbl_3), value);
	}

	inline static int32_t get_offset_of_lastRank_4() { return static_cast<int32_t>(offsetof(RankingLabel_t3462582456, ___lastRank_4)); }
	inline int32_t get_lastRank_4() const { return ___lastRank_4; }
	inline int32_t* get_address_of_lastRank_4() { return &___lastRank_4; }
	inline void set_lastRank_4(int32_t value)
	{
		___lastRank_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKINGLABEL_T3462582456_H
#ifndef MOUNTAIN_T1989270499_H
#define MOUNTAIN_T1989270499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mountain
struct  Mountain_t1989270499  : public MonoBehaviour_t3962482529
{
public:
	// UISprite Mountain::_sprite
	UISprite_t194114938 * ____sprite_2;
	// System.String Mountain::_name
	String_t* ____name_3;

public:
	inline static int32_t get_offset_of__sprite_2() { return static_cast<int32_t>(offsetof(Mountain_t1989270499, ____sprite_2)); }
	inline UISprite_t194114938 * get__sprite_2() const { return ____sprite_2; }
	inline UISprite_t194114938 ** get_address_of__sprite_2() { return &____sprite_2; }
	inline void set__sprite_2(UISprite_t194114938 * value)
	{
		____sprite_2 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_2), value);
	}

	inline static int32_t get_offset_of__name_3() { return static_cast<int32_t>(offsetof(Mountain_t1989270499, ____name_3)); }
	inline String_t* get__name_3() const { return ____name_3; }
	inline String_t** get_address_of__name_3() { return &____name_3; }
	inline void set__name_3(String_t* value)
	{
		____name_3 = value;
		Il2CppCodeGenWriteBarrier((&____name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUNTAIN_T1989270499_H
#ifndef LOCALIZEIMAGE_T2397934916_H
#define LOCALIZEIMAGE_T2397934916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizeImage
struct  LocalizeImage_t2397934916  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LocalizeImage::NGUI
	bool ___NGUI_2;
	// System.String LocalizeImage::japanNameNGUI
	String_t* ___japanNameNGUI_3;
	// UnityEngine.Sprite LocalizeImage::japanName
	Sprite_t280657092 * ___japanName_4;
	// System.Boolean LocalizeImage::setNativeSize
	bool ___setNativeSize_5;
	// UnityEngine.UI.Image LocalizeImage::img
	Image_t2670269651 * ___img_6;
	// UISprite LocalizeImage::sprite
	UISprite_t194114938 * ___sprite_7;

public:
	inline static int32_t get_offset_of_NGUI_2() { return static_cast<int32_t>(offsetof(LocalizeImage_t2397934916, ___NGUI_2)); }
	inline bool get_NGUI_2() const { return ___NGUI_2; }
	inline bool* get_address_of_NGUI_2() { return &___NGUI_2; }
	inline void set_NGUI_2(bool value)
	{
		___NGUI_2 = value;
	}

	inline static int32_t get_offset_of_japanNameNGUI_3() { return static_cast<int32_t>(offsetof(LocalizeImage_t2397934916, ___japanNameNGUI_3)); }
	inline String_t* get_japanNameNGUI_3() const { return ___japanNameNGUI_3; }
	inline String_t** get_address_of_japanNameNGUI_3() { return &___japanNameNGUI_3; }
	inline void set_japanNameNGUI_3(String_t* value)
	{
		___japanNameNGUI_3 = value;
		Il2CppCodeGenWriteBarrier((&___japanNameNGUI_3), value);
	}

	inline static int32_t get_offset_of_japanName_4() { return static_cast<int32_t>(offsetof(LocalizeImage_t2397934916, ___japanName_4)); }
	inline Sprite_t280657092 * get_japanName_4() const { return ___japanName_4; }
	inline Sprite_t280657092 ** get_address_of_japanName_4() { return &___japanName_4; }
	inline void set_japanName_4(Sprite_t280657092 * value)
	{
		___japanName_4 = value;
		Il2CppCodeGenWriteBarrier((&___japanName_4), value);
	}

	inline static int32_t get_offset_of_setNativeSize_5() { return static_cast<int32_t>(offsetof(LocalizeImage_t2397934916, ___setNativeSize_5)); }
	inline bool get_setNativeSize_5() const { return ___setNativeSize_5; }
	inline bool* get_address_of_setNativeSize_5() { return &___setNativeSize_5; }
	inline void set_setNativeSize_5(bool value)
	{
		___setNativeSize_5 = value;
	}

	inline static int32_t get_offset_of_img_6() { return static_cast<int32_t>(offsetof(LocalizeImage_t2397934916, ___img_6)); }
	inline Image_t2670269651 * get_img_6() const { return ___img_6; }
	inline Image_t2670269651 ** get_address_of_img_6() { return &___img_6; }
	inline void set_img_6(Image_t2670269651 * value)
	{
		___img_6 = value;
		Il2CppCodeGenWriteBarrier((&___img_6), value);
	}

	inline static int32_t get_offset_of_sprite_7() { return static_cast<int32_t>(offsetof(LocalizeImage_t2397934916, ___sprite_7)); }
	inline UISprite_t194114938 * get_sprite_7() const { return ___sprite_7; }
	inline UISprite_t194114938 ** get_address_of_sprite_7() { return &___sprite_7; }
	inline void set_sprite_7(UISprite_t194114938 * value)
	{
		___sprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEIMAGE_T2397934916_H
#ifndef LEANTOUCH_T2951860335_H
#define LEANTOUCH_T2951860335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanTouch
struct  LeanTouch_t2951860335  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Lean.Touch.LeanTouch::TapThreshold
	float ___TapThreshold_11;
	// System.Single Lean.Touch.LeanTouch::SwipeThreshold
	float ___SwipeThreshold_13;
	// System.Int32 Lean.Touch.LeanTouch::ReferenceDpi
	int32_t ___ReferenceDpi_15;
	// UnityEngine.LayerMask Lean.Touch.LeanTouch::GuiLayers
	LayerMask_t3493934918  ___GuiLayers_17;
	// System.Boolean Lean.Touch.LeanTouch::RecordFingers
	bool ___RecordFingers_18;
	// System.Single Lean.Touch.LeanTouch::RecordThreshold
	float ___RecordThreshold_19;
	// System.Single Lean.Touch.LeanTouch::RecordLimit
	float ___RecordLimit_20;
	// System.Boolean Lean.Touch.LeanTouch::SimulateMultiFingers
	bool ___SimulateMultiFingers_21;
	// UnityEngine.KeyCode Lean.Touch.LeanTouch::PinchTwistKey
	int32_t ___PinchTwistKey_22;
	// UnityEngine.KeyCode Lean.Touch.LeanTouch::MultiDragKey
	int32_t ___MultiDragKey_23;
	// UnityEngine.Texture2D Lean.Touch.LeanTouch::FingerTexture
	Texture2D_t3840446185 * ___FingerTexture_24;

public:
	inline static int32_t get_offset_of_TapThreshold_11() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___TapThreshold_11)); }
	inline float get_TapThreshold_11() const { return ___TapThreshold_11; }
	inline float* get_address_of_TapThreshold_11() { return &___TapThreshold_11; }
	inline void set_TapThreshold_11(float value)
	{
		___TapThreshold_11 = value;
	}

	inline static int32_t get_offset_of_SwipeThreshold_13() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___SwipeThreshold_13)); }
	inline float get_SwipeThreshold_13() const { return ___SwipeThreshold_13; }
	inline float* get_address_of_SwipeThreshold_13() { return &___SwipeThreshold_13; }
	inline void set_SwipeThreshold_13(float value)
	{
		___SwipeThreshold_13 = value;
	}

	inline static int32_t get_offset_of_ReferenceDpi_15() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___ReferenceDpi_15)); }
	inline int32_t get_ReferenceDpi_15() const { return ___ReferenceDpi_15; }
	inline int32_t* get_address_of_ReferenceDpi_15() { return &___ReferenceDpi_15; }
	inline void set_ReferenceDpi_15(int32_t value)
	{
		___ReferenceDpi_15 = value;
	}

	inline static int32_t get_offset_of_GuiLayers_17() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___GuiLayers_17)); }
	inline LayerMask_t3493934918  get_GuiLayers_17() const { return ___GuiLayers_17; }
	inline LayerMask_t3493934918 * get_address_of_GuiLayers_17() { return &___GuiLayers_17; }
	inline void set_GuiLayers_17(LayerMask_t3493934918  value)
	{
		___GuiLayers_17 = value;
	}

	inline static int32_t get_offset_of_RecordFingers_18() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___RecordFingers_18)); }
	inline bool get_RecordFingers_18() const { return ___RecordFingers_18; }
	inline bool* get_address_of_RecordFingers_18() { return &___RecordFingers_18; }
	inline void set_RecordFingers_18(bool value)
	{
		___RecordFingers_18 = value;
	}

	inline static int32_t get_offset_of_RecordThreshold_19() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___RecordThreshold_19)); }
	inline float get_RecordThreshold_19() const { return ___RecordThreshold_19; }
	inline float* get_address_of_RecordThreshold_19() { return &___RecordThreshold_19; }
	inline void set_RecordThreshold_19(float value)
	{
		___RecordThreshold_19 = value;
	}

	inline static int32_t get_offset_of_RecordLimit_20() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___RecordLimit_20)); }
	inline float get_RecordLimit_20() const { return ___RecordLimit_20; }
	inline float* get_address_of_RecordLimit_20() { return &___RecordLimit_20; }
	inline void set_RecordLimit_20(float value)
	{
		___RecordLimit_20 = value;
	}

	inline static int32_t get_offset_of_SimulateMultiFingers_21() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___SimulateMultiFingers_21)); }
	inline bool get_SimulateMultiFingers_21() const { return ___SimulateMultiFingers_21; }
	inline bool* get_address_of_SimulateMultiFingers_21() { return &___SimulateMultiFingers_21; }
	inline void set_SimulateMultiFingers_21(bool value)
	{
		___SimulateMultiFingers_21 = value;
	}

	inline static int32_t get_offset_of_PinchTwistKey_22() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___PinchTwistKey_22)); }
	inline int32_t get_PinchTwistKey_22() const { return ___PinchTwistKey_22; }
	inline int32_t* get_address_of_PinchTwistKey_22() { return &___PinchTwistKey_22; }
	inline void set_PinchTwistKey_22(int32_t value)
	{
		___PinchTwistKey_22 = value;
	}

	inline static int32_t get_offset_of_MultiDragKey_23() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___MultiDragKey_23)); }
	inline int32_t get_MultiDragKey_23() const { return ___MultiDragKey_23; }
	inline int32_t* get_address_of_MultiDragKey_23() { return &___MultiDragKey_23; }
	inline void set_MultiDragKey_23(int32_t value)
	{
		___MultiDragKey_23 = value;
	}

	inline static int32_t get_offset_of_FingerTexture_24() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335, ___FingerTexture_24)); }
	inline Texture2D_t3840446185 * get_FingerTexture_24() const { return ___FingerTexture_24; }
	inline Texture2D_t3840446185 ** get_address_of_FingerTexture_24() { return &___FingerTexture_24; }
	inline void set_FingerTexture_24(Texture2D_t3840446185 * value)
	{
		___FingerTexture_24 = value;
		Il2CppCodeGenWriteBarrier((&___FingerTexture_24), value);
	}
};

struct LeanTouch_t2951860335_StaticFields
{
public:
	// System.Collections.Generic.List`1<Lean.Touch.LeanTouch> Lean.Touch.LeanTouch::Instances
	List_1_t128967781 * ___Instances_2;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::Fingers
	List_1_t683400304 * ___Fingers_3;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::InactiveFingers
	List_1_t683400304 * ___InactiveFingers_4;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerDown
	Action_1_t3678760453 * ___OnFingerDown_5;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerSet
	Action_1_t3678760453 * ___OnFingerSet_6;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerUp
	Action_1_t3678760453 * ___OnFingerUp_7;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerTap
	Action_1_t3678760453 * ___OnFingerTap_8;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerSwipe
	Action_1_t3678760453 * ___OnFingerSwipe_9;
	// System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>> Lean.Touch.LeanTouch::OnGesture
	Action_1_t855867899 * ___OnGesture_10;
	// System.Int32 Lean.Touch.LeanTouch::highestMouseButton
	int32_t ___highestMouseButton_25;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::tempRaycastResults
	List_1_t537414295 * ___tempRaycastResults_26;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::filteredFingers
	List_1_t683400304 * ___filteredFingers_27;
	// UnityEngine.EventSystems.PointerEventData Lean.Touch.LeanTouch::tempPointerEventData
	PointerEventData_t3807901092 * ___tempPointerEventData_28;
	// UnityEngine.EventSystems.EventSystem Lean.Touch.LeanTouch::tempEventSystem
	EventSystem_t1003666588 * ___tempEventSystem_29;

public:
	inline static int32_t get_offset_of_Instances_2() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___Instances_2)); }
	inline List_1_t128967781 * get_Instances_2() const { return ___Instances_2; }
	inline List_1_t128967781 ** get_address_of_Instances_2() { return &___Instances_2; }
	inline void set_Instances_2(List_1_t128967781 * value)
	{
		___Instances_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instances_2), value);
	}

	inline static int32_t get_offset_of_Fingers_3() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___Fingers_3)); }
	inline List_1_t683400304 * get_Fingers_3() const { return ___Fingers_3; }
	inline List_1_t683400304 ** get_address_of_Fingers_3() { return &___Fingers_3; }
	inline void set_Fingers_3(List_1_t683400304 * value)
	{
		___Fingers_3 = value;
		Il2CppCodeGenWriteBarrier((&___Fingers_3), value);
	}

	inline static int32_t get_offset_of_InactiveFingers_4() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___InactiveFingers_4)); }
	inline List_1_t683400304 * get_InactiveFingers_4() const { return ___InactiveFingers_4; }
	inline List_1_t683400304 ** get_address_of_InactiveFingers_4() { return &___InactiveFingers_4; }
	inline void set_InactiveFingers_4(List_1_t683400304 * value)
	{
		___InactiveFingers_4 = value;
		Il2CppCodeGenWriteBarrier((&___InactiveFingers_4), value);
	}

	inline static int32_t get_offset_of_OnFingerDown_5() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___OnFingerDown_5)); }
	inline Action_1_t3678760453 * get_OnFingerDown_5() const { return ___OnFingerDown_5; }
	inline Action_1_t3678760453 ** get_address_of_OnFingerDown_5() { return &___OnFingerDown_5; }
	inline void set_OnFingerDown_5(Action_1_t3678760453 * value)
	{
		___OnFingerDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerDown_5), value);
	}

	inline static int32_t get_offset_of_OnFingerSet_6() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___OnFingerSet_6)); }
	inline Action_1_t3678760453 * get_OnFingerSet_6() const { return ___OnFingerSet_6; }
	inline Action_1_t3678760453 ** get_address_of_OnFingerSet_6() { return &___OnFingerSet_6; }
	inline void set_OnFingerSet_6(Action_1_t3678760453 * value)
	{
		___OnFingerSet_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerSet_6), value);
	}

	inline static int32_t get_offset_of_OnFingerUp_7() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___OnFingerUp_7)); }
	inline Action_1_t3678760453 * get_OnFingerUp_7() const { return ___OnFingerUp_7; }
	inline Action_1_t3678760453 ** get_address_of_OnFingerUp_7() { return &___OnFingerUp_7; }
	inline void set_OnFingerUp_7(Action_1_t3678760453 * value)
	{
		___OnFingerUp_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerUp_7), value);
	}

	inline static int32_t get_offset_of_OnFingerTap_8() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___OnFingerTap_8)); }
	inline Action_1_t3678760453 * get_OnFingerTap_8() const { return ___OnFingerTap_8; }
	inline Action_1_t3678760453 ** get_address_of_OnFingerTap_8() { return &___OnFingerTap_8; }
	inline void set_OnFingerTap_8(Action_1_t3678760453 * value)
	{
		___OnFingerTap_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerTap_8), value);
	}

	inline static int32_t get_offset_of_OnFingerSwipe_9() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___OnFingerSwipe_9)); }
	inline Action_1_t3678760453 * get_OnFingerSwipe_9() const { return ___OnFingerSwipe_9; }
	inline Action_1_t3678760453 ** get_address_of_OnFingerSwipe_9() { return &___OnFingerSwipe_9; }
	inline void set_OnFingerSwipe_9(Action_1_t3678760453 * value)
	{
		___OnFingerSwipe_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerSwipe_9), value);
	}

	inline static int32_t get_offset_of_OnGesture_10() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___OnGesture_10)); }
	inline Action_1_t855867899 * get_OnGesture_10() const { return ___OnGesture_10; }
	inline Action_1_t855867899 ** get_address_of_OnGesture_10() { return &___OnGesture_10; }
	inline void set_OnGesture_10(Action_1_t855867899 * value)
	{
		___OnGesture_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnGesture_10), value);
	}

	inline static int32_t get_offset_of_highestMouseButton_25() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___highestMouseButton_25)); }
	inline int32_t get_highestMouseButton_25() const { return ___highestMouseButton_25; }
	inline int32_t* get_address_of_highestMouseButton_25() { return &___highestMouseButton_25; }
	inline void set_highestMouseButton_25(int32_t value)
	{
		___highestMouseButton_25 = value;
	}

	inline static int32_t get_offset_of_tempRaycastResults_26() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___tempRaycastResults_26)); }
	inline List_1_t537414295 * get_tempRaycastResults_26() const { return ___tempRaycastResults_26; }
	inline List_1_t537414295 ** get_address_of_tempRaycastResults_26() { return &___tempRaycastResults_26; }
	inline void set_tempRaycastResults_26(List_1_t537414295 * value)
	{
		___tempRaycastResults_26 = value;
		Il2CppCodeGenWriteBarrier((&___tempRaycastResults_26), value);
	}

	inline static int32_t get_offset_of_filteredFingers_27() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___filteredFingers_27)); }
	inline List_1_t683400304 * get_filteredFingers_27() const { return ___filteredFingers_27; }
	inline List_1_t683400304 ** get_address_of_filteredFingers_27() { return &___filteredFingers_27; }
	inline void set_filteredFingers_27(List_1_t683400304 * value)
	{
		___filteredFingers_27 = value;
		Il2CppCodeGenWriteBarrier((&___filteredFingers_27), value);
	}

	inline static int32_t get_offset_of_tempPointerEventData_28() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___tempPointerEventData_28)); }
	inline PointerEventData_t3807901092 * get_tempPointerEventData_28() const { return ___tempPointerEventData_28; }
	inline PointerEventData_t3807901092 ** get_address_of_tempPointerEventData_28() { return &___tempPointerEventData_28; }
	inline void set_tempPointerEventData_28(PointerEventData_t3807901092 * value)
	{
		___tempPointerEventData_28 = value;
		Il2CppCodeGenWriteBarrier((&___tempPointerEventData_28), value);
	}

	inline static int32_t get_offset_of_tempEventSystem_29() { return static_cast<int32_t>(offsetof(LeanTouch_t2951860335_StaticFields, ___tempEventSystem_29)); }
	inline EventSystem_t1003666588 * get_tempEventSystem_29() const { return ___tempEventSystem_29; }
	inline EventSystem_t1003666588 ** get_address_of_tempEventSystem_29() { return &___tempEventSystem_29; }
	inline void set_tempEventSystem_29(EventSystem_t1003666588 * value)
	{
		___tempEventSystem_29 = value;
		Il2CppCodeGenWriteBarrier((&___tempEventSystem_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTOUCH_T2951860335_H
#ifndef LEANSELECTABLE_T2178850769_H
#define LEANSELECTABLE_T2178850769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectable
struct  LeanSelectable_t2178850769  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Lean.Touch.LeanSelectable::HideWithFinger
	bool ___HideWithFinger_3;
	// Lean.Touch.LeanFinger Lean.Touch.LeanSelectable::SelectingFinger
	LeanFinger_t3506292858 * ___SelectingFinger_4;
	// Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::OnSelect
	LeanFingerEvent_t821904700 * ___OnSelect_5;
	// Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::OnSelectUp
	LeanFingerEvent_t821904700 * ___OnSelectUp_6;
	// UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectable::OnDeselect
	UnityEvent_t2581268647 * ___OnDeselect_7;
	// System.Boolean Lean.Touch.LeanSelectable::isSelected
	bool ___isSelected_8;

public:
	inline static int32_t get_offset_of_HideWithFinger_3() { return static_cast<int32_t>(offsetof(LeanSelectable_t2178850769, ___HideWithFinger_3)); }
	inline bool get_HideWithFinger_3() const { return ___HideWithFinger_3; }
	inline bool* get_address_of_HideWithFinger_3() { return &___HideWithFinger_3; }
	inline void set_HideWithFinger_3(bool value)
	{
		___HideWithFinger_3 = value;
	}

	inline static int32_t get_offset_of_SelectingFinger_4() { return static_cast<int32_t>(offsetof(LeanSelectable_t2178850769, ___SelectingFinger_4)); }
	inline LeanFinger_t3506292858 * get_SelectingFinger_4() const { return ___SelectingFinger_4; }
	inline LeanFinger_t3506292858 ** get_address_of_SelectingFinger_4() { return &___SelectingFinger_4; }
	inline void set_SelectingFinger_4(LeanFinger_t3506292858 * value)
	{
		___SelectingFinger_4 = value;
		Il2CppCodeGenWriteBarrier((&___SelectingFinger_4), value);
	}

	inline static int32_t get_offset_of_OnSelect_5() { return static_cast<int32_t>(offsetof(LeanSelectable_t2178850769, ___OnSelect_5)); }
	inline LeanFingerEvent_t821904700 * get_OnSelect_5() const { return ___OnSelect_5; }
	inline LeanFingerEvent_t821904700 ** get_address_of_OnSelect_5() { return &___OnSelect_5; }
	inline void set_OnSelect_5(LeanFingerEvent_t821904700 * value)
	{
		___OnSelect_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelect_5), value);
	}

	inline static int32_t get_offset_of_OnSelectUp_6() { return static_cast<int32_t>(offsetof(LeanSelectable_t2178850769, ___OnSelectUp_6)); }
	inline LeanFingerEvent_t821904700 * get_OnSelectUp_6() const { return ___OnSelectUp_6; }
	inline LeanFingerEvent_t821904700 ** get_address_of_OnSelectUp_6() { return &___OnSelectUp_6; }
	inline void set_OnSelectUp_6(LeanFingerEvent_t821904700 * value)
	{
		___OnSelectUp_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectUp_6), value);
	}

	inline static int32_t get_offset_of_OnDeselect_7() { return static_cast<int32_t>(offsetof(LeanSelectable_t2178850769, ___OnDeselect_7)); }
	inline UnityEvent_t2581268647 * get_OnDeselect_7() const { return ___OnDeselect_7; }
	inline UnityEvent_t2581268647 ** get_address_of_OnDeselect_7() { return &___OnDeselect_7; }
	inline void set_OnDeselect_7(UnityEvent_t2581268647 * value)
	{
		___OnDeselect_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeselect_7), value);
	}

	inline static int32_t get_offset_of_isSelected_8() { return static_cast<int32_t>(offsetof(LeanSelectable_t2178850769, ___isSelected_8)); }
	inline bool get_isSelected_8() const { return ___isSelected_8; }
	inline bool* get_address_of_isSelected_8() { return &___isSelected_8; }
	inline void set_isSelected_8(bool value)
	{
		___isSelected_8 = value;
	}
};

struct LeanSelectable_t2178850769_StaticFields
{
public:
	// System.Collections.Generic.List`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanSelectable::Instances
	List_1_t3650925511 * ___Instances_2;

public:
	inline static int32_t get_offset_of_Instances_2() { return static_cast<int32_t>(offsetof(LeanSelectable_t2178850769_StaticFields, ___Instances_2)); }
	inline List_1_t3650925511 * get_Instances_2() const { return ___Instances_2; }
	inline List_1_t3650925511 ** get_address_of_Instances_2() { return &___Instances_2; }
	inline void set_Instances_2(List_1_t3650925511 * value)
	{
		___Instances_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instances_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECTABLE_T2178850769_H
#ifndef UIRECT_T2875960382_H
#define UIRECT_T2875960382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRect
struct  UIRect_t2875960382  : public MonoBehaviour_t3962482529
{
public:
	// UIRect/AnchorPoint UIRect::leftAnchor
	AnchorPoint_t1754718329 * ___leftAnchor_2;
	// UIRect/AnchorPoint UIRect::rightAnchor
	AnchorPoint_t1754718329 * ___rightAnchor_3;
	// UIRect/AnchorPoint UIRect::bottomAnchor
	AnchorPoint_t1754718329 * ___bottomAnchor_4;
	// UIRect/AnchorPoint UIRect::topAnchor
	AnchorPoint_t1754718329 * ___topAnchor_5;
	// UIRect/AnchorUpdate UIRect::updateAnchors
	int32_t ___updateAnchors_6;
	// UnityEngine.GameObject UIRect::mGo
	GameObject_t1113636619 * ___mGo_7;
	// UnityEngine.Transform UIRect::mTrans
	Transform_t3600365921 * ___mTrans_8;
	// BetterList`1<UIRect> UIRect::mChildren
	BetterList_1_t2030980700 * ___mChildren_9;
	// System.Boolean UIRect::mChanged
	bool ___mChanged_10;
	// System.Boolean UIRect::mParentFound
	bool ___mParentFound_11;
	// System.Boolean UIRect::mUpdateAnchors
	bool ___mUpdateAnchors_12;
	// System.Int32 UIRect::mUpdateFrame
	int32_t ___mUpdateFrame_13;
	// System.Boolean UIRect::mAnchorsCached
	bool ___mAnchorsCached_14;
	// UIRoot UIRect::mRoot
	UIRoot_t4022971450 * ___mRoot_15;
	// UIRect UIRect::mParent
	UIRect_t2875960382 * ___mParent_16;
	// System.Boolean UIRect::mRootSet
	bool ___mRootSet_17;
	// UnityEngine.Camera UIRect::mCam
	Camera_t4157153871 * ___mCam_18;
	// System.Boolean UIRect::mStarted
	bool ___mStarted_19;
	// System.Single UIRect::finalAlpha
	float ___finalAlpha_20;

public:
	inline static int32_t get_offset_of_leftAnchor_2() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___leftAnchor_2)); }
	inline AnchorPoint_t1754718329 * get_leftAnchor_2() const { return ___leftAnchor_2; }
	inline AnchorPoint_t1754718329 ** get_address_of_leftAnchor_2() { return &___leftAnchor_2; }
	inline void set_leftAnchor_2(AnchorPoint_t1754718329 * value)
	{
		___leftAnchor_2 = value;
		Il2CppCodeGenWriteBarrier((&___leftAnchor_2), value);
	}

	inline static int32_t get_offset_of_rightAnchor_3() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___rightAnchor_3)); }
	inline AnchorPoint_t1754718329 * get_rightAnchor_3() const { return ___rightAnchor_3; }
	inline AnchorPoint_t1754718329 ** get_address_of_rightAnchor_3() { return &___rightAnchor_3; }
	inline void set_rightAnchor_3(AnchorPoint_t1754718329 * value)
	{
		___rightAnchor_3 = value;
		Il2CppCodeGenWriteBarrier((&___rightAnchor_3), value);
	}

	inline static int32_t get_offset_of_bottomAnchor_4() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___bottomAnchor_4)); }
	inline AnchorPoint_t1754718329 * get_bottomAnchor_4() const { return ___bottomAnchor_4; }
	inline AnchorPoint_t1754718329 ** get_address_of_bottomAnchor_4() { return &___bottomAnchor_4; }
	inline void set_bottomAnchor_4(AnchorPoint_t1754718329 * value)
	{
		___bottomAnchor_4 = value;
		Il2CppCodeGenWriteBarrier((&___bottomAnchor_4), value);
	}

	inline static int32_t get_offset_of_topAnchor_5() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___topAnchor_5)); }
	inline AnchorPoint_t1754718329 * get_topAnchor_5() const { return ___topAnchor_5; }
	inline AnchorPoint_t1754718329 ** get_address_of_topAnchor_5() { return &___topAnchor_5; }
	inline void set_topAnchor_5(AnchorPoint_t1754718329 * value)
	{
		___topAnchor_5 = value;
		Il2CppCodeGenWriteBarrier((&___topAnchor_5), value);
	}

	inline static int32_t get_offset_of_updateAnchors_6() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___updateAnchors_6)); }
	inline int32_t get_updateAnchors_6() const { return ___updateAnchors_6; }
	inline int32_t* get_address_of_updateAnchors_6() { return &___updateAnchors_6; }
	inline void set_updateAnchors_6(int32_t value)
	{
		___updateAnchors_6 = value;
	}

	inline static int32_t get_offset_of_mGo_7() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mGo_7)); }
	inline GameObject_t1113636619 * get_mGo_7() const { return ___mGo_7; }
	inline GameObject_t1113636619 ** get_address_of_mGo_7() { return &___mGo_7; }
	inline void set_mGo_7(GameObject_t1113636619 * value)
	{
		___mGo_7 = value;
		Il2CppCodeGenWriteBarrier((&___mGo_7), value);
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mTrans_8)); }
	inline Transform_t3600365921 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t3600365921 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t3600365921 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mChildren_9() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mChildren_9)); }
	inline BetterList_1_t2030980700 * get_mChildren_9() const { return ___mChildren_9; }
	inline BetterList_1_t2030980700 ** get_address_of_mChildren_9() { return &___mChildren_9; }
	inline void set_mChildren_9(BetterList_1_t2030980700 * value)
	{
		___mChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_9), value);
	}

	inline static int32_t get_offset_of_mChanged_10() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mChanged_10)); }
	inline bool get_mChanged_10() const { return ___mChanged_10; }
	inline bool* get_address_of_mChanged_10() { return &___mChanged_10; }
	inline void set_mChanged_10(bool value)
	{
		___mChanged_10 = value;
	}

	inline static int32_t get_offset_of_mParentFound_11() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mParentFound_11)); }
	inline bool get_mParentFound_11() const { return ___mParentFound_11; }
	inline bool* get_address_of_mParentFound_11() { return &___mParentFound_11; }
	inline void set_mParentFound_11(bool value)
	{
		___mParentFound_11 = value;
	}

	inline static int32_t get_offset_of_mUpdateAnchors_12() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mUpdateAnchors_12)); }
	inline bool get_mUpdateAnchors_12() const { return ___mUpdateAnchors_12; }
	inline bool* get_address_of_mUpdateAnchors_12() { return &___mUpdateAnchors_12; }
	inline void set_mUpdateAnchors_12(bool value)
	{
		___mUpdateAnchors_12 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrame_13() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mUpdateFrame_13)); }
	inline int32_t get_mUpdateFrame_13() const { return ___mUpdateFrame_13; }
	inline int32_t* get_address_of_mUpdateFrame_13() { return &___mUpdateFrame_13; }
	inline void set_mUpdateFrame_13(int32_t value)
	{
		___mUpdateFrame_13 = value;
	}

	inline static int32_t get_offset_of_mAnchorsCached_14() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mAnchorsCached_14)); }
	inline bool get_mAnchorsCached_14() const { return ___mAnchorsCached_14; }
	inline bool* get_address_of_mAnchorsCached_14() { return &___mAnchorsCached_14; }
	inline void set_mAnchorsCached_14(bool value)
	{
		___mAnchorsCached_14 = value;
	}

	inline static int32_t get_offset_of_mRoot_15() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mRoot_15)); }
	inline UIRoot_t4022971450 * get_mRoot_15() const { return ___mRoot_15; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_15() { return &___mRoot_15; }
	inline void set_mRoot_15(UIRoot_t4022971450 * value)
	{
		___mRoot_15 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_15), value);
	}

	inline static int32_t get_offset_of_mParent_16() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mParent_16)); }
	inline UIRect_t2875960382 * get_mParent_16() const { return ___mParent_16; }
	inline UIRect_t2875960382 ** get_address_of_mParent_16() { return &___mParent_16; }
	inline void set_mParent_16(UIRect_t2875960382 * value)
	{
		___mParent_16 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_16), value);
	}

	inline static int32_t get_offset_of_mRootSet_17() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mRootSet_17)); }
	inline bool get_mRootSet_17() const { return ___mRootSet_17; }
	inline bool* get_address_of_mRootSet_17() { return &___mRootSet_17; }
	inline void set_mRootSet_17(bool value)
	{
		___mRootSet_17 = value;
	}

	inline static int32_t get_offset_of_mCam_18() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mCam_18)); }
	inline Camera_t4157153871 * get_mCam_18() const { return ___mCam_18; }
	inline Camera_t4157153871 ** get_address_of_mCam_18() { return &___mCam_18; }
	inline void set_mCam_18(Camera_t4157153871 * value)
	{
		___mCam_18 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_18), value);
	}

	inline static int32_t get_offset_of_mStarted_19() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___mStarted_19)); }
	inline bool get_mStarted_19() const { return ___mStarted_19; }
	inline bool* get_address_of_mStarted_19() { return &___mStarted_19; }
	inline void set_mStarted_19(bool value)
	{
		___mStarted_19 = value;
	}

	inline static int32_t get_offset_of_finalAlpha_20() { return static_cast<int32_t>(offsetof(UIRect_t2875960382, ___finalAlpha_20)); }
	inline float get_finalAlpha_20() const { return ___finalAlpha_20; }
	inline float* get_address_of_finalAlpha_20() { return &___finalAlpha_20; }
	inline void set_finalAlpha_20(float value)
	{
		___finalAlpha_20 = value;
	}
};

struct UIRect_t2875960382_StaticFields
{
public:
	// UnityEngine.Vector3[] UIRect::mSides
	Vector3U5BU5D_t1718750761* ___mSides_21;

public:
	inline static int32_t get_offset_of_mSides_21() { return static_cast<int32_t>(offsetof(UIRect_t2875960382_StaticFields, ___mSides_21)); }
	inline Vector3U5BU5D_t1718750761* get_mSides_21() const { return ___mSides_21; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mSides_21() { return &___mSides_21; }
	inline void set_mSides_21(Vector3U5BU5D_t1718750761* value)
	{
		___mSides_21 = value;
		Il2CppCodeGenWriteBarrier((&___mSides_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIRECT_T2875960382_H
#ifndef LANE_T3796433026_H
#define LANE_T3796433026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lane
struct  Lane_t3796433026  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material[] Lane::_material
	MaterialU5BU5D_t561872642* ____material_2;

public:
	inline static int32_t get_offset_of__material_2() { return static_cast<int32_t>(offsetof(Lane_t3796433026, ____material_2)); }
	inline MaterialU5BU5D_t561872642* get__material_2() const { return ____material_2; }
	inline MaterialU5BU5D_t561872642** get_address_of__material_2() { return &____material_2; }
	inline void set__material_2(MaterialU5BU5D_t561872642* value)
	{
		____material_2 = value;
		Il2CppCodeGenWriteBarrier((&____material_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANE_T3796433026_H
#ifndef UIVIEWPORT_T1918760134_H
#define UIVIEWPORT_T1918760134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIViewport
struct  UIViewport_t1918760134  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UIViewport::sourceCamera
	Camera_t4157153871 * ___sourceCamera_2;
	// UnityEngine.Transform UIViewport::topLeft
	Transform_t3600365921 * ___topLeft_3;
	// UnityEngine.Transform UIViewport::bottomRight
	Transform_t3600365921 * ___bottomRight_4;
	// System.Single UIViewport::fullSize
	float ___fullSize_5;
	// UnityEngine.Camera UIViewport::mCam
	Camera_t4157153871 * ___mCam_6;

public:
	inline static int32_t get_offset_of_sourceCamera_2() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___sourceCamera_2)); }
	inline Camera_t4157153871 * get_sourceCamera_2() const { return ___sourceCamera_2; }
	inline Camera_t4157153871 ** get_address_of_sourceCamera_2() { return &___sourceCamera_2; }
	inline void set_sourceCamera_2(Camera_t4157153871 * value)
	{
		___sourceCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceCamera_2), value);
	}

	inline static int32_t get_offset_of_topLeft_3() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___topLeft_3)); }
	inline Transform_t3600365921 * get_topLeft_3() const { return ___topLeft_3; }
	inline Transform_t3600365921 ** get_address_of_topLeft_3() { return &___topLeft_3; }
	inline void set_topLeft_3(Transform_t3600365921 * value)
	{
		___topLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___topLeft_3), value);
	}

	inline static int32_t get_offset_of_bottomRight_4() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___bottomRight_4)); }
	inline Transform_t3600365921 * get_bottomRight_4() const { return ___bottomRight_4; }
	inline Transform_t3600365921 ** get_address_of_bottomRight_4() { return &___bottomRight_4; }
	inline void set_bottomRight_4(Transform_t3600365921 * value)
	{
		___bottomRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___bottomRight_4), value);
	}

	inline static int32_t get_offset_of_fullSize_5() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___fullSize_5)); }
	inline float get_fullSize_5() const { return ___fullSize_5; }
	inline float* get_address_of_fullSize_5() { return &___fullSize_5; }
	inline void set_fullSize_5(float value)
	{
		___fullSize_5 = value;
	}

	inline static int32_t get_offset_of_mCam_6() { return static_cast<int32_t>(offsetof(UIViewport_t1918760134, ___mCam_6)); }
	inline Camera_t4157153871 * get_mCam_6() const { return ___mCam_6; }
	inline Camera_t4157153871 ** get_address_of_mCam_6() { return &___mCam_6; }
	inline void set_mCam_6(Camera_t4157153871 * value)
	{
		___mCam_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCam_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWPORT_T1918760134_H
#ifndef UITOOLTIP_T30236576_H
#define UITOOLTIP_T30236576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITooltip
struct  UITooltip_t30236576  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UITooltip::uiCamera
	Camera_t4157153871 * ___uiCamera_3;
	// UILabel UITooltip::text
	UILabel_t3248798549 * ___text_4;
	// UnityEngine.GameObject UITooltip::tooltipRoot
	GameObject_t1113636619 * ___tooltipRoot_5;
	// UISprite UITooltip::background
	UISprite_t194114938 * ___background_6;
	// System.Single UITooltip::appearSpeed
	float ___appearSpeed_7;
	// System.Boolean UITooltip::scalingTransitions
	bool ___scalingTransitions_8;
	// UnityEngine.GameObject UITooltip::mTooltip
	GameObject_t1113636619 * ___mTooltip_9;
	// UnityEngine.Transform UITooltip::mTrans
	Transform_t3600365921 * ___mTrans_10;
	// System.Single UITooltip::mTarget
	float ___mTarget_11;
	// System.Single UITooltip::mCurrent
	float ___mCurrent_12;
	// UnityEngine.Vector3 UITooltip::mPos
	Vector3_t3722313464  ___mPos_13;
	// UnityEngine.Vector3 UITooltip::mSize
	Vector3_t3722313464  ___mSize_14;
	// UIWidget[] UITooltip::mWidgets
	UIWidgetU5BU5D_t2950248968* ___mWidgets_15;

public:
	inline static int32_t get_offset_of_uiCamera_3() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___uiCamera_3)); }
	inline Camera_t4157153871 * get_uiCamera_3() const { return ___uiCamera_3; }
	inline Camera_t4157153871 ** get_address_of_uiCamera_3() { return &___uiCamera_3; }
	inline void set_uiCamera_3(Camera_t4157153871 * value)
	{
		___uiCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___text_4)); }
	inline UILabel_t3248798549 * get_text_4() const { return ___text_4; }
	inline UILabel_t3248798549 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(UILabel_t3248798549 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_tooltipRoot_5() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___tooltipRoot_5)); }
	inline GameObject_t1113636619 * get_tooltipRoot_5() const { return ___tooltipRoot_5; }
	inline GameObject_t1113636619 ** get_address_of_tooltipRoot_5() { return &___tooltipRoot_5; }
	inline void set_tooltipRoot_5(GameObject_t1113636619 * value)
	{
		___tooltipRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___tooltipRoot_5), value);
	}

	inline static int32_t get_offset_of_background_6() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___background_6)); }
	inline UISprite_t194114938 * get_background_6() const { return ___background_6; }
	inline UISprite_t194114938 ** get_address_of_background_6() { return &___background_6; }
	inline void set_background_6(UISprite_t194114938 * value)
	{
		___background_6 = value;
		Il2CppCodeGenWriteBarrier((&___background_6), value);
	}

	inline static int32_t get_offset_of_appearSpeed_7() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___appearSpeed_7)); }
	inline float get_appearSpeed_7() const { return ___appearSpeed_7; }
	inline float* get_address_of_appearSpeed_7() { return &___appearSpeed_7; }
	inline void set_appearSpeed_7(float value)
	{
		___appearSpeed_7 = value;
	}

	inline static int32_t get_offset_of_scalingTransitions_8() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___scalingTransitions_8)); }
	inline bool get_scalingTransitions_8() const { return ___scalingTransitions_8; }
	inline bool* get_address_of_scalingTransitions_8() { return &___scalingTransitions_8; }
	inline void set_scalingTransitions_8(bool value)
	{
		___scalingTransitions_8 = value;
	}

	inline static int32_t get_offset_of_mTooltip_9() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mTooltip_9)); }
	inline GameObject_t1113636619 * get_mTooltip_9() const { return ___mTooltip_9; }
	inline GameObject_t1113636619 ** get_address_of_mTooltip_9() { return &___mTooltip_9; }
	inline void set_mTooltip_9(GameObject_t1113636619 * value)
	{
		___mTooltip_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTooltip_9), value);
	}

	inline static int32_t get_offset_of_mTrans_10() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mTrans_10)); }
	inline Transform_t3600365921 * get_mTrans_10() const { return ___mTrans_10; }
	inline Transform_t3600365921 ** get_address_of_mTrans_10() { return &___mTrans_10; }
	inline void set_mTrans_10(Transform_t3600365921 * value)
	{
		___mTrans_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_10), value);
	}

	inline static int32_t get_offset_of_mTarget_11() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mTarget_11)); }
	inline float get_mTarget_11() const { return ___mTarget_11; }
	inline float* get_address_of_mTarget_11() { return &___mTarget_11; }
	inline void set_mTarget_11(float value)
	{
		___mTarget_11 = value;
	}

	inline static int32_t get_offset_of_mCurrent_12() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mCurrent_12)); }
	inline float get_mCurrent_12() const { return ___mCurrent_12; }
	inline float* get_address_of_mCurrent_12() { return &___mCurrent_12; }
	inline void set_mCurrent_12(float value)
	{
		___mCurrent_12 = value;
	}

	inline static int32_t get_offset_of_mPos_13() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mPos_13)); }
	inline Vector3_t3722313464  get_mPos_13() const { return ___mPos_13; }
	inline Vector3_t3722313464 * get_address_of_mPos_13() { return &___mPos_13; }
	inline void set_mPos_13(Vector3_t3722313464  value)
	{
		___mPos_13 = value;
	}

	inline static int32_t get_offset_of_mSize_14() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mSize_14)); }
	inline Vector3_t3722313464  get_mSize_14() const { return ___mSize_14; }
	inline Vector3_t3722313464 * get_address_of_mSize_14() { return &___mSize_14; }
	inline void set_mSize_14(Vector3_t3722313464  value)
	{
		___mSize_14 = value;
	}

	inline static int32_t get_offset_of_mWidgets_15() { return static_cast<int32_t>(offsetof(UITooltip_t30236576, ___mWidgets_15)); }
	inline UIWidgetU5BU5D_t2950248968* get_mWidgets_15() const { return ___mWidgets_15; }
	inline UIWidgetU5BU5D_t2950248968** get_address_of_mWidgets_15() { return &___mWidgets_15; }
	inline void set_mWidgets_15(UIWidgetU5BU5D_t2950248968* value)
	{
		___mWidgets_15 = value;
		Il2CppCodeGenWriteBarrier((&___mWidgets_15), value);
	}
};

struct UITooltip_t30236576_StaticFields
{
public:
	// UITooltip UITooltip::mInstance
	UITooltip_t30236576 * ___mInstance_2;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(UITooltip_t30236576_StaticFields, ___mInstance_2)); }
	inline UITooltip_t30236576 * get_mInstance_2() const { return ___mInstance_2; }
	inline UITooltip_t30236576 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(UITooltip_t30236576 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOOLTIP_T30236576_H
#ifndef NATIVEPLUGIN_T1231223992_H
#define NATIVEPLUGIN_T1231223992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativePlugin
struct  NativePlugin_t1231223992  : public MonoBehaviour_t3962482529
{
public:
	// System.IntPtr NativePlugin::_nativeInstance
	intptr_t ____nativeInstance_3;

public:
	inline static int32_t get_offset_of__nativeInstance_3() { return static_cast<int32_t>(offsetof(NativePlugin_t1231223992, ____nativeInstance_3)); }
	inline intptr_t get__nativeInstance_3() const { return ____nativeInstance_3; }
	inline intptr_t* get_address_of__nativeInstance_3() { return &____nativeInstance_3; }
	inline void set__nativeInstance_3(intptr_t value)
	{
		____nativeInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEPLUGIN_T1231223992_H
#ifndef UITEXTLIST_T2040849008_H
#define UITEXTLIST_T2040849008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITextList
struct  UITextList_t2040849008  : public MonoBehaviour_t3962482529
{
public:
	// UILabel UITextList::textLabel
	UILabel_t3248798549 * ___textLabel_2;
	// UIProgressBar UITextList::scrollBar
	UIProgressBar_t1222110469 * ___scrollBar_3;
	// UITextList/Style UITextList::style
	int32_t ___style_4;
	// System.Int32 UITextList::paragraphHistory
	int32_t ___paragraphHistory_5;
	// System.Char[] UITextList::mSeparator
	CharU5BU5D_t3528271667* ___mSeparator_6;
	// System.Single UITextList::mScroll
	float ___mScroll_7;
	// System.Int32 UITextList::mTotalLines
	int32_t ___mTotalLines_8;
	// System.Int32 UITextList::mLastWidth
	int32_t ___mLastWidth_9;
	// System.Int32 UITextList::mLastHeight
	int32_t ___mLastHeight_10;
	// BetterList`1<UITextList/Paragraph> UITextList::mParagraphs
	BetterList_1_t266084037 * ___mParagraphs_11;

public:
	inline static int32_t get_offset_of_textLabel_2() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___textLabel_2)); }
	inline UILabel_t3248798549 * get_textLabel_2() const { return ___textLabel_2; }
	inline UILabel_t3248798549 ** get_address_of_textLabel_2() { return &___textLabel_2; }
	inline void set_textLabel_2(UILabel_t3248798549 * value)
	{
		___textLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___textLabel_2), value);
	}

	inline static int32_t get_offset_of_scrollBar_3() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___scrollBar_3)); }
	inline UIProgressBar_t1222110469 * get_scrollBar_3() const { return ___scrollBar_3; }
	inline UIProgressBar_t1222110469 ** get_address_of_scrollBar_3() { return &___scrollBar_3; }
	inline void set_scrollBar_3(UIProgressBar_t1222110469 * value)
	{
		___scrollBar_3 = value;
		Il2CppCodeGenWriteBarrier((&___scrollBar_3), value);
	}

	inline static int32_t get_offset_of_style_4() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___style_4)); }
	inline int32_t get_style_4() const { return ___style_4; }
	inline int32_t* get_address_of_style_4() { return &___style_4; }
	inline void set_style_4(int32_t value)
	{
		___style_4 = value;
	}

	inline static int32_t get_offset_of_paragraphHistory_5() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___paragraphHistory_5)); }
	inline int32_t get_paragraphHistory_5() const { return ___paragraphHistory_5; }
	inline int32_t* get_address_of_paragraphHistory_5() { return &___paragraphHistory_5; }
	inline void set_paragraphHistory_5(int32_t value)
	{
		___paragraphHistory_5 = value;
	}

	inline static int32_t get_offset_of_mSeparator_6() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___mSeparator_6)); }
	inline CharU5BU5D_t3528271667* get_mSeparator_6() const { return ___mSeparator_6; }
	inline CharU5BU5D_t3528271667** get_address_of_mSeparator_6() { return &___mSeparator_6; }
	inline void set_mSeparator_6(CharU5BU5D_t3528271667* value)
	{
		___mSeparator_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSeparator_6), value);
	}

	inline static int32_t get_offset_of_mScroll_7() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___mScroll_7)); }
	inline float get_mScroll_7() const { return ___mScroll_7; }
	inline float* get_address_of_mScroll_7() { return &___mScroll_7; }
	inline void set_mScroll_7(float value)
	{
		___mScroll_7 = value;
	}

	inline static int32_t get_offset_of_mTotalLines_8() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___mTotalLines_8)); }
	inline int32_t get_mTotalLines_8() const { return ___mTotalLines_8; }
	inline int32_t* get_address_of_mTotalLines_8() { return &___mTotalLines_8; }
	inline void set_mTotalLines_8(int32_t value)
	{
		___mTotalLines_8 = value;
	}

	inline static int32_t get_offset_of_mLastWidth_9() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___mLastWidth_9)); }
	inline int32_t get_mLastWidth_9() const { return ___mLastWidth_9; }
	inline int32_t* get_address_of_mLastWidth_9() { return &___mLastWidth_9; }
	inline void set_mLastWidth_9(int32_t value)
	{
		___mLastWidth_9 = value;
	}

	inline static int32_t get_offset_of_mLastHeight_10() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___mLastHeight_10)); }
	inline int32_t get_mLastHeight_10() const { return ___mLastHeight_10; }
	inline int32_t* get_address_of_mLastHeight_10() { return &___mLastHeight_10; }
	inline void set_mLastHeight_10(int32_t value)
	{
		___mLastHeight_10 = value;
	}

	inline static int32_t get_offset_of_mParagraphs_11() { return static_cast<int32_t>(offsetof(UITextList_t2040849008, ___mParagraphs_11)); }
	inline BetterList_1_t266084037 * get_mParagraphs_11() const { return ___mParagraphs_11; }
	inline BetterList_1_t266084037 ** get_address_of_mParagraphs_11() { return &___mParagraphs_11; }
	inline void set_mParagraphs_11(BetterList_1_t266084037 * value)
	{
		___mParagraphs_11 = value;
		Il2CppCodeGenWriteBarrier((&___mParagraphs_11), value);
	}
};

struct UITextList_t2040849008_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,BetterList`1<UITextList/Paragraph>> UITextList::mHistory
	Dictionary_2_t51340336 * ___mHistory_12;

public:
	inline static int32_t get_offset_of_mHistory_12() { return static_cast<int32_t>(offsetof(UITextList_t2040849008_StaticFields, ___mHistory_12)); }
	inline Dictionary_2_t51340336 * get_mHistory_12() const { return ___mHistory_12; }
	inline Dictionary_2_t51340336 ** get_address_of_mHistory_12() { return &___mHistory_12; }
	inline void set_mHistory_12(Dictionary_2_t51340336 * value)
	{
		___mHistory_12 = value;
		Il2CppCodeGenWriteBarrier((&___mHistory_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITEXTLIST_T2040849008_H
#ifndef UISTRETCH_T3058335968_H
#define UISTRETCH_T3058335968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIStretch
struct  UIStretch_t3058335968  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UIStretch::uiCamera
	Camera_t4157153871 * ___uiCamera_2;
	// UnityEngine.GameObject UIStretch::container
	GameObject_t1113636619 * ___container_3;
	// UIStretch/Style UIStretch::style
	int32_t ___style_4;
	// System.Boolean UIStretch::runOnlyOnce
	bool ___runOnlyOnce_5;
	// UnityEngine.Vector2 UIStretch::relativeSize
	Vector2_t2156229523  ___relativeSize_6;
	// UnityEngine.Vector2 UIStretch::initialSize
	Vector2_t2156229523  ___initialSize_7;
	// UnityEngine.Vector2 UIStretch::borderPadding
	Vector2_t2156229523  ___borderPadding_8;
	// UIWidget UIStretch::widgetContainer
	UIWidget_t3538521925 * ___widgetContainer_9;
	// UnityEngine.Transform UIStretch::mTrans
	Transform_t3600365921 * ___mTrans_10;
	// UIWidget UIStretch::mWidget
	UIWidget_t3538521925 * ___mWidget_11;
	// UISprite UIStretch::mSprite
	UISprite_t194114938 * ___mSprite_12;
	// UIPanel UIStretch::mPanel
	UIPanel_t1716472341 * ___mPanel_13;
	// UIRoot UIStretch::mRoot
	UIRoot_t4022971450 * ___mRoot_14;
	// UnityEngine.Animation UIStretch::mAnim
	Animation_t3648466861 * ___mAnim_15;
	// UnityEngine.Rect UIStretch::mRect
	Rect_t2360479859  ___mRect_16;
	// System.Boolean UIStretch::mStarted
	bool ___mStarted_17;

public:
	inline static int32_t get_offset_of_uiCamera_2() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___uiCamera_2)); }
	inline Camera_t4157153871 * get_uiCamera_2() const { return ___uiCamera_2; }
	inline Camera_t4157153871 ** get_address_of_uiCamera_2() { return &___uiCamera_2; }
	inline void set_uiCamera_2(Camera_t4157153871 * value)
	{
		___uiCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___uiCamera_2), value);
	}

	inline static int32_t get_offset_of_container_3() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___container_3)); }
	inline GameObject_t1113636619 * get_container_3() const { return ___container_3; }
	inline GameObject_t1113636619 ** get_address_of_container_3() { return &___container_3; }
	inline void set_container_3(GameObject_t1113636619 * value)
	{
		___container_3 = value;
		Il2CppCodeGenWriteBarrier((&___container_3), value);
	}

	inline static int32_t get_offset_of_style_4() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___style_4)); }
	inline int32_t get_style_4() const { return ___style_4; }
	inline int32_t* get_address_of_style_4() { return &___style_4; }
	inline void set_style_4(int32_t value)
	{
		___style_4 = value;
	}

	inline static int32_t get_offset_of_runOnlyOnce_5() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___runOnlyOnce_5)); }
	inline bool get_runOnlyOnce_5() const { return ___runOnlyOnce_5; }
	inline bool* get_address_of_runOnlyOnce_5() { return &___runOnlyOnce_5; }
	inline void set_runOnlyOnce_5(bool value)
	{
		___runOnlyOnce_5 = value;
	}

	inline static int32_t get_offset_of_relativeSize_6() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___relativeSize_6)); }
	inline Vector2_t2156229523  get_relativeSize_6() const { return ___relativeSize_6; }
	inline Vector2_t2156229523 * get_address_of_relativeSize_6() { return &___relativeSize_6; }
	inline void set_relativeSize_6(Vector2_t2156229523  value)
	{
		___relativeSize_6 = value;
	}

	inline static int32_t get_offset_of_initialSize_7() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___initialSize_7)); }
	inline Vector2_t2156229523  get_initialSize_7() const { return ___initialSize_7; }
	inline Vector2_t2156229523 * get_address_of_initialSize_7() { return &___initialSize_7; }
	inline void set_initialSize_7(Vector2_t2156229523  value)
	{
		___initialSize_7 = value;
	}

	inline static int32_t get_offset_of_borderPadding_8() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___borderPadding_8)); }
	inline Vector2_t2156229523  get_borderPadding_8() const { return ___borderPadding_8; }
	inline Vector2_t2156229523 * get_address_of_borderPadding_8() { return &___borderPadding_8; }
	inline void set_borderPadding_8(Vector2_t2156229523  value)
	{
		___borderPadding_8 = value;
	}

	inline static int32_t get_offset_of_widgetContainer_9() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___widgetContainer_9)); }
	inline UIWidget_t3538521925 * get_widgetContainer_9() const { return ___widgetContainer_9; }
	inline UIWidget_t3538521925 ** get_address_of_widgetContainer_9() { return &___widgetContainer_9; }
	inline void set_widgetContainer_9(UIWidget_t3538521925 * value)
	{
		___widgetContainer_9 = value;
		Il2CppCodeGenWriteBarrier((&___widgetContainer_9), value);
	}

	inline static int32_t get_offset_of_mTrans_10() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mTrans_10)); }
	inline Transform_t3600365921 * get_mTrans_10() const { return ___mTrans_10; }
	inline Transform_t3600365921 ** get_address_of_mTrans_10() { return &___mTrans_10; }
	inline void set_mTrans_10(Transform_t3600365921 * value)
	{
		___mTrans_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_10), value);
	}

	inline static int32_t get_offset_of_mWidget_11() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mWidget_11)); }
	inline UIWidget_t3538521925 * get_mWidget_11() const { return ___mWidget_11; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_11() { return &___mWidget_11; }
	inline void set_mWidget_11(UIWidget_t3538521925 * value)
	{
		___mWidget_11 = value;
		Il2CppCodeGenWriteBarrier((&___mWidget_11), value);
	}

	inline static int32_t get_offset_of_mSprite_12() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mSprite_12)); }
	inline UISprite_t194114938 * get_mSprite_12() const { return ___mSprite_12; }
	inline UISprite_t194114938 ** get_address_of_mSprite_12() { return &___mSprite_12; }
	inline void set_mSprite_12(UISprite_t194114938 * value)
	{
		___mSprite_12 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_12), value);
	}

	inline static int32_t get_offset_of_mPanel_13() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mPanel_13)); }
	inline UIPanel_t1716472341 * get_mPanel_13() const { return ___mPanel_13; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_13() { return &___mPanel_13; }
	inline void set_mPanel_13(UIPanel_t1716472341 * value)
	{
		___mPanel_13 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_13), value);
	}

	inline static int32_t get_offset_of_mRoot_14() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mRoot_14)); }
	inline UIRoot_t4022971450 * get_mRoot_14() const { return ___mRoot_14; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_14() { return &___mRoot_14; }
	inline void set_mRoot_14(UIRoot_t4022971450 * value)
	{
		___mRoot_14 = value;
		Il2CppCodeGenWriteBarrier((&___mRoot_14), value);
	}

	inline static int32_t get_offset_of_mAnim_15() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mAnim_15)); }
	inline Animation_t3648466861 * get_mAnim_15() const { return ___mAnim_15; }
	inline Animation_t3648466861 ** get_address_of_mAnim_15() { return &___mAnim_15; }
	inline void set_mAnim_15(Animation_t3648466861 * value)
	{
		___mAnim_15 = value;
		Il2CppCodeGenWriteBarrier((&___mAnim_15), value);
	}

	inline static int32_t get_offset_of_mRect_16() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mRect_16)); }
	inline Rect_t2360479859  get_mRect_16() const { return ___mRect_16; }
	inline Rect_t2360479859 * get_address_of_mRect_16() { return &___mRect_16; }
	inline void set_mRect_16(Rect_t2360479859  value)
	{
		___mRect_16 = value;
	}

	inline static int32_t get_offset_of_mStarted_17() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mStarted_17)); }
	inline bool get_mStarted_17() const { return ___mStarted_17; }
	inline bool* get_address_of_mStarted_17() { return &___mStarted_17; }
	inline void set_mStarted_17(bool value)
	{
		___mStarted_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISTRETCH_T3058335968_H
#ifndef UISPRITEANIMATION_T1118314077_H
#define UISPRITEANIMATION_T1118314077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISpriteAnimation
struct  UISpriteAnimation_t1118314077  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UISpriteAnimation::frameIndex
	int32_t ___frameIndex_2;
	// System.Int32 UISpriteAnimation::mFPS
	int32_t ___mFPS_3;
	// System.String UISpriteAnimation::mPrefix
	String_t* ___mPrefix_4;
	// System.Boolean UISpriteAnimation::mLoop
	bool ___mLoop_5;
	// System.Boolean UISpriteAnimation::mSnap
	bool ___mSnap_6;
	// UISprite UISpriteAnimation::mSprite
	UISprite_t194114938 * ___mSprite_7;
	// System.Single UISpriteAnimation::mDelta
	float ___mDelta_8;
	// System.Boolean UISpriteAnimation::mActive
	bool ___mActive_9;
	// System.Collections.Generic.List`1<System.String> UISpriteAnimation::mSpriteNames
	List_1_t3319525431 * ___mSpriteNames_10;

public:
	inline static int32_t get_offset_of_frameIndex_2() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___frameIndex_2)); }
	inline int32_t get_frameIndex_2() const { return ___frameIndex_2; }
	inline int32_t* get_address_of_frameIndex_2() { return &___frameIndex_2; }
	inline void set_frameIndex_2(int32_t value)
	{
		___frameIndex_2 = value;
	}

	inline static int32_t get_offset_of_mFPS_3() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mFPS_3)); }
	inline int32_t get_mFPS_3() const { return ___mFPS_3; }
	inline int32_t* get_address_of_mFPS_3() { return &___mFPS_3; }
	inline void set_mFPS_3(int32_t value)
	{
		___mFPS_3 = value;
	}

	inline static int32_t get_offset_of_mPrefix_4() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mPrefix_4)); }
	inline String_t* get_mPrefix_4() const { return ___mPrefix_4; }
	inline String_t** get_address_of_mPrefix_4() { return &___mPrefix_4; }
	inline void set_mPrefix_4(String_t* value)
	{
		___mPrefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPrefix_4), value);
	}

	inline static int32_t get_offset_of_mLoop_5() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mLoop_5)); }
	inline bool get_mLoop_5() const { return ___mLoop_5; }
	inline bool* get_address_of_mLoop_5() { return &___mLoop_5; }
	inline void set_mLoop_5(bool value)
	{
		___mLoop_5 = value;
	}

	inline static int32_t get_offset_of_mSnap_6() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mSnap_6)); }
	inline bool get_mSnap_6() const { return ___mSnap_6; }
	inline bool* get_address_of_mSnap_6() { return &___mSnap_6; }
	inline void set_mSnap_6(bool value)
	{
		___mSnap_6 = value;
	}

	inline static int32_t get_offset_of_mSprite_7() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mSprite_7)); }
	inline UISprite_t194114938 * get_mSprite_7() const { return ___mSprite_7; }
	inline UISprite_t194114938 ** get_address_of_mSprite_7() { return &___mSprite_7; }
	inline void set_mSprite_7(UISprite_t194114938 * value)
	{
		___mSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_7), value);
	}

	inline static int32_t get_offset_of_mDelta_8() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mDelta_8)); }
	inline float get_mDelta_8() const { return ___mDelta_8; }
	inline float* get_address_of_mDelta_8() { return &___mDelta_8; }
	inline void set_mDelta_8(float value)
	{
		___mDelta_8 = value;
	}

	inline static int32_t get_offset_of_mActive_9() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mActive_9)); }
	inline bool get_mActive_9() const { return ___mActive_9; }
	inline bool* get_address_of_mActive_9() { return &___mActive_9; }
	inline void set_mActive_9(bool value)
	{
		___mActive_9 = value;
	}

	inline static int32_t get_offset_of_mSpriteNames_10() { return static_cast<int32_t>(offsetof(UISpriteAnimation_t1118314077, ___mSpriteNames_10)); }
	inline List_1_t3319525431 * get_mSpriteNames_10() const { return ___mSpriteNames_10; }
	inline List_1_t3319525431 ** get_address_of_mSpriteNames_10() { return &___mSpriteNames_10; }
	inline void set_mSpriteNames_10(List_1_t3319525431 * value)
	{
		___mSpriteNames_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteNames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISPRITEANIMATION_T1118314077_H
#ifndef UIROOT_T4022971450_H
#define UIROOT_T4022971450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRoot
struct  UIRoot_t4022971450  : public MonoBehaviour_t3962482529
{
public:
	// UIRoot/Scaling UIRoot::scalingStyle
	int32_t ___scalingStyle_3;
	// System.Int32 UIRoot::manualWidth
	int32_t ___manualWidth_4;
	// System.Int32 UIRoot::manualHeight
	int32_t ___manualHeight_5;
	// System.Int32 UIRoot::minimumHeight
	int32_t ___minimumHeight_6;
	// System.Int32 UIRoot::maximumHeight
	int32_t ___maximumHeight_7;
	// System.Boolean UIRoot::fitWidth
	bool ___fitWidth_8;
	// System.Boolean UIRoot::fitHeight
	bool ___fitHeight_9;
	// System.Boolean UIRoot::adjustByDPI
	bool ___adjustByDPI_10;
	// System.Boolean UIRoot::shrinkPortraitUI
	bool ___shrinkPortraitUI_11;
	// UnityEngine.Transform UIRoot::mTrans
	Transform_t3600365921 * ___mTrans_12;

public:
	inline static int32_t get_offset_of_scalingStyle_3() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___scalingStyle_3)); }
	inline int32_t get_scalingStyle_3() const { return ___scalingStyle_3; }
	inline int32_t* get_address_of_scalingStyle_3() { return &___scalingStyle_3; }
	inline void set_scalingStyle_3(int32_t value)
	{
		___scalingStyle_3 = value;
	}

	inline static int32_t get_offset_of_manualWidth_4() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___manualWidth_4)); }
	inline int32_t get_manualWidth_4() const { return ___manualWidth_4; }
	inline int32_t* get_address_of_manualWidth_4() { return &___manualWidth_4; }
	inline void set_manualWidth_4(int32_t value)
	{
		___manualWidth_4 = value;
	}

	inline static int32_t get_offset_of_manualHeight_5() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___manualHeight_5)); }
	inline int32_t get_manualHeight_5() const { return ___manualHeight_5; }
	inline int32_t* get_address_of_manualHeight_5() { return &___manualHeight_5; }
	inline void set_manualHeight_5(int32_t value)
	{
		___manualHeight_5 = value;
	}

	inline static int32_t get_offset_of_minimumHeight_6() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___minimumHeight_6)); }
	inline int32_t get_minimumHeight_6() const { return ___minimumHeight_6; }
	inline int32_t* get_address_of_minimumHeight_6() { return &___minimumHeight_6; }
	inline void set_minimumHeight_6(int32_t value)
	{
		___minimumHeight_6 = value;
	}

	inline static int32_t get_offset_of_maximumHeight_7() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___maximumHeight_7)); }
	inline int32_t get_maximumHeight_7() const { return ___maximumHeight_7; }
	inline int32_t* get_address_of_maximumHeight_7() { return &___maximumHeight_7; }
	inline void set_maximumHeight_7(int32_t value)
	{
		___maximumHeight_7 = value;
	}

	inline static int32_t get_offset_of_fitWidth_8() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___fitWidth_8)); }
	inline bool get_fitWidth_8() const { return ___fitWidth_8; }
	inline bool* get_address_of_fitWidth_8() { return &___fitWidth_8; }
	inline void set_fitWidth_8(bool value)
	{
		___fitWidth_8 = value;
	}

	inline static int32_t get_offset_of_fitHeight_9() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___fitHeight_9)); }
	inline bool get_fitHeight_9() const { return ___fitHeight_9; }
	inline bool* get_address_of_fitHeight_9() { return &___fitHeight_9; }
	inline void set_fitHeight_9(bool value)
	{
		___fitHeight_9 = value;
	}

	inline static int32_t get_offset_of_adjustByDPI_10() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___adjustByDPI_10)); }
	inline bool get_adjustByDPI_10() const { return ___adjustByDPI_10; }
	inline bool* get_address_of_adjustByDPI_10() { return &___adjustByDPI_10; }
	inline void set_adjustByDPI_10(bool value)
	{
		___adjustByDPI_10 = value;
	}

	inline static int32_t get_offset_of_shrinkPortraitUI_11() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___shrinkPortraitUI_11)); }
	inline bool get_shrinkPortraitUI_11() const { return ___shrinkPortraitUI_11; }
	inline bool* get_address_of_shrinkPortraitUI_11() { return &___shrinkPortraitUI_11; }
	inline void set_shrinkPortraitUI_11(bool value)
	{
		___shrinkPortraitUI_11 = value;
	}

	inline static int32_t get_offset_of_mTrans_12() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450, ___mTrans_12)); }
	inline Transform_t3600365921 * get_mTrans_12() const { return ___mTrans_12; }
	inline Transform_t3600365921 ** get_address_of_mTrans_12() { return &___mTrans_12; }
	inline void set_mTrans_12(Transform_t3600365921 * value)
	{
		___mTrans_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_12), value);
	}
};

struct UIRoot_t4022971450_StaticFields
{
public:
	// System.Collections.Generic.List`1<UIRoot> UIRoot::list
	List_1_t1200078896 * ___list_2;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UIRoot_t4022971450_StaticFields, ___list_2)); }
	inline List_1_t1200078896 * get_list_2() const { return ___list_2; }
	inline List_1_t1200078896 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(List_1_t1200078896 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIROOT_T4022971450_H
#ifndef RESUMEBUTTONASSISSTANT_T2799415769_H
#define RESUMEBUTTONASSISSTANT_T2799415769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResumeButtonAssisstant
struct  ResumeButtonAssisstant_t2799415769  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject ResumeButtonAssisstant::_canvas
	GameObject_t1113636619 * ____canvas_5;
	// System.Boolean ResumeButtonAssisstant::pauseStatus
	bool ___pauseStatus_6;

public:
	inline static int32_t get_offset_of__canvas_5() { return static_cast<int32_t>(offsetof(ResumeButtonAssisstant_t2799415769, ____canvas_5)); }
	inline GameObject_t1113636619 * get__canvas_5() const { return ____canvas_5; }
	inline GameObject_t1113636619 ** get_address_of__canvas_5() { return &____canvas_5; }
	inline void set__canvas_5(GameObject_t1113636619 * value)
	{
		____canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_5), value);
	}

	inline static int32_t get_offset_of_pauseStatus_6() { return static_cast<int32_t>(offsetof(ResumeButtonAssisstant_t2799415769, ___pauseStatus_6)); }
	inline bool get_pauseStatus_6() const { return ___pauseStatus_6; }
	inline bool* get_address_of_pauseStatus_6() { return &___pauseStatus_6; }
	inline void set_pauseStatus_6(bool value)
	{
		___pauseStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESUMEBUTTONASSISSTANT_T2799415769_H
#ifndef RESTOREBUTTONASSISTANT_T450383048_H
#define RESTOREBUTTONASSISTANT_T450383048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestoreButtonAssistant
struct  RestoreButtonAssistant_t450383048  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTOREBUTTONASSISTANT_T450383048_H
#ifndef ADREGIONVIEWER_T142023310_H
#define ADREGIONVIEWER_T142023310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdRegionViewer
struct  AdRegionViewer_t142023310  : public UIAdjuster_t2553577866
{
public:
	// UnityEngine.Color AdRegionViewer::_color
	Color_t2555686324  ____color_8;
	// AdRegionViewer/BannerType AdRegionViewer::_bannerType
	int32_t ____bannerType_9;
	// AdRegionViewer/RectangleType AdRegionViewer::_rectangleType
	int32_t ____rectangleType_10;
	// AdRegionViewer/IconType AdRegionViewer::_iconType
	int32_t ____iconType_11;
	// UnityEngine.GameObject AdRegionViewer::_topBanner
	GameObject_t1113636619 * ____topBanner_12;
	// UnityEngine.GameObject AdRegionViewer::_topBigBanner
	GameObject_t1113636619 * ____topBigBanner_13;
	// UnityEngine.GameObject AdRegionViewer::_bottomBanner
	GameObject_t1113636619 * ____bottomBanner_14;
	// UnityEngine.GameObject AdRegionViewer::_topRectangle
	GameObject_t1113636619 * ____topRectangle_15;
	// UnityEngine.GameObject AdRegionViewer::_bottomRectangle
	GameObject_t1113636619 * ____bottomRectangle_16;
	// UnityEngine.GameObject AdRegionViewer::_titleIcon
	GameObject_t1113636619 * ____titleIcon_17;
	// UnityEngine.GameObject AdRegionViewer::_helpIcon
	GameObject_t1113636619 * ____helpIcon_18;

public:
	inline static int32_t get_offset_of__color_8() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____color_8)); }
	inline Color_t2555686324  get__color_8() const { return ____color_8; }
	inline Color_t2555686324 * get_address_of__color_8() { return &____color_8; }
	inline void set__color_8(Color_t2555686324  value)
	{
		____color_8 = value;
	}

	inline static int32_t get_offset_of__bannerType_9() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____bannerType_9)); }
	inline int32_t get__bannerType_9() const { return ____bannerType_9; }
	inline int32_t* get_address_of__bannerType_9() { return &____bannerType_9; }
	inline void set__bannerType_9(int32_t value)
	{
		____bannerType_9 = value;
	}

	inline static int32_t get_offset_of__rectangleType_10() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____rectangleType_10)); }
	inline int32_t get__rectangleType_10() const { return ____rectangleType_10; }
	inline int32_t* get_address_of__rectangleType_10() { return &____rectangleType_10; }
	inline void set__rectangleType_10(int32_t value)
	{
		____rectangleType_10 = value;
	}

	inline static int32_t get_offset_of__iconType_11() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____iconType_11)); }
	inline int32_t get__iconType_11() const { return ____iconType_11; }
	inline int32_t* get_address_of__iconType_11() { return &____iconType_11; }
	inline void set__iconType_11(int32_t value)
	{
		____iconType_11 = value;
	}

	inline static int32_t get_offset_of__topBanner_12() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____topBanner_12)); }
	inline GameObject_t1113636619 * get__topBanner_12() const { return ____topBanner_12; }
	inline GameObject_t1113636619 ** get_address_of__topBanner_12() { return &____topBanner_12; }
	inline void set__topBanner_12(GameObject_t1113636619 * value)
	{
		____topBanner_12 = value;
		Il2CppCodeGenWriteBarrier((&____topBanner_12), value);
	}

	inline static int32_t get_offset_of__topBigBanner_13() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____topBigBanner_13)); }
	inline GameObject_t1113636619 * get__topBigBanner_13() const { return ____topBigBanner_13; }
	inline GameObject_t1113636619 ** get_address_of__topBigBanner_13() { return &____topBigBanner_13; }
	inline void set__topBigBanner_13(GameObject_t1113636619 * value)
	{
		____topBigBanner_13 = value;
		Il2CppCodeGenWriteBarrier((&____topBigBanner_13), value);
	}

	inline static int32_t get_offset_of__bottomBanner_14() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____bottomBanner_14)); }
	inline GameObject_t1113636619 * get__bottomBanner_14() const { return ____bottomBanner_14; }
	inline GameObject_t1113636619 ** get_address_of__bottomBanner_14() { return &____bottomBanner_14; }
	inline void set__bottomBanner_14(GameObject_t1113636619 * value)
	{
		____bottomBanner_14 = value;
		Il2CppCodeGenWriteBarrier((&____bottomBanner_14), value);
	}

	inline static int32_t get_offset_of__topRectangle_15() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____topRectangle_15)); }
	inline GameObject_t1113636619 * get__topRectangle_15() const { return ____topRectangle_15; }
	inline GameObject_t1113636619 ** get_address_of__topRectangle_15() { return &____topRectangle_15; }
	inline void set__topRectangle_15(GameObject_t1113636619 * value)
	{
		____topRectangle_15 = value;
		Il2CppCodeGenWriteBarrier((&____topRectangle_15), value);
	}

	inline static int32_t get_offset_of__bottomRectangle_16() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____bottomRectangle_16)); }
	inline GameObject_t1113636619 * get__bottomRectangle_16() const { return ____bottomRectangle_16; }
	inline GameObject_t1113636619 ** get_address_of__bottomRectangle_16() { return &____bottomRectangle_16; }
	inline void set__bottomRectangle_16(GameObject_t1113636619 * value)
	{
		____bottomRectangle_16 = value;
		Il2CppCodeGenWriteBarrier((&____bottomRectangle_16), value);
	}

	inline static int32_t get_offset_of__titleIcon_17() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____titleIcon_17)); }
	inline GameObject_t1113636619 * get__titleIcon_17() const { return ____titleIcon_17; }
	inline GameObject_t1113636619 ** get_address_of__titleIcon_17() { return &____titleIcon_17; }
	inline void set__titleIcon_17(GameObject_t1113636619 * value)
	{
		____titleIcon_17 = value;
		Il2CppCodeGenWriteBarrier((&____titleIcon_17), value);
	}

	inline static int32_t get_offset_of__helpIcon_18() { return static_cast<int32_t>(offsetof(AdRegionViewer_t142023310, ____helpIcon_18)); }
	inline GameObject_t1113636619 * get__helpIcon_18() const { return ____helpIcon_18; }
	inline GameObject_t1113636619 ** get_address_of__helpIcon_18() { return &____helpIcon_18; }
	inline void set__helpIcon_18(GameObject_t1113636619 * value)
	{
		____helpIcon_18 = value;
		Il2CppCodeGenWriteBarrier((&____helpIcon_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADREGIONVIEWER_T142023310_H
#ifndef PAUSEBUTTONASSISSTANT_T3297824667_H
#define PAUSEBUTTONASSISSTANT_T3297824667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseButtonAssisstant
struct  PauseButtonAssisstant_t3297824667  : public ButtonAssistant_t3922690862
{
public:
	// UnityEngine.GameObject PauseButtonAssisstant::_canvas
	GameObject_t1113636619 * ____canvas_5;
	// System.Boolean PauseButtonAssisstant::pauseStatus
	bool ___pauseStatus_6;

public:
	inline static int32_t get_offset_of__canvas_5() { return static_cast<int32_t>(offsetof(PauseButtonAssisstant_t3297824667, ____canvas_5)); }
	inline GameObject_t1113636619 * get__canvas_5() const { return ____canvas_5; }
	inline GameObject_t1113636619 ** get_address_of__canvas_5() { return &____canvas_5; }
	inline void set__canvas_5(GameObject_t1113636619 * value)
	{
		____canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_5), value);
	}

	inline static int32_t get_offset_of_pauseStatus_6() { return static_cast<int32_t>(offsetof(PauseButtonAssisstant_t3297824667, ___pauseStatus_6)); }
	inline bool get_pauseStatus_6() const { return ___pauseStatus_6; }
	inline bool* get_address_of_pauseStatus_6() { return &___pauseStatus_6; }
	inline void set_pauseStatus_6(bool value)
	{
		___pauseStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEBUTTONASSISSTANT_T3297824667_H
#ifndef NOADSBUTTONASSISTANT_T697893148_H
#define NOADSBUTTONASSISTANT_T697893148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoAdsButtonAssistant
struct  NoAdsButtonAssistant_t697893148  : public ButtonAssistant_t3922690862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOADSBUTTONASSISTANT_T697893148_H
#ifndef TWEENLETTERS_T3284132802_H
#define TWEENLETTERS_T3284132802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenLetters
struct  TweenLetters_t3284132802  : public UITweener_t260334902
{
public:
	// TweenLetters/AnimationProperties TweenLetters::hoverOver
	AnimationProperties_t1280364982 * ___hoverOver_22;
	// TweenLetters/AnimationProperties TweenLetters::hoverOut
	AnimationProperties_t1280364982 * ___hoverOut_23;
	// UILabel TweenLetters::mLabel
	UILabel_t3248798549 * ___mLabel_24;
	// System.Int32 TweenLetters::mVertexCount
	int32_t ___mVertexCount_25;
	// System.Int32[] TweenLetters::mLetterOrder
	Int32U5BU5D_t385246372* ___mLetterOrder_26;
	// TweenLetters/LetterProperties[] TweenLetters::mLetter
	LetterPropertiesU5BU5D_t3676037960* ___mLetter_27;
	// TweenLetters/AnimationProperties TweenLetters::mCurrent
	AnimationProperties_t1280364982 * ___mCurrent_28;

public:
	inline static int32_t get_offset_of_hoverOver_22() { return static_cast<int32_t>(offsetof(TweenLetters_t3284132802, ___hoverOver_22)); }
	inline AnimationProperties_t1280364982 * get_hoverOver_22() const { return ___hoverOver_22; }
	inline AnimationProperties_t1280364982 ** get_address_of_hoverOver_22() { return &___hoverOver_22; }
	inline void set_hoverOver_22(AnimationProperties_t1280364982 * value)
	{
		___hoverOver_22 = value;
		Il2CppCodeGenWriteBarrier((&___hoverOver_22), value);
	}

	inline static int32_t get_offset_of_hoverOut_23() { return static_cast<int32_t>(offsetof(TweenLetters_t3284132802, ___hoverOut_23)); }
	inline AnimationProperties_t1280364982 * get_hoverOut_23() const { return ___hoverOut_23; }
	inline AnimationProperties_t1280364982 ** get_address_of_hoverOut_23() { return &___hoverOut_23; }
	inline void set_hoverOut_23(AnimationProperties_t1280364982 * value)
	{
		___hoverOut_23 = value;
		Il2CppCodeGenWriteBarrier((&___hoverOut_23), value);
	}

	inline static int32_t get_offset_of_mLabel_24() { return static_cast<int32_t>(offsetof(TweenLetters_t3284132802, ___mLabel_24)); }
	inline UILabel_t3248798549 * get_mLabel_24() const { return ___mLabel_24; }
	inline UILabel_t3248798549 ** get_address_of_mLabel_24() { return &___mLabel_24; }
	inline void set_mLabel_24(UILabel_t3248798549 * value)
	{
		___mLabel_24 = value;
		Il2CppCodeGenWriteBarrier((&___mLabel_24), value);
	}

	inline static int32_t get_offset_of_mVertexCount_25() { return static_cast<int32_t>(offsetof(TweenLetters_t3284132802, ___mVertexCount_25)); }
	inline int32_t get_mVertexCount_25() const { return ___mVertexCount_25; }
	inline int32_t* get_address_of_mVertexCount_25() { return &___mVertexCount_25; }
	inline void set_mVertexCount_25(int32_t value)
	{
		___mVertexCount_25 = value;
	}

	inline static int32_t get_offset_of_mLetterOrder_26() { return static_cast<int32_t>(offsetof(TweenLetters_t3284132802, ___mLetterOrder_26)); }
	inline Int32U5BU5D_t385246372* get_mLetterOrder_26() const { return ___mLetterOrder_26; }
	inline Int32U5BU5D_t385246372** get_address_of_mLetterOrder_26() { return &___mLetterOrder_26; }
	inline void set_mLetterOrder_26(Int32U5BU5D_t385246372* value)
	{
		___mLetterOrder_26 = value;
		Il2CppCodeGenWriteBarrier((&___mLetterOrder_26), value);
	}

	inline static int32_t get_offset_of_mLetter_27() { return static_cast<int32_t>(offsetof(TweenLetters_t3284132802, ___mLetter_27)); }
	inline LetterPropertiesU5BU5D_t3676037960* get_mLetter_27() const { return ___mLetter_27; }
	inline LetterPropertiesU5BU5D_t3676037960** get_address_of_mLetter_27() { return &___mLetter_27; }
	inline void set_mLetter_27(LetterPropertiesU5BU5D_t3676037960* value)
	{
		___mLetter_27 = value;
		Il2CppCodeGenWriteBarrier((&___mLetter_27), value);
	}

	inline static int32_t get_offset_of_mCurrent_28() { return static_cast<int32_t>(offsetof(TweenLetters_t3284132802, ___mCurrent_28)); }
	inline AnimationProperties_t1280364982 * get_mCurrent_28() const { return ___mCurrent_28; }
	inline AnimationProperties_t1280364982 ** get_address_of_mCurrent_28() { return &___mCurrent_28; }
	inline void set_mCurrent_28(AnimationProperties_t1280364982 * value)
	{
		___mCurrent_28 = value;
		Il2CppCodeGenWriteBarrier((&___mCurrent_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENLETTERS_T3284132802_H
#ifndef TWEENFILL_T1298028023_H
#define TWEENFILL_T1298028023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenFill
struct  TweenFill_t1298028023  : public UITweener_t260334902
{
public:
	// System.Single TweenFill::from
	float ___from_22;
	// System.Single TweenFill::to
	float ___to_23;
	// System.Boolean TweenFill::mCached
	bool ___mCached_24;
	// UIBasicSprite TweenFill::mSprite
	UIBasicSprite_t1521297657 * ___mSprite_25;

public:
	inline static int32_t get_offset_of_from_22() { return static_cast<int32_t>(offsetof(TweenFill_t1298028023, ___from_22)); }
	inline float get_from_22() const { return ___from_22; }
	inline float* get_address_of_from_22() { return &___from_22; }
	inline void set_from_22(float value)
	{
		___from_22 = value;
	}

	inline static int32_t get_offset_of_to_23() { return static_cast<int32_t>(offsetof(TweenFill_t1298028023, ___to_23)); }
	inline float get_to_23() const { return ___to_23; }
	inline float* get_address_of_to_23() { return &___to_23; }
	inline void set_to_23(float value)
	{
		___to_23 = value;
	}

	inline static int32_t get_offset_of_mCached_24() { return static_cast<int32_t>(offsetof(TweenFill_t1298028023, ___mCached_24)); }
	inline bool get_mCached_24() const { return ___mCached_24; }
	inline bool* get_address_of_mCached_24() { return &___mCached_24; }
	inline void set_mCached_24(bool value)
	{
		___mCached_24 = value;
	}

	inline static int32_t get_offset_of_mSprite_25() { return static_cast<int32_t>(offsetof(TweenFill_t1298028023, ___mSprite_25)); }
	inline UIBasicSprite_t1521297657 * get_mSprite_25() const { return ___mSprite_25; }
	inline UIBasicSprite_t1521297657 ** get_address_of_mSprite_25() { return &___mSprite_25; }
	inline void set_mSprite_25(UIBasicSprite_t1521297657 * value)
	{
		___mSprite_25 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENFILL_T1298028023_H
#ifndef UIWIDGET_T3538521925_H
#define UIWIDGET_T3538521925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget
struct  UIWidget_t3538521925  : public UIRect_t2875960382
{
public:
	// UnityEngine.Color UIWidget::mColor
	Color_t2555686324  ___mColor_22;
	// UIWidget/Pivot UIWidget::mPivot
	int32_t ___mPivot_23;
	// System.Int32 UIWidget::mWidth
	int32_t ___mWidth_24;
	// System.Int32 UIWidget::mHeight
	int32_t ___mHeight_25;
	// System.Int32 UIWidget::mDepth
	int32_t ___mDepth_26;
	// UnityEngine.Material UIWidget::mMat
	Material_t340375123 * ___mMat_27;
	// UIWidget/OnDimensionsChanged UIWidget::onChange
	OnDimensionsChanged_t3101921181 * ___onChange_28;
	// UIWidget/OnPostFillCallback UIWidget::onPostFill
	OnPostFillCallback_t2835645043 * ___onPostFill_29;
	// UIDrawCall/OnRenderCallback UIWidget::mOnRender
	OnRenderCallback_t133425655 * ___mOnRender_30;
	// System.Boolean UIWidget::autoResizeBoxCollider
	bool ___autoResizeBoxCollider_31;
	// System.Boolean UIWidget::hideIfOffScreen
	bool ___hideIfOffScreen_32;
	// UIWidget/AspectRatioSource UIWidget::keepAspectRatio
	int32_t ___keepAspectRatio_33;
	// System.Single UIWidget::aspectRatio
	float ___aspectRatio_34;
	// UIWidget/HitCheck UIWidget::hitCheck
	HitCheck_t2300079615 * ___hitCheck_35;
	// UIPanel UIWidget::panel
	UIPanel_t1716472341 * ___panel_36;
	// UIGeometry UIWidget::geometry
	UIGeometry_t1059483952 * ___geometry_37;
	// System.Boolean UIWidget::fillGeometry
	bool ___fillGeometry_38;
	// System.Boolean UIWidget::mPlayMode
	bool ___mPlayMode_39;
	// UnityEngine.Vector4 UIWidget::mDrawRegion
	Vector4_t3319028937  ___mDrawRegion_40;
	// UnityEngine.Matrix4x4 UIWidget::mLocalToPanel
	Matrix4x4_t1817901843  ___mLocalToPanel_41;
	// System.Boolean UIWidget::mIsVisibleByAlpha
	bool ___mIsVisibleByAlpha_42;
	// System.Boolean UIWidget::mIsVisibleByPanel
	bool ___mIsVisibleByPanel_43;
	// System.Boolean UIWidget::mIsInFront
	bool ___mIsInFront_44;
	// System.Single UIWidget::mLastAlpha
	float ___mLastAlpha_45;
	// System.Boolean UIWidget::mMoved
	bool ___mMoved_46;
	// UIDrawCall UIWidget::drawCall
	UIDrawCall_t1293405319 * ___drawCall_47;
	// UnityEngine.Vector3[] UIWidget::mCorners
	Vector3U5BU5D_t1718750761* ___mCorners_48;
	// System.Int32 UIWidget::mAlphaFrameID
	int32_t ___mAlphaFrameID_49;
	// System.Int32 UIWidget::mMatrixFrame
	int32_t ___mMatrixFrame_50;
	// UnityEngine.Vector3 UIWidget::mOldV0
	Vector3_t3722313464  ___mOldV0_51;
	// UnityEngine.Vector3 UIWidget::mOldV1
	Vector3_t3722313464  ___mOldV1_52;

public:
	inline static int32_t get_offset_of_mColor_22() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mColor_22)); }
	inline Color_t2555686324  get_mColor_22() const { return ___mColor_22; }
	inline Color_t2555686324 * get_address_of_mColor_22() { return &___mColor_22; }
	inline void set_mColor_22(Color_t2555686324  value)
	{
		___mColor_22 = value;
	}

	inline static int32_t get_offset_of_mPivot_23() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mPivot_23)); }
	inline int32_t get_mPivot_23() const { return ___mPivot_23; }
	inline int32_t* get_address_of_mPivot_23() { return &___mPivot_23; }
	inline void set_mPivot_23(int32_t value)
	{
		___mPivot_23 = value;
	}

	inline static int32_t get_offset_of_mWidth_24() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mWidth_24)); }
	inline int32_t get_mWidth_24() const { return ___mWidth_24; }
	inline int32_t* get_address_of_mWidth_24() { return &___mWidth_24; }
	inline void set_mWidth_24(int32_t value)
	{
		___mWidth_24 = value;
	}

	inline static int32_t get_offset_of_mHeight_25() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mHeight_25)); }
	inline int32_t get_mHeight_25() const { return ___mHeight_25; }
	inline int32_t* get_address_of_mHeight_25() { return &___mHeight_25; }
	inline void set_mHeight_25(int32_t value)
	{
		___mHeight_25 = value;
	}

	inline static int32_t get_offset_of_mDepth_26() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mDepth_26)); }
	inline int32_t get_mDepth_26() const { return ___mDepth_26; }
	inline int32_t* get_address_of_mDepth_26() { return &___mDepth_26; }
	inline void set_mDepth_26(int32_t value)
	{
		___mDepth_26 = value;
	}

	inline static int32_t get_offset_of_mMat_27() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMat_27)); }
	inline Material_t340375123 * get_mMat_27() const { return ___mMat_27; }
	inline Material_t340375123 ** get_address_of_mMat_27() { return &___mMat_27; }
	inline void set_mMat_27(Material_t340375123 * value)
	{
		___mMat_27 = value;
		Il2CppCodeGenWriteBarrier((&___mMat_27), value);
	}

	inline static int32_t get_offset_of_onChange_28() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___onChange_28)); }
	inline OnDimensionsChanged_t3101921181 * get_onChange_28() const { return ___onChange_28; }
	inline OnDimensionsChanged_t3101921181 ** get_address_of_onChange_28() { return &___onChange_28; }
	inline void set_onChange_28(OnDimensionsChanged_t3101921181 * value)
	{
		___onChange_28 = value;
		Il2CppCodeGenWriteBarrier((&___onChange_28), value);
	}

	inline static int32_t get_offset_of_onPostFill_29() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___onPostFill_29)); }
	inline OnPostFillCallback_t2835645043 * get_onPostFill_29() const { return ___onPostFill_29; }
	inline OnPostFillCallback_t2835645043 ** get_address_of_onPostFill_29() { return &___onPostFill_29; }
	inline void set_onPostFill_29(OnPostFillCallback_t2835645043 * value)
	{
		___onPostFill_29 = value;
		Il2CppCodeGenWriteBarrier((&___onPostFill_29), value);
	}

	inline static int32_t get_offset_of_mOnRender_30() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOnRender_30)); }
	inline OnRenderCallback_t133425655 * get_mOnRender_30() const { return ___mOnRender_30; }
	inline OnRenderCallback_t133425655 ** get_address_of_mOnRender_30() { return &___mOnRender_30; }
	inline void set_mOnRender_30(OnRenderCallback_t133425655 * value)
	{
		___mOnRender_30 = value;
		Il2CppCodeGenWriteBarrier((&___mOnRender_30), value);
	}

	inline static int32_t get_offset_of_autoResizeBoxCollider_31() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___autoResizeBoxCollider_31)); }
	inline bool get_autoResizeBoxCollider_31() const { return ___autoResizeBoxCollider_31; }
	inline bool* get_address_of_autoResizeBoxCollider_31() { return &___autoResizeBoxCollider_31; }
	inline void set_autoResizeBoxCollider_31(bool value)
	{
		___autoResizeBoxCollider_31 = value;
	}

	inline static int32_t get_offset_of_hideIfOffScreen_32() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___hideIfOffScreen_32)); }
	inline bool get_hideIfOffScreen_32() const { return ___hideIfOffScreen_32; }
	inline bool* get_address_of_hideIfOffScreen_32() { return &___hideIfOffScreen_32; }
	inline void set_hideIfOffScreen_32(bool value)
	{
		___hideIfOffScreen_32 = value;
	}

	inline static int32_t get_offset_of_keepAspectRatio_33() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___keepAspectRatio_33)); }
	inline int32_t get_keepAspectRatio_33() const { return ___keepAspectRatio_33; }
	inline int32_t* get_address_of_keepAspectRatio_33() { return &___keepAspectRatio_33; }
	inline void set_keepAspectRatio_33(int32_t value)
	{
		___keepAspectRatio_33 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_34() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___aspectRatio_34)); }
	inline float get_aspectRatio_34() const { return ___aspectRatio_34; }
	inline float* get_address_of_aspectRatio_34() { return &___aspectRatio_34; }
	inline void set_aspectRatio_34(float value)
	{
		___aspectRatio_34 = value;
	}

	inline static int32_t get_offset_of_hitCheck_35() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___hitCheck_35)); }
	inline HitCheck_t2300079615 * get_hitCheck_35() const { return ___hitCheck_35; }
	inline HitCheck_t2300079615 ** get_address_of_hitCheck_35() { return &___hitCheck_35; }
	inline void set_hitCheck_35(HitCheck_t2300079615 * value)
	{
		___hitCheck_35 = value;
		Il2CppCodeGenWriteBarrier((&___hitCheck_35), value);
	}

	inline static int32_t get_offset_of_panel_36() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___panel_36)); }
	inline UIPanel_t1716472341 * get_panel_36() const { return ___panel_36; }
	inline UIPanel_t1716472341 ** get_address_of_panel_36() { return &___panel_36; }
	inline void set_panel_36(UIPanel_t1716472341 * value)
	{
		___panel_36 = value;
		Il2CppCodeGenWriteBarrier((&___panel_36), value);
	}

	inline static int32_t get_offset_of_geometry_37() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___geometry_37)); }
	inline UIGeometry_t1059483952 * get_geometry_37() const { return ___geometry_37; }
	inline UIGeometry_t1059483952 ** get_address_of_geometry_37() { return &___geometry_37; }
	inline void set_geometry_37(UIGeometry_t1059483952 * value)
	{
		___geometry_37 = value;
		Il2CppCodeGenWriteBarrier((&___geometry_37), value);
	}

	inline static int32_t get_offset_of_fillGeometry_38() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___fillGeometry_38)); }
	inline bool get_fillGeometry_38() const { return ___fillGeometry_38; }
	inline bool* get_address_of_fillGeometry_38() { return &___fillGeometry_38; }
	inline void set_fillGeometry_38(bool value)
	{
		___fillGeometry_38 = value;
	}

	inline static int32_t get_offset_of_mPlayMode_39() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mPlayMode_39)); }
	inline bool get_mPlayMode_39() const { return ___mPlayMode_39; }
	inline bool* get_address_of_mPlayMode_39() { return &___mPlayMode_39; }
	inline void set_mPlayMode_39(bool value)
	{
		___mPlayMode_39 = value;
	}

	inline static int32_t get_offset_of_mDrawRegion_40() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mDrawRegion_40)); }
	inline Vector4_t3319028937  get_mDrawRegion_40() const { return ___mDrawRegion_40; }
	inline Vector4_t3319028937 * get_address_of_mDrawRegion_40() { return &___mDrawRegion_40; }
	inline void set_mDrawRegion_40(Vector4_t3319028937  value)
	{
		___mDrawRegion_40 = value;
	}

	inline static int32_t get_offset_of_mLocalToPanel_41() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mLocalToPanel_41)); }
	inline Matrix4x4_t1817901843  get_mLocalToPanel_41() const { return ___mLocalToPanel_41; }
	inline Matrix4x4_t1817901843 * get_address_of_mLocalToPanel_41() { return &___mLocalToPanel_41; }
	inline void set_mLocalToPanel_41(Matrix4x4_t1817901843  value)
	{
		___mLocalToPanel_41 = value;
	}

	inline static int32_t get_offset_of_mIsVisibleByAlpha_42() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsVisibleByAlpha_42)); }
	inline bool get_mIsVisibleByAlpha_42() const { return ___mIsVisibleByAlpha_42; }
	inline bool* get_address_of_mIsVisibleByAlpha_42() { return &___mIsVisibleByAlpha_42; }
	inline void set_mIsVisibleByAlpha_42(bool value)
	{
		___mIsVisibleByAlpha_42 = value;
	}

	inline static int32_t get_offset_of_mIsVisibleByPanel_43() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsVisibleByPanel_43)); }
	inline bool get_mIsVisibleByPanel_43() const { return ___mIsVisibleByPanel_43; }
	inline bool* get_address_of_mIsVisibleByPanel_43() { return &___mIsVisibleByPanel_43; }
	inline void set_mIsVisibleByPanel_43(bool value)
	{
		___mIsVisibleByPanel_43 = value;
	}

	inline static int32_t get_offset_of_mIsInFront_44() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mIsInFront_44)); }
	inline bool get_mIsInFront_44() const { return ___mIsInFront_44; }
	inline bool* get_address_of_mIsInFront_44() { return &___mIsInFront_44; }
	inline void set_mIsInFront_44(bool value)
	{
		___mIsInFront_44 = value;
	}

	inline static int32_t get_offset_of_mLastAlpha_45() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mLastAlpha_45)); }
	inline float get_mLastAlpha_45() const { return ___mLastAlpha_45; }
	inline float* get_address_of_mLastAlpha_45() { return &___mLastAlpha_45; }
	inline void set_mLastAlpha_45(float value)
	{
		___mLastAlpha_45 = value;
	}

	inline static int32_t get_offset_of_mMoved_46() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMoved_46)); }
	inline bool get_mMoved_46() const { return ___mMoved_46; }
	inline bool* get_address_of_mMoved_46() { return &___mMoved_46; }
	inline void set_mMoved_46(bool value)
	{
		___mMoved_46 = value;
	}

	inline static int32_t get_offset_of_drawCall_47() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___drawCall_47)); }
	inline UIDrawCall_t1293405319 * get_drawCall_47() const { return ___drawCall_47; }
	inline UIDrawCall_t1293405319 ** get_address_of_drawCall_47() { return &___drawCall_47; }
	inline void set_drawCall_47(UIDrawCall_t1293405319 * value)
	{
		___drawCall_47 = value;
		Il2CppCodeGenWriteBarrier((&___drawCall_47), value);
	}

	inline static int32_t get_offset_of_mCorners_48() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mCorners_48)); }
	inline Vector3U5BU5D_t1718750761* get_mCorners_48() const { return ___mCorners_48; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mCorners_48() { return &___mCorners_48; }
	inline void set_mCorners_48(Vector3U5BU5D_t1718750761* value)
	{
		___mCorners_48 = value;
		Il2CppCodeGenWriteBarrier((&___mCorners_48), value);
	}

	inline static int32_t get_offset_of_mAlphaFrameID_49() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mAlphaFrameID_49)); }
	inline int32_t get_mAlphaFrameID_49() const { return ___mAlphaFrameID_49; }
	inline int32_t* get_address_of_mAlphaFrameID_49() { return &___mAlphaFrameID_49; }
	inline void set_mAlphaFrameID_49(int32_t value)
	{
		___mAlphaFrameID_49 = value;
	}

	inline static int32_t get_offset_of_mMatrixFrame_50() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mMatrixFrame_50)); }
	inline int32_t get_mMatrixFrame_50() const { return ___mMatrixFrame_50; }
	inline int32_t* get_address_of_mMatrixFrame_50() { return &___mMatrixFrame_50; }
	inline void set_mMatrixFrame_50(int32_t value)
	{
		___mMatrixFrame_50 = value;
	}

	inline static int32_t get_offset_of_mOldV0_51() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOldV0_51)); }
	inline Vector3_t3722313464  get_mOldV0_51() const { return ___mOldV0_51; }
	inline Vector3_t3722313464 * get_address_of_mOldV0_51() { return &___mOldV0_51; }
	inline void set_mOldV0_51(Vector3_t3722313464  value)
	{
		___mOldV0_51 = value;
	}

	inline static int32_t get_offset_of_mOldV1_52() { return static_cast<int32_t>(offsetof(UIWidget_t3538521925, ___mOldV1_52)); }
	inline Vector3_t3722313464  get_mOldV1_52() const { return ___mOldV1_52; }
	inline Vector3_t3722313464 * get_address_of_mOldV1_52() { return &___mOldV1_52; }
	inline void set_mOldV1_52(Vector3_t3722313464  value)
	{
		___mOldV1_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWIDGET_T3538521925_H
#ifndef SINGLETONMONOBEHAVIOUR_1_T80899550_H
#define SINGLETONMONOBEHAVIOUR_1_T80899550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<ResultController>
struct  SingletonMonoBehaviour_1_t80899550  : public MonoBehaviourWithInit_t1117120792
{
public:

public:
};

struct SingletonMonoBehaviour_1_t80899550_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::_instance
	ResultController_t2106065388 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t80899550_StaticFields, ____instance_3)); }
	inline ResultController_t2106065388 * get__instance_3() const { return ____instance_3; }
	inline ResultController_t2106065388 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(ResultController_t2106065388 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONMONOBEHAVIOUR_1_T80899550_H
#ifndef UIPANEL_T1716472341_H
#define UIPANEL_T1716472341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanel
struct  UIPanel_t1716472341  : public UIRect_t2875960382
{
public:
	// UIPanel/OnGeometryUpdated UIPanel::onGeometryUpdated
	OnGeometryUpdated_t2462438111 * ___onGeometryUpdated_23;
	// System.Boolean UIPanel::showInPanelTool
	bool ___showInPanelTool_24;
	// System.Boolean UIPanel::generateNormals
	bool ___generateNormals_25;
	// System.Boolean UIPanel::generateUV2
	bool ___generateUV2_26;
	// UIDrawCall/ShadowMode UIPanel::shadowMode
	int32_t ___shadowMode_27;
	// System.Boolean UIPanel::widgetsAreStatic
	bool ___widgetsAreStatic_28;
	// System.Boolean UIPanel::cullWhileDragging
	bool ___cullWhileDragging_29;
	// System.Boolean UIPanel::alwaysOnScreen
	bool ___alwaysOnScreen_30;
	// System.Boolean UIPanel::anchorOffset
	bool ___anchorOffset_31;
	// System.Boolean UIPanel::softBorderPadding
	bool ___softBorderPadding_32;
	// UIPanel/RenderQueue UIPanel::renderQueue
	int32_t ___renderQueue_33;
	// System.Int32 UIPanel::startingRenderQueue
	int32_t ___startingRenderQueue_34;
	// System.Collections.Generic.List`1<UIWidget> UIPanel::widgets
	List_1_t715629371 * ___widgets_35;
	// System.Collections.Generic.List`1<UIDrawCall> UIPanel::drawCalls
	List_1_t2765480061 * ___drawCalls_36;
	// UnityEngine.Matrix4x4 UIPanel::worldToLocal
	Matrix4x4_t1817901843  ___worldToLocal_37;
	// UnityEngine.Vector4 UIPanel::drawCallClipRange
	Vector4_t3319028937  ___drawCallClipRange_38;
	// UIPanel/OnClippingMoved UIPanel::onClipMove
	OnClippingMoved_t476625095 * ___onClipMove_39;
	// UIPanel/OnCreateMaterial UIPanel::onCreateMaterial
	OnCreateMaterial_t2961246930 * ___onCreateMaterial_40;
	// UIDrawCall/OnCreateDrawCall UIPanel::onCreateDrawCall
	OnCreateDrawCall_t609469653 * ___onCreateDrawCall_41;
	// UnityEngine.Texture2D UIPanel::mClipTexture
	Texture2D_t3840446185 * ___mClipTexture_42;
	// System.Single UIPanel::mAlpha
	float ___mAlpha_43;
	// UIDrawCall/Clipping UIPanel::mClipping
	int32_t ___mClipping_44;
	// UnityEngine.Vector4 UIPanel::mClipRange
	Vector4_t3319028937  ___mClipRange_45;
	// UnityEngine.Vector2 UIPanel::mClipSoftness
	Vector2_t2156229523  ___mClipSoftness_46;
	// System.Int32 UIPanel::mDepth
	int32_t ___mDepth_47;
	// System.Int32 UIPanel::mSortingOrder
	int32_t ___mSortingOrder_48;
	// System.String UIPanel::mSortingLayerName
	String_t* ___mSortingLayerName_49;
	// System.Boolean UIPanel::mRebuild
	bool ___mRebuild_50;
	// System.Boolean UIPanel::mResized
	bool ___mResized_51;
	// UnityEngine.Vector2 UIPanel::mClipOffset
	Vector2_t2156229523  ___mClipOffset_52;
	// System.Int32 UIPanel::mMatrixFrame
	int32_t ___mMatrixFrame_53;
	// System.Int32 UIPanel::mAlphaFrameID
	int32_t ___mAlphaFrameID_54;
	// System.Int32 UIPanel::mLayer
	int32_t ___mLayer_55;
	// UnityEngine.Vector2 UIPanel::mMin
	Vector2_t2156229523  ___mMin_57;
	// UnityEngine.Vector2 UIPanel::mMax
	Vector2_t2156229523  ___mMax_58;
	// System.Boolean UIPanel::mSortWidgets
	bool ___mSortWidgets_59;
	// System.Boolean UIPanel::mUpdateScroll
	bool ___mUpdateScroll_60;
	// System.Boolean UIPanel::useSortingOrder
	bool ___useSortingOrder_61;
	// UIPanel UIPanel::mParentPanel
	UIPanel_t1716472341 * ___mParentPanel_62;
	// System.Boolean UIPanel::mHasMoved
	bool ___mHasMoved_65;
	// UIDrawCall/OnRenderCallback UIPanel::mOnRender
	OnRenderCallback_t133425655 * ___mOnRender_66;
	// System.Boolean UIPanel::mForced
	bool ___mForced_67;

public:
	inline static int32_t get_offset_of_onGeometryUpdated_23() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onGeometryUpdated_23)); }
	inline OnGeometryUpdated_t2462438111 * get_onGeometryUpdated_23() const { return ___onGeometryUpdated_23; }
	inline OnGeometryUpdated_t2462438111 ** get_address_of_onGeometryUpdated_23() { return &___onGeometryUpdated_23; }
	inline void set_onGeometryUpdated_23(OnGeometryUpdated_t2462438111 * value)
	{
		___onGeometryUpdated_23 = value;
		Il2CppCodeGenWriteBarrier((&___onGeometryUpdated_23), value);
	}

	inline static int32_t get_offset_of_showInPanelTool_24() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___showInPanelTool_24)); }
	inline bool get_showInPanelTool_24() const { return ___showInPanelTool_24; }
	inline bool* get_address_of_showInPanelTool_24() { return &___showInPanelTool_24; }
	inline void set_showInPanelTool_24(bool value)
	{
		___showInPanelTool_24 = value;
	}

	inline static int32_t get_offset_of_generateNormals_25() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___generateNormals_25)); }
	inline bool get_generateNormals_25() const { return ___generateNormals_25; }
	inline bool* get_address_of_generateNormals_25() { return &___generateNormals_25; }
	inline void set_generateNormals_25(bool value)
	{
		___generateNormals_25 = value;
	}

	inline static int32_t get_offset_of_generateUV2_26() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___generateUV2_26)); }
	inline bool get_generateUV2_26() const { return ___generateUV2_26; }
	inline bool* get_address_of_generateUV2_26() { return &___generateUV2_26; }
	inline void set_generateUV2_26(bool value)
	{
		___generateUV2_26 = value;
	}

	inline static int32_t get_offset_of_shadowMode_27() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___shadowMode_27)); }
	inline int32_t get_shadowMode_27() const { return ___shadowMode_27; }
	inline int32_t* get_address_of_shadowMode_27() { return &___shadowMode_27; }
	inline void set_shadowMode_27(int32_t value)
	{
		___shadowMode_27 = value;
	}

	inline static int32_t get_offset_of_widgetsAreStatic_28() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___widgetsAreStatic_28)); }
	inline bool get_widgetsAreStatic_28() const { return ___widgetsAreStatic_28; }
	inline bool* get_address_of_widgetsAreStatic_28() { return &___widgetsAreStatic_28; }
	inline void set_widgetsAreStatic_28(bool value)
	{
		___widgetsAreStatic_28 = value;
	}

	inline static int32_t get_offset_of_cullWhileDragging_29() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___cullWhileDragging_29)); }
	inline bool get_cullWhileDragging_29() const { return ___cullWhileDragging_29; }
	inline bool* get_address_of_cullWhileDragging_29() { return &___cullWhileDragging_29; }
	inline void set_cullWhileDragging_29(bool value)
	{
		___cullWhileDragging_29 = value;
	}

	inline static int32_t get_offset_of_alwaysOnScreen_30() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___alwaysOnScreen_30)); }
	inline bool get_alwaysOnScreen_30() const { return ___alwaysOnScreen_30; }
	inline bool* get_address_of_alwaysOnScreen_30() { return &___alwaysOnScreen_30; }
	inline void set_alwaysOnScreen_30(bool value)
	{
		___alwaysOnScreen_30 = value;
	}

	inline static int32_t get_offset_of_anchorOffset_31() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___anchorOffset_31)); }
	inline bool get_anchorOffset_31() const { return ___anchorOffset_31; }
	inline bool* get_address_of_anchorOffset_31() { return &___anchorOffset_31; }
	inline void set_anchorOffset_31(bool value)
	{
		___anchorOffset_31 = value;
	}

	inline static int32_t get_offset_of_softBorderPadding_32() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___softBorderPadding_32)); }
	inline bool get_softBorderPadding_32() const { return ___softBorderPadding_32; }
	inline bool* get_address_of_softBorderPadding_32() { return &___softBorderPadding_32; }
	inline void set_softBorderPadding_32(bool value)
	{
		___softBorderPadding_32 = value;
	}

	inline static int32_t get_offset_of_renderQueue_33() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___renderQueue_33)); }
	inline int32_t get_renderQueue_33() const { return ___renderQueue_33; }
	inline int32_t* get_address_of_renderQueue_33() { return &___renderQueue_33; }
	inline void set_renderQueue_33(int32_t value)
	{
		___renderQueue_33 = value;
	}

	inline static int32_t get_offset_of_startingRenderQueue_34() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___startingRenderQueue_34)); }
	inline int32_t get_startingRenderQueue_34() const { return ___startingRenderQueue_34; }
	inline int32_t* get_address_of_startingRenderQueue_34() { return &___startingRenderQueue_34; }
	inline void set_startingRenderQueue_34(int32_t value)
	{
		___startingRenderQueue_34 = value;
	}

	inline static int32_t get_offset_of_widgets_35() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___widgets_35)); }
	inline List_1_t715629371 * get_widgets_35() const { return ___widgets_35; }
	inline List_1_t715629371 ** get_address_of_widgets_35() { return &___widgets_35; }
	inline void set_widgets_35(List_1_t715629371 * value)
	{
		___widgets_35 = value;
		Il2CppCodeGenWriteBarrier((&___widgets_35), value);
	}

	inline static int32_t get_offset_of_drawCalls_36() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___drawCalls_36)); }
	inline List_1_t2765480061 * get_drawCalls_36() const { return ___drawCalls_36; }
	inline List_1_t2765480061 ** get_address_of_drawCalls_36() { return &___drawCalls_36; }
	inline void set_drawCalls_36(List_1_t2765480061 * value)
	{
		___drawCalls_36 = value;
		Il2CppCodeGenWriteBarrier((&___drawCalls_36), value);
	}

	inline static int32_t get_offset_of_worldToLocal_37() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___worldToLocal_37)); }
	inline Matrix4x4_t1817901843  get_worldToLocal_37() const { return ___worldToLocal_37; }
	inline Matrix4x4_t1817901843 * get_address_of_worldToLocal_37() { return &___worldToLocal_37; }
	inline void set_worldToLocal_37(Matrix4x4_t1817901843  value)
	{
		___worldToLocal_37 = value;
	}

	inline static int32_t get_offset_of_drawCallClipRange_38() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___drawCallClipRange_38)); }
	inline Vector4_t3319028937  get_drawCallClipRange_38() const { return ___drawCallClipRange_38; }
	inline Vector4_t3319028937 * get_address_of_drawCallClipRange_38() { return &___drawCallClipRange_38; }
	inline void set_drawCallClipRange_38(Vector4_t3319028937  value)
	{
		___drawCallClipRange_38 = value;
	}

	inline static int32_t get_offset_of_onClipMove_39() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onClipMove_39)); }
	inline OnClippingMoved_t476625095 * get_onClipMove_39() const { return ___onClipMove_39; }
	inline OnClippingMoved_t476625095 ** get_address_of_onClipMove_39() { return &___onClipMove_39; }
	inline void set_onClipMove_39(OnClippingMoved_t476625095 * value)
	{
		___onClipMove_39 = value;
		Il2CppCodeGenWriteBarrier((&___onClipMove_39), value);
	}

	inline static int32_t get_offset_of_onCreateMaterial_40() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onCreateMaterial_40)); }
	inline OnCreateMaterial_t2961246930 * get_onCreateMaterial_40() const { return ___onCreateMaterial_40; }
	inline OnCreateMaterial_t2961246930 ** get_address_of_onCreateMaterial_40() { return &___onCreateMaterial_40; }
	inline void set_onCreateMaterial_40(OnCreateMaterial_t2961246930 * value)
	{
		___onCreateMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateMaterial_40), value);
	}

	inline static int32_t get_offset_of_onCreateDrawCall_41() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___onCreateDrawCall_41)); }
	inline OnCreateDrawCall_t609469653 * get_onCreateDrawCall_41() const { return ___onCreateDrawCall_41; }
	inline OnCreateDrawCall_t609469653 ** get_address_of_onCreateDrawCall_41() { return &___onCreateDrawCall_41; }
	inline void set_onCreateDrawCall_41(OnCreateDrawCall_t609469653 * value)
	{
		___onCreateDrawCall_41 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateDrawCall_41), value);
	}

	inline static int32_t get_offset_of_mClipTexture_42() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipTexture_42)); }
	inline Texture2D_t3840446185 * get_mClipTexture_42() const { return ___mClipTexture_42; }
	inline Texture2D_t3840446185 ** get_address_of_mClipTexture_42() { return &___mClipTexture_42; }
	inline void set_mClipTexture_42(Texture2D_t3840446185 * value)
	{
		___mClipTexture_42 = value;
		Il2CppCodeGenWriteBarrier((&___mClipTexture_42), value);
	}

	inline static int32_t get_offset_of_mAlpha_43() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mAlpha_43)); }
	inline float get_mAlpha_43() const { return ___mAlpha_43; }
	inline float* get_address_of_mAlpha_43() { return &___mAlpha_43; }
	inline void set_mAlpha_43(float value)
	{
		___mAlpha_43 = value;
	}

	inline static int32_t get_offset_of_mClipping_44() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipping_44)); }
	inline int32_t get_mClipping_44() const { return ___mClipping_44; }
	inline int32_t* get_address_of_mClipping_44() { return &___mClipping_44; }
	inline void set_mClipping_44(int32_t value)
	{
		___mClipping_44 = value;
	}

	inline static int32_t get_offset_of_mClipRange_45() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipRange_45)); }
	inline Vector4_t3319028937  get_mClipRange_45() const { return ___mClipRange_45; }
	inline Vector4_t3319028937 * get_address_of_mClipRange_45() { return &___mClipRange_45; }
	inline void set_mClipRange_45(Vector4_t3319028937  value)
	{
		___mClipRange_45 = value;
	}

	inline static int32_t get_offset_of_mClipSoftness_46() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipSoftness_46)); }
	inline Vector2_t2156229523  get_mClipSoftness_46() const { return ___mClipSoftness_46; }
	inline Vector2_t2156229523 * get_address_of_mClipSoftness_46() { return &___mClipSoftness_46; }
	inline void set_mClipSoftness_46(Vector2_t2156229523  value)
	{
		___mClipSoftness_46 = value;
	}

	inline static int32_t get_offset_of_mDepth_47() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mDepth_47)); }
	inline int32_t get_mDepth_47() const { return ___mDepth_47; }
	inline int32_t* get_address_of_mDepth_47() { return &___mDepth_47; }
	inline void set_mDepth_47(int32_t value)
	{
		___mDepth_47 = value;
	}

	inline static int32_t get_offset_of_mSortingOrder_48() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mSortingOrder_48)); }
	inline int32_t get_mSortingOrder_48() const { return ___mSortingOrder_48; }
	inline int32_t* get_address_of_mSortingOrder_48() { return &___mSortingOrder_48; }
	inline void set_mSortingOrder_48(int32_t value)
	{
		___mSortingOrder_48 = value;
	}

	inline static int32_t get_offset_of_mSortingLayerName_49() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mSortingLayerName_49)); }
	inline String_t* get_mSortingLayerName_49() const { return ___mSortingLayerName_49; }
	inline String_t** get_address_of_mSortingLayerName_49() { return &___mSortingLayerName_49; }
	inline void set_mSortingLayerName_49(String_t* value)
	{
		___mSortingLayerName_49 = value;
		Il2CppCodeGenWriteBarrier((&___mSortingLayerName_49), value);
	}

	inline static int32_t get_offset_of_mRebuild_50() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mRebuild_50)); }
	inline bool get_mRebuild_50() const { return ___mRebuild_50; }
	inline bool* get_address_of_mRebuild_50() { return &___mRebuild_50; }
	inline void set_mRebuild_50(bool value)
	{
		___mRebuild_50 = value;
	}

	inline static int32_t get_offset_of_mResized_51() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mResized_51)); }
	inline bool get_mResized_51() const { return ___mResized_51; }
	inline bool* get_address_of_mResized_51() { return &___mResized_51; }
	inline void set_mResized_51(bool value)
	{
		___mResized_51 = value;
	}

	inline static int32_t get_offset_of_mClipOffset_52() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mClipOffset_52)); }
	inline Vector2_t2156229523  get_mClipOffset_52() const { return ___mClipOffset_52; }
	inline Vector2_t2156229523 * get_address_of_mClipOffset_52() { return &___mClipOffset_52; }
	inline void set_mClipOffset_52(Vector2_t2156229523  value)
	{
		___mClipOffset_52 = value;
	}

	inline static int32_t get_offset_of_mMatrixFrame_53() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mMatrixFrame_53)); }
	inline int32_t get_mMatrixFrame_53() const { return ___mMatrixFrame_53; }
	inline int32_t* get_address_of_mMatrixFrame_53() { return &___mMatrixFrame_53; }
	inline void set_mMatrixFrame_53(int32_t value)
	{
		___mMatrixFrame_53 = value;
	}

	inline static int32_t get_offset_of_mAlphaFrameID_54() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mAlphaFrameID_54)); }
	inline int32_t get_mAlphaFrameID_54() const { return ___mAlphaFrameID_54; }
	inline int32_t* get_address_of_mAlphaFrameID_54() { return &___mAlphaFrameID_54; }
	inline void set_mAlphaFrameID_54(int32_t value)
	{
		___mAlphaFrameID_54 = value;
	}

	inline static int32_t get_offset_of_mLayer_55() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mLayer_55)); }
	inline int32_t get_mLayer_55() const { return ___mLayer_55; }
	inline int32_t* get_address_of_mLayer_55() { return &___mLayer_55; }
	inline void set_mLayer_55(int32_t value)
	{
		___mLayer_55 = value;
	}

	inline static int32_t get_offset_of_mMin_57() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mMin_57)); }
	inline Vector2_t2156229523  get_mMin_57() const { return ___mMin_57; }
	inline Vector2_t2156229523 * get_address_of_mMin_57() { return &___mMin_57; }
	inline void set_mMin_57(Vector2_t2156229523  value)
	{
		___mMin_57 = value;
	}

	inline static int32_t get_offset_of_mMax_58() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mMax_58)); }
	inline Vector2_t2156229523  get_mMax_58() const { return ___mMax_58; }
	inline Vector2_t2156229523 * get_address_of_mMax_58() { return &___mMax_58; }
	inline void set_mMax_58(Vector2_t2156229523  value)
	{
		___mMax_58 = value;
	}

	inline static int32_t get_offset_of_mSortWidgets_59() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mSortWidgets_59)); }
	inline bool get_mSortWidgets_59() const { return ___mSortWidgets_59; }
	inline bool* get_address_of_mSortWidgets_59() { return &___mSortWidgets_59; }
	inline void set_mSortWidgets_59(bool value)
	{
		___mSortWidgets_59 = value;
	}

	inline static int32_t get_offset_of_mUpdateScroll_60() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mUpdateScroll_60)); }
	inline bool get_mUpdateScroll_60() const { return ___mUpdateScroll_60; }
	inline bool* get_address_of_mUpdateScroll_60() { return &___mUpdateScroll_60; }
	inline void set_mUpdateScroll_60(bool value)
	{
		___mUpdateScroll_60 = value;
	}

	inline static int32_t get_offset_of_useSortingOrder_61() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___useSortingOrder_61)); }
	inline bool get_useSortingOrder_61() const { return ___useSortingOrder_61; }
	inline bool* get_address_of_useSortingOrder_61() { return &___useSortingOrder_61; }
	inline void set_useSortingOrder_61(bool value)
	{
		___useSortingOrder_61 = value;
	}

	inline static int32_t get_offset_of_mParentPanel_62() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mParentPanel_62)); }
	inline UIPanel_t1716472341 * get_mParentPanel_62() const { return ___mParentPanel_62; }
	inline UIPanel_t1716472341 ** get_address_of_mParentPanel_62() { return &___mParentPanel_62; }
	inline void set_mParentPanel_62(UIPanel_t1716472341 * value)
	{
		___mParentPanel_62 = value;
		Il2CppCodeGenWriteBarrier((&___mParentPanel_62), value);
	}

	inline static int32_t get_offset_of_mHasMoved_65() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mHasMoved_65)); }
	inline bool get_mHasMoved_65() const { return ___mHasMoved_65; }
	inline bool* get_address_of_mHasMoved_65() { return &___mHasMoved_65; }
	inline void set_mHasMoved_65(bool value)
	{
		___mHasMoved_65 = value;
	}

	inline static int32_t get_offset_of_mOnRender_66() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mOnRender_66)); }
	inline OnRenderCallback_t133425655 * get_mOnRender_66() const { return ___mOnRender_66; }
	inline OnRenderCallback_t133425655 ** get_address_of_mOnRender_66() { return &___mOnRender_66; }
	inline void set_mOnRender_66(OnRenderCallback_t133425655 * value)
	{
		___mOnRender_66 = value;
		Il2CppCodeGenWriteBarrier((&___mOnRender_66), value);
	}

	inline static int32_t get_offset_of_mForced_67() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341, ___mForced_67)); }
	inline bool get_mForced_67() const { return ___mForced_67; }
	inline bool* get_address_of_mForced_67() { return &___mForced_67; }
	inline void set_mForced_67(bool value)
	{
		___mForced_67 = value;
	}
};

struct UIPanel_t1716472341_StaticFields
{
public:
	// System.Collections.Generic.List`1<UIPanel> UIPanel::list
	List_1_t3188547083 * ___list_22;
	// System.Single[] UIPanel::mTemp
	SingleU5BU5D_t1444911251* ___mTemp_56;
	// UnityEngine.Vector3[] UIPanel::mCorners
	Vector3U5BU5D_t1718750761* ___mCorners_63;
	// System.Int32 UIPanel::mUpdateFrame
	int32_t ___mUpdateFrame_64;
	// System.Comparison`1<UIPanel> UIPanel::<>f__mg$cache0
	Comparison_1_t1491403520 * ___U3CU3Ef__mgU24cache0_68;
	// System.Comparison`1<UIPanel> UIPanel::<>f__mg$cache1
	Comparison_1_t1491403520 * ___U3CU3Ef__mgU24cache1_69;
	// System.Comparison`1<UIWidget> UIPanel::<>f__mg$cache2
	Comparison_1_t3313453104 * ___U3CU3Ef__mgU24cache2_70;

public:
	inline static int32_t get_offset_of_list_22() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___list_22)); }
	inline List_1_t3188547083 * get_list_22() const { return ___list_22; }
	inline List_1_t3188547083 ** get_address_of_list_22() { return &___list_22; }
	inline void set_list_22(List_1_t3188547083 * value)
	{
		___list_22 = value;
		Il2CppCodeGenWriteBarrier((&___list_22), value);
	}

	inline static int32_t get_offset_of_mTemp_56() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___mTemp_56)); }
	inline SingleU5BU5D_t1444911251* get_mTemp_56() const { return ___mTemp_56; }
	inline SingleU5BU5D_t1444911251** get_address_of_mTemp_56() { return &___mTemp_56; }
	inline void set_mTemp_56(SingleU5BU5D_t1444911251* value)
	{
		___mTemp_56 = value;
		Il2CppCodeGenWriteBarrier((&___mTemp_56), value);
	}

	inline static int32_t get_offset_of_mCorners_63() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___mCorners_63)); }
	inline Vector3U5BU5D_t1718750761* get_mCorners_63() const { return ___mCorners_63; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mCorners_63() { return &___mCorners_63; }
	inline void set_mCorners_63(Vector3U5BU5D_t1718750761* value)
	{
		___mCorners_63 = value;
		Il2CppCodeGenWriteBarrier((&___mCorners_63), value);
	}

	inline static int32_t get_offset_of_mUpdateFrame_64() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___mUpdateFrame_64)); }
	inline int32_t get_mUpdateFrame_64() const { return ___mUpdateFrame_64; }
	inline int32_t* get_address_of_mUpdateFrame_64() { return &___mUpdateFrame_64; }
	inline void set_mUpdateFrame_64(int32_t value)
	{
		___mUpdateFrame_64 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_68() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___U3CU3Ef__mgU24cache0_68)); }
	inline Comparison_1_t1491403520 * get_U3CU3Ef__mgU24cache0_68() const { return ___U3CU3Ef__mgU24cache0_68; }
	inline Comparison_1_t1491403520 ** get_address_of_U3CU3Ef__mgU24cache0_68() { return &___U3CU3Ef__mgU24cache0_68; }
	inline void set_U3CU3Ef__mgU24cache0_68(Comparison_1_t1491403520 * value)
	{
		___U3CU3Ef__mgU24cache0_68 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_68), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_69() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___U3CU3Ef__mgU24cache1_69)); }
	inline Comparison_1_t1491403520 * get_U3CU3Ef__mgU24cache1_69() const { return ___U3CU3Ef__mgU24cache1_69; }
	inline Comparison_1_t1491403520 ** get_address_of_U3CU3Ef__mgU24cache1_69() { return &___U3CU3Ef__mgU24cache1_69; }
	inline void set_U3CU3Ef__mgU24cache1_69(Comparison_1_t1491403520 * value)
	{
		___U3CU3Ef__mgU24cache1_69 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_69), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_70() { return static_cast<int32_t>(offsetof(UIPanel_t1716472341_StaticFields, ___U3CU3Ef__mgU24cache2_70)); }
	inline Comparison_1_t3313453104 * get_U3CU3Ef__mgU24cache2_70() const { return ___U3CU3Ef__mgU24cache2_70; }
	inline Comparison_1_t3313453104 ** get_address_of_U3CU3Ef__mgU24cache2_70() { return &___U3CU3Ef__mgU24cache2_70; }
	inline void set_U3CU3Ef__mgU24cache2_70(Comparison_1_t3313453104 * value)
	{
		___U3CU3Ef__mgU24cache2_70 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_70), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPANEL_T1716472341_H
#ifndef UIBASICSPRITE_T1521297657_H
#define UIBASICSPRITE_T1521297657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite
struct  UIBasicSprite_t1521297657  : public UIWidget_t3538521925
{
public:
	// UIBasicSprite/Type UIBasicSprite::mType
	int32_t ___mType_53;
	// UIBasicSprite/FillDirection UIBasicSprite::mFillDirection
	int32_t ___mFillDirection_54;
	// System.Single UIBasicSprite::mFillAmount
	float ___mFillAmount_55;
	// System.Boolean UIBasicSprite::mInvert
	bool ___mInvert_56;
	// UIBasicSprite/Flip UIBasicSprite::mFlip
	int32_t ___mFlip_57;
	// System.Boolean UIBasicSprite::mApplyGradient
	bool ___mApplyGradient_58;
	// UnityEngine.Color UIBasicSprite::mGradientTop
	Color_t2555686324  ___mGradientTop_59;
	// UnityEngine.Color UIBasicSprite::mGradientBottom
	Color_t2555686324  ___mGradientBottom_60;
	// UnityEngine.Rect UIBasicSprite::mInnerUV
	Rect_t2360479859  ___mInnerUV_61;
	// UnityEngine.Rect UIBasicSprite::mOuterUV
	Rect_t2360479859  ___mOuterUV_62;
	// UIBasicSprite/AdvancedType UIBasicSprite::centerType
	int32_t ___centerType_63;
	// UIBasicSprite/AdvancedType UIBasicSprite::leftType
	int32_t ___leftType_64;
	// UIBasicSprite/AdvancedType UIBasicSprite::rightType
	int32_t ___rightType_65;
	// UIBasicSprite/AdvancedType UIBasicSprite::bottomType
	int32_t ___bottomType_66;
	// UIBasicSprite/AdvancedType UIBasicSprite::topType
	int32_t ___topType_67;

public:
	inline static int32_t get_offset_of_mType_53() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mType_53)); }
	inline int32_t get_mType_53() const { return ___mType_53; }
	inline int32_t* get_address_of_mType_53() { return &___mType_53; }
	inline void set_mType_53(int32_t value)
	{
		___mType_53 = value;
	}

	inline static int32_t get_offset_of_mFillDirection_54() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFillDirection_54)); }
	inline int32_t get_mFillDirection_54() const { return ___mFillDirection_54; }
	inline int32_t* get_address_of_mFillDirection_54() { return &___mFillDirection_54; }
	inline void set_mFillDirection_54(int32_t value)
	{
		___mFillDirection_54 = value;
	}

	inline static int32_t get_offset_of_mFillAmount_55() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFillAmount_55)); }
	inline float get_mFillAmount_55() const { return ___mFillAmount_55; }
	inline float* get_address_of_mFillAmount_55() { return &___mFillAmount_55; }
	inline void set_mFillAmount_55(float value)
	{
		___mFillAmount_55 = value;
	}

	inline static int32_t get_offset_of_mInvert_56() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mInvert_56)); }
	inline bool get_mInvert_56() const { return ___mInvert_56; }
	inline bool* get_address_of_mInvert_56() { return &___mInvert_56; }
	inline void set_mInvert_56(bool value)
	{
		___mInvert_56 = value;
	}

	inline static int32_t get_offset_of_mFlip_57() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mFlip_57)); }
	inline int32_t get_mFlip_57() const { return ___mFlip_57; }
	inline int32_t* get_address_of_mFlip_57() { return &___mFlip_57; }
	inline void set_mFlip_57(int32_t value)
	{
		___mFlip_57 = value;
	}

	inline static int32_t get_offset_of_mApplyGradient_58() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mApplyGradient_58)); }
	inline bool get_mApplyGradient_58() const { return ___mApplyGradient_58; }
	inline bool* get_address_of_mApplyGradient_58() { return &___mApplyGradient_58; }
	inline void set_mApplyGradient_58(bool value)
	{
		___mApplyGradient_58 = value;
	}

	inline static int32_t get_offset_of_mGradientTop_59() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mGradientTop_59)); }
	inline Color_t2555686324  get_mGradientTop_59() const { return ___mGradientTop_59; }
	inline Color_t2555686324 * get_address_of_mGradientTop_59() { return &___mGradientTop_59; }
	inline void set_mGradientTop_59(Color_t2555686324  value)
	{
		___mGradientTop_59 = value;
	}

	inline static int32_t get_offset_of_mGradientBottom_60() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mGradientBottom_60)); }
	inline Color_t2555686324  get_mGradientBottom_60() const { return ___mGradientBottom_60; }
	inline Color_t2555686324 * get_address_of_mGradientBottom_60() { return &___mGradientBottom_60; }
	inline void set_mGradientBottom_60(Color_t2555686324  value)
	{
		___mGradientBottom_60 = value;
	}

	inline static int32_t get_offset_of_mInnerUV_61() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mInnerUV_61)); }
	inline Rect_t2360479859  get_mInnerUV_61() const { return ___mInnerUV_61; }
	inline Rect_t2360479859 * get_address_of_mInnerUV_61() { return &___mInnerUV_61; }
	inline void set_mInnerUV_61(Rect_t2360479859  value)
	{
		___mInnerUV_61 = value;
	}

	inline static int32_t get_offset_of_mOuterUV_62() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___mOuterUV_62)); }
	inline Rect_t2360479859  get_mOuterUV_62() const { return ___mOuterUV_62; }
	inline Rect_t2360479859 * get_address_of_mOuterUV_62() { return &___mOuterUV_62; }
	inline void set_mOuterUV_62(Rect_t2360479859  value)
	{
		___mOuterUV_62 = value;
	}

	inline static int32_t get_offset_of_centerType_63() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___centerType_63)); }
	inline int32_t get_centerType_63() const { return ___centerType_63; }
	inline int32_t* get_address_of_centerType_63() { return &___centerType_63; }
	inline void set_centerType_63(int32_t value)
	{
		___centerType_63 = value;
	}

	inline static int32_t get_offset_of_leftType_64() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___leftType_64)); }
	inline int32_t get_leftType_64() const { return ___leftType_64; }
	inline int32_t* get_address_of_leftType_64() { return &___leftType_64; }
	inline void set_leftType_64(int32_t value)
	{
		___leftType_64 = value;
	}

	inline static int32_t get_offset_of_rightType_65() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___rightType_65)); }
	inline int32_t get_rightType_65() const { return ___rightType_65; }
	inline int32_t* get_address_of_rightType_65() { return &___rightType_65; }
	inline void set_rightType_65(int32_t value)
	{
		___rightType_65 = value;
	}

	inline static int32_t get_offset_of_bottomType_66() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___bottomType_66)); }
	inline int32_t get_bottomType_66() const { return ___bottomType_66; }
	inline int32_t* get_address_of_bottomType_66() { return &___bottomType_66; }
	inline void set_bottomType_66(int32_t value)
	{
		___bottomType_66 = value;
	}

	inline static int32_t get_offset_of_topType_67() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657, ___topType_67)); }
	inline int32_t get_topType_67() const { return ___topType_67; }
	inline int32_t* get_address_of_topType_67() { return &___topType_67; }
	inline void set_topType_67(int32_t value)
	{
		___topType_67 = value;
	}
};

struct UIBasicSprite_t1521297657_StaticFields
{
public:
	// UnityEngine.Vector2[] UIBasicSprite::mTempPos
	Vector2U5BU5D_t1457185986* ___mTempPos_68;
	// UnityEngine.Vector2[] UIBasicSprite::mTempUVs
	Vector2U5BU5D_t1457185986* ___mTempUVs_69;

public:
	inline static int32_t get_offset_of_mTempPos_68() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657_StaticFields, ___mTempPos_68)); }
	inline Vector2U5BU5D_t1457185986* get_mTempPos_68() const { return ___mTempPos_68; }
	inline Vector2U5BU5D_t1457185986** get_address_of_mTempPos_68() { return &___mTempPos_68; }
	inline void set_mTempPos_68(Vector2U5BU5D_t1457185986* value)
	{
		___mTempPos_68 = value;
		Il2CppCodeGenWriteBarrier((&___mTempPos_68), value);
	}

	inline static int32_t get_offset_of_mTempUVs_69() { return static_cast<int32_t>(offsetof(UIBasicSprite_t1521297657_StaticFields, ___mTempUVs_69)); }
	inline Vector2U5BU5D_t1457185986* get_mTempUVs_69() const { return ___mTempUVs_69; }
	inline Vector2U5BU5D_t1457185986** get_address_of_mTempUVs_69() { return &___mTempUVs_69; }
	inline void set_mTempUVs_69(Vector2U5BU5D_t1457185986* value)
	{
		___mTempUVs_69 = value;
		Il2CppCodeGenWriteBarrier((&___mTempUVs_69), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBASICSPRITE_T1521297657_H
#ifndef RESULTCONTROLLER_T2106065388_H
#define RESULTCONTROLLER_T2106065388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultController
struct  ResultController_t2106065388  : public SingletonMonoBehaviour_1_t80899550
{
public:
	// UnityEngine.GameObject ResultController::_dialogRect
	GameObject_t1113636619 * ____dialogRect_4;
	// UnityEngine.GameObject ResultController::closeButton
	GameObject_t1113636619 * ___closeButton_5;
	// UnityEngine.GameObject ResultController::_blockRect
	GameObject_t1113636619 * ____blockRect_6;
	// System.Boolean ResultController::_showInterstitial
	bool ____showInterstitial_7;

public:
	inline static int32_t get_offset_of__dialogRect_4() { return static_cast<int32_t>(offsetof(ResultController_t2106065388, ____dialogRect_4)); }
	inline GameObject_t1113636619 * get__dialogRect_4() const { return ____dialogRect_4; }
	inline GameObject_t1113636619 ** get_address_of__dialogRect_4() { return &____dialogRect_4; }
	inline void set__dialogRect_4(GameObject_t1113636619 * value)
	{
		____dialogRect_4 = value;
		Il2CppCodeGenWriteBarrier((&____dialogRect_4), value);
	}

	inline static int32_t get_offset_of_closeButton_5() { return static_cast<int32_t>(offsetof(ResultController_t2106065388, ___closeButton_5)); }
	inline GameObject_t1113636619 * get_closeButton_5() const { return ___closeButton_5; }
	inline GameObject_t1113636619 ** get_address_of_closeButton_5() { return &___closeButton_5; }
	inline void set_closeButton_5(GameObject_t1113636619 * value)
	{
		___closeButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___closeButton_5), value);
	}

	inline static int32_t get_offset_of__blockRect_6() { return static_cast<int32_t>(offsetof(ResultController_t2106065388, ____blockRect_6)); }
	inline GameObject_t1113636619 * get__blockRect_6() const { return ____blockRect_6; }
	inline GameObject_t1113636619 ** get_address_of__blockRect_6() { return &____blockRect_6; }
	inline void set__blockRect_6(GameObject_t1113636619 * value)
	{
		____blockRect_6 = value;
		Il2CppCodeGenWriteBarrier((&____blockRect_6), value);
	}

	inline static int32_t get_offset_of__showInterstitial_7() { return static_cast<int32_t>(offsetof(ResultController_t2106065388, ____showInterstitial_7)); }
	inline bool get__showInterstitial_7() const { return ____showInterstitial_7; }
	inline bool* get_address_of__showInterstitial_7() { return &____showInterstitial_7; }
	inline void set__showInterstitial_7(bool value)
	{
		____showInterstitial_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTCONTROLLER_T2106065388_H
#ifndef UILABEL_T3248798549_H
#define UILABEL_T3248798549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel
struct  UILabel_t3248798549  : public UIWidget_t3538521925
{
public:
	// UILabel/Crispness UILabel::keepCrispWhenShrunk
	int32_t ___keepCrispWhenShrunk_53;
	// UnityEngine.Font UILabel::mTrueTypeFont
	Font_t1956802104 * ___mTrueTypeFont_54;
	// UIFont UILabel::mFont
	UIFont_t2766063701 * ___mFont_55;
	// System.String UILabel::mText
	String_t* ___mText_56;
	// System.Int32 UILabel::mFontSize
	int32_t ___mFontSize_57;
	// UnityEngine.FontStyle UILabel::mFontStyle
	int32_t ___mFontStyle_58;
	// NGUIText/Alignment UILabel::mAlignment
	int32_t ___mAlignment_59;
	// System.Boolean UILabel::mEncoding
	bool ___mEncoding_60;
	// System.Int32 UILabel::mMaxLineCount
	int32_t ___mMaxLineCount_61;
	// UILabel/Effect UILabel::mEffectStyle
	int32_t ___mEffectStyle_62;
	// UnityEngine.Color UILabel::mEffectColor
	Color_t2555686324  ___mEffectColor_63;
	// NGUIText/SymbolStyle UILabel::mSymbols
	int32_t ___mSymbols_64;
	// UnityEngine.Vector2 UILabel::mEffectDistance
	Vector2_t2156229523  ___mEffectDistance_65;
	// UILabel/Overflow UILabel::mOverflow
	int32_t ___mOverflow_66;
	// System.Boolean UILabel::mApplyGradient
	bool ___mApplyGradient_67;
	// UnityEngine.Color UILabel::mGradientTop
	Color_t2555686324  ___mGradientTop_68;
	// UnityEngine.Color UILabel::mGradientBottom
	Color_t2555686324  ___mGradientBottom_69;
	// System.Int32 UILabel::mSpacingX
	int32_t ___mSpacingX_70;
	// System.Int32 UILabel::mSpacingY
	int32_t ___mSpacingY_71;
	// System.Boolean UILabel::mUseFloatSpacing
	bool ___mUseFloatSpacing_72;
	// System.Single UILabel::mFloatSpacingX
	float ___mFloatSpacingX_73;
	// System.Single UILabel::mFloatSpacingY
	float ___mFloatSpacingY_74;
	// System.Boolean UILabel::mOverflowEllipsis
	bool ___mOverflowEllipsis_75;
	// System.Int32 UILabel::mOverflowWidth
	int32_t ___mOverflowWidth_76;
	// System.Int32 UILabel::mOverflowHeight
	int32_t ___mOverflowHeight_77;
	// UILabel/Modifier UILabel::mModifier
	int32_t ___mModifier_78;
	// System.Boolean UILabel::mShrinkToFit
	bool ___mShrinkToFit_79;
	// System.Int32 UILabel::mMaxLineWidth
	int32_t ___mMaxLineWidth_80;
	// System.Int32 UILabel::mMaxLineHeight
	int32_t ___mMaxLineHeight_81;
	// System.Single UILabel::mLineWidth
	float ___mLineWidth_82;
	// System.Boolean UILabel::mMultiline
	bool ___mMultiline_83;
	// UnityEngine.Font UILabel::mActiveTTF
	Font_t1956802104 * ___mActiveTTF_84;
	// System.Single UILabel::mDensity
	float ___mDensity_85;
	// System.Boolean UILabel::mShouldBeProcessed
	bool ___mShouldBeProcessed_86;
	// System.String UILabel::mProcessedText
	String_t* ___mProcessedText_87;
	// System.Boolean UILabel::mPremultiply
	bool ___mPremultiply_88;
	// UnityEngine.Vector2 UILabel::mCalculatedSize
	Vector2_t2156229523  ___mCalculatedSize_89;
	// System.Single UILabel::mScale
	float ___mScale_90;
	// System.Int32 UILabel::mFinalFontSize
	int32_t ___mFinalFontSize_91;
	// System.Int32 UILabel::mLastWidth
	int32_t ___mLastWidth_92;
	// System.Int32 UILabel::mLastHeight
	int32_t ___mLastHeight_93;
	// UILabel/ModifierFunc UILabel::customModifier
	ModifierFunc_t2998065802 * ___customModifier_94;

public:
	inline static int32_t get_offset_of_keepCrispWhenShrunk_53() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___keepCrispWhenShrunk_53)); }
	inline int32_t get_keepCrispWhenShrunk_53() const { return ___keepCrispWhenShrunk_53; }
	inline int32_t* get_address_of_keepCrispWhenShrunk_53() { return &___keepCrispWhenShrunk_53; }
	inline void set_keepCrispWhenShrunk_53(int32_t value)
	{
		___keepCrispWhenShrunk_53 = value;
	}

	inline static int32_t get_offset_of_mTrueTypeFont_54() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mTrueTypeFont_54)); }
	inline Font_t1956802104 * get_mTrueTypeFont_54() const { return ___mTrueTypeFont_54; }
	inline Font_t1956802104 ** get_address_of_mTrueTypeFont_54() { return &___mTrueTypeFont_54; }
	inline void set_mTrueTypeFont_54(Font_t1956802104 * value)
	{
		___mTrueTypeFont_54 = value;
		Il2CppCodeGenWriteBarrier((&___mTrueTypeFont_54), value);
	}

	inline static int32_t get_offset_of_mFont_55() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFont_55)); }
	inline UIFont_t2766063701 * get_mFont_55() const { return ___mFont_55; }
	inline UIFont_t2766063701 ** get_address_of_mFont_55() { return &___mFont_55; }
	inline void set_mFont_55(UIFont_t2766063701 * value)
	{
		___mFont_55 = value;
		Il2CppCodeGenWriteBarrier((&___mFont_55), value);
	}

	inline static int32_t get_offset_of_mText_56() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mText_56)); }
	inline String_t* get_mText_56() const { return ___mText_56; }
	inline String_t** get_address_of_mText_56() { return &___mText_56; }
	inline void set_mText_56(String_t* value)
	{
		___mText_56 = value;
		Il2CppCodeGenWriteBarrier((&___mText_56), value);
	}

	inline static int32_t get_offset_of_mFontSize_57() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFontSize_57)); }
	inline int32_t get_mFontSize_57() const { return ___mFontSize_57; }
	inline int32_t* get_address_of_mFontSize_57() { return &___mFontSize_57; }
	inline void set_mFontSize_57(int32_t value)
	{
		___mFontSize_57 = value;
	}

	inline static int32_t get_offset_of_mFontStyle_58() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFontStyle_58)); }
	inline int32_t get_mFontStyle_58() const { return ___mFontStyle_58; }
	inline int32_t* get_address_of_mFontStyle_58() { return &___mFontStyle_58; }
	inline void set_mFontStyle_58(int32_t value)
	{
		___mFontStyle_58 = value;
	}

	inline static int32_t get_offset_of_mAlignment_59() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mAlignment_59)); }
	inline int32_t get_mAlignment_59() const { return ___mAlignment_59; }
	inline int32_t* get_address_of_mAlignment_59() { return &___mAlignment_59; }
	inline void set_mAlignment_59(int32_t value)
	{
		___mAlignment_59 = value;
	}

	inline static int32_t get_offset_of_mEncoding_60() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEncoding_60)); }
	inline bool get_mEncoding_60() const { return ___mEncoding_60; }
	inline bool* get_address_of_mEncoding_60() { return &___mEncoding_60; }
	inline void set_mEncoding_60(bool value)
	{
		___mEncoding_60 = value;
	}

	inline static int32_t get_offset_of_mMaxLineCount_61() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMaxLineCount_61)); }
	inline int32_t get_mMaxLineCount_61() const { return ___mMaxLineCount_61; }
	inline int32_t* get_address_of_mMaxLineCount_61() { return &___mMaxLineCount_61; }
	inline void set_mMaxLineCount_61(int32_t value)
	{
		___mMaxLineCount_61 = value;
	}

	inline static int32_t get_offset_of_mEffectStyle_62() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEffectStyle_62)); }
	inline int32_t get_mEffectStyle_62() const { return ___mEffectStyle_62; }
	inline int32_t* get_address_of_mEffectStyle_62() { return &___mEffectStyle_62; }
	inline void set_mEffectStyle_62(int32_t value)
	{
		___mEffectStyle_62 = value;
	}

	inline static int32_t get_offset_of_mEffectColor_63() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEffectColor_63)); }
	inline Color_t2555686324  get_mEffectColor_63() const { return ___mEffectColor_63; }
	inline Color_t2555686324 * get_address_of_mEffectColor_63() { return &___mEffectColor_63; }
	inline void set_mEffectColor_63(Color_t2555686324  value)
	{
		___mEffectColor_63 = value;
	}

	inline static int32_t get_offset_of_mSymbols_64() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mSymbols_64)); }
	inline int32_t get_mSymbols_64() const { return ___mSymbols_64; }
	inline int32_t* get_address_of_mSymbols_64() { return &___mSymbols_64; }
	inline void set_mSymbols_64(int32_t value)
	{
		___mSymbols_64 = value;
	}

	inline static int32_t get_offset_of_mEffectDistance_65() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mEffectDistance_65)); }
	inline Vector2_t2156229523  get_mEffectDistance_65() const { return ___mEffectDistance_65; }
	inline Vector2_t2156229523 * get_address_of_mEffectDistance_65() { return &___mEffectDistance_65; }
	inline void set_mEffectDistance_65(Vector2_t2156229523  value)
	{
		___mEffectDistance_65 = value;
	}

	inline static int32_t get_offset_of_mOverflow_66() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflow_66)); }
	inline int32_t get_mOverflow_66() const { return ___mOverflow_66; }
	inline int32_t* get_address_of_mOverflow_66() { return &___mOverflow_66; }
	inline void set_mOverflow_66(int32_t value)
	{
		___mOverflow_66 = value;
	}

	inline static int32_t get_offset_of_mApplyGradient_67() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mApplyGradient_67)); }
	inline bool get_mApplyGradient_67() const { return ___mApplyGradient_67; }
	inline bool* get_address_of_mApplyGradient_67() { return &___mApplyGradient_67; }
	inline void set_mApplyGradient_67(bool value)
	{
		___mApplyGradient_67 = value;
	}

	inline static int32_t get_offset_of_mGradientTop_68() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mGradientTop_68)); }
	inline Color_t2555686324  get_mGradientTop_68() const { return ___mGradientTop_68; }
	inline Color_t2555686324 * get_address_of_mGradientTop_68() { return &___mGradientTop_68; }
	inline void set_mGradientTop_68(Color_t2555686324  value)
	{
		___mGradientTop_68 = value;
	}

	inline static int32_t get_offset_of_mGradientBottom_69() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mGradientBottom_69)); }
	inline Color_t2555686324  get_mGradientBottom_69() const { return ___mGradientBottom_69; }
	inline Color_t2555686324 * get_address_of_mGradientBottom_69() { return &___mGradientBottom_69; }
	inline void set_mGradientBottom_69(Color_t2555686324  value)
	{
		___mGradientBottom_69 = value;
	}

	inline static int32_t get_offset_of_mSpacingX_70() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mSpacingX_70)); }
	inline int32_t get_mSpacingX_70() const { return ___mSpacingX_70; }
	inline int32_t* get_address_of_mSpacingX_70() { return &___mSpacingX_70; }
	inline void set_mSpacingX_70(int32_t value)
	{
		___mSpacingX_70 = value;
	}

	inline static int32_t get_offset_of_mSpacingY_71() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mSpacingY_71)); }
	inline int32_t get_mSpacingY_71() const { return ___mSpacingY_71; }
	inline int32_t* get_address_of_mSpacingY_71() { return &___mSpacingY_71; }
	inline void set_mSpacingY_71(int32_t value)
	{
		___mSpacingY_71 = value;
	}

	inline static int32_t get_offset_of_mUseFloatSpacing_72() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mUseFloatSpacing_72)); }
	inline bool get_mUseFloatSpacing_72() const { return ___mUseFloatSpacing_72; }
	inline bool* get_address_of_mUseFloatSpacing_72() { return &___mUseFloatSpacing_72; }
	inline void set_mUseFloatSpacing_72(bool value)
	{
		___mUseFloatSpacing_72 = value;
	}

	inline static int32_t get_offset_of_mFloatSpacingX_73() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFloatSpacingX_73)); }
	inline float get_mFloatSpacingX_73() const { return ___mFloatSpacingX_73; }
	inline float* get_address_of_mFloatSpacingX_73() { return &___mFloatSpacingX_73; }
	inline void set_mFloatSpacingX_73(float value)
	{
		___mFloatSpacingX_73 = value;
	}

	inline static int32_t get_offset_of_mFloatSpacingY_74() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFloatSpacingY_74)); }
	inline float get_mFloatSpacingY_74() const { return ___mFloatSpacingY_74; }
	inline float* get_address_of_mFloatSpacingY_74() { return &___mFloatSpacingY_74; }
	inline void set_mFloatSpacingY_74(float value)
	{
		___mFloatSpacingY_74 = value;
	}

	inline static int32_t get_offset_of_mOverflowEllipsis_75() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflowEllipsis_75)); }
	inline bool get_mOverflowEllipsis_75() const { return ___mOverflowEllipsis_75; }
	inline bool* get_address_of_mOverflowEllipsis_75() { return &___mOverflowEllipsis_75; }
	inline void set_mOverflowEllipsis_75(bool value)
	{
		___mOverflowEllipsis_75 = value;
	}

	inline static int32_t get_offset_of_mOverflowWidth_76() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflowWidth_76)); }
	inline int32_t get_mOverflowWidth_76() const { return ___mOverflowWidth_76; }
	inline int32_t* get_address_of_mOverflowWidth_76() { return &___mOverflowWidth_76; }
	inline void set_mOverflowWidth_76(int32_t value)
	{
		___mOverflowWidth_76 = value;
	}

	inline static int32_t get_offset_of_mOverflowHeight_77() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mOverflowHeight_77)); }
	inline int32_t get_mOverflowHeight_77() const { return ___mOverflowHeight_77; }
	inline int32_t* get_address_of_mOverflowHeight_77() { return &___mOverflowHeight_77; }
	inline void set_mOverflowHeight_77(int32_t value)
	{
		___mOverflowHeight_77 = value;
	}

	inline static int32_t get_offset_of_mModifier_78() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mModifier_78)); }
	inline int32_t get_mModifier_78() const { return ___mModifier_78; }
	inline int32_t* get_address_of_mModifier_78() { return &___mModifier_78; }
	inline void set_mModifier_78(int32_t value)
	{
		___mModifier_78 = value;
	}

	inline static int32_t get_offset_of_mShrinkToFit_79() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mShrinkToFit_79)); }
	inline bool get_mShrinkToFit_79() const { return ___mShrinkToFit_79; }
	inline bool* get_address_of_mShrinkToFit_79() { return &___mShrinkToFit_79; }
	inline void set_mShrinkToFit_79(bool value)
	{
		___mShrinkToFit_79 = value;
	}

	inline static int32_t get_offset_of_mMaxLineWidth_80() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMaxLineWidth_80)); }
	inline int32_t get_mMaxLineWidth_80() const { return ___mMaxLineWidth_80; }
	inline int32_t* get_address_of_mMaxLineWidth_80() { return &___mMaxLineWidth_80; }
	inline void set_mMaxLineWidth_80(int32_t value)
	{
		___mMaxLineWidth_80 = value;
	}

	inline static int32_t get_offset_of_mMaxLineHeight_81() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMaxLineHeight_81)); }
	inline int32_t get_mMaxLineHeight_81() const { return ___mMaxLineHeight_81; }
	inline int32_t* get_address_of_mMaxLineHeight_81() { return &___mMaxLineHeight_81; }
	inline void set_mMaxLineHeight_81(int32_t value)
	{
		___mMaxLineHeight_81 = value;
	}

	inline static int32_t get_offset_of_mLineWidth_82() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mLineWidth_82)); }
	inline float get_mLineWidth_82() const { return ___mLineWidth_82; }
	inline float* get_address_of_mLineWidth_82() { return &___mLineWidth_82; }
	inline void set_mLineWidth_82(float value)
	{
		___mLineWidth_82 = value;
	}

	inline static int32_t get_offset_of_mMultiline_83() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mMultiline_83)); }
	inline bool get_mMultiline_83() const { return ___mMultiline_83; }
	inline bool* get_address_of_mMultiline_83() { return &___mMultiline_83; }
	inline void set_mMultiline_83(bool value)
	{
		___mMultiline_83 = value;
	}

	inline static int32_t get_offset_of_mActiveTTF_84() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mActiveTTF_84)); }
	inline Font_t1956802104 * get_mActiveTTF_84() const { return ___mActiveTTF_84; }
	inline Font_t1956802104 ** get_address_of_mActiveTTF_84() { return &___mActiveTTF_84; }
	inline void set_mActiveTTF_84(Font_t1956802104 * value)
	{
		___mActiveTTF_84 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveTTF_84), value);
	}

	inline static int32_t get_offset_of_mDensity_85() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mDensity_85)); }
	inline float get_mDensity_85() const { return ___mDensity_85; }
	inline float* get_address_of_mDensity_85() { return &___mDensity_85; }
	inline void set_mDensity_85(float value)
	{
		___mDensity_85 = value;
	}

	inline static int32_t get_offset_of_mShouldBeProcessed_86() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mShouldBeProcessed_86)); }
	inline bool get_mShouldBeProcessed_86() const { return ___mShouldBeProcessed_86; }
	inline bool* get_address_of_mShouldBeProcessed_86() { return &___mShouldBeProcessed_86; }
	inline void set_mShouldBeProcessed_86(bool value)
	{
		___mShouldBeProcessed_86 = value;
	}

	inline static int32_t get_offset_of_mProcessedText_87() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mProcessedText_87)); }
	inline String_t* get_mProcessedText_87() const { return ___mProcessedText_87; }
	inline String_t** get_address_of_mProcessedText_87() { return &___mProcessedText_87; }
	inline void set_mProcessedText_87(String_t* value)
	{
		___mProcessedText_87 = value;
		Il2CppCodeGenWriteBarrier((&___mProcessedText_87), value);
	}

	inline static int32_t get_offset_of_mPremultiply_88() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mPremultiply_88)); }
	inline bool get_mPremultiply_88() const { return ___mPremultiply_88; }
	inline bool* get_address_of_mPremultiply_88() { return &___mPremultiply_88; }
	inline void set_mPremultiply_88(bool value)
	{
		___mPremultiply_88 = value;
	}

	inline static int32_t get_offset_of_mCalculatedSize_89() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mCalculatedSize_89)); }
	inline Vector2_t2156229523  get_mCalculatedSize_89() const { return ___mCalculatedSize_89; }
	inline Vector2_t2156229523 * get_address_of_mCalculatedSize_89() { return &___mCalculatedSize_89; }
	inline void set_mCalculatedSize_89(Vector2_t2156229523  value)
	{
		___mCalculatedSize_89 = value;
	}

	inline static int32_t get_offset_of_mScale_90() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mScale_90)); }
	inline float get_mScale_90() const { return ___mScale_90; }
	inline float* get_address_of_mScale_90() { return &___mScale_90; }
	inline void set_mScale_90(float value)
	{
		___mScale_90 = value;
	}

	inline static int32_t get_offset_of_mFinalFontSize_91() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mFinalFontSize_91)); }
	inline int32_t get_mFinalFontSize_91() const { return ___mFinalFontSize_91; }
	inline int32_t* get_address_of_mFinalFontSize_91() { return &___mFinalFontSize_91; }
	inline void set_mFinalFontSize_91(int32_t value)
	{
		___mFinalFontSize_91 = value;
	}

	inline static int32_t get_offset_of_mLastWidth_92() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mLastWidth_92)); }
	inline int32_t get_mLastWidth_92() const { return ___mLastWidth_92; }
	inline int32_t* get_address_of_mLastWidth_92() { return &___mLastWidth_92; }
	inline void set_mLastWidth_92(int32_t value)
	{
		___mLastWidth_92 = value;
	}

	inline static int32_t get_offset_of_mLastHeight_93() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___mLastHeight_93)); }
	inline int32_t get_mLastHeight_93() const { return ___mLastHeight_93; }
	inline int32_t* get_address_of_mLastHeight_93() { return &___mLastHeight_93; }
	inline void set_mLastHeight_93(int32_t value)
	{
		___mLastHeight_93 = value;
	}

	inline static int32_t get_offset_of_customModifier_94() { return static_cast<int32_t>(offsetof(UILabel_t3248798549, ___customModifier_94)); }
	inline ModifierFunc_t2998065802 * get_customModifier_94() const { return ___customModifier_94; }
	inline ModifierFunc_t2998065802 ** get_address_of_customModifier_94() { return &___customModifier_94; }
	inline void set_customModifier_94(ModifierFunc_t2998065802 * value)
	{
		___customModifier_94 = value;
		Il2CppCodeGenWriteBarrier((&___customModifier_94), value);
	}
};

struct UILabel_t3248798549_StaticFields
{
public:
	// BetterList`1<UILabel> UILabel::mList
	BetterList_1_t2403818867 * ___mList_95;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Int32> UILabel::mFontUsage
	Dictionary_2_t2853457297 * ___mFontUsage_96;
	// BetterList`1<UIDrawCall> UILabel::mTempDrawcalls
	BetterList_1_t448425637 * ___mTempDrawcalls_97;
	// System.Boolean UILabel::mTexRebuildAdded
	bool ___mTexRebuildAdded_98;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UILabel::mTempVerts
	List_1_t899420910 * ___mTempVerts_99;
	// System.Collections.Generic.List`1<System.Int32> UILabel::mTempIndices
	List_1_t128053199 * ___mTempIndices_100;
	// System.Action`1<UnityEngine.Font> UILabel::<>f__mg$cache0
	Action_1_t2129269699 * ___U3CU3Ef__mgU24cache0_101;

public:
	inline static int32_t get_offset_of_mList_95() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mList_95)); }
	inline BetterList_1_t2403818867 * get_mList_95() const { return ___mList_95; }
	inline BetterList_1_t2403818867 ** get_address_of_mList_95() { return &___mList_95; }
	inline void set_mList_95(BetterList_1_t2403818867 * value)
	{
		___mList_95 = value;
		Il2CppCodeGenWriteBarrier((&___mList_95), value);
	}

	inline static int32_t get_offset_of_mFontUsage_96() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mFontUsage_96)); }
	inline Dictionary_2_t2853457297 * get_mFontUsage_96() const { return ___mFontUsage_96; }
	inline Dictionary_2_t2853457297 ** get_address_of_mFontUsage_96() { return &___mFontUsage_96; }
	inline void set_mFontUsage_96(Dictionary_2_t2853457297 * value)
	{
		___mFontUsage_96 = value;
		Il2CppCodeGenWriteBarrier((&___mFontUsage_96), value);
	}

	inline static int32_t get_offset_of_mTempDrawcalls_97() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTempDrawcalls_97)); }
	inline BetterList_1_t448425637 * get_mTempDrawcalls_97() const { return ___mTempDrawcalls_97; }
	inline BetterList_1_t448425637 ** get_address_of_mTempDrawcalls_97() { return &___mTempDrawcalls_97; }
	inline void set_mTempDrawcalls_97(BetterList_1_t448425637 * value)
	{
		___mTempDrawcalls_97 = value;
		Il2CppCodeGenWriteBarrier((&___mTempDrawcalls_97), value);
	}

	inline static int32_t get_offset_of_mTexRebuildAdded_98() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTexRebuildAdded_98)); }
	inline bool get_mTexRebuildAdded_98() const { return ___mTexRebuildAdded_98; }
	inline bool* get_address_of_mTexRebuildAdded_98() { return &___mTexRebuildAdded_98; }
	inline void set_mTexRebuildAdded_98(bool value)
	{
		___mTexRebuildAdded_98 = value;
	}

	inline static int32_t get_offset_of_mTempVerts_99() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTempVerts_99)); }
	inline List_1_t899420910 * get_mTempVerts_99() const { return ___mTempVerts_99; }
	inline List_1_t899420910 ** get_address_of_mTempVerts_99() { return &___mTempVerts_99; }
	inline void set_mTempVerts_99(List_1_t899420910 * value)
	{
		___mTempVerts_99 = value;
		Il2CppCodeGenWriteBarrier((&___mTempVerts_99), value);
	}

	inline static int32_t get_offset_of_mTempIndices_100() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___mTempIndices_100)); }
	inline List_1_t128053199 * get_mTempIndices_100() const { return ___mTempIndices_100; }
	inline List_1_t128053199 ** get_address_of_mTempIndices_100() { return &___mTempIndices_100; }
	inline void set_mTempIndices_100(List_1_t128053199 * value)
	{
		___mTempIndices_100 = value;
		Il2CppCodeGenWriteBarrier((&___mTempIndices_100), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_101() { return static_cast<int32_t>(offsetof(UILabel_t3248798549_StaticFields, ___U3CU3Ef__mgU24cache0_101)); }
	inline Action_1_t2129269699 * get_U3CU3Ef__mgU24cache0_101() const { return ___U3CU3Ef__mgU24cache0_101; }
	inline Action_1_t2129269699 ** get_address_of_U3CU3Ef__mgU24cache0_101() { return &___U3CU3Ef__mgU24cache0_101; }
	inline void set_U3CU3Ef__mgU24cache0_101(Action_1_t2129269699 * value)
	{
		___U3CU3Ef__mgU24cache0_101 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_101), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILABEL_T3248798549_H
#ifndef UISPRITE_T194114938_H
#define UISPRITE_T194114938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISprite
struct  UISprite_t194114938  : public UIBasicSprite_t1521297657
{
public:
	// UIAtlas UISprite::mAtlas
	UIAtlas_t3195533529 * ___mAtlas_70;
	// System.String UISprite::mSpriteName
	String_t* ___mSpriteName_71;
	// System.Boolean UISprite::mFillCenter
	bool ___mFillCenter_72;
	// UISpriteData UISprite::mSprite
	UISpriteData_t900308526 * ___mSprite_73;
	// System.Boolean UISprite::mSpriteSet
	bool ___mSpriteSet_74;

public:
	inline static int32_t get_offset_of_mAtlas_70() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mAtlas_70)); }
	inline UIAtlas_t3195533529 * get_mAtlas_70() const { return ___mAtlas_70; }
	inline UIAtlas_t3195533529 ** get_address_of_mAtlas_70() { return &___mAtlas_70; }
	inline void set_mAtlas_70(UIAtlas_t3195533529 * value)
	{
		___mAtlas_70 = value;
		Il2CppCodeGenWriteBarrier((&___mAtlas_70), value);
	}

	inline static int32_t get_offset_of_mSpriteName_71() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mSpriteName_71)); }
	inline String_t* get_mSpriteName_71() const { return ___mSpriteName_71; }
	inline String_t** get_address_of_mSpriteName_71() { return &___mSpriteName_71; }
	inline void set_mSpriteName_71(String_t* value)
	{
		___mSpriteName_71 = value;
		Il2CppCodeGenWriteBarrier((&___mSpriteName_71), value);
	}

	inline static int32_t get_offset_of_mFillCenter_72() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mFillCenter_72)); }
	inline bool get_mFillCenter_72() const { return ___mFillCenter_72; }
	inline bool* get_address_of_mFillCenter_72() { return &___mFillCenter_72; }
	inline void set_mFillCenter_72(bool value)
	{
		___mFillCenter_72 = value;
	}

	inline static int32_t get_offset_of_mSprite_73() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mSprite_73)); }
	inline UISpriteData_t900308526 * get_mSprite_73() const { return ___mSprite_73; }
	inline UISpriteData_t900308526 ** get_address_of_mSprite_73() { return &___mSprite_73; }
	inline void set_mSprite_73(UISpriteData_t900308526 * value)
	{
		___mSprite_73 = value;
		Il2CppCodeGenWriteBarrier((&___mSprite_73), value);
	}

	inline static int32_t get_offset_of_mSpriteSet_74() { return static_cast<int32_t>(offsetof(UISprite_t194114938, ___mSpriteSet_74)); }
	inline bool get_mSpriteSet_74() const { return ___mSpriteSet_74; }
	inline bool* get_address_of_mSpriteSet_74() { return &___mSpriteSet_74; }
	inline void set_mSpriteSet_74(bool value)
	{
		___mSpriteSet_74 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISPRITE_T194114938_H
#ifndef UITEXTURE_T3471168817_H
#define UITEXTURE_T3471168817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITexture
struct  UITexture_t3471168817  : public UIBasicSprite_t1521297657
{
public:
	// UnityEngine.Rect UITexture::mRect
	Rect_t2360479859  ___mRect_70;
	// UnityEngine.Texture UITexture::mTexture
	Texture_t3661962703 * ___mTexture_71;
	// UnityEngine.Shader UITexture::mShader
	Shader_t4151988712 * ___mShader_72;
	// UnityEngine.Vector4 UITexture::mBorder
	Vector4_t3319028937  ___mBorder_73;
	// System.Boolean UITexture::mFixedAspect
	bool ___mFixedAspect_74;
	// System.Int32 UITexture::mPMA
	int32_t ___mPMA_75;

public:
	inline static int32_t get_offset_of_mRect_70() { return static_cast<int32_t>(offsetof(UITexture_t3471168817, ___mRect_70)); }
	inline Rect_t2360479859  get_mRect_70() const { return ___mRect_70; }
	inline Rect_t2360479859 * get_address_of_mRect_70() { return &___mRect_70; }
	inline void set_mRect_70(Rect_t2360479859  value)
	{
		___mRect_70 = value;
	}

	inline static int32_t get_offset_of_mTexture_71() { return static_cast<int32_t>(offsetof(UITexture_t3471168817, ___mTexture_71)); }
	inline Texture_t3661962703 * get_mTexture_71() const { return ___mTexture_71; }
	inline Texture_t3661962703 ** get_address_of_mTexture_71() { return &___mTexture_71; }
	inline void set_mTexture_71(Texture_t3661962703 * value)
	{
		___mTexture_71 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_71), value);
	}

	inline static int32_t get_offset_of_mShader_72() { return static_cast<int32_t>(offsetof(UITexture_t3471168817, ___mShader_72)); }
	inline Shader_t4151988712 * get_mShader_72() const { return ___mShader_72; }
	inline Shader_t4151988712 ** get_address_of_mShader_72() { return &___mShader_72; }
	inline void set_mShader_72(Shader_t4151988712 * value)
	{
		___mShader_72 = value;
		Il2CppCodeGenWriteBarrier((&___mShader_72), value);
	}

	inline static int32_t get_offset_of_mBorder_73() { return static_cast<int32_t>(offsetof(UITexture_t3471168817, ___mBorder_73)); }
	inline Vector4_t3319028937  get_mBorder_73() const { return ___mBorder_73; }
	inline Vector4_t3319028937 * get_address_of_mBorder_73() { return &___mBorder_73; }
	inline void set_mBorder_73(Vector4_t3319028937  value)
	{
		___mBorder_73 = value;
	}

	inline static int32_t get_offset_of_mFixedAspect_74() { return static_cast<int32_t>(offsetof(UITexture_t3471168817, ___mFixedAspect_74)); }
	inline bool get_mFixedAspect_74() const { return ___mFixedAspect_74; }
	inline bool* get_address_of_mFixedAspect_74() { return &___mFixedAspect_74; }
	inline void set_mFixedAspect_74(bool value)
	{
		___mFixedAspect_74 = value;
	}

	inline static int32_t get_offset_of_mPMA_75() { return static_cast<int32_t>(offsetof(UITexture_t3471168817, ___mPMA_75)); }
	inline int32_t get_mPMA_75() const { return ___mPMA_75; }
	inline int32_t* get_address_of_mPMA_75() { return &___mPMA_75; }
	inline void set_mPMA_75(int32_t value)
	{
		___mPMA_75 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITEXTURE_T3471168817_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (GetTouchDelegate_t4218246285), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (RemoveTouchDelegate_t2508278027), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (OnScreenResize_t2279991692), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (ProcessEventsIn_t702674362)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2603[3] = 
{
	ProcessEventsIn_t702674362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (OnCustomInput_t3508588789), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (OnSchemeChange_t1701155603), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (MoveDelegate_t16019400), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (VoidDelegate_t3100799918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (BoolDelegate_t3825226153), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (FloatDelegate_t906524069), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (VectorDelegate_t435795517), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (ObjectDelegate_t2041570719), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (KeyCodeDelegate_t3064672302), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (DepthEntry_t628749918)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[4] = 
{
	DepthEntry_t628749918::get_offset_of_depth_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DepthEntry_t628749918::get_offset_of_hit_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DepthEntry_t628749918::get_offset_of_point_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DepthEntry_t628749918::get_offset_of_go_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (Touch_t3749196807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[4] = 
{
	Touch_t3749196807::get_offset_of_fingerId_0(),
	Touch_t3749196807::get_offset_of_phase_1(),
	Touch_t3749196807::get_offset_of_position_2(),
	Touch_t3749196807::get_offset_of_tapCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (GetTouchCountCallback_t3185863032), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (GetTouchCallback_t97678626), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (UIColorPicker_t463494602), -1, sizeof(UIColorPicker_t463494602_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2617[14] = 
{
	UIColorPicker_t463494602_StaticFields::get_offset_of_current_2(),
	UIColorPicker_t463494602::get_offset_of_value_3(),
	UIColorPicker_t463494602::get_offset_of_selectionWidget_4(),
	UIColorPicker_t463494602::get_offset_of_onChange_5(),
	UIColorPicker_t463494602::get_offset_of_mTrans_6(),
	UIColorPicker_t463494602::get_offset_of_mUITex_7(),
	UIColorPicker_t463494602::get_offset_of_mTex_8(),
	UIColorPicker_t463494602::get_offset_of_mCam_9(),
	UIColorPicker_t463494602::get_offset_of_mPos_10(),
	UIColorPicker_t463494602::get_offset_of_mWidth_11(),
	UIColorPicker_t463494602::get_offset_of_mHeight_12(),
	UIColorPicker_t463494602_StaticFields::get_offset_of_mRed_13(),
	UIColorPicker_t463494602_StaticFields::get_offset_of_mGreen_14(),
	UIColorPicker_t463494602_StaticFields::get_offset_of_mBlue_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (UIFont_t2766063701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[12] = 
{
	UIFont_t2766063701::get_offset_of_mMat_2(),
	UIFont_t2766063701::get_offset_of_mUVRect_3(),
	UIFont_t2766063701::get_offset_of_mFont_4(),
	UIFont_t2766063701::get_offset_of_mAtlas_5(),
	UIFont_t2766063701::get_offset_of_mReplacement_6(),
	UIFont_t2766063701::get_offset_of_mSymbols_7(),
	UIFont_t2766063701::get_offset_of_mDynamicFont_8(),
	UIFont_t2766063701::get_offset_of_mDynamicFontSize_9(),
	UIFont_t2766063701::get_offset_of_mDynamicFontStyle_10(),
	UIFont_t2766063701::get_offset_of_mSprite_11(),
	UIFont_t2766063701::get_offset_of_mPMA_12(),
	UIFont_t2766063701::get_offset_of_mPacked_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (UIInput_t421821618), -1, sizeof(UIInput_t421821618_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[46] = 
{
	UIInput_t421821618_StaticFields::get_offset_of_current_2(),
	UIInput_t421821618_StaticFields::get_offset_of_selection_3(),
	UIInput_t421821618::get_offset_of_label_4(),
	UIInput_t421821618::get_offset_of_inputType_5(),
	UIInput_t421821618::get_offset_of_onReturnKey_6(),
	UIInput_t421821618::get_offset_of_keyboardType_7(),
	UIInput_t421821618::get_offset_of_hideInput_8(),
	UIInput_t421821618::get_offset_of_selectAllTextOnFocus_9(),
	UIInput_t421821618::get_offset_of_submitOnUnselect_10(),
	UIInput_t421821618::get_offset_of_validation_11(),
	UIInput_t421821618::get_offset_of_characterLimit_12(),
	UIInput_t421821618::get_offset_of_savedAs_13(),
	UIInput_t421821618::get_offset_of_selectOnTab_14(),
	UIInput_t421821618::get_offset_of_activeTextColor_15(),
	UIInput_t421821618::get_offset_of_caretColor_16(),
	UIInput_t421821618::get_offset_of_selectionColor_17(),
	UIInput_t421821618::get_offset_of_onSubmit_18(),
	UIInput_t421821618::get_offset_of_onChange_19(),
	UIInput_t421821618::get_offset_of_onValidate_20(),
	UIInput_t421821618::get_offset_of_mValue_21(),
	UIInput_t421821618::get_offset_of_mDefaultText_22(),
	UIInput_t421821618::get_offset_of_mDefaultColor_23(),
	UIInput_t421821618::get_offset_of_mPosition_24(),
	UIInput_t421821618::get_offset_of_mDoInit_25(),
	UIInput_t421821618::get_offset_of_mAlignment_26(),
	UIInput_t421821618::get_offset_of_mLoadSavedValue_27(),
	UIInput_t421821618_StaticFields::get_offset_of_mDrawStart_28(),
	UIInput_t421821618_StaticFields::get_offset_of_mLastIME_29(),
	UIInput_t421821618_StaticFields::get_offset_of_mKeyboard_30(),
	UIInput_t421821618_StaticFields::get_offset_of_mWaitForKeyboard_31(),
	UIInput_t421821618::get_offset_of_mSelectionStart_32(),
	UIInput_t421821618::get_offset_of_mSelectionEnd_33(),
	UIInput_t421821618::get_offset_of_mHighlight_34(),
	UIInput_t421821618::get_offset_of_mCaret_35(),
	UIInput_t421821618::get_offset_of_mBlankTex_36(),
	UIInput_t421821618::get_offset_of_mNextBlink_37(),
	UIInput_t421821618::get_offset_of_mLastAlpha_38(),
	UIInput_t421821618::get_offset_of_mCached_39(),
	UIInput_t421821618::get_offset_of_mSelectMe_40(),
	UIInput_t421821618::get_offset_of_mSelectTime_41(),
	UIInput_t421821618::get_offset_of_mStarted_42(),
	UIInput_t421821618::get_offset_of_mCam_43(),
	UIInput_t421821618::get_offset_of_mEllipsis_44(),
	UIInput_t421821618_StaticFields::get_offset_of_mIgnoreKey_45(),
	UIInput_t421821618::get_offset_of_onUpArrow_46(),
	UIInput_t421821618::get_offset_of_onDownArrow_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (InputType_t3084801673)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2620[4] = 
{
	InputType_t3084801673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (Validation_t3618213463)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2621[8] = 
{
	Validation_t3618213463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (KeyboardType_t3374647619)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2622[9] = 
{
	KeyboardType_t3374647619::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (OnReturnKey_t3803455137)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2623[4] = 
{
	OnReturnKey_t3803455137::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (OnValidate_t1246632601), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (UIInputOnGUI_t4239979770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (UILabel_t3248798549), -1, sizeof(UILabel_t3248798549_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2626[49] = 
{
	UILabel_t3248798549::get_offset_of_keepCrispWhenShrunk_53(),
	UILabel_t3248798549::get_offset_of_mTrueTypeFont_54(),
	UILabel_t3248798549::get_offset_of_mFont_55(),
	UILabel_t3248798549::get_offset_of_mText_56(),
	UILabel_t3248798549::get_offset_of_mFontSize_57(),
	UILabel_t3248798549::get_offset_of_mFontStyle_58(),
	UILabel_t3248798549::get_offset_of_mAlignment_59(),
	UILabel_t3248798549::get_offset_of_mEncoding_60(),
	UILabel_t3248798549::get_offset_of_mMaxLineCount_61(),
	UILabel_t3248798549::get_offset_of_mEffectStyle_62(),
	UILabel_t3248798549::get_offset_of_mEffectColor_63(),
	UILabel_t3248798549::get_offset_of_mSymbols_64(),
	UILabel_t3248798549::get_offset_of_mEffectDistance_65(),
	UILabel_t3248798549::get_offset_of_mOverflow_66(),
	UILabel_t3248798549::get_offset_of_mApplyGradient_67(),
	UILabel_t3248798549::get_offset_of_mGradientTop_68(),
	UILabel_t3248798549::get_offset_of_mGradientBottom_69(),
	UILabel_t3248798549::get_offset_of_mSpacingX_70(),
	UILabel_t3248798549::get_offset_of_mSpacingY_71(),
	UILabel_t3248798549::get_offset_of_mUseFloatSpacing_72(),
	UILabel_t3248798549::get_offset_of_mFloatSpacingX_73(),
	UILabel_t3248798549::get_offset_of_mFloatSpacingY_74(),
	UILabel_t3248798549::get_offset_of_mOverflowEllipsis_75(),
	UILabel_t3248798549::get_offset_of_mOverflowWidth_76(),
	UILabel_t3248798549::get_offset_of_mOverflowHeight_77(),
	UILabel_t3248798549::get_offset_of_mModifier_78(),
	UILabel_t3248798549::get_offset_of_mShrinkToFit_79(),
	UILabel_t3248798549::get_offset_of_mMaxLineWidth_80(),
	UILabel_t3248798549::get_offset_of_mMaxLineHeight_81(),
	UILabel_t3248798549::get_offset_of_mLineWidth_82(),
	UILabel_t3248798549::get_offset_of_mMultiline_83(),
	UILabel_t3248798549::get_offset_of_mActiveTTF_84(),
	UILabel_t3248798549::get_offset_of_mDensity_85(),
	UILabel_t3248798549::get_offset_of_mShouldBeProcessed_86(),
	UILabel_t3248798549::get_offset_of_mProcessedText_87(),
	UILabel_t3248798549::get_offset_of_mPremultiply_88(),
	UILabel_t3248798549::get_offset_of_mCalculatedSize_89(),
	UILabel_t3248798549::get_offset_of_mScale_90(),
	UILabel_t3248798549::get_offset_of_mFinalFontSize_91(),
	UILabel_t3248798549::get_offset_of_mLastWidth_92(),
	UILabel_t3248798549::get_offset_of_mLastHeight_93(),
	UILabel_t3248798549::get_offset_of_customModifier_94(),
	UILabel_t3248798549_StaticFields::get_offset_of_mList_95(),
	UILabel_t3248798549_StaticFields::get_offset_of_mFontUsage_96(),
	UILabel_t3248798549_StaticFields::get_offset_of_mTempDrawcalls_97(),
	UILabel_t3248798549_StaticFields::get_offset_of_mTexRebuildAdded_98(),
	UILabel_t3248798549_StaticFields::get_offset_of_mTempVerts_99(),
	UILabel_t3248798549_StaticFields::get_offset_of_mTempIndices_100(),
	UILabel_t3248798549_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_101(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (Effect_t2533209744)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2627[5] = 
{
	Effect_t2533209744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (Overflow_t884389639)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2628[5] = 
{
	Overflow_t884389639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (Crispness_t2029465680)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2629[4] = 
{
	Crispness_t2029465680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (Modifier_t3840764400)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2630[5] = 
{
	Modifier_t3840764400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (ModifierFunc_t2998065802), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (UILocalize_t3543745742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[2] = 
{
	UILocalize_t3543745742::get_offset_of_key_2(),
	UILocalize_t3543745742::get_offset_of_mStarted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (UIOrthoCamera_t1944225589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[2] = 
{
	UIOrthoCamera_t1944225589::get_offset_of_mCam_2(),
	UIOrthoCamera_t1944225589::get_offset_of_mTrans_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (UIPanel_t1716472341), -1, sizeof(UIPanel_t1716472341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2634[49] = 
{
	UIPanel_t1716472341_StaticFields::get_offset_of_list_22(),
	UIPanel_t1716472341::get_offset_of_onGeometryUpdated_23(),
	UIPanel_t1716472341::get_offset_of_showInPanelTool_24(),
	UIPanel_t1716472341::get_offset_of_generateNormals_25(),
	UIPanel_t1716472341::get_offset_of_generateUV2_26(),
	UIPanel_t1716472341::get_offset_of_shadowMode_27(),
	UIPanel_t1716472341::get_offset_of_widgetsAreStatic_28(),
	UIPanel_t1716472341::get_offset_of_cullWhileDragging_29(),
	UIPanel_t1716472341::get_offset_of_alwaysOnScreen_30(),
	UIPanel_t1716472341::get_offset_of_anchorOffset_31(),
	UIPanel_t1716472341::get_offset_of_softBorderPadding_32(),
	UIPanel_t1716472341::get_offset_of_renderQueue_33(),
	UIPanel_t1716472341::get_offset_of_startingRenderQueue_34(),
	UIPanel_t1716472341::get_offset_of_widgets_35(),
	UIPanel_t1716472341::get_offset_of_drawCalls_36(),
	UIPanel_t1716472341::get_offset_of_worldToLocal_37(),
	UIPanel_t1716472341::get_offset_of_drawCallClipRange_38(),
	UIPanel_t1716472341::get_offset_of_onClipMove_39(),
	UIPanel_t1716472341::get_offset_of_onCreateMaterial_40(),
	UIPanel_t1716472341::get_offset_of_onCreateDrawCall_41(),
	UIPanel_t1716472341::get_offset_of_mClipTexture_42(),
	UIPanel_t1716472341::get_offset_of_mAlpha_43(),
	UIPanel_t1716472341::get_offset_of_mClipping_44(),
	UIPanel_t1716472341::get_offset_of_mClipRange_45(),
	UIPanel_t1716472341::get_offset_of_mClipSoftness_46(),
	UIPanel_t1716472341::get_offset_of_mDepth_47(),
	UIPanel_t1716472341::get_offset_of_mSortingOrder_48(),
	UIPanel_t1716472341::get_offset_of_mSortingLayerName_49(),
	UIPanel_t1716472341::get_offset_of_mRebuild_50(),
	UIPanel_t1716472341::get_offset_of_mResized_51(),
	UIPanel_t1716472341::get_offset_of_mClipOffset_52(),
	UIPanel_t1716472341::get_offset_of_mMatrixFrame_53(),
	UIPanel_t1716472341::get_offset_of_mAlphaFrameID_54(),
	UIPanel_t1716472341::get_offset_of_mLayer_55(),
	UIPanel_t1716472341_StaticFields::get_offset_of_mTemp_56(),
	UIPanel_t1716472341::get_offset_of_mMin_57(),
	UIPanel_t1716472341::get_offset_of_mMax_58(),
	UIPanel_t1716472341::get_offset_of_mSortWidgets_59(),
	UIPanel_t1716472341::get_offset_of_mUpdateScroll_60(),
	UIPanel_t1716472341::get_offset_of_useSortingOrder_61(),
	UIPanel_t1716472341::get_offset_of_mParentPanel_62(),
	UIPanel_t1716472341_StaticFields::get_offset_of_mCorners_63(),
	UIPanel_t1716472341_StaticFields::get_offset_of_mUpdateFrame_64(),
	UIPanel_t1716472341::get_offset_of_mHasMoved_65(),
	UIPanel_t1716472341::get_offset_of_mOnRender_66(),
	UIPanel_t1716472341::get_offset_of_mForced_67(),
	UIPanel_t1716472341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_68(),
	UIPanel_t1716472341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_69(),
	UIPanel_t1716472341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_70(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (RenderQueue_t2721716586)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2635[4] = 
{
	RenderQueue_t2721716586::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (OnGeometryUpdated_t2462438111), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (OnClippingMoved_t476625095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (OnCreateMaterial_t2961246930), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (UIRoot_t4022971450), -1, sizeof(UIRoot_t4022971450_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2639[11] = 
{
	UIRoot_t4022971450_StaticFields::get_offset_of_list_2(),
	UIRoot_t4022971450::get_offset_of_scalingStyle_3(),
	UIRoot_t4022971450::get_offset_of_manualWidth_4(),
	UIRoot_t4022971450::get_offset_of_manualHeight_5(),
	UIRoot_t4022971450::get_offset_of_minimumHeight_6(),
	UIRoot_t4022971450::get_offset_of_maximumHeight_7(),
	UIRoot_t4022971450::get_offset_of_fitWidth_8(),
	UIRoot_t4022971450::get_offset_of_fitHeight_9(),
	UIRoot_t4022971450::get_offset_of_adjustByDPI_10(),
	UIRoot_t4022971450::get_offset_of_shrinkPortraitUI_11(),
	UIRoot_t4022971450::get_offset_of_mTrans_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (Scaling_t3068739704)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2640[4] = 
{
	Scaling_t3068739704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (Constraint_t400694485)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2641[5] = 
{
	Constraint_t400694485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (UISprite_t194114938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[5] = 
{
	UISprite_t194114938::get_offset_of_mAtlas_70(),
	UISprite_t194114938::get_offset_of_mSpriteName_71(),
	UISprite_t194114938::get_offset_of_mFillCenter_72(),
	UISprite_t194114938::get_offset_of_mSprite_73(),
	UISprite_t194114938::get_offset_of_mSpriteSet_74(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (UISpriteAnimation_t1118314077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[9] = 
{
	UISpriteAnimation_t1118314077::get_offset_of_frameIndex_2(),
	UISpriteAnimation_t1118314077::get_offset_of_mFPS_3(),
	UISpriteAnimation_t1118314077::get_offset_of_mPrefix_4(),
	UISpriteAnimation_t1118314077::get_offset_of_mLoop_5(),
	UISpriteAnimation_t1118314077::get_offset_of_mSnap_6(),
	UISpriteAnimation_t1118314077::get_offset_of_mSprite_7(),
	UISpriteAnimation_t1118314077::get_offset_of_mDelta_8(),
	UISpriteAnimation_t1118314077::get_offset_of_mActive_9(),
	UISpriteAnimation_t1118314077::get_offset_of_mSpriteNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (UISpriteData_t900308526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[13] = 
{
	UISpriteData_t900308526::get_offset_of_name_0(),
	UISpriteData_t900308526::get_offset_of_x_1(),
	UISpriteData_t900308526::get_offset_of_y_2(),
	UISpriteData_t900308526::get_offset_of_width_3(),
	UISpriteData_t900308526::get_offset_of_height_4(),
	UISpriteData_t900308526::get_offset_of_borderLeft_5(),
	UISpriteData_t900308526::get_offset_of_borderRight_6(),
	UISpriteData_t900308526::get_offset_of_borderTop_7(),
	UISpriteData_t900308526::get_offset_of_borderBottom_8(),
	UISpriteData_t900308526::get_offset_of_paddingLeft_9(),
	UISpriteData_t900308526::get_offset_of_paddingRight_10(),
	UISpriteData_t900308526::get_offset_of_paddingTop_11(),
	UISpriteData_t900308526::get_offset_of_paddingBottom_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (UIStretch_t3058335968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[16] = 
{
	UIStretch_t3058335968::get_offset_of_uiCamera_2(),
	UIStretch_t3058335968::get_offset_of_container_3(),
	UIStretch_t3058335968::get_offset_of_style_4(),
	UIStretch_t3058335968::get_offset_of_runOnlyOnce_5(),
	UIStretch_t3058335968::get_offset_of_relativeSize_6(),
	UIStretch_t3058335968::get_offset_of_initialSize_7(),
	UIStretch_t3058335968::get_offset_of_borderPadding_8(),
	UIStretch_t3058335968::get_offset_of_widgetContainer_9(),
	UIStretch_t3058335968::get_offset_of_mTrans_10(),
	UIStretch_t3058335968::get_offset_of_mWidget_11(),
	UIStretch_t3058335968::get_offset_of_mSprite_12(),
	UIStretch_t3058335968::get_offset_of_mPanel_13(),
	UIStretch_t3058335968::get_offset_of_mRoot_14(),
	UIStretch_t3058335968::get_offset_of_mAnim_15(),
	UIStretch_t3058335968::get_offset_of_mRect_16(),
	UIStretch_t3058335968::get_offset_of_mStarted_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (Style_t3184300279)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2646[8] = 
{
	Style_t3184300279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (UITextList_t2040849008), -1, sizeof(UITextList_t2040849008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2647[11] = 
{
	UITextList_t2040849008::get_offset_of_textLabel_2(),
	UITextList_t2040849008::get_offset_of_scrollBar_3(),
	UITextList_t2040849008::get_offset_of_style_4(),
	UITextList_t2040849008::get_offset_of_paragraphHistory_5(),
	UITextList_t2040849008::get_offset_of_mSeparator_6(),
	UITextList_t2040849008::get_offset_of_mScroll_7(),
	UITextList_t2040849008::get_offset_of_mTotalLines_8(),
	UITextList_t2040849008::get_offset_of_mLastWidth_9(),
	UITextList_t2040849008::get_offset_of_mLastHeight_10(),
	UITextList_t2040849008::get_offset_of_mParagraphs_11(),
	UITextList_t2040849008_StaticFields::get_offset_of_mHistory_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (Style_t3106380496)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2648[3] = 
{
	Style_t3106380496::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (Paragraph_t1111063719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[2] = 
{
	Paragraph_t1111063719::get_offset_of_text_0(),
	Paragraph_t1111063719::get_offset_of_lines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (UITexture_t3471168817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[6] = 
{
	UITexture_t3471168817::get_offset_of_mRect_70(),
	UITexture_t3471168817::get_offset_of_mTexture_71(),
	UITexture_t3471168817::get_offset_of_mShader_72(),
	UITexture_t3471168817::get_offset_of_mBorder_73(),
	UITexture_t3471168817::get_offset_of_mFixedAspect_74(),
	UITexture_t3471168817::get_offset_of_mPMA_75(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (UITooltip_t30236576), -1, sizeof(UITooltip_t30236576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[14] = 
{
	UITooltip_t30236576_StaticFields::get_offset_of_mInstance_2(),
	UITooltip_t30236576::get_offset_of_uiCamera_3(),
	UITooltip_t30236576::get_offset_of_text_4(),
	UITooltip_t30236576::get_offset_of_tooltipRoot_5(),
	UITooltip_t30236576::get_offset_of_background_6(),
	UITooltip_t30236576::get_offset_of_appearSpeed_7(),
	UITooltip_t30236576::get_offset_of_scalingTransitions_8(),
	UITooltip_t30236576::get_offset_of_mTooltip_9(),
	UITooltip_t30236576::get_offset_of_mTrans_10(),
	UITooltip_t30236576::get_offset_of_mTarget_11(),
	UITooltip_t30236576::get_offset_of_mCurrent_12(),
	UITooltip_t30236576::get_offset_of_mPos_13(),
	UITooltip_t30236576::get_offset_of_mSize_14(),
	UITooltip_t30236576::get_offset_of_mWidgets_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (UIViewport_t1918760134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[5] = 
{
	UIViewport_t1918760134::get_offset_of_sourceCamera_2(),
	UIViewport_t1918760134::get_offset_of_topLeft_3(),
	UIViewport_t1918760134::get_offset_of_bottomRight_4(),
	UIViewport_t1918760134::get_offset_of_fullSize_5(),
	UIViewport_t1918760134::get_offset_of_mCam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (Lane_t3796433026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[1] = 
{
	Lane_t3796433026::get_offset_of__material_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (LeanFingerSwipe_t3060022393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[5] = 
{
	LeanFingerSwipe_t3060022393::get_offset_of_IgnoreGuiFingers_2(),
	LeanFingerSwipe_t3060022393::get_offset_of_CheckAngle_3(),
	LeanFingerSwipe_t3060022393::get_offset_of_Angle_4(),
	LeanFingerSwipe_t3060022393::get_offset_of_AngleThreshold_5(),
	LeanFingerSwipe_t3060022393::get_offset_of_OnFingerSwipe_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (FingerEvent_t2652301007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (LeanFinger_t3506292858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[12] = 
{
	LeanFinger_t3506292858::get_offset_of_Index_0(),
	LeanFinger_t3506292858::get_offset_of_Age_1(),
	LeanFinger_t3506292858::get_offset_of_Set_2(),
	LeanFinger_t3506292858::get_offset_of_LastSet_3(),
	LeanFinger_t3506292858::get_offset_of_Tap_4(),
	LeanFinger_t3506292858::get_offset_of_TapCount_5(),
	LeanFinger_t3506292858::get_offset_of_Swipe_6(),
	LeanFinger_t3506292858::get_offset_of_StartScreenPosition_7(),
	LeanFinger_t3506292858::get_offset_of_LastScreenPosition_8(),
	LeanFinger_t3506292858::get_offset_of_ScreenPosition_9(),
	LeanFinger_t3506292858::get_offset_of_StartedOverGui_10(),
	LeanFinger_t3506292858::get_offset_of_Snapshots_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (LeanGesture_t1335127695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (LeanSelectable_t2178850769), -1, sizeof(LeanSelectable_t2178850769_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2658[7] = 
{
	LeanSelectable_t2178850769_StaticFields::get_offset_of_Instances_2(),
	LeanSelectable_t2178850769::get_offset_of_HideWithFinger_3(),
	LeanSelectable_t2178850769::get_offset_of_SelectingFinger_4(),
	LeanSelectable_t2178850769::get_offset_of_OnSelect_5(),
	LeanSelectable_t2178850769::get_offset_of_OnSelectUp_6(),
	LeanSelectable_t2178850769::get_offset_of_OnDeselect_7(),
	LeanSelectable_t2178850769::get_offset_of_isSelected_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (LeanFingerEvent_t821904700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (LeanSnapshot_t4136513957), -1, sizeof(LeanSnapshot_t4136513957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2660[3] = 
{
	LeanSnapshot_t4136513957::get_offset_of_Age_0(),
	LeanSnapshot_t4136513957::get_offset_of_ScreenPosition_1(),
	LeanSnapshot_t4136513957_StaticFields::get_offset_of_InactiveSnapshots_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (LeanTouch_t2951860335), -1, sizeof(LeanTouch_t2951860335_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2661[28] = 
{
	LeanTouch_t2951860335_StaticFields::get_offset_of_Instances_2(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_Fingers_3(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_InactiveFingers_4(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerDown_5(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerSet_6(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerUp_7(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerTap_8(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerSwipe_9(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnGesture_10(),
	LeanTouch_t2951860335::get_offset_of_TapThreshold_11(),
	0,
	LeanTouch_t2951860335::get_offset_of_SwipeThreshold_13(),
	0,
	LeanTouch_t2951860335::get_offset_of_ReferenceDpi_15(),
	0,
	LeanTouch_t2951860335::get_offset_of_GuiLayers_17(),
	LeanTouch_t2951860335::get_offset_of_RecordFingers_18(),
	LeanTouch_t2951860335::get_offset_of_RecordThreshold_19(),
	LeanTouch_t2951860335::get_offset_of_RecordLimit_20(),
	LeanTouch_t2951860335::get_offset_of_SimulateMultiFingers_21(),
	LeanTouch_t2951860335::get_offset_of_PinchTwistKey_22(),
	LeanTouch_t2951860335::get_offset_of_MultiDragKey_23(),
	LeanTouch_t2951860335::get_offset_of_FingerTexture_24(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_highestMouseButton_25(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_tempRaycastResults_26(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_filteredFingers_27(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_tempPointerEventData_28(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_tempEventSystem_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (LocalizeImage_t2397934916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[6] = 
{
	LocalizeImage_t2397934916::get_offset_of_NGUI_2(),
	LocalizeImage_t2397934916::get_offset_of_japanNameNGUI_3(),
	LocalizeImage_t2397934916::get_offset_of_japanName_4(),
	LocalizeImage_t2397934916::get_offset_of_setNativeSize_5(),
	LocalizeImage_t2397934916::get_offset_of_img_6(),
	LocalizeImage_t2397934916::get_offset_of_sprite_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (Mountain_t1989270499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[2] = 
{
	Mountain_t1989270499::get_offset_of__sprite_2(),
	Mountain_t1989270499::get_offset_of__name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (TweenFill_t1298028023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[4] = 
{
	TweenFill_t1298028023::get_offset_of_from_22(),
	TweenFill_t1298028023::get_offset_of_to_23(),
	TweenFill_t1298028023::get_offset_of_mCached_24(),
	TweenFill_t1298028023::get_offset_of_mSprite_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (TweenLetters_t3284132802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[7] = 
{
	TweenLetters_t3284132802::get_offset_of_hoverOver_22(),
	TweenLetters_t3284132802::get_offset_of_hoverOut_23(),
	TweenLetters_t3284132802::get_offset_of_mLabel_24(),
	TweenLetters_t3284132802::get_offset_of_mVertexCount_25(),
	TweenLetters_t3284132802::get_offset_of_mLetterOrder_26(),
	TweenLetters_t3284132802::get_offset_of_mLetter_27(),
	TweenLetters_t3284132802::get_offset_of_mCurrent_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (AnimationLetterOrder_t1458767740)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2666[4] = 
{
	AnimationLetterOrder_t1458767740::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (LetterProperties_t3768168197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[3] = 
{
	LetterProperties_t3768168197::get_offset_of_start_0(),
	LetterProperties_t3768168197::get_offset_of_duration_1(),
	LetterProperties_t3768168197::get_offset_of_offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (AnimationProperties_t1280364982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[9] = 
{
	AnimationProperties_t1280364982::get_offset_of_animationOrder_0(),
	AnimationProperties_t1280364982::get_offset_of_overlap_1(),
	AnimationProperties_t1280364982::get_offset_of_randomDurations_2(),
	AnimationProperties_t1280364982::get_offset_of_randomness_3(),
	AnimationProperties_t1280364982::get_offset_of_offsetRange_4(),
	AnimationProperties_t1280364982::get_offset_of_pos_5(),
	AnimationProperties_t1280364982::get_offset_of_rot_6(),
	AnimationProperties_t1280364982::get_offset_of_scale_7(),
	AnimationProperties_t1280364982::get_offset_of_alpha_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (NoAdsButtonAssistant_t697893148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (PauseButtonAssisstant_t3297824667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[2] = 
{
	PauseButtonAssisstant_t3297824667::get_offset_of__canvas_5(),
	PauseButtonAssisstant_t3297824667::get_offset_of_pauseStatus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (RankingLabel_t3462582456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[3] = 
{
	RankingLabel_t3462582456::get_offset_of_frame_2(),
	RankingLabel_t3462582456::get_offset_of_lbl_3(),
	RankingLabel_t3462582456::get_offset_of_lastRank_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (RestoreButtonAssistant_t450383048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (ResultController_t2106065388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[4] = 
{
	ResultController_t2106065388::get_offset_of__dialogRect_4(),
	ResultController_t2106065388::get_offset_of_closeButton_5(),
	ResultController_t2106065388::get_offset_of__blockRect_6(),
	ResultController_t2106065388::get_offset_of__showInterstitial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (U3CStartU3Ec__Iterator0_t3890121266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[4] = 
{
	U3CStartU3Ec__Iterator0_t3890121266::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3890121266::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3890121266::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3890121266::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (ResumeButtonAssisstant_t2799415769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[2] = 
{
	ResumeButtonAssisstant_t2799415769::get_offset_of__canvas_5(),
	ResumeButtonAssisstant_t2799415769::get_offset_of_pauseStatus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (NativePlugin_t1231223992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[2] = 
{
	0,
	NativePlugin_t1231223992::get_offset_of__nativeInstance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (MonoBehaviourWithInit_t1117120792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[1] = 
{
	MonoBehaviourWithInit_t1117120792::get_offset_of__isInitialized_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (AudioName_t948499878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (DefineValue_t901717641), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (LayerMaskNo_t2613727478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (LayerNo_t1223498628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (PlayerSettingsValue_t3578457295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (RecyclingObjectName_t2466862849), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (ResourcesDirectoryPath_t218739485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (ResourcesFilePath_t4283178319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[138] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (SceneName_t250522944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (SceneNo_t932058348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (SortingLayerName_t3039061313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (TagName_t432652465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (Define_t955668934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (DirectoryPath_t2513158077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (CacheData_t2005210238), -1, sizeof(CacheData_t2005210238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2693[5] = 
{
	CacheData_t2005210238_StaticFields::get_offset_of__entity_2(),
	CacheData_t2005210238::get_offset_of_IsClear_3(),
	CacheData_t2005210238::get_offset_of_IsNewRecored_4(),
	CacheData_t2005210238::get_offset_of_StageNo_5(),
	CacheData_t2005210238::get_offset_of_CurrentScore_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (ExcelData_t3659864980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[1] = 
{
	ExcelData_t3659864980::get_offset_of_RowDataList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (RowData_t2690997100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[1] = 
{
	RowData_t2690997100::get_offset_of_DataList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (GameType_t3504513020)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2696[4] = 
{
	GameType_t3504513020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (UnityProjectSetting_t2579824760), -1, sizeof(UnityProjectSetting_t2579824760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2697[16] = 
{
	0,
	UnityProjectSetting_t2579824760_StaticFields::get_offset_of__entity_3(),
	UnityProjectSetting_t2579824760::get_offset_of__gameType_4(),
	UnityProjectSetting_t2579824760::get_offset_of_StageNum_5(),
	UnityProjectSetting_t2579824760::get_offset_of__loadStageNo_6(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatInTitle_7(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatInClear_8(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatInMiss_9(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatDict_10(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatInTitleEnglish_11(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatInClearEnglish_12(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatInMissEnglish_13(),
	UnityProjectSetting_t2579824760::get_offset_of__shareFormatDictEnglish_14(),
	UnityProjectSetting_t2579824760::get_offset_of__tag_15(),
	UnityProjectSetting_t2579824760::get_offset_of__iosStoreURL_16(),
	UnityProjectSetting_t2579824760::get_offset_of__androidStoreURL_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (UserData_t2822117362), -1, sizeof(UserData_t2822117362_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2698[17] = 
{
	0,
	UserData_t2822117362_StaticFields::get_offset_of__isInitializedPlayCount_1(),
	UserData_t2822117362_StaticFields::get_offset_of__playCount_2(),
	0,
	UserData_t2822117362_StaticFields::get_offset_of__isInitializedHighScoreDict_4(),
	UserData_t2822117362_StaticFields::get_offset_of__highScoreDict_5(),
	0,
	UserData_t2822117362_StaticFields::get_offset_of__isInitializedMuteSetting_7(),
	UserData_t2822117362_StaticFields::get_offset_of__isMute_8(),
	UserData_t2822117362_StaticFields::get_offset_of_OnChangeMuteSetting_9(),
	0,
	UserData_t2822117362_StaticFields::get_offset_of__isInitFirstTimeOpenApp_11(),
	UserData_t2822117362_StaticFields::get_offset_of__isFirstTimeOpenApp_12(),
	UserData_t2822117362_StaticFields::get_offset_of_OnChangeFirstTime_13(),
	0,
	UserData_t2822117362_StaticFields::get_offset_of__isInitializedTimeOfPushedHouseAdButton_15(),
	UserData_t2822117362_StaticFields::get_offset_of__timeOfPushedHouseAdButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (AdRegionViewer_t142023310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[11] = 
{
	AdRegionViewer_t142023310::get_offset_of__color_8(),
	AdRegionViewer_t142023310::get_offset_of__bannerType_9(),
	AdRegionViewer_t142023310::get_offset_of__rectangleType_10(),
	AdRegionViewer_t142023310::get_offset_of__iconType_11(),
	AdRegionViewer_t142023310::get_offset_of__topBanner_12(),
	AdRegionViewer_t142023310::get_offset_of__topBigBanner_13(),
	AdRegionViewer_t142023310::get_offset_of__bottomBanner_14(),
	AdRegionViewer_t142023310::get_offset_of__topRectangle_15(),
	AdRegionViewer_t142023310::get_offset_of__bottomRectangle_16(),
	AdRegionViewer_t142023310::get_offset_of__titleIcon_17(),
	AdRegionViewer_t142023310::get_offset_of__helpIcon_18(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
