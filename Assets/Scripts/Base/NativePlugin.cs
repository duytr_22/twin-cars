﻿//  NativePlugin.cs
//  ProductName ChibiCrashers
//
//  Created by kan kikuchi on 2015.09.09.

//モバイルの時だけMOBILE_DEVICEを宣言する
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL || UNITY_WEBPLAYER
#undef MOBILE_DEVICE
#else
#define MOBILE_DEVICE
#endif

// #undef MOBILE_DEVICE//lazycat cheat

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// ネイティブと連携するためのクラス
/// </summary>
public class NativePlugin : MonoBehaviour
{

    //Androidのクラス名のフォーマット
    private const string ANDROIND_CLASS_NAME_FORMAT = PlayerSettingsValue.BUNDLE_IDENTIFIER + ".{0}";

#if UNITY_IOS
    //ネイティブ側で作成したインスタンス
    protected IntPtr _nativeInstance;
#elif UNITY_ANDROID
	protected AndroidJavaObject _androidPlugin;
#endif

    //=================================================================================
    //初期化
    //=================================================================================

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init(Func<string, IntPtr> init, string androindClassName = "")
    {
#if UNITY_IOS && MOBILE_DEVICE
		Debug.Log (gameObject.name + "Init");
		_nativeInstance = init(name);
#elif UNITY_ANDROID && MOBILE_DEVICE
		if(!string.IsNullOrEmpty(androindClassName)){
			string classFullName = string.Format (ANDROIND_CLASS_NAME_FORMAT, androindClassName);
			Debug.Log ("ネイティブプラグイン名 : " + classFullName);
			_androidPlugin = new AndroidJavaObject(classFullName);
		}
#endif
    }

}