﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(CustomRaycast))]
public class ObstacleSpeedBehavior : MonoBehaviour
{
    public enum BoostState
    {
        None,
        Default, //normal speed
        BoostUp, //increase speed
        BoostDown //decrease speed
    }
    [SerializeField] BoostState _state;
    [SerializeField] CarCrashingPhysiscsBehavior _carCrashPhysics;
    [SerializeField] ParticleSystem _fxSparkle;
    [SerializeField] CustomRaycast _centerRaycast, _leftRaycast, _rightRaycast;
    float _defaultMoveSpeed;
    float _boostedUpSpeed;
    float _boostedDownSpeed;
    const float _upLerpSpeed = 15f;
    const float _downLerpSpeed = 7.5f;
    const float _boostedDuration = 0.6f;
    const float _boostedCooldown = 1f;
    float _currentSpeed;
    CubeScript _cubeScript;
    bool _isStopProcess;
    float _boostedTime;
    bool _centerRaycastResult, _leftRaycastResult, _rightRaycastResult;

    void Awake()
    {
        _cubeScript = GetComponent<CubeScript>();
        _fxSparkle.Play();
    }

    void OnEnable()
    {
        _centerRaycast.onRaycastResult += OnCenterRaycastResult;
        _leftRaycast.onRaycastResult += OnLeftRaycastResult;
        _rightRaycast.onRaycastResult += OnRightRaycastResult;
        _carCrashPhysics.onCollisionEnterEvent += OnCarCrashed;
    }

    void OnDisable()
    {
        _defaultMoveSpeed = 0;
        _state = BoostState.Default;
        _centerRaycast.onRaycastResult -= OnCenterRaycastResult;
        _leftRaycast.onRaycastResult -= OnLeftRaycastResult;
        _rightRaycast.onRaycastResult -= OnRightRaycastResult;
        _carCrashPhysics.onCollisionEnterEvent -= OnCarCrashed;
    }

    private void OnCarCrashed()
    {
        _isStopProcess = true;
        _fxSparkle.Stop();
    }

    void Update()
    {
        if (_isStopProcess == true)
            return;

        //first initialize
        if (_defaultMoveSpeed == 0)
        {
            _currentSpeed = _defaultMoveSpeed = _cubeScript.GetMoveSpeed();
            _boostedUpSpeed = _defaultMoveSpeed * 1.5f;
            _boostedDownSpeed = _defaultMoveSpeed * 0.8f;
        }

        //check raycast result
        if (_centerRaycastResult == true)
        {
            if (_leftRaycastResult ^ _rightRaycastResult)
                _state = BoostState.BoostDown;
            else
                if (_boostedTime == 0)
                _state = BoostState.BoostUp;
        }
        else
        {
            if (_leftRaycastResult & _rightRaycastResult)
                _state = BoostState.BoostDown;
            else
                if (_boostedTime == 0)
                _state = BoostState.BoostUp;
        }

        //check state
        switch (_state)
        {
            case BoostState.BoostUp:
                //allow car boosted in given duration then back to default speed
                if (_boostedTime < _boostedDuration)
                    _boostedTime += Time.deltaTime;
                else
                    _state = BoostState.Default;

                _currentSpeed = Mathf.Lerp(_currentSpeed, _boostedUpSpeed, Time.deltaTime * _upLerpSpeed);
                _cubeScript.SetMoveSpeed(_currentSpeed);
                break;

            case BoostState.BoostDown:
                //allow car boosted in given duration then back to default speed
                if (_boostedTime < _boostedDuration)
                    _boostedTime += Time.deltaTime;
                else
                    _state = BoostState.Default;

                _currentSpeed = Mathf.Lerp(_currentSpeed, _boostedDownSpeed, Time.deltaTime * _downLerpSpeed);
                _cubeScript.SetMoveSpeed(_currentSpeed);
                break;

            case BoostState.Default:
                //prevent car get boosted too much by adding delay time
                if (_boostedTime < _boostedCooldown)
                    _boostedTime += Time.deltaTime;
                else
                    _boostedTime = 0;

                _currentSpeed = Mathf.Lerp(_currentSpeed, _defaultMoveSpeed, Time.deltaTime * 2f);
                _cubeScript.SetMoveSpeed(_currentSpeed);
                break;
        }
    }

    private void OnCenterRaycastResult(RaycastHit hit, bool result)
    {
        _centerRaycastResult = result;
    }

    private void OnRightRaycastResult(RaycastHit hit, bool result)
    {
        _rightRaycastResult = result;
    }

    private void OnLeftRaycastResult(RaycastHit hit, bool result)
    {
        _leftRaycastResult = result;
    }
}
