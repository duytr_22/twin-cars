﻿//  ToTitleButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.26.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// タイトル画面へ戻るボタンのアシスタントクラス
/// </summary>
public class BackTitleButtonAssistant : ButtonAssistant
{

	public enum Location
	{
		Game,
		Result,
		StageSelect
	}

	[SerializeField]
	private Location _location;

	[SerializeField]
	private bool _forceReset = false;

	bool _canTap = false;

	//=================================================================================
	//内部
	//=================================================================================

	IEnumerator Start ()
	{
		if (UserData.PlayCount % 3 == 0)
			yield return new WaitForSeconds (0.75f);
		else
			yield return new WaitForSeconds (0.0f);
		
		_canTap = true;
	}

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{
		if (_canTap || _forceReset) {

			if (_location == Location.Game) {
				AdManager.Instance.UpdateAdWithPause (false);
			}

			SceneNavigator.Instance.MoveScene (SceneName.TITLE);
		}
	}
		
}