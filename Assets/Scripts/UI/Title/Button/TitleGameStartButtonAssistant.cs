﻿//  GameStartButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲーム開始ボタンのアシスタントクラス
/// </summary>
public class TitleGameStartButtonAssistant : ButtonAssistant
{

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		AdManager.Instance.CanShowSplash = true;

		//play tutorial if this is the first time run this app
		if (PlayerPrefs.GetInt ("isFirstRun") == 0) {
			SceneNavigator.Instance.MoveScene (SceneName.TUTORIAL);
			SceneManager.LoadScene (3);
			PlayerPrefs.SetInt ("isFirstRun", 1);
		} else {
			//モードによって移動先を変える
			if (UnityProjectSetting.Entity.GameType == GameType.Mini) {
				SceneNavigator.Instance.MoveScene (SceneName.GAME);
			}
		}
	}

}