﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizeImage : MonoBehaviour
{
	[SerializeField]
	bool NGUI = false;

	[SerializeField]
	string japanNameNGUI;

	[SerializeField]
	Sprite japanName;

	[SerializeField]
	bool setNativeSize = true;

	Image img;
	UISprite sprite;

	// Use this for initialization
	void Start ()
	{
		if (Application.systemLanguage
		    == SystemLanguage.Japanese) {
		
			if (!NGUI) {

				img = GetComponent<Image> ();

				img.sprite = japanName;

				if (setNativeSize)
					img.SetNativeSize ();
				
			} else {

				sprite = GetComponent<UISprite> ();

				sprite.spriteName = japanNameNGUI;

				UIButton buttonClose = gameObject.GetComponent<UIButton> ();
				if(buttonClose != null ) {
					
						buttonClose.disabledSprite = japanNameNGUI;
						buttonClose.pressedSprite = japanNameNGUI;
						buttonClose.hoverSprite = japanNameNGUI;
						buttonClose.normalSprite = japanNameNGUI;
				}			
			}
		}
	}
}
