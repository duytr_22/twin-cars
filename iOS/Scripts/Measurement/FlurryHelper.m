//
//  FlurryHelper.m
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Flurry関係の処理を行うクラス

#import "FlurryHelper.h"
#import "Flurry.h"
#import "CommonConfig.h"

@implementation FlurryHelper

+(void)startFlurry{
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:FLURRY_API_KEY];
}

+(void)reportFlurryWithEvent:(NSString *)eventName{
    NSLog(@":::::: flurry ::::::\n%@",eventName);
    [Flurry logEvent:eventName];
}


@end