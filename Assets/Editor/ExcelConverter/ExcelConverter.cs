﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;

/// <summary>
/// エクセルを変換するクラス
/// </summary>
public class ExcelConverter : AssetPostprocessor {

	/// <summary>
	/// エクセルデータを変換する
	/// </summary>
	protected static void ExcelDataConversion<DataType>(string exportPath, string targetFilePath, string targetName) where DataType : ExcelData{

		//アセットデータ作成、既にあるものは削除
		DataType data  = ScriptableObject.CreateInstance<DataType> ();
		data.hideFlags = HideFlags.NotEditable;

		AssetDatabase.DeleteAsset (exportPath);
		AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath);

		//各設定項目を保存(エクセルの一番上に記載されてるやつ KeyとかNameとか)
		Dictionary<string, int> itemNameDic = new Dictionary<string, int> ();

		//Keyを保存
		Dictionary<string, string> keyDic = new Dictionary<string, string> ();

		//エクセルを読込み、内容を変換してく
		using (FileStream stream = File.Open (targetFilePath, FileMode.Open, FileAccess.Read)) {

			//対象エクセル読込み、一番最初のシートを取得
			IWorkbook book = new HSSFWorkbook (stream);
			ISheet sheet = book.GetSheetAt (0);

			//各行を取得していく
			for (int i = 0; i <= sheet.LastRowNum; i++) {
				IRow row = sheet.GetRow (i);
				int count = 0;

				//最初の行は各設定項目を保存
				if (i == 0) {
					foreach (ICell cell in row.Cells) {
						itemNameDic [cell.StringCellValue] = count;
						count++;
					}
				} 
				else {
					//各ステータスを読込み、リストに登録
					ExcelData.RowData rowData = new ExcelData.RowData ();
					foreach (ICell cell in row.Cells) {

						//セルの内容を取得し、タグを置換
						string cellStr = cell.ToString ();

						//keyを登録
						if(count == 0){

							//keyの重複検知
							if(keyDic.ContainsKey(cell.ToString())){
								Debug.LogError(targetName + "の" + cell.ToString() + "というkeyが重複しています!");
							}

							keyDic [cell.ToString ()] = cellStr;
						}

						rowData.DataList.Add (cellStr);
						count++;
					}

					//データがない行はスルー
					if(count == 0){
						continue;
					}

					data.RowDataList.Add (rowData);
				}
			}
		}

		//変換したデータを書き出す
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath, typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);

		//設定値のkeyを定数で管理するクラス"を書き出す
		ConstantsClassCreator.Create (targetName + "DataKeyNo", targetName + "の設定値のkeyの番号を定数で管理するクラス", itemNameDic);
		ConstantsClassCreator.Create (targetName + "Key",       targetName + "のkeyを定数で管理するクラス",             keyDic);
	}

}