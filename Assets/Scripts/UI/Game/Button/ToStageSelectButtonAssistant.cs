﻿//  ToStageSelectButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kikuchikan on 2015.08.13.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ステージセレクト画面へ戻るボタンのアシスタントクラス
/// </summary>
public class ToStageSelectButtonAssistant : ButtonAssistant
{

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		Time.timeScale = 1;
	}

}
