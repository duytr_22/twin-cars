﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueGameButtonAssistant : ButtonAssistant
{
	[SerializeField]
	bool showAds = false;

	[SerializeField]
	GameObject dialog;

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		AdManager.Instance.HideGameRect ();

		if (showAds) { // Yes

			MainManager.Instance.showAdWhenRetry = true;

			AdManager.Instance.ShowRewardVideo ();

			dialog.SetActive (false);

		} else { //  No 

			dialog.SetActive (false);

			GameManager.Instance.End (false);
		}
		
	}

}
