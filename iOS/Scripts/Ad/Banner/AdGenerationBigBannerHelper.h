//
//  BannerAdController.h
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Adstirのバナー広告のコントローラー

#import "BannerAdController.h"

#import  <ADG/ADGManagerViewController.h>

@interface AdGenerationBigBannerHelper : BannerAdController<ADGManagerViewControllerDelegate>

@property (nonatomic, retain) ADGManagerViewController *banner;

-(UIView*)prepareBannerAd;

@end
