﻿//  UIAdjuster.cs
//  ProductName ピンボール
//
//  Created by kan kikuchi on 2015.12.03.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// アス比に合わせてUIの位置やサイズを調整するクラス
/// </summary>
[ExecuteInEditMode]
public class UIAdjuster : MonoBehaviour {

	//現在のアス比
	private float _currentAspectRate = 0;
	public  float CurrentAspectRate{
		get{return _currentAspectRate;}
	}

	//アス比の種類
	protected enum AspectRateType{
		iPhone4, iPhone5
	}

	//デフォルトの画面サイズ
	private const float SCREEN_SIZE_DEFULT_WIDTH  = 640.0f;
	private const float SCREEN_SIZE_DEFULT_HEIGHT = 960.0f;

	public static readonly Vector2 SCREEN_SIZE_DEFULT   = new Vector2(SCREEN_SIZE_DEFULT_WIDTH, SCREEN_SIZE_DEFULT_HEIGHT);
	public static readonly Vector2 SCREEN_SIZE_640_960  = new Vector2(640, 960);
	public static readonly Vector2 SCREEN_SIZE_640_1136 = new Vector2(640, 1136);

	//=================================================================================
	//初期化
	//=================================================================================

	protected virtual void Awake(){
		AdjustUI ();
	}

	//インスペクターで値が変更された時
	protected virtual void OnValidate(){
		if(Application.isPlaying){
			return;
		}
		AdjustUI();
	}

	//=================================================================================
	//判定
	//=================================================================================

	//iPadかどうか
	protected static bool IsIpad(){
		bool isIpad = SystemInfo.deviceModel.Contains ("iPad");

		//エディター上ではiPadの判定が取れないのでアス比で判断
		#if UNITY_EDITOR && UNITY_IOS

		float aspectRate = (float)Screen.width / (float)Screen.height;

		if (Mathf.Abs (aspectRate - (3f / 4f)) < 0.001f) {
			isIpad = true;
		}

		#endif

		return isIpad;
	}

	//=================================================================================
	//更新
	//=================================================================================

	#if UNITY_EDITOR
	protected virtual void Update () {
		if(Application.isPlaying){
			return;
		}
		//アス比が変わったら調整
		if(_currentAspectRate != Screen.width / Screen.height){
			AdjustUI ();
		}
	}
	#endif

	//UIを調整
	protected virtual void AdjustUI(){
		_currentAspectRate = (float)Screen.width / (float)Screen.height;
	}

	//=================================================================================
	//画面サイズ関係
	//=================================================================================

	/// <summary>
	/// デフォルトのアス比との比を返す
	/// </summary>
	protected static float GetScreenSizeRate(Vector2 defultScreenSize, bool isStandardHeight){
		if(isStandardHeight){
			return (defultScreenSize.y / defultScreenSize.x)  / ((float)Screen.height / (float)Screen.width);
		}
		return (defultScreenSize.x  / defultScreenSize.y) / ((float)Screen.width  / (float)Screen.height);
	}
	public static float GetScreenSizeRate(){
		return GetScreenSizeRate (SCREEN_SIZE_DEFULT, false);
	}

	/// <summary>
	/// デフォルトの画面サイズと現状のサイズのアス比の差を返す
	/// </summary>
	protected static float GetScreenSizeRateDistance(Vector2 defultScreenSize){

		float heightRate = defultScreenSize.y / defultScreenSize.x  - (float)Screen.height / (float)Screen.width;
		float widthRate  = defultScreenSize.x / defultScreenSize.y -  (float)Screen.width  / (float)Screen.height;

		float screenSizeRateDistance = Mathf.Max(heightRate, widthRate);

		return screenSizeRateDistance;
	}

}