﻿Shader "Custom/Unlit/TextureTransparentColored"
{
	Properties
    {
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
        _Color1 ("Main Color1", Color) = (1,1,1,1)
		_Color2 ("Main Color2", Color) = (1,1,1,1)
		_ShadowColor1 ("Shadow Color 1", Color) = (1,1,1,1)
		_ShadowColor2 ("Shadow Color 2", Color) = (1,1,1,1)
		// _Color ("Main Color", Color) = (1,1,1,1)
		// _MainTex ("Base (RGB)", 2D) = "white" {}
    }

    SubShader
    {
        //Tags {"Queue"="Transparent" "IgnoreProjector"="true" "RenderType"="Transparent"}
		Tags {"Queue" = "Geometry" "IgnoreProjector"="true" "RenderType" = "Opaque"}
        // LOD 100
        
        // ZWrite On
        // Lighting Off
        // Fog { Mode Off }

        //Blend SrcAlpha OneMinusSrcAlpha
   
        Pass
		{
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma fragmentoption ARB_fog_exp2
			#pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog

			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			//#include "Lighting.cginc"
			// struct appdata
			// {
			// 	float4 vertex : POSITION;
			// 	float2 uv : TEXCOORD0;
			// };

			// struct v2f
			// {
			// 	float2 uv : TEXCOORD0;
			// 	float4 vertex : SV_POSITION;
			// 	LIGHTING_COORDS(1,2)
			// };

			struct v2f
				{
					float4	pos			: SV_POSITION;
					float2	uv			: TEXCOORD0;
					UNITY_FOG_COORDS(1)
					LIGHTING_COORDS(1,2)
				};

			
			float4 _MainTex_ST;
            fixed4 _Color1;
			fixed4 _Color2;
			fixed4 _ShadowColor1;
			fixed4 _ShadowColor2;
			v2f vert (appdata_tan v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex).xy;
				 UNITY_TRANSFER_FOG(o,o.pos);
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			sampler2D_half _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed atten = SHADOW_ATTENUATION(i);
				//UNITY_LIGHT_ATTENUATION(atten, i, 0);

				// fixed atten = LIGHT_ATTENUATION(i);

				fixed4 col = tex2D(_MainTex, i.uv);//* atten;
				half3 delta = col.rgb - _Color1.rgb;
				// fixed4 shadowcolor = (abs(delta.r) + abs(delta.g) + abs(delta.b)) > 0.1 ? _ShadowColor2 : _ShadowColor1;
				fixed4 maincolor = (atten >= 0.9) ? _Color1 : _ShadowColor1;
				UNITY_APPLY_FOG(i.fogCoord, maincolor);
				return maincolor;
				//return col;
			}
			ENDCG
		}
		Pass {
			Tags {"LightMode" = "ForwardAdd"}
			Blend One One
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fwdadd_fullshadows
				#pragma fragmentoption ARB_fog_exp2
				#pragma fragmentoption ARB_precision_hint_fastest
	            //#pragma multi_compile_fog
				
				#include "UnityCG.cginc"
				#include "AutoLight.cginc"
				
				struct v2f
				{
					float4	pos			: SV_POSITION;
					float2	uv			: TEXCOORD0;
					//UNITY_FOG_COORDS(1)
					LIGHTING_COORDS(1,2)
				};

				float4 _MainTex_ST;
				fixed4 _Color1;
				fixed4 _Color2;
				fixed4 _ShadowColor1;
				fixed4 _ShadowColor2;
				v2f vert (appdata_tan v)
				{
					v2f o;
					
					o.pos = UnityObjectToClipPos( v.vertex);
					o.uv = TRANSFORM_TEX (v.texcoord, _MainTex).xy;
					 //UNITY_TRANSFER_FOG(o,o.pos);
					TRANSFER_VERTEX_TO_FRAGMENT(o);
					return o;
				}

				sampler2D_half _MainTex;

				fixed4 frag(v2f i) : SV_Target
				{
					//fixed atten = LIGHT_ATTENUATION(i);	// Light attenuation + shadows.
					fixed atten = SHADOW_ATTENUATION(i); // Shadows ONLY.
					fixed4 col = tex2D(_MainTex, i.uv);
					half3 delta = col.rgb - _Color1.rgb;
					// fixed4 shadowcolor = (abs(delta.r) + abs(delta.g) + abs(delta.b)) > 0.1 ? _ShadowColor2 : _ShadowColor1;
					fixed4 maincolor = (atten >= 0.9) ? _Color1 : _ShadowColor1;
					//UNITY_APPLY_FOG(i.fogCoord, maincolor);
					return maincolor;
				}
			ENDCG
		}
    } FallBack "VertexLit"
}
