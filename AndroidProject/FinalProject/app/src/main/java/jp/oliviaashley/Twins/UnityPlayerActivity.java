package jp.oliviaashley.Twins;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.igaworks.IgawCommon;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.integration.IntegrationHelper;
import com.tenjin.android.TenjinSDK;
import com.unity3d.player.UnityPlayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import jp.oliviaashley.Advertising.AdvertisingManager;
import jp.oliviaashley.Advertising.SceneManager;
import jp.oliviaashley.Social.GooglePlayServices.GooglePlayServicesManager;

public class UnityPlayerActivity extends Activity {
    protected UnityPlayer mUnityPlayer; // don't change the name of this
    // variable; referenced from native code

    private static final String TAG = "UnityPlayerActivity";

    // Setup activity layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

//        IgawCommon.autoSessionTracking(getApplication());

        mUnityPlayer = new UnityPlayer(this);
        if (mUnityPlayer.getSettings().getBoolean("hide_status_bar", true))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int glesMode = mUnityPlayer.getSettings().getInt("gles_mode", 1);
        boolean trueColor8888 = true;
        mUnityPlayer.init(glesMode, trueColor8888);

        final ImageView img = new ImageView(this);
        img.setImageResource(R.drawable.splash);
        img.setScaleType(ImageView.ScaleType.FIT_XY);
        mUnityPlayer.addView(img);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mUnityPlayer.removeView(img);
            }
        }, 6000);

        setContentView(mUnityPlayer.getView());
        mUnityPlayer.requestFocus();

        IronSource.init(this, this.getResources().getString(R.string.IRONSOURCE_API_KEY));
//        IntegrationHelper.validateIntegration(this);
//        IronSource.setAdaptersDebug(true);

        AdvertisingManager.doOnCreate(this, (FrameLayout) getWindow().getDecorView());
        SceneManager.setWindowSize(this, 320, 480);
        GooglePlayServicesManager.doOnCreate(this);

        if (isOnline()) {
            Log.d("myTag", "online");

            final SharedPreferences sharedPreferences = this.getSharedPreferences(
                    "NewsPopUp", Context.MODE_PRIVATE);

            int count = sharedPreferences.getInt("LaunchCount", 0);

            // Update play count
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (editor != null) {
                editor.putInt("LaunchCount", count + 1);
                editor.commit();
            }

//            // CheckSplSpfJson
//            String type = CheckSplSpfJson.getNewsType();
//
//            if (type.equals("1")){ // Nend
//
//            }else if(type.equals("2")){
//
//                if (count >= 3) {
////                    HeyzapHepler.showInterstitialHouseAd(this);
//                }
//            } else if(type.equals("0")){

//                if (count >= 2) {

                    // Show cross promotion
                    boolean isAppInstalled = appInstalledOrNot("jp.oliviaashley.WorldMaker");

                    SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
                    boolean defaultValue = sharedPref.getBoolean(this.getResources().getString(R.string.remove_ads), false);

                    if (!isAppInstalled && !defaultValue) {

//                        UnityPlayer.UnitySendMessage("CrossPromotion", "ShowCrossPromotion", "");
                    }
//                }
//            }
        }
    }

    // Quit Unity
    @Override
    protected void onDestroy() {
//		AdvertisingManager.doOnDestroy(this);
        mUnityPlayer.quit();
        GooglePlayServicesManager.doOnDestroy(this);
        super.onDestroy();
    }

    // Pause Unity
    @Override
    protected void onPause() {
        super.onPause();
        mUnityPlayer.pause();
        AdvertisingManager.doOnPause(this);
    }

    // Resume Unity
    @Override
    protected void onResume() {
        super.onResume();
        mUnityPlayer.resume();
        AdvertisingManager.doOnResume(this);

        //Integrate TenjinSDK connect call
        TenjinSDK instance = TenjinSDK.getInstance(this, this.getResources().getString(R.string.tenjin_ipa_key));
        instance.connect();
    }

    protected void onStart() {
        super.onStart();
        AdvertisingManager.doOnStart(this);
        GooglePlayServicesManager.doOnStart(this);
    }

    protected void onStop() {
        AdvertisingManager.doOnStop(this);
        GooglePlayServicesManager.doOnStop(this);
        super.onStop();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        //AdvertisingManager.doOnBackPressed(this);
        AdvertisingManager.showExitDialog(this);
    }

    @Override
    protected void onActivityResult(int request, int response, Intent data) {

        super.onActivityResult(request, response, data);

        GooglePlayServicesManager.doOnActivityResult(request, response, data);

        /*
		IAP
		 */
        if (request == this.getResources().getInteger(R.integer.IAP_Request_Code)) {

            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);

            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (response == RESULT_OK) {

                try {

                    JSONObject jo = new JSONObject(purchaseData);

                    String sku = jo.getString("productId");
                    String token = jo.getString("purchaseToken");

                    if(sku.equals(getResources().getString(R.string.Item_No_Ads))) { // No ads

                        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean(getString(R.string.remove_ads), true);
                        editor.commit();

                        UnityPlayer.UnitySendMessage("NativeManager", "PurchaseSuccess", "");

                        GooglePlayServicesManager.ReportPurchase(this);
                    }

                } catch (JSONException e) {

                    Log.v("IAP", "Failed to parse purchase data.");

                    e.printStackTrace();
                }
            }
        }
        /*
        IAP
         */
    }

    // This ensures the layout will be correct.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown");

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //AdvertisingManager.doOnBackPressed(this);
            AdvertisingManager.showExitDialog(this);
            return true;
        }

        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    /* API12 */
    public boolean onGenericMotionEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    // アプリケーション名
    private String getAppName(String title) {
        String[] strsA;
        String[] strsB;

        Log.d(TAG, "title:" + title);
        strsA = title.split("「");
        strsB = strsA[1].split("」");
        Log.d(TAG, "strsB[0]:" + strsB[0]);

        return strsB[0];
    }

    // インストール済みかどうか
    private boolean checkInstalled(String scheme) {
        PackageManager pm = this.getPackageManager();
        List<ApplicationInfo> infos = pm.getInstalledApplications(0);
        for (ApplicationInfo ai : infos) {
            if (scheme != null && scheme.equals(ai.packageName)) {
                return true;
            }
        }
        return false;
    }

    // オンラインかどうか
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null) {
            int type = info.getType();
            if (type == ConnectivityManager.TYPE_MOBILE
                    || type == ConnectivityManager.TYPE_WIFI) {
                Log.d("myTag", "NetworkInfo:" + info.getTypeName());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // レコメンドダイアログ
    private void createRecomDialog(final String title, final String url) {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        if (Locale.getDefault().equals(Locale.JAPAN)) {
            adb.setTitle("お知らせ");
            adb.setMessage(title);
            adb.setPositiveButton("はい", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Flurry
                    FlurryAgent.logEvent("POPUP_TAP_YES:" + getAppName(title));

                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });
            adb.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        } else {
            adb.setTitle("Hot News!");
            adb.setMessage(title);
            adb.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Flurry
                    FlurryAgent.logEvent("POPUP_TAP_YES:" + getAppName(title));

                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });
            adb.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        AlertDialog ad = adb.create();
        ad.show();
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }
}
