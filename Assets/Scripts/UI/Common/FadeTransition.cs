﻿//  FadeTransition.cs
//  ProductName Template
//
//  Created by kan.kikuchi

using UnityEngine;
using System.Collections;

/// <summary>
/// フェード用クラス
/// </summary>
public class FadeTransition : MonoBehaviour {

	//シーン移動時間
	public const float FADE_TIME = 0.2f;

	//状態
	private enum Status{
		WaitFadeOut, FadeOut, WaitFadeIn, FadeIn
	}
	private Status _status = Status.WaitFadeOut;

	//暗転用黒テクスチャ
	private Texture2D _blackTexture;

	//現在の透明度
	private float _fadeAlpha = 0;

	//フェード中か
	private bool _isFading = false;

	//=================================================================================
	//初期化
	//=================================================================================

	//黒背景作成
	private IEnumerator CreateBlackTexture (){
		_blackTexture = new Texture2D (32, 32, TextureFormat.RGB24, false);

		//レンダリングが完了するまで待つ(待たないとReadPixelsでエラーが出ることがある)
		yield return new WaitForEndOfFrame();

		_blackTexture.ReadPixels (new Rect (0, 0, 32, 32), 0, 0, false);
		_blackTexture.SetPixel (0, 0, Color.white);
		_blackTexture.Apply ();
	}

	//=================================================================================
	//ステータス変更
	//=================================================================================

	/// <summary>
	/// フェードインを開始する
	/// </summary>
	public void FadeIn(){
		_status = Status.FadeIn;
	}

	//フェードイン終了
	private void FadeInEnd(){
		_isFading = false;
		_status   = Status.WaitFadeOut;
	}

	/// <summary>
	/// フェードアウトを開始する
	/// </summary>
	public void FadeOut(){
		_isFading = true;
		_fadeAlpha = 0;
		_status = Status.FadeOut;
	}

	//フェードアウト終了
	private void FadeOutEnd(){
		_status = Status.WaitFadeIn;
		GetComponent<SceneNavigator> ().FadeOutEnd ();
	}

	//=================================================================================
	//更新
	//=================================================================================

	private void Update () {
		if(!_isFading){
			return;
		}

		//フェードアウト
		if(_status == Status.FadeOut && _fadeAlpha != 1.0f){
			_fadeAlpha += Time.unscaledDeltaTime / FadeTransition.FADE_TIME;

			if(_fadeAlpha >= 1.0f){
				FadeOutEnd ();
			}
		}
		//フェードイン
		else if(_status == Status.FadeIn && _fadeAlpha != 0.0f){
			_fadeAlpha -= Time.unscaledDeltaTime / FadeTransition.FADE_TIME;

			if(_fadeAlpha <= 0.0f){
				FadeInEnd ();
			}
		}

	}

	//フェード中は黒背景を表示し、透明度を変更
	private void OnGUI (){
		if(!_isFading){
			return;
		}

		if(_blackTexture == null){
			StartCoroutine ("CreateBlackTexture");
		}

		//透明度を更新して黒テクスチャを描画
		GUI.color = new Color (0, 0, 0, _fadeAlpha);
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), _blackTexture);
	}

}