//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  スプラッシュ(インターステイシャル)広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "InterstitialAdController.h"

@implementation InterstitialAdController

//初期化
- (id)init{
    self = [super init];
    return self;
}

//表示
-(void)showInterstitialAd:(int)playCount{}

//表示が可能か
-(BOOL)canShow:(int)playCount{
    
    //ランキングのかぶせるやつはこの時点で表示
    if(_isRankInterstitial){
        return YES;
    } else {
        
        int64_t splashAdFrequency = [GAEConfig getSplashFrequency];
        
        //GAEの設定で未設定だったら3が入っているはず。0だったら表示しない
        if (splashAdFrequency == 0) {
            return NO;
        }
        
        NSLog(@"playCount %% splashAdFrequency\n%d %% %lld = %lld", playCount, splashAdFrequency, playCount % splashAdFrequency);
        
        //GAEの設定で割った余りが0じゃなかったら表示しない
        if (playCount % splashAdFrequency != 0) {
            NSLog(@"GAEの設定で割った余りが0じゃなかったら表示しない: %d %% %lld = %lld", playCount, splashAdFrequency, playCount % splashAdFrequency);
            return NO;
        }
        
        return YES;
        
    }
}

-(void)setRankType:(BOOL)isRankInterstitial{
    _isRankInterstitial = isRankInterstitial;
}

@end
