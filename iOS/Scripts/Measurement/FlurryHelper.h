//
//  FlurryHelper.m
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Flurry関係の処理を行うクラス

@interface FlurryHelper : NSObject;

+(void)startFlurry;
+(void)reportFlurryWithEvent:(NSString *)eventName;
@end