﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


public class TutorialManager : MonoBehaviour
{
    static TutorialManager instance;

    public static TutorialManager Instance { get { return instance; } }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    public enum TutorialState
    {
        Start_ShowText0,
        ShowText_1,
        ShowFingerRightAndDelay,
        HideFingerThenMoveBallRight,
        MoveBallRightBack,
        ShowText2,
        MovePlayerRight,
        Scroll,
        DelayTime_1,
        ShowText3,
        MovePlayerRightBack_ShowText3_2Finger,
        Hide2Finger_ShowText4,
        Scroll_2,
        DelayTime_2,
        Move2PlayerBack,
        Scroll_3,
        //		Pause,
        ShowText9,
        ShowText5,
        Finish,
    }

    public GamePlayScript _gamePlayScript;
    public GameObject playerLeft;
    public GameObject playerRight;

    public PlayerScript _pLeft;
    public PlayerScript _pRight;

    public GameObject fingerLeft;
    public GameObject fingerRight;
    public List<GameObject> tutTexts;

    //tutorial spawn
    public List<TutorialSpawnPoints> tutorialSpawnPoints;
    [HideInInspector] public int tutorialMaxRowsCube;
    private TutorialState state;


    private bool movingLeft;
    private bool movingRight;

    private bool isDelay;
    public float _timeDelay;

    private int tapCount;
    [HideInInspector] public float lastTimeScale;
    [HideInInspector] public bool isPause;
    public bool demoPlay;
    public bool isPass;

    // Use this for initialization
    void Start()
    {
        DOTween.Init(true, true, LogBehaviour.Default);

        TutorialPointScript.OnTriggerTutorialPoint += OnTutorialEventTriggerPoint;
        TutorialPointScript.OnTriggerTutorialPassPoint += OnTutorialEventTriggerPassPoint;
        demoPlay = true;
        PlayerScript.OnPlayerDie += OnPlayerDie;
        _timeDelay = 0.0f;
        state = TutorialState.Start_ShowText0;

        _pLeft = playerLeft.GetComponent<PlayerScript>();
        _pRight = playerRight.GetComponent<PlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPause)
        {
            return;
        }
        if (!isDelay)
        {
            this.UpdateTutorialInput();
        }
        else
        {
            _timeDelay -= Time.fixedDeltaTime;
            if (_timeDelay <= 0)
            {
                isDelay = false;
            }
        }
    }

    void OnTutorialEventTriggerPoint()
    {
        if (tutorialMaxRowsCube <= 4 && tutorialMaxRowsCube > 0)
        {
            tutorialMaxRowsCube--;
            Time.timeScale = 0;
            state++;
        }
        isPass = false;
    }

    void OnTutorialEventTriggerPassPoint()
    {
        isPass = true;
    }

    public void showFinger(bool isFingerLeft, bool value)
    {
        if (isFingerLeft)
        {
            FingerBlinkScript leftScript = fingerLeft.GetComponent<FingerBlinkScript>();
            if (value)
            {
                leftScript.start();
            }
            else
            {
                leftScript.stop();
            }
        }
        else
        {
            FingerBlinkScript rightScript = fingerRight.GetComponent<FingerBlinkScript>();
            if (value)
            {
                rightScript.start();
            }
            else
            {
                rightScript.stop();
            }
        }
    }

    public void ShowFingers(bool left, bool right)
    {
        FingerBlinkScript leftScript = fingerLeft.GetComponent<FingerBlinkScript>();
        if (left)
        {
            leftScript.start();
        }
        else
        {
            leftScript.stop();
        }

        FingerBlinkScript rightScript = fingerRight.GetComponent<FingerBlinkScript>();
        if (right)
        {
            rightScript.start();
        }
        else
        {
            rightScript.stop();
        }
    }

    public void showFingersWithoutMove(bool left, bool right)
    {
        FingerBlinkScript leftScript = fingerLeft.GetComponent<FingerBlinkScript>();
        if (left)
        {
            leftScript.startWithoutScale();
        }
        else
        {
            leftScript.stop();
        }

        FingerBlinkScript rightScript = fingerRight.GetComponent<FingerBlinkScript>();
        if (right)
        {
            rightScript.startWithoutScale();
        }
        else
        {
            rightScript.stop();
        }
    }

    public void ShowFingers(bool finger)
    {
        this.ShowFingers(finger, finger);
    }

    public void ShowTutorialText(int id)
    {
        id += 1;
        for (int i = 0; i < tutTexts.Count; i++)
        {
            if (i == id)
            {
                tutTexts[i].SetActive(true);
            }
            else
            {
                tutTexts[i].SetActive(false);
            }
        }
    }

    public void UpdateTutorialInput()
    {
        Touch[] touches = Input.touches;

        float durationDislayText = 2.5f;
        switch (state)
        {

            case TutorialState.Start_ShowText0:
                ShowTutorialText(-1);
                break;
            case TutorialState.ShowText_1:
                ShowTutorialText(0);
                delayTimeAfter(0.5f, true);
                break;
            case TutorialState.ShowFingerRightAndDelay:
                //			showFinger (false, true);
                ShowFingers(false, true);
                delayTimeAfter(0.9f, true);
                break;
            case TutorialState.HideFingerThenMoveBallRight:
                ShowFingers(false, false);
                StartCoroutine(MovePlayer(_pRight, 1, false));
                delayTimeAfter(0.9f, true);
                break;

            case TutorialState.MoveBallRightBack:
                ShowFingers(false, false);
                StartCoroutine(MovePlayer(_pRight, -1, false));
                delayTimeAfter(0.15f, true);
                break;

            case TutorialState.ShowText2:
                ShowTutorialText(1);
                delayTimeAfter(1.0f, true);
                break;

            case TutorialState.MovePlayerRight:
                showFingersWithoutMove(false, true);
                StartCoroutine(MovePlayer(_pRight, 1, false));
                delayTimeAfter(durationDislayText / 2, true);
                break;

            case TutorialState.Scroll:
                Time.timeScale = 1;
                break;

            case TutorialState.DelayTime_1:
                ShowTutorialText(2);
                delayTimeAfter(1.0f, true);
                break;
            case TutorialState.ShowText3:
                ShowFingers(false, false);
                StartCoroutine(MovePlayer(_pRight, -1, false));
                delayTimeAfter(durationDislayText / 2, true);
                break;

            case TutorialState.MovePlayerRightBack_ShowText3_2Finger:

                ShowTutorialText(3);
                ShowFingers(true, true);
                delayTimeAfter(durationDislayText / 2 + 0.5f, true);
                break;

            case TutorialState.Hide2Finger_ShowText4:
                StartCoroutine(MovePlayer(_pRight, 1, false));
                StartCoroutine(MovePlayer(_pLeft, 1, false));
                ShowTutorialText(1);
                ShowFingers(false, false);
                showFingersWithoutMove(true, true);
                delayTimeAfter(durationDislayText / 2, true);
                break;

            case TutorialState.Scroll_2:
                Time.timeScale = 1;
                break;

            case TutorialState.DelayTime_2:
                ShowTutorialText(2);
                delayTimeAfter(durationDislayText / 2, true);
                break;

            case TutorialState.Move2PlayerBack:
                ShowTutorialText(-2);
                ShowFingers(false, false);

                StartCoroutine(MovePlayer(_pRight, -1, false));
                StartCoroutine(MovePlayer(_pLeft, -1, false));
                delayTimeAfter(0.5f, true);
                break;

            case TutorialState.Scroll_3:
                Time.timeScale = 1;
                //	delayTimeAfter (0.25f, true);
                break;

            case TutorialState.ShowText9:
                ShowTutorialText(8);
                delayTimeAfter(durationDislayText, true);
                break;

            case TutorialState.ShowText5:
                ShowTutorialText(4);
                delayTimeAfter(durationDislayText, true);
                break;
            case TutorialState.Finish:
                delayTimeAfter(999, false);
                SceneNavigator.Instance.MoveScene(SceneName.GAME);
                break;
            default:
                break;
        }
    }

    IEnumerator MovePlayer(PlayerScript script, int dir, bool isState)
    {

        if (script.type == PlayerScript.PlayerType.PlayerTypeLeft)
        {
            if (movingLeft)
            {
                yield break;
            }
            else
            {
                movingLeft = true;
            }
        }

        if (script.type == PlayerScript.PlayerType.PlayerTypeRight)
        {
            if (movingRight)
            {
                yield break;
            }
            else
            {
                movingRight = true;
            }
        }

        float t = 0;
        float duration = 0.15f;
        if (dir > 0)
        {
            t = 0;
        }
        else
        {
            t = duration;
        }

        SEManager.Instance.PlaySE(AudioName.SE_MOVE, 0, 1, 1);

        while (dir > 0 ? t < duration : t > 0.0f)
        {
            if (dir > 0)
            {
                t += Time.fixedDeltaTime;
            }
            else
            {
                t -= Time.fixedDeltaTime;
            }

            float posX = script.positionA.x + (Mathf.Abs(script.positionB.x - script.positionA.x) * (t / duration) * (int)script.type);
            script.transform.localPosition = new Vector3(posX, script.positionA.y, script.positionA.z);

            yield return null;
        }

        if (dir == 1)
        {
            script.transform.localPosition = script.positionB;
        }
        else
        {
            script.transform.localPosition = script.positionA;
        }

        if (isState)
        {
            state++;
        }

        if (script.type == PlayerScript.PlayerType.PlayerTypeLeft)
        {
            movingLeft = false;
        }
        else
        {
            movingRight = false;
        }

        yield break;
    }

    void delayTimeAfter(float seond, bool isState)
    {
        _timeDelay = seond;
        isDelay = true;
        if (isState)
            state++;
    }

    IEnumerator DelayTime(float maxTime, bool isState)
    {
        if (isDelay)
        {
            yield break;
        }
        else
        {
            isDelay = true;
        }

        float countDelayTime = 0;
        while (countDelayTime <= maxTime)
        {
            countDelayTime += (1 / 60.0f);
            yield return null;
        }
        if (isState)
            state++;
        yield break;
    }

    void OnPlayerDie(PlayerScript script)
    {
        //state = TutorialState.Over;
        state++;
    }
}
