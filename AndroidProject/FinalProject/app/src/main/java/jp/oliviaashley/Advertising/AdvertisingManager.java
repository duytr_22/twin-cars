package jp.oliviaashley.Advertising;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.FrameLayout;

import java.util.Locale;

import jp.oliviaashley.Advertising.Providers.AdmobHelper;
import jp.oliviaashley.Advertising.Providers.FlurryHelper;
import jp.oliviaashley.Advertising.Providers.IronSourceHelper;
import jp.oliviaashley.Advertising.Providers.MovieAdsHelper;


public class AdvertisingManager {
    private static final String TAG = "Advertising";

    public static void doOnCreate(final Activity activity,
                                  final FrameLayout mainLayout) {
        Log.v(TAG, "doOnCreate()");
        // Scene Manager
        SceneManager.doOnCreate(activity, mainLayout);
        // Splash Manager
        SplashAdManager.doOnCreate(activity);

        IronSourceHelper.doOnCreate(activity);
        // Flurry
        FlurryHelper.doOnCreate(activity);

        //Admob
        AdmobHelper.doOnCreate(activity);

        MovieAdsHelper.doOnCreate(activity);
    }

    public static void doOnStart(final Activity activity) {
        Log.v(TAG, "doOnStart()");
        // Flurry
        FlurryHelper.doOnStart(activity);
    }

    public static void doOnResume(@SuppressWarnings("UnusedParameters") final Activity activity) {
        Log.v(TAG, "doOnResume()");
    }

    public static void doOnPause(final Activity activity) {
        Log.v(TAG, "doOnPause()");
        // Banner Manager
        BannerManager.doOnPause(activity);
    }

    public static void doOnStop(final Activity activity) {
        Log.v(TAG, "doOnStop()");
        // Flurry
        FlurryHelper.doOnStop(activity);
    }

    public static void doOnDestroy(final Activity activity) {
        Log.v(TAG, "doOnDestroy()");
        // Banner Manager
        BannerManager.doOnDestroy(activity);
    }

    public static void doOnRestart(@SuppressWarnings("UnusedParameters") final Activity activity) {
        Log.v(TAG, "doOnRestart()");
    }

    public static void doOnBackPressed(final Activity activity) {
        Log.v(TAG, "doOnBackPressed()");
        if (SceneManager.currentScene == 1 || SceneManager.currentScene == 2) {
            showExitDialog(activity);
        }
    }

    public static void doOnWindowFocusChanged(final Activity activity, boolean hasFocus) {
        // Native Banner Ad
        SceneManager.doOnWindowFocusChanged(activity, hasFocus);
    }

    public static void showExitDialog(final Activity activity) {

        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        if (Locale.getDefault().equals(Locale.JAPAN)) {
            adb.setTitle("終了");
            adb.setMessage("アプリを終了しますか？");
            adb.setPositiveButton("はい", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // exit
                    activity.finish();
                }
            });
            adb.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        } else {
            adb.setTitle("Exit Application");
            adb.setMessage("Are you sure?");
            adb.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // exit
                    activity.finish();
                }
            });
            adb.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        AlertDialog ad = adb.create();
        ad.show();
    }


}
