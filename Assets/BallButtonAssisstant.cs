﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallButtonAssisstant : ButtonAssistant
{

	[SerializeField]
	GameObject _icon;

	// Use this for initialization
	void OnEnable ()
	{
		_icon.SetActive (false);

		for (int i = 0; i < 9; i++) {

			if (PlayerPrefs.GetInt (string.Format (Define.BALL_UNLOCK, i + 1), 0) == 1) { // New

				_icon.SetActive (true);

				break;
			}
		}

		for (int i = 0; i < 5; i++) {

			if (PlayerPrefs.GetInt (string.Format (Define.STAGE_UNLOCK, i + 1), 0) == 1) { // New

				_icon.SetActive (true);

				break;
			}
		}
	}
	
	// Update is called once per frame
	protected override void OnClickButton ()
	{
		SceneNavigator.Instance.MoveScene (SceneName.BALL);
	}
}
