﻿//  Define.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.28.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using static CarsRenderPattern;

/// <summary>
/// 汎用的な定数クラス
/// </summary>
public static class Define
{
	//スコアの単位
	public const string SCORE_UNIT = "";

	public const string LAST_SCORE = "LAST_SCORE";
	public const string CONTINUE_GAME = "CONTINUE_GAME";
	public const string CURRENT_COLOR_INDEX = "CURRENT_COLOR_INDEX";
	public const string NO_ADS = "NO_ADS";
	public const string BALL_UNLOCK = "BALL_UNLOCK_{0}";
	public const string BALL_SELECTED = "BALL_SELECTED";
	public const string STAGE_UNLOCK = "STAGE_UNLOCK_{0}";
	public const string STAGE_SELECTED = "STAGE_SELECTED";
	public const string TOTAL_SCORE = "TOTAL_SCORE";
	public const string OBSTACLE_NAME = "Obs_Car";
	public const string PLAYER_NAME = "PLAYER";
	public const string IS_SHOW_TUTORIAL_KEY = "isFirstRun";
	public static PatternID DEBUG_CAR_PATTERN_PLAYER = PatternID.Pattern2a;
	public static PatternID DEBUG_CAR_PATTERN_ENEMY = PatternID.Pattern2a;

	public static bool IsIphoneX()
	{
		bool isIphoneX = false;
		int screenHeight = Screen.height;
		float screenRate = (float) Screen.height / (float) Screen.width;
		if (screenRate >= 2.0f)
		{
			isIphoneX = true;
		}

		return isIphoneX;
	}


	public static bool IsShowTutorial()
	{
		return PlayerPrefs.GetInt(IS_SHOW_TUTORIAL_KEY) == 0;
	}

	public static bool IsIpad()
	{
		bool isIpad = SystemInfo.deviceModel.Contains("iPad");

		//エディター上ではiPadの判定が取れないのでアス比で判断
#if UNITY_EDITOR && UNITY_IOS
        float aspectRate = (float)Screen.width / (float)Screen.height;

        if (Mathf.Abs(aspectRate - (3f / 4f)) < 0.001f)
        {
            isIpad = true;
        }

#endif

		return isIpad;
	}

	public static void NextPlayerSkin()
	{
		if (DEBUG_CAR_PATTERN_PLAYER != PatternID.Pattern3b)
			DEBUG_CAR_PATTERN_PLAYER++;
		else
			DEBUG_CAR_PATTERN_PLAYER = PatternID.Pattern1;
	}

	public static void PrevPlayerSkin()
	{
		if (DEBUG_CAR_PATTERN_PLAYER != PatternID.Pattern1)
			DEBUG_CAR_PATTERN_PLAYER--;
		else
			DEBUG_CAR_PATTERN_PLAYER = PatternID.Pattern3b;
	}

	public static void NextEnemySkin()
	{
		if (DEBUG_CAR_PATTERN_ENEMY != PatternID.Pattern3b)
			DEBUG_CAR_PATTERN_ENEMY++;
		else
			DEBUG_CAR_PATTERN_ENEMY = PatternID.Pattern1;
	}

	public static void PrevEnemySkin()
	{
		if (DEBUG_CAR_PATTERN_ENEMY != PatternID.Pattern1)
			DEBUG_CAR_PATTERN_ENEMY--;
		else
			DEBUG_CAR_PATTERN_ENEMY = PatternID.Pattern3b;
	}
}