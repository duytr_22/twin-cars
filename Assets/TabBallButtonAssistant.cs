﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabBallButtonAssistant : ButtonAssistant
{

	TweenScale _scaleTw;
	TweenPosition _positionTw;

	[SerializeField]
	GameObject _icon;

	void Start ()
	{
		_scaleTw = GetComponent<TweenScale> ();
		_positionTw = GetComponent<TweenPosition> ();

		CheckShowIcon ();
	}

	public void CheckShowIcon ()
	{
		_icon.SetActive (false);

		for (int i = 0; i < 9; i++) {

			if (PlayerPrefs.GetInt (string.Format (Define.BALL_UNLOCK, i + 1), 0) == 1) { // New

				_icon.SetActive (true);

				break;
			}
		}
	}

	protected override void OnClickButton ()
	{
		BallController.Instance.ShowTab (0);
	}

	public void SelectAnim ()
	{
		_scaleTw.PlayForward ();
		_positionTw.PlayForward ();
	}

	public void UnselectAnim ()
	{
		_scaleTw.PlayReverse ();
		_positionTw.PlayReverse ();
	}
}
