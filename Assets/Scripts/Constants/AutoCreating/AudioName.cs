﻿/// <summary>
/// オーディオ名を定数で管理するクラス
/// </summary>
public static class AudioName{

	  public const string SE_BUTTON = "SE_Button";
	  public const string SE_MOVE   = "SE_Move";

}
