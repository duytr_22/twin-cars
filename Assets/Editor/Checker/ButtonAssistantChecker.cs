﻿//  ButtonAssistantChecker.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.05.13.

using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ButtonにButtonAssistantが付いてるかチェックするクラス
/// </summary>
[InitializeOnLoad]
public class ButtonAssistantChecker {

	//Unityエディター起動時に事項される
	static ButtonAssistantChecker(){
		//エディターの状態が変更された時のコールバックを設定
		EditorApplication.playmodeStateChanged += OnPlayModeStateChanged;
	}

	//エディターの状態が変更された時に実行
	private static void OnPlayModeStateChanged (){
		//再生された時のみ実行
		if(!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode){
			CheckButtonAssistant ();
		}
	}

	//ButtonにButtonAssistantに付いてるかチェック
	private static void CheckButtonAssistant(){
		//ヒエラルキー上のButtonを全て取得
		foreach (Button button in GameObject.FindObjectsOfType<Button>()) {
			
			//Buttonが付いているのにButtonAssistantが付いていないのがあればエラー
			if(button.gameObject.GetComponent<ButtonAssistant>() == null){
				Debug.LogWarning ("There isn't ButtonAssistant in " + button.gameObject.name);
			}

		}
	}

}