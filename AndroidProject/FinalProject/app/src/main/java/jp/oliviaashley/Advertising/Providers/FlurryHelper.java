package jp.oliviaashley.Advertising.Providers;

import android.app.Activity;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryAgentListener;

import jp.oliviaashley.Twins.R;

/**
 * Library dependencies:
 * FlurryAnalytics_5.3.0.jar
 * AndroidManifest.xml:
 * <uses-permission android:name="android.permission.INTERNET"/>
 * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 * <p/>
 * Optional on AndroidManifest.xml:
 * <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
 * <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
 *
 * @author Aaron Santin - 2014
 * @version 1.0
 */
public class FlurryHelper {
    private static final String TAG = "FlurryHelper";

    public static void doOnCreate(Activity activity) {
        Log.v(TAG, "doOnCreate()");

        new FlurryAgent.Builder()
    			.withListener(new FlurryAgentListener() {

    				@Override
    				public void onSessionStarted() {
    					// TODO Auto-generated method stub
    					Log.v(TAG, "FlurryAgentListener onSessionStarted()");
    				}
    			})
    			.withLogEnabled(true)
    			.withLogLevel(Log.INFO)
    			.withContinueSessionMillis(5000L)
    			.withCaptureUncaughtExceptions(true)
    			.withPulseEnabled(true)
    			.build(activity, activity.getResources().getString(R.string.Flurry_APIKey));
    }

    /**
     * Put this method on your activity onStart method to initialize the Flurry
     * provider
     *
     * @param activity The main activity
     */
    public static void doOnStart(Activity activity) {
        Log.v(TAG, "doOnStart()");
    }

    /**
     * Put this method on your activity onStop method to stop the Flurry
     * provider
     *
     * @param activity The main activity
     */
    public static void doOnStop(Activity activity) {
        Log.v(TAG, "doOnStop()");
    }

    /**
     * Personalized report to put on Native Interface Methods
     *
     * @param count count
     */
    public static void reportFlurryWithGameCount(int count) {
        Log.v(TAG, "reportFlurryWithGameCount:" + count);
        String strEvent = "PLAY_COUNT:" + count;
        if (count <= 10) {
            FlurryAgent.logEvent(strEvent);
        } else if (count <= 100 && count % 10 == 0) {
            FlurryAgent.logEvent(strEvent);
        } else if (count <= 1000 && count % 100 == 0) {
            FlurryAgent.logEvent(strEvent);
        } else if (count <= 10000 && count % 1000 == 0) {
            FlurryAgent.logEvent(strEvent);
        } else if (count <= 100000 && count % 10000 == 0) {
            FlurryAgent.logEvent(strEvent);
        }
    }

    public static void reportFlurryWithEvent(String eventName) {
        Log.v(TAG, "reportFlurryWithEvent:" + eventName);
        FlurryAgent.logEvent(eventName);
    }

}
