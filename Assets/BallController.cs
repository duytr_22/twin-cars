﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : SingletonMonoBehaviour<BallController>
{
	[SerializeField]
	UILabel _numberLbl, _stageNumberLbl;

	private List<BallSelectButtonAssistant> _list = new List<BallSelectButtonAssistant> ();

	public List<BallSelectButtonAssistant> BallList {
		get { return _list; }
		set { _list = value; }
	}


	private List<StageSelectButtonAssistant> _stageList = new List<StageSelectButtonAssistant> ();

	public List<StageSelectButtonAssistant> StageList {
		get { return _stageList; }
		set { _stageList = value; }
	}

	[SerializeField]
	TweenPosition _ballGroup, _stageGroup;

	[SerializeField]
	TabBallButtonAssistant _tabBall;

	[SerializeField]
	TabStageButtonAssistant _tabStage;

	int _currentIdx = 0;

	Vector3 start, end;
	bool isMoving = false;

	// Use this for initialization
	void Start ()
	{
		int idx = 1;

		for (int i = 2; i < 10; i++) {

			if (PlayerPrefs.GetInt (string.Format (Define.BALL_UNLOCK, i)) >= 1) {

				idx++;
			}
		}

		_numberLbl.text = idx.ToString ();

		idx = 1;

		for (int i = 2; i < 6; i++) {

			if (PlayerPrefs.GetInt (string.Format (Define.STAGE_UNLOCK, i)) >= 1) {

				idx++;
			}
		}

		_stageNumberLbl.text = idx.ToString ();
	}

	public void CheckShowIcon ()
	{
		_tabStage.CheckShowIcon ();
		_tabBall.CheckShowIcon ();
	}

	// Update is called once per frame
	public void UncheckAll ()
	{
		foreach (BallSelectButtonAssistant item in _list) {

			item.Uncheck ();
		}
	}

	public void UncheckAllStage ()
	{
		foreach (StageSelectButtonAssistant item in _stageList) {

			item.Uncheck ();
		}
	}

	public void SwipeLeft ()
	{
		ShowTab (1);
	}

	public void SwipeRight ()
	{
		ShowTab (0);
	}

	public void ShowTab (int idx)
	{
		if (_currentIdx == idx)
			return;
		
		isMoving = true;

		if (_currentIdx == 0) {

			_ballGroup.PlayForward ();
			_stageGroup.PlayForward ();

			_tabBall.SelectAnim ();
			_tabStage.SelectAnim ();

		} else {
			
			_ballGroup.PlayReverse ();
			_stageGroup.PlayReverse ();

			_tabBall.UnselectAnim ();
			_tabStage.UnselectAnim ();
		}

		Invoke ("EndAnimation", _ballGroup.duration);

		_currentIdx = idx;
	}

	void EndAnimation ()
	{
		isMoving = false;
	}
}
