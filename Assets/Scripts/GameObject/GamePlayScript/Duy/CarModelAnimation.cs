﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CarModelAnimation : MonoBehaviour
{
	public enum CurveState
	{
		None,
		UpCurve,
		DownCurve
	}

	[SerializeField] CurveState _state;
	[SerializeField] CarCrashingPhysiscsBehavior _carPhysicsBehavior;
	[SerializeField] AnimationCurve _curve;
	// [SerializeField] TimelineSystem _startingTimeline;
	// [SerializeField] Animator _carChildAnimator;
	[SerializeField] float _lerpSpeed = 0.2f;
	[SerializeField] float _minRest = 2, _maxRest = 3;
	[SerializeField] float _maxDistance = 0.75f;
	[SerializeField] ParticleSystem _sparkleFXLeft, _sparkleFXRight;

	CarsRenderPatternController _patternController;
	bool _isFxPlayin;
	float _time;
	Vector3 _targetPosition;
	Vector3 _startingPosition;
	static bool _isStop;
	static List<CarsRenderPatternController> _allPatternControllerAttached;

	void Awake()
	{
		_patternController = GetComponent<CarsRenderPatternController>();
	}

	void OnEnable()
	{
		_carPhysicsBehavior.onCollisionEnterEvent += OnCarCollisionEnter;
		
		if (_allPatternControllerAttached == null)
			_allPatternControllerAttached = new List<CarsRenderPatternController>();
		
		//add all pattern controller in both left and right players
		_allPatternControllerAttached.Add(_patternController);
	}

	void OnDisable()
	{
		_carPhysicsBehavior.onCollisionEnterEvent -= OnCarCollisionEnter;
		_isStop = false;
		_allPatternControllerAttached.Clear();
	}

	private void OnCarCollisionEnter()
	{
		_isStop = true;
		foreach (var item in _allPatternControllerAttached)
			item.StopRenderAnimatior();
		VFXSystem.Instance.StopAllSmokeFX();
	}

	void Start()
	{
		TimelineSystem.onTimelineCompleted += () =>
		{
			_startingPosition = transform.localPosition;
			_targetPosition = transform.localPosition + transform.forward.normalized * _maxDistance;
			StartCoroutine(Animated());
		};
	}

	private IEnumerator Animated()
	{
		//init value
		var dist = (_targetPosition - _startingPosition).magnitude;
		var targetTime = 0f;
		var isRest = true;
		var restTime = Random.Range(_minRest, _maxRest);

		var direction = 1f; //forward

		while (_isStop == false)
		{
			if (isRest == false)
			{
				_time = Mathf.PingPong(targetTime * _lerpSpeed, 1);
				targetTime += Time.deltaTime;

				var value = _curve.Evaluate(_time);
				var diff = dist * value;
				transform.localPosition = _startingPosition + transform.forward.normalized * diff * direction;

				if (value < 1 && _time < 0.5f)
					_state = CurveState.UpCurve;
				else if (_time > 0.5f)
					_state = CurveState.DownCurve;

				if (direction == 1 && _state == CurveState.UpCurve
				    || direction == -1 && _state == CurveState.DownCurve)
				{
					if (_isFxPlayin == false)
					{
						_isFxPlayin = true;
						_sparkleFXLeft.Play();
						_sparkleFXRight.Play();
					}
				}

				if (direction == 1 && _state == CurveState.DownCurve
				    || direction == -1 && _state == CurveState.UpCurve)
				{
					if (_isFxPlayin == true)
					{
						_isFxPlayin = false;
						_sparkleFXLeft.Stop();
						_sparkleFXRight.Stop();
					}
				}
			}

			if (_time + Time.deltaTime > 1f && isRest == false)
			{
				isRest = true;
				restTime = Random.Range(_minRest, _maxRest);
				targetTime = 0;
				direction = Random.value < 0.5f ? 1 : -0.6f; //to make move backward is a little bit less than forward
				_state = CurveState.None;
				_isFxPlayin = false;
				_sparkleFXLeft.Stop();
				_sparkleFXRight.Stop();
			}

			if (isRest == true)
			{
				restTime -= Time.deltaTime;
				if (restTime < 0)
					isRest = false;
			}


			yield return new WaitForEndOfFrame();
		}
	}
}