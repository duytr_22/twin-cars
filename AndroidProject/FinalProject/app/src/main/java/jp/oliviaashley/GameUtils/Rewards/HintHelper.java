package jp.oliviaashley.GameUtils.Rewards;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import jp.oliviaashley.Social.TwitterOAuthHelper;
import jp.oliviaashley.Social.TwitterOAuthLogInActivity;
import jp.oliviaashley.Twins.R;


/**
 * ヒントのノルマ選択し、ノルマ達成の結果を返す
 * ・ツイートでシェア
 * ・動画リワード
 */
public class HintHelper {
    private static final String TAG = "HintHelper";

    private static Activity mActivity = null;
    private static String mFilePath = null;
    private static int mCurrentStage = 0;

    //Twitter
    public static final int REQUEST_CODE_TWITTER = 1001;
    public static TwitterOAuthHelper tweet_hlp;
    public static String tweet = "";

    //Twitter ログイン済みかどうか


    /**
     * (Java -> JNI -> C++)ヒントを得るためのノルマを達成したかの結果
     *
     * @param result ... true:ノルマを達成, false:ノルマ未達成(キャンセル・中断 etc)
     */
    private static native void hintNormCompleted(boolean result);

    //ヒントのノルマを達成したかどうか
    public static void didNormCompleted(boolean result) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);

        if (result) {
            // アラートダイアログのタイトルを設定します
            alertDialogBuilder.setTitle("ヒントを獲得");

            // アラートダイアログのメッセージを設定します
            alertDialogBuilder.setMessage("ありがとうございます！\n" + "ヒントを表示します。"
                    + "\n\n" + "※ステージの初期状態からスタートします"
            );

            alertDialogBuilder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //ヒントを得るためのノルマを達成したことを通知
                            hintNormCompleted(true);
                        }
                    });

            alertDialogBuilder.setCancelable(false);
        } else {
            // アラートダイアログのタイトルを設定します
            alertDialogBuilder.setTitle("ヒントの獲得に失敗");

            // アラートダイアログのメッセージを設定します
            alertDialogBuilder.setMessage("ヒントの獲得に失敗しました。");

            alertDialogBuilder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //ヒントを得るためのノルマを達成しなかったことを通知
                            hintNormCompleted(false);
                        }
                    });

            alertDialogBuilder.setCancelable(false);
        }

        alertDialogBuilder.show();
    }

    //ヒントの確認「いらない」「ツイートでシェアして取得」「動画広告を見て取得」
    public static void showHintAlert(Activity activity, String filePath, int currentStage) {
        Log.d(TAG, "showHintAlert()");

        mActivity = activity;
        mFilePath = filePath;
        mCurrentStage = currentStage;

        //Tweet文章
        {
            final String strAppName = activity.getResources().getString(R.string.app_name).replaceAll(" ", "");
            //final String strGame = activity.getResources().getString(R.string.share_utils_android_game);

            tweet = "だれか、この問題教えて！！！\n"
                    + "Androidゲーム 「" + strAppName + " " + "ステージ" + mCurrentStage + "」\n"
                    + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() + "\n"
                    + "#" + strAppName;
        }


        if (activity != null) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);

                    // アラートダイアログのタイトルを設定します
                    alertDialogBuilder.setTitle("ヒントをもらう？");

//MARK: setItems との併用は不可能
//					// アラートダイアログのメッセージを設定します
//					alertDialogBuilder.setMessage("※ステージの初期状態からスタートします");

                    //動画リワード 可能かどうかで 2種類
                    //if (!AMoAdVideoAdHelper.isLoadedTrigger())
                    {
                        CharSequence[] items = {
                                "シェアしてヒントをもらう",
                                "いらない",
                        };

                        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case (0): {
                                        //Twitter投稿
                                        launchTwitter();
                                        break;
                                    }
                                    case (1): {
                                        //ヒントを得るためのノルマを達成しなかったことを通知
                                        hintNormCompleted(false);
                                        break;
                                    }
                                }
                            }
                        };

                        alertDialogBuilder.setItems(items, listener);
                    }
//					else
//					{
//						CharSequence[] items = {
//								"シェアしてヒントをもらう",
//								"動画を見てヒントをもらう",
//								"いらない",
//						};
//
//						DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener(){
//
//							@Override
//							public void onClick(DialogInterface dialog, int which) {
//								switch (which)
//								{
//									case (0):
//									{
//										//Twitter投稿
//										HintHelper.launchTwitter();
//										break;
//									}
//									case (1):
//									{
//										//動画リワード
//										HintHelper.onTriggerVideo();
//										break;
//									}
//									case (2):
//									{
//										//ヒントを得るためのノルマを達成しなかったことを通知
//										hintNormCompleted(false);
//										break;
//									}
//								}
//							}
//						};
//
//						alertDialogBuilder.setItems(items, listener);
//					}

                    // アラートダイアログのキャンセルが可能かどうかを設定
                    alertDialogBuilder.setCancelable(false);

                    //ダイアログ
                    {
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        alertDialog.setCanceledOnTouchOutside(false);

                        // アラートダイアログを表示します
                        alertDialog.show();
                    }
                }
            });
        }
    }


    //動画広告
    private static void onTriggerVideo() {
        Log.d(TAG, "onTriggerVideo()");
    }


    //Twitterリワード
    private static void launchTwitter() {
        Log.d(TAG, "launchTwitter()");
        if (tweet_hlp == null) {
            tweet_hlp = new TwitterOAuthHelper(mActivity);
        }

        Activity activity = mActivity;
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    try {
                        //MARK: Tweet文章は showHintAlert で最初に生成

                        if (tweet_hlp.verify_logindata()) {
                            //Twitter投稿
                            final boolean result = tweet_hlp.Send_Tweet(tweet, mFilePath);

                            //結果をゲーム側に返す
                            HintHelper.didNormCompleted(result);
                        } else {
                            //ログイン処理へ
                            Intent i = new Intent(mActivity, TwitterOAuthLogInActivity.class);
                            i.putExtra("URL", tweet_hlp.get_AuthenticationURL());

                            mActivity.startActivityForResult(i, REQUEST_CODE_TWITTER);
                        }
                    } catch (Exception e) {
                        //結果をゲーム側に返す
                        HintHelper.didNormCompleted(false);

                        e.printStackTrace();
                        Log.d(TAG, "Twitter起動失敗 Error");
                    }
                }
            });
        }
    }


    public static void onActivityResult(int request, int response, Intent data) {
        if (!(tweet_hlp == null || tweet_hlp.verify_logindata())) {
            final boolean result = tweet_hlp.onActivityResult(request, response, data, tweet, mFilePath);
            HintHelper.didNormCompleted(result);

            tweet_hlp = null;
        }
    }
}
