//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  スプラッシュ(インターステイシャル)広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "CommonConfig.h"

@interface InterstitialAdController : NSObject{
    //ランキング表示時に表示するスプラッシュか
    BOOL _isRankInterstitial;
}

-(BOOL)canShow:(int)playCount;

-(id)init;
-(void)showInterstitialAd:(int)playCount;
-(void)setRankType:(BOOL)isRankInterstitial;

@end
