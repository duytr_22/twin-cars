﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWidgetAdjust : MonoBehaviour 
{

	[SerializeField] UIWidget _widget;
	[SerializeField] float _value = 50.0f;

	// Use this for initialization
	void Start () 
	{
//		_widget.leftAnchor.target = _target;
//		_widget.rightAnchor.target = _target;
//		_widget.topAnchor.target = _target;
//		_widget.bottomAnchor.target = _target;
		
		//StartCoroutine (AdjustPositionCourotine ());
		if (Define.IsIphoneX ()) 
		{
			_widget.topAnchor.Set (1, _widget.topAnchor.absolute - _value);
			_widget.bottomAnchor.Set (1, _widget.bottomAnchor.absolute - _value);
		}
	}

//	IEnumerator AdjustPositionCourotine()
//	{
//		yield return new WaitForEndOfFrame();
//
//
//	}

}
