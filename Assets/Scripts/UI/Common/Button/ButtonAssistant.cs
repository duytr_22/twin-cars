﻿//  ButtonAssistant.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.18.

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(UIButton))]

/// <summary>
/// ボタンの補助をするクラス
/// </summary>
public class ButtonAssistant : MonoBehaviour
{
	protected Collider _collider;
	protected UIButton _button;
	protected UISprite _image;

	//=================================================================================
	//初期化
	//=================================================================================

	protected virtual void Awake ()
	{
		//ボタンを取得
		_button = GetComponent<UIButton> ();
		_collider = GetComponent<Collider> ();
		_image = GetComponent<UISprite> ();
	}

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	void OnClick ()
	{
		if (_button.enabled) {

			SEManager.Instance.PlaySE (AudioName.SE_BUTTON);

			OnClickButton ();
		}
	}

	protected virtual void OnClickButton ()
	{

	}

	public virtual void DisableButton ()
	{
		_button.enabled = false;
		_button.defaultColor = _button.disabledColor;
	}

	public virtual void EnableButton ()
	{
		_button.enabled = true;
		_button.defaultColor = Color.white;
	}

	protected void UpdateButton (string name)
	{
		_button.hoverSprite = name;
		_button.normalSprite = name;
		_button.pressedSprite = name;
	}

}