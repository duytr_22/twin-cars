package jp.oliviaashley.Social;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jp.oliviaashley.Twins.R;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;


public class TwitterOAuthHelper {
    private static final String TAG = "TwitterOAuthHelper";

    private final Twitter twitter;
    private RequestToken mRequestToken = null;
    public int TWITTER_AUTH = 1;

    // Shared Preferences Name
    public static final String SHARED_PREF_NAME = "twitter";

    // Shared Preferences Name RecordS.
    public static final String TW_ACCTOKEN = "twitter_access_token2";
    public static final String TW_ACCTOKEN_SECRET = "twitter_access_token_secret2";

    //内部変数
    private final SharedPreferences settings;
    private final Activity activity;


    public TwitterOAuthHelper(Activity activity) {
        this.activity = activity;
        this.settings = activity.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        this.mRequestToken = null;

        final String consumerKey = activity.getString(R.string.twitter_consumer_key);
        final String consumerSecret = activity.getString(R.string.twitter_consumer_secret);
        final String callbackURL = activity.getString(R.string.twitter_callback_url);

        twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(consumerKey, consumerSecret);

        try {
            mRequestToken = twitter.getOAuthRequestToken(callbackURL);
        } catch (TwitterException e) {
            Log.d(TAG, "Init request token failed!");
            e.printStackTrace();
        }
    }

    public void Store_OAuth_verifier(String OAuth) {
        AccessToken at;
        try {
            at = twitter.getOAuthAccessToken(mRequestToken, OAuth);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(TW_ACCTOKEN, at.getToken());
            editor.putString(TW_ACCTOKEN_SECRET, at.getTokenSecret());
            editor.apply();
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    public String get_AuthenticationURL() {
        if (mRequestToken == null) {
            try {
                String tokenUrl = activity.getString(R.string.twitter_callback_url);
                mRequestToken = twitter.getOAuthRequestToken(tokenUrl);
            } catch (TwitterException e) {
                Log.d(TAG, "Init request token failed!");
                e.printStackTrace();
            }
        }
        return mRequestToken.getAuthenticationURL();
    }


    public void Logoff() {
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(TW_ACCTOKEN);
        editor.remove(TW_ACCTOKEN_SECRET);
        editor.apply();
    }

    public boolean verify_logindata() {

        if (settings.getString(TW_ACCTOKEN, null) != null) {
            Log.d(TAG, "shrpref.isExist(TW_ACCTOKEN)-> true");

            if (settings.getString(TW_ACCTOKEN_SECRET, null) != null) {
                Log.d(TAG, "shrpref.isExist(TW_ACCTOKEN_SECRET)-> true");

                return true;
            } else {

                Log.d(TAG, "shrpref.isExist(TW_ACCTOKEN_SECRET)-> false");
                return false;
            }
        } else {
            Log.d(TAG, "shrpref.isExist(TW_ACCTOKEN)-> false");
            return false;
        }
    }


    // テキストのみTwitter投稿
    public boolean Send_Tweet(String tweet_text) {
        //画像無しで投稿
        return Send_Tweet(tweet_text, null);
    }

    // 画像付きTwitter投稿
    public boolean Send_Tweet(String tweet_text, String image_path) {
        Log.d(TAG, "Tweet Send_Tweet ... started");

        String accessToken = settings.getString(TW_ACCTOKEN, null);
        String accessTokenSecret = settings.getString(TW_ACCTOKEN_SECRET, null);

        Log.e(TAG, "accessToken= " + accessToken);
        Log.e(TAG, "accessTokenSecret= " + accessTokenSecret);

        if ((accessToken != null) && (accessTokenSecret != null)) {
            Configuration conf = new ConfigurationBuilder()
                    .setOAuthConsumerKey(activity.getString(R.string.twitter_consumer_key))
                    .setOAuthConsumerSecret(activity.getString(R.string.twitter_consumer_secret))
                    // datos obtenidos del shared pref
                    .setOAuthAccessToken(accessToken)
                    .setOAuthAccessTokenSecret(accessTokenSecret).build();

            // usamos lo seteado anteriormente para obtener una instancia para
            // autenticacion OAuth.
            // creamos objeto para acceder a twitter.
            Twitter t = new TwitterFactory(conf).getInstance();

            try {
                final StatusUpdate status = new StatusUpdate(tweet_text);

                if (image_path != null) {
                    try {
                        //assets
                        AssetManager assetManager = activity.getAssets();
                        InputStream ims;
                        ims = assetManager.open(image_path);

                        //resizedPicture from originalPicture
                        Bitmap originalPicture = BitmapFactory.decodeStream(ims);
                        int height = originalPicture.getHeight();
                        int width = originalPicture.getWidth();
                        Matrix matrix = new Matrix();
                        matrix.setScale(0.5f, 0.5f);
                        Bitmap resizedPicture = Bitmap.createBitmap(originalPicture, 0, 0, width, height, matrix, true);

                        //保存先
                        File savePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                        File saveFile = new File(savePath, "screenshot.jpeg");
                        if (!savePath.exists()) {
                            if(!savePath.mkdir()) {
                                Log.d(TAG, "Can't create directory to save image.");
                                return false;
                            }
                        }

                        // ファイルの作成
                        FileOutputStream fos;
                        try {
                            fos = new FileOutputStream(saveFile);
                            resizedPicture.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.flush();
                            fos.close();
                        } catch (IOException e) {
                            Log.e("TwitterShare", "IOE:" + e.toString());
                        }

                        status.media(saveFile);
                    } catch (IOException e) {
                        //writable path (...を想定)
                        //status.media(new File(image_path));

                        //resizedPicture from originalPicture
                        Bitmap originalPicture = BitmapFactory.decodeFile(image_path);
                        int height = originalPicture.getHeight();
                        int width = originalPicture.getWidth();
                        Matrix matrix = new Matrix();
                        matrix.setScale(0.5f, 0.5f);
                        Bitmap resizedPicture = Bitmap.createBitmap(originalPicture, 0, 0, width, height, matrix, true);

                        //保存先
                        File savePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                        File saveFile = new File(savePath, "screenshot.jpeg");
                        if (!savePath.exists()) {
                            if(!savePath.mkdir()) {
                                Log.d(TAG, "Can't create directory to save image.");
                                return false;
                            }
                        }

                        // ファイルの作成
                        FileOutputStream fos;
                        try {
                            fos = new FileOutputStream(saveFile);
                            resizedPicture.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.flush();
                            fos.close();
                        } catch (IOException er) {
                            Log.e("TwitterShare", "IOE:" + er.toString());
                        }

                        status.media(saveFile);
                    }
                }

                // Twitter投稿
                t.updateStatus(status);
            }
            // error
            catch (TwitterException e) {
                //MARK: 覚え書き
                //何時間以内か分かりませんが、
                //ある一定時間内で同じ内容のツイートを投稿した場合は、エラーになります。

                Log.d(TAG, "Error Tweet Not Sent!! ... FAIL");
                e.printStackTrace();

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "Tweetに失敗しました", Toast.LENGTH_SHORT).show();
                    }
                });

                return false;
            }

            Log.d(TAG, "Tweet Sent!! ... OK");
            return true;
        } else {
            Log.d(TAG, "Error Tweet Not Sent!! ... FAIL");

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, "Tweetに失敗しました", Toast.LENGTH_SHORT).show();
                }
            });

            return false;
        }
    }

    // ログインし、Twitter投稿
    public boolean onActivityResult(@SuppressWarnings("UnusedParameters") int request, int response, Intent data, String tweet, String imagefilepath) {
        Log.d(TAG, "onActivityResult() ... Start");

        // 処理結果 ログインしてTwitter投稿まで出来たかどうか
        boolean result = false;

        do {
            if (response != Activity.RESULT_OK) {
                Log.d(TAG, "onActivityResult() ... work break!! not response :" + response);
                break;
            }

            final String oauthVerifier = (String) data.getExtras().get(TwitterOAuthLogInActivity.TWITTERWEB_VERIFIER);
            // Log.d(TAG, "onActivityResult() data.getExtras(): " + oauthVerifier);
            if (oauthVerifier == null) {
                Log.d(TAG, "onActivityResult() ... work break!! oauthVerifier is null");
                break;
            }

            // OAuthデータを記録
            // Grabamos el valor de oauthVerifier en el shared preferences
            {
                Log.d(TAG, "oauthVerifier -> " + oauthVerifier);
                Store_OAuth_verifier(oauthVerifier);
            }

            // Tweet文章がない
            if (tweet == null) {
                Log.d(TAG, "onActivityResult() ... work break!! tweet-text is null");
                break;
            }

            // Twitter投稿
            if (!Send_Tweet(tweet, imagefilepath)) {
                // 問題があれば抜ける
                break;
            }

            Log.d(TAG, "onActivityResult() ... tweet!!");

            // ログインし、Twitter投稿まで出来た
            result = true;
        } while (false);

        return result;
    }
}
