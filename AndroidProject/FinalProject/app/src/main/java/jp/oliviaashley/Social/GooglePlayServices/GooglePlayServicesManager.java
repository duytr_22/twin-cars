package jp.oliviaashley.Social.GooglePlayServices;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.NonNull;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.example.games.basegameutils.GameHelper;
import com.tenjin.android.TenjinSDK;
import com.unity3d.player.UnityPlayer;

import org.json.JSONObject;

import java.util.ArrayList;

import jp.oliviaashley.Twins.R;

public class GooglePlayServicesManager {
    private final static String TAG = "GooglePlayServicesMng";
    private final static String SHARED_PREFERENCES_FILE_NAME = "GooglePlayServicesManager";
    private final static String SHARED_PREFERENCES_KEY_LOGIN_IGNORED = "SharedPreferencesKeyLoginIgnored";

    private final static int REQUEST_ACHIEVEMENTS = 111;
    private final static int REQUEST_LEADERBOARDS = 222;
    private final static int REQUEST_LEADERBOARD = 333;

    private static GameHelper mHelper;
    private static final int mRequestedClients = GameHelper.CLIENT_GAMES;
    private static DefaultOutbox mOutbox;

    private static final boolean mDebugLog = true;

    private static Activity activity = null;

    /*IAP*/
    private static IInAppBillingService mService;
    private static ServiceConnection mServiceConn;
    /*IAP*/

    public static void doOnCreate(Activity activity) {
        Log.d(TAG, "doOnCreate()");

        GooglePlayServicesManager.activity = activity;

        if (mHelper == null) {
            mHelper = new GameHelper(activity, mRequestedClients);
            mHelper.enableDebugLog(mDebugLog);
        }

        try {
            mHelper.setup(getGameHelperListener());
            if (mHelper.isSignedIn()) {
                Log.d(TAG, "Already signed in YO!!");
            } else {
                if (!GooglePlayServicesManager.activity.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE).getBoolean(SHARED_PREFERENCES_KEY_LOGIN_IGNORED, false)) {
                    Log.d(TAG, "Trying to show the login window...");
                    mHelper.beginUserInitiatedSignIn();
                } else {
                    Log.d(TAG, "The login was skipped the first time...");
                    mHelper.setConnectOnStart(false);
                }
            }
        } catch (IllegalStateException e) {
            // don't do it twice...
            Log.e(TAG, "IllegalStateException:" + e.toString());
            return;
        }

        mOutbox = new DefaultOutbox();
        mOutbox.loadLocal(activity);

         /*IAP*/
        mServiceConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                mService = IInAppBillingService.Stub.asInterface(service);
            }
        };

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        activity.bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
        /*IAP*/
    }

    public static void doOnStart(Activity activity) {
        Log.d(TAG, "doOnStart()");

        mHelper.onStart(activity);
    }

    public static void doOnStop(Activity activity) {
        Log.d(TAG, "doOnStop()");

        mHelper.onStop();
        mOutbox.saveLocal(activity);
    }

    public static void doOnDestroy(Activity activity) {
        Log.d(TAG, "doOnDestroy()");

        mHelper = null;
        mOutbox = null;

        /*IAP*/
        if (mService != null) {
            activity.unbindService(mServiceConn);
        }
        /*IAP*/
    }

    public static void doOnActivityResult(int request, int response, Intent data) {
        Log.d(TAG, "doOnActivityResult()");

        mHelper.onActivityResult(request, response, data);
    }

    private static GoogleApiClient getApiClient() {
        return mHelper.getApiClient();
    }

    public static boolean isSignedIn() {
        return mHelper.isSignedIn();
    }

    public static void beginUserInitiatedSignIn() {
        Log.d(TAG, "beginUserInitiatedSignIn()");

        mHelper.beginUserInitiatedSignIn();
    }

    public static void signOut() {
        mHelper.signOut();
    }

    public static void showAlert(String message) {
        mHelper.makeSimpleDialog(message).show();
    }

    public static void showAlert(String title, String message) {
        mHelper.makeSimpleDialog(title, message).show();
    }

    public static String getInvitationId() {
        return mHelper.getInvitationId();
    }

    public static void reconnectClient() {
        mHelper.reconnectClient();
    }

    public static boolean hasSignInError() {
        return mHelper.hasSignInError();
    }

    public static GameHelper.SignInFailureReason getSignInError() {
        return mHelper.getSignInError();
    }

    private static GameHelper.GameHelperListener getGameHelperListener() {

        return new GameHelper.GameHelperListener() {

            @Override
            public void onSignInSucceeded() {
                Log.v(TAG, "The sign in fuction worked!");
                GooglePlayServicesManager.activity.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE).edit().putBoolean(SHARED_PREFERENCES_KEY_LOGIN_IGNORED, false).apply();
            }

            @Override
            public void onSignInFailed() {
                Log.i(TAG, "Login ignored or failed");
                GooglePlayServicesManager.activity.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE).edit().putBoolean(SHARED_PREFERENCES_KEY_LOGIN_IGNORED, true).apply();
            }
        };

    }

    public static void showAchievements(Activity activity) {
        if (isSignedIn()) {
            activity.startActivityForResult(
                    Games.Achievements.getAchievementsIntent(getApiClient()),
                    REQUEST_ACHIEVEMENTS);
        } else {
            showAlert(activity.getString(R.string.achievements_not_available));
        }
    }

    public static void showLeaderboards(final Activity activity) {
        if (isSignedIn()) {
            activity.startActivityForResult(
                    Games.Leaderboards.getAllLeaderboardsIntent(getApiClient()),
                    REQUEST_LEADERBOARDS);
        } else {
            beginUserInitiatedSignIn();
        }
    }

    public static void showWorldLeaderboard(final Activity activity) {
        if (isSignedIn()) {
            try {
                activity.startActivityForResult(
                        Games.Leaderboards.getAllLeaderboardsIntent(getApiClient()),
                        REQUEST_LEADERBOARD);
            } catch (Exception e) {
                Log.w(TAG, "Really not signed in");
                e.printStackTrace();
                beginUserInitiatedSignIn();
            }
        } else {
            beginUserInitiatedSignIn();
        }
    }

    public static void pushAcomplishements(Activity activity, long score, int mode) {
        Log.d(TAG, "pushAcomplishements (activity, " + score + ", " + mode + ")");
        if (!isSignedIn()) {
			// can't push to the cloud, so save locally
			Log.v(TAG,"Not signed in ");
			mOutbox.saveLocal(activity, score);
			return;
		}

		try {
			mOutbox.saveLocal(activity, score);
			if (!mOutbox.isEmpty()) {
				int id;
				switch (mode){
				case 1:
					id = R.string.leaderboard_world_ranking_1;
					Games.Leaderboards.submitScore(getApiClient(),
							activity.getString(id), score);
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			Log.w(TAG, "Probably not really signed in");
			e.printStackTrace();
		}
    }

    public static void reportScore(Activity activity, String id, long score) {

        if (!isSignedIn()) {
            // can't push to the cloud, so save locally
            Log.v(TAG, "Not signed in ");
            mOutbox.saveLocal(activity, score);
            return;
        }

        try {
            mOutbox.saveLocal(activity, score);
            if (!mOutbox.isEmpty()) {
                Games.Leaderboards.submitScore(getApiClient(), id, score);
            }
        } catch (Exception e) {
            Log.w(TAG, "Probably not really signed in");
            e.printStackTrace();
        }
    }

    public static void getUserRanking(final Activity activity) {

        Games.Leaderboards.loadLeaderboardMetadata(getApiClient(), activity.getResources().getString(R.string.leaderboard_world_ranking_1), true).setResultCallback(new ResultCallback<Leaderboards.LeaderboardMetadataResult>() {
            @Override
            public void onResult(@NonNull Leaderboards.LeaderboardMetadataResult leaderboardMetadataResult) {

                Games.Leaderboards.loadCurrentPlayerLeaderboardScore(getApiClient()
                        , activity.getResources().getString(R.string.leaderboard_world_ranking_1)
                        , LeaderboardVariant.TIME_SPAN_ALL_TIME
                        , LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {

                    @Override
                    public void onResult(Leaderboards.LoadPlayerScoreResult loadPlayerScoreResult) {

                        if (loadPlayerScoreResult != null && loadPlayerScoreResult.getScore() != null) {

                            long rank = loadPlayerScoreResult.getScore().getRank();

                            UnityPlayer.UnitySendMessage("RankingButtonLabel", "OnUpdateRanking", rank + "");
                        }
                    }
                });
            }
        });
    }

    /*
    IAP
     */

    public static void buyItem(String itemID)
    {
        try {

            Bundle buyIntentBundle = mService.getBuyIntent(3, activity.getPackageName(), itemID, "inapp", "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");

            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

            activity.startIntentSenderForResult(pendingIntent.getIntentSender()
                    , activity.getResources().getInteger(R.integer.IAP_Request_Code)
                    , new Intent()
                    , Integer.valueOf(0)
                    , Integer.valueOf(0)
                    , Integer.valueOf(0));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void consumePurchase(final String token) {

        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {

                    mService.consumePurchase(3, activity.getPackageName(), token);

                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }

    public static void ReportPurchase(Activity activity)
    {

        // Report
         try {
            ArrayList<String> skuList = new ArrayList<String> ();
            skuList.add(activity.getString(R.string.Item_No_Ads));

            Bundle querySkus = new Bundle();

            querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

            Bundle skuDetails = mService.getSkuDetails(3,
                    activity.getPackageName(), "inapp", querySkus);

            int response = skuDetails.getInt("RESPONSE_CODE");

            if (response == 0) {

                ArrayList<String> responseList
                        = skuDetails.getStringArrayList("DETAILS_LIST");

                for (String thisResponse : responseList) {

                    JSONObject object = new JSONObject(thisResponse);

                    String sku = object.getString("productId");
                    String price = object.getString("price_amount_micros");
                    String currencyCode = object.getString("price_currency_code");

                    TenjinSDK.getInstance(activity, activity.getResources().getString(R.string.tenjin_ipa_key)).transaction(sku, currencyCode, 1, Double.parseDouble(price)/1000000);
                }
            }

        } catch (Exception e) {

             e.printStackTrace();
         }
    }

    public static boolean restoreItem()
    {
        try {

            Bundle ownedItems = mService.getPurchases(3, activity.getPackageName(), "inapp", null);

            int response = ownedItems.getInt("RESPONSE_CODE");

            if (response == 0) {

                ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String>  purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String>  signatureList = ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                String continuationToken = ownedItems.getString("INAPP_CONTINUATION_TOKEN");

                for (int i = 0; i < purchaseDataList.size(); ++i) {

                    String purchaseData = purchaseDataList.get(i);
                    JSONObject jo = new JSONObject(purchaseData);

                    String sku = ownedSkus.get(i);

                    Log.v("IAP", jo.getString("purchaseToken"));
                    Log.v("IAP", sku);

                    if(sku.equals(activity.getResources().getString(R.string.Item_No_Ads))) {

                        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean(activity.getString(R.string.remove_ads), true);
                        editor.commit();

                        UnityPlayer.UnitySendMessage("NativeManager", "PurchaseSuccess", "");

                        return true;
                    }
                }
            }

            return  false;

        } catch (Exception e) {

            e.printStackTrace();

            return  false;
        }
    }

    /*
    IAP
     */
}
