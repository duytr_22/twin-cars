﻿/// <summary>
/// レイヤーマスク番号を定数で管理するクラス
/// </summary>
public static class LayerMaskNo{

	  public const int BACK                  = 256;
	  public const int BACK1                 = 32768;
	  public const int CUBE                  = 4096;
	  public const int DEFAULT               = 1;
	  public const int EFFECT                = 512;
	  public const int END_POINT             = 16384;
	  public const int GAME_LAYER            = 1024;
	  public const int IGNORE_RAYCAST        = 4;
	  public const int LANE                  = 2048;
	  public const int NEW_DEFAULT_COLLISION = 2097152;
	  public const int NEW_LANE_COLLISION    = 1048576;
	  public const int PLAYER                = 8192;
	  public const int TRANSPARENT_FX        = 2;
	  public const int TUTORIAL              = 262144;
	  public const int UI                    = 32;
	  public const int UIBACK                = 65536;
	  public const int UIBACK_SCORE          = 131072;
	  public const int UITOP                 = 524288;
	  public const int WATER                 = 16;

}
