//
//  FiveRectangleAdController.h
//  Unity-iPhone
//
//
//

#import "RectangleAdController.h"
#import <ADG/ADGManagerViewController.h>

@interface AdGenerationRectangleAdController : RectangleAdController<ADGManagerViewControllerDelegate>

-(UIView *)prepareRectangleAd:(CGRect)frame;

@property (nonatomic, retain) ADGManagerViewController *banner;

-(void) reloadRectangleAd;
-(void) removeRectangleAd;
@end
