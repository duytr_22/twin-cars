﻿/// <summary>
/// タグ名を定数で管理するクラス
/// </summary>
public static class TagName{

	  public const string EDITOR_ONLY         = "EditorOnly";
	  public const string FINISH              = "Finish";
	  public const string GAME_CONTROLLER     = "GameController";
	  public const string MAIN_CAMERA         = "MainCamera";
	  public const string PLAYER              = "Player";
	  public const string RESPAWN             = "Respawn";
	  public const string TUTORIAL_PASS_POINT = "TutorialPassPoint";
	  public const string TUTORIAL_POINT      = "TutorialPoint";
	  public const string UICAMERA            = "UICamera";
	  public const string UNTAGGED            = "Untagged";

}
