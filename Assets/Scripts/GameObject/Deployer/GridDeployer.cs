﻿//  GridDeployer.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.21.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 子を格子状に並べるクラス
/// </summary>
public class GridDeployer : MonoBehaviour {

	//並べる次元
	private enum Dimension{
		x, y, z
	}
	[SerializeField]
	private Dimension _previousDimension = Dimension.x, _nextDimension = Dimension.y;

	//中心
	public enum PivotType{
		Center, First
	}
	[SerializeField]
	private PivotType _previousPivot = PivotType.Center, _nextPivot = PivotType.Center;

	public PivotType PreviousPivot{
		set{_previousPivot = value;}
	}
	public PivotType NextPivot{
		set{_nextPivot = value;}
	}

	//並べる間隔
	[SerializeField]
	private float _previousSpace = 1, _nextSpace = 1;

	public Vector2 Space{
		get{
			return new Vector2 (_previousSpace, _nextSpace);
		}
		set{
			_previousSpace = value.x;
			_nextSpace     = value.y;
		}
	}

	//並べる個数
	[SerializeField][Header("0 == Unlimit")]
	private int _limit;

	public int Limit{
		set{_limit = value;}
	}

	//=================================================================================
	//初期化
	//=================================================================================

	private void OnValidate (){

		//同じ方向に並べないように
		foreach (Dimension dimension in Enum.GetValues(typeof(Dimension))) {
			if(_previousDimension != _nextDimension){
				break;
			}
			_nextDimension = dimension;
		}

		//個数はマイナスにならないように
		_limit = Mathf.Max (0, _limit);

		Deploy ();
	}

	//=================================================================================
	//外部
	//=================================================================================

	/// <summary>
	/// 子を格子状に配置する
	/// </summary>
	[ContextMenu("Deploy")]
	public void Deploy(){

		//子を取得
		List<GameObject> _childList = new List<GameObject> ();
		foreach(Transform child in transform) {
			_childList.Add (child.gameObject);
		}

		//初期位置計算
		float previousInitialPos = -_previousSpace * (float)(_childList.Count - 1) / 2f;
		float nextinitialPos = 0;

		if (_limit != 0) {
			previousInitialPos = -_previousSpace * (Mathf.Min(_limit, _childList.Count) - 1f) / 2f;

			int secondLineNum = Mathf.CeilToInt((float)_childList.Count / (float)_limit);
			nextinitialPos = -_nextSpace * (float)(secondLineNum - 1f) / 2f;
		}

		if(_previousPivot == PivotType.First){
			previousInitialPos = 0;
		}
		if(_nextPivot == PivotType.First){
			nextinitialPos = 0;
		}

		//並び替え
		for (int i = 0; i < _childList.Count; i++) {
			
			//座標を計算
			int   previousLineNo = i;
			float previousLinePos = previousInitialPos;
			float nextLinePos     = nextinitialPos;

			if(_limit != 0){
				previousLineNo = i % _limit;
				nextLinePos += _nextSpace * (i / _limit);
			}

			previousLinePos += _previousSpace * previousLineNo;

			//座標設定
			Vector3 pos = Vector3.zero;

			pos = UpdatePosFromeDimension (pos, _previousDimension, previousLinePos);
			pos = UpdatePosFromeDimension (pos, _nextDimension,     nextLinePos);

			_childList [i].transform.localPosition = pos;
		}

	}

	//次元を指定して位置を設定
	private Vector3 UpdatePosFromeDimension(Vector3 pos, Dimension dimension, float value){
		if(dimension == Dimension.x){
			pos.x = value;
		}
		else if(dimension == Dimension.y){
			pos.y = value;
		}
		else if(dimension == Dimension.z){
			pos.z = value;
		}

		return pos;
	}

}