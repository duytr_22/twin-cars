﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct ColorBalance
{
	public int score;
	public int colorIndex;
}

public class BackgroundScript : MonoBehaviour
{

	private static BackgroundScript instance;

	public static BackgroundScript Instance {
		get {
			return instance;
		}
	}

	private void Awake ()
	{
		if (instance != null && instance != this) {
			Destroy (instance);
		} else {
			instance = this;
		}
	}

	public delegate void ColorBackChange (Color color);

	public event ColorBackChange OnBackChangeColor;
	public event ColorBackChange OnHazeChangeColor;
	public event ColorBackChange OnMountainChangeColor;
	public event ColorBackChange OnCloudChangeColor;

	public Camera renderCamera;

	[Space (10)]
	[Header ("==== Back Colors ====")]
	[Space (0)]
	public List<Color> backColors;
	[HideInInspector] public Color currentBackColor;
	[HideInInspector] public Color nextBackColor;
	[HideInInspector] public Color previousBackColor;

	[Space (10)]
	[Header ("==== Haze Colors ====")]
	[Space (0)]
	public List<Color> hazeColors;
	[HideInInspector] public Color currentHazeColor;
	[HideInInspector] public Color nextHazeColor;
	[HideInInspector] public Color previousHazeColor;

	[Space (10)]
	[Header ("==== Mountain Colors ====")]
	[Space (0)]
	public List<Color> moutainColors;
	[HideInInspector] public Color currentMountainColor;
	[HideInInspector] public Color nextMountainColor;
	[HideInInspector] public Color previousMountainColor;

	[Space (10)]
	[Header ("==== Cloud Colors ====")]
	[Space (0)]
	public List<Color> cloudColors;
	[HideInInspector] public Color currentCloudColor;
	[HideInInspector] public Color nextCloudColor;
	[HideInInspector] public Color previousCloudColor;

	[Space (10)]
	[Header ("==== Fog Colors ====")]
	[Space (0)]
	public List<Color> fogColors;
	[HideInInspector] public Color currentFogColor;
	[HideInInspector] public Color nextFogColor;
	[HideInInspector] public Color previousFogColor;

	[Space (10)]
	[Header ("==== Camera Background Colors ====")]
	[Space (0)]
	public List<Color> cameraColors;
	[HideInInspector] public Color currentCameraColor;
	[HideInInspector] public Color nextCameraColor;
	[HideInInspector] public Color previousCameraColor;


	[Space (10)]
	[Header ("==== Mountain Start Colors ====")]
	[Space (0)]
	public List<Color> mStartColors;
	[HideInInspector] public Color currentMStartColor;
	[HideInInspector] public Color nextMStartColor;
	[HideInInspector] public Color previousMStartColor;

	[Space (10)]
	[Header ("==== Mountain End Colors ====")]
	[Space (0)]
	public List<Color> mEndColors;
	[HideInInspector] public Color currentMEndColor;
	[HideInInspector] public Color nextMEndColor;
	[HideInInspector] public Color previousMEndColor;


	[HideInInspector] public int currentColorIndex = 0;

	[Space (10)]
	[Header ("====Balance Color ====")]
	[Space (0)]
	public List<ColorBalance> colorBalances;
	public int maxColorScoreLoop;
	public float changeColorDuration;
	private int colorScoreCount;
	private bool isChangeColor;
	private int colorStep;

	// Use this for initialization
	void Start ()
	{
		//assgin call back when score change(use to determite where color should change)
		if (GameManager.Instance.playMode == GameManager.PlayMode.PlayModeGame) {
			GameManager.Instance.OnChangeScore += UpdateScoreColor;
		} else {
			GameManager.Instance.OnChangeScore += UpdateScoreColor;
		}

		if (PlayerPrefs.GetInt (Define.CONTINUE_GAME, 0) == 1 && SceneNavigator.Instance.CurrentSceneName == SceneName.GAME) {

			currentColorIndex = PlayerPrefs.GetInt (Define.CURRENT_COLOR_INDEX, 0);

			isChangeColor = true;
		}

//		if (true) {
//
//			currentColorIndex = 5;
//
//			isChangeColor = true;
//		}

		PlayerPrefs.SetInt (Define.CURRENT_COLOR_INDEX, 0);

		// back color setup
		currentBackColor = backColors [currentColorIndex];
		previousBackColor = currentBackColor;
		nextBackColor = currentBackColor;

		// haze color setup
		currentHazeColor = hazeColors [currentColorIndex];
		previousHazeColor = currentHazeColor;
		nextHazeColor = currentHazeColor;

		// mountain color setup
		currentMountainColor = moutainColors [currentColorIndex];
		previousMountainColor = currentMountainColor;
		nextMountainColor = currentMountainColor;

		// cloud color setup
		currentCloudColor = cloudColors [currentColorIndex];
		previousCloudColor = currentCloudColor;
		nextCloudColor = currentCloudColor;

		// fog color setup
		// currentFogColor = fogColors [currentColorIndex];
		// previousFogColor = currentFogColor;
		// nextFogColor = currentFogColor;
		// RenderSettings.fogColor = currentFogColor;

		// camera color setup
		currentCameraColor = cameraColors [currentColorIndex];
		previousCameraColor = currentCameraColor;
		nextCameraColor = currentCameraColor;
		renderCamera.backgroundColor = currentCameraColor;

		colorStep = 1;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Time.timeScale == 0) {
			return;
		}
		this.UpdateColor ();
	}

	void UpdateColor ()
	{
		if (isChangeColor) {
			isChangeColor = false;
			this.StartCoroutine (this.ChangeBackColor ());
			this.StartCoroutine (this.ChangeHazeColor ());
//			this.StartCoroutine (this.ChangeMountainColor ());
			this.StartCoroutine (this.ChangeCloudColor ());
			this.StartCoroutine (this.ChangeFogColor ());
			this.StartCoroutine (this.ChangeCameraColor ());
		}
	}

	void UpdateScoreColor (int score)
	{
		colorScoreCount++;
				
		if (colorScoreCount >= maxColorScoreLoop) {
			
			colorScoreCount = 0;

			currentColorIndex += colorStep;

			PlayerPrefs.SetInt (Define.CURRENT_COLOR_INDEX, currentColorIndex);

			isChangeColor = true;

			if (currentColorIndex >= moutainColors.Count) {
				
				colorStep = 1;
				currentColorIndex = 0;
			}

			//back color
			previousBackColor = currentBackColor;
			nextBackColor = backColors [currentColorIndex];

			//haze color
			previousHazeColor = currentHazeColor;
			nextHazeColor = hazeColors [currentColorIndex];

			//cloud color
			previousCloudColor = currentCloudColor;
			nextCloudColor = cloudColors [currentColorIndex];

			//fog color
			previousFogColor = currentFogColor;
			nextFogColor = fogColors [currentColorIndex];

			//camera color
			previousCameraColor = currentCameraColor;
			nextCameraColor = cameraColors [currentColorIndex];

			DynamicBack[] dynamicBackArray = transform.GetComponentsInChildren<DynamicBack> ();
			foreach (DynamicBack dynamicBack in dynamicBackArray) {
				dynamicBack.ChangeTweenColorInMiddle ();
			}
		}
	}

	void UpdateColorWithBalance (int score)
	{
		for (int i = colorBalances.Count - 1; i >= 0; i--) {

			ColorBalance b = colorBalances [i];

			if (b.score <= score) {
				
				if (i != currentColorIndex) {

					currentColorIndex = i;
					isChangeColor = true;

					//back color
					previousBackColor = currentBackColor;
					nextBackColor = backColors [currentColorIndex];

					//haze color
					previousHazeColor = currentHazeColor;
					nextHazeColor = hazeColors [currentColorIndex];

					//cloud color
					previousCloudColor = currentCloudColor;
					nextCloudColor = cloudColors [currentColorIndex];

					//fog color
					previousFogColor = currentFogColor;
					nextFogColor = fogColors [currentColorIndex];

					//camera color
					previousCameraColor = currentCameraColor;
					nextCameraColor = cameraColors [currentColorIndex];

					DynamicBack[] dynamicBackArray = transform.GetComponentsInChildren<DynamicBack> ();
					foreach (DynamicBack dynamicBack in dynamicBackArray) {
						dynamicBack.ChangeTweenColorInMiddle ();
					}
				}

				break;
			}
		}
	}

	IEnumerator ChangeBackColor ()
	{
		float t = 0;
		while (t < changeColorDuration) {
			t += Time.deltaTime;
			float ratio = t / changeColorDuration;
			currentBackColor = Color.Lerp (previousBackColor, nextBackColor, ratio);
			this.OnBackChangeColor (currentBackColor);
			yield return null;
		}
		yield break;
	}

	IEnumerator ChangeHazeColor ()
	{
		float t = 0;
		while (t < changeColorDuration) {
			t += Time.deltaTime;
			float ratio = t / changeColorDuration;
			currentHazeColor = Color.Lerp (previousHazeColor, nextHazeColor, ratio);
			this.OnHazeChangeColor (currentHazeColor);
			yield return null;
		}
		yield break;
	}

	//	IEnumerator ChangeMountainColor() {
	//		float t = 0;
	//		while (t < changeColorDuration) {
	//			t += Time.deltaTime;
	//			float ratio = t / changeColorDuration;
	//			currentMountainColor = Color.Lerp (previousMountainColor, nextMountainColor, ratio);
	//			this.OnMountainChangeColor (currentMountainColor);
	//			yield return null;
	//		}
	//		yield break;
	//	}

	IEnumerator ChangeCloudColor ()
	{
		float t = 0;
		while (t < changeColorDuration) {
			t += Time.deltaTime;
			float ratio = t / changeColorDuration;
			currentCloudColor = Color.Lerp (previousCloudColor, nextCloudColor, ratio);

			if (OnCloudChangeColor != null)
				this.OnCloudChangeColor (currentCloudColor);
			
			yield return null;
		}
		yield break;
	}

	IEnumerator ChangeFogColor ()
	{
		float t = 0;
		while (t < changeColorDuration) {
			t += Time.deltaTime;
			float ratio = t / changeColorDuration;
			currentFogColor = Color.Lerp (previousFogColor, nextFogColor, ratio);
			RenderSettings.fogColor = currentFogColor;
			yield return null;
		}
		yield break;
	}

	IEnumerator ChangeCameraColor ()
	{
		float t = 0;
		while (t < changeColorDuration) {
			t += Time.deltaTime;
			float ratio = t / changeColorDuration;
			currentCameraColor = Color.Lerp (previousCameraColor, nextCameraColor, ratio);
			renderCamera.backgroundColor = currentCameraColor;
			yield return null;
		}
		yield break;
	}
}
