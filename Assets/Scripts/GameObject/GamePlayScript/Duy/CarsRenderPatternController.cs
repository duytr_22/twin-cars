﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using static CarsRenderPattern;

public class CarsRenderPatternController : MonoBehaviour
{
	[SerializeField] CarsRenderPattern _patternObject;
	PatternID _currentIndex;
	PatternData _data;
	GameObject _currentModel;
	Vector3 _startLocalPosition = new Vector3(0, -0.5f, 0);
	Animator _animator;
	Bounds _mainRendererBounds;

	void Awake()
	{
		_currentIndex = _patternObject.isPlayer ? Define.DEBUG_CAR_PATTERN_PLAYER : Define.DEBUG_CAR_PATTERN_ENEMY;
		_data = _patternObject.GetPattern(_currentIndex);

		if (_data != PatternData.Error)
		{
			_currentModel = Instantiate(_data.GetPrefab(), transform);
			_currentModel.transform.localPosition = _startLocalPosition;
			_animator = _currentModel.GetComponent<Animator>();

			var driverPrefab = _data.GetDriver();
			if (driverPrefab != null)
				_currentModel.GetComponent<DriverPatternController>().ActiveDriver(driverPrefab);
			SetMainRenderer();
		}
	}

	void SetMainRenderer()
	{
		var size = 0f;
		var allBounds = _currentModel.GetComponentsInChildren<Collider>();
		foreach (var item in allBounds)
		{
			if (item.bounds.size.magnitude > size)
			{
				// Debug.Log(item.gameObject.name);
				size = item.bounds.size.magnitude;
				_mainRendererBounds = item.bounds;
			}
		}
	}

	public Bounds GetRendererBounds()
	{
		return _mainRendererBounds;
	}

	public Transform GetMainModelTransform()
	{
		return _currentModel.transform;
	}

	public void StopRenderAnimatior()
	{
		if (_animator != null)
			_animator.enabled = false;
	}
}