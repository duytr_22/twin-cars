//
//  GameCenter.h
//  TemplateProject
//
//
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>


@interface GameCenter : NSObject

+ (GameCenter *)sharedGameCenter;

- (BOOL) startGameCenter;
- (BOOL) isGameCenterAPIAvailable;
- (void) authenticateLocalPlayer;
- (void) reportScore:(int64_t)score Category:(NSString *)category;
- (void) showLeaderboard;   // インスタンス版
- (void) getUserRanking;

@end

@interface GameCenter (Deprecated)

// 以下基本的にDeprepcatedでお願いします
+ (BOOL) startGameCenter DEPRECATED_ATTRIBUTE;
// ゲームセンター使用可能かどうか
+ (BOOL) isGameCenterAPIAvailable DEPRECATED_ATTRIBUTE;
// 接続、認証
+ (void) authenticateLocalPlayer DEPRECATED_ATTRIBUTE;
// スコア送信、表示
+ (void) reportScore:(int64_t)score Category:(NSString *)category DEPRECATED_ATTRIBUTE;
+ (void) showLeaderboard DEPRECATED_ATTRIBUTE;

@end
