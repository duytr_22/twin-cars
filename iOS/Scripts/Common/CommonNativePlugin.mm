//
//  CommonNativePlugin.mm
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  Unity側との汎用的な処理の連携を行うプラグイン

#import "CommonNativeController.h"

//=================================================================================
//Unity側との連携
//=================================================================================


extern "C" {
    
    //初期化、ネイティブ側のプラグインのインスタンスを作成し、Unity側へ渡す
    void *_CommonNativePlugin_Init(const char *gameObjectName){
        id instance = [[CommonNativeController alloc]initWithGameObjectName:gameObjectName];
        CFRetain((CFTypeRef)instance);
        return (__bridge void *)instance;
    }
    
    //Flurryにイベント送信
    void _ReportToFlurry(void *instance, const char *eventName){
        CommonNativeController *controller = (__bridge CommonNativeController*)instance;
        [controller reportFlurryWithEvent:eventName];
    }
    
    //アプリ内でAppStoreを開く
    void _ShowAppStoreInApp(void *instance, int idNo){
        CommonNativeController *controller = (__bridge CommonNativeController*)instance;
        [controller showAppStoreInApp:idNo];
    }
    
	//ウェブページ表示
	void _ShowCommonWeb (void *instance){
		CommonNativeController *controller = (__bridge CommonNativeController*)instance;
		[controller showCommonWeb:true];
	}
	
    //レビュー催促を表示する
    void _ShowReviewPopUp(void *instance){
        CommonNativeController *controller = (__bridge CommonNativeController*)instance;
        [controller showReviewPopUp];
    }
    
    //レビューページを表示する
    void _ShowReviewPage(void *instance){
        CommonNativeController *controller = (__bridge CommonNativeController*)instance;
        [controller showReviewPage];
    }
}


