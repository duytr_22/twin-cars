﻿//  RetryButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kikuchikan on 2015.08.05.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// リトライボタンのアシスタントクラス
/// </summary>
public class RetryButtonAssistant : ButtonAssistant
{
	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{		
		
//		bool checkShowAd = ResultController.Instance.ShowInterstial;
//		if (checkShowAd) {
//
//			Time.timeScale = 0;
//			MainManager.Instance.showAdWhenRetry = true;
//			AdManager.Instance.ShowSplashAd ();
//		}
			
		SceneNavigator.Instance.MoveScene (SceneName.GAME);
	}

}