//
//  UserInfoObject.m
//
//  Created on 1/25/13.
//

#import "UserInfoObject.h"

#import "NSString+UUID.h"
#import "NSString+Encryotion.h"

typedef void (^UserInfoObjecteConfirmWhichNewAppAvailableBlock)(NSDictionary *dict, NSError *error);


@interface UserInfoObject ()
@end

@implementation UserInfoObject
@synthesize fmsReachability = _fmsReachability;



- (Reachability *) fmsReachability
{
    if (_fmsReachability == nil) {
        _fmsReachability = [Reachability reachabilityWithHostName:self.fmsHost] ;
    }

    return _fmsReachability;
}


- (void)encodeWithCoder:(NSCoder*)coder
{
}


- (id)initWithCoder:(NSCoder*)decoder
{
    self = [super init];
    return self;
}


- (NSString *) fmsHost
{
    return FMS_HOST;
}


+ (UserInfoObject *) userInfo
{
    UserInfoObject *userInfo = [[UserInfoObject alloc] init];

    return userInfo;
}


- (void) confirmWhichNewAppAvailableWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.fmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.fmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = POP_ID;

        //まず言語のリストを取得します。
        NSArray *languages = [NSLocale preferredLanguages];
        // 取得したリストの0番目に、現在選択されている言語の言語コード(日本語なら”ja-JP”)が格納されるので、NSStringに格納します。
        NSString *languageID = [languages objectAtIndex:0];

        // 日本語以外の場合
        if (![languageID isEqualToString:@"ja-JP"]) {
            flagID = POP_ID_E;
        }

        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];

        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (error) {
            return;
        }

        NSError *json_error;
        __block NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&json_error] ;

        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler(json, error);  // 返り値から値を取得
            }
            json = nil;
        });
    });
}


- (void) confirmWhichGenuineAdAvailableWithHandler:(void (^)(NSDictionary *, NSError *))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.fmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.fmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = END_ID;

        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];

        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (error) {
            return;
        }

        NSError *json_error;
        __block NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&json_error] ;

        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler(json, error);  // 返り値から値を取得
            }
           json = nil;
        });
    });
}


- (void) confirmWhichAdSplashWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.fmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.fmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = SPL_ID;

        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];

        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (error) {
            return;
        }

        NSError *json_error;
        __block NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&json_error] ;

        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler(json, error);  // 返り値から値を取得
            }
            json = nil;
        });
    });
}

@end
