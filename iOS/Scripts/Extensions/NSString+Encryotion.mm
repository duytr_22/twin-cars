//
//  NSString+Encryotion.m
//  colors
//
//  Created by masaya takaki on 1/24/13.
//  Copyright (c) 2013 littlenstar.com. All rights reserved.
//

#import "NSString+Encryotion.h"
#include "Encryption.h"

#define key 123453

@implementation NSString (Encryotion)

+ (NSString *) stringWidthEncryptedData:(NSData *) data
{
    std::string decoded = Encryption::xorCalc(key, (const char *)[data bytes]);
//    NSData * data = [[[NSData alloc] initWithBytes:decoded.c_str() length:[data length]] autorelease];
    return [[NSString alloc] initWithBytes:decoded.c_str() length:decoded.length() encoding:NSUTF8StringEncoding];
}

- (NSData *) encryptedString
{
    std::string encoded = Encryption::xorCalc(key, (const char *)[self UTF8String]);
    return [[NSData alloc] initWithBytes:encoded.c_str() length:encoded.size()];
}

@end
