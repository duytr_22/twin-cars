﻿//  TitleAdButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// タイトルシーン下部の広告ボタンのアシスタントクラス
/// </summary>
public class TitleAdButtonAssistant : ButtonAssistant
{

	[SerializeField]
	private GameObject _houseAdButton;

	//=================================================================================
	//初期化
	//=================================================================================

	protected override void Awake ()
	{
		base.Awake ();

		//iOSは消す
		//#if UNITY_IOS
		gameObject.SetActive (false);

		if (_houseAdButton != null) {
			_houseAdButton.transform.SetLocalPositionX (0);
		}

		//#endif
	}

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{
		AdManager.Instance.ShowWallAd ();
	}

}
