﻿//  ModeSelectButtonAssistant.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.25.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// モード選択ボタンの補助クラス
/// </summary>
public class ModeSelectButtonAssistant : ButtonAssistant
{

	//このボタンのステージNo
	[SerializeField]
	private int _stageNo = 1;

	//ベストスコアを表示するテキスト
	[SerializeField]
	private Text _bestScoreText;

	//イベントシステム
	[SerializeField]
	private GameObject _eventSystem;

	//=================================================================================
	//初期化
	//=================================================================================

	private void Start ()
	{
		if (_bestScoreText != null) {
			int highScore = Mathf.RoundToInt ((float)UserData.HighScore * 100f);
			_bestScoreText.text = highScore.ToString () + Define.SCORE_UNIT;
		}
	}

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{

		CacheData.Entity.StageNo = _stageNo;

		//EventSystemが重複しないように
		_eventSystem.SetActive (false);
	}

}