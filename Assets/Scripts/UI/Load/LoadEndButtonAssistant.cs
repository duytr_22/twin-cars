﻿//  LoadEndButtonAssistant.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.29.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ロードシーンからゲームを開始するボタンのアシスタントクラス
/// </summary>
public class LoadEndButtonAssistant : ButtonAssistant
{

	private bool _isInteractable = true;

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClickButton ()
	{
		SceneNavigator.Instance.MoveScene (SceneName.GAME);
	}
}